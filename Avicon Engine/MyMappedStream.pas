unit MyMappedStream;

interface

uses
  Windows, Classes, SysUtils, Dialogs;

type

  TMappedStream = class
  private

    FBody: TFileStream;
    FBufferSize: Longint;
    FBuffer: Pointer;
    FPosition: Longint;
    FLoadedPos: Longint;
    FLoadedSize: Longint;
    FSize: Longint;

    function GetPosition: Longint;
    procedure SetPosition(const NewPos: Longint);
    function GetSide: Longint;
    procedure SetSize(const NewSize: Longint);

  public

    constructor Create;
    destructor Destroy; override;

    procedure LoadFromFile(FN: string);
    procedure ReadBuffer(var Ptr; Size: Longint);
    procedure WriteBuffer(const Ptr; Size: Longint);

    function GetPtr: Pointer;
    property Position: Longint read GetPosition write SetPosition;
    property Size: Longint read GetSide write SetSize;
  end;

////////////////////////////////////////////////////////////////////////////////
// TCustomMappedStream
////////////////////////////////////////////////////////////////////////////////
(*
 TCustomMappedStream = class(TStream)
 private
   FMapHandle: THandle;
   FFileHandle: THandle;
   FReadOnly: Boolean;
   FMemory: Pointer;
   FPosition, FSize: Int64;
 protected
   function CreateMapView(APageSize: Int64; AName: PChar = nil): Int64;
   procedure CloseMapView;
 public
   function Read(var Buffer; Count: Longint): Longint; override;
   function Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;
   property Memory: Pointer read FMemory;
   property MapHandle: THandle read FMapHandle;
   property FileHandle: THandle read FFileHandle;
   property ReadOnly: Boolean read FReadOnly;
 end;

////////////////////////////////////////////////////////////////////////////////
// TFileMappedStream
////////////////////////////////////////////////////////////////////////////////

 TFileMappedStream = class(TCustomMappedStream)
 private
   FFilename: String;
 protected
   procedure SetSize(NewSize: Longint); overload; override;
   procedure SetSize(const NewSize: Int64); overload; override;
 public
   constructor Create(const Filename: String; Mode: Word);
   destructor Destroy; override;
   function Write(const Buffer; Count: Longint): Longint; override;
   procedure Clear;
 end;
 *)

// resourcestring
// rsNoDataAvailable = 'No data available.';
// rsCannotAccessMemoryData = 'Cannot access memory data.';


implementation


constructor TMappedStream.Create;
begin
  FBufferSize:= 10 * 1024 * 1024;
  GetMem(FBuffer, FBufferSize);
  FBody:= nil;
  FLoadedPos:= -1;
  FLoadedSize:= -1;
  FPosition:= 0;
end;

destructor TMappedStream.Destroy;
begin
  FreeMem(FBuffer);
  FBody.Free;
  FBody:= nil;
end;

procedure TMappedStream.LoadFromFile(FN: string);
begin
  FBody:= TFileStream.Create(FN, fmOpenReadWrite or fmShareDenyNone);
  FSize:= FBody.Size;
end;

procedure TMappedStream.ReadBuffer(var Ptr; Size: Longint);
//var
//  src: Pointer;

begin
  if not ((FPosition >= FLoadedPos) and (FPosition + Size <= FLoadedPos + FLoadedSize)) then
  begin
    FBody.Position:= FPosition;
    FLoadedPos:= FPosition;
    FLoadedSize:= FBufferSize;
    if FBody.Position + FBufferSize - 1 > FBody.Size then FLoadedSize:= FBody.Size - FBody.Position;
    FBody.ReadBuffer(FBuffer^, FLoadedSize);
  end;

//  src:= Pointer(Integer(FBuffer) + FPosition - FLoadedPos);
//  System.Move(src^, Ptr, integer(Size));
  System.Move(Pointer(Longint(FBuffer) - FLoadedPos + FPosition)^, Ptr, Size);
  Inc(FPosition, Size);
end;

procedure TMappedStream.WriteBuffer(const Ptr; Size: Longint);
begin
  FBody.Position:= FPosition;
  FBody.WriteBuffer(Ptr, Size);
  FPosition:= FBody.Position;
end;

function TMappedStream.GetPosition: Longint;
begin
//  Result:= FBody.Position;
  Result:= FPosition;
end;

procedure TMappedStream.SetPosition(const NewPos: Longint);
begin
//  FPosition:= NewPos
  if (NewPos >= 0) and (NewPos <= FSize) then FPosition:= NewPos;
//                                             else if (NewPos < 0) then
//                                                  ShowMessage('SetPosition - Val < 0');
end;

function TMappedStream.GetSide: Longint;
begin
  Result:= FSize;
end;

procedure TMappedStream.SetSize(const NewSize: Longint);
begin
  FLoadedPos:= -1;
  FLoadedSize:= -1;
  FBody.Size:= NewSize;
  FSize:= NewSize;
end;

function TMappedStream.GetPtr: Pointer;
begin
  Result:= Pointer(Integer(FBuffer) + FPosition - FLoadedPos);
end;

(*
{$IFDEF CPUX64}
type
  PtrInt  = Int64;
  PtrUInt = UInt64;
{$ELSE}
type
  PtrInt  = LongInt;
  PtrUInt = LongWord;
{$ENDIF}

////////////////////////////////////////////////////////////////////////////////
// TCustomMappedStream
////////////////////////////////////////////////////////////////////////////////

function TCustomMappedStream.CreateMapView(APageSize: Int64; AName: PChar = nil): Int64;
var
 Inf: TMemoryBasicInformation;
begin
 if APageSize = 0 then
   APageSize := 1;
 if not Readonly then
   FMapHandle := CreateFileMapping(FFileHandle, nil, PAGE_READWRITE, 0, APageSize, AName)
 else
   FMapHandle := CreateFileMapping(FFileHandle, nil, PAGE_READONLY, 0, APageSize, AName);
 Win32Check(FMapHandle <> 0);

 if not FReadOnly then
   FMemory := MapViewOfFile(FMapHandle, FILE_MAP_READ or FILE_MAP_WRITE, 0, 0, 0)
 else
   FMemory := MapViewOfFile(FMapHandle, FILE_MAP_READ, 0, 0, 0);

 if (Assigned(FMemory)) and (AName = nil) then
   Result := APageSize
 else
   if (Assigned(FMemory)) and (Assigned(AName)) then
     begin
       VirtualQuery(FMemory,inf,SizeOf(TMemoryBasicInformation));
       Result := inf.RegionSize;
     end
   else
     raise EStreamError.Create(SysErrorMessage(GetLastError));
end;

//------------------------------------------------------------------------------

procedure TCustomMappedStream.CloseMapView;
begin
 if Memory <> nil then
   UnmapViewOfFile(Memory);
 if FMapHandle <> 0 then
   CloseHandle(FMapHandle);
end;

//------------------------------------------------------------------------------

function TCustomMappedStream.Read(var Buffer; Count: Integer): Longint;
begin
 if FMemory <> nil then
   begin
     if FPosition + Count > Size then
       Count := FSize - FPosition;
     Move(Pointer(PtrInt(FMemory) + FPosition)^, Buffer, Count);
     Inc(FPosition, Count);
     Result := Count;
   end
 else
   raise EStreamError.Create(rsNoDataAvailable);
end;

//------------------------------------------------------------------------------

function TCustomMappedStream.Seek(const Offset: Int64; Origin: TSeekOrigin): Int64;
begin
 case Origin of
   soBeginning: FPosition := Offset;
   soCurrent  : FPosition := FPosition + Offset;
   soEnd      : FPosition := FSize + Offset;
 end;
 Result := FPosition;
end;

////////////////////////////////////////////////////////////////////////////////
// TFileMappedStream
////////////////////////////////////////////////////////////////////////////////

constructor TFileMappedStream.Create(const Filename: String; Mode: Word);
const
 Access: array[0..4] of Cardinal = (GENERIC_READ, GENERIC_WRITE,
                                    GENERIC_READ or GENERIC_WRITE,
                                    GENERIC_READ or GENERIC_WRITE,
                                    GENERIC_READ or GENERIC_WRITE);
 CreateFlag: array[0..4] of Cardinal = (OPEN_EXISTING, OPEN_EXISTING,
                                        OPEN_EXISTING, CREATE_ALWAYS,
                                        CREATE_ALWAYS);
var
 FileFlag: Cardinal;
begin
 inherited Create;
 FFilename := Filename;
 FReadOnly := Mode = 0;
 if Mode = fmCreate then
   Mode := 3;
 if Mode <> 4 then
   FileFlag := FILE_ATTRIBUTE_NORMAL
 else
   FileFlag := FILE_FLAG_DELETE_ON_CLOSE;
 FFileHandle := CreateFile(PChar(FFilename), Access[Mode], 0, nil, CreateFlag[Mode], FileFlag, 0);
 Win32Check(FFileHandle <> INVALID_HANDLE_VALUE);
 FSize := CreateMapView(GetFileSize(FFileHandle, nil));
end;

//------------------------------------------------------------------------------

procedure TFileMappedStream.SetSize(NewSize: Integer);
begin
 SetSize(Int64(NewSize));
end;

//------------------------------------------------------------------------------

procedure TFileMappedStream.SetSize(const NewSize: Int64);
begin
 CloseMapView;
 SetFilePointer(FFileHandle, NewSize, nil, FILE_BEGIN);
 SetEndOfFile(FFileHandle);
 FSize := CreateMapView(NewSize);
end;

//------------------------------------------------------------------------------

destructor TFileMappedStream.Destroy;
begin
 CloseMapView;
 CloseHandle(FFileHandle);
 inherited Destroy;
end;

//------------------------------------------------------------------------------

function TFileMappedStream.Write(const Buffer; Count: Integer): Longint;
begin
 if (FMemory <> nil) and (not Readonly) then
   begin
     if (FPosition + Count > FSize) then
       SetSize(FPosition+Count);
     Move(Buffer,Pointer(PtrInt(FMemory) + FPosition)^,Count);
     inc(FPosition,Count);
     Result := Count;
   end
 else
   raise EStreamError.Create(rsCannotAccessMemoryData);
end;

//------------------------------------------------------------------------------

procedure TFileMappedStream.Clear;
begin
 SetSize(0);
 FPosition := 0;
end;
    *)

end.
