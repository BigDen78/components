{$I DEF.INC}

{$DEFINE FIX_COORD_JUMP}

{$IF DEFINED(ESTONIA) or DEFINED(AVICON31E_KZ)}
{$DEFINE MY_DATA_STREAM}
{$ENDIF}


{ ���� ����� ��� �������, ��� RELEASE ������ ���� ��� ����! }
{ $DEFINE GENERATE_LOG }
{ $ DEFINE GENERATE_GLOBAL_LOG}
{ $ DEFINE SPECIAL_LOG_N1}
{ $ DEFINE SPECIAL_LOG_N2} // ��� ���� ������ ���� ����� "C:\1\events.log"
{ $ DEFINE ALWAYs_Analyze } // �������� ������ ����� ��� ������ ��������
{ $ DEFINE SKIP_BACKMOTIONS}
{ $ DEFINE DEBUG_ADD_COORD} // ��������� ������� ����������
{ $ DEFINE DEBUG_CutExHeader} // - ������� ExHeader ��� �������� ���� ������

unit AviconDataSource;

interface

uses
  Types, Classes, Controls, Windows, Messages, SysUtils, Math, AviconTypes, DataFileConfig, SyncObjs, Dialogs, PNGImage, MyMappedStream, Graphics;

//{$SetPEFlags IMAGE_FILE_LARGE_ADDRESS_AWARE}

type

  TAviconDataSourceErrors = (eIncorrectFileVersion, eIncorrectFile, eFileNotFound, eUnknownError, eFileisNoLongerSupported, eFileisNotSupported);

  TAviconDataSourceError = set of TAviconDataSourceErrors;

  TMiasParams = record
    Par: TEvalChannelsParams;
    Sensor1: array [TRail] of Boolean;
    Temperature: Single; // ����������� ����������� �������
    Mode: Integer;

    Channel: Boolean; // ���� ������� � ��� ��� ���� ������� ���� � ����� ������
    Rail: TRail;      // ����
    EvalChNum: Byte;  // ����� ������ ������
    Time: Word;       // ����� ���������� � ���������� ������ (���)

  end;

  TKMKontentItem = record
    Pk: Integer;
    Len: Int64;
    Idx: Integer;
  end;

  TCoordListItem = record
    KM: Integer;
    Content: array of TKMKontentItem;
    Len_: Integer;
    Idx_: Integer;
  end;

  TCoordList = array of TCoordListItem;

  // ------------------------< ������ ������� �������� >---------------------------------------

  THSEcho = packed record
    Delay: array [1 .. 16] of Byte;
    Ampl: array [1 .. 16] of Byte;
    Count: Integer;
    Idx: Integer;
  end;

  THSListItem = record
    // HSCh: Integer;
    // HSMode: Integer;
    EventIdx: Integer;
    DisCoord: Integer;
    SysCoord: Integer;
    Header: THSHead;
    PrismDelay: Integer;
    PrismDelayFlag: Boolean;
    Samples: array of THSEcho;
  end;

  THSList = array of THSListItem;

  // ------------------------< ������ ������� ������� >---------------------------------------

  TMinMaxPair = packed record
    MinAtt: Byte;
    MaxAtt: Byte;
  end;

  THeadScanListItem = packed record
    Size: Integer;
    Rail: TRail;    // ���� ���� ���������� ��������: ����� / ������
    WorkSide: Byte; // ������� �����: ������� ������� 1 / ������� ������� 2
    ThresholdUnit: Byte; // ������� ��������� ������ ������������ ��������������� �����
    ThresholdValue: Byte; // ����� ������������ ��������������� �����������: �� ��� % (
    OperatingTime: Integer; // ����� ������ �� ��������: ���.

    CodeDefect: THeaderBigStr; // ����������: ��� �������
    Sizes: THeaderBigStr; // ����������: �������
    Comments: THeaderBigStr; // ����������: �����������
    Cross_Cross_Section_Image_Size: Integer; // ���������� ������� - ������ ����������� � ������� PNG
    Vertically_Cross_Section_Image_Size: Integer; // ������������ ������� - ������ ����������� � ������� PNG
    Horizontal_Cross_Section_Image_Size: Integer; // �������������� ������� - ������ ����������� � ������� PNG
    Zero_Probe_Section_Image_Size: Integer; // ������ ������ - ������ ����������� � ������� PNG

    Link: THeaderBigStr; // �������� - ����� ��� ����� �����
    ThresholdValueOfAttenuator: array [1..3] of TMinMaxPair; // ��������� �������� �����������
    deltaH: Integer;  // �������� ������ �������
    deltaL: Integer;  // �������� ������������� ������� (����� ������)
    deltaX: Integer;  // �������� ������ �������
    H: Integer;       // ������� ��������� ������� �� ����������� ������� ������
    Latitude: Single; // GPS ���������� - ������
    Longitude: Single;// GPS ���������� - �������
    Temperature: Single; // �����������
    RailType: THeaderBigStr; // ��� ������

    Cross_Cross_Section_Image: array of Byte; // ����������� � ������� PNG
    Vertically_Cross_Section_Image: array of Byte; // ����������� � ������� PNG
    Horizontal_Cross_Section_Image: array of Byte; // ����������� � ������� PNG
    Zero_Probe_Section_Image: array of Byte; // ������ ������ - ����������� � ������� PNG
    Cross_Image: TPNGimage;
    Vertically_Image: TPNGimage;
    Horizontal_Image: TPNGimage;
    Zero_Probe_Image: TPNGimage;

    DisCrd: Integer;
    EventIdx: Integer;
    OperatorName: string;
    FormatVer: Byte;
  end;

  PHeadScanListItem = ^THeadScanListItem;

  THeadScanList = array of THeadScanListItem;

  // ------------------------< ������ ������� �������� �-��������� >---------------------------------------

  THSAScanListItem = packed record
    DataSize: Integer;
    DataType: Byte;
    Rail: TRail; // �������
    Channel: Byte; // ����� ������
    ScanSurface: Byte; // ����� ����������� ������������ ������
    TimeDelay: Word; // ����� � ������, ��� / 10
    Att: ShortInt; // ����������, ��
    Ku: ShortInt; // �������� �������� ����������������, ��
    TVG: ShortInt; // �������� ���, ���
    StGate: Byte; // ������ ������ ���, ���
    EdGate: Byte; // ����� ������ ���, ���
    StrokeDuration: Byte;
    ImageData: array of Byte; // ����������� � ������� PNG (Size - 10)
    Image: TPNGimage;
  end;

  THSAScanList = array of THSAScanListItem;

// ----------------------------------------------------------------------------

  TimeListItem = record
    H: Integer;
    M: Integer;
    DC: Integer;
  end;

  TBMItem = packed record
    StSkipOff: Integer;
    OkOff: Integer;
    // Pack: TPackData;
    StDisCrd: Integer;
    EnDisCrd: Integer;
  end;

  THCListItem = record // 182 �����
    Rail: TRail;             // ����
    Ch: Integer;
    StartDisCoord: Integer;     // ���������� ���������� �������
    EndDisCoord: Integer;       // ���������� ���������� �������
    Valut: Integer;             // ���������� ���������
  end;

  TMediaItem = record

    DataType: Integer;
    Data: TMemoryStream;
    EventIdx: Integer;

  end;

  TAirBrushTemoOffItem = record
    StartDisCoord: Integer;     // ���������� ���������� ������ ���� ����������
    EndDisCoord: Integer;       // ���������� ���������� ����� ���� ����������
  end;

  TVData = record
    Km : Integer;
    Pk : Integer;
    M  : Integer;
    mm : Integer;
    Value: Extended;
  end;

  // ������������ �������
  {
  TAKStateDataItemStates = array [rLeft..rRight] of array [0..15] of Boolean;

  TAKStateDataItem = record
    DisCrd: Integer;
    SummState: Boolean;
    State: TAKStateDataItemStates;
  end;
   }
  TChFileIdxMask = array [0..15] of Boolean;

  TAKStateDataItem_ = record
    DisCrd: Integer;
    Rail: TRail;
    Ch: Integer;
    State: Boolean;
  end;

  TACPointsItem = record
    SysCoord: Integer; // ��������� �����������
    DisCoord: Integer; // ���������� �����������
    State: Boolean; // ��������� ��
    BackMotionFlag: Boolean; // ���� ���� �������� �����
  end;

  TUCSItem = record
    StSysCoord: Integer; // ��������� ����������� ������
    StDisCoord: Integer; // ���������� ����������� ������
    EdSysCoord: Integer; // ��������� ����������� �����
    EdDisCoord: Integer; // ���������� ����������� �����
  end;

  TBodyBadZones = record
    From: Integer;
    To_: Integer;
  end;

  TGoodIDList = array [0..255] of Boolean;

  TAviconDataSource = class
  private
    FConfig: TDataFileConfig;
    FHead: TFileHeader;
    FFileName: string; // ��� �����
    {$IFDEF MY_DATA_STREAM}
    FBody: TMappedStream;
    {$ENDIF}
    {$IFNDEF MY_DATA_STREAM}
    FBody: TMemoryStream;
    {$ENDIF}
    FExHeader: TExHeader;
    FExHdrLoadOk: Boolean;
    FEvents: array of TFileEvent;
    FParams: array of TMiasParams;
    FSaveEventsData: array of TFileEvent;
    FNotebook: array of TFileNotebook;
    FTimeList: array of TimeListItem;
    FCoordList: TCoordList;
    FModifyData: Boolean;
    FUnknownFileVersion: Boolean;
    FDataSize: Integer;
    FLastErrorIdx: Integer;
    FLastBMIdx: Integer;
    FMaxSysCoord: Integer;
    FCurSysCoord: Longint;
    FCurDisCoord: Int64;
    FSaveSysCoord: Longint;
    FSaveDisCoord: Int64;
    FCurParams: TMiasParams;
    FCurOffset: Integer;
    FUnPackFlag: array [TRail] of Boolean;
    FGetBMStateIdx: Integer;
    FGetBMStateState: Boolean;
    FGetParamIdx: Integer;
    FRSPSkipSideRail: Boolean;
    FRSPFile: Boolean;
    FSkipBM: Boolean;
    FCanSkipBM: Boolean;
    FBMItems: array of TBMItem;
    // ����� ��� ������ � ���-���������
    FSrcDat: array [0 .. 7] of Byte;
//    FDestDat: array [rLeft .. rRight] of TDestDat;
    FFullHeader: Boolean; // ���� � ����������� ����������
    FExtendedChannels: Boolean; // ����������� ������ � �������� > 16

    FSaveFileDate: TDateTime; // ���� �������� ����� - �� ������ �������� �����
    FSaveFileSize: Integer; // ������ ����� - �� ������ �������� �����
    FReadOnlyFlag: Boolean; // ���� ������ ��� ������
    FObmen1: Integer;
    FParDat: TMemoryStream;
    FLoadFromStream: Boolean;
    FCoordSys: TCoordSys;
    FBScanChList: array of Integer;
    FBScanChListOK: array of Boolean;

    {$IFDEF GENERATE_LOG}
    FLog: TStringList;
    {$ENDIF}
    {$IFDEF SPECIAL_LOG_N1}
    FLog: TStringList;
    {$ENDIF}
    {$IFDEF SPECIAL_LOG_N2}
    FLog: TStringList;
    {$ENDIF}

    FFillEventsData: Boolean;

    FTimeExists: Boolean; // ������� ������� �������
    FTemperatureExists: Boolean; // ������� �����������
    FSpeedExists: Boolean; // ������� �������� �������� � ���������� � ���������� ��������
    FTestRecordFileExists: Boolean; // ������� ����� ������ ������������ ������
    FTestRecordFileOffset: DWord;
    FTestRecordFileSize: DWord;

    FPaintSystemParamsExists: Boolean;
    FPaintSystemParams: TPaintSystemParams;

//    FNewFileFormat: Boolean; // ����� ������ �����: ������-31, ������-15 (Nautiz X8), Filux X111W
    FChFileIndex: TIntegerDynArray; // ������ �������� ������� �����
    FChFileIdxMask: TChFileIdxMask; // ����� ������������ � ����� �������

    FAnalyzeACExecuted: Boolean;
    FAnalyzeACExecuted_MinLen: Integer;
    FAnalyzeACExecuted_MaxIncludeLen: Integer;
    FSkipEventFlag_HandScan: Boolean;
    FSkipCoordEventCount: Integer;
    FSkipCoordFlag: Integer;
    FSkipCoordSysCrd: Integer;

//    {$IFDEF SPECIAL_LOG_N2}
    FFirstSysCoord: Boolean;
    FSaveFullSysCoord: Integer;
//    {$ENDIF}

    FIsGoodID: TGoodIDList;

    LoadDataCount: Integer;

//    FuckFlag: Boolean;
    NewGPSPoint: TGPSPoint;

  protected

    function GetId: Integer;
    function GetErrorCount: Integer;
    function GetCoordSys: TCoordSys;
    function GetStartMRFCrd: TMRFCrd;
    function GetEndMRFCrd: TMRFCrd;
    function GetStartCaCrd: TCaCrd;
    function GetEndCaCrd: TCaCrd;
    procedure LoadExData;
    procedure SaveExData(CutOldExHeader: Boolean);
    procedure AnalyzeFileBody;
    function FillEventsData: Boolean;
    procedure FillTimeList;

    function FindBSState(StartDisCoord: Integer): Boolean;
    procedure TestBMOffsetFirst;
    function TestBMOffsetNext: Boolean;
    procedure SetSkipBM(NewState: Boolean);
    function GetSysCoordByRealIdx(KmIdx, PkIdx: Integer; Metr: Extended): Integer;
    function GetEventCount: Integer;
    function GetEvent(Index: Integer): TFileEvent;
    function GetParam(Index: Integer): TMiasParams;
    function GetNotebookCount: Integer;
    function GetNotebookP(Index: Integer): pFileNotebook;
    function GetMaxDisCoord: Integer;
    function NormalizeDisCoord(Src: Integer): Integer;
    function SkipEvent(ID: Byte): Boolean; // ������� �������
    function LoadEventData(ID: Byte; Ptr: PEventData; MaxSize: Integer): Boolean; // ������� ������ �������
    function GetAirBrushCount: Integer;
    function GetAirBrushItem(Index: Integer): tAirBrushItem;
    function GetAutoSearchResListCount: Integer;
    function GetAutoSearchResItem(Index: Integer): edAutomaticSearchRes;
    function GetAlarmCount: Integer;
    function GetHCCount: Integer;
    function GetLongLabelCount: Integer;
    function GetHCItem(Index: Integer): THCListItem;
    function GetLLItem(Index: Integer): edLongLabel;
    function GetAlarmItem(Index: Integer): edAlarmData;
    function GetStartRealCoord: TRealCoord;
    function GetEndRealCoord: TRealCoord;
    procedure GetGoodID(DeviceID: TAviconID);

  public
    FBodyBadZones: array of TBodyBadZones;

    Coord_DEBUG_GRAPH: array of TPoint;
    OperatorNameList: TStringList;
    CurrentOperatorName: string;
    BoltStykMarkExists: Boolean;
    BoltStykMark_MinDS: Integer;
    BoltStykMark_MaxDS: Integer;

    SkipFlag: Boolean;
    FInBMZone: array of Boolean;
    FNotebookInBMZone: array of Boolean;

    FCurEcho: TCurEcho;
    FACPoints: array of TACPointsItem;
    Debug: Boolean;

    EventsCount: array [0..255] of Integer;
    VData: array [0..3] of array of TVData;
    StartFileSensor1: array [TRail] of Boolean;
    Sensor1DataExists: array [TRail] of Boolean;

    SensorPos: Integer;
    AirBrushPos: Integer;

    Error: TAviconDataSourceError;

    UnpackBottom: Boolean;
    FCDList: array of Integer;
    FAirBrushList: array of tAirBrushItem; // edAirBrush2;
    FAutomaticSearchResList: array of edAutomaticSearchRes;
    FAlarmList: array of edAlarmData;
//    FAirBrushList_: array of Tpoint;   // DEBUG // ���������� ���������� �������� ������� �� �������������

    FHCList: array of THCListItem;
    FLongLabelList: array of edLongLabel;
    FMediaList: array of TMediaItem;
    FAirBrushTempOff: array of TAirBrushTemoOffItem;

    HSList: THSList;
    HeadScanList: THeadScanList;
    HSAScanList: THSAScanList;
    BackMotion: Boolean;

  // ������������ �������

//    AKStateData: array of TAKStateDataItem;
    AKStateData_: array of TAKStateDataItem_;
    UCSData: array of TUCSItem;
    UCSProcent: Single;
    UCSLength: Single;
//    AKStateDataLastIndex: array [rLeft..rRight] of array [0..16] of Integer; // ��������� �� / ���������� ���������� ��

    StartShift: Integer;
    LabelList: TIntegerDynArray;
    SkipTestDateTime: Boolean;
//    CurCrdLongLinc: Boolean;

    GPSPoints: array of TGPSPoint;
    FSaveGPSPoints: array of TGPSPoint;
    Avicon15Flag: Boolean;
    AlarmTempOffState: Boolean;

    UMUA_Left_Index: Integer;
    UMUB_Left_Index: Integer;
    UMUA_Right_Index: Integer;
    UMUB_Right_Index: Integer;

    FCurLoadSysCoord_UMUA_Left: Integer;
    FCurLoadSysCoord_UMUB_Left: Integer;
    FCurLoadSysCoord_UMUA_Right: Integer;
    FCurLoadSysCoord_UMUB_Right: Integer;

    FCurLoadDelta_UMUA_Left: ShortInt;
    FCurLoadDelta_UMUB_Left: ShortInt;
    FCurLoadDelta_UMUA_Right: ShortInt;
    FCurLoadDelta_UMUB_Right: ShortInt;

    AKGis: array [0..100] of Integer;
    BAD_AC_algorithm_situation: Int64;
    GOOD_AC_algorithm_situation: Int64;

    // AK Debug
    AKCount: array [TRail, 0..16] of Integer;
    AKSummLen: array [TRail, 0..16] of Integer;
    AKExists: array [TRail, 0..100] of Boolean;
    AKMinLen: array [TRail, 0..16] of Integer;
    AKMaxLen: array [TRail, 0..16] of Integer;
    AKMediumLen: array [TRail, 0..16] of Integer;
    ACEventCount: Integer;


    constructor Create;
    destructor Destroy; override;
    function LoadFromFile(FileName: string; VengrData: boolean = False): Boolean;
//    function LoadFromStream(Stream: TStream): Boolean;
    function LoadHeader(FileName: string): Boolean;
    function ExtractTestRecordFile(FileName: string): Boolean;
    procedure CloseFile;
    procedure ReAnalyze;
    function TestCheckSumm: Boolean;

    procedure LoadData(StartDisCoord, EndDisCoord, LoadShift: Longint; BlockOk: TDataNotifyEvent);
    procedure AnalyzeFileBody_;
    function SysToDisCoord(Coord: Integer): Integer;
    function DisToSysCoord(Coord: Integer; Force: Boolean = False): Integer;
    procedure DisToDisCoords(Coord: Integer; var Res: TIntegerDynArray);
    procedure DisToDisCoords_(Coord: Integer; var Res: TIntegerDynArray; var StIdx, Count: Integer);
    function TestChangeDirCrd(Coord: Integer): Boolean;
    procedure DisToFileOffset(NeedDisCoord: Integer; var DisCoord: Int64; var SysCoord: Integer; var OffSet: Integer);
    function GetBMStateFirst(DisCoord: Integer): Boolean;
    function GetBMStateNext(var DisCoord: Integer; var State: Boolean): Boolean;
    function GetEventsData(Index: Integer): TEventData;
    function GetParamFirst(DisCoord: Integer; var Params: TMiasParams): Boolean;
    function GetParamNext(DisCoord: Integer; var Params: TMiasParams): Boolean;
    function GetSysCoordByReal(Km, Pk: Integer; Metr: Extended): Integer;

    procedure AnalyzeAC(Debug_: Boolean);
    procedure AnalyzeAC_forNoAC_Data(MinLen, MaxIncludeLen, AmplTh: Integer; Debug_: Boolean);
    procedure FilterACData(minLen_mm: Integer);
    function RailToPathRail(R: TRail): TRail;


  // -------------< ������� ��������� - ����������� �� >-----------------------------------

    function DisToMRFCrdPrm(DisCoord: Integer; var CoordParams: TMRFCrdParams): Boolean;
    function DisToMRFCrd(DisCoord: Longint): TMRFCrd;
    function MRFCrdToDisCrd(RFC: TMRFCrd; var DisCoord: Longint): Boolean;

  // -------------< ������� ��������� - ��������� >----------------------------------------

    function DisToCaCrdPrm(DisCoord: Integer; var CoordParams: TCaCrdParams): Boolean;
    function DisToCaCrd(DisCoord: Longint): TCaCrd;
    function CaCrdToDis(CaCrd: TCaCrd; var DisCoord: Longint): Boolean;

  // --------------------------------------------------------------------------------------

    function DisToCoordParams(DisCoord: Integer; var CrdParams: TCrdParams): Boolean;
    function DisToRealCoord(DisCoord: Longint): TRealCoord;

    function RealToDisCoord(Crd: TRealCoord): Longint;

    function GetTime(DisCoord: Integer; TimerIndex: Integer = 0): string;
    function GetTimeFromEvent(StartIdx: Integer; TimerIndex: Integer = 0): string;
    function GetTemperature(DisCoord: Integer; var Val: Single): Boolean;
    function GetGPSCoord(DisCoord: Integer): TExtGPSPoint;
    function GetSpeedState(DisCoord: Integer; var Speed: Single; var State: Integer): Boolean;
    function GetSpeed(DisCoord: Integer; var Speed: Single): string;
    function MinAmplCode: Integer;
    function NotebookAdd(NewDat: pFileNotebook = nil): Integer;
    procedure NotebookDelete(Index: Integer);
    procedure CutExHeader;
    procedure SetScanStep(New: Word);
    function GetEventIdx(StartDisCoord, EndDisCoord: Integer; var StartIdx, EndIdx: Integer; EmptyPar: Integer): Boolean;
    procedure GetNearestEventIdx(DisCoord: Integer; var LeftIdx, RightIdx: Integer; var SameCoord: TIntegerDynArray; Force: Boolean = False);
    function GetLeftEventIdx(DisCoord: Integer; Force: Boolean = False): Integer;
    function GetRightEventIdx(DisCoord: Integer): Integer;
    procedure GetEventData(Idx: Integer; var ID: Byte; var pData: PEventData);
    procedure SaveNB;
    procedure SavePiece(StDisCood, EdDisCood: Integer; SaveNB: Boolean; FileName: string);

    function GetTimeList(Index: Integer): TimeListItem;
    function GetTimeListCount: Integer;
    function isSingleRailDevice: Boolean;

    function isFilus_X27_Scheme1: Boolean;
    function isFilus_X27W_Scheme1: Boolean;
    function isFilus_X27W_Scheme2: Boolean;

    function isTwoWheels: Boolean;

    function isEGOUS: Boolean;
    function isEGOUS_Scheme1: Boolean;
    function isEGOUS_Scheme2: Boolean;
    function isEGOUS_Scheme3: Boolean;
    function isEGOUS_WheelScheme1: Boolean;
    function isEGOUS_WheelScheme2: Boolean;
    function isEGOUS_WheelScheme3: Boolean;

    function isVMT_US_BigWP: Boolean;
    function isVMT_US_BigWP_BHead: Boolean;
    function isEGOUSW: Boolean;
    function isVMTUS_Scheme_1: Boolean; // �������
    function isVMTUS_Scheme_2: Boolean; // �������
    function isVMTUS_Scheme_1_or_2: Boolean; // �������
    function isVMTUS_Scheme_4: Boolean; // ����
    function isVMTUS_Scheme_5: Boolean; // ������
    function isVMTUS_Scheme_4_or_5: Boolean;
    function isFilus_X17DW: Boolean;
    function isEGOUSW_BHead: Boolean;

    function isMDL: Boolean;

    function isA31: Boolean;
    function isA31_Scheme1: Boolean;
    function isA31_Scheme2: Boolean;
    function isA31Estonia_Scheme2: Boolean;

    function isUSK: Boolean;

    function isA15Old: Boolean;
    function isA15New: Boolean;

//    function isOldFileFormat: Boolean; // ����� � �������� ���������� + ������

    function isAutoDecodingEnabled: Boolean;
    procedure AutomaticSearchRes_SetSkipBM();

    property EventsData[Index: Integer]: TEventData read GetEventsData;
    property TimeList[Index: Integer]: TimeListItem read GetTimeList;
    property TimeListCount: Integer read GetTimeListCount;

    property FileName: string read FFileName;

    {$IFDEF MY_DATA_STREAM}
    property FileBody: TMappedStream read FBody;
    {$ENDIF}
    {$IFNDEF MY_DATA_STREAM}
    property FileBody: TMemoryStream read FBody;
    {$ENDIF}
    property Header: TFileHeader read FHead write FHead;

    property StartMRFCrd: TMRFCrd read GetStartMRFCrd;
    property EndMRFCrd: TMRFCrd read GetEndMRFCrd;
    property StartCaCrd: TCaCrd read GetStartCaCrd;
    property EndCaCrd: TCaCrd read GetEndCaCrd;

    property StartRealCoord: TRealCoord read GetStartRealCoord;
    property EndRealCoord: TRealCoord read GetEndRealCoord;

    property ExHeader: TExHeader read FExHeader;
    property ModifyExData: Boolean read FModifyData write FModifyData;
    property MaxDisCoord: Integer read GetMaxDisCoord;
    property EventCount: Integer read GetEventCount;
    property Event[Index: Integer]: TFileEvent read GetEvent;
    property Params[Index: Integer]: TMiasParams read GetParam;
    property NotebookCount: Integer read GetNotebookCount;
    property Notebook[Index: Integer]: pFileNotebook read GetNotebookP;
    property AirBrushCount: Integer read GetAirBrushCount;
    property AirBrush[Index: Integer]: tAirBrushItem read GetAirBrushItem;
    property AutoSearchResCount: Integer read GetAutoSearchResListCount;
    property AutoSearchRes[Index: Integer]: edAutomaticSearchRes read GetAutoSearchResItem;
    property AlarmCount: Integer read GetAlarmCount;
    property Alarm[Index: Integer]: edAlarmData read GetAlarmItem;
    property HCCount: Integer read GetHCCount;
    property HC[Index: Integer]: THCListItem read GetHCItem;
    property LLCount: Integer read GetLongLabelCount;
    property LL[Index: Integer]: edLongLabel read GetLLItem;
    property SkipBackMotion: Boolean read FSkipBM write SetSkipBM;
    property CurOffset: Integer read FCurOffset;
    property CurSysCoord: Longint read FCurSysCoord;
    property CurDisCoord: Int64 read FCurDisCoord {write FCurDisCoord};
    property CurParams: TMiasParams read FCurParams;
    property CurEcho: TCurEcho read FCurEcho write FCurEcho;
    property FullHeader: Boolean read FFullHeader;
    property CoordList: TCoordList read FCoordList;
    property CoordSys: TCoordSys read GetCoordSys;
    property Config: TDataFileConfig read FConfig;

    property TimeExists: Boolean read FTimeExists; // ������� ������� �������
    property TemperatureExists: Boolean read FTemperatureExists; // ������� �����������
    property SpeedExists: Boolean read FSpeedExists; // ������� �������� �������� � ���������� � ���������� ��������
    property TestRecordFileExists: Boolean read FTestRecordFileExists; // ������� ����� ������ ������������ ������
    property PaintSystemParamsExists: Boolean read FPaintSystemParamsExists;
    property PaintSystemParams: TPaintSystemParams read FPaintSystemParams;
    property ExtendedChannels: Boolean read FExtendedChannels; // ����������� ������ � �������� > 16
    property ID: Integer read GetId;
    property ErrorCount: Integer read GetErrorCount;
    property ChFileIndex: TIntegerDynArray read FChFileIndex; // ������ �������� ������� �����
    property ChFileIdxMask: TChFileIdxMask read FChFileIdxMask; // ����� ������������ � ����� �������
    property DataFileSize: Integer read FSaveFileSize; // ������ �����

    // FHeader: TAvk11Header2;
    // FBSAmplLink: array of TBSAmplLinkItem;
    // LoadProg: TGauge;
    // DelayKoeff: Integer;
    // CurAmpl: TCurAmpl;
    // function TestOffsetFirst: Boolean; // <--- �������� ���������� �� protected
    // function TestOffsetNext: Boolean; // <--- �������� ���������� �� protected
    // function SideToRail(Side: TSide): TRail;
    // procedure AddRSPInfo(ID: Integer; Text: string);
    // function  GetRSPTextByID(ID: Integer; var Res: Boolean): string;
    procedure GenerateGPSPoints;
    procedure MakeNoteBookFromNordco;

    function GetRailPathNumber_in_String: string;
    function GetRailPathNumber_in_HeadStr: THeaderStr;
    // procedure TestCanSkipBM;
    // property RSPSkipSideRail: Boolean read FRSPSkipSideRail write FRSPSkipSideRail;
    // property DataRail: RRail read FExHeader.DataRail;
    // property ErrorCount: Integer read GetErrorCount;
    // property Error[Index: Integer]: TFileError read GetError;
    // property FileDecodingCount: Integer read GetFileDecodingCount;
    // property FileDecoding[Index: Integer]: TFileDecoding read GetFileDecoding;
    // property RSPInfoCount: Integer read GetRSPInfoCount;
    // property RSPInfoByIdx[Index: Integer]: TRSPInfo read GetRSPInfoByIndex;
    // property RSPInfoById[Index: Integer]: TRSPInfo read GetRSPInfoByID;
    // property Log: TStringList read FLog;
    // property UnknownFileVersion: Boolean read FUnknownFileVersion;
    // property RSPFile: Boolean read FRSPFile;
    // property UnPackFlag[Index: RRail]: Boolean read GetFUnPackFlag;
    // property BodyModify: Boolean read GetBodyModify write SetBodyModify;
    // property Pack: TPackData read FPack;
  end;

  TAvk11DatSrc = TAviconDataSource;

//const
//  EchoCnt: array [0 .. 11] of Integer = (0, -1, 1, 2, -1, 3, 4, -1, 5, 6, -1, 7);
//  EchoLen: array [0 .. 7] of Integer = (2, 3, 5, 6, 8, 9, 11, 0);

var
  ConfigList: TDataFileConfigList;
  function GetString(Len: Byte; Text: array of Char): string;


implementation

var
  GlobalCount: Integer = 0;
  {$IFDEF GENERATE_GLOBAL_LOG}
  FGLog: TStringList;
  {$ENDIF}

// ------------------------------------------------------------------------------

function GetString(Len: Byte; Text: array of Char): string;
var
  I: Integer;

begin
  Result:= '';
  for I := 0 to Len - 1 do
    if (Text[I] <> Chr($A)) and (Text[I] <> '_') then Result:= Result + Text[I];
end;

// ------------------------< TAviconDataSource >---------------------------------------

constructor TAviconDataSource.Create;
begin
//  FuckFlag:= True;
//  FuckFlag:= False;
  LoadDataCount:= 0;
  SkipFlag:= False;


  FSkipEventFlag_HandScan:= False;
  FSkipCoordEventCount:= 0;

  UMUA_Left_Index:= 0;
  UMUB_Left_Index:= 0;
  UMUA_Right_Index:= 0;
  UMUB_Right_Index:= 0;

  FCurLoadSysCoord_UMUA_Left:=  0;
  FCurLoadSysCoord_UMUB_Left:=  0;
  FCurLoadSysCoord_UMUA_Right:= 0;
  FCurLoadSysCoord_UMUB_Right:= 0;

  FCurLoadDelta_UMUA_Left:=  0;
  FCurLoadDelta_UMUB_Left:=  0;
  FCurLoadDelta_UMUA_Right:= 0;
  FCurLoadDelta_UMUB_Right:= 0;


  // FAnalyzeNewCrd:= nil;
  // UnpackBottom:= True;
  // SkipTestDateTime:= False;
  Avicon15Flag:= False;
  FFillEventsData:= False;
  FSkipBM:= False;
  StartShift:= 0;
  FConfig:= nil;

  // FRSPSkipSideRail:= False;
  // FRSPFile:= False;
  FParDat:= TMemoryStream.Create;
  Sensor1DataExists[rLeft]:= False;
  Sensor1DataExists[rRight]:= False;

  SetLength(FLongLabelList, 0);
  SetLength(FHCList, 0);

  FTemperatureExists:= False; // �����������
  FTimeExists:= True; // ������� �������
  FSpeedExists:= False; // �������� � ���������� �������� ��������
  FTestRecordFileExists:= False; // ���� ������ ������������ ������

  SetLength(FAirBrushList, 0);
  SetLength(FAlarmList, 0);
//  SetLength(FAirBrushList_, 0);
  FPaintSystemParamsExists:= False;

  FAnalyzeACExecuted:= False;
  Debug:= False;

  OperatorNameList:= TStringList.Create;
  BoltStykMarkExists:= False;
end;

destructor TAviconDataSource.Destroy;
var
  I: Integer;

begin
  if FSkipBM then  SetSkipBM(False);
  if FModifyData then SaveExData(FExHdrLoadOk);

  if Assigned(FBody) then
  begin
    FBody.Free;
    FBody:= nil;
  end;

  // SetLength(FErrors, 0);
  SetLength(FEvents, 0);
  SetLength(FNotebook, 0);
  SetLength(FInBMZone, 0);
  SetLength(FNotebookInBMZone, 0);
  SetLength(FParams, 0);
  // SetLength(FDecoding, 0);
  // SetLength(FEventsData, 0);
  // SetLength(FRSPInfo, 0);
  SetLength(FCDList, 0);
  SetLength(FTimeList, 0);
  SetLength(GPSPoints, 0);
  SetLength(FSaveEventsData, 0);
  SetLength(FBMItems, 0);
  for I:= 0 to High(FCoordList) do SetLength(FCoordList[I].Content, 0);
  SetLength(FCoordList, 0);
  FParDat.Free;
  SetLength(FLongLabelList, 0);
  SetLength(FHCList, 0);

  SetLength(FAirBrushList, 0);
  SetLength(FAlarmList, 0);
//  SetLength(FAirBrushList_, 0);

  SetLength(FMediaList, 0);
  SetLength(FAirBrushTempOff, 0);
  SetLength(UCSData, 0);


  SetLength(HSList, 0);
  for I := 0 to Length(HeadScanList) - 1 do
  begin
    if Assigned(HeadScanList[I].Cross_Image)      then HeadScanList[I].Cross_Image.Free;
    if Assigned(HeadScanList[I].Vertically_Image) then HeadScanList[I].Vertically_Image.Free;
    if Assigned(HeadScanList[I].Horizontal_Image) then HeadScanList[I].Horizontal_Image.Free;
    if Assigned(HeadScanList[I].Zero_Probe_Image) then HeadScanList[I].Zero_Probe_Image.Free;
  end;
  SetLength(HeadScanList, 0);
  for I := 0 to Length(HSAScanList) - 1 do HSAScanList[I].Image.Free;
  SetLength(HSAScanList, 0);

  OperatorNameList.Free;
//  SetLength(BoltStykZones, 0);
end;

function TAviconDataSource.GetId: Integer;
begin
  Result:= 1; // ������ - ������ ���� ������ ��������
end;


function TAviconDataSource.GetErrorCount: Integer;
begin
  Result:= 0; // ������ - ����� ��� �����
end;


// ------------------------< ������������ �������� ������ >---------------------

function TAviconDataSource.GetEventCount: Integer;
begin
  Result:= Length(FEvents);
end;

function TAviconDataSource.GetEvent(Index: Integer): TFileEvent;
begin
  if (Index >= 0) and (Index < Length(FEvents)) then
    Result:= FEvents[Index];
end;

function TAviconDataSource.GetParam(Index: Integer): TMiasParams;
begin
  if (Index >= 0) and (Index < Length(FEvents)) then
    Result:= FParams[Index];
end;

function TAviconDataSource.GetAirBrushCount: Integer;
begin
  Result:= Length(FAirBrushList);
end;

function TAviconDataSource.GetAutoSearchResListCount: Integer;
begin
  Result:= Length(FAutomaticSearchResList);
end;

function TAviconDataSource.GetAutoSearchResItem(Index: Integer): edAutomaticSearchRes;
begin
  if (Index >= 0) and (Index < Length(FAutomaticSearchResList)) then Result:= FAutomaticSearchResList[Index];
end;

function TAviconDataSource.GetAlarmCount: Integer;
begin
  Result:= Length(FAlarmList);
end;

function TAviconDataSource.GetHCCount: Integer;
begin
  Result:= Length(FHCList);
end;

function TAviconDataSource.GetHCItem(Index: Integer): THCListItem;
begin
  if (Index >= 0) and (Index < Length(FHCList)) then Result:= FHCList[Index];
end;

function TAviconDataSource.GetLongLabelCount: Integer;
begin
  Result:= Length(FLongLabelList);
end;

function TAviconDataSource.GetLLItem(Index: Integer): edLongLabel;
begin
  if (Index >= 0) and (Index < Length(FLongLabelList)) then Result:= FLongLabelList[Index];
end;

function TAviconDataSource.GetAirBrushItem(Index: Integer): tAirBrushItem;
begin
  if (Index >= 0) and (Index < Length(FAirBrushList)) then Result:= FAirBrushList[Index];
end;

function TAviconDataSource.GetAlarmItem(Index: Integer): edAlarmData;
begin
  if (Index >= 0) and (Index < Length(FAirBrushList)) then Result:= FAlarmList[Index];
end;

function TAviconDataSource.GetNotebookCount: Integer;
begin
  Result:= Length(FNotebook);
end;

function TAviconDataSource.GetNotebookP(Index: Integer): pFileNotebook;
begin
  Result:= @FNotebook[Index];
end;

function TAviconDataSource.GetStartRealCoord: TRealCoord;
begin
  Result.Sys:= CoordSys;
  Result.MRFCrd:= GetStartMRFCrd;
  Result.CaCrd:= GetStartCaCrd;
end;

function TAviconDataSource.GetEndRealCoord: TRealCoord;
begin
  Result.Sys:= CoordSys;
  Result.MRFCrd:= GetEndMRFCrd;
  Result.CaCrd:= GetEndCaCrd;
end;

{
  function TAviconDataSource.GetFileDecodingCount: Integer;
  begin
  Result:= Length(FDecoding);
  end;

  function TAviconDataSource.GetFileDecoding(Index: Integer): TFileDecoding;
  begin
  Result:= FDecoding[Index];
  end;

  function TAviconDataSource.GetRSPInfoCount: Integer;
  begin
  Result:= Length(FRSPInfo);
  end;

  function TAviconDataSource.GetRSPInfoByIndex(Index: Integer): TRSPInfo;
  begin
  Result:= FRSPInfo[Index];
  end;

  function TAviconDataSource.GetRSPInfoByID(ID: Integer): TRSPInfo;
  var
  I: Integer;

  begin
  for I:= 0 to High(FRSPInfo) do
  if FRSPInfo[I].ID = ID then
  begin
  Result:= FRSPInfo[I];
  Exit;
  end;
  end;

  function TAviconDataSource.GetRSPTextByID(ID: Integer; var Res: Boolean): string;
  var
  I: Integer;

  begin
  Res:= False;
  for I:= 0 to High(FRSPInfo) do
  if FRSPInfo[I].ID = ID then
  begin
  Result:= FRSPInfo[I].Text;
  Res:= True;
  Exit;
  end;
  end;

  procedure TAviconDataSource.AddRSPInfo(ID: Integer; Text: string);
  begin
  FModifyData:= True;
  SetLength(FRSPInfo, Length(FRSPInfo) + 1);
  FRSPInfo[High(FRSPInfo)].ID:= ID;
  FRSPInfo[High(FRSPInfo)].Text:= Text;
  end;
}
function TAviconDataSource.NotebookAdd(NewDat: pFileNotebook = nil): Integer;
var
  I: Integer;

begin
  FModifyData:= True;
  I:= Length(FNotebook);
  SetLength(FNotebook, I + 1);
  if Assigned(NewDat) then FNotebook[I]:= NewDat^;
  Result:= I;
end;

procedure TAviconDataSource.NotebookDelete(Index: Integer);
var
  I: Integer;

begin
  FModifyData:= True;
  for I:= Index to High(FNotebook) - 1 do FNotebook[I]:= FNotebook[I + 1];
  SetLength(FNotebook, Length(FNotebook) - 1);
end;

function TAviconDataSource.GetMaxDisCoord: Integer;
begin
  if FSkipBM then Result:= FMaxSysCoord
             else Result:= FExHeader.EndDisCoord;
end;
{
  function TAviconDataSource.GetFUnPackFlag(Index: RRail): Boolean;
  begin
  //  Result:= FUnPackFlag[Index];
  end;
}

function TAviconDataSource.GetCoordSys: TCoordSys;
begin
  Result:= TCoordSys(FHead.PathCoordSystem);
end;

function TAviconDataSource.GetStartMRFCrd: TMRFCrd;
begin
  Result.Km:= FHead.StartKM;
  Result.Pk:= FHead.StartPk;
  Result.mm:= FHead.StartMetre * 1000;
end;

function TAviconDataSource.GetEndMRFCrd: TMRFCrd;
begin
  Result:= FExHeader.EndMRFCrd;
end;

function TAviconDataSource.GetStartCaCrd: TCaCrd;
begin
  Result:= FHead.StartChainage;
end;

function TAviconDataSource.GetEndCaCrd: TCaCrd;
begin
  Result:= FExHeader.EndCaCrd;
end;

// ------------------------< ������ � ����������� ���������� >---------------------

procedure TAviconDataSource.LoadExData;
var
  I, J: Integer;
  ID: Byte;
  pData: PEventData;
  tmp: string;
  ExHeadOk: Boolean;
  NeedReAnalyze: Boolean;
  Head: TExHeaderHead;
  DataSize: Integer;
  OldNotebook5: array of TFileNotebook_ver_5;
  OldNotebook6: array of TFileNotebook_ver_6;

//  IDCount: array [0..255] of Integer;
//  IDCountLog: TStringList;

begin
  NeedReAnalyze:= False;
  ExHeadOk:= (FHead.TableLink <> 0) and (FHead.TableLink <= FBody.Size);
  FUnknownFileVersion:= False;

//  for I := 0 to 255 do IDCount[I]:= 0;

  if ExHeadOk then
    try
      FBody.Position:= FHead.TableLink;
      FBody.ReadBuffer(Head, SizeOf(Head));
    except
      // FLog.Add(Language.GetCaption(0150011));
      ExHeadOk:= False;
    end;

  if ExHeadOk then
    for I:= 0 to 13 do
      if Head.FFConst[I] <> $FF
       then
      begin
        // FLog.Add(Language.GetCaption(0150011));
        ExHeadOk:= False;
        Break;
      end;

  if ExHeadOk then
  begin

    FDataSize:= FHead.TableLink;

    case Head.DataVer of
      (*
        3: begin // �� ������ �����������, � ���� �����������������
        end;

        4: begin // �� ������ ������������, � ���� �����������������
        end;

        5: begin
        try
        FBody.Position:= FHead.TableLink;
        FBody.ReadBuffer(FExHeader, SizeOf(FExHeader));
        except
        //             FLog.Add(Language.GetCaption(0150011));
        ExHeadOk:= False;
        end;
        NeedReAnalyze:= True;
        end;
        *)
      7:
        begin
          try
            FBody.Position:= FHead.TableLink;
            FBody.ReadBuffer(FExHeader, SizeOf(FExHeader));
          except
            // FLog.Add(Language.GetCaption(0150011));
            ExHeadOk:= False;
          end;
        end;
      (*
        7..255: begin
        FUnknownFileVersion:= True;
        Exit;

        {  try
        FBody.Position:= FHead.TableLink + 14;
        FBody.ReadBuffer(DataSize, SizeOf(Integer));
        FBody.Position:= FHead.TableLink;
        FBody.ReadBuffer(FExHeader, Min(SizeOf(FExHeader), DataSize));
        except
        FLog.Add(Language.GetCaption(0150011));
        ExHeadOk:= False;
        end; }

        end; *)
    end;
    {
      if FExHeader.ErrorCount <> 0 then
      if FExHeader.ErrorDataVer = 3 then
      begin
      SetLength(FErrors, FExHeader.ErrorCount);
      try
      FBody.Position:= FExHeader.ErrorOffset;
      FBody.ReadBuffer(FErrors[0], FExHeader.ErrorCount * FExHeader.ErrorItemSize);
      except
      //          FLog.Add(Language.GetCaption(0150012));
      NeedReAnalyze:= True;
      SetLength(FErrors, 0);
      end;
      end
      else
      begin
      FUnknownFileVersion:= True;
      Exit;
      end;
      }
    if FExHeader.EventCount <> 0 then
      if FExHeader.EventDataVer = 5 then
      begin

        SetLength(FEvents, FExHeader.EventCount);
        try
          if FExHeader.EventOffset + Length(FEvents) * FExHeader.EventItemSize <= FBody.Size then
          begin
            FBody.Position:= FExHeader.EventOffset;
            for I := 0 to High(FEvents) do
            begin
              FBody.ReadBuffer(FEvents[I], FExHeader.EventItemSize);
            //IDCount[FEvents[I].ID]:= IDCount[FEvents[I].ID] + 1;
            end;
          end
          else
          begin
            NeedReAnalyze:= True;
            SetLength(FEvents, 0);
          end;
        except
          // FLog.Add(Language.GetCaption(0150013));
          NeedReAnalyze:= True;
          SetLength(FEvents, 0);
        end;

{        IDCountLog:= TStringList.Create;
        for I := 0 to 255 do IDCountLog.Add(Format('%x - %d', [I, IDCount[I]]));
        IDCountLog.SaveToFile('d:\idCount.txt');
        IDCountLog.Free; }
      end
      else
      begin
        FUnknownFileVersion:= True;
        Exit;
      end;

    if FExHeader.NotebookCount <> 0 then
    begin
      case FExHeader.NotebookDataVer of
        3:
          begin
          end;

        4:
          begin
          end;

        5:
          begin
            SetLength(OldNotebook5, FExHeader.NotebookCount);
            try
              if FExHeader.NotebookOffset + FExHeader.NotebookCount * FExHeader.NotebookItemSize < FBody.Size then
              begin
                FBody.Position:= FExHeader.NotebookOffset;
                FBody.ReadBuffer(OldNotebook5[0], FExHeader.NotebookCount * FExHeader.NotebookItemSize);
              end
              else
              begin
                SetLength(OldNotebook5, 0);
                FModifyData:= True;
              end;

            except
              SetLength(OldNotebook5, 0);
              FModifyData:= True;
            end;

            SetLength(FNotebook, FExHeader.NotebookCount);
            for I := 0 to FExHeader.NotebookCount - 1 do
            begin
              FNotebook[I].ID:= OldNotebook5[I].ID;
              FNotebook[I].Rail:= OldNotebook5[I].Rail;
              FNotebook[I].DisCoord:= OldNotebook5[I].DisCoord;
              FNotebook[I].Defect:= StringToHeaderStr(OldNotebook5[I].Defect);
              FNotebook[I].BlokNotText:= StringToHeaderBigStr(OldNotebook5[I].BlokNotText);
              FNotebook[I].DT:= OldNotebook5[I].DT;
              FNotebook[I].Zoom:= OldNotebook5[I].Zoom;
              FNotebook[I].Reduction:= OldNotebook5[I].Reduction;
              FNotebook[I].ShowChGate_:= OldNotebook5[I].ShowChGate_;
              FNotebook[I].ShowChSettings_:= OldNotebook5[I].ShowChSettings_;
              FNotebook[I].ShowChInfo_:= OldNotebook5[I].ShowChInfo_;
              FNotebook[I].ViewMode:= OldNotebook5[I].ViewMode;
              FNotebook[I].DrawRail:= OldNotebook5[I].DrawRail;
              FNotebook[I].AmplTh:= OldNotebook5[I].AmplTh;
              FNotebook[I].AmplDon:= OldNotebook5[I].AmplDon;
              FNotebook[I].ViewChannel:= OldNotebook5[I].ViewChannel;
              FNotebook[I].ViewLine:= OldNotebook5[I].ViewLine;
            end;

          end;
        6:
          begin

            SetLength(OldNotebook6, FExHeader.NotebookCount);
            try
              if FExHeader.NotebookOffset + FExHeader.NotebookCount * FExHeader.NotebookItemSize < FBody.Size then
              begin
                FBody.Position:= FExHeader.NotebookOffset;
                FBody.ReadBuffer(OldNotebook6[0], FExHeader.NotebookCount * FExHeader.NotebookItemSize);
              end
              else
              begin
                SetLength(OldNotebook6, 0);
                FModifyData:= True;
              end;
            except
              SetLength(OldNotebook6, 0);
              FModifyData:= True;
            end;

            SetLength(FNotebook, FExHeader.NotebookCount);
            for I := 0 to FExHeader.NotebookCount - 1 do
            begin
              FillChar(&FNotebook[I].ID, SizeOf(TFileNotebook_ver_7), 0);

              FNotebook[I].ID:= OldNotebook6[I].ID;
              FNotebook[I].Rail:= OldNotebook6[I].Rail;
              FNotebook[I].DisCoord:= OldNotebook6[I].DisCoord;
              FNotebook[I].Defect:= OldNotebook6[I].Defect;
              FNotebook[I].BlokNotText:= OldNotebook6[I].BlokNotText;
              FNotebook[I].DT:= OldNotebook6[I].DT;
              FNotebook[I].Zoom:= OldNotebook6[I].Zoom;
              FNotebook[I].Reduction:= OldNotebook6[I].Reduction;
              FNotebook[I].ShowChGate_:= OldNotebook6[I].ShowChGate_;
              FNotebook[I].ShowChSettings_:= OldNotebook6[I].ShowChSettings_;
              FNotebook[I].ShowChInfo_:= OldNotebook6[I].ShowChInfo_;
              FNotebook[I].ViewMode:= OldNotebook6[I].ViewMode;
              FNotebook[I].DrawRail:= OldNotebook6[I].DrawRail;
              FNotebook[I].AmplTh:= OldNotebook6[I].AmplTh;
              FNotebook[I].AmplDon:= OldNotebook6[I].AmplDon;
              FNotebook[I].ViewChannel:= OldNotebook6[I].ViewChannel;
              FNotebook[I].ViewLine:= OldNotebook6[I].ViewLine;
            end;

          end;
        7:
          begin
            SetLength(FNotebook, FExHeader.NotebookCount);
            try
              if FExHeader.NotebookOffset + FExHeader.NotebookCount * FExHeader.NotebookItemSize <= FBody.Size then
              begin
                FBody.Position:= FExHeader.NotebookOffset;
                FBody.ReadBuffer(FNotebook[0], FExHeader.NotebookCount * FExHeader.NotebookItemSize);
              end
              else
              begin
                SetLength(FNotebook, 0);
                FModifyData:= True;
              end;
            except
              SetLength(FNotebook, 0);
              FModifyData:= True;
            end;
          end;

        8 .. 99:
          begin
            FUnknownFileVersion:= True;
            Exit;
          end;
      end;
    end;

    { if FExHeader.DecodingCount <> 0 then
      if FExHeader.DecodingDataVer = 3 then
      begin
      SetLength(FDecoding, FExHeader.DecodingCount);
      try
      FBody.Position:= FExHeader.DecodingOffset;
      FBody.ReadBuffer(FDecoding[0], FExHeader.DecodingCount * FExHeader.DecodingItemSize);
      except
      //          FLog.Add(Language.GetCaption(0150015));
      SetLength(FDecoding, 0);
      FModifyData:= True;
      end;
      end
      else
      begin
      FUnknownFileVersion:= True;
      Exit;
      end;

      if FExHeader.RSPCount <> 0 then
      if FExHeader.RSPDataVer = 3 then
      begin
      SetLength(FRSPInfo, FExHeader.RSPCount);
      try
      FBody.Position:= FExHeader.RSPOffset;
      FBody.ReadBuffer(FRSPInfo[0], FExHeader.RSPCount * FExHeader.RSPItemSize);
      except
      //          FLog.Add(Language.GetCaption(0150016));
      SetLength(FRSPInfo, 0);
      FModifyData:= True;
      end;
      end
      else
      begin
      FUnknownFileVersion:= True;
      Exit;
      end; }
  end
  else
  begin
    NeedReAnalyze:= True;
    // FExHeader.Modify:= $FF;
  end;

  if FExHeader.EventCount = 0 then NeedReAnalyze:= True;
  FExHdrLoadOk:= ExHeadOk;

  {$IFNDEF GENERATE_LOG}
  {$IFNDEF GENERATE_GLOBAL_LOG}
  {$IFNDEF SPECIAL_LOG_N1}
  {$IFNDEF ALWAYs_Analyze}
  {$IFNDEF SPECIAL_LOG_N2}
  if (not ExHeadOk) or NeedReAnalyze or ((isA31 or isFilus_X17DW or isA15New) and (FExHeader.Reserv2[0] = $FF) or (FExHeader.Reserv2[0] = $01) or (FExHeader.Reserv2[0] = $02) or (FExHeader.Reserv2[0] = $03) or (FExHeader.Reserv2[0] = $04) or (FExHeader.Reserv2[0] = $05)) then
  {$ENDIF}
  {$ENDIF}
  {$ENDIF}
  {$ENDIF}
  {$ENDIF}
  begin
    try
      AnalyzeFileBody;
      SaveExData(FExHdrLoadOk);
      FModifyData:= False;
    except
      Error:= Error + [eUnknownError];
      Exit;
    end;
  end;

  // ��������������� ��� ������ ��������� ����� �� ������ ������� � ���� ������ - FBodyBadZones
  for I:= 0 to EventCount - 1 do
  begin
    if Event[I].ID = EID_StartBadZone then
    begin
      J:= Length(FBodyBadZones);
      SetLength(FBodyBadZones, J + 1);
      FBodyBadZones[J].From:= Event[I].Offset;
      FBodyBadZones[J].To_:= - 1;
    end;

    if (Length(FBodyBadZones) <> 0) and
       (Event[I].ID = EID_EndBadZone) and
       (FBodyBadZones[High(FBodyBadZones)].To_ = - 1) then
    begin
      FBodyBadZones[High(FBodyBadZones)].To_:= Event[I].Offset;
    end;
  end;
end;

procedure TAviconDataSource.CutExHeader;
var
  F: file;

  fs: TFileStream;
  fs1: TFileStream;

begin

  fs := TFileStream.Create(FFileName, fmOpenRead);
  fs1 := TFileStream.Create(ExtractFilePath(FFileName) + StringReplace(ExtractFileName(FileName),ExtractFileExt(FileName),'',[]) + '_cut_' + ExtractFileExt(FileName), fmCreate);
  Fs1.CopyFrom(fs, FHead.TableLink);
  FS1.Free;
  FS.Free;
(*

  try
    AssignFile(F, FFileName); // �������� ����� ��� ������
    Reset(F, 1);
  except
    // FLog.Add(Language.GetCaption(0150019));
    // Show-Message(Language.GetCaption(0150006));
    Exit;
  end;

  // ��������� ������� ������������ ���������
  try
    Seek(F, FHead.TableLink);
    Truncate(F);
    System.CloseFile(F);
  except
    // FLog.Add(Language.GetCaption(0150020));
    Exit;
  end; *)
end;

procedure TAviconDataSource.SaveExData(CutOldExHeader: Boolean);
var
  I: Integer;
  F: file;
  FileDate: TDateTime;
  Evt: TFileEvent;

begin
  if not FFullHeader then Exit;
  if FReadOnlyFlag then Exit;
  if FLoadFromStream then Exit;

  if not SkipTestDateTime then
  begin
    FileDate:= FileDateToDateTime(FileAge(FFileName));
    if (FSaveFileDate <> FileDate) or (FSaveFileSize <> FBody.Size) then
    begin
      //      Show-Message(Language.GetCaption(0150009));
      Exit;
    end;
  end;


  {$IFDEF MY_DATA_STREAM}
  if CutOldExHeader and (FHead.TableLink <> $FFFFFFFF) then   // ��������� ������� ������������ ���������
  begin
    try
      FBody.Size:= FHead.TableLink
    except
    //      FLog.Add(Language.GetCaption(0150020));
      Exit;
    end;
  end;

  try
//    Seek(F, FileSize(F));
    FHead.TableLink:= FBody.Size;         // ����������� ��������� ������ ������������ ���������
    FDataSize:= FBody.Size;
  except
    //    FLog.Add(Language.GetCaption(0150021));
    Exit;
  end;

  try
    //Seek(F, 0);
    FBody.Position:= 0;
    FBody.WriteBuffer(FHead, Sizeof(TFileHeader)); // ������ ������ �������� ��������� ������������ ���������
  except
    //    FLog.Add(Language.GetCaption(0150022));
    Exit;
  end;

//  FExHeader.EndKM:= 0 {FLastStolb.Km[1]} ;
//  FExHeader.EndPK:= 0 {FLastStolb.Pk[1]} ;
//  FExHeader.EndMM:= 0 {(FCurSaveSysCrd - FLastStolbSysCrd) * FHead.ScanStep div 100};
//  FExHeader.EndSysCoord:= 0 {FCurSaveSysCrd};
//  FExHeader.EndDisCoord:= 0 {FCurSaveDisCrd};
  FillChar(FExHeader.FFConst[0], Length(FExHeader.FFConst), $FF);
  FillChar(FExHeader.Reserv1[0], Length(FExHeader.Reserv1), $FF);

  if not isA31 then FillChar(FExHeader.Reserv2[0], Length(FExHeader.Reserv2), $FF)
               else FillChar(FExHeader.Reserv2[2], Length(FExHeader.Reserv2) - 2, $FF);

  FExHeader.DataVer:= 7;
  FExHeader.DataSize:= SizeOf(TExHeader);

//  Seek(F, FHead.TableLink); // ������ ������ ������������ ���������
//  BlockWrite(F, FExHeader, FExHeader.DataSize);
  FBody.Position:= FHead.TableLink;
  FBody.WriteBuffer(FExHeader, FExHeader.DataSize); // ������ ������ �������� ��������� ������������ ���������

  FExHeader.EventDataVer:= 5; // ������ ������ ������� �����
  FExHeader.EventItemSize:= 13;
  FExHeader.EventCount:= Length(FEvents);
//  FExHeader.EventOffset:= FileSize(F);
  if FExHeader.EventCount <> 0 then
  begin
//    FExHeader.EventOffset:= FilePos(F);
    FExHeader.EventOffset:= FBody.Position;

    for I:= 0 to High(FEvents) do
    begin
      // Evt.ID:= FEvents[I].ID;
      // Evt.OffSet:= FEvents[I].OffSet;
      // Evt.SysCoord:= FEvents[I].SysCoord;
      // Evt.DisCoord:= FEvents[I].DisCoord;
      Evt:= FEvents[I];
//      BlockWrite(F, Evt, FExHeader.EventItemSize);
      FBody.WriteBuffer(Evt, FExHeader.EventItemSize);
    end;
  end;

  FExHeader.NotebookDataVer:= 7; // ������ ������ ������� �������������
//  FExHeader.NotebookOffset:= FileSize(F);
  FExHeader.NotebookItemSize:= SizeOf(TFileNotebook);
  FExHeader.NotebookCount:= Length(FNotebook);
  if FExHeader.NotebookCount <> 0 then
    try
  //    FExHeader.NotebookOffset:= FilePos(F);
      FExHeader.NotebookOffset:= FBody.Position;
//      BlockWrite(F, FNotebook[0], FExHeader.NotebookCount * SizeOf(TFileNotebook));
      FBody.WriteBuffer(FNotebook[0], FExHeader.NotebookCount * SizeOf(TFileNotebook));
    except
    //      FLog.Add(Language.GetCaption(0150025));
      Exit;
    end;

                                                  // ������ ������ ������������ ���������
//  Seek(F, FHead.TableLink);
//  BlockWrite(F, FExHeader, FExHeader.DataSize);
//  Close(F);
  FBody.Position:= FHead.TableLink;
  FBody.WriteBuffer(FExHeader, FExHeader.DataSize); // ������ ������ �������� ��������� ������������ ���������

  {$ENDIF}

  {$IFNDEF MY_DATA_STREAM}
  try
    AssignFile(F, FFileName);                // �������� ����� ��� ������
    Reset(F, 1);
  except
    //    FLog.Add(Language.GetCaption(0150019));
    //    Show-Message(Language.GetCaption(0150006));
    Exit;
  end;

  if CutOldExHeader and (FHead.TableLink <> $FFFFFFFF) then   // ��������� ������� ������������ ���������
  begin
    try
      Seek(F, FHead.TableLink);
      Truncate(F);
    except
    //      FLog.Add(Language.GetCaption(0150020));
      Exit;
    end;
  end;

  try
    Seek(F, FileSize(F));
    FHead.TableLink:= FileSize(F);         // ����������� ��������� ������ ������������ ���������
    FDataSize:= FileSize(F);
  except
    //    FLog.Add(Language.GetCaption(0150021));
    Exit;
  end;

  try
    Seek(F, 0);
    BlockWrite(F, FHead, Sizeof(TFileHeader)); // ������ ������ �������� ��������� ������������ ���������
  except
    //    FLog.Add(Language.GetCaption(0150022));
    Exit;
  end;

//  FExHeader.EndKM:= 0 {FLastStolb.Km[1]} ;
//  FExHeader.EndPK:= 0 {FLastStolb.Pk[1]} ;
//  FExHeader.EndMM:= 0 {(FCurSaveSysCrd - FLastStolbSysCrd) * FHead.ScanStep div 100};
//  FExHeader.EndSysCoord:= 0 {FCurSaveSysCrd};
//  FExHeader.EndDisCoord:= 0 {FCurSaveDisCrd};
  FillChar(FExHeader.FFConst[0], Length(FExHeader.FFConst), $FF);
  FillChar(FExHeader.Reserv1[0], Length(FExHeader.Reserv1), $FF);

  if not (isA31 or isFilus_X17DW or isA15New) then FillChar(FExHeader.Reserv2[0], Length(FExHeader.Reserv2), $FF)
               else FillChar(FExHeader.Reserv2[2], Length(FExHeader.Reserv2) - 2, $FF);

  FExHeader.DataVer:= 7;
  FExHeader.DataSize:= SizeOf(TExHeader);


  Seek(F, FHead.TableLink); // ������ ������ ������������ ���������
  BlockWrite(F, FExHeader, FExHeader.DataSize);

  FExHeader.EventDataVer:= 5; // ������ ������ ������� �����
  FExHeader.EventItemSize:= 13;
  FExHeader.EventCount:= Length(FEvents);
//  FExHeader.EventOffset:= FileSize(F);
  if FExHeader.EventCount <> 0 then
  begin
    FExHeader.EventOffset:= FilePos(F);

    for I:= 0 to High(FEvents) do
    begin
      // Evt.ID:= FEvents[I].ID;
      // Evt.OffSet:= FEvents[I].OffSet;
      // Evt.SysCoord:= FEvents[I].SysCoord;
      // Evt.DisCoord:= FEvents[I].DisCoord;
      Evt:= FEvents[I];
      BlockWrite(F, Evt, FExHeader.EventItemSize);
    end;
  end;

  FExHeader.NotebookDataVer:= 7; // ������ ������ ������� �������������
//  FExHeader.NotebookOffset:= FileSize(F);
  FExHeader.NotebookItemSize:= SizeOf(TFileNotebook);
  FExHeader.NotebookCount:= Length(FNotebook);
  if FExHeader.NotebookCount <> 0 then
    try
      FExHeader.NotebookOffset:= FilePos(F);
      BlockWrite(F, FNotebook[0], FExHeader.NotebookCount * SizeOf(TFileNotebook));
    except
    //      FLog.Add(Language.GetCaption(0150025));
      Exit;
    end;

                                                  // ������ ������ ������������ ���������
  Seek(F, FHead.TableLink);
  BlockWrite(F, FExHeader, FExHeader.DataSize);
  Close(F);
  {$ENDIF}




  (*
    FillChar(FExHeader.FFConst[0], Length(FExHeader.FFConst), $FF);
    FillChar(FExHeader.Reserv1[1], Length(FExHeader.Reserv1), $FF);
    FillChar(FExHeader.Reserv2[1], Length(FExHeader.Reserv2), $FF);

    FExHeader.DataVer:= 6;
    FExHeader.DataSize:= SizeOf(TExHeader);
    FExHeader.Flag:= 0;

    try
    Seek(F, FHead.TableLink);                   // ������ ������������ ���������
    BlockWrite(F, FExHeader, FExHeader.DataSize);
    except
    //    FLog.Add(Language.GetCaption(0150028));
    Exit;
    end;

    FExHeader.ErrorDataVer:= 3;                   // ������ ������ ������ ��������� �����
    FExHeader.ErrorItemSize:= SizeOf(TFileError);
    FExHeader.ErrorCount:= Length(FErrors);
    FExHeader.ErrorOffset:= 0;
    if FExHeader.ErrorCount <> 0 then
    try
    FExHeader.ErrorOffset:= FilePos(F);
    BlockWrite(F, FErrors[0], FExHeader.ErrorCount * FExHeader.ErrorItemSize);
    except
    //      FLog.Add(Language.GetCaption(0150023));
    Exit;
    end;

    FExHeader.EventDataVer:= 3;                   // ������ ������ ������� �����
    FExHeader.EventItemSize:= SizeOf(TFileEvent);
    FExHeader.EventCount:= Length(FEvents);
    FExHeader.EventOffset:= 0;
    if FExHeader.EventCount <> 0 then
    try
    FExHeader.EventOffset:= FilePos(F);
    BlockWrite(F, FEvents[0], FExHeader.EventCount * FExHeader.EventItemSize);
    except
    //      FLog.Add(Language.GetCaption(0150024));
    Exit;
    end;

    FExHeader.NotebookDataVer:= 4;                // ������ ������ ������� �������������
    FExHeader.NotebookOffset:= 0;
    FExHeader.NotebookItemSize:= SizeOf(TFileNotebook_Ver4);
    FExHeader.NotebookCount:= Length(FNotebook);
    if FExHeader.NotebookCount <> 0 then
    try
    FExHeader.NotebookOffset:= FilePos(F);
    BlockWrite(F, FNotebook[0], FExHeader.NotebookCount * SizeOf(TFileNotebook_Ver4));
    except
    //      FLog.Add(Language.GetCaption(0150025));
    Exit;
    end;

    FExHeader.DecodingDataVer:= 3;                 // ������ ������ ������� � ������ �����������
    FExHeader.DecodingOffset:= 0;
    FExHeader.DecodingItemSize:= SizeOf(TFileDecoding);
    FExHeader.DecodingCount:= Length(FDecoding);
    if FExHeader.DecodingCount <> 0 then
    try
    FExHeader.DecodingOffset:= FilePos(F);
    BlockWrite(F, FDecoding[0], FExHeader.DecodingCount * SizeOf(TFileDecoding));
    except
    //      FLog.Add(Language.GetCaption(0150026));
    Exit;
    end;

    FExHeader.RSPDataVer:= 3;                 // ������ ������ ������ ���
    FExHeader.RSPOffset:= 0;
    FExHeader.RSPItemSize:= SizeOf(TRSPInfo);
    FExHeader.RSPCount:= Length(FRSPInfo);
    if FExHeader.RSPCount <> 0 then
    try
    FExHeader.RSPOffset:= FilePos(F);
    BlockWrite(F, FRSPInfo[0], FExHeader.RSPCount * SizeOf(TRSPInfo));
    except
    //      FLog.Add(Language.GetCaption(0150027));
    Exit;
    end;

    FExHeader.UCSDataVer:= 3;
    FExHeader.UCSItemSize:= 0;
    FExHeader.UCSOffset:= 0;
    FExHeader.UCSCount:= 0;

    FExHeader.LabelEditDataVer:= 3;
    FExHeader.LabelEditItemSize:= 0;
    FExHeader.LabelEditOffset:= 0;
    FExHeader.LabelEditCount:= 0;

    try                                             // ������ ������ ������������ ���������
    Seek(F, FHead.TableLink);
    BlockWrite(F, FExHeader, FExHeader.DataSize);
    Close(F);
    except
    //    FLog.Add(Language.GetCaption(0150028));
    Exit;
    end;

    FBody.Clear;
    FBody.LoadFromFile(FFileName);
*)
    FSaveFileDate:= FileDateToDateTime(FileAge(FFileName));
    FSaveFileSize:= FBody.Size;

    FExHdrLoadOk:= True;
end;

// ------< ������ � ��� ��������� - ���������� ������ 1 - �� ������ >-----------
(*
procedure TAviconDataSource.DataToEcho;
begin
  if FSrcDat[0] shr 3 and 7 = 1 then
  begin
    with FDestDat[TRail(FSrcDat[0] shr 6 and 1)] do
    begin
      // Count:= 0;
      case FSrcDat[0] and $07 of
   //     0: ZerroFlag:= True;
        1:
          begin
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[1];
            Ampl[Count]:= FSrcDat[2] shr 4 and $0F;
          end;
        2:
          begin
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[1];
            Ampl[Count]:= FSrcDat[3] shr 4 and $0F;
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[2];
            Ampl[Count]:= FSrcDat[3] and $0F;
          end;
        3:
          begin
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[1];
            Ampl[Count]:= FSrcDat[4] shr 4 and $0F;
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[2];
            Ampl[Count]:= FSrcDat[4] and $0F;
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[3];
            Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
          end;
        4:
          begin
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[1];
            Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[2];
            Ampl[Count]:= FSrcDat[5] and $0F;
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[3];
            Ampl[Count]:= FSrcDat[6] shr 4 and $0F;
            if Count < 16 then
              Inc(Count);
            Delay[Count]:= FSrcDat[4];
            Ampl[Count]:= FSrcDat[6] and $0F;
          end;
      end;
    end;
  end;
end;
 *)
// ------< ������ ��������� ����� � ����������� ��������� �������� >-----------

procedure TAviconDataSource.CloseFile;
begin
  SaveExData(FExHdrLoadOk);
end;

//var
//  Log: TStringList = nil;

function TAviconDataSource.SkipEvent(ID: Byte): Boolean; // ������� �������
var
  FWID: Word;
  FDWID: DWord;
  FID: Byte;
  Ch: Byte;
  I, P: Integer;
  Data: array [0..50] of Byte;
  ErrorFlag: Boolean;
  LoadByte: array [1 .. 331] of Byte;
  DataSize: DWord;
  DataType: Byte;
  Temperature: Single;
  tmpHeader: THSHead;
  tmpHeader_OLD: THSHead_OLD;
  tmpHSHead: THeadScanListItem;
  tmpItem: THSItem;
  DataType_1_1: Boolean;
  DataType_1_2: Boolean;
  DataType_2_1: Boolean;
  DataType_2_2: Boolean;
//  SaveOffset: Integer;
  AScanHead: edAScanFrame;
  ExHeader__: TExHeader;
  LoadID: Byte;
  Time: edTime;
  LoadLongInt: Longint;


begin
  FSkipEventFlag_HandScan:= False;
  Result:= True;
  if ID and 128 = 0 then
  begin
    Ch:= (ID shr 2) and 15;
    FBody.Position:= FBody.Position + EchoLen[ID and $03];
    if FExtendedChannels and (Ch > 14) then FBody.Position:= FBody.Position + 1;
  end
  else
  begin
//    SaveOffset:= FBody.Position - 1;
    if not FIsGoodID[ID] then Result:= False;
    case ID of
{0x82}      EID_HandScan:  // ������
                     begin
                       P:= FBody.Position;
                       FBody.ReadBuffer(tmpHeader, SizeOf(THSHead));

                    (* if not ((tmpHeader.DataSize > 0) and (tmpHeader.DataSize <= 100000) and
                               (tmpHeader.DelayMultiply in [1, 3]) and
                               (Integer(tmpHeader.Rail) in [0, 1]) and
                               (tmpHeader.ScanSurface in [0..9]) and
                               (tmpHeader.HandChNum in [0..7])) then
                       begin
                         FBody.Position:= SaveOffset + 1;
                       end
                       else *)
                       begin
                         FBody.Position:= FBody.Position + tmpHeader.DataSize;
                         FSkipEventFlag_HandScan:= True;
                      (*
                         FBody.Position:= FBody.Position + tmpHeader.DataSize - 3;
                         FBody.ReadBuffer(tmpItem, 3);
                         DataType_1_1:= (tmpItem.Sample = $FFFF) and (tmpItem.Count = 0);

                         FBody.Position:= P;
                         FBody.ReadBuffer(tmpHeader, SizeOf(THSHead));
                         FBody.Position:= FBody.Position + tmpHeader.DataSize - 5;
                         FBody.ReadBuffer(tmpItem, 5);
                         DataType_1_2:= (tmpItem.Sample = $FFFF) and (tmpItem.Count = 1);

                         FBody.Position:= P;
                         FBody.ReadBuffer(tmpHeader_OLD, SizeOf(THSHead_OLD));
                         FBody.Position:= FBody.Position + tmpHeader.DataSize - 3;
                         FBody.ReadBuffer(tmpItem, 3);
                         DataType_2_1:= (tmpItem.Sample = $FFFF) and (tmpItem.Count = 0);

                         FBody.Position:= P;
                         FBody.ReadBuffer(tmpHeader_OLD, SizeOf(THSHead_OLD));
                         FBody.Position:= FBody.Position + tmpHeader.DataSize - 5;
                         FBody.ReadBuffer(tmpItem, 5);
                         DataType_2_2:= (tmpItem.Sample = $FFFF) and (tmpItem.Count = 1);

                         FBody.Position:= P;
                         if DataType_1_1 or DataType_1_2 then
                         begin
                           FBody.ReadBuffer(tmpHeader, SizeOf(THSHead));
                           FBody.Position:= FBody.Position + tmpHeader.DataSize;
                         end
                         else
                   //      if DataType_2_1 or DataType_2_2 then
                         begin
                           FBody.ReadBuffer(tmpHeader_OLD, SizeOf(THSHead_OLD));
                           FBody.Position:= FBody.Position + tmpHeader_OLD.DataSize;
                         end;        *)
                       end;
                     end;
{0xBE}EID_RailHeadScaner: begin   // ������ ����������� ������� ������� ������
                             FBody.ReadBuffer(tmpHSHead.Size, 12);
                         (*  if not ((tmpHSHead.Size > 0) and (tmpHSHead.Size <= 100000) and
                                     (tmpHSHead.WorkSide in [1, 2]) and
                                     (Integer(tmpHSHead.Rail) in [0, 1, 100, 101]) and
                                     (tmpHSHead.OperatingTime < 2000) and
                                     (tmpHSHead.ThresholdUnit in [1, 2])) then
                             begin
                               FBody.Position:= SaveOffset + 1;
                             end
                             else *) FBody.Position:= FBody.Position + tmpHSHead.Size - 8;
                          end;
{0xC6} EID_AScanFrame: begin   // ���� �-���������

//                         FBody.ReadBuffer(AScanHead, 15);
                         (*
                         if not ((AScanHead.DataSize > 0) and (tmpHSHead.Size <= 500000) and
                                 (AScanHead.DataType = 1) and
                                 (Ord(AScanHead.Data.Rail) in [0, 1]) and
                                 (AScanHead.Data.Channel in [0..15, 255])) then
                         begin
                           FBody.Position:= SaveOffset + 1;
                         end
                         else *)
//                         FBody.Position:= FBody.Position + AScanHead.DataSize - 11;
                         FBody.ReadBuffer(AScanHead, 5);
                         FBody.Position:= FBody.Position + AScanHead.DataSize;
                       end;

{0x90}             EID_Ku, // ��������� �������� ����������������
{0x91}            EID_Att, // ��������� �����������
{0x92}            EID_TVG, // ��������� ���
{0x93}          EID_StStr, // ��������� ��������� ������ ������
{0x94}         EID_EndStr, // ��������� ��������� ����� ������
{0x95}         EID_HeadPh: // ������ ���������� ���������
                           begin
                             FBody.ReadBuffer(Data[0], 3);         // ��������� ������
                             // if not ((Data[0] in [0, 1]) and (Data[1] in [0..15, 255])) then FBody.Position:= SaveOffset + 1;
                           end;
{0x9C}     EID_PrismDelay: begin
                             FBody.ReadBuffer(Data[0], 4);         // ��������� ������
                             // if not ((Data[0] in [0, 1]) and (Data[1] in [0..15, 255])) then FBody.Position:= SaveOffset + 1;
                           end;

{0x96}           EID_Mode: begin
                            FBody.ReadBuffer(Data[0], 7); (*
                            if not ((Data[1] = 0) and (Data[0] in [0..12]) and
                                    ((Boolean(Data[2]) = False) or ((Boolean(Data[2]) = True) and (Data[3] in [0, 1]) and (Data[4] in [0..15, 255])))) then
                            begin
                              FBody.Position:= SaveOffset + 1;
                            end else *)
                            FSkipEventFlag_HandScan:= (Data[0] in [1, 3]) and (Data[1] = 0);
                           end;
{0x9B}    EID_SetRailType: FBody.Position:= FBody.Position + 0; // ��������� �� ��� ������
{0xA0}          EID_Stolb: FBody.Position:= FBody.Position + 144;
                        (*   begin
                             FBody.ReadBuffer(LoadByte[1], 144);
                             for I := 17 to 144 do
                               if LoadByte[I] <> 0 then
                               begin
                                 FBody.Position:= SaveOffset + 1;
                                 Break;
                               end;
                           end; *)
{0xA7}  EID_StolbChainage:  FBody.Position:= FBody.Position + 136; // ������� ����������
                          (*  begin
                              FBody.ReadBuffer(LoadByte[1], 136);
                              for I := 9 to 136 do
                                if LoadByte[I] <> 0 then
                                begin
                                  FBody.Position:= SaveOffset + 1;
                                  Break;
                                end;
                            end; *)

{0xAC}    EID_AutomaticSearchRes: // FBody.Position:= FBody.Position + 9;  // ��������� ������� �������, ���������� ��� �������������� ������
                          begin
                            FBody.ReadBuffer(LoadByte[1], 9);
                          {  if not ((LoadByte[1] in [0, 1]) and (LoadByte[2] in [0..15])) then
                            begin
                              FBody.Position:= SaveOffset + 1;
                            end; }
                          end;

{0xAD}        EID_TestRecordFile: begin                                  // ���� ������ ������������ ������
                                    FBody.ReadBuffer(FDWID, 4); {
                                    if (FDWID <= 5310) or (FDWID > FBody.Size) or (FBody.Position <> 5315) then
                                    begin
                                      FBody.Position:= SaveOffset + 1;
                                    end else }
                                    FBody.Position:= FBody.Position + FDWID;
                                  end;

{0xAF} EID_QualityCalibrationRec: // FBody.Position:= FBody.Position + 15;  // �������� ��������� ������� ��������
                          begin
                            FBody.ReadBuffer(LoadByte[1], 15);
                         {   if not ((LoadByte[1] in [0, 1]) and (LoadByte[2] in [0..5]) and (LoadByte[3] <= 12)) then
                            begin
                              FBody.Position:= SaveOffset + 1;
                            end;    }
                          end;

{0xB8}        EID_Temperature: (*{$IFDEF Temperature_is_Byte}
                               FBody.Position:= FBody.Position + 2; // �����������
                               {$ENDIF}
                               {$IFNDEF Temperature_is_Byte}
                               FBody.Position:= FBody.Position + 5; // �����������
                               {$ENDIF}*)
                              begin
                                FBody.ReadBuffer(LoadByte[1], 1);
                                FBody.ReadBuffer(Temperature, 4);
                             {   if not ((LoadByte[1] in [0..3]) and (Temperature > - 100) and (Temperature < 80)) then
                                  FBody.Position:= SaveOffset + 1;   }
                              end;

{0xBD}          EID_SmallDate: begin   // ����� ������������� �������
                                  FBody.ReadBuffer(FID, 1);
                                  FBody.Position:= FBody.Position + FID + 1;
                               end;
                        (*  begin
                            DataSize:= 0;
                            FBody.ReadBuffer(DataSize, 1);
                            FBody.ReadBuffer(DataType, 1);
                            if not (DataType in [1..2]) then
                            begin
                              FBody.Position:= SaveOffset + 1;
                            end else FBody.Position:= FBody.Position + DataSize;
                          end; *)

{0xC2}          EID_Media: begin
                             FBody.ReadBuffer(FID, 1); // ���
                             if FID in [1, 2, 3] then
                             begin
                               FBody.ReadBuffer(FDWID, 4); // ������
                               FBody.Position:= FBody.Position + FDWID;   // �����������
                             end;

                           {  begin
                               FBody.ReadBuffer(DataType, 1);
                               FBody.ReadBuffer(DataSize, 4);
                              if not (DataType in [1..3]) or (DataSize > 10000000) or (DataSize < 0) then
                               begin
                                 FBody.Position:= SaveOffset + 1;
                               end else  FBody.Position:= FBody.Position + DataSize;
                             end;  }

                           end;
{0xC5}       EID_SensFail: // FBody.Position:= FBody.Position + 6;   // ���������� �������� ���������������� �� ������������ ��������
                          begin
                            FBody.ReadBuffer(LoadByte[1], 6);
                          {  if not ((LoadByte[1] in [0, 1]) and (LoadByte[2] in [0..15, 255])) then
                            begin
                              FBody.Position:= SaveOffset + 1;
                            end;    }
                          end;

{0xC7}     EID_MediumDate:(* begin   // ������� ������������� �������
                              FBody.ReadBuffer(FWID, 2);
                              FBody.Position:= FBody.Position + FWID + 1;
                           end; *)

                          begin
                            DataSize:= 0;
                            FBody.ReadBuffer(DataSize, 2);
                            FBody.ReadBuffer(DataType, 1);

                         {  if not (((DataType = 1) and (DataSize <= 5310 + 25)) or
                                    ((DataType = 2) and (DataSize <= SizeOf(edStartSwitchEx) + 10)) or
                                    ((DataType = 3) and (DataSize <= SizeOf(edEndSwitchEx) + 10))) then
                            begin
                              FBody.Position:= SaveOffset + 1;
                            end else } FBody.Position:= FBody.Position + DataSize;
                          end;

{0xC8}        EID_BigDate: begin   // ������� ������������� �������
                            //  FBody.ReadBuffer(FDWID, 4);
                            //  FBody.Position:= FBody.Position + FDWID + 1;
                            // FBody.Position:= SaveOffset + 1; // ���� ��� ������ � ���� �������� !!!
                          end;

{0xA1}         EID_Switch: // ����� ����������� ��������
                           begin
                             FBody.ReadBuffer(FID, 1);
                             FBody.Position:= FBody.Position + FID * SizeOf(Char);
                           end;
{0xA2}       EID_DefLabel: // ����� ������� + �����
                           begin
                             FBody.ReadBuffer(FID, 1);
                             FBody.Position:= FBody.Position + 1 + FID * SizeOf(Char);
                           end;
{0xA3}      EID_TextLabel: // ��������� �������
                           begin
                             FBody.ReadBuffer(FID, 1);
                             FBody.Position:= FBody.Position + FID * SizeOf(Char);
                           end;
{0xA4}     EID_StBoltStyk: FBody.Position:= FBody.Position + 0;   // ������� ������ ��
{0xA5}    EID_EndBoltStyk: FBody.Position:= FBody.Position + 0;   // ���������� ������ ��
{0xA6}           EID_Time: FBody.Position:= FBody.Position + 2;   // ������� �������
{0xA8}  EID_ZerroProbMode: FBody.Position:= FBody.Position + 1;   // ����� ������ �������� 0 ����
{0xA9}      EID_LongLabel: FBody.Position:= FBody.Position + 24;  // ����������� �������
{0xAA}            EID_SpeedState: FBody.Position:= FBody.Position + 5;   // �������� � ���������� �������� ��������
{0xAB}    EID_ChangeOperatorName: FBody.Position:= FBody.Position + 129; // ����� ��������� (��� ���������)

{0xAE}   EID_OperatorRemindLabel: begin
                                    FBody.ReadBuffer(FID, 1);
                                    FBody.Position:= FBody.Position + 10 + FID * SizeOf(Char); // ������� ����, ������� ���������� � ������ ��� ����������� ��������� (��������� ������ � ������)
                                  end;

{0xB0}        EID_Sensor1: FBody.Position:= FBody.Position + 2;   // ������ ������� �� � �������

{0xB1}       EID_AirBrush: {if (not isA31) and (not isA15New) and (not isFilus_X17DW) then} FBody.Position:= FBody.Position + 182; // ��������������
                                                                                    // else FBody.Position:= SaveOffset + 1;
{0xB2}   EID_PaintMarkRes: {if (not isA31) and (not isA15New) and (not isFilus_X17DW) then} FBody.Position:= FBody.Position + 8;   // ��������� � ���������� ������� �� ������-�������
                                                                                    // else FBody.Position:= SaveOffset + 1;
{0xB3}EID_AirBrushTempOff: {if (not isA31) and (not isA15New) and (not isFilus_X17DW) then} FBody.Position:= FBody.Position + 5;   // ��������� ���������� ���������������
                                                                                    // else FBody.Position:= SaveOffset + 1;
{0xB4}      EID_AirBrush2: {if (not isA31) and (not isA15New) and (not isFilus_X17DW) then} FBody.Position:= FBody.Position + 246; // �������������� � ���������� �����������
                                                                                    // else FBody.Position:= SaveOffset + 1;

{0xB5}   EID_AlarmTempOff: FBody.Position:= FBody.Position + 5;   // ��������� ���������� ��� �� ���� �������
{0xB6} EID_StartSwitchShunter: FBody.Position:= FBody.Position + 0; // ������ ���� ����������� ��������
{0xB7}   EID_EndSwitchShunter: FBody.Position:= FBody.Position + 0;	 // ����� ���� ����������� ��������
{0xB9}          EID_DebugData: FBody.Position:= FBody.Position + 128;  // ���������� ����������
{0xBA}  EID_PaintSystemParams: {if (not isA31) and (not isA15New) and (not isFilus_X17DW) then} FBody.Position:= FBody.Position + 2048; // ��������� ������������
                                                                                       //  else FBody.Position:= SaveOffset + 1;

{0xBB}        EID_UMUPaintJob: {if (not isA31) and (not isA15New) and (not isFilus_X17DW) then} FBody.Position:= FBody.Position + 9;    // ������� ��� �� �������������
                                                                                       //  else FBody.Position:= SaveOffset + 1;
{0xBC}         EID_ACData: FBody.Position:= FBody.Position + 1; // ������ ������������� ��������
{0xBF}  EID_SensAllowedRanges: begin   // ������� ����������� ���������� ��
                                  FBody.ReadBuffer(FID, 1);
                                  FBody.Position:= FBody.Position + FID * 3;
                                (*
                                  ErrorFlag:= False;
                                  FBody.ReadBuffer(Data[0], 1);
                                  if Data[0] <= 17 then
                                  begin
                                    FBody.ReadBuffer(Data[1], Data[0] * 3);
                                    for I := 1 to Data[0] do
                                      if not (Data[I * 3 - 2] in [0..15, 255]) then
                                      begin
                                        ErrorFlag:= True;
                                        Break;
                                      end;
                                  end else ErrorFlag:= True; *)
                                //  if ErrorFlag then FBody.Position:= SaveOffset + 1;
                               end;
{0xC0}       EID_GPSCoord: FBody.Position:= FBody.Position + 8;   // �������������� ����������
{0xC1}       EID_GPSState: FBody.Position:= FBody.Position + 9;   // ��������� ��������� GPS
{0xC3}      EID_GPSCoord2: FBody.Position:= FBody.Position + 12;  // �������������� ���������� �� ���������
{0xC4}     EID_NORDCO_Rec: FBody.Position:= FBody.Position + SizeOf(edNORDCO_Rec); // ������ NORDCO

{0xF0}       EID_CheckSum: begin   // ����������� �����
                              FBody.Position:= FBody.Position + 4;
                           end;

//       EID_CheckSum: FBody.Position:= FBody.Position + 5; // ����� � ����������� �����
{0xF2}      EID_SysCrd_NS: FBody.Position:= FBody.Position + 1;
{0xF3}      EID_SysCrd_NF: FBody.Position:= FBody.Position + 4;
                          (* begin
                             FBody.ReadBuffer(LoadLongInt, 4);
                             if LoadLongInt > FExHeader.EndSysCoord then FBody.Position:= SaveOffset + 1;
                           end; *)
{0xF1}      EID_SysCrd_SS: FBody.Position:= FBody.Position + 2;   // ��������� ���������� ��������
{0xF4}      EID_SysCrd_SF: FBody.Position:= FBody.Position + 5;   // ������ ��������� ���������� � �������� �������
{0xF9}      EID_SysCrd_FS: FBody.Position:= FBody.Position + 5;   // ��������� ���������� ��������
{0xFC}      EID_SysCrd_FF: FBody.Position:= FBody.Position + 8;   // ������ ��������� ���������� � ������ �������
{0xF5}   EID_SysCrd_UMUA_Left_NF: FBody.Position:= FBody.Position + 4; // ������ ��������� ���������� ��� ������ ��� A, ����� �������
{0xF6}   EID_SysCrd_UMUB_Left_NF: FBody.Position:= FBody.Position + 4; // ������ ��������� ���������� ��� ������ ��� �, ����� �������
{0xF7}  EID_SysCrd_UMUA_Right_NF: FBody.Position:= FBody.Position + 4; // ������ ��������� ���������� ��� ������ ��� A, ������ �������
{0xF8}  EID_SysCrd_UMUB_Right_NF: FBody.Position:= FBody.Position + 4; // ������ ��������� ���������� ��� ������ ��� �, ������ �������
        EID_EndFile: // FBody.Position:= FBody.Position + 13;  // ����� �����
                     begin
                       ErrorFlag:= False;
                       FBody.ReadBuffer(LoadByte, 13);
                       for i := 1 to 13 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                       if not ErrorFlag then
                         if FBody.Position <> FBody.Size then
                         begin
                           if FBody.Size - FBody.Position >= SizeOf(TExHeader) then
                           begin
                             FBody.ReadBuffer(LoadByte, 170 + 32 + 128);
                             for i := 1 to 14 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                             if not ErrorFlag then
                               for i := 107 to 170 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                             if not ErrorFlag then
                               for i := 205 + 2 to 330 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                             ErrorFlag:= ErrorFlag or not (LoadByte[19] in [0..8]) or (LoadByte[20] + LoadByte[21] + LoadByte[22] <> 0);
                           end;
                         end;
                     //  if ErrorFlag then FBody.Position:= SaveOffset + 1
                     //               else ; // ��� �� �� ��� ���������!
                     end;
      else Result:= False;
    end;
    {$IFDEF GENERATE_LOG}
//    FLog.Add(Format('Pos: %x; ID: %x; Len: %x', [P, ID, FBody.Position - P + 1]));
    {$ENDIF}
    {$IFDEF SPECIAL_LOG_N1}
//    if Assigned(FLog) and (FBody.Position > 969000) and (FBody.Position < 999999) then
//      FLog.Add(Format('Pos: %d; ID: %x; Len: %x', [P, ID, FBody.Position - P + 1]));
    {$ENDIF}
  end;
end;

function TAviconDataSource.LoadEventData(ID: Byte; Ptr: PEventData; MaxSize: Integer): Boolean; // ������� ������ �������
var
  FDWID: DWord;
  FWID: Word;
  FID: Byte;
  Size_: Integer;

  P1: Pointer;
  tmp: TMemoryStream;

begin
  try
    case ID of
  {0x90}          EID_Ku: FBody.ReadBuffer(Ptr^, 3);             // ��������� �������� ����������������
  {0x91}          EID_Att: FBody.ReadBuffer(Ptr^, 3);            // ��������� �����������
  {0x92}          EID_TVG: FBody.ReadBuffer(Ptr^, 3);            // ��������� ���
  {0x93}        EID_StStr: FBody.ReadBuffer(Ptr^, 3);            // ��������� ��������� ������ ������
  {0x94}       EID_EndStr: FBody.ReadBuffer(Ptr^, 3);            // ��������� ��������� ����� ������
  {0x95}       EID_HeadPh: FBody.ReadBuffer(Ptr^, 3);            // ������ ���������� ���������
  {0x96}         EID_Mode: FBody.ReadBuffer(Ptr^, 7);            // ��������� ������
  {0x9B}  EID_SetRailType: ;                                     // ��������� �� ��� ������
  {0x9C}   EID_PrismDelay: FBody.ReadBuffer(Ptr^, 4);            // ��������� 2�� (word)
  {0xA0}        EID_Stolb: FBody.ReadBuffer(Ptr^, 144);          // ������� ����������
  {0xA1}       EID_Switch: // ����� ����������� ��������
                     begin
                       FBody.ReadBuffer(pedTextLabel(Ptr)^.Len, 1);
                       FBody.ReadBuffer(pedTextLabel(Ptr)^.Text[0], pedTextLabel(Ptr)^.Len * SizeOf(Char));
                     end;
  {0xA2}    EID_DefLabel: // ����� �������
                                 begin
                                   FBody.ReadBuffer(pedDefLabel(Ptr)^.Len, 1);
                                   FBody.ReadBuffer(pedDefLabel(Ptr)^.Rail, 1);
                                   FBody.ReadBuffer(pedDefLabel(Ptr)^.Text[0], pedTextLabel(Ptr)^.Len * SizeOf(Char));
                                 end;
  {0xA3}    EID_TextLabel: // ��������� �������
                     begin
                       FBody.ReadBuffer(pedTextLabel(Ptr)^.Len, 1);
                       FBody.ReadBuffer(pedTextLabel(Ptr)^.Text[0], pedTextLabel(Ptr)^.Len * SizeOf(Char));
                     end;
  {0xA4}    EID_StBoltStyk: ; // ������� ������ ��
  {0xA5}   EID_EndBoltStyk: ; // ���������� ������ ��
  {0xA6}          EID_Time: FBody.ReadBuffer(Ptr^, 2);   // ������� �������
  {0xA7} EID_StolbChainage: FBody.ReadBuffer(Ptr^, 136); // ������� ����������
  {0xA8}EID_ZerroProbMode: FBody.ReadBuffer(Ptr^, 1);   // ����� ������ �������� 0 ����
  {0xA9}   EID_LongLabel: FBody.ReadBuffer(Ptr^, 24);  // ����������� �������
  {0xAA}     EID_SpeedState: FBody.ReadBuffer(Ptr^, 5);   // �������� � ���������� �������� ��������
  {0xAB} EID_ChangeOperatorName: FBody.ReadBuffer(Ptr^, 129); // ����� ��������� (��� ���������)
  {0xAC} EID_AutomaticSearchRes: FBody.ReadBuffer(Ptr^, 9);   // ��������� ������� �������, ���������� ��� �������������� ������
  {0xAD} EID_TestRecordFile: ;                                // ���� ������ ������������ ������
  {0xAE}   EID_OperatorRemindLabel: begin
                                      FBody.ReadBuffer(pedOperatorRemindLabel(Ptr)^.Len, 11);
                                      FBody.ReadBuffer(pedOperatorRemindLabel(Ptr)^.Text[0], pedOperatorRemindLabel(Ptr)^.Len *  SizeOf(Char));
                                    end;

  {0xAF} EID_QualityCalibrationRec:	FBody.ReadBuffer(Ptr^, 15);// �������� ��������� ������� ��������
  {0xB0}         EID_Sensor1: FBody.ReadBuffer(Ptr^, 2);   // ������ ������� �� � �������
  {0xB1}        EID_AirBrush: FBody.ReadBuffer(Ptr^, 182); // ��������������
  {0xB2}    EID_PaintMarkRes: FBody.ReadBuffer(Ptr^, 8);   // ��������� � ���������� ������� �� ������-�������
  {0xB3} EID_AirBrushTempOff: FBody.ReadBuffer(Ptr^, 5);   // ��������� ���������� ���������������
  {0xB4}       EID_AirBrush2: FBody.ReadBuffer(Ptr^, 246); // �������������� � ���������� �����������
  {0xB5}    EID_AlarmTempOff: FBody.ReadBuffer(Ptr^, 5);   // ��������� ���������� ��� �� ���� �������

  {0xB6} EID_StartSwitchShunter: ; // ������ ���� ����������� ��������
  {0xB7} EID_EndSwitchShunter: ; // ����� ���� ����������� ��������
  {0xB8}

         {$IFDEF Temperature_is_Byte}
         EID_Temperature: FBody.ReadBuffer(Ptr^, 2); // �����������
         {$ENDIF}
         {$IFNDEF Temperature_is_Byte}
         EID_Temperature: FBody.ReadBuffer(Ptr^, 5); // �����������
         {$ENDIF}

  {0xB9}          EID_DebugData: FBody.ReadBuffer(Ptr^, 128);  // ���������� ����������
  {0xBA} // EID_PaintSystemParams: FBody.ReadBuffer(Ptr^, 2048); // ��������� ������������
  {0xBB}        EID_UMUPaintJob: FBody.ReadBuffer(Ptr^, 9);    // ������� ��� �� �������������
  {0xBC}             EID_ACData: FBody.ReadBuffer(Ptr^, 1);    // ������ ������������� ��������

  {0xBD}          EID_SmallDate: begin   // ����� ������������� �������
                                   FBody.ReadBuffer(pedSmallDate(Ptr)^.DataSize, 2);
                                   SetLength(pedSmallDate(Ptr)^.Data, pedSmallDate(Ptr)^.DataSize);
                                   if pedSmallDate(Ptr)^.DataSize <> 0 then
                                     FBody.ReadBuffer(pedSmallDate(Ptr)^.Data[0], pedSmallDate(Ptr)^.DataSize);
                                 end;
  {0xBE}     EID_RailHeadScaner: begin   // ������ ����������� ������� ������� ������
                                    {
                                    FBody.ReadBuffer(pedRailHeadScaner(Ptr)^.Size, 1560);

                                    if pedRailHeadScaner(Ptr)^.Cross_Cross_Section_Image_Size <> 0 then
                                    begin
                                      SetLength(pedRailHeadScaner(Ptr)^.Cross_Cross_Section_Image, pedRailHeadScaner(Ptr)^.Cross_Cross_Section_Image_Size);
                                      FBody.ReadBuffer(pedRailHeadScaner(Ptr)^.Cross_Cross_Section_Image[0], pedRailHeadScaner(Ptr)^.Cross_Cross_Section_Image_Size);
                                    end;

                                    if pedRailHeadScaner(Ptr)^.Vertically_Cross_Section_Image_Size <> 0 then
                                    begin
                                      SetLength(pedRailHeadScaner(Ptr)^.Vertically_Cross_Section_Image, pedRailHeadScaner(Ptr)^.Vertically_Cross_Section_Image_Size);
                                      FBody.ReadBuffer(pedRailHeadScaner(Ptr)^.Vertically_Cross_Section_Image[0], pedRailHeadScaner(Ptr)^.Vertically_Cross_Section_Image_Size);
                                    end;

                                    if pedRailHeadScaner(Ptr)^.Horizontal_Cross_Section_Image_Size <> 0 then
                                    begin
                                      SetLength(pedRailHeadScaner(Ptr)^.Horizontal_Cross_Section_Image, pedRailHeadScaner(Ptr)^.Horizontal_Cross_Section_Image_Size);
                                      FBody.ReadBuffer(pedRailHeadScaner(Ptr)^.Horizontal_Cross_Section_Image[0], pedRailHeadScaner(Ptr)^.Horizontal_Cross_Section_Image_Size);
                                    end;
                                    }
                                    {
                                    Size_:= Length(pedRailHeadScaner(Ptr)^.Cross_Cross_Section_Image);
                                    tmp:= TMemoryStream.Create;
                                    tmp.SetSize(Size_);
                                    move(pedRailHeadScaner(Ptr)^.Cross_Cross_Section_Image[0], tmp.Memory^, Size_);
                                    tmp.SaveToFile('C:\temp\test1.png');
                                    tmp.Free;

                                    Size_:= Length(pedRailHeadScaner(Ptr)^.Vertically_Cross_Section_Image);
                                    tmp:= TMemoryStream.Create;
                                    tmp.SetSize(Size_);
                                    move(pedRailHeadScaner(Ptr)^.Vertically_Cross_Section_Image[0], tmp.Memory^, Size_);
                                    tmp.SaveToFile('C:\temp\test2.png');
                                    tmp.Free;

                                    Size_:= Length(pedRailHeadScaner(Ptr)^.Horizontal_Cross_Section_Image);
                                    tmp:= TMemoryStream.Create;
                                    tmp.SetSize(Size_);
                                    move(pedRailHeadScaner(Ptr)^.Horizontal_Cross_Section_Image[0], tmp.Memory^, Size_);
                                    tmp.SaveToFile('C:\temp\test3.png');
                                    tmp.Free;
 
                  }                 //SetLength(pedRailHeadScaner(Ptr)^.Data, pedRailHeadScaner(Ptr)^.Size);
                                    //FBody.ReadBuffer(pedRailHeadScaner(Ptr)^.Data[0], pedRailHeadScaner(Ptr)^.Size);
                                 end;
  {0xBF}  EID_SensAllowedRanges: begin   // ������� ����������� ���������� ��
                                   FBody.ReadBuffer(pedSensAllowedRanges(Ptr)^.ChCount, 1);
                                   if pedSensAllowedRanges(Ptr)^.ChCount <> 0 then
                                   begin
                                     SetLength(pedSensAllowedRanges(Ptr)^.Ranges, pedSensAllowedRanges(Ptr)^.ChCount);
                                     FBody.ReadBuffer(pedSensAllowedRanges(Ptr)^.Ranges[0], pedSensAllowedRanges(Ptr)^.ChCount * 3);
                                   end;
                                 end;

  {0xC0}     EID_GPSCoord: FBody.ReadBuffer(Ptr^, 8);   // �������������� ����������
  {0xC1}     EID_GPSState: FBody.ReadBuffer(Ptr^, 9);   // ��������� ��������� GPS
  {0xC2}        EID_Media: FBody.ReadBuffer(Ptr^, 5);   // �����������
  {0xC3}    EID_GPSCoord2: FBody.ReadBuffer(Ptr^, 12);  // �������������� ���������� �� ���������
  {0xC4}   EID_NORDCO_Rec: begin
                             FBody.ReadBuffer(Ptr^, SizeOf(edNORDCO_Rec)); // ������ NORDCO
                   //          Show-Message(FloatToStr(pedNORDCO_Rec(ptr)^.SurveyDate));
                           end;
  {0xC5}     EID_SensFail: FBody.ReadBuffer(Ptr^, 6);   // ���������� �������� ���������������� �� ������������ ��������

  {0xC6}     EID_AScanFrame: begin   // ���� �-���������
                               {
                                FBody.ReadBuffer(pedAScanFrame(Ptr)^.DataSize, 5);
                                FBody.ReadBuffer(pedAScanFrame(Ptr)^.Data.Rail, 10);
                                SetLength(pedAScanFrame(Ptr)^.Data.ImageData, pedAScanFrame(Ptr)^.DataSize - 10);
                                FBody.ReadBuffer(pedAScanFrame(Ptr)^.Data.ImageData[0], pedAScanFrame(Ptr)^.DataSize - 10);
                               }
                                  {
                                tmp:= TMemoryStream.Create;
                                tmp.SetSize(pedAScanFrame(Ptr)^.DataSize - 10);
                                move(&pedAScanFrame(Ptr)^.Data.ImageData[0], tmp.Memory^, pedAScanFrame(Ptr)^.DataSize - 10);
                                tmp.SaveToFile('C:\temp\test.png');
                                tmp.Free;
                                   }
                             end;
  {0xC7}     EID_MediumDate: begin        // ������� ������������� �������
                               FBody.ReadBuffer(pedMediumDate(Ptr)^.DataSize, 3);
                               if (pedMediumDate(Ptr)^.DataSize <= MaxSize) and (pedMediumDate(Ptr)^.DataSize > 0) then
                                    FBody.ReadBuffer(pedMediumDate(Ptr)^.Data[0], pedMediumDate(Ptr)^.DataSize);
                             //  else Show-Message('MediumDate to large !');
                             end;
  {0xC8}        EID_BigDate: begin   // ������� ������������� �������
                  {              FBody.ReadBuffer(pedBigDate(Ptr)^.DataSize, 5);
                                SetLength(pedBigDate(Ptr)^.Data, pedBigDate(Ptr)^.DataSize);
                                if pedBigDate(Ptr)^.DataSize <> 0 then
                                  FBody.ReadBuffer(pedBigDate(Ptr)^.Data[0], pedBigDate(Ptr)^.DataSize);   }
                             end;
  {0xF0}       EID_CheckSum: begin   // ����������� �����
                               FBody.Position:= FBody.Position + 4;
                             end;


        //     EID_CheckSum: FBody.ReadBuffer(Ptr^, 5); // ����� � ����������� �����
  {0xF2}    EID_SysCrd_NS: FBody.ReadBuffer(Ptr^, 1);
  {0xF3}    EID_SysCrd_NF: FBody.ReadBuffer(Ptr^, 4);
  {0xFF}  EID_EndFile: begin
                         FBody.ReadBuffer(Ptr^, 13);  // ����� �����
        //                     if Assigned(Log) then Log.Add('0xFF');
                       end;

  {0xF1}    EID_SysCrd_SS: FBody.ReadBuffer(Ptr^, 2);   // ��������� ���������� ��������
  {0xF4}    EID_SysCrd_SF: FBody.ReadBuffer(Ptr^, 5);   // ������ ��������� ���������� � �������� �������
  {0xF9}    EID_SysCrd_FS: FBody.ReadBuffer(Ptr^, 5);   // ��������� ���������� ��������
  {0xFC}    EID_SysCrd_FF: FBody.ReadBuffer(Ptr^, 8);   // ������ ��������� ���������� � ������ �������
  {0xF5}   EID_SysCrd_UMUA_Left_NF: FBody.ReadBuffer(Ptr^, 4); // ������ ��������� ���������� ��� ������ ��� A, ����� �������
  {0xF6}   EID_SysCrd_UMUB_Left_NF: FBody.ReadBuffer(Ptr^, 4); // ������ ��������� ���������� ��� ������ ��� �, ����� �������
  {0xF7}  EID_SysCrd_UMUA_Right_NF: FBody.ReadBuffer(Ptr^, 4); // ������ ��������� ���������� ��� ������ ��� A, ������ �������
  {0xF8}  EID_SysCrd_UMUB_Right_NF: FBody.ReadBuffer(Ptr^, 4); // ������ ��������� ���������� ��� ������ ��� �, ������ �������
      else
      begin
        result:= false;
        exit;
      end;
    end;
    result:= true;
  except
    result:= false;
  end;
end;

procedure TAviconDataSource.AnalyzeFileBody_;
var
  LoadID: Byte;
  I: Integer;

begin

 // Show-Message('!');

  for I:= 0 to 255 do EventsCount[I]:= 0;

  if not FFullHeader then FBody.Position:= 17
                     else FBody.Position:= SizeOf(TFileHeader); // ������� ���������

  if FBody.Position < FDataSize then
    repeat
      FBody.ReadBuffer(LoadID, 1); // �������� �������������� �������
      if SkipEvent(LoadID) then
      begin
        if LoadID and 128 = 0 then Inc(EventsCount[0])
                              else Inc(EventsCount[LoadID]);
      end
    until FBody.Position + 1 > FDataSize;

end;


procedure TAviconDataSource.AnalyzeFileBody; // ���������� - ��������� ���� � �����. - ����� �����
label LoadCont, LoadStop;

var
  HSItem: THSItem;
  CurSysCoord: Integer;
  FullSysCoord: Integer;
//  SaveSkipFullSysCoord: Integer; !!!
  Load_CurSysCoord: Integer;
  LastSysCoord: Integer;
  LastDisCoord: Integer;
  CurDisCoord: Int64;
  BackMotionFlag: Boolean;
  LastCoordEvt: Integer;
  SaveOffset: Integer;
//  SaveOffset_2: Integer;
  LastSaveOffset: Integer;
  I: Integer;
  LoadID: Byte;
  LoadByte: array [1 .. 331] of Byte;
  LoadLongInt: array [1 .. 5] of Longint; // absolute LoadByte[1];
  NeedSync: Boolean;
  Card: DWord;
  CheckSumm: Int64;
  ErrorOffset: Integer;
  NewProgress: Integer;
  LoadStopFlag: Boolean;
  FirstCoordAfterError: Boolean;
  LastStolbSysCoord: Integer;
  AddErrID: Integer;
  FFF: Boolean;
  Last: Integer;
  Temp1, Temp2: Extended;
  StopSearch: Boolean;
  tmp: Boolean;
  SearchBMEnd: Boolean;
  StartBMSysCoord: Integer;
  R: TRail;
  Time: edTime;
  Head_: TFileHeader;
  Head__: TExHeaderHead;
  ExHeader__: TExHeader;
  DEBUG_Update_crd: Boolean;
  P: Integer;
  Data: array [0..50] of Byte;
  ErrorFlag: Boolean;
  tmpHeader: THSHead;
  tmpHeader_OLD: THSHead_OLD;
  tmpItem: THSItem;
  tmpHSHead: THeadScanListItem;
  AScanHead: edAScanFrame;
  DataType_1_1: Boolean;
  DataType_1_2: Boolean;
  DataType_2_1: Boolean;
  DataType_2_2: Boolean;
  SkipFlag: Boolean;
  WorkTime: DWord;
  DataSize: DWord;
  DataType: Byte;
  Temperature: Single;
  Ch: Byte;
  DefLabel           : edDefLabel;
  TextLabel          : edTextLabel;
  OperatorRemindLabel: edOperatorRemindLabel;
  StartSwitchEx      : edStartSwitchEx;
  EndSwitchEx        : edEndSwitchEx;

  ErrorState: Boolean;

  procedure AddEvent(ID: Byte; SysCoord: Integer = - MaxInt; DisCoord: Integer = -MaxInt);
  begin
    // UnPackEcho;

    //if Length(FEvents) + 1 = 1404 then Show-Message('2');


    SetLength(FEvents, Length(FEvents) + 1);

    FEvents[ High(FEvents)].ID:= ID;
    FEvents[ High(FEvents)].OffSet:= SaveOffset;
    if SysCoord = -MaxInt then FEvents[ High(FEvents)].SysCoord:= CurSysCoord
                          else FEvents[ High(FEvents)].SysCoord:= SysCoord;
    if DisCoord = -MaxInt then FEvents[ High(FEvents)].DisCoord:= CurDisCoord
                          else FEvents[ High(FEvents)].DisCoord:= DisCoord;

    LastSaveOffset:= SaveOffset;
  end;


function TestText(Text: array of Char; Len: Integer): Boolean;
var
  I: Integer;

begin
  Result:= True;
  for I := 0 to Len - 1 do
    if not ((Integer(Text[I]) <= $FF) or ((Integer(Text[I]) >= $400) and (Integer(Text[I]) <= $4FF)) or ((Integer(Text[I]) >= $2100) and (Integer(Text[I]) <= $214F))) then
    begin
      Result:= False;
      Break;
    end;
end;

procedure FindFileBodyTrouble(BadID: Integer);
begin
  if not ErrorState then
  begin
    ErrorState:= True;
    AddEvent(EID_StartBadZone);
  end;
end;

procedure FindFileBodyOK;
begin
  if ErrorState then
  begin
    ErrorState:= False;
    AddEvent(EID_EndBadZone);
  end;
end;

begin
  ErrorState:= False;
  DEBUG_Update_crd:= false;
  FExHeader.EndSysCoord:= 0;
//  FLog:= nil;
  {$IFDEF GENERATE_LOG}
  FLog:= TStringList.Create;
  if Assigned(FLog) and FileExists('c:\1\events.log') then  FLog.LoadFromFile('c:\1\events.log');
  {$ENDIF}
  {$IFDEF SPECIAL_LOG_N1}
  FLog:= TStringList.Create;
  if Assigned(FLog) and FileExists('c:\1\events.log') then  FLog.LoadFromFile('c:\1\events.log');
  {$ENDIF}
  {$IFDEF SPECIAL_LOG_N2}
  FLog:= TStringList.Create;
  if Assigned(FLog) and FileExists('c:\1\events.log') then  FLog.LoadFromFile('c:\1\events.log');
  {$ENDIF}

  // ����� ������
  BackMotionFlag:= False;
  SearchBMEnd:= False;
  SetLength(FEvents, 0); // ������� ������� ���������
  // SetLength(FErrors, 0); // ������� ������� ������

  LoadStopFlag:= False;
  CurSysCoord:= 0;
  FullSysCoord:= 0;
//  SaveSkipFullSysCoord:= FullSysCoord; !!!
  Load_CurSysCoord:= 0;
  CurDisCoord:= 0;
  LastSysCoord:= MaxInt;
  LastSaveOffset:= 0;
  LastCoordEvt:= -1;
  NeedSync:= False;
  LastStolbSysCoord:= 0;
//  SaveOffset_2:= 0;
  tmp:= true;

//  {$IFDEF SPECIAL_LOG_N2}
  FFirstSysCoord:= True;
  FSaveFullSysCoord:= -1;
//  {$ENDIF}

  {$IFDEF SPECIAL_LOG_N2}
  if Assigned(FLog) then
  begin
    WorkTime:= GetTickCount();
    FLog.Add('');
    FLog.Add(Format('-------------------- Start AnalyzeFileBody  -----------[ %s ]------------', [ExtractFileName(FileName)]));
    FLog.Add('');
  end;
  {$ENDIF}

  if not FFullHeader then FBody.Position:= 17
                     else begin
                            //FBody.Position:= SizeOf(TFileHeader); // ������� ���������
                            FBody.Position:= 0; // ������� ���������
                            FBody.ReadBuffer(Head_, SizeOf(TFileHeader));
                          end;

  SaveOffset:= FBody.Position; // ���������� offset �������
  AddEvent(EID_FwdDir, 0);

LoadCont :

  FExHeader.Reserv2[1]:= $FF; // �������� ��� ���� ��� ������� ����������!

  if FBody.Position < FDataSize then
  try
    repeat

//      LastId:= LoadID;
//      LastIdOffset:= FBody.Position;

//      SaveOffset_2:= SaveOffset;
      SaveOffset:= FBody.Position; // ���������� offset �������
      FBody.ReadBuffer(LoadID, 1); // �������� �������������� �������

//     if (FBody.Position = 30736358 {68}) and tmp then
//     begin
//       tmp:= false;
//       Show-Message('2');
//     end;

      if LoadID and 128 = 0 then
      begin
         // �������
        Ch:= (LoadID shr 2) and 15;
        FBody.Position:= FBody.Position + EchoLen[LoadID and $03];
        if FExtendedChannels and (Ch > 14) then FBody.Position:= FBody.Position + 1;

      end else
       if FIsGoodID[LoadID] then
       begin
        case LoadID of

  EID_SysCrd_NS:  begin // �������� ����������
                    FBody.ReadBuffer(LoadByte[1], 1);
                    CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[1]);
//                    CurSysCoord:= Integer((Integer(FullSysCoord) and $FFFFFF00) or LoadByte[1]); !!!
                    if FSkipCoordEventCount = 0 then FExHeader.EndSysCoord:= Max(FExHeader.EndSysCoord, CurSysCoord);
                  end;
  EID_SysCrd_NF:  begin // - ������ ����������
                    FBody.ReadBuffer(LoadLongInt[2], 4);
                    CurSysCoord:= LoadLongInt[2];

{                    if (FSkipCoordEventCount = 0) or
                      (FSkipCoordEventCount <> 0) and (abs((CurSysCoord shr 8) - (FullSysCoord shr 8)) <= 2) then begin
                      FullSysCoord:= LoadLongInt[2]; //SaveSkipFullSysCoord:= FullSysCoord;
                    end; !!!
//TODO � �� ��� �������� ������ ������ ����������, ����� ����� �������
}
                    if not FFirstSysCoord then
                    begin
                      if (FSkipCoordEventCount = 0) then // ��� ���������� ������� �� ����� �������
                      begin

                        if (Abs(Int64(FSaveFullSysCoord) - Int64(CurSysCoord)) > 25000 {512}) then
                        begin
                          {$IFDEF SPECIAL_LOG_N2}
                          if Assigned(FLog) then FLog.Add(Format('Pos: %d; Coord Jump - from: %d, to: %d; Delta: %d', [SaveOffset, FSaveFullSysCoord, CurSysCoord, CurSysCoord - FSaveFullSysCoord]));
                          {$ENDIF}

                          LoadID:= 0; // ������� ��� �������
                          CurSysCoord:= FSaveFullSysCoord;
                          FBody.Position:= SaveOffset + 1;
                          FindFileBodyTrouble(EID_SysCrd_NF);
                        end;

                        {$IFDEF SPECIAL_LOG_N2}
                      {  if not ((Abs(CurSysCoord - FSaveFullSysCoord) = 1) or
                                (Abs(CurSysCoord - FSaveFullSysCoord) = 254) or
                                (Abs(CurSysCoord - FSaveFullSysCoord) = 255) or
                                (Abs(CurSysCoord - FSaveFullSysCoord) = 256) or
                                (Abs(CurSysCoord - FSaveFullSysCoord) = 257) or
                                (Abs(CurSysCoord - FSaveFullSysCoord) = 258)) then
                        begin
                          if Assigned(FLog) then FLog.Add(Format('Pos: %d; Crd1: %d; Crd2: %d; Delta: %d', [SaveOffset, FSaveFullSysCoord, CurSysCoord, CurSysCoord - FSaveFullSysCoord]));
                        end;    }
                       {$ENDIF}
                      end;
                    end else FFirstSysCoord:= False;
                    if (LoadID <> 0) and (FSkipCoordEventCount = 0) then
                    begin
                      FindFileBodyOK;
                      FSaveFullSysCoord:= CurSysCoord;
                    end;
                  end;
  EID_HandScan: begin           // ������
                  P:= FBody.Position;
                  FBody.ReadBuffer(tmpHeader, SizeOf(THSHead));

                  if not ((tmpHeader.DataSize > 0) and (tmpHeader.DataSize <= 100000) and
                          (tmpHeader.DelayMultiply in [1, 3]) and
                          (Integer(tmpHeader.Rail) in [0, 1]) and
                  //        (tmpHeader.ScanSurface in [0..9]) and
                          (Config.HandChanNumIsValid(tmpHeader.HandChNum))) then
//                          (tmpHeader.HandChNum in [0..7])) then
                  begin
                    {$IFDEF SPECIAL_LOG_N2}
                    if Assigned(FLog) then
                    begin
                      FLog.Add(Format('Pos: %d; Bad HandScan Event', [FBody.Position]));
                      FLog.Add(Format(' - DataSize: %d', [tmpHeader.DataSize]));
                      FLog.Add(Format(' - DelayMultiply: %d', [tmpHeader.DelayMultiply]));
                      FLog.Add(Format(' - Rail: %d', [Integer(tmpHeader.Rail)]));
                      FLog.Add(Format(' - ScanSurface: %d', [tmpHeader.ScanSurface]));
                      FLog.Add(Format(' - HandChNum: %d', [tmpHeader.HandChNum]));
                    end;
                    {$ENDIF}
                    LoadID:= 0;
                    FBody.Position:= SaveOffset + 1;
                    FindFileBodyTrouble(EID_HandScan);
                  end
                  else
                  begin
                  (*
                    FBody.Position:= FBody.Position + tmpHeader.DataSize - 3;
                    FBody.ReadBuffer(tmpItem, 3);
                    DataType_1_1:= (tmpItem.Sample = $FFFF) and (tmpItem.Count = 0);

                    FBody.Position:= P;
                    FBody.ReadBuffer(tmpHeader, SizeOf(THSHead));
                    FBody.Position:= FBody.Position + tmpHeader.DataSize - 5;
                    FBody.ReadBuffer(tmpItem, 5);
                    DataType_1_2:= (tmpItem.Sample = $FFFF) and (tmpItem.Count = 1);

                    FBody.Position:= P;
                    FBody.ReadBuffer(tmpHeader_OLD, SizeOf(THSHead_OLD));
                    FBody.Position:= FBody.Position + tmpHeader.DataSize - 3;
                    FBody.ReadBuffer(tmpItem, 3);
                    DataType_2_1:= (tmpItem.Sample = $FFFF) and (tmpItem.Count = 0);

                    FBody.Position:= P;
                    FBody.ReadBuffer(tmpHeader_OLD, SizeOf(THSHead_OLD));
                    FBody.Position:= FBody.Position + tmpHeader.DataSize - 5;
                    FBody.ReadBuffer(tmpItem, 5);
                    DataType_2_2:= (tmpItem.Sample = $FFFF) and (tmpItem.Count = 1);
                  *)

                    FBody.Position:= FBody.Position + tmpHeader.DataSize;
                    FSkipEventFlag_HandScan:= True;
                    FindFileBodyOK;

                    {$IFDEF SPECIAL_LOG_N2}
                    //if Assigned(FLog) then
                    //begin
                    //  FLog.Add(Format('Pos: %d; HandScan Event Size: %d', [FBody.Position, tmpHeader.DataSize]));
                    //  HandScan_Event_Max_Size:= Max(HandScan_Event_Max_Size, tmpHeader.DataSize);
                    //  FLog.Add(Format('--- HandScan Event Max Size: %d', [HandScan_Event_Max_Size]));
                    //end;
                    {$ENDIF}
                  (*
                    FBody.Position:= P;
                    if DataType_1_1 or DataType_1_2 then
                    begin
                      FBody.ReadBuffer(tmpHeader, SizeOf(THSHead));
                      FBody.Position:= FBody.Position + tmpHeader.DataSize;
                      FSkipEventFlag_HandScan:= True;
                    end
                    else
             //       if DataType_2_1 or DataType_2_2 then
                    begin
                      FBody.ReadBuffer(tmpHeader_OLD, SizeOf(THSHead_OLD));
                      FBody.Position:= FBody.Position + tmpHeader_OLD.DataSize;
                      FSkipEventFlag_HandScan:= True;
                    end;
                   *)
                  end;
                end;

  EID_RailHeadScaner:   // �������� ������ ������� ������� ������
                begin
                  FBody.ReadBuffer(tmpHSHead.Size, 12);
                  if not ((tmpHSHead.Size > 0) and (tmpHSHead.Size <= 100000) and
                          (tmpHSHead.WorkSide in [1, 2]) and
                          (Integer(tmpHSHead.Rail) in [0, 1, 100, 101, 104, 105]) and
//                          (Integer(tmpHSHead.Rail and 3) in [0, 1]) and
                          (tmpHSHead.OperatingTime < 2000) and
                          (tmpHSHead.ThresholdUnit in [1, 2])) then
                  begin
                    {$IFDEF SPECIAL_LOG_N2}
                    if Assigned(FLog) then
                    begin
                      FLog.Add(Format('Pos: %d; Bad Rail Head Scaner event', [FBody.Position]));
                      FLog.Add(Format(' - DataSize: %d', [tmpHSHead.Size]));
                      FLog.Add(Format(' - WorkSide: %d', [tmpHSHead.WorkSide]));
                      FLog.Add(Format(' - Rail: %d', [Integer(tmpHSHead.Rail)]));
                      FLog.Add(Format(' - OperatingTime: %d', [tmpHSHead.OperatingTime]));
                      FLog.Add(Format(' - ThresholdUnit: %d', [tmpHSHead.ThresholdUnit]));
                    end;
                    {$ENDIF}
                    LoadID:= 0;
                    FBody.Position:= SaveOffset + 1;
                    FindFileBodyTrouble(EID_RailHeadScaner);
                  end
                  else
                  begin
                    FBody.Position:= FBody.Position + tmpHSHead.Size - 8;
                    FindFileBodyOK;
                    {$IFDEF SPECIAL_LOG_N2}
                    if Assigned(FLog) then
                    begin
//                      FLog.Add(Format('Pos: %d; Rail Head Scaner event Size: %d', [FBody.Position, tmpHSHead.Size]));
                      RailHeadScaner_Event_Max_Size:= Max(RailHeadScaner_Event_Max_Size, tmpHeader.DataSize);
                    end;
                    {$ENDIF}
                  end;
                end;

  EID_AScanFrame: begin
                    FBody.ReadBuffer(AScanHead, 15);
                    if not ((AScanHead.DataSize > 0) and (AScanHead.DataSize < 500000) and  //(tmpHSHead.Size <= 500000) and
                            (AScanHead.DataType in [1, 2]) and //= 1) and
                            (Ord(AScanHead.Data.Rail) in [0, 1]) and
                            (AScanHead.Data.Channel in [0..15, 255])) then
                    begin
                      {$IFDEF SPECIAL_LOG_N2}
                      if Assigned(FLog) then
                      begin
                        FLog.Add(Format('Pos: %d; Bad AScan Frame event', [FBody.Position]));
                        FLog.Add(Format(' - DataSize: %d', [AScanHead.DataSize]));
                        FLog.Add(Format(' - DataType: %d', [AScanHead.DataType]));
                        FLog.Add(Format(' - Rail: %d', [Integer(AScanHead.Data.Rail)]));
                        FLog.Add(Format(' - Channel: %d', [AScanHead.Data.Channel]));
                      end;
                      {$ENDIF}
                      LoadID:= 0;
                      FBody.Position:= SaveOffset + 1;
                      FindFileBodyTrouble(EID_AScanFrame);
                    end
                    else
                    begin
                      if AScanHead.DataType = 2 then FBody.Position:= FBody.Position + AScanHead.DataSize - 11
                      else FBody.Position:= FBody.Position + AScanHead.DataSize - 10;
                      FindFileBodyOK;
                      {$IFDEF SPECIAL_LOG_N2}
                      if Assigned(FLog) then
                        FLog.Add(Format('Pos: %d; AScan Frame event: %d', [FBody.Position, tmpHSHead.Size]));
                      {$ENDIF}
                    end;
                  end;

  EID_Ku, // ��������� �������� ����������������
  EID_Att, // ��������� �����������
  EID_TVG, // ��������� ���
  EID_StStr, // ��������� ��������� ������ ������
  EID_EndStr, // ��������� ��������� ����� ������
  EID_HeadPh: // ������ ���������� ���������
                          begin
                            FBody.ReadBuffer(LoadByte[1], 3);
                            if not ((LoadByte[1] in [0, 1]) and (LoadByte[2] in [0..15, 255])) then
                            begin
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                                FLog.Add(Format('Pos: %d; Bad ChParam Event [id:%x] - Rail: %d, Ch: %d', [FBody.Position, LoadId, LoadByte[1], LoadByte[2]]));
                              {$ENDIF}
                              LoadID:= 0;
                              FBody.Position:= SaveOffset + 1;
                              FindFileBodyTrouble(EID_HeadPh);
                            end else FindFileBodyOK;
                          end;

  EID_PrismDelay: // ��������� 2�� (word)
                          begin
                            FBody.ReadBuffer(LoadByte[1], 4);
                            if not ((LoadByte[1] in [0, 1]) and (LoadByte[2] in [0..15, 255])) then
                            begin
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                                FLog.Add(Format('Pos: %d; Bad ChParam Event [id:%x] - Rail: %d, Ch: %d', [FBody.Position, LoadId, LoadByte[1], LoadByte[2]]));
                              {$ENDIF}
                              LoadID:= 0;
                              FBody.Position:= SaveOffset + 1;
                              FindFileBodyTrouble(EID_PrismDelay);
                            end else FindFileBodyOK;
                          end;
       EID_Mode: // ��������� ������
                          begin
                            FBody.ReadBuffer(LoadByte[1], 7);
//                            if not ((LoadByte[2] = 0) and (LoadByte[1] in [0..12]) and (LoadByte[4] in [0, 1]) and (LoadByte[5] in [0..15, 255])) then
                            if not ((LoadByte[2] = 0) and (LoadByte[1] in [0..12]) and
                                    ((Boolean(LoadByte[3]) = False) or ((Boolean(LoadByte[3]) = True) and (LoadByte[4] in [0, 1]) and (LoadByte[5] in [0..15, 255])))) then
                            begin
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                                FLog.Add(Format('Pos: %d; Bad Mode Event - Mode: %d; Rail: %d, Ch: %d', [FBody.Position, LoadByte[1] + LoadByte[2] * 256, LoadByte[4], LoadByte[5]]));
                              {$ENDIF}
                              LoadID:= 0;
                              FBody.Position:= SaveOffset + 1;
                              FindFileBodyTrouble(EID_Mode);
                            end
                            else
                            begin
                              FindFileBodyOK;
                              FSkipEventFlag_HandScan:= (LoadByte[1] in [1, 3]) and (LoadByte[2] = 0);
                            end;
                          end;
//  EID_SetRailType = $9B; // ��������� �� ��� ������
        EID_Stolb: // ������� ����������
                          begin
                            FBody.ReadBuffer(LoadByte[1], 144);
                            for I := 17 to 144 do
                              if LoadByte[I] <> 0 then
                              begin
                                {$IFDEF SPECIAL_LOG_N2}
                                if Assigned(FLog) then
                                  FLog.Add(Format('Pos: %d; Bad Stolb Event in Zerro zone [Pos: %d]', [FBody.Position, I - 17]));
                                {$ENDIF}
                                LoadID:= 0;
                                FBody.Position:= SaveOffset + 1;
                                FindFileBodyTrouble(EID_Stolb);
                                Break;
                              end;
                            if LoadID <> 0 then FindFileBodyOK;
                          end;

    EID_StolbChainage: // ������� ���������� Chainage
                          begin
                            FBody.ReadBuffer(LoadByte[1], 136);
                            for I := 9 to 136 do
                              if LoadByte[I] <> 0 then
                              begin
                                {$IFDEF SPECIAL_LOG_N2}
                                if Assigned(FLog) then
                                  FLog.Add(Format('Pos: %d; Bad StolbChainage Event in Zerro zone [Pos: %d]', [FBody.Position, I - 9]));
                                {$ENDIF}
                                LoadID:= 0;
                                FBody.Position:= SaveOffset + 1;
                                FindFileBodyTrouble(EID_StolbChainage);
                                Break;
                              end;
                            if LoadID <> 0 then FindFileBodyOK;
                          end;
  EID_AutomaticSearchRes: // ��������� ������� �������, ���������� ��� �������������� ������
                          begin
                            FBody.ReadBuffer(LoadByte[1], 9);
                            if not ((LoadByte[1] in [0, 1]) and (LoadByte[2] in [0..15])) then
                            begin
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                                FLog.Add(Format('Pos: %d; Bad ChParam Event - Rail: %d, Ch: %d', [FBody.Position, LoadByte[1], LoadByte[2]]));
                              {$ENDIF}
                              LoadID:= 0;
                              FBody.Position:= SaveOffset + 1;
                              FindFileBodyTrouble(EID_AutomaticSearchRes);
                            end else FindFileBodyOK;
                          end;

      EID_TestRecordFile: // ���� ������ ������������ ������
                          begin
                            FBody.ReadBuffer(P, 4);
                            if (P <= 5310) or (P > FBody.Size) or (FBody.Position <> 5315) then
                            begin
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                                FLog.Add(Format('Pos: %d; Bad TestRecord - Pos: %d, Size: %d', [FBody.Position, FBody.Position, P]));
                              {$ENDIF}
                              LoadID:= 0;
                              FBody.Position:= SaveOffset + 1;
                              FindFileBodyTrouble(EID_TestRecordFile);
                            end
                            else
                            begin
                              FBody.Position:= FBody.Position + P;
                              FindFileBodyOK;
                            end;
                           end;

  EID_QualityCalibrationRec:	// �������� ��������� ������� ��������
                          begin
                            FBody.ReadBuffer(LoadByte[1], 15);
                            if not ((LoadByte[1] in [0, 1]) and (LoadByte[2] in [0..5]) and (LoadByte[3] <= 12)) then
                            begin
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                              FLog.Add(Format('Pos: %d; Bad QualityCalibrationRec Event - Rail: %d, Type: %d', [FBody.Position, LoadByte[1], LoadByte[2]]));
                              {$ENDIF}
                              LoadID:= 0;
                              FBody.Position:= SaveOffset + 1;
                              FindFileBodyTrouble(EID_QualityCalibrationRec);
                            end else FindFileBodyOK;
                          end;

          EID_Temperature: // �����������
                          begin
                            FBody.ReadBuffer(LoadByte[1], 1);
                            FBody.ReadBuffer(Temperature, 4);
                            if not ((LoadByte[1] in [0..3]) {and (Temperature > - 100) and (Temperature < 80)}) then // ���������������� �.�. ������ ����������� ����� ������� � ��-�� ����� ���������� bad zones � �������� ��������
                            begin
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
//                              FLog.Add(Format('Pos: %d; Bad Temperature Event - Nensor Number: %d, Value: %f', [FBody.Position, LoadByte[1], Temperature]));
                              FLog.Add(Format('Pos: %d; Bad Temperature Event - Nensor Number: %d, Value: XXX', [FBody.Position, LoadByte[1]]));
                              {$ENDIF}
                              LoadID:= 0;
                              FBody.Position:= SaveOffset + 1;
                              FindFileBodyTrouble(EID_Temperature);
                            end else FindFileBodyOK;
                          end;

          EID_SmallDate: // ����� ������������� �������
                          begin
                            DataSize:= 0;
                            FBody.ReadBuffer(DataSize, 1);
                            FBody.ReadBuffer(DataType, 1);
                            if not (DataType in [1..2]) then
                            begin
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                                FLog.Add(Format('Pos: %d; Bad SmallDate Event - DataType: %d', [FBody.Position, DataType]));
                              {$ENDIF}
                              LoadID:= 0;
                              FBody.Position:= SaveOffset + 1;
                              FindFileBodyTrouble(EID_SmallDate);
                            end
                            else
                            begin
                              FBody.Position:= FBody.Position + DataSize;
                              FindFileBodyOK;
                             end;
                          end;

              EID_Media: // �����������
                          begin
                            FBody.ReadBuffer(DataType, 1);
                            FBody.ReadBuffer(DataSize, 4);
                            if not (DataType in [1..3]) or (DataSize > 10000000) or (DataSize < 0) then
                            begin
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                                FLog.Add(Format('Pos: %d; Bad SmallDate Event - DataType: %d', [FBody.Position, DataType]));
                              {$ENDIF}
                              LoadID:= 0;
                              FBody.Position:= SaveOffset + 1;
                              FindFileBodyTrouble(EID_Media);

                            end
                            else
                            begin
                              FBody.Position:= FBody.Position + DataSize;
                              FindFileBodyOK;
                            end;
                          end;

           EID_SensFail: // ���������� �������� ���������������� �� ������������ ��������
                          begin
                            FBody.ReadBuffer(LoadByte[1], 6);
                            if not ((LoadByte[1] in [0, 1]) and (LoadByte[2] in [0..15, 255])) then
                            begin
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                                FLog.Add(Format('Pos: %d; Bad SensFail Event - Rail: %d, Ch: %d', [FBody.Position, LoadByte[1], LoadByte[2]]));
                              {$ENDIF}
                              LoadID:= 0;
                              FBody.Position:= SaveOffset + 1;
                              FindFileBodyTrouble(EID_SensFail);
                            end else FindFileBodyOK;
                          end;

         EID_MediumDate: // ������� ������������� �������
                          begin
                            DataSize:= 0;
                            FBody.ReadBuffer(DataSize, 2);
                            FBody.ReadBuffer(DataType, 1);

                            //if not ((DataType in [1..3]) or (DataType = $FF)) then
                            if not (((DataType = 1) and (DataSize <= 5310 + 25)) or
                                    ((DataType = 2) and (DataSize <= SizeOf(edStartSwitchEx) + 10)) or
                                    ((DataType = 3) and (DataSize <= SizeOf(edEndSwitchEx) + 10))) then
                            begin
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                                FLog.Add(Format('Pos: %d; Bad MediumDate Event - DataType: %d', [FBody.Position, DataType]));
                              {$ENDIF}
                              LoadID:= 0;
                              FBody.Position:= SaveOffset + 1;
                              FindFileBodyTrouble(EID_MediumDate);
                            end else
                          (*  if (DataType = 2) then
                            begin
                              FBody.ReadBuffer(StartSwitchEx.SwitchType, 6);
                              FBody.ReadBuffer(StartSwitchEx.Text[0], StartSwitchEx.Len * 2);
                              if (not TestText(StartSwitchEx.Text, StartSwitchEx.Len)) then
                              begin
                                {$IFDEF SPECIAL_LOG_N2}
                                if Assigned(FLog) then
                                  FLog.Add(Format('Pos: %d; Bad TextLabel or Switch Event', [FBody.Position]));
                                {$ENDIF}
                                LoadID:= 0;
                                FBody.Position:= SaveOffset + 1;
                              end;
                            end else

                            if (DataType = 3) then
                            begin
                              FBody.ReadBuffer(EndSwitchEx.Len, 1);
                              FBody.ReadBuffer(EndSwitchEx.Text[0], EndSwitchEx.Len * 2);
                              if (not TestText(EndSwitchEx.Text, StartSwitchEx.Len)) then
                              begin
                                {$IFDEF SPECIAL_LOG_N2}
                                if Assigned(FLog) then
                                  FLog.Add(Format('Pos: %d; Bad TextLabel or Switch Event', [FBody.Position]));
                                {$ENDIF}
                                LoadID:= 0;
                                FBody.Position:= SaveOffset + 1;
                              end;
                            end
                            else      *)
                            begin
                              FBody.Position:= FBody.Position + DataSize;
                              FindFileBodyOK;
                            end;
                          end;

            EID_DefLabel: // ������� �������
                          begin
                            FBody.ReadBuffer(DefLabel, 2);
                            FBody.ReadBuffer(DefLabel.Text[0], DefLabel.Len * 2);
                            if (not (Integer(DefLabel.Rail) in [0, 1])) or
                               (not TestText(DefLabel.Text, DefLabel.Len)) then
                            begin
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                                FLog.Add(Format('Pos: %d; Bad DefLabel Event', [FBody.Position]));
                              {$ENDIF}
                              LoadID:= 0;
                              FBody.Position:= SaveOffset + 1;
                              FindFileBodyTrouble(EID_DefLabel);
                            end
                            else FindFileBodyOK;
                          end;
              EID_Switch, // ����� ����������� ��������
           EID_TextLabel: // ��������� �������
                          begin
                            FBody.ReadBuffer(TextLabel, 1);
                            FBody.ReadBuffer(TextLabel.Text[0], TextLabel.Len * 2);
                            if (not TestText(TextLabel.Text, TextLabel.Len)) then
                            begin
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                                FLog.Add(Format('Pos: %d; Bad TextLabel or Switch Event', [FBody.Position]));
                              {$ENDIF}
                              LoadID:= 0;
                              FBody.Position:= SaveOffset + 1;
                              FindFileBodyTrouble(EID_TextLabel);
                            end else FindFileBodyOK;
                          end;
  EID_ChangeOperatorName: begin
                            FBody.ReadBuffer(TextLabel.Len, 1);
                            FBody.ReadBuffer(TextLabel.Text[0], 128);
//                            FBody.ReadBuffer(TextLabel.Text[0], TextLabel.Len * 2);
                            if (TextLabel.Len > 64) or
                               (not TestText(TextLabel.Text, TextLabel.Len)) then
                            begin
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                                FLog.Add(Format('Pos: %d; Bad TextLabel or Switch Event', [FBody.Position]));
                              {$ENDIF}
                              LoadID:= 0;
                              FBody.Position:= SaveOffset + 1;
                              FindFileBodyTrouble(EID_ChangeOperatorName);
                            end
                            else
                            begin
                              FindFileBodyOK;
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                                FLog.Add('---------- ChangeOperatorName ----------');
                              {$ENDIF}
                            end;

                          end;
 EID_OperatorRemindLabel: begin
                            FBody.ReadBuffer(OperatorRemindLabel, 11);
                            FBody.ReadBuffer(OperatorRemindLabel.Text[0], OperatorRemindLabel.Len * 2);

                            if (not (Integer(OperatorRemindLabel.Rail) in [0, 1])) or
                               (not (OperatorRemindLabel.LabelType in [0, 1, 2, 3])) or
                               (not TestText(OperatorRemindLabel.Text, OperatorRemindLabel.Len)) then
                            begin
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                                FLog.Add(Format('Pos: %d; Bad OperatorRemindLabel Event', [FBody.Position]));
                              {$ENDIF}
                              LoadID:= 0;
                              FBody.Position:= SaveOffset + 1;
                              FindFileBodyTrouble(EID_OperatorRemindLabel);
                            end else FindFileBodyOK;
                          end;

            EID_BigDate: // ������� ������������� �������
                          begin
                         //   DataSize:= 0;
                         //   FBody.ReadBuffer(DataSize, 4);
                         //   FBody.ReadBuffer(DataType, 1);
                         //   if not (DataType in [1..2]) then
                         //   begin
                         //   end else FBody.Position:= FBody.Position + DataSize;
                            {$IFDEF SPECIAL_LOG_N2}
                            if Assigned(FLog) then
                              FLog.Add(Format('Pos: %d; Bad BigDate Event - not use now!!!', [FBody.Position]));
                            {$ENDIF}
                            LoadID:= 0;
                            FBody.Position:= SaveOffset + 1;
                            FindFileBodyTrouble(EID_BigDate);
                          end; //  else FindFileBodyOK;

{0xB1}       EID_AirBrush: if (not isA31) and (not isA15New) and (not isFilus_X17DW) and (not isMDL) then begin
                                                                                            FBody.Position:= FBody.Position + 182; // ��������������
                                                                                            FindFileBodyOK;
                                                                                          end
                                                                                          else
                                                                                          begin
                                                                                            {$IFDEF SPECIAL_LOG_N2}
                                                                                            if Assigned(FLog) then
                                                                                              FLog.Add(Format('Pos: %d; EID_AirBrush - not use in this file type!!!', [FBody.Position]));
                                                                                            {$ENDIF}
                                                                                            FBody.Position:= SaveOffset + 1;
                                                                                            FindFileBodyTrouble(EID_AirBrush);
                                                                                          end;

{0xB2}   EID_PaintMarkRes: if (not isA31) and (not isA15New) and (not isFilus_X17DW) and (not isMDL) then begin
                                                                                            FBody.Position:= FBody.Position + 8;   // ��������� � ���������� ������� �� ������-�������
                                                                                            FindFileBodyOK;
                                                                                          end
                                                                                          else
                                                                                          begin
                                                                                            {$IFDEF SPECIAL_LOG_N2}
                                                                                            if Assigned(FLog) then
                                                                                              FLog.Add(Format('Pos: %d; EID_AirBrush - not use in this file type!!!', [FBody.Position]));
                                                                                            {$ENDIF}
                                                                                            FBody.Position:= SaveOffset + 1;
                                                                                            FindFileBodyTrouble(EID_PaintMarkRes);
                                                                                          end;

{0xB3}EID_AirBrushTempOff: if (not isA31) and (not isA15New) and (not isFilus_X17DW) and (not isMDL) then begin
                                                                                            FBody.Position:= FBody.Position + 5;   // ��������� ���������� ���������������
                                                                                            FindFileBodyOK;
                                                                                          end
                                                                                          else
                                                                                          begin
                                                                                            {$IFDEF SPECIAL_LOG_N2}
                                                                                            if Assigned(FLog) then
                                                                                              FLog.Add(Format('Pos: %d; EID_AirBrush - not use in this file type!!!', [FBody.Position]));
                                                                                            {$ENDIF}
                                                                                            FBody.Position:= SaveOffset + 1;
                                                                                            FindFileBodyTrouble(EID_AirBrushTempOff);
                                                                                          end;

{0xB4}      EID_AirBrush2: if (not isA31) and (not isA15New) and (not isFilus_X17DW) and (not isMDL) then begin
                                                                                            FBody.Position:= FBody.Position + 246; // �������������� � ���������� �����������
                                                                                            FindFileBodyOK;
                                                                                          end
                                                                                          else
                                                                                          begin
                                                                                            {$IFDEF SPECIAL_LOG_N2}
                                                                                            if Assigned(FLog) then
                                                                                              FLog.Add(Format('Pos: %d; EID_AirBrush - not use in this file type!!!', [FBody.Position]));
                                                                                            {$ENDIF}
                                                                                            FBody.Position:= SaveOffset + 1;
                                                                                            FindFileBodyTrouble(EID_AirBrush2);
                                                                                          end;

{0xBA}  EID_PaintSystemParams: if (not isA31) and (not isA15New) and (not isFilus_X17DW) and (not isMDL) then
                                                                                         begin
                                                                                           FBody.Position:= FBody.Position + 2048; // ��������� ������������
                                                                                           FindFileBodyOK;
                                                                                         end
                                                                                         else
                                                                                         begin
                                                                                           {$IFDEF SPECIAL_LOG_N2}
                                                                                           if Assigned(FLog) then
                                                                                             FLog.Add(Format('Pos: %d; EID_AirBrush - not use in this file type!!!', [FBody.Position]));
                                                                                           {$ENDIF}
                                                                                           FBody.Position:= SaveOffset + 1;
                                                                                           FindFileBodyTrouble(EID_PaintSystemParams);
                                                                                         end;


{0xBB} EID_UMUPaintJob: if (not isA31) and (not isA15New) and (not isFilus_X17DW) and (not isMDL) then
                                                                                  begin
                                                                                    FBody.Position:= FBody.Position + 9;    // ������� ��� �� �������������
                                                                                    FindFileBodyOK;
                                                                                  end
                                                                                  else
                                                                                  begin
                                                                                    {$IFDEF SPECIAL_LOG_N2}
                                                                                    if Assigned(FLog) then
                                                                                      FLog.Add(Format('Pos: %d; EID_AirBrush - not use in this file type!!!', [FBody.Position]));
                                                                                    {$ENDIF}
                                                                                    FBody.Position:= SaveOffset + 1;
                                                                                    FindFileBodyTrouble(EID_UMUPaintJob);
                                                                                  end;

 EID_SensAllowedRanges: begin
                          ErrorFlag:= False;
                          FBody.ReadBuffer(Data[0], 1);
                          if Data[0] <= 17 then
                          begin
                            FBody.ReadBuffer(Data[1], Data[0] * 3);
                            for I := 1 to Data[0] do
                              if not (Data[I * 3 - 2] in [0..15, 255]) then
                              begin
                                ErrorFlag:= True;
                                Break;
                              end;
                          end else ErrorFlag:= True;
                          if ErrorFlag then
                          begin
                            {$IFDEF SPECIAL_LOG_N2}
                            if Assigned(FLog) then
                              FLog.Add(Format('Pos: %d; Bad SensAllowedRanges event', [FBody.Position]));
                            {$ENDIF}
                            FBody.Position:= SaveOffset + 1;
                            FindFileBodyTrouble(EID_SensAllowedRanges);
                          end else FindFileBodyOK;
                        end;

          EID_EndFile:    begin // ����� �����

                            ErrorFlag:= False;
                            FBody.ReadBuffer(LoadByte, 13);
                            for i := 1 to 13 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                            if not ErrorFlag then
                              if FBody.Position <> FBody.Size then
                              begin
                                if FBody.Size - FBody.Position >= SizeOf(TExHeader) then
                                begin
                                  FBody.ReadBuffer(LoadByte, 170 + 32 + 128);
                                  for i := 1 to 14 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                                  if not ErrorFlag then
                                    for i := 107 to 170 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                                  if not ErrorFlag then
                                    for i := 205 + 2 to 330 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                                  ErrorFlag:= ErrorFlag or not (LoadByte[19] in [0..8]) or (LoadByte[20] + LoadByte[21] + LoadByte[22] <> 0);
                                end;
                              end;
                            if ErrorFlag then begin
                                                {$IFDEF SPECIAL_LOG_N2}
                                                if Assigned(FLog) then
                                                FLog.Add(Format('Pos: %d; Bad EndFile Event', [FBody.Position]));
                                                {$ENDIF}
                                                LoadID:= 0;
                                                FBody.Position:= SaveOffset + 1;
                                                FindFileBodyTrouble(EID_EndFile);
                                              end
                                         else begin
                                                {$IFDEF SPECIAL_LOG_N2}
                                                if Assigned(FLog) then
                                                FLog.Add(Format('Pos: %d; Good EndFile Event', [FBody.Position]));
                                                {$ENDIF}
                                                AddEvent(EID_EndFile);
                                                FindFileBodyOK;
                                                Break;
                                              end;


(*
                            if FBody.Position = 5311 then
                            begin
                              FBody.Position:= FBody.Position - 1; // FHead.TableLink;
                              //FBody.ReadBuffer(Head__, SizeOf(Head__));
                              FBody.ReadBuffer(ExHeader__, SizeOf(ExHeader__));

                              repeat
                                FBody.ReadBuffer(LoadID, 1); // �������� �������������� �������
                                if LoadID = EID_Time then
                                begin
                                  FBody.ReadBuffer(Time, 2); // �������� �������������� �������
                                  if Time.Hour = FHead.Hour then
                                  begin
                            //        Show-Message(IntToStr(FHead.Hour) + ':' + IntToStr(FHead.Minute));
                                    Break;
                                  end else FBody.Position:= FBody.Position - 1;
                                end;
                              until False;
                              {$IFDEF SPECIAL_LOG_N2}
                              if Assigned(FLog) then
                                FLog.Add(Format('EID_EndFile - pos: 5311 - find pos: %d', [FBody.Position]));
                              {$ENDIF}
                            end
                            else
                            begin
                              FBody.ReadBuffer(LoadByte, 9 + 4);

                              DataType:= 1;
                              for i := 1 to 13 do
                                if LoadByte[i] <> $FF then
                                begin
                                  DataType:= 0;
                                  Break;
                                end;

                              if FBody.Position < FHead.TableLink - 10 then DataType:= 0;

                              if DataType = 0 then begin
                                                     {$IFDEF SPECIAL_LOG_N2}
                                                     if Assigned(FLog) then
                                                     FLog.Add(Format('Pos: %d; Bad EndFile Event', [FBody.Position]));
                                                     {$ENDIF}
                                                     LoadID:= 0;
                                                     FBody.Position:= SaveOffset + 1;
                                                   end
                                              else begin
                                                     {$IFDEF SPECIAL_LOG_N2}
                                                     if Assigned(FLog) then
                                                     FLog.Add(Format('Pos: %d; Good EndFile Event', [FBody.Position]));
                                                     {$ENDIF}
                                                     AddEvent(EID_EndFile);
                                                     Break;
                                                   end;

                            end; *)
                          end;

 {         EID_ACData:     begin
                            FBody.ReadBuffer(LoadByte[1], 1);
                            CurAKState[TRail(LoadByte[1] shr 6 and 1), (LoadByte[1] shr 2) and 15]:= Boolean(LoadByte[1] shr 7 and 1);
                            AddEvent(EID_ACData_);
                            Break;
                          end; }


              EID_SysCrd_SS:  begin // - �������� ������, �������� ����������
                                FBody.ReadBuffer(LoadByte[1], 2);
                                // if not TestOffset(FBody.Position - 3 - LoadByte[1]) then raise EBadLink.Create('');
                                CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[2]);
                              end;
              EID_SysCrd_SF:  begin // - �������� ������, ������ ����������
                                FBody.ReadBuffer(LoadByte[1], 1);
                                // if not TestOffset(FBody.Position - 2 - LoadByte[1]) then raise EBadLink.Create('');
                                FBody.ReadBuffer(LoadLongInt, 4);
                                CurSysCoord:= LoadLongInt[1];
                              end;
              EID_SysCrd_FS:  begin // - ������ ������, �������� ����������
                                FBody.ReadBuffer(LoadLongInt[1], 4);
                                FBody.ReadBuffer(LoadByte[1], 1);
                                // if not TestOffset(FBody.Position - 6 - LoadLongInt[1]) then raise EBadLink.Create('');
                                CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[1]);
                              end;
              EID_SysCrd_FF:  begin // - ������ ������, ������ ����������
                                FBody.ReadBuffer(LoadLongInt[1], 4);
                                FBody.ReadBuffer(LoadLongInt[2], 4);
                                // if not TestOffset(FBody.Position - 9 - LoadLongInt[1]) then raise EBadLink.Create('');
                                CurSysCoord:= LoadLongInt[2];
                              end;
          else
          // raise EMyExcept.Create('');       if not SkipEvent(LoadID) { LoadID and 128 = 0 } then // �������
          begin
            SkipFlag:= SkipEvent(LoadID);
          end;
        end;
       end
       else
       begin
        LoadID:= 0;
        {$IFDEF SPECIAL_LOG_N2}
        if Assigned(FLog) then
  //        if not SkipFlag then
          begin
            FLog.Add(Format('Pos: %d; Unknown ID - %x', [FBody.Position, LoadID]));
          end;
        {$ENDIF}
       end;

      {$IFDEF FIX_COORD_JUMP}
      if FSkipEventFlag_HandScan then FSkipCoordEventCount:= 15;
      {$ENDIF}

      {$IFDEF DEBUG_ADD_COORD}
      CurSysCoord:= CurSysCoord + 100;
      DEBUG_Update_crd:= true;
      {$ENDIF}


      if (LoadID in [EID_SysCrd_NS, EID_SysCrd_NF, EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF]) or
          DEBUG_Update_crd then // ��������� ����������
      begin
        DEBUG_Update_crd:= false;

        if LastSysCoord <> MaxInt then // ���� ��� �� ������ ���������� ����� ...
        begin
          {$IFDEF FIX_COORD_JUMP}
          if (FSkipCoordEventCount <> 0) then // ���������� ������� �� ����� �������
          begin
            if Abs(CurSysCoord - LastSysCoord) > 20 then
            begin
              CurSysCoord:= LastSysCoord;
              FExHeader.Reserv2[1]:= $01; // ���������� ������� �� ����� �������
            end;
            Dec(FSkipCoordEventCount);
          end;
          {$ENDIF}

          FExHeader.EndSysCoord:= Max(FExHeader.EndSysCoord, CurSysCoord);
          Last:= CurDisCoord;
          Inc(CurDisCoord, Abs(CurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)

          {$IF DEFINED(ESTONIA) or DEFINED(AVICON31E_KZ)}
          if Length(FEvents) < 100000 then
          {$ENDIF}
          {$IFNDEF SKIP_BACKMOTIONS}
          begin

            if (not BackMotionFlag) and (CurSysCoord < LastSysCoord) then
            // ���� ���������� ����������� �������� (������ �� �����)
            begin
              AddEvent(EID_BwdDir, LastSysCoord, Last);
              BackMotionFlag:= True;
              // AddEvent(EID_BwdDir);
              if not SearchBMEnd then
              begin
                StartBMSysCoord:= LastSysCoord;
                SearchBMEnd:= True;
              end;
            end
            else if BackMotionFlag and (CurSysCoord > LastSysCoord) then
            // ���� ���������� ����������� �������� (����� �� ������)
            begin
              AddEvent(EID_FwdDir, LastSysCoord, Last);
              BackMotionFlag:= False;
              // AddEvent(EID_FwdDir);
            end;
            if SearchBMEnd and (CurSysCoord > StartBMSysCoord) then
            begin
              SearchBMEnd:= False;
              if CurSysCoord - LastSysCoord <> 0 then AddEvent(EID_EndBM, StartBMSysCoord, Round(LastDisCoord + (StartBMSysCoord - LastSysCoord) * (CurDisCoord - LastDisCoord) / (CurSysCoord - LastSysCoord)))
                                                 else AddEvent(EID_EndBM);
            end;
          end;
          {$ENDIF}
        end
        else
        begin
          for I:= 0 to High(FEvents) do
          begin
            FEvents[I].SysCoord:= CurSysCoord;
            FEvents[I].DisCoord:= 0;
          end;
        end;

        {$IFDEF GENERATE_GLOBAL_LOG}
        if (MaxInt <> LastSysCoord) and (Abs(Abs(LastSysCoord) - Abs(CurSysCoord)) > 2000) then
          if FGLog.IndexOf(FFileName) = - 1 then FGLog.Add(FFileName);
        {$ENDIF}

        LastSysCoord:= CurSysCoord;
        LastDisCoord:= CurDisCoord;
      end;

      if (LoadID in [ EID_HandScan    ,
                      EID_Ku          ,
                      EID_Att         ,
                      EID_TVG         ,
                      EID_StStr       ,
                      EID_EndStr      ,
                      EID_HeadPh      ,
                      EID_Mode        ,
                      EID_SetRailType ,
                      EID_PrismDelay  ,
                      EID_Stolb       ,
                      EID_StolbChainage,
                      EID_Switch      ,
                      EID_DefLabel    ,
                      EID_TextLabel   ,
                      EID_StBoltStyk  ,
                      EID_EndBoltStyk ,
                 //     EID_ACData      ,
                      EID_SensFail    ,
                      EID_SmallDate, //? ����� ������������� �������
                      EID_RailHeadScaner, // ������ ����������� ������� ������� ������
                      EID_AScanFrame,   // ���� �-���������
                      EID_MediumDate,   //? ������� ������������� �������
                      EID_BigDate,      //? ������� ������������� �������
                      EID_CheckSum,
                      EID_Time        ,
                      EID_ZerroProbMode,
                      EID_LongLabel   ,

                      EID_SpeedState, // �������� � ���������� �������� ��������
                      EID_ChangeOperatorName, // ����� ��������� (��� ���������)
                      //EID_AutomaticSearchRes, // ��������� ������� �������, ���������� ��� �������������� ������
                      EID_TestRecordFile, // ���� ������ ������������ ������
                      EID_OperatorRemindLabel, // ������� ����, ������� ���������� � ������ ��� ����������� ��������� (��������� ������ � ������)
                      EID_QualityCalibrationRec,	// �������� ��������� ������� ��������

                      EID_Sensor1     ,
                      EID_AirBrush    ,
                      EID_PaintMarkRes ,
                      EID_AirBrushTempOff,
                      EID_AirBrush2   ,
                      EID_AlarmTempOff,

                      EID_StartSwitchShunter,  // ������ ���� ����������� ��������
                      EID_EndSwitchShunter,	 // ����� ���� ����������� ��������
                      EID_Temperature, // �����������
                      //EID_DebugData,
//                      EID_PaintSystemParams,
//                      EID_UMUPaintJob,
                      EID_GPSCoord    ,
                      EID_GPSState    ,
                      EID_Media       ,
                      EID_GPSCoord2   ,
                      EID_NORDCO_Rec  ,
                      EID_EndFile     ]) then AddEvent(LoadID);

      if FBody.Position - LastSaveOffset >= 16384 then AddEvent(EID_LinkPt);

    until FBody.Position + 1 > FDataSize;

  except
    {$IFDEF SPECIAL_LOG_N2}
    if Assigned(FLog) then FLog.Add(Format('Pos: %d; Except ID %d', [FBody.Position, LoadID]));
    {$ENDIF}
  end;

LoadStop :

  if (Length(FEvents) <> 0) and
    (FEvents[ High(FEvents)].DisCoord <> CurDisCoord) then
    AddEvent(EID_LinkPt);

  // FExHeader.EndKM:= CurrCoord.Km[1];
  // FExHeader.EndPK:= CurrCoord.Pk[1];

  if LastStolbSysCoord = 0 then // ���� ��� �� ������ ������ ����� ...
  begin
    // if Header.MoveDir > 0 then FExHeader.EndMM:= FHead.StartMetre * 1000 - (CurSysCoord - LastStolbSysCoord) * Header.ScanStep div 100
    // else FExHeader.EndMM:= FHead.StartMetre * 1000 + (CurSysCoord - LastStolbSysCoord) * Header.ScanStep div 100
  end
  else
  begin
    // if Header.MoveDir > 0 then FExHeader.EndMM:= 100000 - (CurSysCoord - LastStolbSysCoord) * Header.ScanStep div 100
    // else FExHeader.EndMM:= (CurSysCoord - LastStolbSysCoord) * Header.ScanStep div 100;
  end;

//  FExHeader.EndSysCoord:= CurSysCoord;
  FExHeader.EndDisCoord:= CurDisCoord;

  FFillEventsData:= False;
  FillEventsData;

  if CoordSys = csMetricRF  then FExHeader.EndMRFCrd:= DisToMRFCrd(MaxDisCoord)
                            else FExHeader.EndCaCrd:= DisToCaCrd(MaxDisCoord);
//  FExHeader.Reserv2[0]:= $03; // �������� ��������� ������ ����� ��� �������� ������� ���������� ����� ������� �������� (��� ������-31)
//  FExHeader.Reserv2[0]:= $04; // �������� ��������� ������ ����� � ����� �������� �������� ���� �����
//  FExHeader.Reserv2[0]:= $05; // ������� ���������� � �������� �������
  FExHeader.Reserv2[0]:= $06; // ������� ���������� � �������� �������


  {$IFDEF SPECIAL_LOG_N2}
  if Assigned(FLog) then
  begin

    WorkTime:= GetTickCount() - WorkTime;
    FLog.Add('');
    FLog.Add(Format('-------------------- End AnalyzeFileBody  -----------[ Work Time: %d ms ]------------', [WorkTime]));
    FLog.Add('');

    if HandScan_Event_Max_Size <> 0 then
      FLog.Add(Format('--- HandScan Event Max event  Size: %d', [HandScan_Event_Max_Size]));
    if RailHeadScaner_Event_Max_Size <> 0 then
      FLog.Add(Format('--- Rail Head Scaner Max event Size: %d', [RailHeadScaner_Event_Max_Size]));
  end;
  {$ENDIF}

  {$IFDEF GENERATE_LOG}
  FLog.SaveToFile('c:\1\events.log');
  FLog.Free;
  FLog:= nil;
  {$ENDIF}
  {$IFDEF SPECIAL_LOG_N1}
  FLog.SaveToFile('c:\1\events.log');
  FLog.Free;
  FLog:= nil;
  {$ENDIF}
  {$IFDEF SPECIAL_LOG_N2}
  FLog.SaveToFile('c:\1\events.log');
  FLog.Free;
  FLog:= nil;
  {$ENDIF}
end;

procedure TAviconDataSource.ReAnalyze;
begin
  AnalyzeFileBody;
//  FFillEventsData:= False;
//  FillEventsData;
  SaveExData(FExHdrLoadOk);
end;

// -------------< �������� ����������� ����� >------------------------------------

function TAviconDataSource.TestCheckSumm: Boolean;
var
  I: Integer;
  Card: DWord;
  CheckSumm: Int64;

begin
  (* {$OVERFLOWCHECKS OFF}
    CheckSumm:= CheckSummStartValue;
    FBody.Position:= 0;
    for I:= 1 to 29 do
    begin
    FBody.ReadBuffer(Card, 4);
    CheckSumm:= CheckSumm + Card;
    end;
    FBody.Position:= FBody.Position + 3 * 4;
    while FBody.Position + 4 < FDataSize {FBody.Size} do
    begin
    FBody.ReadBuffer(Card, 4);
    CheckSumm:= CheckSumm + Card;
    end;
    CheckSumm:= CheckSumm and $00000000FFFFFFFF;
    {$OVERFLOWCHECKS ON}

    Result:= FHead.CheckSumm = CheckSumm; *)
  Result:= True;
end;

// -------------< �������� ��� ������ >------------------------------------
(*
  function TAviconDataSource.TestOffsetFirst: Boolean;
  var
  I: Integer;

  begin
  Result:= True;
  FLastErrorIdx:= 0;
  for I:= 0 to High(FErrors) do
  begin
  if (FBody.Position < FErrors[I].Offset) then
  begin
  Result:= True;
  Break;
  end;
  if (FBody.Position >= FErrors[I].Offset) and
  (FBody.Position < FErrors[I].Offset + FErrors[I].Size) then
  case FErrors[I].Code of
  EID_BadBody,
  EID_BadLinc,
  EID_BadCoord: begin
  FBody.Position:= FErrors[I].Offset + FErrors[I].Size;
  FLastErrorIdx:= I;
  Result:= True;
  Break;
  end;
  EID_BadEnd: begin
  Result:= False;
  Break;
  end;
  end;
  end;
  end;

  function TAviconDataSource.TestOffsetNext: Boolean;
  var
  I: Integer;

  begin
  Result:= True;
  for I:= FLastErrorIdx to High(FErrors) do
  begin
  if (FBody.Position < FErrors[I].Offset) then
  begin
  FLastErrorIdx:= I;
  Result:= True;
  Break;
  end;
  if (FBody.Position >= FErrors[I].Offset) and
  (FBody.Position < FErrors[I].Offset + FErrors[I].Size) then
  case FErrors[I].Code of
  EID_BadBody,
  EID_BadLinc,
  EID_BadCoord: begin
  FBody.Position:= FErrors[I].Offset + FErrors[I].Size;
  FLastErrorIdx:= I;
  Result:= True;
  Break;
  end;
  EID_BadEnd: begin
  Result:= False;
  Break;
  end;
  end;
  end;
  end;
*)
procedure TAviconDataSource.TestBMOffsetFirst;
var
  I: Integer;

begin
  // Result:= False;

  FLastBMIdx:= 0;
  for I:= 0 to High(FBMItems) do
  begin
    if (FBody.Position < FBMItems[I].StSkipOff) then
      Break;
    if (FBody.Position >= FBMItems[I].StSkipOff) and
      (FBody.Position < FBMItems[I].OkOff) then
    begin
      FLastBMIdx:= I;
      Break;

      // FBody.Position:= FBMItems[I].OkOff;
      // FPack:= FBMItems[I].Pack;
      // Result:= True;

    end;
  end;

end;

function TAviconDataSource.TestBMOffsetNext: Boolean;
var
  I: Integer;

begin
  Result:= False;
  for I:= FLastBMIdx to High(FBMItems) do
  begin
    if (FBody.Position < FBMItems[I].StSkipOff) then
    begin
      FLastBMIdx:= I;
      Break;
    end;
    if (FBody.Position >= FBMItems[I].StSkipOff) and
      (FBody.Position < FBMItems[I].OkOff) then
    begin
      FLastBMIdx:= I;
      Result:= True;
      Break;
    end;
  end;
end;


function TAviconDataSource.FindBSState(StartDisCoord: Integer): Boolean;
begin
  if FCurDisCoord < FObmen1 then
  begin
//    FObmen2:= FPack;
    Result:= True;
  end else Result:= False;
end;

function TAviconDataSource.GetSysCoordByRealIdx(KmIdx, PkIdx: Integer; Metr: Extended): Integer;
begin
  case Header.MoveDir of
    0: begin
         if PkIdx < High(CoordList[KmIdx].Content)
           then Result:= Event[CoordList[KmIdx].Content[PkIdx + 1].Idx].SysCoord
           else if KmIdx < High(CoordList) then Result:= Event[CoordList[KmIdx + 1].Content[0].Idx].SysCoord
                                           else Result:= DisToSysCoord(MaxDisCoord) + 100 * (100000 - ExHeader.EndMRFCrd.mm) div Header.ScanStep;
         Result:= Result - Round(Metr * 1000 / (Header.ScanStep / 100));
       end;
    1: Result:= Event[CoordList[KmIdx].Content[PkIdx].Idx].SysCoord + Round((Metr - Ord((KmIdx = 0) and (PkIdx = 0)) * (Header.StartMetre + StartShift)) * 1000 / (Header.ScanStep / 100));

  end;
end;

function TAvk11DatSrc.GetSysCoordByReal(Km, Pk: Integer; Metr: Extended): Integer;
var
  I: Integer;
  J: Integer;

begin
  for I:= 0 to High(FCoordList) do
    if FCoordList[I].KM = Km then
      for J:= 0 to High(FCoordList[I].Content) do
        if FCoordList[I].Content[J].Pk = Pk then
        begin
          Result:= GetSysCoordByRealIdx(I, J, Metr);
          Exit;
        end;
end;

procedure TAviconDataSource.SetSkipBM(NewState: Boolean);
var
  I: Integer;
  J: Integer;
  K: Integer;
  L: Integer;
  Flag: Boolean;

  GetDisCoord: Int64;
  GetSysCoord: Integer;
  GetOffset: Integer;

  FBMItems_: array of TBMItem;
  ED_: array of TFileEvent;


begin
  if NewState and (not FSkipBM) then
  begin
//    for I:= 0 to High(FNotebook) do
//      FNotebook[I].SysCoord:= DisToSysCoord(FNotebook[I].DisCoord);

    SetLength(FSaveEventsData, Length(FEvents)); // ��������� �������
    Move(FEvents[0], FSaveEventsData[0], Length(FEvents) * SizeOf(TFileEvent));

    SetLength(ED_, Length(FEvents));
    Move(FEvents[0], ED_[0], Length(FEvents) * SizeOf(TFileEvent));

    Flag:= False;
    SetLength(FBMItems, 0);
    SetLength(FBMItems_, 0);

    SetLength(FInBMZone, Length(FEvents));
    for I:= 0 to High(FEvents) do
    begin
      if (not Flag) and (FEvents[I].ID = EID_BwdDir) then // ���� �������� �������� ����� - ���������� ��� ��� ��������
      begin
        Flag:= True;
        J:= FEvents[I].SysCoord;
        K:= FEvents[I].OffSet;
        L:= FEvents[I].DisCoord;
      end;

      if Flag and (FEvents[I].ID = EID_EndBM) then  // ���� �������� ����� ����������� - ��������� ������ � ���
      begin
        Flag:= False;
        SetLength(FBMItems_, Length(FBMItems_) + 1);
        FBMItems_[High(FBMItems_)].StSkipOff:= K;
        FBMItems_[High(FBMItems_)].OkOff:= FEvents[I].OffSet;
        FBMItems_[High(FBMItems_)].StDisCrd:= L;
        FBMItems_[High(FBMItems_)].EnDisCrd:= FEvents[I].DisCoord;

        FObmen1:= FEvents[I].DisCoord;
        LoadData(L, GetMaxDisCoord, 0, FindBSState);
//        FBMItems_[High(FBMItems_)].Pack:= FObmen2;
      end;

      if Flag then
      begin                                             // ���� �� � ���� �������� ����� - ����� ��� ��������� �� ���������� ��� ������
        ED_[I].SysCoord:= J;
        ED_[I].DisCoord:= J;
        FInBMZone[I]:= true;
      end
      else
      begin                                             // ���� �� �� � ���� �������� ����� - ����� ��� ������� ��������� �� �� ��������� ����������
        ED_[I].DisCoord:= ED_[I].SysCoord;
        FInBMZone[I]:= false;
      end
    end;

    if Flag then
    begin
      SetLength(FBMItems_, Length(FBMItems_) + 1);
      FBMItems_[High(FBMItems_)].StSkipOff:= K;
      FBMItems_[High(FBMItems_)].OkOff:= Header.TableLink;
      FBMItems_[High(FBMItems_)].StDisCrd:= L;
      FBMItems_[High(FBMItems_)].EnDisCrd:= MaxDisCoord;

      FObmen1:= Header.TableLink;
      LoadData(L, GetMaxDisCoord, 0, FindBSState);
//      FBMItems_[High(FBMItems_)].Pack:= FObmen2;
    end;

    SetLength(FNotebookInBMZone, Length(FNotebook));
    for I:= 0 to High(FNotebook) do
    begin
      FNotebookInBMZone[I]:= False;
      for J:= 0 to High(FBMItems_) do
        if (FNotebook[I].DisCoord >= FBMItems_[J].StDisCrd) and
           (FNotebook[I].DisCoord <= FBMItems_[J].EnDisCrd) then
        begin
          FNotebookInBMZone[I]:= True;
          Break;
        end;
    end;

    SetLength(FSaveGPSPoints, Length(GPSPoints)); // ��������� �������
    Move(GPSPoints[0], FSaveGPSPoints[0], Length(GPSPoints) * SizeOf(TGPSPoint));
    SetLength(GPSPoints, 0);
    for I:= 0 to High(FSaveGPSPoints) do
    begin
      Flag:= False;
      for J:= 0 to High(FBMItems_) do
        if (FSaveGPSPoints[I].DisCoord >= FBMItems_[J].StDisCrd) and
           (FSaveGPSPoints[I].DisCoord <= FBMItems_[J].EnDisCrd) then
        begin
          Flag:= True;
          Break;
        end;
      if not Flag then begin
        SetLength(GPSPoints, Length(GPSPoints) + 1);
        GPSPoints[High(GPSPoints)]:= FSaveGPSPoints[I];
        GPSPoints[High(GPSPoints)].DisCoord:= FSaveGPSPoints[I].SysCoord;
      end;
    end;


    SetLength(FBMItems, Length(FBMItems_));
    if Length(FBMItems) > 0 then
      Move(FBMItems_[0], FBMItems[0], Length(FBMItems) * SizeOf(TBMItem));
    SetLength(FBMItems_, 0);

    Move(ED_[0], FEvents[0], Length(FEvents) * SizeOf(TFileEvent));
    SetLength(ED_, 0);

    FillTimeList;

  end;

  if (not NewState) and FSkipBM then
  begin
    Move(FSaveEventsData[0], FEvents[0], Length(FSaveEventsData) * SizeOf(TFileEvent));
    SetLength(FSaveEventsData, 0);
    SetLength(FBMItems, 0);
    FillTimeList;
//    for I:= 0 to High(FNotebook) do
//    if FNotebook[I].DisCoord = - MaxInt then
//      FNotebook[I].DisCoord:= SysToDisCoord(FNotebook[I].SysCoord);
    SetLength(GPSPoints, Length(FSaveGPSPoints));
    Move(FSaveGPSPoints[0], GPSPoints[0], Length(FSaveGPSPoints) * SizeOf(TGPSPoint));
    SetLength(FSaveGPSPoints, 0);
  end;

  FSkipBM:= NewState;
end;

procedure TAviconDataSource.AutomaticSearchRes_SetSkipBM();
var
  I: Integer;
  J: Integer;
  Flag: Boolean;

begin
(*  for I:= 0 to High(FAutomaticSearchResList) do
  begin
    Flag:= False;
    for J:= 0 to High(FBMItems) do
      if (AutoSearchRes[I].CentrCoord >= FBMItems[J].StDisCrd) and
         (AutoSearchRes[I].CentrCoord <= FBMItems[J].EnDisCrd) then
      begin
        Flag:= True;
        Break;
      end;

    if not Flag then FAutomaticSearchResList[I].CentrCoord:= DisToSysCoord(AutoSearchRes[I].CentrCoord, True)
                else FAutomaticSearchResList[I].CentrCoord:= - 10000;
  end;*)
end;

// -------------< ������ � ������� >------------------------------------

function TAviconDataSource.NormalizeDisCoord(Src: Integer): Integer;
begin
  if Src < 0 then
    Result:= 0
  else if Src > MaxDisCoord then
    Result:= MaxDisCoord
  else
    Result:= Src;
end;

{
  function TAviconDataSource.GetBodyModify: Boolean;
  begin
  Result:= FExHeader.Modify = 0;
  end;

  procedure TAviconDataSource.SetBodyModify(NewState: Boolean);
  begin
  FModifyData:= True;
  case NewState of
  False: FExHeader.Modify:= $FF;
  True: FExHeader.Modify:= 0;
  end;
  end;

  procedure TAviconDataSource.SetPOVer(s: string);
  begin
  //  FExHeader.POVer[0]:= Chr(s[1]);
  //  FExHeader.POVer[1]:= Chr(s[2]);
  //  FExHeader.POVer[2]:= Chr(s[3]);
  //  FExHeader.POVer[3]:= Chr(s[4]);
  end;
}
procedure TAvk11DatSrc.FillTimeList;
var
  I: Integer;
  ID: Byte;
  OldH: Integer;
  OldM: Integer;
  pData: PEventData;

begin
  for I:= 0 to High(FEvents) do
  begin
    GetEventData(I, ID, pData);
    if ID in [EID_Time] then
    begin
      if (pedTime(pData)^.Hour and $E0 = 0) and

         ((I = 0) or
          (pedTime(pData)^.Hour <> OldH) or
          (pedTime(pData)^.Minute <> OldM)) then
      begin
        SetLength(FTimeList, Length(FTimeList) + 1);
        FTimeList[High(FTimeList)].H:= pedTime(pData)^.Hour;
        FTimeList[High(FTimeList)].M:= pedTime(pData)^.Minute;
        FTimeList[High(FTimeList)].DC:= FEvents[I].DisCoord;
        OldH:= pedTime(pData)^.Hour;
        OldM:= pedTime(pData)^.Minute;
      end;
    end;
  end;
end;

var
 QQ, LastI: Integer;

function TAviconDataSource.FillEventsData: Boolean;
const
  KuRek: array [0 .. 9] of Integer = (14, 14, 12, 12, 16, 16, 14, 14, 16, 16);

var
  I, J, K, L, Ch: Integer;
  DisList: TIntegerDynArray;
  ID: Byte;
  Params_: TMiasParams;
  HSItem: THSItem;
  Echo: array [1 .. 12] of Byte;
  pData: PEventData;
  R: TRail;
  Flag: Boolean;
  LastKm: Integer;
  LastPk: Integer;
  SkipBytes: Integer;
  ReAn: Boolean;
  SkipFlag: Boolean;

  // ������������ �������
  R_: TRail;
  Ch_: Integer;
  St_: Boolean;
//  AKState: TAKStateDataItemStates;
  MinDisCrd: Integer;

  CheckSum: Byte;
  CheckSumData: Byte;
  CheckSumLen: Integer;
  MRFCrdPrm: TMRFCrdParams;
  CaCrdPrm: TCaCrdParams;
  MRFCrd: TMRFCrd;
  CaCrd: TCaCrd;
  tmp: edAirBrush;
  EndOffset: Integer;
  I1, I2: Integer;
  Sensor1FirstFlag: array [TRail] of Boolean;
  tmp_: edAirBrush;
  TestDisCoord: Integer;
  Cnt1, Cnt2: Integer;
  Speed_: Integer;
  State_: Integer;
  Size_: Integer;
  MemStr: TMemoryStream;
  CurSysCoord: Integer;
  CurDisCoord: Integer;
  LastSysCoord: Integer;
  LoadID: Byte;
  LoadByte: array [1 .. 22] of Byte;
  LoadLongInt: array [1 .. 5] of Longint;

  tmpHeader: THSHead;
  tmpHeader_OLD: THSHead_OLD;
  tmpItem: THSItem;

  DataType_1_1: Boolean;
  DataType_1_2: Boolean;
  DataType_2_1: Boolean;
  DataType_2_2: Boolean;

  P: Integer;
  ChIndex: Integer;
  SummState: Boolean;
  LastSummState: Boolean;
  SaveParamsIndex: Integer;
  SaveOffset: Integer;

  EData: TEventData;
  CrdParams: TCrdParams;
  ver: Byte;
//  tmpimage: TPNGImage;
  ImageRect: TRect;
  bmp,bmp2: TBitmap;

function MMToSysCrd(MM: Integer): Integer; // �������� ���������� � ���
begin
  Result:= Round(MM / (Header.ScanStep / 100));
end;

begin
  Result:= True;
  QQ:= 1;
  if FFillEventsData then Exit;

//  {$IFDEF LOG}
//  AssignFile(FLog, 'Events.LOG');
//  Rewrite(FLog);
//  {$ENDIF}

  Sensor1FirstFlag[rLeft]:= True;
  Sensor1FirstFlag[rRight]:= True;

  // ������������ ������ ��������� �������� - ���������
  for R:= rLeft to rRight do
    for I:= MinEvalChannel to MaxEvalChannel do
      FillChar(Params_.Par[R, I].Ku, SizeOf(TEvalChannelParams), 0);

  for R:= rLeft to rRight do
    Params_.Sensor1[R]:= False;
  Params_.Mode:= 0;

  ReAn:= False;
  FMaxSysCoord:= - MaxInt;
  Flag:= False;
  FCanSkipBM:= False;

  DisToCoordParams(0, CrdParams);
  OperatorNameList.Add(Format('%s - %s / %s', [HeaderStrToString(FHead.OperatorName), GetTime(0), RealCrdToStr(CrdParamsToRealCrd(CrdParams, FHead.MoveDir), 1)]));
  CurrentOperatorName:= HeaderStrToString(FHead.OperatorName); // ����� ��� ��������� ������� ��������

  SaveParamsIndex:= -1;
  SetLength(FParams, Length(FEvents));
  for I:= 0 to High(FEvents) do
  begin
    FMaxSysCoord:= Max(FMaxSysCoord, FEvents[I].SysCoord);

                                          // ���������� ����������� �������
    if not (FEvents[I].ID in [EID_FwdDir, // ������ ������� ������
                              EID_LinkPt, // ����� ��� ����� ��������� � ������� � �����
                              EID_BwdDir, // ������ ������� �����
                              EID_EndBM,
                              EID_StartBadZone,
                              EID_EndBadZone]) then
    begin
      if (FEvents[I].ID = EID_HandScan) then  // �������� ������ ������� ��������
      begin
        SetLength(HSList, Length(HSList) + 1);
        HSList[High(HSList)].PrismDelayFlag:= False;
        FileBody.Position:= FEvents[I].OffSet + 1;
        SaveOffset:= FEvents[I].OffSet;
        with HSList[High(HSList)] do
        begin
          EventIdx:= I;
          DisCoord:= FEvents[I].DisCoord;
          SysCoord:= FEvents[I].DisCoord;

          begin
            P:= FBody.Position;
            FileBody.ReadBuffer(HSList[High(HSList)].Header, SizeOf(THSHead));

            if not ((HSList[High(HSList)].Header.DataSize > 0) and (HSList[High(HSList)].Header.DataSize <= 100000) and
                    (HSList[High(HSList)].Header.DelayMultiply in [1, 3]) and
                    (Integer(HSList[High(HSList)].Header.Rail) in [0, 1]) and
                    (HSList[High(HSList)].Header.ScanSurface in [0..9]) and
//                    (HSList[High(HSList)].Header.HandChNum in [0..7])) then
                    (Config.HandChanNumIsValid(HSList[High(HSList)].Header.HandChNum))) then
           begin
          //    FBody.Position:= SaveOffset + 1;
              SetLength(HSList, Length(HSList) - 1);
            end
            else
            begin
              FBody.Position:= P;
              FBody.ReadBuffer(tmpHeader, SizeOf(THSHead));
              FBody.Position:= FBody.Position + tmpHeader.DataSize - 3;
              FBody.ReadBuffer(tmpItem, 3);
              DataType_1_1:= (tmpItem.Sample = $FFFF) and (tmpItem.Count = 0);

              FBody.Position:= P;
              FBody.ReadBuffer(tmpHeader, SizeOf(THSHead));
              FBody.Position:= FBody.Position + tmpHeader.DataSize - 5;
              FBody.ReadBuffer(tmpItem, 5);
              DataType_1_2:= (tmpItem.Sample = $FFFF) and (tmpItem.Count = 1);

              FBody.Position:= P;
              FBody.ReadBuffer(tmpHeader_OLD, SizeOf(THSHead_OLD));
              FBody.Position:= FBody.Position + tmpHeader.DataSize - 3;
              FBody.ReadBuffer(tmpItem, 3);
              DataType_2_1:= (tmpItem.Sample = $FFFF) and (tmpItem.Count = 0);

              FBody.Position:= P;
              FBody.ReadBuffer(tmpHeader_OLD, SizeOf(THSHead_OLD));
              FBody.Position:= FBody.Position + tmpHeader.DataSize - 5;
              FBody.ReadBuffer(tmpItem, 5);
              DataType_2_2:= (tmpItem.Sample = $FFFF) and (tmpItem.Count = 1);

              FBody.Position:= P;
              if DataType_1_1 or DataType_1_2 then
              begin
                FBody.ReadBuffer(HSList[High(HSList)].Header, SizeOf(THSHead));
  //              FBody.Position:= FBody.Position + tmpHeader.DataSize;
              end
              else
   //           if DataType_2_1 or DataType_2_2 then
              begin
                FBody.ReadBuffer(HSList[High(HSList)].Header, SizeOf(THSHead_OLD));
                HSList[High(HSList)].Header.TVG:= 0;
  //              FBody.Position:= FBody.Position + tmpHeader.DataSize;
              end;

              repeat
                FileBody.ReadBuffer(HSItem, 3);

                if (HSItem.Sample = $FFFF) and (HSItem.Count = 1) then
                begin
                  FileBody.ReadBuffer(HSList[High(HSList)].PrismDelay, 2);
                  HSList[High(HSList)].PrismDelayFlag:= True;
                  Break;
                end;

                if (Length(Samples) = 0) or
                   (HSItem.Sample <> Samples[ High(Samples)].Idx) then SetLength(Samples, Length(Samples) + 1);

                Samples[ High(Samples)].Count:= 0;
                Samples[ High(Samples)].Idx:= HSItem.Sample;

                if HSItem.Count <= 4 then begin Cnt1:= HSItem.Count; Cnt2:= 0; end;
                if HSItem.Count > 4 then begin Cnt1:= 4; Cnt2:= HSItem.Count - 4; end;

            //    FileBody.ReadBuffer(Echo, 1);

                with Samples[ High(Samples)] do
                  case Cnt1 {HSItem.Count} of
                    1:begin
                        FileBody.ReadBuffer(Echo, 2);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[2] shr 4 and $0F;
                      end;
                    2:begin
                        FileBody.ReadBuffer(Echo, 3);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[3] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[2];
                        Ampl[Count]:= Echo[3] and $0F;
                      end;
                    3:begin
                        FileBody.ReadBuffer(Echo, 5);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[4] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[2];
                        Ampl[Count]:= Echo[4] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[3];
                        Ampl[Count]:= Echo[5] shr 4 and $0F;
                      end;
                    4:begin
                        FileBody.ReadBuffer(Echo, 6);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[5] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[2];
                        Ampl[Count]:= Echo[5] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[3];
                        Ampl[Count]:= Echo[6] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[4];
                        Ampl[Count]:= Echo[6] and $0F;
                      end;
                    5:begin
                        FileBody.ReadBuffer(Echo, 8);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[6] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[2];
                        Ampl[Count]:= Echo[6] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[3];
                        Ampl[Count]:= Echo[7] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[4];
                        Ampl[Count]:= Echo[7] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[5];
                        Ampl[Count]:= Echo[8] and $0F;
                      end;
                    6:begin
                        FileBody.ReadBuffer(Echo, 9);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[7] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[2];
                        Ampl[Count]:= Echo[7] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[3];
                        Ampl[Count]:= Echo[8] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[4];
                        Ampl[Count]:= Echo[8] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[5];
                        Ampl[Count]:= Echo[9] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[6];
                        Ampl[Count]:= Echo[9] and $0F;
                      end;
                    7:begin
                        FileBody.ReadBuffer(Echo, 11);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[8] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[2];
                        Ampl[Count]:= Echo[8] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[3];
                        Ampl[Count]:= Echo[9] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[4];
                        Ampl[Count]:= Echo[9] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[5];
                        Ampl[Count]:= Echo[10] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[6];
                        Ampl[Count]:= Echo[10] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[7];
                        Ampl[Count]:= Echo[11] and $0F;
                      end;
                    8:begin
                        FileBody.ReadBuffer(Echo, 12);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[9] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[2];
                        Ampl[Count]:= Echo[9] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[3];
                        Ampl[Count]:= Echo[10] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[4];
                        Ampl[Count]:= Echo[10] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[5];
                        Ampl[Count]:= Echo[11] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[6];
                        Ampl[Count]:= Echo[11] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[7];
                        Ampl[Count]:= Echo[12] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[8];
                        Ampl[Count]:= Echo[12] and $0F;
                      end;
                  end;

              //  if Cnt2 <> 0 then FileBody.ReadBuffer(Echo, 1);

                with Samples[ High(Samples)] do
                  case Cnt2 {HSItem.Count} of
                    1:begin
                        FileBody.ReadBuffer(Echo, 2);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[2] shr 4 and $0F;
                      end;
                    2:begin
                        FileBody.ReadBuffer(Echo, 3);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[3] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[2];
                        Ampl[Count]:= Echo[3] and $0F;
                      end;
                    3:begin
                        FileBody.ReadBuffer(Echo, 5);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[4] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[2];
                        Ampl[Count]:= Echo[4] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[3];
                        Ampl[Count]:= Echo[5] shr 4 and $0F;
                      end;
                    4:begin
                        FileBody.ReadBuffer(Echo, 6);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[5] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[2];
                        Ampl[Count]:= Echo[5] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[3];
                        Ampl[Count]:= Echo[6] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[4];
                        Ampl[Count]:= Echo[6] and $0F;
                      end;
                    5:begin
                        FileBody.ReadBuffer(Echo, 8);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[6] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[2];
                        Ampl[Count]:= Echo[6] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[3];
                        Ampl[Count]:= Echo[7] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[4];
                        Ampl[Count]:= Echo[7] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[5];
                        Ampl[Count]:= Echo[8] and $0F;
                      end;
                    6:begin
                        FileBody.ReadBuffer(Echo, 9);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[7] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[2];
                        Ampl[Count]:= Echo[7] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[3];
                        Ampl[Count]:= Echo[8] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[4];
                        Ampl[Count]:= Echo[8] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[5];
                        Ampl[Count]:= Echo[9] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[6];
                        Ampl[Count]:= Echo[9] and $0F;
                      end;
                    7:begin
                        FileBody.ReadBuffer(Echo, 11);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[8] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[2];
                        Ampl[Count]:= Echo[8] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[3];
                        Ampl[Count]:= Echo[9] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[4];
                        Ampl[Count]:= Echo[9] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[5];
                        Ampl[Count]:= Echo[10] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[6];
                        Ampl[Count]:= Echo[10] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[7];
                        Ampl[Count]:= Echo[11] and $0F;
                      end;
                    8:begin
                        FileBody.ReadBuffer(Echo, 12);
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[1];
                        Ampl[Count]:= Echo[9] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[2];
                        Ampl[Count]:= Echo[9] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[3];
                        Ampl[Count]:= Echo[10] shr 4 and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[4];
                        Ampl[Count]:= Echo[10] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[5];
                        Ampl[Count]:= Echo[11] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[6];
                        Ampl[Count]:= Echo[11] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[7];
                        Ampl[Count]:= Echo[12] and $0F;
                        if Count < 16 then Inc(Count);
                        Delay[Count]:= Echo[8];
                        Ampl[Count]:= Echo[12] and $0F;
                      end;
                  end;

              until HSItem.Sample = $FFFF;
            end;

//            Show-Message(IntToStr(FileBody.Position) + ' Size:' + IntToStr(FileBody.Size));
          end;
        end;
      end
      else
      if (FEvents[I].ID = EID_RailHeadScaner) then  // �������� ������ ������� ������� ������
      begin
        SetLength(HeadScanList, Length(HeadScanList) + 1);
        FileBody.Position:= FEvents[I].OffSet + 1;

        with HeadScanList[High(HeadScanList)] do
        begin

          Cross_Image:= nil;
          Vertically_Image:= nil;
          Horizontal_Image:= nil;
          Zero_Probe_Image:= nil;

          FBody.ReadBuffer(Size, 5);
          ver := byte(Rail) and $FC;
          Rail := TRail(byte(Rail) and $3);
          if ver < 50 then
          begin
            FBody.ReadBuffer(WorkSide, 1560 - 5); // ������ ������ ��� ��
            Zero_Probe_Section_Image_Size:= 0;
          end
          else if ver = 100 then FBody.ReadBuffer(WorkSide, 1560 - 5 + 4) // ����� ������ � ��
          else if ver = 104 then FBody.ReadBuffer(WorkSide, 2618 - 5 + 4) // + ������ ��� ���������
          else FBody.Position:= FBody.Position + Size - 1;
          DisCrd:= FEvents[I].DisCoord;
          GetTemperature(DisCrd, Temperature);
          FormatVer:= ver;
          EventIdx:= I;
          OperatorName:= CurrentOperatorName;

          if ((ver <= 104) and
              (HeadScanList[High(HeadScanList)].Size >= 0) and (HeadScanList[High(HeadScanList)].Size <= 100000) and //100K???
              (HeadScanList[High(HeadScanList)].WorkSide in [1, 2]) and
              (Integer(HeadScanList[High(HeadScanList)].Rail) in [0, 1]) and // 100, 101]) and
              (HeadScanList[High(HeadScanList)].OperatingTime < 2000) and
              (HeadScanList[High(HeadScanList)].ThresholdUnit in [1, 2])) then
          begin
          try
            if Cross_Cross_Section_Image_Size <> 0 then
            begin
              SetLength(Cross_Cross_Section_Image, Cross_Cross_Section_Image_Size);
              FBody.ReadBuffer(Cross_Cross_Section_Image[0], Cross_Cross_Section_Image_Size);
              Size_:= Cross_Cross_Section_Image_Size;
              MemStr:= TMemoryStream.Create;
              MemStr.SetSize(Size_);
              move(Cross_Cross_Section_Image[0], MemStr.Memory^, Size_);
              Cross_Image:= TPNGimage.Create;
              try
                Cross_Image.LoadFromStream(MemStr);
              except
                Cross_Image.Free;
                Cross_Image:= nil;
              end;
              MemStr.Free;
              SetLength(Cross_Cross_Section_Image, 0);
            end;

            if Vertically_Cross_Section_Image_Size <> 0 then //
            begin
              SetLength(Vertically_Cross_Section_Image, Vertically_Cross_Section_Image_Size);
              FBody.ReadBuffer(Vertically_Cross_Section_Image[0], Vertically_Cross_Section_Image_Size);
              Size_:= Vertically_Cross_Section_Image_Size;
              MemStr:= TMemoryStream.Create;
              MemStr.SetSize(Size_);
              move(Vertically_Cross_Section_Image[0], MemStr.Memory^, Size_);
              Vertically_Image:= TPNGimage.Create;
              try
                Vertically_Image.LoadFromStream(MemStr);
              except
                Vertically_Image.Free;
                Vertically_Image:= nil;
              end;
              if Assigned(Vertically_Image) then begin
//                if ver <= 104{100} then begin// �������� ������� ����� "����� 0����(���, ���) ����� ����"
                bmp := TBitmap.Create;
                bmp2:= TBitmap.Create;
                ImageRect:= Rect(0, 0, Vertically_Image.Width, Vertically_Image.Height - 18);
                bmp.Assign(Vertically_Image);
                bmp2.Height:= Vertically_Image.Height - 18;
                bmp2.Width:= Vertically_Image.Width;
                bmp2.Canvas.CopyRect(bmp2.Canvas.ClipRect, bmp.Canvas, ImageRect);
                Vertically_Image.Assign(bmp2);
                bmp.Free;
                bmp2.Free;
              end;
              MemStr.Free;
              SetLength(Vertically_Cross_Section_Image, 0);
            end;

            if Horizontal_Cross_Section_Image_Size <> 0 then
            begin
              SetLength(Horizontal_Cross_Section_Image, Horizontal_Cross_Section_Image_Size);
              FBody.ReadBuffer(Horizontal_Cross_Section_Image[0], Horizontal_Cross_Section_Image_Size);
              Size_:= Horizontal_Cross_Section_Image_Size;
              MemStr:= TMemoryStream.Create;
              MemStr.SetSize(Size_);
              move(Horizontal_Cross_Section_Image[0], MemStr.Memory^, Size_);
              Horizontal_Image:= TPNGimage.Create;
              try
                Horizontal_Image.LoadFromStream(MemStr);
              except
                Horizontal_Image.Free;
                Horizontal_Image:= nil;
              end;
           //   Horizontal_Image.SaveToFile('C:\temp\test3_' + IntToStr(I) + '.png');
              MemStr.Free;
              SetLength(Horizontal_Cross_Section_Image, 0);
            end;

            if ver > 50 then  // ����� ������ � ��
              if Zero_Probe_Section_Image_Size <> 0 then
              begin
                SetLength(Zero_Probe_Section_Image, Zero_Probe_Section_Image_Size);
                FBody.ReadBuffer(Zero_Probe_Section_Image[0], Zero_Probe_Section_Image_Size);
                Size_:= Zero_Probe_Section_Image_Size;
                MemStr:= TMemoryStream.Create;
                MemStr.SetSize(Size_);
                move(Zero_Probe_Section_Image[0], MemStr.Memory^, Size_);
                Zero_Probe_Image:= TPNGimage.Create;
                try
                  Zero_Probe_Image.LoadFromStream(MemStr);
                except
                  Zero_Probe_Image.Free;
                  Zero_Probe_Image:= nil;
                end;
                //Zero_Probe_Image.SaveToFile('C:\temp\test4_' + IntToStr(I) + '.png');
                MemStr.Free;
                SetLength(Zero_Probe_Section_Image, 0);
              end;
          except
              SetLength(Cross_Cross_Section_Image, 0);
              SetLength(Vertically_Cross_Section_Image, 0);
              SetLength(Horizontal_Cross_Section_Image, 0);
              SetLength(Zero_Probe_Section_Image, 0);
          end;

//            if Integer(Rail) > 50 then Rail:= TRail(Integer(Rail) - 100);
//            if Integer(Rail) > 50 then Rail:= TRail(Integer(Rail) - 100);

          end
          else
          begin
            SetLength(HeadScanList, Length(HeadScanList) - 1);
           // FBody.Position:= SaveOffset + 1;
          end;
        end;
      end
      else
      if (FEvents[I].ID = EID_AScanFrame) then  // �������� ������ �-���������
      begin
        SetLength(HSAScanList, Length(HSAScanList) + 1);
        FileBody.Position:= FEvents[I].OffSet + 1;
        with HSAScanList[High(HSAScanList)] do
        begin
          FBody.ReadBuffer(DataSize, 5);
          FBody.ReadBuffer(Rail, 10);

          if not ((HSAScanList[High(HSAScanList)].DataSize > 0) and (HSAScanList[High(HSAScanList)].DataSize <= 500000) and
                  (HSAScanList[High(HSAScanList)].DataType in [1,2]) and //= 1) and
                  (Ord(HSAScanList[High(HSAScanList)].Rail) in [0, 1]) and
                  (HSAScanList[High(HSAScanList)].Channel in [0..15, 255])) then
          begin
          //  FBody.Position:= SaveOffset + 1;
            SetLength(HSAScanList, Length(HSAScanList) - 1);
          end
          else
          begin
          if (HSAScanList[High(HSAScanList)].DataType = 1) then begin
            SetLength(ImageData, DataSize - 10);
            FBody.ReadBuffer(ImageData[0], DataSize - 10);
            MemStr:= TMemoryStream.Create;
            MemStr.SetSize(DataSize - 10);
            move(ImageData[0], MemStr.Memory^, DataSize - 10);
            Image:= TPNGimage.Create;
            Image.LoadFromStream(MemStr);

           // Image.SaveToFile('C:\temp\test_1' + IntToStr(I) + '.png');

            Image.Canvas.CopyRect(Rect(102, 20, 102 + 400 - 170, 65), Image.Canvas, Rect(170, 20, 400, 65)); // H
            Image.Canvas.CopyRect(Rect(102 + 336 - 170 + 1, 20, 102 + 336 - 170 + 1 + 718 - 409, 65), Image.Canvas, Rect(409, 20, 718, 65)); // Table
            Image.Canvas.CopyRect(Rect(102 + 336 - 170 + 718 - 409, 20, 102 + 336 - 170 + 718 - 409 + 1023 - 800, 65), Image.Canvas, // Att
                                  Rect(800, 20, 1023, 65));

  //          Image.SaveToFile('C:\temp\test_1.png');

            Image.Canvas.CopyRect(Rect(0, 0, 796 - 101, 525 - 20), Image.Canvas, Rect(101, 20, 796, 525)); // Cut signals image
//            Image.Resize(796 - 101, 525 - 20);
            SetLength(ImageData, 0);
            MemStr.Free;
          end
          else if (HSAScanList[High(HSAScanList)].DataType = 2) then begin
            FBody.ReadBuffer(StrokeDuration,1);
            SetLength(ImageData, DataSize - 11);
            FBody.ReadBuffer(ImageData[0], DataSize - 11);
            MemStr:= TMemoryStream.Create;
            MemStr.SetSize(DataSize - 11);
            move(ImageData[0], MemStr.Memory^, DataSize - 11);
            Image:= TPNGimage.Create;
            Image.LoadFromStream(MemStr);
            SetLength(ImageData, 0);
            MemStr.Free;
          end;
          end;
        end;
      end
      else                                   // �������� ������ �������
      begin
//        if FEvents[I].OffSet > 0 then
//        begin
          FBody.Position:= FEvents[I].OffSet + 1;
  //        QQ:= QQ + 1;
          if not LoadEventData(FEvents[I].ID, @FEvents[I].Data, 2048) then
          begin
            Result:= False;
            break;
          end;
//        end;
      end;
    end;

   // ------------------------------------- ������ ����������

    if (FEvents[I].ID = EID_ChangeOperatorName) then
    begin
      GetEventData(I, ID, pData);
      DisToCoordParams(FEvents[I].DisCoord, CrdParams);

      OperatorNameList.Add(Format('%s - %s / %s', [GetString(pedTextLabel(pData)^.Len, pedTextLabel(pData)^.Text), GetTime(FEvents[I].DisCoord), RealCrdToStr(CrdParamsToRealCrd(CrdParams, FHead.MoveDir), 1)]));
      CurrentOperatorName:= GetString(pedTextLabel(pData)^.Len, pedTextLabel(pData)^.Text); // ����� ��� ��������� ������� ��������
    end;

{
    if FEvents[I].Event.ID = EID_Mode then // ��������� ������
    begin
      if FEvents[I].Data[1] in [60 .. 66, 70 .. 76] then
        SkipFlag:= True;
      if FEvents[I].Data[1] in [1, 2, 7, 12, 20 .. 59] then
        SkipFlag:= False;
    end;
}
//    if (not SkipFlag) or (SkipFlag and not(FEvents[I].Event.ID in [EID_Sens,
  //      EID_Att, EID_VRU, EID_StStr, EID_EndStr, EID_2Tp])) then



//      if (FEvents[I].Data[2] >= MinEvalChannel) and
//         (FEvents[I].Data[2] <= MaxEvalChannel) then


    {  if FEvents[I].ID in [        EID_Ku,
                                  EID_Att,
                                  EID_TVG,
                                EID_StStr,
                               EID_EndStr,
                           EID_PrismDelay   ]  then

     }
//      with FEvents[I] do
//        if Data[2] > 64 then Show-Message('!!! ch');

//      with FEvents[I] do
//        if Data[1] > 1 then Show-Message('!!! r');

      with FEvents[I] do
        case ID of

           EID_Ku: if (Data[2] >= MinEvalChannel) and (Data[2] <= MaxEvalChannel) and
                      (Data[1] >= 0) and (Data[1] <= 1)
                       then Params_.Par[TRail(Data[1]), Data[2]].Ku:= ShortInt(Data[3]);
          EID_Att: if (Data[2] >= MinEvalChannel) and (Data[2] <= MaxEvalChannel) and
                      (Data[1] >= 0) and (Data[1] <= 1)
                       then Params_.Par[TRail(Data[1]), Data[2]].Att:= Data[3];
          EID_TVG: if (Data[2] >= MinEvalChannel) and (Data[2] <= MaxEvalChannel) and
                      (Data[1] >= 0) and (Data[1] <= 1)
                       then
                       begin
                         Params_.Par[TRail(Data[1]), Data[2]].TVG:= Data[3]; // ��� ����� ������-31 ���������� ��� ��� ������� ����
                         if isA31 then
                         begin
                            if Data[2] = 1 then Params_.Par[TRail(Data[1]), 0].TVG:= Data[3];
                            if Data[2] = 6 then Params_.Par[TRail(Data[1]), 8].TVG:= Data[3];
                            if Data[2] = 7 then Params_.Par[TRail(Data[1]), 9].TVG:= Data[3];
                         end else
                         if isA15New then // ��� ����� ������-15 ���������� ��� ��� ������� ����
                         begin
                            if Data[2] = 1 then Params_.Par[TRail(Data[1]), 0].TVG:= Data[3];
                            if Data[2] = 6 then Params_.Par[TRail(Data[1]), 8].TVG:= Data[3];
                            if Data[2] = 7 then Params_.Par[TRail(Data[1]), 9].TVG:= Data[3];
                         end else
                         if isFilus_X17DW then // ��� ����� ������-15 ���������� ��� ��� ������� ����
                         begin
                            if Data[2] = 1 then Params_.Par[TRail(Data[1]), 0].TVG:= Data[3];
                            if Data[2] = 6 then Params_.Par[TRail(Data[1]), 8].TVG:= Data[3];
                            if Data[2] = 7 then Params_.Par[TRail(Data[1]), 9].TVG:= Data[3];
                         end else
                         if isMDL then // ��� ����� ��� ���������� ��� ��� ������� ����
                         begin
                            if Data[2] = 1 then Params_.Par[TRail(Data[1]), 0].TVG:= Data[3];
                            if Data[2] = 6 then Params_.Par[TRail(Data[1]), 8].TVG:= Data[3];
                            if Data[2] = 7 then Params_.Par[TRail(Data[1]), 9].TVG:= Data[3];
                         end;

                       end;
        EID_StStr: if (Data[2] >= MinEvalChannel) and (Data[2] <= MaxEvalChannel) and
                      (Data[1] >= 0) and (Data[1] <= 1) then
                   begin
                     Params_.Par[TRail(Data[1]), Data[2]].StStr:= Data[3];
                     Params_.Par[TRail(Data[1]), Data[2]].StStr_mul_DelayMultiply:= Data[3] * Config.GetScanChByEvalChNum(Data[2]).DelayMultiply;
                   end;
       EID_EndStr: if (Data[2] >= MinEvalChannel) and (Data[2] <= MaxEvalChannel) and
                      (Data[1] >= 0) and (Data[1] <= 1) then
                   begin
//                     QQ:= QQ + 1;
                     Params_.Par[TRail(Data[1]), Data[2]].EndStr:= Data[3];
                     Params_.Par[TRail(Data[1]), Data[2]].EndStr_mul_DelayMultiply:= Data[3] * Config.GetScanChByEvalChNum(Data[2]).DelayMultiply;
                   end;
   EID_PrismDelay: if (Data[2] >= MinEvalChannel) and (Data[2] <= MaxEvalChannel) and
                      (Data[1] >= 0) and (Data[1] <= 1)
                       then
                       begin

                           // ��� ����� ������-31 ���������� ��� ��� ������� ����

                         Params_.Par[TRail(Data[1]), Data[2]].PrismDelay:= Data[3] + Data[4] * $100;
                         if isA31 then
                         begin
                            if Data[2] = 1 then Params_.Par[TRail(Data[1]), 0].PrismDelay:= Data[3] + Data[4] * $100;
                            if Data[2] = 6 then Params_.Par[TRail(Data[1]), 8].PrismDelay:= Data[3] + Data[4] * $100;
                            if Data[2] = 7 then Params_.Par[TRail(Data[1]), 9].PrismDelay:= Data[3] + Data[4] * $100;
                         end;
                         if isA15New then // ��� ����� ������-15 ���������� ��� ��� ������� ����
                         begin
                            if Data[2] = 1 then Params_.Par[TRail(Data[1]), 0].PrismDelay:= Data[3] + Data[4] * $100;
                            if Data[2] = 6 then Params_.Par[TRail(Data[1]), 8].PrismDelay:= Data[3] + Data[4] * $100;
                            if Data[2] = 7 then Params_.Par[TRail(Data[1]), 9].PrismDelay:= Data[3] + Data[4] * $100;
                         end;
                         if isFilus_X17DW then
                         begin
                            if Data[2] = 1 then Params_.Par[TRail(Data[1]), 0].PrismDelay:= Data[3] + Data[4] * $100;
                            if Data[2] = 6 then Params_.Par[TRail(Data[1]), 8].PrismDelay:= Data[3] + Data[4] * $100;
                            if Data[2] = 7 then Params_.Par[TRail(Data[1]), 9].PrismDelay:= Data[3] + Data[4] * $100;
                         end;
                         if isMDL then
                         begin
                            if Data[2] = 1 then Params_.Par[TRail(Data[1]), 0].PrismDelay:= Data[3] + Data[4] * $100;
                            if Data[2] = 6 then Params_.Par[TRail(Data[1]), 8].PrismDelay:= Data[3] + Data[4] * $100;
                            if Data[2] = 7 then Params_.Par[TRail(Data[1]), 9].PrismDelay:= Data[3] + Data[4] * $100;
                         end;
                       end;
         EID_Mode: begin
                     Params_.Mode:= Data[1] + Data[2] * $100;
                     Params_.Channel:= Boolean(Data[3]);            // ���� ������� � ��� ��� ���� ������� ���� � ����� ������
                     Params_.Rail:= TRail(Data[4]);                 // ����
                     Params_.EvalChNum:= Data[5];                   // ����� ������ ������
                     Params_.Time:= 0;
                     if SaveParamsIndex <> - 1 then FParams[SaveParamsIndex].Time:= Data[6] + Data[7] * $100;       // ����� ���������� � ���������� ������ (���)
                     SaveParamsIndex:= I;
//                     Params_.Time:= Data[6] + Data[7] * $100;       // ����� ���������� � ���������� ������ (���)
                   end;
      EID_Sensor1: begin
                     if Data[1] = 0 then R_:= rLeft else  R_:= rRight;
                     Sensor1DataExists[R_]:= True;
                     if (Sensor1FirstFlag[R_]) then
                     begin
                       StartFileSensor1[R_]:= not(Boolean(Data[2]));
                       Sensor1FirstFlag[R_]:= False;
                     end;

                     Params_.Sensor1[R_]:= Boolean(Data[2]);
//                     {$IFDEF LOG}
//                     Writeln(FLog, Format('State: %d; DisCoord: %d; Rail: %d', [Data[2], FEvents[I].DisCoord, Data[1]]) );
//                     if Data[2] = 0 then Writeln(FLog, '');
//                     {$ENDIF}
                   end;
   EID_Temperature:
                    FTemperatureExists:= True; // �����������

      EID_SensFail: ; //Show-Message('!');

    EID_DebugData : begin
//                      Show-Message('!'); // ���������� ����������
                   end;
  EID_PaintSystemParams:  begin
                            FPaintSystemParamsExists:= True;
                            Move(&Data[1], &FPaintSystemParams, 2048); // ��������� ������������
                          end;
//  EID_UMUPaintJob

          EID_Time: FTimeExists:= True; // ������� �������
    EID_SpeedState:  // �������� � ���������� �������� ��������
                    begin
                  //    GetEventData(I, ID, pData);
                  //    Speed_:= pedSpeedState(pData)^.Speed;
                  //    State_:= pedSpeedState(pData)^.State;
                      FSpeedExists:= True;
                    end;

EID_TestRecordFile: FTestRecordFileExists:= True; // ���� ������ ������������ ������

        end;

    if I > High(FParams) then ShowMessage(Format('Idx: %d; MaxIdx: %d; LastI: %d', [I, High(FParams), LastI]));

    FParams[I]:= Params_;
    LastI:= I;
  end;


  for I:= 0 to High(FEvents) do  // ���������� ������� ����� ������ ��������. ����������� ��� ����� ������ � ����� ������ ����� ����� ������
    if (FEvents[I].ID = EID_RailHeadScaner) and (I > 0) then
    begin
      for J := I - 1 downto 0 do
        if (FEvents[I].DisCoord = FEvents[J].DisCoord) and
           (FEvents[J].ID = EID_Mode) and
           (FEvents[J].Data[1] = 3) and
           (FEvents[J].Data[2] = 0) then FEvents[J].ID:= EID_SysCrd_NS;
    end;


//  {$IFDEF LOG}
//  System.CloseFile(FLog);
//  {$ENDIF}

  if not Flag then FCanSkipBM:= True;

  SetLength(FCDList, 0);               // ������ �������� ��������� ����������� ��������
  for I:= 0 to High(FEvents) do
  begin
    if (FEvents[I].ID = EID_FwdDir) or (FEvents[I].ID = EID_BwdDir) then
    begin
      SetLength(FCDList, Length(FCDList) + 1);
      FCDList[ High(FCDList)]:= I;
    end;
  end;
  SetLength(FCDList, Length(FCDList) + 1);
  FCDList[ High(FCDList)]:= High(FEvents);

  FillTimeList;

  for I:= 0 to High(FEvents) do
  begin
    // ------------------------------------- ���������� ������������

    if (FEvents[I].ID = EID_AirBrushTempOff) then
    begin
      GetEventData(I, ID, pData);

      if pedAirBrushTempOff(pData)^.State = 1 then
      begin
        if (Length(FAirBrushTempOff) = 0) or
           ((Length(FAirBrushTempOff) <> 0) and (FAirBrushTempOff[High(FAirBrushTempOff)].EndDisCoord <> - 1)) then
        begin
          SetLength(FAirBrushTempOff, Length(FAirBrushTempOff) + 1);
          FAirBrushTempOff[High(FAirBrushTempOff)].StartDisCoord:= FEvents[I].DisCoord;
          FAirBrushTempOff[High(FAirBrushTempOff)].EndDisCoord:= - 1;
        end;
      end;

      if pedAirBrushTempOff(pData)^.State = 0 then
      begin
        if (Length(FAirBrushTempOff) <> 0) then
          FAirBrushTempOff[High(FAirBrushTempOff)].EndDisCoord:= FEvents[I].DisCoord;
      end;
    end;

  end;

  BoltStykMarkExists:= False;

  for I:= 0 to High(FEvents) do
  begin
    SkipFlag:= False; // �������� �� �������� �� ������� � ���� ���������� ������������
    if (FEvents[I].ID = EID_AirBrush) then
    begin
      GetEventData(I, ID, pData);
      TestDisCoord:= pedAirBrush(pData)^.DisCoord;
    end;

    if (FEvents[I].ID = EID_AirBrush2) then
    begin
      GetEventData(I, ID, pData);
      TestDisCoord:= pedAirBrush2(pData)^.DisCoord;
    end;

    for J:= 0 to High(FAirBrushTempOff) do
      if (TestDisCoord >= FAirBrushTempOff[J].StartDisCoord) and
         (TestDisCoord <= FAirBrushTempOff[J].EndDisCoord) then
      begin
        SkipFlag:= True;
        Break;
      end;

    // ------------------------------------- // ������ ������� ������������

    if (FEvents[I].ID = EID_AirBrush) then
    begin
      GetEventData(I, ID, pData);
      if pedAirBrush(pData)^.DisCoord = - 1000000 then // HC data
      begin
        tmp:= pedAirBrush(pData)^;
        SetLength(FHCList, Length(FHCList) + 1);
        FHCList[High(FHCList)].Ch:= tmp.Items[0].ScanCh;
        FHCList[High(FHCList)].Rail:= tmp.Rail;
        FHCList[High(FHCList)].StartDisCoord:= tmp.Items[0].StartDisCoord;
        FHCList[High(FHCList)].EndDisCoord:= tmp.Items[0].EndDisCoord;
        FHCList[High(FHCList)].Valut:= Round(PSmallint(@tmp.Items[0].StartDelay)^);
      end
      else
      if pedAirBrush(pData)^.Count <> 0 then   // ��� ����������� ���������� ������� ������������ �� ������
      begin
        SetLength(FAirBrushList, Length(FAirBrushList) + 1);
//        SetLength(FAirBrushList_, Length(FAirBrushList_) + 1);
//        FAirBrushList_[High(FAirBrushList_)].X:= FEvents[I].DisCoord;
//        FAirBrushList_[High(FAirBrushList_)].Y:= Ord(SkipFlag);

        tmp_:= pedAirBrush(pData)^;                          // �������������� �� ������� ������� � ����� ������
        FAirBrushList[High(FAirBrushList)].PaintDisCoord:= pedAirBrush2(pData)^.DisCoord + MMToSysCrd(AirBrushPos + FPaintSystemParams.AirBrush_Shift);
        FAirBrushList[High(FAirBrushList)].SendCmdDisCoord:= FEvents[I].DisCoord;
        FAirBrushList[High(FAirBrushList)].Skip:= SkipFlag;
        FAirBrushList[High(FAirBrushList)].Data.Rail:= tmp_.Rail;
        FAirBrushList[High(FAirBrushList)].Data.DisCoord:= tmp_.DisCoord;
        FAirBrushList[High(FAirBrushList)].Data.Count:= tmp_.Count;

        for J:= 0 to 15 do
        begin
          FAirBrushList[High(FAirBrushList)].Data.Items[J].ScanCh:= tmp_.Items[J].ScanCh;
          FAirBrushList[High(FAirBrushList)].Data.Items[J].StartDisCoord:= tmp_.Items[J].StartDisCoord;
          FAirBrushList[High(FAirBrushList)].Data.Items[J].EndDisCoord:= tmp_.Items[J].EndDisCoord;
          FAirBrushList[High(FAirBrushList)].Data.Items[J].StartDelay:= tmp_.Items[J].StartDelay;
          FAirBrushList[High(FAirBrushList)].Data.Items[J].EndDelay:= tmp_.Items[J].EndDelay;
          FAirBrushList[High(FAirBrushList)].Data.Items[J].Debug:= - 1;
        end;
      end;
    end;

    // ������ 2

    if (FEvents[I].ID = EID_AirBrush2) then
    begin
      GetEventData(I, ID, pData);
      if pedAirBrush2(pData)^.Count <> 0 then   // ��� ����������� ���������� ������� ������������ �� ������
      begin
        SetLength(FAirBrushList, Length(FAirBrushList) + 1);
        FAirBrushList[High(FAirBrushList)].Data:= pedAirBrush2(pData)^;
        FAirBrushList[High(FAirBrushList)].PaintDisCoord:= pedAirBrush2(pData)^.DisCoord + MMToSysCrd(AirBrushPos + FPaintSystemParams.AirBrush_Shift);
        FAirBrushList[High(FAirBrushList)].SendCmdDisCoord:= FEvents[I].DisCoord;
        FAirBrushList[High(FAirBrushList)].Skip:= SkipFlag;
	    end;
    end;

    // ------------------------------------ ������ �������� ���� ��������� �� ���
(*
    if (FEvents[I].ID = EID_AutomaticSearchRes) then
    begin
      GetEventData(I, ID, pData);

      if (pedAutomaticSearchRes(pData)^.CentrCoord < 0) or
         (pedAutomaticSearchRes(pData)^.CentrCoord > GetMaxDisCoord) then
      begin
  //      J:= Length(FAutomaticSearchResList);
      end
      else
      begin
        J:= Length(FAutomaticSearchResList);
        SetLength(FAutomaticSearchResList, J + 1);
        FAutomaticSearchResList[J]:= pedAutomaticSearchRes(pData)^;
                     // ������� ������ �������� �������� ������� �� ���������� ������ �� ���������� ���������������� ���������� ��� �������� �����
                     // ����� ����������� ������������� ���� ���������
//        L:= 0;
//        for K := 0 to High(FCDList) do
//          if FEvents[FCDList[K]].DisCoord > FAutomaticSearchResList[J].CentrCoord then Break else Inc(L);
//
//        FAutomaticSearchResList[J].CentrCoord:= FAutomaticSearchResList[J].CentrCoord - Round((L - 1) * 2.2); // 1.8

        if FAutomaticSearchResList[J].ScanCh = 0 then FAutomaticSearchResList[J].ScanCh:= 1;
      end;

    (*  DisToDisCoords(SysToDisCoord(FAutomaticSearchResList[J].CentrCoord), DisList);
      for K := High(DisList) downto 0 do if DisList[K] < Event[K].DisCoord then
      begin
        FAutomaticSearchResList[J].CentrCoord:= DisList[K];
        Break;
      end; * )
    end;
    *)

    // ------------------------------------- ������ ����������
{
    if (FEvents[I].ID = EID_ChangeOperatorName) then
    begin
      GetEventData(I, ID, pData);
      DisToCoordParams(FEvents[I].DisCoord, CrdParams);

      OperatorNameList.Add(Format('%s - %s / %s', [GetString(pedTextLabel(pData)^.Len, pedTextLabel(pData)^.Text), GetTime(FEvents[I].DisCoord), RealCrdToStr(CrdParamsToRealCrd(CrdParams, FHead.MoveDir), 1)]));
    end;
}
    if (FEvents[I].ID = EID_StBoltStyk) or
       (FEvents[I].ID = EID_EndBoltStyk) then
    begin
      if not BoltStykMarkExists then BoltStykMark_MinDS:= FEvents[I].DisCoord;
      BoltStykMark_MaxDS:= FEvents[I].DisCoord;
      BoltStykMarkExists:= True;
    end;

    // ------------------------------------- ������ ���

    if (FEvents[I].ID = EID_DebugData) then
    begin
      GetEventData(I, ID, pData);
      if pedDebugData(pData)^.Data[0] = 1 then // ���
      begin
        SetLength(FAlarmList, Length(FAlarmList) + 1);

        FAlarmList[High(FAlarmList)].Rail:= TRail(pedDebugData(pData)^.Data[1]);
        FAlarmList[High(FAlarmList)].EvalCh:= EvalChToScanCh[pedDebugData(pData)^.Data[2]];
        FAlarmList[High(FAlarmList)].DisCoord:= FEvents[I].DisCoord;
        FAlarmList[High(FAlarmList)].StDisCrd:= pedDebugData(pData)^.Data[3];
        FAlarmList[High(FAlarmList)].EdDisCrd:= pedDebugData(pData)^.Data[4];
      end;
    end;

    // ------------------------------------- ������ ����������� �������
    if (FEvents[I].ID = EID_LongLabel) then
    begin
      GetEventData(I, ID, pData);
      SetLength(FLongLabelList, Length(FLongLabelList) + 1);
      FLongLabelList[High(FLongLabelList)]:= pLongLabel(pData)^;
    end;

    // ------------------------------------- �����������
    if (FEvents[I].ID = EID_Media) then
    begin
      GetEventData(I, ID, pData);
      SetLength(FMediaList, Length(FMediaList) + 1);
      FMediaList[High(FMediaList)].DataType:= pedMediaData(pData)^.DataType;

      if pedMediaData(pData)^.DataType in [1, 2, 3] then
        if (FEvents[I].OffSet + 6 + pedMediaData(pData)^.Size <= FBody.Size) then
        begin
          FMediaList[High(FMediaList)].Data:= TMemoryStream.Create();
          FMediaList[High(FMediaList)].EventIdx:= I;
          FileBody.Position:= FEvents[I].OffSet + 6;

          {$IFDEF MY_DATA_STREAM}
          FMediaList[High(FMediaList)].Data.SetSize(pedMediaData(pData)^.Size);
          FileBody.ReadBuffer(FMediaList[High(FMediaList)].Data.Memory^, pedMediaData(pData)^.Size);
          {$ENDIF}
          {$IFNDEF MY_DATA_STREAM}
          FMediaList[High(FMediaList)].Data.CopyFrom(FileBody, pedMediaData(pData)^.Size);
          {$ENDIF}


        end;

      {$IFDEF GENERATE_GLOBAL_LOG}
      FGLog.Add(FFileName + ' ' + IntToStr(pedMediaData(pData)^.DataType));
      {$ENDIF}
    end;


    // ------------------------------------- // ������ ����� ������� ������ �������� 0 ����
   // EID_ZerroProbMode

  end;





  // ------------------------------------- // ������ ����� ����������� �����
(*
  for I:= 0 to High(FEvents) do
  begin
    if (FEvents[I].ID = EID_CheckSum) then
    begin
      GetEventData(I, ID, pData);
      CheckSumLen:= FEvents[I].OffSet - pedCheckSum(pData)^.Offset;
      FileBody.Position:= pedCheckSum(pData)^.Offset;
      CheckSum:= 0;
      {$OVERFLOWCHECKS OFF}
      {$RANGECHECKS OFF}
      for J:= 0 to CheckSumLen - 1 do
      begin
        FileBody.ReadBuffer(CheckSumData, 1);
        CheckSum:= CheckSum + CheckSumData;
      end;
      {$RANGECHECKS ON}
      {$OVERFLOWCHECKS ON}
      if pedCheckSum(pData)^.Checksum <> CheckSum then Show-Message('!');
    end;
  end;
 *)
  // ----------------------------------------------------------------------------------

  SetLength(FCoordList, 1);
  FCoordList[0].KM:= Header.StartKM;
  with FCoordList[High(FCoordList)] do
  begin
    SetLength(Content, Length(Content) + 1);
    Content[High(Content)].Pk:= Header.StartPk;
    Content[High(Content)].Idx:= 0;
    LastKm:= High(FCoordList);
    LastPk:= High(Content);
  end;

  LastKm:= 0;
  LastPk:= 0;

  for I:= 0 to EventCount - 1 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_Stolb then
    begin
      if pCoordPost(pData)^.Km[0] <> pCoordPost(pData)^.Km[1] then
      begin
        SetLength(FCoordList, Length(FCoordList) + 1);
        FCoordList[High(FCoordList)].KM:= pCoordPost(pData)^.Km[1];
        with FCoordList[High(FCoordList)] do
        begin
          SetLength(Content, Length(Content) + 1);
          if Header.MoveDir < 0 then Content[High(Content)].Pk:= 10
                                else Content[High(Content)].Pk:= 1;
          Content[High(Content)].Idx:= I;
          FCoordList[LastKm].Content[LastPk].Len:= Round((Event[I].SysCoord - Event[FCoordList[LastKm].Content[LastPk].Idx].SysCoord) / 100 * Header.ScanStep);
          LastKm:= High(FCoordList);
          LastPk:= High(Content);
        end;
      end;
      if pCoordPost(pData)^.Km[0] = pCoordPost(pData)^.Km[1] then
        with FCoordList[High(FCoordList)] do
        begin
          SetLength(Content, Length(Content) + 1);
          Content[High(Content)].Pk:= pCoordPost(pData)^.Pk[1];
          Content[High(Content)].Idx:= I;
          FCoordList[LastKm].Content[LastPk].Len:= Round((Event[I].SysCoord - Event[FCoordList[LastKm].Content[LastPk].Idx].SysCoord) / 100 * Header.ScanStep);
          LastKm:= High(FCoordList);
          LastPk:= High(Content);
        end;
    end;
  end;


{  Z1:= DisToSysCoord(MaxDisCoord);
  Z2:= Event[FCoordList[LastKm].Content[LastPk].Idx].SysCoord;
  Z3:= Round((Z1 - Z2) / 100 * Header.ScanStep);
  FCoordList[LastKm].Content[LastPk].Len:= Z3;   }

  FCoordList[LastKm].Content[LastPk].Len:= Round((DisToSysCoord(MaxDisCoord) - Event[FCoordList[LastKm].Content[LastPk].Idx].SysCoord) / 100 * Header.ScanStep);

  SetLength(LabelList, 0);
  for I:= 0 to EventCount - 1 do
  begin
    if Event[I].ID in [EID_Stolb, EID_StolbChainage, EID_Switch, EID_DefLabel, EID_TextLabel, EID_AirBrushTempOff, EID_ZerroProbMode, EID_AlarmTempOff, EID_Media] then
    begin
      SetLength(LabelList, Length(LabelList) + 1);
      LabelList[High(LabelList)]:= I;
    end;

    EData:= Event[I].Data;
    if (Event[I].ID = EID_MediumDate) and (pedMediumDate(@EData)^.DataType in [2, 3]) then
    begin
      SetLength(LabelList, Length(LabelList) + 1);
      LabelList[High(LabelList)]:= I;
    end;
  end;

  // ���� ������������ ������

  for I:= 0 to EventCount - 1 do
    if Event[I].ID = EID_TestRecordFile then
    begin
      FTestRecordFileOffset:= Event[I].OffSet + 1;
      FileBody.Position:= FTestRecordFileOffset;
      FileBody.ReadBuffer(FTestRecordFileSize, 4);
      FTestRecordFileExists:= True;
    end;

  if ReAn then
  begin
    ReAnalyze;
//    FillEventsData;
  end; (*
  else
  if FNewFileFormat then // ��� ������: ������-31; ������-15 (Nautiz X8); Filus-X111W
  begin

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///   �������� ������ ��  - �������� ����� � ����������� ������ ��� ���������� �� � ������ ����������� ������������� � 70 ��  ///
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    if not FFullHeader then FBody.Position:= 17
                       else FBody.Position:= SizeOf(TFileHeader); // ������� ���������

    CurSysCoord:= 0;
    CurDisCoord:= 0;
    LastSysCoord:= MaxInt;

    SetLength(AKStateData, 1); // ��������� ��������� ������ - �� ����
    AKStateData[0].SummState:= False;
    AKStateData[0].DisCrd:= - 1;
    for R:= rLeft to rRight do
      for ChIndex:= 0 to 15 do
        AKStateData[0].State[R][ChIndex]:= False;

    if FBody.Position < FDataSize then
    try
      repeat
        FBody.ReadBuffer(LoadID, 1); // �������� �������������� �������

        case LoadID of

           EID_ACData:    begin
                            FBody.ReadBuffer(LoadByte[1], 1);

                            R_:= TRail(LoadByte[1] shr 6 and 1);
                            Ch_:= (LoadByte[1] shr 2) and 15;
                            St_:= Boolean(LoadByte[1] shr 7 and 1);
                            if (Ch_ < 0) or (Ch_ > 16) then Break; // !!!!
                            AKState[R_][Ch_]:= St_;

                            if (CurDisCoord > AKStateData[High(AKStateData)].DisCrd) then
                            begin
                              SetLength(AKStateData, Length(AKStateData) + 1);
                              AKStateData[High(AKStateData)].DisCrd:= CurDisCoord;
                            end;

                            AKStateData[High(AKStateData)].State:= AKState;
                            AKStateData[High(AKStateData)].SummState:= True;
                            for R:= rLeft to rRight do
                              for ChIndex:= 0 to 15 do
                                if FChFileIdxMask[ChIndex] then
                                  AKStateData[High(AKStateData)].SummState:= AKStateData[High(AKStateData)].SummState and AKState[R][ChIndex];
                          end;
            EID_SysCrd_SS:  begin // - �������� ������, �������� ����������
                              FBody.ReadBuffer(LoadByte[1], 2);
                              // if not TestOffset(FBody.Position - 3 - LoadByte[1]) then raise EBadLink.Create('');
                              CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[2]);
                            end;
            EID_SysCrd_SF:  begin // - �������� ������, ������ ����������
                              FBody.ReadBuffer(LoadByte[1], 1);
                              // if not TestOffset(FBody.Position - 2 - LoadByte[1]) then raise EBadLink.Create('');
                              FBody.ReadBuffer(LoadLongInt, 4);
                              CurSysCoord:= LoadLongInt[1];
                            end;
            EID_SysCrd_FS:  begin // - ������ ������, �������� ����������
                              FBody.ReadBuffer(LoadLongInt[1], 4);
                              FBody.ReadBuffer(LoadByte[1], 1);
                              // if not TestOffset(FBody.Position - 6 - LoadLongInt[1]) then raise EBadLink.Create('');
                              CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[1]);
                            end;
            EID_SysCrd_FF:  begin // - ������ ������, ������ ����������
                              FBody.ReadBuffer(LoadLongInt[1], 4);
                              FBody.ReadBuffer(LoadLongInt[2], 4);
                              // if not TestOffset(FBody.Position - 9 - LoadLongInt[1]) then raise EBadLink.Create('');
                              CurSysCoord:= LoadLongInt[2];
                            end;
            EID_SysCrd_NS:  begin // �������� ����������
                              FBody.ReadBuffer(LoadByte[1], 1);
                              CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[1]);
                            end;
            EID_SysCrd_NF:  begin // - ������ ����������
                              FBody.ReadBuffer(LoadLongInt[2], 4);
                              CurSysCoord:= LoadLongInt[2];
                            end;
            EID_EndFile:    begin // ����� �����
                              FBody.ReadBuffer(LoadByte, 9 + 4);
                              Break;
                            end;

          else SkipEvent(LoadID);
        end;

        if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF, EID_SysCrd_NS, EID_SysCrd_NF] then // ��������� ����������
        begin

          if LastSysCoord <> MaxInt then // ���� ��� �� ������ ���������� ����� ...
            Inc(CurDisCoord, Abs(CurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
          LastSysCoord:= CurSysCoord;
        end;


      until FBody.Position + 1 > FDataSize;

      except
      end;

      // ������������ ������ ��������������������� �������� �� ������ ��

      LastSummState:= False;
      SetLength(UCSData, 1);
      UCSData[0].X:= 0;
      for I:= 0 to High(AKStateData) do
      begin
        if (not LastSummState) and (AKStateData[I].SummState) then
        begin
          if AKStateData[I].DisCrd <> UCSData[High(UCSData)].X then UCSData[High(UCSData)].Y:= AKStateData[I].DisCrd
                                                               else SetLength(UCSData,Length(UCSData) - 1);
        end
        else
        if (LastSummState) and (not AKStateData[I].SummState) then
        begin
          SetLength(UCSData, Length(UCSData) + 1);
          UCSData[High(UCSData)].X:= AKStateData[I].DisCrd;
        end;
        LastSummState:= AKStateData[I].SummState;
      end;

    /////////////////////////////////////////////////////////////////////////////////
  end;
*)
  FFillEventsData:= true;
end;

procedure TAviconDataSource.DisToFileOffset(NeedDisCoord: Integer; var DisCoord: Int64; var SysCoord: Integer; var OffSet: Integer);
var
  R: TRail;
  I: Integer;
  LastSysCoord: Integer;
  SaveOffset: Integer;
  SkipBytes: Integer;
  CurSysCoord: Integer;
  CurDisCoord: Int64;
  LoadID: Byte;
  LoadByte: array [1 .. 22] of Byte;
  LoadLongInt: array [1 .. 5] of Longint; // absolute LoadByte[1];
  HSHead: THSHead;
  HSItem: THSItem;
  StartIdx, EndIdx: Integer;
  DisCoord_: Integer;
  SysCoord_: Integer;
  Offset_: Integer;
  CurDisCoord_: Integer;
  CurSysCoord_: Integer;
  CurPosition_: Integer;
//  BSHead: mBSAmplHead;

  SameCoord: TIntegerDynArray;

  BMSkipFlag: Boolean;
  TMP: Boolean;
  BodyBadZones_idx: Integer;

begin
  // ClearLog;
  NeedDisCoord:= NormalizeDisCoord(NeedDisCoord);
  // GetEventIdx(NeedDisCoord, NeedDisCoord, StartIdx, EndIdx);

  // ���� �� ���������� ������ � �����
  // ��� ����� - ������� ��������� ������� � ������ ��� ������� �����������
  // ToLog(Format('NeedDisCoord: %d', [NeedDisCoord]));                              // ----------------------------------------------------------------------------------------
  StartIdx:= GetLeftEventIdx(NeedDisCoord);

  I:= FEvents[StartIdx].DisCoord;
  while (I = FEvents[StartIdx].DisCoord) and (StartIdx <> 0) do Dec(StartIdx);
  {
    for I:= 1 to 5 do                               // ������ ����� �� ������� � ������ ��������
    begin
    if (StartIdx <> 0) and
    ((FEvents[StartIdx].Event.Pack[rLeft].Ampl = $FF) or
    (FEvents[StartIdx].Event.Pack[rRight].Ampl = $FF)) then Dec(StartIdx)
    else Break;
    end; }
  // ToLog(Format('Event Idx: %d', [StartIdx]));                              // ----------------------------------------------------------------------------------------

  (*
    // ���� ����� ������ ������ ������ ������ ��� �������� ����������
    // ��� ����� ����������� ��� ������� � ������ �����������
    while (NeedDisCoord = FEvents[StartIdx].Event.DisCoord) and (StartIdx <> 0) do Dec(StartIdx);
    I:= FEvents[StartIdx].Event.DisCoord;
    while (I = FEvents[StartIdx].Event.DisCoord) and (StartIdx <> 0) do Dec(StartIdx);
  *)
  // StartIdx:= GetLeftEventIdx(NeedDisCoord - 1);

  // if StartIdx > 0 then Dec(StartIdx);
  // if StartIdx > 0 then Dec(StartIdx);

  // GetNearestEventIdx(NeedDisCoord, StartIdx, EndIdx, SameCoord);
  // if Length(SameCoord) <> 0 then StartIdx:= SameCoord[0];

  if StartIdx = -1 then
  begin
    DisCoord:= 0;
    SysCoord:= 0;
    OffSet:= 0;
    Exit;
  end;

  // StartIdx:= GetLeftEventIdx(0);

  FBody.Position:= FEvents[StartIdx].OffSet;
  LastSysCoord:= FEvents[StartIdx].SysCoord;
  CurSysCoord:= FEvents[StartIdx].SysCoord;
  CurDisCoord:= FEvents[StartIdx].DisCoord;
  // FPack:= FEvents[StartIdx].Event.Pack;

  // ToLog(Format('Start Event: %d', [StartIdx]));                              // ----------------------------------------------------------------------------------------
  // ToLog(Format('Start State Rail: 0, BS: %d', [Ord(FPack[r_Left].Ampl <> 255)]));  // ----------------------------------------------------------------------------------------
  // ToLog(Format('Start State Rail: 1, BS: %d', [Ord(FPack[r_Right].Ampl <> 255)])); // ----------------------------------------------------------------------------------------

  FillChar(FCurEcho, SizeOf(TCurEcho), 0);

  if CurDisCoord >= NeedDisCoord then
  begin
    DisCoord:= CurDisCoord;
    SysCoord:= CurSysCoord;
    OffSet:= FBody.Position;
    Exit;
  end;

//  TestOffsetFirst;
  if FSkipBM then TestBMOffsetFirst;
{
  for R:= rLeft to rRight do
  begin
    FCurEcho[R, 1].Count:= Ord(FPack[R].Ampl <> 255);
    FCurEcho[R, 1].Delay[1]:= FPack[R].Delay;
    FCurEcho[R, 1].Ampl[1]:= FPack[R].Ampl;
  end;
}

  BodyBadZones_idx:= -1;
  for I := 0 to High(FBodyBadZones) do
  begin
    if FBody.Position < FBodyBadZones[I].From then
    begin
      BodyBadZones_idx:= I;
      Break;
    end
    else
    if (FBody.Position >= FBodyBadZones[I].From) and
       (FBody.Position <= FBodyBadZones[I].To_) then
    begin
      FBody.Position:= FBodyBadZones[I].To_;
      BodyBadZones_idx:= I + 1;
      Break;
    end;
  end;
  if Length(FBodyBadZones) <> 0 then
    if BodyBadZones_idx > High(FBodyBadZones) then
      BodyBadZones_idx:= -1;

  BMSkipFlag:= False;
  repeat
//    if not TestOffsetNext then Exit;
    if FSkipBM then
    begin
      TMP:= BMSkipFlag;
      BMSkipFlag:= TestBMOffsetNext;
      if TMP and (not BMSkipFlag) then
        for R:= rLeft to rRight do
          FCurEcho[R, 1].Count:= 0;
    end;

    SaveOffset:= FBody.Position;

    CurDisCoord_:= CurDisCoord;
    CurSysCoord_:= CurSysCoord;
    CurPosition_:= FBody.Position;

    if BodyBadZones_idx <> - 1 then
    begin
      if (FBody.Position >= FBodyBadZones[BodyBadZones_idx].From) and
         (FBody.Position < FBodyBadZones[BodyBadZones_idx].To_) then
      begin
        FBody.Position:= FBodyBadZones[BodyBadZones_idx].To_;
        BodyBadZones_idx:= I + 1;
        if BodyBadZones_idx > High(FBodyBadZones) then BodyBadZones_idx:= -1;
      end;
    end;

    FBody.ReadBuffer(LoadID, SizeOf(LoadID));
(*    SkipBytes:= -1;
    if LoadID and 128 = 0 then
    begin
      if LoadID shr 3 and $07 = 1 then // ���� ������ ����� �� �������������
      begin
        FSrcDat[0]:= LoadID;
        FBody.ReadBuffer(FSrcDat[1], Avk11EchoLen[LoadID and $07]);
        DataToEcho;
      end
      else
      begin
        if LoadID shr 3 and $07 = 0 then
          SkipBytes:= 1 // ���� ������� �� ���������� 1 ����
        else
          SkipBytes:= Avk11EchoLen[LoadID and $07]; // ����� ����������
      end
    end
    else
    begin
      if not BMSkipFlag then
          *)
      begin
        case LoadID of
                         (*
          EID_HandScan:
            begin // ������
              FBody.ReadBuffer(HSHead, 6);
              repeat
                FBody.ReadBuffer(HSItem, 3);
                if HSItem.Count <= 4 then
                  FBody.ReadBuffer(HSItem.Data[0], Avk11EchoLen[HSItem.Count]);
              until HSItem.Sample = $FFFF;
            end;

          EID_NewHeader:
            begin
              FBody.ReadBuffer(SkipBytes, 4);
              FBody.Position:= FBody.Position + SkipBytes;
              SkipBytes:= -1;
            end;
          {
            EID_BSAmpl:    begin
            FBody.ReadBuffer(BSHead, SizeOf(mBSAmplHead));
            FBody.Position:= FBody.Position + BSHead.Len * 2;
            SkipBytes:= - 1;
            end;
          }
          EID_ACLeft, EID_ACRight, EID_Sens, EID_Att, EID_VRU, EID_StStr,
            EID_EndStr, EID_HeadPh, EID_Mode, EID_2Tp_Word, EID_2Tp,
            EID_ZondImp, EID_SetRailType, EID_Stolb, EID_Strelka, EID_DefLabel,
            EID_TextLabel, EID_StBoltStyk, EID_EndBoltStyk, EID_Time,
            EID_StLineLabel, EID_EndLineLabel, EID_StSwarStyk, EID_EndSwarStyk,
            EID_OpChange, EID_PathChange, EID_MovDirChange, EID_SezdNo,
            EID_LongLabel, EID_GPSCoord, EID_GPSState, EID_GPSError:
            SkipBytes:= Avk11EventLen[LoadID]; *)

(*         EID_SysCrd_SS:
            begin // ��������� ���������� �������� ������, �������� ����������
              FBody.ReadBuffer(LoadByte[1], 2);
              CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00)
                  or LoadByte[2]);
            end;
          EID_SysCrd_SF:
            begin // ��������� ���������� �������� ������, ������ ����������
              FBody.ReadBuffer(LoadByte[1], 1);
              FBody.ReadBuffer(LoadLongInt, 4);
              CurSysCoord:= LoadLongInt[1];
            end;
          EID_SysCrd_FS:
            begin // ��������� ���������� ������ ������, �������� ����������
              FBody.ReadBuffer(LoadByte[1], 5);
              CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00)
                  or LoadByte[5]);
            end;
          EID_SysCrd_FF:
            begin // ��������� ���������� ������ ������, ������ ����������
              FBody.ReadBuffer(LoadByte[1], 4);
              FBody.ReadBuffer(LoadLongInt, 4);
              CurSysCoord:= LoadLongInt[1];
            end; *)
          EID_SysCrd_NS:
            begin // ��������� ���������� ������ ������, �������� ����������
              FBody.ReadBuffer(LoadByte[1], 1);
              CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[1]);
            end;
          EID_SysCrd_NF:
            begin // ��������� ���������� ������ ������, ������ ����������
              FBody.ReadBuffer(LoadLongInt, 4);
            (*  if LoadLongInt[1] > FExHeader.EndSysCoord then FBody.Position:= SaveOffset + 1
                                                        else *) CurSysCoord:= LoadLongInt[1];
            end;
          EID_EndFile:
            begin // ����� �����
              DisCoord:= CurDisCoord;
              SysCoord:= CurSysCoord;
              OffSet:= FBody.Position;
              Exit;
            end
          else if not SkipEvent(LoadID) { LoadID and 128 = 0 } then ;// �������
        end

      end;
{      else
      begin
        SkipBytes:= Avk11EventLen[LoadID];
        if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then UnPackEcho;
      end;
    end;
 }
//    if SkipBytes <> -1 then
//      FBody.Position:= FBody.Position + SkipBytes;

    if (not BMSkipFlag) and
       (LoadID in [ EID_SysCrd_NS, EID_SysCrd_NF, EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF]) then
    begin
      Inc(CurDisCoord, Abs(CurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
      LastSysCoord:= CurSysCoord;

      { if not SkipUnPack then }
//      UnPackEcho;
      // SkipUnPack:= False;

      if CurDisCoord > NeedDisCoord then
      begin
        DisCoord:= CurDisCoord_;
        SysCoord:= CurSysCoord_;
        OffSet:= CurPosition_;
        FBody.Position:= OffSet;
        // ToLog(Format('EndState Rail: 0, BS: %d', [Ord(FPack[r_Left].Ampl <> 255)]));  // ----------------------------------------------------------------------------------------
        // ToLog(Format('EndState Rail: 1, BS: %d', [Ord(FPack[r_Right].Ampl <> 255)])); // ----------------------------------------------------------------------------------------
        Break;
      end
      else if CurDisCoord = NeedDisCoord then
      begin
        DisCoord:= CurDisCoord;
        SysCoord:= CurSysCoord;
        OffSet:= FBody.Position;
        // ToLog(Format('EndState Rail: 0, BS: %d', [Ord(FPack[r_Left].Ampl <> 255)]));  // ----------------------------------------------------------------------------------------
        // ToLog(Format('EndState Rail: 1, BS: %d', [Ord(FPack[r_Right].Ampl <> 255)])); // ----------------------------------------------------------------------------------------
        Break;
      end
    end;
  until False;
end;

procedure TAviconDataSource.LoadData(StartDisCoord, EndDisCoord, LoadShift: Longint; BlockOk: TDataNotifyEvent);
var
  R: TRail;
  LoadID, Dat: Byte;
  I, J, SkipBytes, OffSet, Ch, T: Integer;
  ErrorFlag: Boolean;
  LastSysCoord: Longint;
  LastDisCoord: Longint;
  SaveLastSysCoord: Longint;
  LoadByte: array [1 .. 331] of Byte;
  LoadLongInt: array [1 .. 5] of Longint; // absolute LoadByte[1];
  HSHead: THSHead;
  HSItem: THSItem;
  CurEcho_: TCurEcho;
  NewSysCoord: Longint;
  BMSkipFlag: Boolean;
  TMP: Boolean;
  NeedDisCoord: Integer;
  StartIdx: Integer;
  SavePos: Integer;
  R_: Integer;
  pASR: pedAutomaticSearchRes;
  ExHeader__: TExHeader;
  Time: edTime;
  DataType: Integer;
  Len: Integer;
  BodyBadZones_idx: Integer;

begin
  {$IFDEF Coord_DEBUG_GRAPH}
  SetLength(Coord_DEBUG_GRAPH, 0);
  {$ENDIF}

  Inc(LoadDataCount);

  SetLength(FAutomaticSearchResList, 0);
  SetLength(AKStateData_, 0);


  for R:= FConfig.MinRail to FConfig.MaxRail do // ������� ������� ������� ��������
    for I:= FConfig.MinScanChNumber to FConfig.MaxScanChNumber do
      FCurEcho[R, I].Count:= 0;

  NeedDisCoord:= NormalizeDisCoord(StartDisCoord - LoadShift);

  // ���� �� ���������� ������ � �����
  // ��� ����� - ������� ��������� ������� � ������ ��� ������� �����������
  // ToLog(Format('NeedDisCoord: %d', [NeedDisCoord]));                              // ----------------------------------------------------------------------------------------
  StartIdx:= GetLeftEventIdx(NeedDisCoord);

  I:= FEvents[StartIdx].DisCoord;
  while (I = FEvents[StartIdx].DisCoord) and (StartIdx <> 0) do Dec(StartIdx);

  FCurDisCoord:= FEvents[StartIdx].DisCoord;
  FCurSysCoord:= FEvents[StartIdx].SysCoord;
  OffSet:= FEvents[StartIdx].OffSet;

  LastSysCoord:= FCurSysCoord;
  FBody.Position:= OffSet;

  GetParamFirst(StartDisCoord - LoadShift, FCurParams);
//  if not TestOffsetFirst then Exit;
  if FSkipBM then TestBMOffsetFirst;
  BMSkipFlag:= False;

  BodyBadZones_idx:= -1;
  for I := 0 to High(FBodyBadZones) do
  begin
    if OffSet < FBodyBadZones[I].From then
    begin
      BodyBadZones_idx:= I;
      Break;
    end
    else
    if (OffSet >= FBodyBadZones[I].From) and
       (OffSet <= FBodyBadZones[I].To_) then
    begin
      FBody.Position:= FBodyBadZones[I].To_;
      BodyBadZones_idx:= I + 1;
      Break;
    end;
  end;
  if Length(FBodyBadZones) <> 0 then
    if BodyBadZones_idx > High(FBodyBadZones) then
      BodyBadZones_idx:= -1;

  repeat
    if FSkipBM then
    begin
      TMP:= BMSkipFlag;
      BMSkipFlag:= TestBMOffsetNext;
      if TMP and (not BMSkipFlag) then
        for R:= rLeft to rRight do
          for I:= 1 to 12 do
            FCurEcho[R, I].Count:= 0;
    end;

    FCurOffset:= FBody.Position;
    SkipBytes:= -1;

    if Header.TableLink <> 0 then
      if FBody.Position >= Header.TableLink then Exit;

    if BodyBadZones_idx <> - 1 then
    begin
      if (FBody.Position >= FBodyBadZones[BodyBadZones_idx].From) and
         (FBody.Position < FBodyBadZones[BodyBadZones_idx].To_) then
      begin
        FBody.Position:= FBodyBadZones[BodyBadZones_idx].To_;
        BodyBadZones_idx:= I + 1;
        if BodyBadZones_idx > High(FBodyBadZones) then BodyBadZones_idx:= -1;
      end;
    end;

    FBody.ReadBuffer(LoadID, 1);

    if LoadID and 128 = 0 then
    begin
      FSrcDat[0]:= LoadID;

      Ch:= (LoadID shr 2) and 15;
(*      if Ch = 0 then
      begin
        FFileBody.ReadBuffer(Dat, 1);
        CurAmpl[SideToRail(TSide(LoadID shr 6 and 1))]:= Dat;
      end
      else  *)
      begin

        if FExtendedChannels and (Ch > 14) then
        begin
          FBody.ReadBuffer(Ch, 1);
          Ch:= Ch + 15;
        end;
        SavePos:= FBody.Position;
        FBody.ReadBuffer(FSrcDat[1], EchoLen[LoadID and 3]);

        with FCurEcho[TRail(LoadID shr 6 and 1), Ch] do
        begin

          case LoadID and $03 of
//            0: ZerroFlag:= True;
            0: //if FSrcDat[1] <> $FF then
               begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 //DelayOffset[Count]:= SavePos;
                 Ampl[Count]:= FSrcDat[2] shr 4 and $0F;
               end; // else ZerroFlag2:= True;
            1: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 //DelayOffset[Count]:= SavePos;
                 Ampl[Count]:= FSrcDat[3] shr 4 and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[2];
                 //DelayOffset[Count]:= SavePos + 1;
                 Ampl[Count]:= FSrcDat[3] and $0F;
//                 ZerroFlag2:= False;
               end;
            2: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 //DelayOffset[Count]:= SavePos;
                 Ampl[Count]:= FSrcDat[4] shr 4 and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[2];
                 //DelayOffset[Count]:= SavePos + 1;
                 Ampl[Count]:= FSrcDat[4] and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[3];
                 //DelayOffset[Count]:= SavePos + 2;
                 Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
//                 ZerroFlag2:= False;
               end;
            3: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 //DelayOffset[Count]:= SavePos;
                 Ampl[Count]:= FSrcDat[5] shr 4 and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[2];
                 //DelayOffset[Count]:= SavePos + 1;
                 Ampl[Count]:= FSrcDat[5] and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[3];
                 //DelayOffset[Count]:= SavePos + 2;
                 Ampl[Count]:= FSrcDat[6] shr 4 and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[4];
                 //DelayOffset[Count]:= SavePos + 3;
                 Ampl[Count]:= FSrcDat[6] and $0F;
//                 ZerroFlag2:= False;
               end;
          end;
        end;
      end;
    end
    else
    begin
      if not BMSkipFlag then
      begin
        case LoadID of
          EID_AutomaticSearchRes:
            begin
              SetLength(FAutomaticSearchResList, Length(FAutomaticSearchResList) + 1);
              pASR:= @FAutomaticSearchResList[High(FAutomaticSearchResList)];
              FBody.ReadBuffer(pASR^.Rail, SizeOf(edAutomaticSearchRes));

              if (not ((Integer(pASR^.Rail) in [0, 1]) and (pASR^.ScanCh in [0..15]))) or
                 (pASR^.CentrCoord < 0) or
                 (pASR^.CentrCoord > GetMaxDisCoord) then
              begin
                 SetLength(FAutomaticSearchResList, Length(FAutomaticSearchResList) - 1);
              end else if pASR^.ScanCh = 0 then pASR^.ScanCh:= 1;
            end;

          EID_ACData:
            begin
              FBody.ReadBuffer(LoadID, 1);
              if LoadID and $01 = 0 then
              begin
                SetLength(AKStateData_, Length(AKStateData_) + 1);
                with AKStateData_[High(AKStateData_)] do
                begin
                  DisCrd:= FCurDisCoord;
                  Rail:= TRail(LoadID shr 6 and 1);
                  Ch:= (LoadID shr 2) and 15;
                  State:= Boolean(LoadID shr 7 and 1);
                end;
              end;

            //  CurAKExists[TRail(LoadID shr 6 and 1), (LoadID shr 2) and 15]:= True;
            //  CurAKState[TRail(LoadID shr 6 and 1), (LoadID shr 2) and 15]:= Boolean(LoadID shr 7 and 1);
            end;

          EID_AirBrushTempOff:
            begin
              FBody.ReadBuffer(AlarmTempOffState, 1);
              FBody.ReadBuffer(LoadByte[2], 4);
            end;
          EID_SysCrd_NS:
            begin // ��������� ���������� �������� ����������
              FBody.ReadBuffer(LoadByte[1], 1);
              NewSysCoord:= Integer((Integer(FCurSysCoord) and $FFFFFF00) or LoadByte[1]);
            end;
          EID_SysCrd_NF:
            begin // ��������� ���������� ������ ����������
              FBody.ReadBuffer(LoadLongInt, 4);
            (*  if LoadLongInt[1] > FExHeader.EndSysCoord then FBody.Position:= FCurOffset + 1
                                                        else *) NewSysCoord:= LoadLongInt[1];
            end;

          EID_EndFile: begin // ����� �����
                         ErrorFlag:= False;
                         FBody.ReadBuffer(LoadByte, 13);
                         for i := 1 to 13 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                         if not ErrorFlag then
                           if FBody.Position <> FBody.Size then
                           begin
                             if FBody.Size - FBody.Position >= SizeOf(TExHeader) then
                             begin
                               FBody.ReadBuffer(LoadByte, 170 + 32 + 128);
                               for i := 1 to 14 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                               if not ErrorFlag then
                                 for i := 107 to 170 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                               if not ErrorFlag then
                                 for i := 205 + 2 to 330 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                               ErrorFlag:= ErrorFlag or not (LoadByte[19] in [0..8]) or (LoadByte[20] + LoadByte[21] + LoadByte[22] <> 0);
                             end;
                           end;
                         if ErrorFlag then FBody.Position:= FCurOffset + 1
                                      else ; // ��� �� �� ��� ���������!

(*                            if FBody.Position = 5311 then
                            begin
                              FBody.Position:= FBody.Position - 1; // FHead.TableLink;
                              FBody.ReadBuffer(ExHeader__, SizeOf(ExHeader__));

                              repeat
                                FBody.ReadBuffer(LoadID, 1); // �������� �������������� �������
                                if LoadID = EID_Time then
                                begin
                                  FBody.ReadBuffer(Time, 2); // �������� �������������� �������
                                  if Time.Hour = FHead.Hour then
                                  begin
                                    Break;
                                  end else FBody.Position:= FBody.Position - 1;
                                end;
                              until False;
                            end
                            else
                            begin
                              FBody.ReadBuffer(LoadByte, 9 + 4);

                              DataType:= 1;
                              for i := 1 to 13 do
                                if LoadByte[i] <> $FF then
                                begin
                                  DataType:= 0;
                                  Break;
                                end;

                              if FBody.Position < FHead.TableLink - 10 then DataType:= 0;

                              if DataType = 0 then begin
                                                     FBody.Position:= FCurOffset + 1;
                                                   end
                                              else begin
                                                    // Exit;
                                                     Break;
                                                   end;

                            end; *)
                       end;


            EID_SysCrd_SS:
              begin // ��������� ���������� �������� ������, �������� ����������
                FBody.ReadBuffer(LoadByte[1], 2);
                NewSysCoord:= Integer((Integer(FCurSysCoord) and $FFFFFF00)
                    or LoadByte[2]);
              end;
            EID_SysCrd_SF:
              begin // ��������� ���������� �������� ������, ������ ����������
                FBody.ReadBuffer(LoadByte[1], 1);
                FBody.ReadBuffer(LoadLongInt, 4);
                NewSysCoord:= LoadLongInt[1];
              end;
            EID_SysCrd_FS:
              begin // ��������� ���������� ������ ������, �������� ����������
                FBody.ReadBuffer(LoadByte[1], 5);
                NewSysCoord:= Integer((Integer(FCurSysCoord) and $FFFFFF00)
                    or LoadByte[5]);
              end;
            EID_SysCrd_FF:
              begin // ��������� ���������� ������ ������, ������ ����������
                FBody.ReadBuffer(LoadByte[1], 4);
                FBody.ReadBuffer(LoadLongInt, 4);
                NewSysCoord:= LoadLongInt[1];
              end;

    EID_SysCrd_UMUA_Left_NF: begin // ������ ��������� ���������� ��� ������ ��� A, ����� �������
                               FBody.ReadBuffer(LoadLongInt, 4);
                               FCurLoadSysCoord_UMUA_Left:= LoadLongInt[1];
                             end;
    EID_SysCrd_UMUB_Left_NF: begin // ������ ��������� ���������� ��� ������ ��� �, ����� �������
                                Inc(UMUB_Left_Index);
                                FBody.ReadBuffer(LoadLongInt, 4);
                                FCurLoadSysCoord_UMUB_Left:= LoadLongInt[1];
                              end;
    EID_SysCrd_UMUA_Right_NF: begin // ������ ��������� ���������� ��� ������ ��� A, ������ �������
                                Inc(UMUA_Right_Index);
                                FBody.ReadBuffer(LoadLongInt, 4);
                                FCurLoadSysCoord_UMUA_Right:= LoadLongInt[1];
                              end;
    EID_SysCrd_UMUB_Right_NF: begin // ������ ��������� ���������� ��� ������ ��� �, ������ �������
                                Inc(UMUB_Right_Index);
                                FBody.ReadBuffer(LoadLongInt, 4);
                                FCurLoadSysCoord_UMUB_Right:= LoadLongInt[1];
                              end;
            else SkipEvent(LoadID);
        end;

        {$IFDEF FIX_COORD_JUMP}
        if FSkipEventFlag_HandScan then FSkipCoordEventCount:= 15;
        {$ENDIF}

        if LoadID in [EID_SysCrd_NS, EID_SysCrd_NF, EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then
        begin
          {$IFDEF FIX_COORD_JUMP}
          if (FSkipCoordEventCount <> 0) then // ���������� ������� �� ����� �������
          begin
            if Abs(NewSysCoord - LastSysCoord) > 20 then NewSysCoord:= LastSysCoord;
            Dec(FSkipCoordEventCount);
          end;
          {$ENDIF}

          {$IFDEF Coord_DEBUG_GRAPH}
          Len:= Length(Coord_DEBUG_GRAPH);
          SetLength(Coord_DEBUG_GRAPH, Len + 1);
          Coord_DEBUG_GRAPH[Len]:= Point(NewSysCoord, 1);
          {$ENDIF}


          begin

            BackMotion:= NewSysCoord < LastSysCoord;

            if Assigned(BlockOk) and (FCurDisCoord >= StartDisCoord - LoadShift) and (FCurDisCoord <= EndDisCoord) then
              if not BlockOk(StartDisCoord) then Break; // ��������� ������� ��� "������" ����������

            FCurSysCoord:= NewSysCoord;

            Inc(FCurDisCoord, Abs(FCurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
            LastSysCoord:= FCurSysCoord;
            LastDisCoord:= FCurDisCoord;

            GetParamNext(FCurDisCoord, FCurParams);
            if CurDisCoord >= EndDisCoord then Exit;

            FillChar(FCurEcho, SizeOf(FCurEcho), 0);

{            for R:= FConfig.MinRail to FConfig.MaxRail do // ������� ������� ������� ��������
              for I:= FConfig.MinScanChNumber to FConfig.MaxScanChNumber do
              begin
                FCurEcho[R, I].Count:= 0;
              end; }
          end;
        end;
      end
    end;
  until False;
end;

function TAviconDataSource.GetBMStateFirst(DisCoord: Integer): Boolean;
var
  I: Integer;
  EndIdx: Integer;
  StartIdx: Integer;

begin
  Result:= False;
  FGetBMStateIdx:= 0;
  if FSkipBM then
  begin
    FGetBMStateState:= False;
    Exit;
  end;

  for I:= High(FCDList) downto 0 do
  begin
    if FEvents[FCDList[I]].DisCoord <= DisCoord then
    begin
      if FEvents[FCDList[I]].ID = EID_FwdDir then
      begin
        Result:= False;
        FGetBMStateIdx:= I + 1;
        Break;
      end;
      if FEvents[FCDList[I]].ID = EID_BwdDir { EID_FwdDirEnd } then
      begin
        Result:= True;
        FGetBMStateIdx:= I + 1;
        Break;
      end;
      Break;
    end;
  end;
  FGetBMStateState:= Result;

  {
    FGetBMStateIdx:= 0;
    GetEventIdx(0, DisCoord, StartIdx, EndIdx, 0);

    Result:= False;
    for I:= EndIdx downto 0 do
    begin
    if FEvents[I].Event.ID = EID_FwdDir then
    begin
    Result:= False;
    FGetBMStateIdx:= I + 1;
    Break;
    end;
    if FEvents[I].Event.ID = EID_BwdDir then
    begin
    Result:= True;
    FGetBMStateIdx:= I + 1;
    Break;
    end;
    end;
    FGetBMStateState:= Result;
    }
end;

function TAviconDataSource.GetBMStateNext(var DisCoord: Integer; var State: Boolean): Boolean;
var
  I: Integer;
  EndIdx: Integer;
  StartIdx: Integer;

begin
  Result:= False;
  State:= FGetBMStateState;
  if FSkipBM then Exit;

  for I:= FGetBMStateIdx to High(FCDList) do
  begin
    if FEvents[FCDList[I]].ID = EID_FwdDir then
    begin
      State:= False;
      FGetBMStateIdx:= I + 1;
      FGetBMStateState:= False;
      DisCoord:= FEvents[FCDList[I]].DisCoord;
      Result:= I <> GetEventCount - 1;
      Break;
    end;
    if FEvents[FCDList[I]].ID = EID_BwdDir { EID_FwdDirEnd } then
    begin
      State:= True;
      FGetBMStateIdx:= I + 1;
      FGetBMStateState:= True;
      DisCoord:= FEvents[FCDList[I]].DisCoord;
      Result:= I <> GetEventCount - 1;
      Break;
    end;
  end;

  {
    Result:= False;
    State:= FGetBMStateState;
    for I:= FGetBMStateIdx to GetEventCount - 1 do
    begin
    if FEvents[I].Event.ID = EID_FwdDir then
    begin
    State:= False;
    FGetBMStateIdx:= I + 1;
    FGetBMStateState:= False;
    DisCoord:= FEvents[I].Event.DisCoord;
    Result:= I <> GetEventCount - 1;
    Break;
    end;
    if FEvents[I].Event.ID = EID_BwdDir then
    begin
    State:= True;
    FGetBMStateIdx:= I + 1;
    FGetBMStateState:= True;
    DisCoord:= FEvents[I].Event.DisCoord;
    Result:= I <> GetEventCount - 1;
    Break;
    end;
    end;
    }
end;

procedure TAviconDataSource.SetScanStep(New: Word);
begin
  FHead.ScanStep:= New;
end;

function TAviconDataSource.GetEventIdx(StartDisCoord, EndDisCoord: Integer;
  var StartIdx, EndIdx: Integer; EmptyPar: Integer): Boolean;
var
  I: Integer;
  SameCoord1: TIntegerDynArray;
  SameCoord2: TIntegerDynArray;
  LeftIdx1: Integer;
  LeftIdx2: Integer;
  RightIdx1: Integer;
  RightIdx2: Integer;

begin

  GetNearestEventIdx(StartDisCoord, LeftIdx1, RightIdx1, SameCoord1);
  GetNearestEventIdx(EndDisCoord, LeftIdx2, RightIdx2, SameCoord2);

  if Length(SameCoord1) <> 0 then
    StartIdx:= SameCoord1[0]
  else
    StartIdx:= LeftIdx1 + 1;

  if Length(SameCoord2) <> 0 then
    EndIdx:= SameCoord2[ High(SameCoord2)]
  else
    EndIdx:= RightIdx2 (*- 1*);

  {
    StartIdx:= High(FEvents);
    EndIdx:= 0;

    for I:= 0 to High(FEvents) do
    begin
    if (StartDisCoord <= FEvents[I].Event.DisCoord) and
    (EndDisCoord   >= FEvents[I].Event.DisCoord) then
    begin
    StartIdx:= Min(StartIdx, I);
    EndIdx:=   Max(EndIdx, I);
    end;
    if EndDisCoord < FEvents[I].Event.DisCoord then Break;
    end; }
end;

procedure TAviconDataSource.GetNearestEventIdx(DisCoord: Integer; var LeftIdx, RightIdx: Integer; var SameCoord: TIntegerDynArray; Force: Boolean = False);
var
  I: Integer;
  CLeft: Integer;
  CRight: Integer;
  CurrPos: Integer;

begin
  if (not FSkipBM) or (FSkipBM and (not Force))  then
  begin
    if Length(FEvents) = 0 then Exit;
    CLeft:= 0; // ������ � ������ � ������
    CRight:= High(FEvents); // ����� �������������� � �����
    repeat
      CurrPos:= (CLeft + CRight) div 2; // ���������� ��������
      begin
        if FEvents[CurrPos].DisCoord < DisCoord then CLeft:= CurrPos // ������� ����� �������� ����� ��� ����������� �������
                                                else CRight:= CurrPos;
        if (CLeft = CRight) or (Abs(CLeft - CRight) = 1) then // �������� �� ����� ������
        begin
          for I:= CLeft downto 0 do
            if DisCoord = FEvents[I].DisCoord then
            begin
              SetLength(SameCoord, Length(SameCoord) + 1);
              SameCoord[ High(SameCoord)]:= I;
              LeftIdx:= I;
            end
            else
            begin
              if Length(SameCoord) <> 0 then
                LeftIdx:= SameCoord[0]
              else
                LeftIdx:= CLeft;
              Break;
            end;

          for I:= CLeft + 1 to High(FEvents) do
            if DisCoord = FEvents[I].DisCoord then
            begin
              SetLength(SameCoord, Length(SameCoord) + 1);
              SameCoord[ High(SameCoord)]:= I;
              RightIdx:= I;
            end
            else
            begin
              if Length(SameCoord) <> 0 then
                RightIdx:= SameCoord[ High(SameCoord)]
              else
                RightIdx:= I;
              Break;
            end;

          Exit;
        end;
      end;
    until False;
  end
  else
  begin
    if Length(FSaveEventsData) = 0 then Exit;
    CLeft:= 0; // ������ � ������ � ������
    CRight:= High(FSaveEventsData); // ����� �������������� � �����
    repeat
      CurrPos:= (CLeft + CRight) div 2; // ���������� ��������
      begin
        if FSaveEventsData[CurrPos].DisCoord < DisCoord then CLeft:= CurrPos // ������� ����� �������� ����� ��� ����������� �������
                                                else CRight:= CurrPos;
        if (CLeft = CRight) or (Abs(CLeft - CRight) = 1) then // �������� �� ����� ������
        begin
          for I:= CLeft downto 0 do
            if DisCoord = FSaveEventsData[I].DisCoord then
            begin
              SetLength(SameCoord, Length(SameCoord) + 1);
              SameCoord[ High(SameCoord)]:= I;
              LeftIdx:= I;
            end
            else
            begin
              if Length(SameCoord) <> 0 then
                LeftIdx:= SameCoord[0]
              else
                LeftIdx:= CLeft;
              Break;
            end;

          for I:= CLeft + 1 to High(FSaveEventsData) do
            if DisCoord = FSaveEventsData[I].DisCoord then
            begin
              SetLength(SameCoord, Length(SameCoord) + 1);
              SameCoord[ High(SameCoord)]:= I;
              RightIdx:= I;
            end
            else
            begin
              if Length(SameCoord) <> 0 then
                RightIdx:= SameCoord[ High(SameCoord)]
              else
                RightIdx:= I;
              Break;
            end;

          Exit;
        end;
      end;
    until False;
  end;
end;

function TAviconDataSource.GetLeftEventIdx(DisCoord: Integer; Force: Boolean = False): Integer;
var
  LeftIdx, RightIdx: Integer;
  SameCoord: TIntegerDynArray;

begin
  GetNearestEventIdx(DisCoord, LeftIdx, RightIdx, SameCoord, Force);
  if Length(SameCoord) <> 0 then Result:= SameCoord[High(SameCoord)]
                            else Result:= LeftIdx;
end;

function TAviconDataSource.GetRightEventIdx(DisCoord: Integer): Integer;
var
  LeftIdx, RightIdx: Integer;
  SameCoord: TIntegerDynArray;

begin
  GetNearestEventIdx(DisCoord, LeftIdx, RightIdx, SameCoord);
  if Length(SameCoord) <> 0 then Result:= SameCoord[0]
                            else Result:= RightIdx;
end;

procedure TAviconDataSource.GetEventData(Idx: Integer; var ID: Byte; var pData: PEventData);
begin
  if (Idx >= 0) and (Idx <= High(FEvents)) then
  begin
    ID:= FEvents[Idx].ID;
    pData:= Addr(FEvents[Idx].Data);
  end;
end;

function TAviconDataSource.GetTimeList(Index: Integer): TimeListItem;
begin
  Result:= FTimeList[Index];
end;

function TAviconDataSource.GetTimeListCount: Integer;
begin
  Result:= Length(FTimeList)
end;

function TAviconDataSource.GetEventsData(Index: Integer): TEventData;
begin
  Result:= FEvents[Index].Data;
end;

function TAviconDataSource.GetParamFirst(DisCoord: Integer; var Params: TMiasParams): Boolean;
{
  var
  StartIdx, EndIdx: Integer;
  }
begin
  DisCoord:= NormalizeDisCoord(DisCoord);
  if DisCoord <= 0 then  DisCoord:= 1;

  FGetParamIdx:= GetLeftEventIdx(DisCoord);
  if (FGetParamIdx <> -1) and (FGetParamIdx < Length(FParams)) then Params:= FParams[FGetParamIdx];
end;

function TAviconDataSource.GetParamNext(DisCoord: Integer; var Params: TMiasParams): Boolean;
var
  I: Integer;
  StartIdx, EndIdx: Integer;

begin
  if FGetParamIdx = -1 then   Exit;
  // DisCoord:= NormalizeDisCoord(DisCoord);
  // GetEventIdx(DisCoord, DisCoord, StartIdx, EndIdx);
  // FGetParamIdx:= EndIdx;

  for I:= FGetParamIdx to High(FEvents) do
  begin
    if DisCoord = FEvents[I].DisCoord then
    begin
      Params:= FParams[I];
      FGetParamIdx:= I;
      Break;
    end;
    if DisCoord < FEvents[I].DisCoord then
    begin
      Params:= FParams[I - 1];
      FGetParamIdx:= I;
      Break;
    end;
  end;
end;

function TAviconDataSource.RailToPathRail(R: TRail): TRail;
begin
  if CompareMem(@Header.DeviceID, @Avicon15Scheme1, 8) or
     CompareMem(@Header.DeviceID, @USK003R, 8) or
     CompareMem(@Header.DeviceID, @USK004R, 8) then
  begin
    Result:= TRail(Header.WorkRailTypeA);
  end
  else
  begin
    if (Header.UsedItems[uiCorrSides] = 1) and (Header.CorrespondenceSides = csReverse) then
    begin
      case R of
         rLeft: Result:= rRight;
        rRight: Result:= rLeft;
      end;
    end else Result:= R;
  end;
end;

function TAviconDataSource.isTwoWheels: Boolean;
begin
  Result:= isEGOUS_WheelScheme1 or isEGOUS_WheelScheme2 or isVMTUS_Scheme_1 or isVMTUS_Scheme_2 or isMDL;
end;

function TAviconDataSource.isUSK: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @USK004R, 8) or
           CompareMem(@FHead.DeviceID, @USK003R, 8);
end;

function TAviconDataSource.isSingleRailDevice: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @RKS_U_Scheme_5, 8) or
           CompareMem(@FHead.DeviceID, @RKS_U_Scheme_8, 8) or
           CompareMem(@FHead.DeviceID, @Avicon15Scheme1, 8) or
           CompareMem(@FHead.DeviceID, @USK003R, 8) or
           CompareMem(@FHead.DeviceID, @USK004R, 8) or
           CompareMem(@FHead.DeviceID, @FilusX17DW, 8);
end;

function TAviconDataSource.isEGOUSW: Boolean; // ����� VMT � ������������ �������� � ��� �������
begin
  Result:= CompareMem(@FHead.DeviceID, @EGO_USWScheme1, 8) or
           CompareMem(@FHead.DeviceID, @EGO_USWScheme2, 8) {or
           CompareMem(@FHead.DeviceID, @EGO_USWScheme3, 8) } or
           CompareMem(@FHead.DeviceID, @VMT_US_BigWP, 8) or
           CompareMem(@FHead.DeviceID, @VMT_US_BigWP_N2, 8);
           CompareMem(@FHead.DeviceID, @VMT_USW, 8);
end;

function TAviconDataSource.isVMTUS_Scheme_1: Boolean; // �������
begin
  Result:= CompareMem(@FHead.DeviceID, @EGO_USWScheme1, 8);
end;

function TAviconDataSource.isVMTUS_Scheme_2: Boolean; // �������
begin
  Result:= CompareMem(@FHead.DeviceID, @EGO_USWScheme2, 8);
end;

function TAviconDataSource.isVMTUS_Scheme_1_or_2: Boolean; // �������
begin
  Result:= CompareMem(@FHead.DeviceID, @EGO_USWScheme1, 8)  or
           CompareMem(@FHead.DeviceID, @EGO_USWScheme2, 8) { or
           CompareMem(@FHead.DeviceID, @EGO_USWScheme3, 8)} ;
end;

function TAviconDataSource.isVMTUS_Scheme_4: Boolean; // ����
begin
  Result:= CompareMem(@FHead.DeviceID, @VMT_US_BigWP, 8);
end;

function TAviconDataSource.isVMTUS_Scheme_5: Boolean; // ������
begin
  Result:= CompareMem(@FHead.DeviceID, @VMT_US_BigWP_N2, 8);
end;

function TAviconDataSource.isVMTUS_Scheme_4_or_5: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @VMT_US_BigWP, 8) or
           CompareMem(@FHead.DeviceID, @VMT_US_BigWP_N2, 8);
end;


function TAviconDataSource.isFilus_X17DW: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @FilusX17DW, 8);
end;

function TAviconDataSource.isFilus_X27_Scheme1: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon14Scheme1, 8);
end;

function TAviconDataSource.isFilus_X27W_Scheme1: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon14Wheel, 8);
end;

function TAviconDataSource.isFilus_X27W_Scheme2: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon14Wheel2, 8);
end;

function TAviconDataSource.isVMT_US_BigWP: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @VMT_US_BigWP, 8) or
           CompareMem(@FHead.DeviceID, @VMT_US_BigWP_N2, 8);
end;

function TAviconDataSource.isVMT_US_BigWP_BHead: Boolean;
begin
  Result:= (isVMTUS_Scheme_1_or_2 or isVMTUS_Scheme_4 or isVMTUS_Scheme_5) and
           (Header.ControlDirection = cd_Head_B_Tail_A);
end;

function TAviconDataSource.isMDL: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @MDLWheel, 8) or
           CompareMem(@FHead.DeviceID, @MDLWheel2, 8);
end;

function TAviconDataSource.isA31: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon31Scheme1, 8) or
           CompareMem(@FHead.DeviceID, @Avicon31Scheme2, 8) or
           CompareMem(@FHead.DeviceID, @Avicon31Scheme3, 8) or
           CompareMem(@FHead.DeviceID, @Avicon31Estonia, 8) or
           CompareMem(@FHead.DeviceID, @Avicon31Estonia2, 8);
end;

function TAviconDataSource.isA31_Scheme1: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon31Scheme1, 8) or
           CompareMem(@FHead.DeviceID, @Avicon31Estonia, 8);
end;

function TAviconDataSource.isA31_Scheme2: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon31Scheme2, 8);
end;

function TAviconDataSource.isA31Estonia_Scheme2: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon31Estonia2, 8);
end;

function TAviconDataSource.isA15New: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon15N8Scheme1, 8);
end;
(*
function TAviconDataSource.isOldFileFormat: Boolean;
begin
//  Result:= not (isA31 or isA15New or isFilus_X17DW);
  Result:= isA15Old or isEGOUS or isEGOUSW;
end;
*)
function TAviconDataSource.isA15Old: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon15Scheme1, 8);
end;

function TAviconDataSource.isEGOUS: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon16Scheme1, 8) or
           CompareMem(@FHead.DeviceID, @Avicon16Scheme2, 8) or
           CompareMem(@FHead.DeviceID, @Avicon16Scheme3, 8) or
           CompareMem(@FHead.DeviceID, @Avicon16Wheel, 8) or
           CompareMem(@FHead.DeviceID, @Avicon16Wheel2, 8) or
           CompareMem(@FHead.DeviceID, @Avicon16Wheel3, 8);

end;

function TAviconDataSource.isEGOUS_Scheme1: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon16Scheme1, 8);
end;

function TAviconDataSource.isEGOUS_Scheme2: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon16Scheme2, 8);
end;

function TAviconDataSource.isEGOUS_Scheme3: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon16Scheme3, 8);
end;

function TAviconDataSource.isEGOUS_WheelScheme1: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon16Wheel, 8);
end;

function TAviconDataSource.isEGOUS_WheelScheme2: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon16Wheel2, 8);
end;

function TAviconDataSource.isEGOUS_WheelScheme3: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon16Wheel3, 8);
end;

function TAviconDataSource.isEGOUSW_BHead: Boolean;
begin
  Result:= isEGOUSW and (Header.ControlDirection = cd_Head_B_Tail_A);
end;

function TAviconDataSource.isAutoDecodingEnabled: Boolean;
begin
  Result:= isA31 or isA15New or isA15Old;
end;

function TAviconDataSource.GetTime(DisCoord: Integer; TimerIndex: Integer = 0): string;
var
  StartIdx: Integer;
begin
  DisCoord:= NormalizeDisCoord(DisCoord);
  StartIdx:= GetLeftEventIdx(DisCoord);

  Result:= GetTimeFromEvent(StartIdx, TimerIndex);
end;

function TAviconDataSource.GetTimeFromEvent(StartIdx: Integer; TimerIndex: Integer = 0): string;
var
  ID: Byte;
  I: Integer;
  pData: PEventData;

function CodeToTime(Hour, Minute: Byte) : string;
var
  HS, MS: string;
  I: Integer;

begin
  HS:= IntToStr(Hour);
  if Length(HS) = 1 then HS:= '0' + HS;
  MS:= IntToStr(Minute);
  if Length(MS) = 1 then MS:= '0' + MS;
  Result:= Format('%s:%s', [HS, MS]);
end;

begin
  Result:= CodeToTime(FHead.Hour, FHead.Minute);

  for I:= StartIdx downto 0 do
  begin
    GetEventData(I, ID, pData);
    if (ID in [EID_Time]) and (pedTime(pData)^.Hour and $E0 = TimerIndex) then
    begin
      Result:= CodeToTime(pedTime(pData)^.Hour and $1F, pedTime(pData)^.Minute);
      Exit;
    end;
  end;
end;

{
function TAviconDataSource.GetTime(DisCoord: Integer; TimerIndex: Integer = 0): string;
var
  ID: Byte;
  I: Integer;
  pData: PEventData;
  StartIdx: Integer;

function CodeToTime(Hour, Minute: Byte) : string;
var
  HS, MS: string;
  I: Integer;

begin
  HS:= IntToStr(Hour);
  if Length(HS) = 1 then HS:= '0' + HS;
  MS:= IntToStr(Minute);
  if Length(MS) = 1 then MS:= '0' + MS;
  Result:= Format('%s:%s', [HS, MS]);
end;

begin
  DisCoord:= NormalizeDisCoord(DisCoord);
  StartIdx:= GetLeftEventIdx(DisCoord);

  Result:= CodeToTime(FHead.Hour, FHead.Minute);

  for I:= StartIdx downto 0 do
  begin
    GetEventData(I, ID, pData);
    if (ID in [EID_Time]) and (pedTime(pData)^.Hour and $E0 = TimerIndex) then
    begin
      Result:= CodeToTime(pedTime(pData)^.Hour and $1F, pedTime(pData)^.Minute);
      Exit;
    end;
  end;
end;
}

function TAviconDataSource.GetTemperature(DisCoord: Integer; var Val: Single): Boolean;
var
  ID: Byte;
  I: Integer;
  pData: PEventData;
  StartIdx: Integer;

begin
  DisCoord:= NormalizeDisCoord(DisCoord);
  StartIdx:= GetLeftEventIdx(DisCoord);

  for I:= StartIdx downto 0 do
  begin
    GetEventData(I, ID, pData);
    if ID in [EID_Temperature] then
    begin
      Val:= pedTemperature(@(pData^[2]))^.Value;
      Result:= True;
      Exit;
    end;
  end;
  Result:= False;
end;



function TAviconDataSource.GetSpeedState(DisCoord: Integer; var Speed: Single; var State: Integer): Boolean;
var
  ID: Byte;
  I: Integer;
  pData: PEventData;
  StartIdx: Integer;

begin
  DisCoord:= NormalizeDisCoord(DisCoord);
  StartIdx:= GetLeftEventIdx(DisCoord);

  for I:= StartIdx downto 0 do
  begin
    GetEventData(I, ID, pData);
    if ID in [EID_SpeedState] then
    begin
      Result:= True;
      Speed:= pedSpeedState(pData)^.Speed / 10;
      State:= pedSpeedState(pData)^.State;
      Exit;
    end;
  end;
  Result:= False;
end;

function TAviconDataSource.GetGPSCoord(DisCoord: Integer): TExtGPSPoint;
var
  ID: Byte;
  I: Integer;
  pData: PEventData;
  StartIdx: Integer;
begin
  DisCoord:= NormalizeDisCoord(DisCoord);
  Result.DisCoord:= - 1;
  if (High(GPSPoints)>=0) and (DisCoord >= GPSPoints[High(GPSPoints)].DisCoord) then begin
    I:= High(GPSPoints);
    Result.Lat:= GPSPoints[I].Lat;
    Result.Lon:= GPSPoints[I].Lon;
    Result.Speed:= GPSPoints[I].Speed;
    Result.DisCoord:= DisCoord;
  end;
  for I:= 0 to High(GPSPoints) - 1 do
  begin
    if (DisCoord >= GPSPoints[I].DisCoord) then begin
      if (DisCoord <= GPSPoints[I + 1].DisCoord) then
      begin
        Result.State:= GPSPoints[I].State;

        if not (isNAN(GPSPoints[I].Lat) or isNAN(GPSPoints[I].Lon)) and
           (GPSPoints[I].Lon >= -180) and (GPSPoints[I].Lon <= 180) and
           (GPSPoints[I].Lat >= -90) and (GPSPoints[I].Lat <= 90) then
        begin
          try
            if GPSPoints[I + 1].DisCoord <> GPSPoints[I].DisCoord then
            begin
              Result.Lat:= GPSPoints[I].Lat + (GPSPoints[I + 1].Lat - GPSPoints[I].Lat) * ((DisCoord - GPSPoints[I].DisCoord) / (GPSPoints[I + 1].DisCoord - GPSPoints[I].DisCoord));
              Result.Lon:= GPSPoints[I].Lon + (GPSPoints[I + 1].Lon - GPSPoints[I].Lon) * (DisCoord - GPSPoints[I].DisCoord) / (GPSPoints[I + 1].DisCoord - GPSPoints[I].DisCoord);
            end
            else
            begin
              Result.Lat:= GPSPoints[I].Lat;
              Result.Lon:= GPSPoints[I].Lon;
            end;
          except
            Result.Lat:= 0;
            Result.Lon:= 0;
          end;
        end
        else
        begin
          Result.Lat:= 0;
          Result.Lon:= 0;
        end;

        Result.Speed:= GPSPoints[I].Speed;
        Result.DisCoord:= DisCoord;
        Exit;
      end;
    end
    else begin
      Result.Lat:= 0;
      Result.Lon:= 0;
      Result.Speed:= GPSPoints[I].Speed;
      Result.DisCoord:= DisCoord;
      Exit;
    end;
  end;
end;

function TAviconDataSource.GetSpeed(DisCoord: Integer; var Speed: Single): string;
var
  I: Integer;
  J: Integer;
  K: Integer;
  T: Extended;
  S: Extended;

begin
  for I:= High(FTimeList) downto 0 do
    if FTimeList[I].DC <= DisCoord then
    begin
      if I <> High(FTimeList) then
        J:= I + 1
      else
        J:= I - 1;
      K:= I;
      if K > 0 then
        Dec(K);
      if J < High(FTimeList) then
        Inc(J);
      if K > 0 then
        Dec(K);
      if J < High(FTimeList) then
        Inc(J);
      if K > 0 then
        Dec(K);
      if J < High(FTimeList) then
        Inc(J);
      if K > 0 then
        Dec(K);
      if J < High(FTimeList) then
        Inc(J);

      T:= Abs(FTimeList[J].H + FTimeList[J].M / 60 -
          (FTimeList[K].H + FTimeList[K].M / 60));
      if T <> 0 then
      begin
        S:= Abs(FTimeList[J].DC - FTimeList[K].DC)
          * FHead.ScanStep / 100 / 1000000;
        Speed:= S / T;
        if T <> 0 then Result:= Format('%3.1f', [S / T])
                  else Result:= '?';
      end;
      Exit;
    end;
end;

function TAviconDataSource.MinAmplCode: Integer;
begin
  if not isA31 then Result:= Config.MinAmplCode else
  begin
    if Header.UsedItems[uiBScanTreshold_minus_6dB] <> 0 then Result:= 3;
    if Header.UsedItems[uiBScanTreshold_minus_12dB] <> 0 then Result:= 0;
  end;

end;

procedure ScanDir(StartDir: string; Mask: string; List: TStrings);
var
  SearchRec: TSearchRec;

begin
  if Mask = '' then Mask := '*.*';
  if StartDir[Length(StartDir)] <> '\' then
  StartDir := StartDir + '\';
  if FindFirst(StartDir + Mask, faAnyFile, SearchRec) = 0 then
  begin
    repeat
    //  Application.ProcessMessages;
      if (SearchRec.Attr and faDirectory) <> faDirectory
        then List.AddObject(StartDir + SearchRec.Name, Pointer(SearchRec.Size))
        else if (SearchRec.Name <> '..') and (SearchRec.Name <> '.') then
        begin
          List.AddObject(StartDir + SearchRec.Name + '\', Pointer(0));
          ScanDir(StartDir + SearchRec.Name + '\', Mask, List);
        end;
    until FindNext(SearchRec) <> 0;
    FindClose(SearchRec);
  end;
end;

function TAviconDataSource.LoadFromFile(FileName: string; VengrData: boolean = False): Boolean;
var
  FileID: TAviconID;
  DeviceID: TAviconID;
  DeviceVer: Byte;
  ID: Word;
  I, J, K: Integer;
  // OldHeader1: TAvk11Header1;
  FileAttr: Integer;
  FileList: TStringList;
  VFile: TStringList;
  Code: Integer;
  ValStr: string;
  tmp: string;
  idx: integer;
  val2: integer;
  val1: integer;
  val3: Integer;
  tmpp: TVData;

begin


///////////////////////////////////////////////////////
{
  FileList:= TStringList.Create;
  ScanDir(ExtractFilePath(FileName), 'pk_*.txt', FileList);

//  Decimalseparator:= ',';
  if VengrData then
  begin
    VFile:= TStringList.Create;
    for I := 0 to FileList.Count - 1 do
    begin

      tmp:= ExtractFileName(FileList[I]);
      delete(tmp, length(tmp)-3, 4);
      idx:= StrToInt(tmp[length(tmp)]) - 1;
      VFile.LoadFromFile(FileList[I]);

      for ID:= 1 to VFile.Count - 1 do
      begin
        SetLength(VData[idx], Length(VData[idx]) + 1);

        J:= Pos('km', VFile[ID]);
        TryStrToInt(Copy(VFile[ID], 1, J - 1), VData[idx, High(VData[idx])].Km);

        K:= Pos('pk', VFile[ID]);
        TryStrToInt(Copy(VFile[ID], J + 2, K - (J + 2)), VData[idx, High(VData[idx])].Pk);

        J:= K;
        K:= Pos('m', Copy(VFile[ID], K, 255)) + K - 1;
        TryStrToInt(Copy(VFile[ID], J + 2, K - (J + 2)), VData[idx, High(VData[idx])].m);

        J:= K;
        K:= Pos('mm', VFile[ID]);
        TryStrToInt(Copy(VFile[ID], J + 1, K - (J + 1)), VData[idx, High(VData[idx])].mm);

        J:= J + 1 + K - (J + 1) + 3;
        ValStr:= Copy(VFile[ID], J, 255);
        ValStr[Pos(',', ValStr)]:= '.';
        Val(ValStr, VData[idx, High(VData[idx])].Value, Code);

      end;
    end;
    VFile.Free;
          {
    for Idx := 0 to 3 do
      for I := 0 to High(VData[idx]) do
      begin
        Val1:= VData[idx, I].Km * 1000000 + VData[idx, I].Pk * 100000 + VData[idx, I].m * 1000 + VData[idx, I].mm;
        K:= - 1;
        for J := 0 to High(VData[idx]) do
          if I <> J then
          begin
            Val2:= VData[idx, J].Km * 1000000 + VData[idx, J].Pk * 100000 + VData[idx, J].m * 1000 + VData[idx, J].mm;
            if Val2 < Val1 then
            begin
              K:= J;
              Val1:= Val2;
            end;
          end;
        if K <> - 1 then
        begin
          tmpp:= VData[idx, I];
          VData[idx, I]:= VData[idx, K];
          VData[idx, K]:= tmpp;
        end;
      end;
  end;
  }


//  Decimalseparator:= '.';


{

97km 10pk 0m 952mm	     0,008
97km 10pk 0m 956mm	     0,006
97km 10pk 0m 969mm	     0,019
97km 10pk 0m 972mm	     0,019
97km 10pk 0m 996mm	     0,009
97km 10pk 0m 998mm	     0,011
97km 10pk 1m 7mm	     0,002
97km 10pk 1m 23mm	     0,002
97km 10pk 1m 24mm	     0,002
 }

///////////////////////////////////////////////////////


  Error:= [];
  FLoadFromStream:= False;
  FReadOnlyFlag:= False;
  FileAttr:= FileGetAttr(FileName);
  if FileAttr and faReadOnly <> 0 then
    if FileSetAttr(FileName, FileAttr and $FE) <> 0 then
      FReadOnlyFlag:= True;

  Result:= False;

  if not FileExists(FileName) then
  begin
    Error:= Error + [eFileNotFound];
    Exit;
  end;

  FFileName:= FileName;

  {$IFDEF MY_DATA_STREAM}
  FBody:= TMappedStream.Create;
  {$ENDIF}
  {$IFNDEF MY_DATA_STREAM}
  FBody:= TMemoryStream.Create;
  {$ENDIF}


  try
    FBody.LoadFromFile(FileName);
  except
    Error:= Error + [eUnknownError];
    Exit;
  end;

  FDataSize:= FBody.Size;
  FSaveFileDate:= FileDateToDateTime(FileAge(FFileName));
  FSaveFileSize:= FBody.Size;

  if FDataSize < 17 then
  begin
    Error:= Error + [eIncorrectFile];
    Exit;
  end;

  FBody.Position:= 0;
  FBody.ReadBuffer(FileID, 8);
{  if not CompareMem(@FileID, @AviconFileID, 8) then
  begin
    Error:= Error + [eIncorrectFile];
    Exit;
  end;
}
  FBody.ReadBuffer(DeviceID, 8);
  FBody.ReadBuffer(DeviceVer, 1);

  if (DeviceVer <> 1) and (DeviceVer <> 2) and (DeviceVer <> 3) then
  begin
    Error:= Error + [eIncorrectFileVersion];
    Exit;
  end;

  FFullHeader:=       (DeviceID[7] and $01) <> 0; // ���� � ����������� ����������
  FExtendedChannels:= (DeviceID[7] and $02) <> 0; // ���������� ���������� �������

  GetGoodID(DeviceID);

  if FFullHeader then
  begin
    if FDataSize < SizeOf(TFileHeader) then
    begin
      Error:= Error + [eIncorrectFile];
      Exit;
    end;

    // �������� ��������� �����, ��� ������ ���������� �� ������ � ������ �� ��������, ���������� ������������ �� HeaderVer

    try
      FBody.Position:= 0;
      FBody.ReadBuffer(FHead, SizeOf(TFileHeader));
      FConfig:= ConfigList.GetItemByID(FHead.DeviceID, FHead.DeviceVer);
    except
      Error:= Error + [eUnknownError];
      Exit;
    end;

    //if isA15Old or isUSK
    // or isFilus_X27_Scheme1 or isFilus_X27W_Scheme1 or isFilus_X27W_Scheme2
    //then
    //begin
    //  Error:= Error + [eFileisNoLongerSupported];
    //  Exit;
    //end;

    {
    if (not isA31) and (not isA15New) then
    begin
      Error:= Error + [eFileisNotSupported];
      Exit;
    end;
    }
    if not Assigned(FConfig) then
    begin
      Error:= Error + [eIncorrectFileVersion];
      Exit;
    end;

    if FHead.HeaderVer = 1 then
    begin
      FHead.PathCoordSystem:= Ord(csMetricRF); // � ������ 1 �� ���� ������ ������ �������� � ��������� ���������� - ������ �����
    end
    else
    if FHead.HeaderVer > 7 then //6
    begin
      Error:= Error + [eIncorrectFileVersion];
      Exit;
    end;
  end
  else
  begin
    FConfig:= ConfigList.GetItemByID(DeviceID, FHead.DeviceVer);
    FHead.ScanStep:= 100;
  end;


  for I := 0 to 15 do FChFileIdxMask[I]:= False; // ����� ������������ � ����� �������
  for I := 0 to FConfig.ScanChannelCount - 1 do
  begin
    FChFileIdxMask[FConfig.ScanChannelByIdx[I].BS_Ch_File_Number]:= True;
    SetLength(FChFileIndex, Length(ChFileIndex) + 1); // ������ �������� ������� �����
    FChFileIndex[High(FChFileIndex)]:= FConfig.ScanChannelByIdx[I].BS_Ch_File_Number;
  end;

  // ����������� ���� ����� - ������ / �����
  // �������� �� �������������� �� �� ������ � AviconDataContainer
{  FNewFileFormat:= False;
  for I := 0 to 15 do
    if FHead.ChIdxtoCID[I] <> 0 then
    begin
      FNewFileFormat:= True;
      Break;
    end;
 }
  LoadExData;
  if FUnknownFileVersion then
  begin
    Error:= Error + [eIncorrectFileVersion];
    Exit;
  end;

  Result:= FillEventsData;
  GenerateGPSPoints;
  MakeNoteBookFromNordco;

  SensorPos:= 0;
  AirBrushPos:= 0;
  if PaintSystemParamsExists then
    GetSystemPos(FHead.DeviceID, PaintSystemParams, FHead.ControlDirection, Addr(SensorPos), Addr(AirBrushPos));

  SetLength(FBScanChList, 0);
  if isA31_Scheme1 or isA31Estonia_Scheme2 then
  begin
    SetLength(FBScanChList, 9);
    FBScanChList[ 0]:=  1;
    FBScanChList[ 1]:=  2;
    FBScanChList[ 2]:=  3;
    FBScanChList[ 3]:=  4;
    FBScanChList[ 4]:=  5;
    FBScanChList[ 5]:=  6;
    FBScanChList[ 6]:=  7;
    FBScanChList[ 7]:= 10;
    FBScanChList[ 8]:= 11;
  end
  else
  if isA31_Scheme2 then
  begin
    SetLength(FBScanChList, 13);
    FBScanChList[ 0]:=  1;
    FBScanChList[ 1]:=  2;
    FBScanChList[ 2]:=  3;
    FBScanChList[ 3]:=  4;
    FBScanChList[ 4]:=  5;
    FBScanChList[ 5]:=  6;
    FBScanChList[ 6]:=  7;
    FBScanChList[ 7]:= 10;
    FBScanChList[ 8]:= 11;
    FBScanChList[ 9]:= 12;
    FBScanChList[10]:= 13;
    FBScanChList[11]:= 14;
    FBScanChList[12]:= 15;
  end
  else
  if isA15New then
  begin
    SetLength(FBScanChList, 8);
    FBScanChList[ 0]:=  0;
    FBScanChList[ 1]:=  1;
    FBScanChList[ 2]:=  2;
    FBScanChList[ 3]:=  3;
    FBScanChList[ 4]:=  4;
    FBScanChList[ 5]:=  5;
    FBScanChList[ 6]:=  6;
    FBScanChList[ 7]:=  7;
  end;

  SetLength(FBScanChListOK, 16);
  for I := 0 to 15 do FBScanChListOK[I]:= False;
  for I := 0 to High(FBScanChList) do FBScanChListOK[FBScanChList[I]]:= True;


  if FHead.UncontrolledSectionMinLen = 0 then FHead.UncontrolledSectionMinLen:= 60;



  {$IFDEF DEBUG_CutExHeader}
  CutExHeader;
  {$ENDIF}

//  Show-Message(IntToStr(FHead.UncontrolledSectionMinLen));

//  FHead.UsedItems[uiGPSTrackinDegrees]:= 1;
//  FHead.UsedItems[uiAcousticContact]:= 1;
end;
(*
function TAviconDataSource.LoadFromStream(Stream: TStream): Boolean;
var
  ID: Word;
  I: Integer;
  DeviceVer: Byte;
  FileAttr: Integer;
  FileID: TAviconID;
  DeviceID: TAviconID;

begin
  FLoadFromStream:= True;
  FReadOnlyFlag:= True;
  Result:= False;
  FBody:= TMemoryStream.Create;
  FBody.LoadFromStream(Stream);
  FDataSize:= FBody.Size;
  FSaveFileSize:= FBody.Size;

  if FDataSize < 17 then
  begin
    Error:= Error + [eIncorrectFile];
    Exit;
  end;

  FBody.Position:= 0;
  FBody.ReadBuffer(FileID, 8);
  if not CompareMem(@FileID, @AviconFileID, 8) then
  begin
    Error:= Error + [eIncorrectFile];
    Exit;
  end;

  FBody.ReadBuffer(DeviceID, 8);
  FBody.ReadBuffer(DeviceVer, 1);
  FFullHeader:= (DeviceID[7] and $01) <> 0; // ���� � ����������� ����������

  if FFullHeader then
  begin
    if FDataSize < SizeOf(TFileHeader) then
    begin
      Error:= Error + [eIncorrectFile];
      Exit;
    end;

    try
      FBody.Position:= 0;
      FBody.ReadBuffer(FHead, SizeOf(TFileHeader));
      FConfig:= ConfigList.GetItemByID(FHead.DeviceID, FHead.DeviceVer);
    except
      Error:= Error + [eUnknownError];
      Exit;
    end;
  end
  else
  begin
    FConfig:= ConfigList.GetItemByID(DeviceID, DeviceVer);
    FHead.ScanStep:= 100;
  end;

  LoadExData;
  if FUnknownFileVersion then
  begin
    Error:= Error + [eIncorrectFileVersion];
    Result:= False;
    Exit;
  end;

  FillEventsData;

  SetLength(FBScanChList, 0);
  if isA31_Scheme1 then
  begin
    SetLength(FBScanChList, 9);
    FBScanChList[ 0]:=  1;
    FBScanChList[ 1]:=  2;
    FBScanChList[ 2]:=  3;
    FBScanChList[ 3]:=  4;
    FBScanChList[ 4]:=  5;
    FBScanChList[ 5]:=  6;
    FBScanChList[ 6]:=  7;
    FBScanChList[ 7]:= 10;
    FBScanChList[ 8]:= 11;
  end;
  if isA31_Scheme2 then
  begin
    SetLength(FBScanChList, 13);
    FBScanChList[ 0]:=  1;
    FBScanChList[ 1]:=  2;
    FBScanChList[ 2]:=  3;
    FBScanChList[ 3]:=  4;
    FBScanChList[ 4]:=  5;
    FBScanChList[ 5]:=  6;
    FBScanChList[ 6]:=  7;
    FBScanChList[ 7]:= 10;
    FBScanChList[ 8]:= 11;
    FBScanChList[ 9]:= 12;
    FBScanChList[10]:= 13;
    FBScanChList[11]:= 14;
    FBScanChList[12]:= 15;
  end;

  SetLength(FBScanChListOK, 16);
  for I := 0 to 15 do FBScanChListOK[I]:= False;
  for I := 0 to High(FBScanChList) do FBScanChListOK[FBScanChList[I]]:= True;

  Result:= True;
end;
*)
function TAviconDataSource.LoadHeader(FileName: string): Boolean;
begin
end;

function TAviconDataSource.ExtractTestRecordFile(FileName: string): Boolean;
var
  tmp1: TMemoryStream;
  tmp2: Pointer;

begin
  if FTestRecordFileExists then
  begin
    FileBody.Position:= FTestRecordFileOffset + 4;
    GetMem(tmp2, FTestRecordFileSize);
    FileBody.ReadBuffer(tmp2^, FTestRecordFileSize);
    tmp1:= TMemoryStream.Create;
    tmp1.WriteBuffer(tmp2^, FTestRecordFileSize);
    FreeMem(tmp2);
    tmp2:= nil;
    tmp1.SaveToFile(FileName);
    tmp1.Free;
  end;
  Result:= FTestRecordFileExists;
end;

function TAviconDataSource.DisToSysCoord(Coord: Integer; Force: Boolean = False): Integer;
var
  LeftIdx, RightIdx: Integer;
  Del: Integer;

begin
  Coord:= NormalizeDisCoord(Coord);
  LeftIdx:= GetLeftEventIdx(Coord, Force);

  if (not FSkipBM) or (FSkipBM and (not Force))  then
  begin
    if FEvents[LeftIdx].DisCoord = Coord then Result:= FEvents[LeftIdx].SysCoord
    else
    begin
      RightIdx:= GetRightEventIdx(Coord);
      Del:= (FEvents[RightIdx].DisCoord - FEvents[LeftIdx].DisCoord);
      if Del <> 0 then Result:= Round(FEvents[LeftIdx].SysCoord + ((Coord - FEvents[LeftIdx].DisCoord) / Del) * (FEvents[RightIdx].SysCoord - FEvents[LeftIdx].SysCoord))
                  else Result:= FEvents[LeftIdx].SysCoord;
    end;
  end else

  if FSaveEventsData[LeftIdx].DisCoord = Coord then Result:= FSaveEventsData[LeftIdx].SysCoord
  else
  begin
    RightIdx:= GetRightEventIdx(Coord);
    Del:= (FSaveEventsData[RightIdx].DisCoord - FSaveEventsData[LeftIdx].DisCoord);
    if Del <> 0 then Result:= Round(FSaveEventsData[LeftIdx].SysCoord + ((Coord - FSaveEventsData[LeftIdx].DisCoord) / Del) * (FSaveEventsData[RightIdx].SysCoord - FSaveEventsData[LeftIdx].SysCoord))
                else Result:= FSaveEventsData[LeftIdx].SysCoord;
  end;

end;

procedure TAviconDataSource.DisToDisCoords(Coord: Integer; var Res: TIntegerDynArray);
var
  I, J: Integer;
begin
  Coord:= DisToSysCoord(NormalizeDisCoord(Coord));
  for I:= 0 to EventCount - 2 do
    if (FEvents[I].SysCoord <= Coord) and (FEvents[I + 1].SysCoord >= Coord) or
       (FEvents[I].SysCoord >= Coord) and (FEvents[I + 1].SysCoord <= Coord) then
    begin
      J:= FEvents[I + 0].DisCoord + Abs(Coord - FEvents[I + 0].SysCoord);
      if (Length(Res) = 0) or (J <> Res[ High(Res)]) then
      begin
        SetLength(Res, Length(Res) + 1);
        Res[ High(Res)]:= J;
      end;
    end;
end;

procedure TAviconDataSource.DisToDisCoords_(Coord: Integer; var Res: TIntegerDynArray; var StIdx, Count: Integer);
var
  I, J: Integer;
  Flg: Boolean;

begin
  Flg:= False;

  Coord:= DisToSysCoord(NormalizeDisCoord(Coord));
  for I:= StIdx to EventCount - 2 do
    if (FEvents[I].SysCoord <= Coord) and
      (FEvents[I + 1].SysCoord >= Coord) or
      (FEvents[I].SysCoord >= Coord) and
      (FEvents[I + 1].SysCoord <= Coord) then
    begin
      if not Flg then
      begin
        StIdx:= I;
        Flg:= True;
      end;
      J:= FEvents[I + 0].DisCoord + Abs(Coord - FEvents[I + 0].SysCoord);
      // if (Length(Res) = 0) or (J <> Res[High(Res)]) then
      if (Count = 0) or ((Count <> 0) and (J <> Res[Count - 1])) then
      begin
        Res[Count]:= J;
        Inc(Count);

        // SetLength(Res, Length(Res) + 1);
        // Res[High(Res)]:= J;
      end;
    end;
end;

function TAviconDataSource.TestChangeDirCrd(Coord: Integer): Boolean;
var
  I: Integer;

begin
  for I:= 0 to EventCount - 1 do
    if (FEvents[I].DisCoord = Coord) and
      (FEvents[I].ID in [EID_FwdDir, EID_BwdDir]) then
    begin
      Result:= True;
      Exit;
    end;
end;

function TAviconDataSource.SysToDisCoord(Coord: Integer): Integer;
var
  I: Integer;

begin
{
  if Coord > FMaxSysCoord then
  begin
    Result:= MaxDisCoord;
    Exit;
  end;
  Result:= 0;
  for I:= 0 to High(FEvents) - 1 do
    if (Coord >= FEvents[I + 0].SysCoord) and (Coord <= FEvents[I + 1].SysCoord) then
    begin
      Result:= FEvents[I + 0].DisCoord + Abs(FEvents[I + 0].SysCoord - Coord);
      Exit;
    end;
}

  Result:= 0;
  for I:= 0 to High(FEvents) - 1 do // ����� �� �������
  begin
    if (Coord >= FEvents[I + 0].SysCoord) and (Coord <= FEvents[I + 1].SysCoord) then
    begin
      Result:= FEvents[I + 0].DisCoord + Abs(FEvents[I + 0].SysCoord - Coord);
      Exit;
    end;
  end;

  for I:= 0 to High(FEvents) - 1 do // ����� �� ������
  begin
    if (Coord <= FEvents[I + 0].SysCoord) and (Coord >= FEvents[I + 1].SysCoord) then
    begin
      Result:= FEvents[I + 0].DisCoord + Abs(FEvents[I + 0].SysCoord - Coord);
      Exit;
    end;
  end;

  if Coord > FMaxSysCoord then Result:= FMaxSysCoord;
//  if Coord < FMinSysCoord then Result:= 0;

end;

// -------------< ������� ��������� - ����������� �� >-----------------------------------

function TAviconDataSource.DisToMRFCrdPrm(DisCoord: Integer; var CoordParams: TMRFCrdParams): Boolean;
var
  ID: Byte;
  I, J: Integer;
  pData: PEventData;
  StartIdx: Integer;
  MMLen: Integer;
  LeftFlg: Boolean;
  RightFlg: Boolean;
  Post: TMRFCrd;

begin
  DisCoord:= NormalizeDisCoord(DisCoord);

                                      // ����� ������ �������������� ����� �� DisCoord
  LeftFlg:= False;
  for I:= GetLeftEventIdx(DisCoord) downto 0 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_Stolb then
    begin
      CoordParams.LeftStolb[ssLeft].Km:= pCoordPost(pData)^.Km[0];
      CoordParams.LeftStolb[ssLeft].Pk:= pCoordPost(pData)^.Pk[0];
      CoordParams.LeftStolb[ssRight].Km:= pCoordPost(pData)^.Km[1];
      CoordParams.LeftStolb[ssRight].Pk:= pCoordPost(pData)^.Pk[1];
      CoordParams.ToLeftStolbMM:= Round((DisToSysCoord(DisCoord) - FEvents[I].SysCoord) / 100 * FHead.ScanStep);
      CoordParams.LeftIdx:= I;
      LeftFlg:= True;
      Break;
    end;
  end;
                                              // ����� ������ �������������� ������ �� DisCoord
  RightFlg:= False;
  for I:= GetRightEventIdx(DisCoord) to High(FEvents) do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_Stolb then
    begin
      CoordParams.RightStolb[ssLeft].Km:= pCoordPost(pData)^.Km[0];
      CoordParams.RightStolb[ssLeft].Pk:= pCoordPost(pData)^.Pk[0];
      CoordParams.RightStolb[ssRight].Km:= pCoordPost(pData)^.Km[1];
      CoordParams.RightStolb[ssRight].Pk:= pCoordPost(pData)^.Pk[1];
      CoordParams.ToRightStolbMM:= Round((FEvents[I].SysCoord - DisToSysCoord(DisCoord)) / 100 * FHead.ScanStep);
      CoordParams.RightIdx:= I;
      RightFlg:= True;
      Break;
    end;
  end;

  if not LeftFlg then // ��� ������ ����� - ��������� �� ��������� ��������� � ������
  begin
    Post.Km:= FHead.StartKM;
    Post.Pk:= FHead.StartPk;
    Post:= GetPrevMRFPost(Post, FHead.MoveDir);
    CoordParams.LeftStolb[ssLeft].Km:= Post.Km;
    CoordParams.LeftStolb[ssLeft].Pk:= Post.Pk;
    CoordParams.LeftStolb[ssRight].Km:= FHead.StartKM;
    CoordParams.LeftStolb[ssRight].Pk:= FHead.StartPk;
    try
      if FHead.MoveDir > 0 then CoordParams.ToLeftStolbMM:= Round(FHead.StartMetre * 1000 + DisToSysCoord(DisCoord) / 100 * FHead.ScanStep)
                           else CoordParams.ToLeftStolbMM:= (100 - FHead.StartMetre) * 1000 + DisToSysCoord(DisCoord) * FHead.ScanStep div 100;
    except

      CoordParams.ToLeftStolbMM:= 0;
    end;
    CoordParams.LeftIdx:= - 1;
  end;
  if not RightFlg then // ��� ������ ������ - ��������� �� ��������� �������� ��������� � ������
  begin

    CoordParams.RightStolb[ssLeft].Km:= CoordParams.LeftStolb[ssRight].Km;
    CoordParams.RightStolb[ssLeft].Pk:= CoordParams.LeftStolb[ssRight].Pk;
    Post.Km:= CoordParams.LeftStolb[ssRight].Km;
    Post.Pk:= CoordParams.LeftStolb[ssRight].Pk;
    Post:= GetNextMRFPost(Post, FHead.MoveDir);
    CoordParams.RightStolb[ssRight].Km:= Post.Km;
    CoordParams.RightStolb[ssRight].Pk:= Post.Pk;
    CoordParams.ToRightStolbMM:= 100000 - CoordParams.ToLeftStolbMM; // ���� ��� ������ ������ ������ ��� ����� ������ ����� 100 ������
    CoordParams.RightIdx:= - 1;

(*
    CoordParams.RightStolb[ssLeft].Km:= FExHeader.EndMRFCrd.Km;
    CoordParams.RightStolb[ssLeft].Pk:= FExHeader.EndMRFCrd.Pk;
    Post.Km:= FExHeader.EndMRFCrd.Km;
    Post.Pk:= FExHeader.EndMRFCrd.Pk;
    Post:= GetNextMRFPost(Post, FHead.MoveDir);
    CoordParams.RightStolb[ssRight].Km:= Post.Km;
    CoordParams.RightStolb[ssRight].Pk:= Post.Pk;
    CoordParams.ToRightStolbMM:= 100000 - CoordParams.ToLeftStolbMM; // ���� ��� ������ ������ ������ ��� ����� ������ ����� 100 ������
    CoordParams.RightIdx:= - 1;
*)
  end;
end;

function TAviconDataSource.DisToMRFCrd(DisCoord: Longint): TMRFCrd;
var
  CoordParams: TMRFCrdParams;

begin
  DisToMRFCrdPrm(DisCoord, CoordParams);
  Result:= MRFCrdParamsToCrd(CoordParams, Header.MoveDir);
end;

function TAviconDataSource.MRFCrdToDisCrd(RFC: TMRFCrd; var DisCoord: Longint): Boolean;
var
  I, J: Integer;
  ID: Byte;
  pData: PEventData;
  Len: Integer;

begin
  if (FHead.MoveDir > 0) then
  begin
    if (FHead.StartKM = RFC.Km) and
       (FHead.StartPk = RFC.Pk) then
    begin
      Result:= True;
      DisCoord:= SysToDisCoord(Round((RFC.mm - FHead.StartMetre * 1000) / (FHead.ScanStep / 100)));
      Exit;
    end;

    for I:= 0 to EventCount - 1 do
    begin
      GetEventData(I, ID, pData);
      if (ID = EID_Stolb) and
         (pCoordPost(pData)^.Km[1] = RFC.Km) and
         (pCoordPost(pData)^.Pk[1] = RFC.Pk) then
      begin
        Result:= True;
        DisCoord:= SysToDisCoord(DisToSysCoord(Event[I].DisCoord) + Round(RFC.mm / (FHead.ScanStep / 100)));
        Exit;
      end;
    end;
  end
  else
  begin
    if (FHead.StartKM = RFC.Km) and
       (FHead.StartPk = RFC.Pk) then
    begin

      for I:= 0 to EventCount - 1 do // ���� �� ������ ?
      begin
        GetEventData(I, ID, pData);
        if (ID = EID_Stolb) then             // ����
        begin
          if (pCoordPost(pData)^.Km[0] = RFC.Km) and  //
             (pCoordPost(pData)^.Pk[0] = RFC.Pk) then
          begin
            Len:= 0;
            DisCoord:= SysToDisCoord(DisToSysCoord(Event[I].DisCoord) - Round((RFC.mm + Len) / (FHead.ScanStep / 100)));
            Exit;
          end;
        end;
      end;

      Result:= True;
      DisCoord:= SysToDisCoord(Round((FHead.StartMetre * 1000 - RFC.mm) / (FHead.ScanStep / 100)));
      Exit;
    end;

    for I:= 0 to EventCount - 1 do
    begin
      GetEventData(I, ID, pData);
      if (ID = EID_Stolb) and
         (pCoordPost(pData)^.Km[1] = RFC.Km) and
         (pCoordPost(pData)^.Pk[1] = RFC.Pk) then
      begin

        for J:= I + 1 to EventCount - 1 do // ���� �� ������ ����� ?
        begin
          GetEventData(J, ID, pData);
          if ID = EID_Stolb then           // ���� ����� ����������� �� ����
          begin
            Result:= True;
            DisCoord:= SysToDisCoord(DisToSysCoord(Event[J].DisCoord) - Round(RFC.mm / (FHead.ScanStep / 100)));
            Exit;
          end;
        end;
                                           // ���, ����� ������� ��� ����� ������ 100 �
        Result:= True;
        DisCoord:= SysToDisCoord(DisToSysCoord(Event[I].DisCoord) + Round((100000 - RFC.mm) / (FHead.ScanStep / 100)));
        Exit;
      end;
    end;
  end;
end;

  // -------------< ������� ��������� - ��������� >----------------------------------------

function TAviconDataSource.DisToCaCrdPrm(DisCoord: Integer; var CoordParams: TCaCrdParams): Boolean;
var
  StartIdx: Integer;
  MMLen: Integer;
  ID: Byte;
  I, J: Integer;
  LeftFlg: Boolean;
  RightFlg: Boolean;
  pData: PEventData;

begin
  try

    CoordParams.Sys:= CoordSys;
                                      // ����� ������ �������������� ����� �� DisCoord
    LeftFlg:= False;
    for I:= GetLeftEventIdx(DisCoord) downto 0 do
    begin
      GetEventData(I, ID, pData);
      if ID = EID_StolbChainage then
      begin
        CoordParams.LeftPostVal:= pCoordPostChainage(pData)^.Val;
        CoordParams.ToLeftPostMM:= (DisToSysCoord(DisCoord) - FEvents[I].SysCoord) * FHead.ScanStep div 100;
        CoordParams.LeftIdx:= I;
        LeftFlg:= True;
        Break;
      end;
    end;
                                      // ����� ������ �������������� ������ �� DisCoord
    RightFlg:= False;
    for I:= GetRightEventIdx(DisCoord) to High(FEvents) do
    begin
      GetEventData(I, ID, pData);
      if ID = EID_StolbChainage then
      begin
        CoordParams.RightPostVal:= pCoordPostChainage(pData)^.Val;
        CoordParams.ToRightPostMM:= (FEvents[I].SysCoord - DisToSysCoord(DisCoord)) * FHead.ScanStep div 100;
        CoordParams.RightIdx:= I;
        RightFlg:= True;
        Break;
      end;
    end;

    if not LeftFlg then // ��� ������ ����� - ��������� �� ��������� �������
    begin
      if FHead.MoveDir > 0 then
      begin
        CoordParams.LeftPostVal:= GetPrevCaPost(CoordSys,
                                                FHead.StartChainage,
                                                FHead.MoveDir); // �������� ����� ������� ��� �����
        CoordParams.ToLeftPostMM:= GetMMLenFromCaPost(CoordSys,
                                                      FHead.StartChainage) +
                                   DisToSysCoord(DisCoord) * FHead.ScanStep div 100;

        //      ���������
        //        �����                  StartChainage            DisCoord
        //          |                         |                       |
        //     ---- X <---------------------> X <-------------------> X ------------
        //          |     MMLenFromCaPost        DisToSys * ScanStep  |
        //          |                                                 |
        //          x <---------------- ToLeftPostMM ---------------> X

      end
      else
      begin
        CoordParams.LeftPostVal:= FHead.StartChainage; // �������� ����� ������� ��� �����
        CoordParams.LeftPostVal.YYY:= 0;
        CoordParams.LeftPostVal.XXX:= CoordParams.LeftPostVal.XXX + 1;

        CoordParams.ToLeftPostMM:= PostMMLen(CoordSys) -
                                   GetMMLenFromCaPost(CoordSys, FHead.StartChainage) +
                                   DisToSysCoord(DisCoord) * FHead.ScanStep div 100;

        //      LeftPostVal             StartChainage            DisCoord         RightPostVal
        //          |                         |                       |                |
        //     -----|-------------------------|-----------------------|----------------|-------
        //                                    |<- DisToSys*ScanStep ->|
        //                                    |<----------- MMLenFromCaPost ---------->|
        //          |<------------------------------ PostMMLen ----------------------->|
        //          |<----------------- ������� �������� ------------>|

      end;
      CoordParams.LeftIdx:= - 1;
    end;

    if not RightFlg  then // ��� ������ ������
    begin
      if FHead.MoveDir > 0 then
      begin
        CoordParams.RightPostVal:= CoordParams.LeftPostVal;         // �������� ����� - ������
        CoordParams.RightPostVal.XXX:= CoordParams.RightPostVal.XXX + 1;
        CoordParams.RightPostVal.YYY:= 0;
        CoordParams.ToRightPostMM:= PostMMLen(CoordSys) - CoordParams.ToLeftPostMM;

        //
        //        �����                    DisCoord               EndCaCrd         RightPostVal
        //          |                         |                       |                  |
        //     ---- x <---------------------> x-----------------------x------------------x----
        //          |     ToLeftPostMM        |                                          |
        //          x <--------------------------- PostMMLen --------------------------> x
        //                                    |                                          |
        //                                    x <------ PostMMLen - ToLeftPostMM ------> x

      end
      else
      begin
        CoordParams.RightPostVal:= CoordParams.LeftPostVal;
        CoordParams.RightPostVal.YYY:= 0;
        if CoordParams.RightPostVal.YYY = 0 then Dec(CoordParams.RightPostVal.XXX)
                                            else CoordParams.RightPostVal.YYY:= 0;
        CoordParams.ToRightPostMM:= PostMMLen(CoordSys) - CoordParams.ToLeftPostMM;
      end;
      CoordParams.RightIdx:= - 1;
    end;
  except

  end;
end;

function TAviconDataSource.DisToCaCrd(DisCoord: Longint): TCaCrd;
var
  CoordParams: TCaCrdParams;

begin
  DisToCaCrdPrm(DisCoord, CoordParams);
  Result:= CaCrdParamsToCrd(CoordParams, Header.MoveDir);
end;

function TAviconDataSource.CaCrdToDis(CaCrd: TCaCrd; var DisCoord: Longint): Boolean;
var
  ID: Byte;
  I, J, Sys, Len: Integer;
  pData: PEventData;
  Val1, Val2: TCaCrd;
  DisCrd: Integer;
  t1: int64;
  t2: int64;
  t3: int64;

begin
  if (FHead.MoveDir > 0) then
  begin
    if CaCrd.XXX < FHead.StartChainage.XXX then // ���������� ������ ��������� ���������� �����
    begin
      DisCoord:= SysToDisCoord(Round((CaCrdToMM(CoordSys, CaCrd) - CaCrdToMM(CoordSys, FHead.StartChainage)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

    Val1:= CaCrd;
    Val1.XXX:= 0;

    DisCrd:= 0;                                 // ���������� ����� ��������� ���������� �����
    Val2:= FHead.StartChainage;
    if FHead.StartChainage.XXX = CaCrd.XXX then
    begin
      Val2.XXX:= 0;
      DisCoord:= SysToDisCoord(Round((CaCrdToMM(CoordSys, Val1) - CaCrdToMM(CoordSys, Val2)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

    for I:= 0 to EventCount - 1 do              // ���� ����� �������� "�����" �������� ����������
    begin
      GetEventData(I, ID, pData);
      if ID = EID_StolbChainage then
      begin
        Val2:= pCoordPostChainage(pData)^.Val;
        DisCrd:= Event[I].DisCoord;
        if Val2.XXX = CaCrd.XXX then
        begin
          Val2.XXX:= 0;
          DisCoord:= SysToDisCoord(DisToSysCoord(DisCrd) + Round((CaCrdToMM(CoordSys, Val1) - CaCrdToMM(CoordSys, Val2)) / (FHead.ScanStep / 100)));
          Result:= True;
          Exit;
        end;
      end;
    end;

    if CaCrd.XXX > Val2.XXX then // ���������� ������ ���������� ���������� ������
    begin
      Val1:= CaCrd;
      DisCoord:= DisCrd + SysToDisCoord(Round((CaCrdToMM(CoordSys, Val1) - CaCrdToMM(CoordSys, Val2)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

  end
  else
  begin

    if CaCrd.XXX > FHead.StartChainage.XXX then // ���������� ������ ��������� ���������� �����
    begin
      DisCoord:= SysToDisCoord(Round((CaCrdToMM(CoordSys, FHead.StartChainage) - CaCrdToMM(CoordSys, CaCrd)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

    Val1:= CaCrd;
    Val1.XXX:= 0;

    DisCrd:= 0;                                 // ���������� ����� ��������� ���������� �����
    Val2:= FHead.StartChainage;
    if FHead.StartChainage.XXX = CaCrd.XXX then
    begin
      Val2.XXX:= 0;
      DisCoord:= SysToDisCoord(Round((CaCrdToMM(CoordSys, Val2) - CaCrdToMM(CoordSys, Val1)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

    for I:= 0 to EventCount - 1 do              // ���� ����� �������� "�����" �������� ����������
    begin
      GetEventData(I, ID, pData);
      if ID = EID_StolbChainage then
      begin
        Val2:= pCoordPostChainage(pData)^.Val;
        DisCrd:= Event[I].DisCoord;
        if Val2.XXX = CaCrd.XXX + 1 then
        begin

          for J:= I + 1 to EventCount - 1 do // ���� �� ������ ����� ?
          begin
            GetEventData(J, ID, pData);
            if ID = EID_StolbChainage then           // ���� ����� ����������� �� ����
            begin
              Result:= True;
              Sys:= Round(CaCrdToMM(CoordSys, Val1) / (FHead.ScanStep / 100));
              DisCoord:= SysToDisCoord(DisToSysCoord(Event[J].DisCoord) - Sys);
              Result:= True;
              Exit;
            end;
          end;
                                             // ���, ����� ������� ��� ����� �������� PostMMLen ��
          Val2.XXX:= 0;
          Sys:= Round((PostMMLen(CoordSys) - CaCrdToMM(CoordSys, Val1)) / (FHead.ScanStep / 100));
          DisCoord:= SysToDisCoord(DisToSysCoord(DisCrd) + Sys);
          Result:= True;
          Exit;
        end;
      end;
    end;

    if CaCrd.XXX < Val2.XXX then // ���������� ������ ���������� ���������� ������
    begin
      Val1:= CaCrd;
      DisCoord:= DisCrd + SysToDisCoord(Round((CaCrdToMM(CoordSys, Val2) - CaCrdToMM(CoordSys, Val1)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

  end;
  Result:= False;
end;

// --------------------------------------------------------------------------------------

function TAviconDataSource.DisToCoordParams(DisCoord: Integer; var CrdParams: TCrdParams): Boolean;
begin
  CrdParams.Sys:= GetCoordSys;
  if GetCoordSys = csMetricRF then Result:= DisToMRFCrdPrm(DisCoord, CrdParams.MRFCrdParams)
                              else Result:= DisToCaCrdPrm(DisCoord, CrdParams.CaCrdParams);
end;

function TAviconDataSource.DisToRealCoord(DisCoord: Longint): TRealCoord;
begin
  Result.Sys:= GetCoordSys;
  if GetCoordSys = csMetricRF then Result.MRFCrd:= DisToMRFCrd(DisCoord)
                              else Result.CaCrd:= DisToCaCrd(DisCoord);
end;

function TAviconDataSource.RealToDisCoord(Crd: TRealCoord): Longint;
begin
  if Crd.Sys = csMetricRF then MRFCrdToDisCrd(Crd.MRFCrd, Result)
                          else CaCrdToDis(Crd.CaCrd, Result);
end;

// --------------------------------------------------------------------------------------

procedure TAviconDataSource.SaveNB;
begin
  if FModifyData then
    SaveExData(FExHdrLoadOk);
end;

// --------------------------------------------------------------------------------------

//procedure TAviconDataSource.SavePiece(StDisCood, EdDisCood: Integer; SaveNB: Boolean; FileName: string);
//begin
// �������� � ���������
// - ��������� ���������
// - ��������� �����

// ��������� ��������� ���������� �� ���������� StDisCood
// ��������� ��������� ���������� �� ���������� StDisCood

//end;

procedure TAviconDataSource.SavePiece(StDisCood, EdDisCood: Integer; SaveNB: Boolean; FileName: string);
(*
var
  I: Integer;
  DisCoord1: Int64;
  SysCoord1: Integer;
  Offset1: Integer;
  DisCoord2: Int64;
  SysCoord2: Integer;
  Offset2: Integer;
  LoadID: Byte;
  SkipBytes: Integer;
  OutDat: TAvk11DatDst;
  HSItem: THSItem;
  CurSysCoord: Integer;
  ByteOffset: Byte;
  ByteCoord: Byte;
  LongCoord: LongInt;
  LongOffset: LongInt;
  NewCoord: Integer;
  LastSaveOffset: Integer;
  LastSysCrd: Integer;
  BackLink: Integer;
  FID: Byte;
  Params: TMiasParams;
  R: RRail;
  Ch: Integer;
  Rel: TRealCoord;
  OutDat2: TAvk11DatSrc;
  Tmp: TFileNotebook_Ver4;
  CoordParams: TCoordParams;
  Buff: array[5..13] of Byte;
  RightIdx_: Integer;
//  BSHead: mBSAmplHead;
*)
begin
//               ENGLAND
//  StDisCood:= Max(0, StDisCood);
(*
  DisToFileOffset(Max(0, StDisCood), DisCoord1, SysCoord1, Offset1);
  DisToFileOffset(EdDisCood, DisCoord2, SysCoord2, Offset2);
  Rel:= DisToRealCoord(DisCoord1);

  OutDat:= TAvk11DatDst.Create;

  FFileBody.Position:= 0;
  FFileBody.ReadBuffer(OutDat.Header, 128);

  OutDat.FData:= TMemoryStream.Create;
  OutDat.FData.Position:= 0;
  OutDat.Header.StartKM:= Rel.Km;
  OutDat.Header.StartPk:= Rel.Pk;
  OutDat.Header.StartMetre:= Round(Rel.MM / 1000);
  OutDat.Header.TableLink:= $FFFFFFFF;

  OutDat.FData.WriteBuffer(OutDat.Header, 128);

  FFileBody.Position:= Offset1;
  LastSaveOffset:= - 1;

  GetParamFirst(DisCoord1, Params);

  for R:= r_Left to r_Right do
    for Ch:= 0 to GetMaxEvalChannel do
    begin
      OutDat.AddSens(Ord(R), Ch, Params.Sens[R, Ch]);
      OutDat.AddAtt(Ord(R), Ch, Params.Att[R, Ch]);
      OutDat.AddVRU(Ord(R), Ch, Params.VRU[R, Ch]);
      OutDat.AddStStr(Ord(R), Ch, Params.StStr[R, Ch]);
      OutDat.AddEndStr(Ord(R), Ch, Params.EdStr[R, Ch]);
      OutDat.Add2Tp(Ord(R), Ch, Params.DwaTp[R, Ch]);
      OutDat.AddZondImp(Ord(R), Ch, Params.ZondImp[R, Ch]);
    end;

  OutDat.AddMode(Params.Mode);

  if (FFileDevice <> fd29N) then
  begin
    OutDat.AddHeadPh(Params.HeadPh[r_Left, 0], Params.HeadPh[r_Left, 1], Params.HeadPh[r_Left, 2], Params.HeadPh[r_Left, 3], Params.HeadPh[r_Left, 4], Params.HeadPh[r_Left, 5],
                     Params.HeadPh[r_Left, 6], Params.HeadPh[r_Left, 7], Params.HeadPh[r_Left, 8], Params.HeadPh[r_Left, 9], Params.HeadPh[r_Right, 0], Params.HeadPh[r_Right, 1],
                     Params.HeadPh[r_Right, 2], Params.HeadPh[r_Right, 3], Params.HeadPh[r_Right, 4], Params.HeadPh[r_Right, 5], Params.HeadPh[r_Right, 6], Params.HeadPh[r_Right, 7],
                     Params.HeadPh[r_Right, 8], Params.HeadPh[r_Right, 9]);
  end
  else
  begin
    OutDat.AddHeadPh(Params.HeadPh[r_Left, 0],
                     Params.HeadPh[r_Left, 1],
                     Params.HeadPh[r_Left, 2],
                     Params.HeadPh[r_Left, 3],
                     Params.HeadPh[r_Left, 4],
                     Params.HeadPh[r_Left, 5],
                     Params.HeadPh[r_Left, 6],
                     Params.HeadPh[r_Left, 7],
                     Params.HeadPh[r_Left, 8],
                     Params.HeadPh[r_Left, 9],
                     Params.HeadPh[r_Left, 10],
                     Params.HeadPh[r_Left, 11],

                     Params.HeadPh[r_Right, 0],
                     Params.HeadPh[r_Right, 1],
                     Params.HeadPh[r_Right, 2],
                     Params.HeadPh[r_Right, 3],
                     Params.HeadPh[r_Right, 4],
                     Params.HeadPh[r_Right, 5],
                     Params.HeadPh[r_Right, 6],
                     Params.HeadPh[r_Right, 7],
                     Params.HeadPh[r_Right, 8],
                     Params.HeadPh[r_Right, 9],
                     Params.HeadPh[r_Right, 10],
                     Params.HeadPh[r_Right, 11]);
  end;

  CurSysCoord:= SysCoord1;
  LastSysCrd:= 0;
  TestOffsetFirst;
  repeat
    if not TestOffsetNext then Exit;
    FFileBody.ReadBuffer(LoadID, SizeOf(LoadID));

    SkipBytes:= - 1;
    if LoadID and 128 = 0
      then SkipBytes:= Avk11EchoLen[GetEchoCount(LoadID)]
      else
    case LoadID of

   EID_HandScan: begin           // ������
                   FFileBody.Position:= FFileBody.Position - 1;
                   CopyMS(FFileBody, OutDat.FData, 6 + 1);
                   repeat
                     FFileBody.ReadBuffer(HSItem, 3);
                     FFileBody.Position:= FFileBody.Position - 3;
                     if HSItem.Count <= 4 then CopyMS(FFileBody, OutDat.FData, 3 + Avk11EchoLen[HSItem.Count]);
                   until HSItem.Sample = $FFFF;
                 end;

{  EID_NewHeader: begin
                   FFileBody.ReadBuffer(SkipBytes, 4);
                   FFileBody.Position:= FFileBody.Position + SkipBytes;
                   SkipBytes:= - 1;
                 end; }
{
     EID_BSAmpl: begin
                   FFileBody.ReadBuffer(BSHead, SizeOf(mBSAmplHead));
                   FFileBody.Position:= FFileBody.Position + BSHead.Len * 2;
                   SkipBytes:= - 1;
                 end;
}

                 EID_Sens        ,  EID_Att         ,  EID_VRU          , EID_England_ID  ,
                 EID_StStr       ,  EID_EndStr      ,  EID_HeadPh       , EID_Mode        ,
                 EID_2Tp         ,  EID_ZondImp     ,  EID_SetRailType  , EID_Stolb       ,
                 EID_DefLabel    ,  EID_TextLabel   ,  EID_StBoltStyk   , EID_EndBoltStyk ,
                 EID_Time        ,  EID_StLineLabel ,  EID_EndLineLabel , EID_StSwarStyk  ,
                 EID_EndSwarStyk ,  EID_StopPackLeft,  EID_StopPackRight, EID_Strelka     , EID_SpeedState,
                 EID_SezdNo      ,  EID_ExPath      ,  EID_WorkSurface  , EID_Speed       , EID_VerExt      : SkipBytes:= Avk11EventLen[LoadID];

        EID_SysCrd_SS: begin // ��������� ���������� �������� ������, �������� ����������
                         FFileBody.ReadBuffer(ByteOffset, 1);
                         FFileBody.ReadBuffer(ByteCoord, 1);
                         CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or ByteCoord);
                       end;
        EID_SysCrd_SF: begin // ��������� ���������� �������� ������, ������ ����������
                         FFileBody.ReadBuffer(ByteOffset, 1);
                         FFileBody.ReadBuffer(LongCoord, 4);
                         CurSysCoord:= LongCoord;
                       end;
        EID_SysCrd_FS: begin // ��������� ���������� ������ ������, �������� ����������
                         FFileBody.ReadBuffer(LongOffset, 4);
                         FFileBody.ReadBuffer(ByteCoord, 1);
                         CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or ByteCoord);
                       end;
        EID_SysCrd_FF: begin // ��������� ���������� ������ ������, ������ ����������
                         FFileBody.ReadBuffer(LongOffset, 4);
                         FFileBody.ReadBuffer(LongCoord, 4);
                         CurSysCoord:= LongCoord;
                       end;
    end;

    if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then
    begin
      NewCoord:= CurSysCoord - SysCoord1;

      if LastSaveOffset = - 1 then BackLink:= 0
                              else BackLink:= OutDat.FData.Position - LastSaveOffset;

      LastSaveOffset:= OutDat.FData.Position;

      if LastSysCrd and $FFFFFF00 = NewCoord and $FFFFFF00 then
      begin  // �������� ����������
        if BackLink and $FFFFFF00 = 0 then // �������� ������
        begin
          FID:= EID_SysCrd_SS;
          OutDat.FData.WriteBuffer(FID, 1);
          OutDat.FData.WriteBuffer(BackLink, 1);
          OutDat.FData.WriteBuffer(NewCoord, 1);
        end
        else
        begin
          FID:= EID_SysCrd_FS;
          OutDat.FData.WriteBuffer(FID, 1);
          OutDat.FData.WriteBuffer(BackLink, 4);
          OutDat.FData.WriteBuffer(NewCoord, 1);
        end;
      end
      else
      begin  // ������ ����������
        if BackLink and $FFFFFF00 = 0 then // �������� ������
        begin
          FID:= EID_SysCrd_SF;
          OutDat.FData.WriteBuffer(FID, 1);
          OutDat.FData.WriteBuffer(BackLink, 1);
          OutDat.FData.WriteBuffer(NewCoord, 4);
        end
        else
        begin
          FID:= EID_SysCrd_FF;
          OutDat.FData.WriteBuffer(FID, 1);
          OutDat.FData.WriteBuffer(BackLink, 4);
          OutDat.FData.WriteBuffer(NewCoord, 4);
        end;
      end;
      LastSysCrd:= NewCoord;
    end;

    if SkipBytes <> - 1 then
    begin
      FFileBody.Position:= FFileBody.Position - 1;
      CopyMS(FFileBody, OutDat.FData, SkipBytes + 1);
    end;
  until FFileBody.Position >= Offset2;

  if Header.MoveDir = 0 then
  begin
    GetCoord(EdDisCood, CoordParams);

    if CoordSys = csNormal then RightIdx_:= CoordParams.R.RightIdx
                           else RightIdx_:= CoordParams.E.RightIdx;

    if RightIdx_ <> - 1 then
    begin
      NewCoord:= FEventsData[RightIdx_].Event.SysCoord - SysCoord1;

      while NewCoord - LastSysCrd >= 10000 do
      begin
        Inc(LastSysCrd, 10000);
        BackLink:= OutDat.FData.Position - LastSaveOffset;
        LastSaveOffset:= OutDat.FData.Position;
        FID:= EID_SysCrd_FF;
        OutDat.FData.WriteBuffer(FID, 1);
        OutDat.FData.WriteBuffer(BackLink, 4);
        OutDat.FData.WriteBuffer(LastSysCrd, 4);
      end;

      BackLink:= OutDat.FData.Position - LastSaveOffset;
      LastSaveOffset:= OutDat.FData.Position;
      FID:= EID_SysCrd_FF;
      OutDat.FData.WriteBuffer(FID, 1);
      OutDat.FData.WriteBuffer(BackLink, 4);
      OutDat.FData.WriteBuffer(NewCoord, 4);

      FID:= FEventsData[RightIdx_].Event.ID;
      OutDat.FData.WriteBuffer(FID, 1);
      OutDat.FData.WriteBuffer(FEventsData[RightIdx_].Data, Avk11EventLen[FID]);
    end;
  end;

  FID:= EID_EndFile;
  OutDat.FData.WriteBuffer(FID, 1);
  for I:= 5 to 13 do Buff[I]:= $FF;
  BackLink:= OutDat.FData.Position - LastSaveOffset;
  OutDat.FData.WriteBuffer(BackLink, 4);
  OutDat.FData.WriteBuffer(Buff, 9);
  OutDat.FData.SaveToFile(FileName);
  OutDat.Free;

  OutDat2:= TAvk11DatSrc.Create;
  OutDat2.LoadFromFile(FileName);

  if SaveNB then
    for I:= 0 to NotebookCount - 1 do
      if (Notebook[I].DisCoord >= StDisCood) and
         (Notebook[I].DisCoord <= EdDisCood) then
      begin
        Tmp:= Notebook[I]^;
        Tmp.DisCoord:= Tmp.DisCoord - Max(0, StDisCood);
        Tmp.StDisCoord:= Tmp.StDisCoord - Max(0, StDisCood);
        Tmp.EdDisCoord:= Tmp.EdDisCoord - Max(0, StDisCood);
        Tmp.SysCoord:= OutDat2.DisToSysCoord(Tmp.DisCoord);
        OutDat2.NotebookAdd(@Tmp);
      end;

  OutDat2.Free; *)
end;

procedure TAviconDataSource.GenerateGPSPoints;
var
  ID: Byte;
  I, J: Integer;
//  New: TGPSPoint;
  Coord: TRealCoord;
  pData: PEventData;
  str: string;
  OKFlag: Boolean;

  procedure InitNewGPSPoint;
  begin
    NewGPSPoint.DisCoord := 0;
    NewGPSPoint.SysCoord := 0;
    NewGPSPoint.State:= false;
    NewGPSPoint.SatCount:= 0;
    NewGPSPoint.Speed:= 0;
    NewGPSPoint.Lat := NAN;
    NewGPSPoint.Lon := NAN;
  end;
begin

  SetLength(GPSPoints, 0);
  InitNewGPSPoint;
  for I:= 0 to GetEventCount - 1 do
  begin
    GetEventData(I, ID, pData);

    if ID = EID_GPSState then //3
    begin
      if (NewGPSPoint.DisCoord <> Event[I].DisCoord) then
      if not (isNAN(NewGPSPoint.Lat) or isNAN(NewGPSPoint.Lon)) and
        (NewGPSPoint.Lat >= - 90) and (NewGPSPoint.Lat <= 90) and (NewGPSPoint.Lon >= -180) and (NewGPSPoint.Lon <= 180) and (NewGPSPoint.Lat <> 0) and (NewGPSPoint.Lon <> 0) then
      begin
        SetLength(GPSPoints, Length(GPSPoints) + 1);
        GPSPoints[High(GPSPoints)]:= NewGPSPoint;
        InitNewGPSPoint;
      end;
      NewGPSPoint.DisCoord:= Event[I].DisCoord;
      NewGPSPoint.SysCoord:= Event[I].SysCoord;
      NewGPSPoint.State:= Boolean(pedGPSState(pData)^.State);
      NewGPSPoint.SatCount:= pedGPSState(pData)^.UseSatCount;//pedGPSState(pData)^.Reserv[0];
    end;

    if ID = EID_GPSCoord then //2
    begin
      if not (isNAN(NewGPSPoint.Lat) or isNAN(NewGPSPoint.Lon)) and
        (NewGPSPoint.Lat >= - 90) and (NewGPSPoint.Lat <= 90) and (NewGPSPoint.Lon >= -180) and (NewGPSPoint.Lon <= 180) and (NewGPSPoint.Lat <> 0) and (NewGPSPoint.Lon <> 0) then
      if (NewGPSPoint.DisCoord <> Event[I].DisCoord) or
        ((NewGPSPoint.Lat <> pedGPSCoord(pData)^.Lat) or (NewGPSPoint.Lon <> pedGPSCoord(pData)^.Lon)) then
      begin
        SetLength(GPSPoints, Length(GPSPoints) + 1);
        GPSPoints[High(GPSPoints)]:= NewGPSPoint;
        InitNewGPSPoint;
      end;
      NewGPSPoint.Lat:= pedGPSCoord(pData)^.Lat;
      NewGPSPoint.Lon:= pedGPSCoord(pData)^.Lon;
      NewGPSPoint.DisCoord:= Event[I].DisCoord;
      NewGPSPoint.SysCoord:= Event[I].SysCoord;
    end;

    if ID = EID_GPSCoord2 then  //1
    begin
      if not (isNAN(NewGPSPoint.Lat) or isNAN(NewGPSPoint.Lon)) and
        (NewGPSPoint.Lat >= - 90) and (NewGPSPoint.Lat <= 90) and (NewGPSPoint.Lon >= -180) and (NewGPSPoint.Lon <= 180) and (NewGPSPoint.Lat <> 0) and (NewGPSPoint.Lon <> 0) then
      if (NewGPSPoint.DisCoord <> Event[I].DisCoord) or (NewGPSPoint.Lat <> pedGPSCoord2(pData)^.Lat) or
         (NewGPSPoint.Lon <> pedGPSCoord2(pData)^.Lon) then
      begin
        SetLength(GPSPoints, Length(GPSPoints) + 1);
        GPSPoints[High(GPSPoints)]:= NewGPSPoint;
        InitNewGPSPoint;
      end;
      NewGPSPoint.DisCoord:= Event[I].DisCoord;
      NewGPSPoint.SysCoord:= Event[I].SysCoord;

      NewGPSPoint.Lat:= pedGPSCoord2(pData)^.Lat;
      NewGPSPoint.Lon:= pedGPSCoord2(pData)^.Lon;
      NewGPSPoint.Speed:= pedGPSCoord2(pData)^.Speed;
    end;
  end;
  if not (isNAN(NewGPSPoint.Lat) or isNAN(NewGPSPoint.Lon)) and
    (NewGPSPoint.Lat >= - 90) and (NewGPSPoint.Lat <= 90) and (NewGPSPoint.Lon >= -180) and (NewGPSPoint.Lon <= 180) and (NewGPSPoint.Lat <> 0) and (NewGPSPoint.Lon <> 0) then
  begin
    SetLength(GPSPoints, Length(GPSPoints) + 1);
    GPSPoints[High(GPSPoints)]:= NewGPSPoint;
  end;
end;
{
procedure TAviconDataSource.GenerateGPSPoints;
var
  ID: Byte;
  I, J (*, Lat_, Lon_*): Integer;

//  Lat__: Extended;
//  Lon__: Extended;

  New: TGPSPoint;
  Coord: TRealCoord;
  pData: PEventData;
  str: string;
  OKFlag: Boolean;

begin

  SetLength(GPSPoints, 0);
  for I:= 0 to GetEventCount - 1 do
  begin
    GetEventData(I, ID, pData);

    if ID = EID_GPSState then
    begin
      New.DisCoord:= Event[I].DisCoord;
      New.SysCoord:= Event[I].SysCoord;
      New.State:= Boolean(pedGPSState(pData)^.State);
      New.SatCount:= pedGPSState(pData)^.Reserv[0];
      New.Speed:= pedGPSState(pData)^.Reserv[1];

      SetLength(GPSPoints, Length(GPSPoints) + 1);
      GPSPoints[High(GPSPoints)]:= New;
    end;

    if ID = EID_GPSCoord then
    begin
      New.DisCoord:= Event[I].DisCoord;
      New.SysCoord:= Event[I].SysCoord;

      OKFlag:= False;
      try

        New.Lat:= pedGPSCoord(pData)^.Lat;
        New.Lon:= pedGPSCoord(pData)^.Lon;
        New.Speed:= 0;

        if not (isNAN(New.Lat) or isNAN(New.Lon)) then
          OKFlag := (New.Lat >= - 90) and (New.Lat <= 90) and (New.Lon >= -180) and (New.Lon <= 180) and (New.Lat <> 0) and (New.Lon <> 0);

      except
      //  Show-Message('!');

      end;

      if OKFlag then
      begin
        SetLength(GPSPoints, Length(GPSPoints) + 1);
        GPSPoints[High(GPSPoints)]:= New;
      end;
    end;

    if ID = EID_GPSCoord2 then
    begin
      New.DisCoord:= Event[I].DisCoord;

//      Lat_:= pedGPSCoord_(pData)^.Lat;
//      Lon_:= pedGPSCoord_(pData)^.Lon;

      New.Lat:= 0; // pedGPSCoord2(pData)^.Lat;
      New.Lon:= 0; // pedGPSCoord2(pData)^.Lon;
      New.Speed:= pedGPSCoord2(pData)^.Speed;

      if (New.Lat >= - 90) and (New.Lat <= 90) and (New.Lon >= -180) and (New.Lon <= 180) and (New.Lat <> 0) and (New.Lon <> 0) then
      begin
        SetLength(GPSPoints, Length(GPSPoints) + 1);
        GPSPoints[High(GPSPoints)]:= New;
      end;
    end;

  end;
end;
}
procedure TAviconDataSource.MakeNoteBookFromNordco;
var
  ID: Byte;
  I, J: Integer;
  pData: PEventData;

begin
  // -------- ������� NORDCO -----------------------------------------------------

  if Length(FNotebook) = 0 then
    for I:= 0 to EventCount - 1 do
      if Event[I].ID in [EID_NORDCO_Rec] then
      begin
        GetEventData(I, ID, pData);

        SetLength(FNotebook, Length(FNotebook) + 1);
        J:= High(FNotebook);

        FillChar(&FNotebook[J].ID, SizeOf(TFileNotebook_ver_7), 0);

        FNotebook[J].ID:= 3;  // ���: 1 - ��������� �������� / 2 - ������ �������� / 3 - NORDCO
        FNotebook[J].DisCoord:= pedNORDCO_Rec(pData)^.DisCoord;
        FNotebook[J].AssetType:= pedNORDCO_Rec(pData)^.AssetType; // NORDCO::AssetType - ��� �������� �������� ���� - �� ������: Left Rail, Right Rail
        FNotebook[J].Defect:= pedNORDCO_Rec(pData)^.Type_; // NORDCO::Type - ��� ������� �� UIC 712
        FNotebook[J].BlokNotText:= pedNORDCO_Rec(pData)^.Comment; // NORDCO::Comment - ����������
        FNotebook[J].DT:= pedNORDCO_Rec(pData)^.SurveyDate; // NORDCO::SurveyDate - ���� ����������� �������
        FNotebook[J].DT:= pedNORDCO_Rec(pData)^.DataEntryDate; // NORDCO::DataEntryDate - ���� ����������� ������ ��������
        FNotebook[J].Line:= pedNORDCO_Rec(pData)^.Line; // �������� ��� ������������� �������������� �����
        FNotebook[J].Track:= pedNORDCO_Rec(pData)^.Track; // ������������� ����, � ������� ��������� ������
        FNotebook[J].LocationFrom:= pedNORDCO_Rec(pData)^.LocationFrom; // ������ ������������� �������
        FNotebook[J].LocationTo:= pedNORDCO_Rec(pData)^.LocationTo; // ����� ������������� �������. � ������ ��������� ������� �� �� �����, ��� ����  "Location From"
        FNotebook[J].LatitudeFrom:= pedNORDCO_Rec(pData)^.LatitudeFrom; // GPS-������ ��� ���� "Location From"
        FNotebook[J].LatitudeTo:= pedNORDCO_Rec(pData)^.LatitudeTo; // GPS-������ ��� ���� "Location To"
        FNotebook[J].LongitudeFrom:= pedNORDCO_Rec(pData)^.LongitudeFrom; // GPS-������� ��� ���� "Location From"
        FNotebook[J].LongitudeTo:= pedNORDCO_Rec(pData)^.LongitudeTo; // GPS-������� ��� ���� "Location To"
        FNotebook[J].Source:= pedNORDCO_Rec(pData)^.Source; // ����� ����������� ������� - �� ������: Ultrasonic Trolley, Visual Inspection
        FNotebook[J].Status:= pedNORDCO_Rec(pData)^.Status; // ��� ���� ���������, ������ �� ������ (�.�. ������ �� ����) - �� ������: Open, Closed
        FNotebook[J].IsFailure:= pedNORDCO_Rec(pData)^.IsFailure; // ���� ���������� ������ ������� �����, �� ��� ���� ������ ����� �������� TRUE. ������� ����� ������� ����� ������, ������� �������� ����������� ��������
        FNotebook[J].Size:= pedNORDCO_Rec(pData)^.Size; // ������ ������� ���  ����������� �������. ��������, ���� ������ ��� �������, �� �� �����.
        FNotebook[J].Size2:= pedNORDCO_Rec(pData)^.Size2; // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
        FNotebook[J].Size3:= pedNORDCO_Rec(pData)^.Size3; // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
        FNotebook[J].UnitofMeasurement:= pedNORDCO_Rec(pData)^.UnitofMeasurement; // ��. ��������� ���� "Size"
        FNotebook[J].UnitofMeasurement2:= pedNORDCO_Rec(pData)^.UnitofMeasurement2; // ��. ��������� ���� "Size2"
        FNotebook[J].UnitofMeasurement3:= pedNORDCO_Rec(pData)^.UnitofMeasurement3; // ��. ��������� ���� "Size3"
        FNotebook[J].Operator_:= pedNORDCO_Rec(pData)^.Operator_; // ��������
        FNotebook[J].Device:= pedNORDCO_Rec(pData)^.Device; // ������

        FNotebook[J].Zoom:= 400; // �������
        FNotebook[J].Reduction:= 2;
        FNotebook[J].ShowChGate_:= True;
        FNotebook[J].ShowChSettings_:= True;
        FNotebook[J].ShowChInfo_:= True;
        FNotebook[J].ViewMode:= 1; // ����� �����������: 1 - � ��������� / 2 - � ���� ������
        FNotebook[J].AmplTh:= 0; // ����� �����������
        FNotebook[J].AmplDon:= 0; // ��������� ������� ������� 0 - ����
        FNotebook[J].ViewChannel:= 0; // ������: 0 - ��� / 1 - ���������� / 2 - �����������
        FNotebook[J].ViewLine:= 0;

        FModifyData:= True;
      end;

end;

function TAviconDataSource.GetRailPathNumber_in_String: string;
begin
  if Header.UsedItems[uiRailPathNumber] = 1 then Result := IntToStr(Header.RailPathNumber);
  if Header.UsedItems[uiRailPathTextNumber] = 1 then Result := HeaderStrToString(Header.RailPathTextNumber);
end;

function TAviconDataSource.GetRailPathNumber_in_HeadStr: THeaderStr;
begin
  if Header.UsedItems[uiRailPathNumber] = 1 then Result := StringToHeaderStr(IntToStr(Header.RailPathNumber));
  if Header.UsedItems[uiRailPathTextNumber] = 1 then Result := Header.RailPathTextNumber;
end;

procedure TAviconDataSource.AnalyzeAC(Debug_: Boolean);
label LoadCont, LoadStop;

var
  CurSysCoord: Integer;
  LastSysCoord: Integer;
  CurDisCoord: Int64;
  SkipBytes: Integer;
  I, J, K, Summ: Integer;
  ErrorFlag: Boolean;
  LoadID: Byte;
  LoadByte: array [1 .. 331] of Byte;
  LoadLongInt: array [1 .. 5] of Longint; // absolute LoadByte[1];
//  Last: Integer;
  R: TRail;
  Ch: Integer;
  ChIdx: Integer;
  CurAKState: array [TRail, 0..16] of Boolean; // Flag AC Exists
  CurAKCoord: array [TRail, 0..16] of Integer; // Lost AC coord or -1 AC exist
  CurEcho: array [TRail, 0..16] of Boolean; // Flag echo signals exists
  Log: array [TRail, 0..16] of TStringList; // Log

  AllChState: Boolean;
  OldAllChState: Boolean;
  AllChCrd: Integer;
  Rail: TRail;

  UpdateACData: Boolean;
  UCSDataIdx: Integer;

  Dat: array [0 .. 7] of Byte;

  BackMotionFlag: Boolean;
  LastBackMotionFlag: Boolean;
  StartBackMotionSysCoord: Integer;

  Dir: Boolean;

  StSysCrd: Integer;
  EdSysCrd: Integer;
  StSysCrdIdx: Integer;
  EdSysCrdIdx: Integer;

  ExHeader__: TExHeader;
  DataType: Integer;
  Time: edTime;
  SaveOffset: Integer;

  BodyBadZones_idx: Integer;

function GetSign(Dir: Integer): Boolean;
begin
  Result:= Dir >= 0;
end;

begin
  if FAnalyzeACExecuted and (not Debug) then Exit;
  FAnalyzeACExecuted:= True;
  Debug:= Debug_;

  SetLength(UCSData, 1000);  // �������������� ����� � ������� �����
  UCSDataIdx:= 0;            // ������ ��� ���������� ������ � ������ �����
  ACEventCount:= 0;          // DEBUG
//  BAD_AC_algorithm_situation:= 0;
//  GOOD_AC_algorithm_situation:= 0;

  CurSysCoord:= 0;           // ������� ��������� ����������
  LastSysCoord:= MaxInt;     // ���������� ��������� ���������� - ����������� � ���� ���� �� ���� ������
  CurDisCoord:= 0;
  AllChState:= True;         // ��������� �� ��� ������ ����� - ����� ��������� ��� ������� �� AND
  OldAllChState:= True;      // ���������� ��������� ��
  UpdateACData:= False;      // ���� ���� ����� ������ ��
//  FillChar(AKGis[0], Length(AKGis) * SizeOf(Integer), 0);
  AllChCrd:= - 1;
  BackMotionFlag:= False;    // ���� - ��������� � ���� �������� ����� - �� ���� � ���� ��� ������� ������� �����
  LastBackMotionFlag:= False; // ���������� ��������� ����� BackMotionFlag

                              // "���������" �������� ������ ...
  for R:= rLeft to rRight do  // ... ��� ���� ����� ...
  begin
    for Ch:= 0 to 16 do       // ... � �������
    begin
      CurAKState[R, Ch]:= True; // ������� ��� ���������� ������� ���� ...
      CurAKCoord[R, Ch]:= - 2;  // ... � ������ ��� ����������
   //   CurEcho[R, Ch]:= False;

      if Debug then // �������
      begin
        AKMinLen[R, Ch]:= maxint;
        AKMaxLen[R, Ch]:= 0;
        AKMediumLen[R, Ch]:= 0;
        AKCount[R, Ch]:= 0;
        AKSummLen[R, Ch]:= 0;
        Log[R, Ch]:= TStringList.Create;
      end;
    end;
    if Debug then // �������
      for Ch:= 0 to 99 do AKExists[R, Ch]:= False;
  end;
                                                      // ������ �����
  if not FFullHeader then FBody.Position:= 17
                     else FBody.Position:= SizeOf(TFileHeader); // ������� ���������

LoadCont :

  // ��������������� ���� ���� - ����������� ������ ����� ��������� ��������� �� (���������� ��� ���� ����� � �������),
  //  ����� ����������� ���������� �� �� ������ ����� ����������� �������� ������� � ��������� ���� �������� �����


  BodyBadZones_idx:= -1;
  for I := 0 to High(FBodyBadZones) do
  begin
    if FBody.Position < FBodyBadZones[I].From then
    begin
      BodyBadZones_idx:= I;
      Break;
    end
    else
    if (FBody.Position >= FBodyBadZones[I].From) and
       (FBody.Position <= FBodyBadZones[I].To_) then
    begin
      FBody.Position:= FBodyBadZones[I].To_;
      BodyBadZones_idx:= I + 1;
      Break;
    end;
  end;
  if Length(FBodyBadZones) <> 0 then
    if BodyBadZones_idx > High(FBodyBadZones) then
      BodyBadZones_idx:= -1;


  if FBody.Position < FDataSize then

  try
    repeat
      SkipBytes:= -1;

      if BodyBadZones_idx <> - 1 then
      begin
        if (FBody.Position >= FBodyBadZones[BodyBadZones_idx].From) and
           (FBody.Position < FBodyBadZones[BodyBadZones_idx].To_) then
        begin
          FBody.Position:= FBodyBadZones[BodyBadZones_idx].To_;
          BodyBadZones_idx:= I + 1;
          if BodyBadZones_idx > High(FBodyBadZones) then BodyBadZones_idx:= -1;
        end;
      end;

      SaveOffset:= FBody.Position;
      FBody.ReadBuffer(LoadID, 1); // �������� �������������� �������

      if LoadID and 128 = 0 then // ������ �������� �-���������
      begin
        FBody.ReadBuffer(Dat[1], EchoLen[LoadID and 3]);
        for I := 1 to LoadID and $03 do
          CurEcho[TRail(LoadID shr 6 and 1), (LoadID shr 2) and 15]:= CurEcho[TRail(LoadID shr 6 and 1), (LoadID shr 2) and 15] or (FSrcDat[1] > 30);
        if FExtendedChannels and (Ch > 14) then FBody.Position:= FBody.Position + 1;
      end
      else
        case LoadID of

          EID_SysCrd_NS:  begin // �������� ����������
                            FBody.ReadBuffer(LoadByte[1], 1);
                            CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[1]);
                          end;
          EID_SysCrd_NF:  begin // - ������ ����������
                            FBody.ReadBuffer(LoadLongInt[2], 4);
                          {  if LoadLongInt[2] > FExHeader.EndSysCoord then FBody.Position:= SaveOffset + 1
                                                                      else} CurSysCoord:= LoadLongInt[2];
                          end;
          EID_EndFile:    begin // ����� �����
                            ErrorFlag:= False;
                            FBody.ReadBuffer(LoadByte, 13);
                            for i := 1 to 13 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                            if not ErrorFlag then
                              if FBody.Position <> FBody.Size then
                              begin
                                if FBody.Size - FBody.Position >= SizeOf(TExHeader) then
                                begin
                                  FBody.ReadBuffer(LoadByte, 170 + 32 + 128);
                                  for i := 1 to 14 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                                  if not ErrorFlag then
                                    for i := 107 to 170 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                                  if not ErrorFlag then
                                    for i := 205 + 2 to 330 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                                  ErrorFlag:= ErrorFlag or not (LoadByte[19] in [0..8]) or (LoadByte[20] + LoadByte[21] + LoadByte[22] <> 0);
                                end;
                              end;
                        //    if ErrorFlag then FBody.Position:= SaveOffset + 1
                        //                 else ; // ��� �� �� ��� ���������!
   (*                       if FBody.Position = 5311 then
                            begin
                              FBody.Position:= FBody.Position - 1; // FHead.TableLink;
                              FBody.ReadBuffer(ExHeader__, SizeOf(ExHeader__));

                              repeat
                                FBody.ReadBuffer(LoadID, 1); // �������� �������������� �������
                                if LoadID = EID_Time then
                                begin
                                  FBody.ReadBuffer(Time, 2); // �������� �������������� �������
                                  if Time.Hour = FHead.Hour then
                                  begin
                                    Break;
                                  end else FBody.Position:= FBody.Position - 1;
                                end;
                              until False;
                            end
                            else
                            begin
                              FBody.ReadBuffer(LoadByte, 9 + 4);

                              DataType:= 1;
                              for i := 1 to 13 do
                                if LoadByte[i] <> $FF then
                                begin
                                  DataType:= 0;
                                  Break;
                                end;

                              if FBody.Position < FHead.TableLink - 10 then DataType:= 0;

                              if DataType = 0 then begin
                                                     FBody.Position:= FCurOffset + 1;
                                                   end
                                              else begin
                                                    // Exit;
                                                     Break;
                                                   end;

                            end;       *)
                          end;
          EID_ACData:     begin                                // �������� ������ ��
                            FBody.ReadBuffer(LoadByte[1], 1);
                            if LoadByte[1] and $01 = 0 then
                            begin

                              Inc(ACEventCount);  // DEBUG

                              Rail:= TRail(LoadByte[1] shr 6 and 1); // ��������� ����
                              Ch:= (LoadByte[1] shr 2) and 15; // ��������� ������
                              CurAKState[Rail, Ch]:= Boolean(LoadByte[1] shr 7 and 1); // ��������� ���������
                              UpdateACData:= UpdateACData or FBScanChListOK[Ch]; // ������� �������� ������ !!! ������ � ���� ������� �� ������ ����� �15 ��� ��� ��������� ������ ������ FBScanChListOK ���������� bool �������� ��� �������������� ������� ������� ��� ����� 1 � ����� 2

                              if Debug then // �������
                                AKCount[Rail, Ch]:= AKCount[Rail, Ch] + 1;

                           //   if Debug then Show-Message('������ ������ ��');

                           {   if Debug then // �������
                                AKExists[Rail, Ch]:= True; }
                            (*
                             //   FBScanChList
                              if (isA31_Scheme1 and (Ch <= 11)) or isA31_Scheme2 then
                              begin

                                //if Debug then // �������
                                //   Log[Rail, Ch].Add(Format('%d %d %d %d', [CurDisCoord, Ord(CurAKState[Rail, Ch]), CurAKCoord[Rail, Ch], CurDisCoord - CurAKCoord[Rail, Ch]]));

                                // Create Gistogramm
                                if CurAKState[Rail, Ch] then
                                begin
                                  if CurAKCoord[Rail, Ch] >= 0 then
                                  begin
                                    I:= CurDisCoord - CurAKCoord[Rail, Ch];
                                    if I <= 100 then Inc(AKGis[I]);
                                    CurAKCoord[Rail, Ch]:= - 1;

                                  l{ if Debug then // �������
                                    begin
                                      AKCount[Rail, Ch]:= AKCount[Rail, Ch] + 1;
                                      AKSummLen[Rail, Ch]:= AKSummLen[Rail, Ch] + I;
                                      AKMinLen[Rail, Ch]:= Min(AKMinLen[Rail, Ch], I);
                                      AKMaxLen[Rail, Ch]:= Max(AKMaxLen[Rail, Ch], I);
                                      AKMediumLen[Rail, Ch]:= AKMediumLen[Rail, Ch] + I;
                                    end; }

                                  end {else if Debug and (CurAKCoord[Rail, Ch] = - 1) then Show-Message('������ ������ ��')}
                                end
                                else if CurAKCoord[Rail, Ch] < 0 then CurAKCoord[Rail, Ch]:= CurDisCoord
                                //                                 else if Debug then Show-Message('������ ������ ��');
                              end; *)
                            end;
                          end;

          EID_SysCrd_SS:  begin // - �������� ������, �������� ����������
                            FBody.ReadBuffer(LoadByte[1], 2);
                            CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[2]);
                          end;
          EID_SysCrd_SF:  begin // - �������� ������, ������ ����������
                            FBody.ReadBuffer(LoadByte[1], 1);
                            FBody.ReadBuffer(LoadLongInt, 4);
                            CurSysCoord:= LoadLongInt[1];
                          end;
          EID_SysCrd_FS:  begin // - ������ ������, �������� ����������
                            FBody.ReadBuffer(LoadLongInt[1], 4);
                            FBody.ReadBuffer(LoadByte[1], 1);
                            CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[1]);
                          end;
          EID_SysCrd_FF:  begin // - ������ ������, ������ ����������
                            FBody.ReadBuffer(LoadLongInt[1], 4);
                            FBody.ReadBuffer(LoadLongInt[2], 4);
                            CurSysCoord:= LoadLongInt[2];
                          end;
          else SkipEvent(LoadID);
        end;

      {$IFDEF FIX_COORD_JUMP}
      if FSkipEventFlag_HandScan then FSkipCoordEventCount:= 15;
      {$ENDIF}


      if LoadID in [EID_SysCrd_NS, EID_SysCrd_NF, EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then
      begin
        if (LastSysCoord <> MaxInt) then // ��� �� ������ ��������� ����������
        begin
          {$IFDEF FIX_COORD_JUMP}
          if (FSkipCoordEventCount <> 0) then // ���������� ������� �� ����� �������
          begin
            if Abs(CurSysCoord - LastSysCoord) > 20 then CurSysCoord:= LastSysCoord;
            Dec(FSkipCoordEventCount);
          end;
(*          if (FSkipCoordEventCount <> 0) then // ���������� ������� �� ����� �������
          begin
            if (LastSysCoord <> MaxInt) and (Abs(CurSysCoord - LastSysCoord) > 200) then CurSysCoord:= LastSysCoord; // ���������� ������� �� ����� �������
            //CurSysCoord:= LastSysCoord;
            Dec(FSkipCoordEventCount);
          end; *)
          {$ENDIF}
          Inc(CurDisCoord, Abs(CurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
        end
        else
        begin
          CurDisCoord:= 0; // ��� ������ ��������� ����������
        end;

        if UpdateACData then // ���� ����� ������ ��
        begin
          AllChState:= True; // ����������� ������ ��������� �� ��� ������ ����� - ����� ��������� ��� ������� �� AND
          for R:= rLeft to rRight do
          begin
            for ChIdx:= 0 to High(FBScanChList) do
              if not CurAKState[R, FBScanChList[ChIdx]] then
              begin
                AllChState:= False;
                Break;
              end;
            if not AllChState then Break;
          end;
        end;

        if (LastSysCoord <> MaxInt) then // ��� �� ������ ��������� ����������
        begin
          if not BackMotionFlag and (CurSysCoord < LastSysCoord) then // ������ �� � ���� �������� ����� � ����� ���������� ������ ����������
          begin
            Dir:= GetSign(CurSysCoord - LastSysCoord); // ���������� ����� ����������� ��������
            BackMotionFlag:= True;                     // ������������ ���� ���� �������� �����
            StartBackMotionSysCoord:= LastSysCoord;    // ��������� ���������� ������ ���� �������� �����

            SetLength(FACPoints, Length(FACPoints) + 1); // ��������� ����� ������ ��
            FACPoints[High(FACPoints)].SysCoord:= CurSysCoord; // ��������� �����������
            FACPoints[High(FACPoints)].DisCoord:= CurDisCoord; // ���������� �����������
            FACPoints[High(FACPoints)].State:= AllChState; // ��������� ��
            FACPoints[High(FACPoints)].BackMotionFlag:= BackMotionFlag; // ���� ���� �������� �����

          end
          else
          if BackMotionFlag then // ������ ��������� � ���� �������� �����
          begin
            if GetSign(CurSysCoord - LastSysCoord) <> Dir then // ���������� ����������� �������� �������
            begin
              // ����� �����������
              Dir:= GetSign(CurSysCoord - LastSysCoord);

              SetLength(FACPoints, Length(FACPoints) + 1); // ��������� ����� ������ ��
              FACPoints[High(FACPoints)].SysCoord:=       CurSysCoord;
              FACPoints[High(FACPoints)].DisCoord:=       CurDisCoord;
              FACPoints[High(FACPoints)].State:=          AllChState;
              FACPoints[High(FACPoints)].BackMotionFlag:= BackMotionFlag;

            end;

            if (CurSysCoord > StartBackMotionSysCoord) then  // ����� �� ���������� ������ ���� �������� ����� - ���� �����������
            begin
              BackMotionFlag:= False; // ���� �������� ����� �����������

              SetLength(FACPoints, Length(FACPoints) + 1); // ��������� ����� ������ ��
              FACPoints[High(FACPoints)].SysCoord      := CurSysCoord;
              FACPoints[High(FACPoints)].DisCoord      := CurDisCoord;
              FACPoints[High(FACPoints)].State         := AllChState;
              FACPoints[High(FACPoints)].BackMotionFlag:= BackMotionFlag;
            end;
          end;
        end;

        if OldAllChState <> AllChState then // ���������� ��������� ��  - ��������� ����� ������ ��
        begin
          SetLength(FACPoints, Length(FACPoints) + 1);
          FACPoints[High(FACPoints)].SysCoord      := CurSysCoord;
          FACPoints[High(FACPoints)].DisCoord      := CurDisCoord;
          FACPoints[High(FACPoints)].State         := AllChState;
          FACPoints[High(FACPoints)].BackMotionFlag:= BackMotionFlag;
        end;

        LastBackMotionFlag:= BackMotionFlag; // C�������� ������� ��������� ���������� � ���� �������� �����
        LastSysCoord:= CurSysCoord; // C�������� ������� ����������

        if UpdateACData then // C�������� ������� ��������� �� � ���������� ���� ������� ����� ������ ��
        begin
          OldAllChState:= AllChState;
          UpdateACData:= False;
        end;



        // AC algorithm goodness test
      {  if Debug then // �������
        begin
          for R:= rLeft to rRight do
          begin
            for Ch:= 0 to 16 do
            begin
              if not CurAKState[R, Ch] and CurEcho[R, Ch] then Inc(BAD_AC_algorithm_situation);
              if not CurAKState[R, Ch] and not CurEcho[R, Ch] then Inc(GOOD_AC_algorithm_situation);
            end;
          end;

          for R:= rLeft to rRight do
            for Ch:= 0 to 16 do
              CurEcho[R, Ch]:= False;
        end;                        }
      end;

      if SkipBytes <> -1 then FBody.Position:= FBody.Position + SkipBytes;  // ���� � ����� �� "����������" ��� ������ �� �� ������ ���������

    until FBody.Position + 1 > FDataSize;

  except
  end;


LoadStop :
  // ���� ����������

  // �� ������������� ������� ����� ��������� �� ����������� ���� ��������������������� ��������
  for I := 0 to High(FACPoints) - 1 do
  // ��������������� ���� ����� � �������� I, I + 1
  begin
    // ������ ������ -
    if  ((not FACPoints[I].BackMotionFlag) and (FACPoints[I + 1].BackMotionFlag) and (not FACPoints[I].State)) or // ��������� � ������ ���� �������� �����
        ((not FACPoints[I].BackMotionFlag) and (not FACPoints[I + 1].BackMotionFlag) and (not FACPoints[I].State) and (FACPoints[I + 1].State)) then   // ��� ���� �������� �����
    begin
{
      UCSData[UCSDataIdx][0].X:= FACPoints[I].SysCoord;
      UCSData[UCSDataIdx][0].Y:= FACPoints[I + 1].SysCoord;
      UCSData[UCSDataIdx][1].X:= FACPoints[I].DisCoord;
      UCSData[UCSDataIdx][1].Y:= FACPoints[I + 1].DisCoord;
}
      UCSData[UCSDataIdx].StSysCoord:= FACPoints[I].SysCoord;
      UCSData[UCSDataIdx].EdSysCoord:= FACPoints[I + 1].SysCoord;
      UCSData[UCSDataIdx].StDisCoord:= FACPoints[I].DisCoord;
      UCSData[UCSDataIdx].EdDisCoord:= FACPoints[I + 1].DisCoord;

      Inc(UCSDataIdx);
      if UCSDataIdx = Length(UCSData) then SetLength(UCSData, Length(UCSData) + 1000);
    end;

{
.SysCoord
.DisCoord
.State
.BackMotionFlag
}
    // ��������� ������ - �� � ���
    if (((FACPoints[I].BackMotionFlag) and (FACPoints[I + 1].BackMotionFlag)) or // � ���� �������� �����
        ((FACPoints[I].BackMotionFlag) and (not FACPoints[I + 1].BackMotionFlag)))   // �� ����� ���� �������� �����
        and
        // ������ ���������
        ((FACPoints[I].State) and (not FACPoints[I + 1].State) or     // ���� ����� �������
         (FACPoints[I].State) and (FACPoints[I + 1].State)) then  // ���� ����� ����� ����
    begin
               // ���� ������� �� - StSysCrd .. EdSysCrd
      StSysCrd:= Min(FACPoints[I].SysCoord, FACPoints[I + 1].SysCoord);
      EdSysCrd:= Max(FACPoints[I].SysCoord, FACPoints[I + 1].SysCoord);
      StSysCrdIdx:= -1;
      EdSysCrdIdx:= -1;

               // ����� ��� ����� � �������� ������������ ���� ������� ��
      for J := UCSDataIdx - 1 downto 0 do
      if UCSData[J].StSysCoord <> - 1 then
      begin
    //    if (StSysCrd > UCSData[J].Y) then StSysCrdIdx:= J else
        if StSysCrdIdx = - 1 then
          if (StSysCrd >= UCSData[J].StSysCoord) then StSysCrdIdx:= J;

    //    if (EdSysCrdIdx > UCSData[J].Y) then EdSysCrdIdx:= J else
        if EdSysCrdIdx = - 1 then
          if (EdSysCrd >= UCSData[J].StSysCoord) then EdSysCrdIdx:= J;

        if (StSysCrdIdx <> - 1) and (EdSysCrdIdx <> - 1) then Break;
      end;
      if StSysCrdIdx = - 1 then StSysCrdIdx:= 0;
      if EdSysCrdIdx = - 1 then EdSysCrdIdx:= 0;
                                             // ���� ������� �� ��������� ����������� ���� �����
      for J := StSysCrdIdx to EdSysCrdIdx do
        if (StSysCrd <= UCSData[J].StSysCoord) and (EdSysCrd >= UCSData[J].EdSysCoord) then
        begin
          UCSData[J].StSysCoord:= - 1;
          UCSData[J].EdSysCoord:= - 1;
          UCSData[J].StDisCoord:= - 1;
          UCSData[J].EdDisCoord:= - 1;
        end
        else
                // ���� ������� �� �������� ����������� ���� ����� - ������ ���� �����
        if (StSysCrd <= UCSData[J].StSysCoord) and (EdSysCrd > UCSData[J].StSysCoord) and (EdSysCrd < UCSData[J].EdSysCoord) then
        begin
                // ����� ����������� ���� �����
          UCSData[J].StSysCoord:= EdSysCrd;
          UCSData[J].StDisCoord:= SysToDisCoord(EdSysCrd);
        end
        else
                // ���� ������� �� �������� ����������� ���� ����� - ����� ���� �����
        if (StSysCrd > UCSData[J].StSysCoord) and (StSysCrd < UCSData[J].EdSysCoord) and (EdSysCrd > UCSData[J].EdSysCoord) then
        begin
                // ����� ����������� ���� �����
          UCSData[J].EdSysCoord:= StSysCrd;
          UCSData[J].EdDisCoord:= SysToDisCoord(StSysCrd);
        end
        else
                 // ���� ������� �� ��������� ������ ���� �����
        if (StSysCrd > UCSData[J].StSysCoord) and (EdSysCrd < UCSData[J].EdSysCoord) then
        begin
                // ��������� ���� �� ��� �����
          SetLength(UCSData, Length(UCSData) + 1);
          for K := UCSDataIdx - 1 downto J do UCSData[K + 1]:= UCSData[K];

          UCSData[J].EdSysCoord:= StSysCrd;
          UCSData[J].EdDisCoord:= SysToDisCoord(StSysCrd);
          UCSData[J + 1].StSysCoord:= EdSysCrd;
          UCSData[J + 1].EdDisCoord:= SysToDisCoord(EdSysCrd);
          Inc(UCSDataIdx);
          if UCSDataIdx = Length(UCSData) then SetLength(UCSData, Length(UCSData) + 1000);

        end;
    end;

  end;

  SetLength(UCSData, UCSDataIdx);

  // �������� ������� ��������� ��� � ��������� ���
  for I := High(UCSData) downto 0 do

    if ((UCSData[I].StSysCoord = - 1) and (UCSData[I].EdSysCoord = - 1)) or
       ((UCSData[I].EdDisCoord - UCSData[I].StDisCoord) * Header.ScanStep div 100 < 60) then
    begin
      for J := I to High(UCSData) - 1 do UCSData[J]:= UCSData[J + 1];
      SetLength(UCSData, Length(UCSData) - 1);
    end;

  // ���������� ������������� ����� ���
  for I := High(UCSData) - 1 downto 0 do
    if (UCSData[I + 1].StDisCoord - UCSData[I].EdDisCoord) * Header.ScanStep div 100 < 60 then
    begin
      UCSData[I].EdDisCoord:= UCSData[I + 1].EdDisCoord;
      if I <> High(UCSData) - 1 then
        for J := I + 1 to High(UCSData) - 1 do UCSData[J]:= UCSData[J + 1];
      SetLength(UCSData, Length(UCSData) - 1);
    end;


  Summ:= 0;
  for I := 0 to High(UCSData) do
    Summ:= Summ + UCSData[I].EdDisCoord - UCSData[I].StDisCoord;

  if MaxDisCoord <> 0 then
  begin
    UCSLength:= Summ * Header.ScanStep / 100 / 1000;
    UCSProcent:= 100 * Summ / MaxDisCoord;
  end;

  {
  if Debug then // �������
    for R:= rLeft to rRight do
      for Ch:= 0 to 16 do
      begin
        if AKCount[Rail, Ch] <> 0 then
          AKMediumLen[R, Ch]:= Round(AKMediumLen[R, Ch] / AKCount[Rail, Ch]);
        Log[R, Ch].SaveToFile(Format('C:\Temp\ACLog_%d_%d.txt', [Ord(R), Ch]));
        Log[R, Ch].Free;
      end; }
end;

procedure TAviconDataSource.AnalyzeAC_forNoAC_Data(MinLen, MaxIncludeLen, AmplTh: Integer; Debug_: Boolean);
label LoadCont, LoadStop;

var
  CurSysCoord: Integer;
  LastSysCoord: Integer;
  CurDisCoord: Int64;
  SkipBytes: Integer;
  I, J, K, Summ: Integer;
  ErrorFlag: Boolean;
  LoadID: Byte;
  LoadByte: array [1 .. 331] of Byte;
  LoadLongInt: array [1 .. 5] of Longint; // absolute LoadByte[1];
//  Last: Integer;
  R: TRail;
  Ch: Integer;
  ChIdx: Integer;
  CurAKState: array [TRail] of Boolean; // Flag AC Exists
  CurAKCoord: array [TRail, 0..16] of Integer; // Lost AC coord or -1 AC exist
  CurEcho: array [TRail, 0..16] of Boolean; // Flag echo signals exists
  Log: array [TRail, 0..16] of TStringList; // Log

  AllChState: Boolean;
  OldAllChState: Boolean;
  AllChCrd: Integer;
  Rail: TRail;

//  UpdateACData: Boolean;
  UCSDataIdx: Integer;

  Dat: array [0 .. 7] of Byte;

  BackMotionFlag: Boolean;
  LastBackMotionFlag: Boolean;
  StartBackMotionSysCoord: Integer;

  Dir: Boolean;

  StSysCrd: Integer;
  EdSysCrd: Integer;
  StSysCrdIdx: Integer;
  EdSysCrdIdx: Integer;

  ExHeader__: TExHeader;
  DataType: Integer;
  Time: edTime;
  SaveOffset: Integer;

  BodyBadZones_idx: Integer;

  FCurEcho: TCurEcho;

  ChParam: TMiasParams;


function GetSign(Dir: Integer): Boolean;
begin
  Result:= Dir >= 0;
end;

begin
  if FAnalyzeACExecuted and
     (FAnalyzeACExecuted_MinLen <> MinLen) and
     (FAnalyzeACExecuted_MaxIncludeLen <> MaxIncludeLen) and
     (not Debug) then Exit;

  FAnalyzeACExecuted_MinLen:= MinLen;
  FAnalyzeACExecuted_MaxIncludeLen:= MaxIncludeLen;
  FAnalyzeACExecuted:= True;
  Debug:= Debug_;

  SetLength(UCSData, 1000);  // �������������� ����� � ������� �����
  UCSDataIdx:= 0;            // ������ ��� ���������� ������ � ������ �����
  ACEventCount:= 0;          // DEBUG

  CurSysCoord:= 0;           // ������� ��������� ����������
  LastSysCoord:= MaxInt;     // ���������� ��������� ���������� - ����������� � ���� ���� �� ���� ������
  CurDisCoord:= 0;
  AllChState:= True;         // ��������� �� ��� ������ ����� - ����� ��������� ��� ������� �� AND
  OldAllChState:= True;      // ���������� ��������� ��
//  UpdateACData:= False;      // ���� ���� ����� ������ ��

  AllChCrd:= - 1;
  BackMotionFlag:= False;    // ���� - ��������� � ���� �������� ����� - �� ���� � ���� ��� ������� ������� �����
  LastBackMotionFlag:= False; // ���������� ��������� ����� BackMotionFlag

                              // "���������" �������� ������ ...
  // ... ��� ���� ����� ...
  for R:= Config.MinRail to Config.MaxRail do
  begin
    for Ch:= 0 to 16 do       // ... � �������
    begin
   //   CurAKState[R, Ch]:= True; // ������� ��� ���������� ������� ���� ...
      CurAKCoord[R, Ch]:= - 2;  // ... � ������ ��� ����������
   //   CurEcho[R, Ch]:= False;

      if Debug then // �������
      begin
        AKMinLen[R, Ch]:= maxint;
        AKMaxLen[R, Ch]:= 0;
        AKMediumLen[R, Ch]:= 0;
        AKCount[R, Ch]:= 0;
        AKSummLen[R, Ch]:= 0;
        Log[R, Ch]:= TStringList.Create;
      end;
    end;
    if Debug then // �������
      for Ch:= 0 to 99 do AKExists[R, Ch]:= False;
  end;
                                                      // ������ �����
  if not FFullHeader then FBody.Position:= 17
                     else FBody.Position:= SizeOf(TFileHeader); // ������� ���������

LoadCont :

  // ��������������� ���� ���� - ����������� ������ ����� ��������� ��������� �� (���������� ��� ���� ����� � �������),
  //  ����� ����������� ���������� �� �� ������ ����� ����������� �������� ������� � ��������� ���� �������� �����

  BodyBadZones_idx:= -1;
  for I := 0 to High(FBodyBadZones) do
  begin
    if FBody.Position < FBodyBadZones[I].From then
    begin
      BodyBadZones_idx:= I;
      Break;
    end
    else
    if (FBody.Position >= FBodyBadZones[I].From) and
       (FBody.Position <= FBodyBadZones[I].To_) then
    begin
      FBody.Position:= FBodyBadZones[I].To_;
      BodyBadZones_idx:= I + 1;
      Break;
    end;
  end;
  if Length(FBodyBadZones) <> 0 then
    if BodyBadZones_idx > High(FBodyBadZones) then
      BodyBadZones_idx:= -1;

  FillChar(FCurEcho, SizeOf(TCurEcho), 0);
  GetParamFirst(0, ChParam);

  if FBody.Position < FDataSize then
  try
    repeat
      SkipBytes:= -1;

      if BodyBadZones_idx <> - 1 then
      begin
        if (FBody.Position >= FBodyBadZones[BodyBadZones_idx].From) and
           (FBody.Position < FBodyBadZones[BodyBadZones_idx].To_) then
        begin
          FBody.Position:= FBodyBadZones[BodyBadZones_idx].To_;
          BodyBadZones_idx:= I + 1;
          if BodyBadZones_idx > High(FBodyBadZones) then BodyBadZones_idx:= -1;
        end;
      end;

      SaveOffset:= FBody.Position;
      FBody.ReadBuffer(LoadID, 1); // �������� �������������� �������

      if LoadID and 128 = 0 then
      begin
        FSrcDat[0]:= LoadID;
        Ch:= (LoadID shr 2) and 15;

        if FExtendedChannels and (Ch > 14) then
        begin
          FBody.ReadBuffer(Ch, 1);
          Ch:= Ch + 15;
        end;
        FBody.ReadBuffer(FSrcDat[1], EchoLen[LoadID and 3]);

        with FCurEcho[TRail(LoadID shr 6 and 1), Ch] do
        begin

          case LoadID and $03 of
            0: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 Ampl[Count]:= FSrcDat[2] shr 4 and $0F;
               end;
            1: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 Ampl[Count]:= FSrcDat[3] shr 4 and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[2];
                 Ampl[Count]:= FSrcDat[3] and $0F;
               end;
            2: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 Ampl[Count]:= FSrcDat[4] shr 4 and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[2];
                 Ampl[Count]:= FSrcDat[4] and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[3];
                 Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
               end;
            3: begin
                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[1];
                 Ampl[Count]:= FSrcDat[5] shr 4 and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[2];
                 Ampl[Count]:= FSrcDat[5] and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[3];
                 Ampl[Count]:= FSrcDat[6] shr 4 and $0F;

                 if Count < 16 then Inc(Count);
                 Delay[Count]:= FSrcDat[4];
                 Ampl[Count]:= FSrcDat[6] and $0F;
               end;
          end;
        end;
      end
      else
        case LoadID of

          EID_SysCrd_NS:  begin // �������� ����������
                            FBody.ReadBuffer(LoadByte[1], 1);
                            CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[1]);
                          end;
          EID_SysCrd_NF:  begin // - ������ ����������
                            FBody.ReadBuffer(LoadLongInt[2], 4);
                          {  if LoadLongInt[2] > FExHeader.EndSysCoord then FBody.Position:= SaveOffset + 1
                                                                      else} CurSysCoord:= LoadLongInt[2];
                          end;
          EID_EndFile:    begin // ����� �����
                            ErrorFlag:= False;
                            FBody.ReadBuffer(LoadByte, 13);
                            for i := 1 to 13 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                            if not ErrorFlag then
                              if FBody.Position <> FBody.Size then
                              begin
                                if FBody.Size - FBody.Position >= SizeOf(TExHeader) then
                                begin
                                  FBody.ReadBuffer(LoadByte, 170 + 32 + 128);
                                  for i := 1 to 14 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                                  if not ErrorFlag then
                                    for i := 107 to 170 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                                  if not ErrorFlag then
                                    for i := 205 + 2 to 330 do if LoadByte[i] <> $FF then begin ErrorFlag:= True; Break; end;
                                  ErrorFlag:= ErrorFlag or not (LoadByte[19] in [0..8]) or (LoadByte[20] + LoadByte[21] + LoadByte[22] <> 0);
                                end;
                              end;
                          end;
          EID_SysCrd_SS:  begin // - �������� ������, �������� ����������
                            FBody.ReadBuffer(LoadByte[1], 2);
                            CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[2]);
                          end;
          EID_SysCrd_SF:  begin // - �������� ������, ������ ����������
                            FBody.ReadBuffer(LoadByte[1], 1);
                            FBody.ReadBuffer(LoadLongInt, 4);
                            CurSysCoord:= LoadLongInt[1];
                          end;
          EID_SysCrd_FS:  begin // - ������ ������, �������� ����������
                            FBody.ReadBuffer(LoadLongInt[1], 4);
                            FBody.ReadBuffer(LoadByte[1], 1);
                            CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[1]);
                          end;
          EID_SysCrd_FF:  begin // - ������ ������, ������ ����������
                            FBody.ReadBuffer(LoadLongInt[1], 4);
                            FBody.ReadBuffer(LoadLongInt[2], 4);
                            CurSysCoord:= LoadLongInt[2];
                          end;
          else SkipEvent(LoadID);
        end;

      {$IFDEF FIX_COORD_JUMP}
      if FSkipEventFlag_HandScan then FSkipCoordEventCount:= 15;
      {$ENDIF}


      if LoadID in [EID_SysCrd_NS, EID_SysCrd_NF, EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then
      begin
        if (LastSysCoord <> MaxInt) then // ��� �� ������ ��������� ����������
        begin
          {$IFDEF FIX_COORD_JUMP}
          if (FSkipCoordEventCount <> 0) then // ���������� ������� �� ����� �������
          begin
            if Abs(CurSysCoord - LastSysCoord) > 20 then CurSysCoord:= LastSysCoord;
            Dec(FSkipCoordEventCount);
          end;
          {$ENDIF}
          Inc(CurDisCoord, Abs(CurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
        end
        else
        begin
          CurDisCoord:= 0; // ��� ������ ��������� ����������
        end;

        GetParamNext(CurDisCoord, ChParam);

        AllChState:= True;
        for R:= Config.MinRail to Config.MaxRail do
        begin
          CurAKState[R]:= False;
          for I:= 1 to FCurEcho[R, 1].Count do
            if (FCurEcho[R, 1].Delay[I] / 3 >= ChParam.Par[R][0].StStr) and
               (FCurEcho[R, 1].Delay[I] / 3 <= ChParam.Par[R][0].EndStr) and
               (FCurEcho[R, 1].Ampl[I] >= AmplTh) then
            begin
              CurAKState[R]:= True;
              Break;
            end;
          AllChState:= AllChState and CurAKState[R]; // ����������� ������ ��������� �� ��� ������ ����� - ����� ��������� ��� ������� �� AND
        end;

        FillChar(FCurEcho, SizeOf(TCurEcho), 0);

        if (LastSysCoord <> MaxInt) then // ��� �� ������ ��������� ����������
        begin
          if not BackMotionFlag and (CurSysCoord < LastSysCoord) then // ������ �� � ���� �������� ����� � ����� ���������� ������ ����������
          begin
            Dir:= GetSign(CurSysCoord - LastSysCoord); // ���������� ����� ����������� ��������
            BackMotionFlag:= True;                     // ������������ ���� ���� �������� �����
            StartBackMotionSysCoord:= LastSysCoord;    // ��������� ���������� ������ ���� �������� �����

            SetLength(FACPoints, Length(FACPoints) + 1); // ��������� ����� ������ ��
            FACPoints[High(FACPoints)].SysCoord:= CurSysCoord; // ��������� �����������
            FACPoints[High(FACPoints)].DisCoord:= CurDisCoord; // ���������� �����������
            FACPoints[High(FACPoints)].State:= AllChState; // ��������� ��
            FACPoints[High(FACPoints)].BackMotionFlag:= BackMotionFlag; // ���� ���� �������� �����

          end
          else
          if BackMotionFlag then // ������ ��������� � ���� �������� �����
          begin
            if GetSign(CurSysCoord - LastSysCoord) <> Dir then // ���������� ����������� �������� �������
            begin
              // ����� �����������
              Dir:= GetSign(CurSysCoord - LastSysCoord);

              SetLength(FACPoints, Length(FACPoints) + 1); // ��������� ����� ������ ��
              FACPoints[High(FACPoints)].SysCoord:=       CurSysCoord;
              FACPoints[High(FACPoints)].DisCoord:=       CurDisCoord;
              FACPoints[High(FACPoints)].State:=          AllChState;
              FACPoints[High(FACPoints)].BackMotionFlag:= BackMotionFlag;

            end;

            if (CurSysCoord > StartBackMotionSysCoord) then  // ����� �� ���������� ������ ���� �������� ����� - ���� �����������
            begin
              BackMotionFlag:= False; // ���� �������� ����� �����������

              SetLength(FACPoints, Length(FACPoints) + 1); // ��������� ����� ������ ��
              FACPoints[High(FACPoints)].SysCoord      := CurSysCoord;
              FACPoints[High(FACPoints)].DisCoord      := CurDisCoord;
              FACPoints[High(FACPoints)].State         := AllChState;
              FACPoints[High(FACPoints)].BackMotionFlag:= BackMotionFlag;
            end;
          end;
        end;

        if OldAllChState <> AllChState then // ���������� ��������� ��  - ��������� ����� ������ ��
        begin
          SetLength(FACPoints, Length(FACPoints) + 1);
          FACPoints[High(FACPoints)].SysCoord      := CurSysCoord;
          FACPoints[High(FACPoints)].DisCoord      := CurDisCoord;
          FACPoints[High(FACPoints)].State         := AllChState;
          FACPoints[High(FACPoints)].BackMotionFlag:= BackMotionFlag;
          OldAllChState:= AllChState;
        end;

        LastBackMotionFlag:= BackMotionFlag; // C�������� ������� ��������� ���������� � ���� �������� �����
        LastSysCoord:= CurSysCoord; // C�������� ������� ����������
      end;

      if SkipBytes <> -1 then FBody.Position:= FBody.Position + SkipBytes;  // ���� � ����� �� "����������" ��� ������ �� �� ������ ���������

    until FBody.Position + 1 > FDataSize;

  except
  end;


LoadStop :
  // ���� ����������

  // �� ������������� ������� ����� ��������� �� ����������� ���� ��������������������� ��������
  for I := 0 to High(FACPoints) - 1 do
  // ��������������� ���� ����� � �������� I, I + 1
  begin
    // ������ ������ -
    if  ((not FACPoints[I].BackMotionFlag) and (FACPoints[I + 1].BackMotionFlag) and (not FACPoints[I].State)) or // ��������� � ������ ���� �������� �����
        ((not FACPoints[I].BackMotionFlag) and (not FACPoints[I + 1].BackMotionFlag) and (not FACPoints[I].State) and (FACPoints[I + 1].State)) then   // ��� ���� �������� �����
    begin
      UCSData[UCSDataIdx].StSysCoord:= FACPoints[I].SysCoord;
      UCSData[UCSDataIdx].EdSysCoord:= FACPoints[I + 1].SysCoord;
      UCSData[UCSDataIdx].StDisCoord:= FACPoints[I].DisCoord;
      UCSData[UCSDataIdx].EdDisCoord:= FACPoints[I + 1].DisCoord;

      Inc(UCSDataIdx);
      if UCSDataIdx = Length(UCSData) then SetLength(UCSData, Length(UCSData) + 1000);
    end;

    // ��������� ������ - �� � ���
    if (((FACPoints[I].BackMotionFlag) and (FACPoints[I + 1].BackMotionFlag)) or // � ���� �������� �����
        ((FACPoints[I].BackMotionFlag) and (not FACPoints[I + 1].BackMotionFlag)))   // �� ����� ���� �������� �����
        and
        // ������ ���������
        ((FACPoints[I].State) and (not FACPoints[I + 1].State) or     // ���� ����� �������
         (FACPoints[I].State) and (FACPoints[I + 1].State)) then  // ���� ����� ����� ����
    begin
               // ���� ������� �� - StSysCrd .. EdSysCrd
      StSysCrd:= Min(FACPoints[I].SysCoord, FACPoints[I + 1].SysCoord);
      EdSysCrd:= Max(FACPoints[I].SysCoord, FACPoints[I + 1].SysCoord);
      StSysCrdIdx:= -1;
      EdSysCrdIdx:= -1;

               // ����� ��� ����� � �������� ������������ ���� ������� ��
      for J := UCSDataIdx - 1 downto 0 do
      if UCSData[J].StSysCoord <> - 1 then
      begin
        if StSysCrdIdx = - 1 then
          if (StSysCrd >= UCSData[J].StSysCoord) then StSysCrdIdx:= J;

        if EdSysCrdIdx = - 1 then
          if (EdSysCrd >= UCSData[J].StSysCoord) then EdSysCrdIdx:= J;

        if (StSysCrdIdx <> - 1) and (EdSysCrdIdx <> - 1) then Break;
      end;
      if StSysCrdIdx = - 1 then StSysCrdIdx:= 0;
      if EdSysCrdIdx = - 1 then EdSysCrdIdx:= 0;

      // ���� ������� �� ��������� ����������� ���� �����
      for J := StSysCrdIdx to EdSysCrdIdx do
        if (StSysCrd <= UCSData[J].StSysCoord) and (EdSysCrd >= UCSData[J].EdSysCoord) then
        begin
          UCSData[J].StSysCoord:= - 1;
          UCSData[J].EdSysCoord:= - 1;
          UCSData[J].StDisCoord:= - 1;
          UCSData[J].EdDisCoord:= - 1;
        end
        else
                // ���� ������� �� �������� ����������� ���� ����� - ������ ���� �����
        if (StSysCrd <= UCSData[J].StSysCoord) and (EdSysCrd > UCSData[J].StSysCoord) and (EdSysCrd < UCSData[J].EdSysCoord) then
        begin
                // ����� ����������� ���� �����
          UCSData[J].StSysCoord:= EdSysCrd;
          UCSData[J].StDisCoord:= SysToDisCoord(EdSysCrd);
        end
        else
                // ���� ������� �� �������� ����������� ���� ����� - ����� ���� �����
        if (StSysCrd > UCSData[J].StSysCoord) and (StSysCrd < UCSData[J].EdSysCoord) and (EdSysCrd > UCSData[J].EdSysCoord) then
        begin
                // ����� ����������� ���� �����
          UCSData[J].EdSysCoord:= StSysCrd;
          UCSData[J].EdDisCoord:= SysToDisCoord(StSysCrd);
        end
        else
                 // ���� ������� �� ��������� ������ ���� �����
        if (StSysCrd > UCSData[J].StSysCoord) and (EdSysCrd < UCSData[J].EdSysCoord) then
        begin
                // ��������� ���� �� ��� �����
          SetLength(UCSData, Length(UCSData) + 1);
          for K := UCSDataIdx - 1 downto J do UCSData[K + 1]:= UCSData[K];

          UCSData[J].EdSysCoord:= StSysCrd;
          UCSData[J].EdDisCoord:= SysToDisCoord(StSysCrd);
          UCSData[J + 1].StSysCoord:= EdSysCrd;
          UCSData[J + 1].EdDisCoord:= SysToDisCoord(EdSysCrd);
          Inc(UCSDataIdx);
          if UCSDataIdx = Length(UCSData) then SetLength(UCSData, Length(UCSData) + 1000);

        end;
    end;
  end;

  SetLength(UCSData, UCSDataIdx);

  // �������� ��������� ���
  for I := High(UCSData) downto 0 do

    if (UCSData[I].StSysCoord = - 1) and (UCSData[I].EdSysCoord = - 1) then
    begin
      for J := I to High(UCSData) - 1 do UCSData[J]:= UCSData[J + 1];
      SetLength(UCSData, Length(UCSData) - 1);
    end;

  // ���������� ��� ����������� �������� �� ���������� �����
  for I := High(UCSData) - 1 downto 0 do
    if (UCSData[I + 1].StDisCoord - UCSData[I].EdDisCoord) * Header.ScanStep div 100 < MaxIncludeLen then
    begin
      UCSData[I].EdDisCoord:= UCSData[I + 1].EdDisCoord;
      if I <> High(UCSData) - 1 then
        for J := I + 1 to High(UCSData) - 1 do UCSData[J]:= UCSData[J + 1];
      SetLength(UCSData, Length(UCSData) - 1);
    end;

  // �������� ������� ��������� ���
  for I := High(UCSData) downto 0 do
    if ((UCSData[I].EdDisCoord - UCSData[I].StDisCoord) * Header.ScanStep div 100 < MinLen) then
    begin
      for J := I to High(UCSData) - 1 do UCSData[J]:= UCSData[J + 1];
      SetLength(UCSData, Length(UCSData) - 1);
    end;

  Summ:= 0;
  for I := 0 to High(UCSData) do
    Summ:= Summ + UCSData[I].EdDisCoord - UCSData[I].StDisCoord;

  if MaxDisCoord <> 0 then
  begin
    UCSLength:= Summ * Header.ScanStep / 100 / 1000;
    UCSProcent:= 100 * Summ / MaxDisCoord;
  end;
end;

procedure TAviconDataSource.FilterACData(minLen_mm: Integer);
label LoadCont, LoadStop;

var
  CurSysCoord: Integer;
  LastSysCoord: Integer;
  CurDisCoord: Int64;
  SkipBytes: Integer;
  I: Integer;
  LoadID: Byte;
  LoadByte: array [1 .. 22] of Byte;
  LoadLongInt: array [1 .. 5] of Longint; // absolute LoadByte[1];
//  Last: Integer;
  R: TRail;
  Ch: Integer;
  CurAKState: array [TRail, 0..16] of Boolean; // Flag AC Exists
  CurAKCoord: array [TRail, 0..16] of Integer; // Lost AC coord or -1 AC exist
  AKEventOffset: array [TRail, 0..16] of Integer;
//  CurEcho: array [TRail, 0..16] of Boolean; // Flag echo signals exists
//  Log: array [TRail, 0..16] of TStringList; // Log

  AllChState: Boolean;
  OldAllChState: Boolean;
  AllChCrd: Integer;
  Rail: TRail;

  UpdateACData: Boolean;
  UCSDataIdx: Integer;

  Dat: array [0 .. 7] of Byte;

begin
(* ��������� ��� ��� ��� �������
  UCSDataIdx:= 0;
  CurSysCoord:= 0;
  CurDisCoord:= 0;
  AllChState:= True;
  OldAllChState:= True;
  UpdateACData:= False;
  FillChar(AKGis[0], Length(AKGis) * SizeOf(Integer), 0);
  LastSysCoord:= MaxInt;
  AllChCrd:= - 1;
  for R:= rLeft to rRight do
  begin
    for Ch:= 0 to 16 do
    begin
      CurAKState[R, Ch]:= True;
      CurAKCoord[R, Ch]:= - 2; //  ������ ������ ��
      AKEventOffset[R, Ch]:= - 1;
    end;
  end;

  if not FFullHeader then FBody.Position:= 17
                     else FBody.Position:= SizeOf(TFileHeader); // ������� ���������

LoadCont :


  if FBody.Position < FDataSize then

  try
    repeat
      SkipBytes:= -1;
      FBody.ReadBuffer(LoadID, 1); // �������� �������������� �������

      if LoadID and 128 = 0 then
      begin
        FBody.ReadBuffer(Dat[1], EchoLen[LoadID and 3]);
        if FExtendedChannels and (Ch > 14) then FBody.Position:= FBody.Position + 1;
      end
      else
        case LoadID of

          EID_SysCrd_SS:  begin // - �������� ������, �������� ����������
                            FBody.ReadBuffer(LoadByte[1], 2);
                            CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[2]);
                          end;
          EID_SysCrd_SF:  begin // - �������� ������, ������ ����������
                            FBody.ReadBuffer(LoadByte[1], 1);
                            FBody.ReadBuffer(LoadLongInt, 4);
                            CurSysCoord:= LoadLongInt[1];
                          end;
          EID_SysCrd_FS:  begin // - ������ ������, �������� ����������
                            FBody.ReadBuffer(LoadLongInt[1], 4);
                            FBody.ReadBuffer(LoadByte[1], 1);
                            CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[1]);
                          end;
          EID_SysCrd_FF:  begin // - ������ ������, ������ ����������
                            FBody.ReadBuffer(LoadLongInt[1], 4);
                            FBody.ReadBuffer(LoadLongInt[2], 4);
                            CurSysCoord:= LoadLongInt[2];
                          end;
          EID_SysCrd_NS:  begin // �������� ����������
                            FBody.ReadBuffer(LoadByte[1], 1);
                            CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[1]);
                          end;
          EID_SysCrd_NF:  begin // - ������ ����������
                            FBody.ReadBuffer(LoadLongInt[2], 4);
                            CurSysCoord:= LoadLongInt[2];
                          end;
          EID_EndFile:    begin // ����� �����
                            FBody.ReadBuffer(LoadByte, 9 + 4);
                            Break;
                          end;
          EID_ACData:     begin
                            FBody.ReadBuffer(LoadByte[1], 1);
                            if LoadByte[1] and $01 = 0 then
                            begin

                              Rail:= TRail(LoadByte[1] shr 6 and 1);
                              Ch:= (LoadByte[1] shr 2) and 15;
                              CurAKState[Rail, Ch]:= Boolean(LoadByte[1] shr 7 and 1);

                              if (isA31_Scheme1 and (Ch <= 11)) or isA31_Scheme2 then
                              begin
  //                              UpdateACData:= True;

                                if CurAKState[Rail, Ch] then
                                begin
                                  if CurAKCoord[Rail, Ch] >= 0 then
                                  begin
                                    if (CurDisCoord - CurAKCoord[Rail, Ch]) * Header.ScanStep / 100 < minLen_mm then
                                    begin
                                      LoadByte[1]:= LoadByte[1] or 1;
                                      FBody.Position:= FBody.Position - 1;
                                      FBody.WriteBuffer(LoadByte[1], 1);

                                      if AKEventOffset[Rail, Ch] <> - 1 then
                                      begin
                                        I:= FBody.Position;
                                        FBody.Position:= AKEventOffset[Rail, Ch];
                                        FBody.ReadBuffer(LoadByte[1], 1);
                                        LoadByte[1]:= LoadByte[1] or 1;
                                        FBody.Position:= FBody.Position - 1;
                                        FBody.WriteBuffer(LoadByte[1], 1);
                                        FBody.Position:= I;
                                      end;
                                    end;
                                    CurAKCoord[Rail, Ch]:= - 1;
                                    AKEventOffset[Rail, Ch]:= -1;
                                  end
                                end
                                else
                                if CurAKCoord[Rail, Ch] < 0 then
                                begin
                                  CurAKCoord[Rail, Ch]:= CurDisCoord;
                                  AKEventOffset[Rail, Ch]:= FBody.Position - 1;
                                end;
                              end;
                            end;
                          end;

          else SkipEvent(LoadID);
        end;

      {$IFDEF FIX_COORD_JUMP}
      if FSkipEventFlag_HandScan then FSkipCoordEventCount:= 15;
      {$ENDIF}

      if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF, EID_SysCrd_NS, EID_SysCrd_NF] then // ��������� ����������
      begin
        if (LastSysCoord <> MaxInt) then
        begin
          {$IFDEF FIX_COORD_JUMP}
          if (FSkipCoordEventCount <> 0) then // ���������� ������� �� ����� �������
          begin
            CurSysCoord:= LastSysCoord;
            Dec(FSkipCoordEventCount);
          end;
( *          if (FSkipCoordEventCount <> 0) then // ���������� ������� �� ����� �������
          begin
            if (LastSysCoord <> MaxInt) and (Abs(CurSysCoord - LastSysCoord) > 200) then CurSysCoord:= LastSysCoord; // ���������� ������� �� ����� �������
//            CurSysCoord:= LastSysCoord;
            Dec(FSkipCoordEventCount);
          end; * )
          {$ENDIF}
//          if (CurSysCoord - LastSysCoord < - 200) then CurSysCoord:= LastSysCoord; // ���������� ������� �� ����� �������
//          Last:= CurDisCoord;
          Inc(CurDisCoord, Abs(CurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
        end
        else
        begin
//          Last:= 0;
          CurDisCoord:= 0;
        end;

        LastSysCoord:= CurSysCoord;
      end;

      if SkipBytes <> -1 then FBody.Position:= FBody.Position + SkipBytes;

    until FBody.Position + 1 > FDataSize;

  except
  end;

LoadStop :

//  FBody.SaveToFile(FFileName + '.������������� �� (������ 60��).a31');
*)
end;

procedure TAviconDataSource.GetGoodID(DeviceID: TAviconID);
var
  ForAll: TGoodIDList;

begin
  FillChar(ForAll, 128 * SizeOf(Boolean), 1);
  FillChar(ForAll[128], 127 * SizeOf(Boolean), 0);

  ForAll[EID_Sensor1]:= False; // ������ ������� �� � �������
  ForAll[EID_AirBrush]:= False; // ��������������

  ForAll[EID_HandScan]:= True; // ������
  ForAll[EID_Ku]:= True; // ��������� �������� ����������������
  ForAll[EID_Att]:= True; // ��������� �����������
  ForAll[EID_TVG]:= True; // ��������� ���
  ForAll[EID_StStr]:= True; // ��������� ��������� ������ ������
  ForAll[EID_EndStr]:= True; // ��������� ��������� ����� ������
  ForAll[EID_HeadPh]:= True; // ������ ���������� ���������
  ForAll[EID_Mode]:= True; // ��������� ������
  ForAll[EID_SetRailType]:= True; // ��������� �� ��� ������
  ForAll[EID_PrismDelay]:= True; // ��������� 2�� (word)
  ForAll[EID_Stolb]:= True; // ������� ����������
  ForAll[EID_Switch]:= True; // ����� ����������� ��������
  ForAll[EID_DefLabel]:= True; // ������� �������
  ForAll[EID_TextLabel]:= True; // ��������� �������
  ForAll[EID_StBoltStyk]:= True; // ������� ������ ��
  ForAll[EID_EndBoltStyk]:= True; // ���������� ������ ��
  ForAll[EID_Time]:= True; // ������� �������
  ForAll[EID_LongLabel]:= True; // ����������� �������

  ForAll[EID_GPSCoord]:= True; // �������������� ����������
  ForAll[EID_GPSState]:= True; // ��������� ��������� GPS
  ForAll[EID_GPSCoord2]:= True; // �������������� ���������� �� ���������
  ForAll[EID_NORDCO_Rec]:= True; // ������ NORDCO

  ForAll[EID_SpeedState]:= True; // �������� � ���������� �������� ��������

  ForAll[EID_StolbChainage]:= True; // ������� ���������� Chainage
  ForAll[EID_ZerroProbMode]:= True; // ����� ������ �������� 0 ����

  ForAll[EID_CheckSum]:= True; // ����������� �����

  ForAll[EID_StartSwitchShunter]:= True;  // ������ ���� ����������� ��������
  ForAll[EID_EndSwitchShunter]:= True;	 // ����� ���� ����������� ��������
  ForAll[EID_DebugData]:= True; // ���������� ����������

  ForAll[EID_EndFile]:= True; // ����� �����



////////////////////////////////////////

  if CompareMem(@DeviceID, @Avicon15N8Scheme1, 8) or // ������-15 (Nautiz X8)
     CompareMem(@DeviceID, @Avicon31Scheme1, 8) or  // ������-31 ����� 1
     CompareMem(@DeviceID, @Avicon31Scheme2, 8) or  // ������-31 ����� 2
     CompareMem(@DeviceID, @Avicon31Scheme3, 8) or  // ������-31 ����� 3 (22�)
     CompareMem(@DeviceID, @Avicon31Estonia, 8) or  // ������-31 �������
     CompareMem(@DeviceID, @Avicon31Estonia2, 8) or  // ������-31 ������� ����� 2
     CompareMem(@DeviceID, @MDLWheel, 8)        or  // ���
     CompareMem(@DeviceID, @MDLWheel2, 8)       or  // ���
     CompareMem(@DeviceID, @Avicon16Wheel3 , 8) or  // ������-16, ��� ����� 3: 6x65� , 2x42�, 2x55�,0� (��� 1��) ���� ��� � � Filus X17-DW, ������ 2 ���� (Linux)
     CompareMem(@DeviceID, @VMT_USW, 8)         or  // ������-26, ��� ����� 5, ������� �2: 6x65�, 2x55�x90�, 2x42�, 0� (��� 1��) (Linux)
     CompareMem(@DeviceID, @Avicon16Scheme4, 8) or  // ������-16, ��� ����� 4: 4�58��34�, 2�70�, 2�42�, 0� ���� ��� � � ������-31 (Linux)
     CompareMem(@DeviceID, @FilusX17DW, 8)      or  // Filus X17-DW
     CompareMem(@DeviceID, @FilusX27MWScheme1,8) or // ������-26, ��� ����� 5, ������� �2: 6x65�, 2x55�x90�, 2x42�, 0� (��� 1��) (Linux)
     CompareMem(@DeviceID, @FilusX27MW      , 8) then       // ������-26, ��� ����� 5, ������� �2: 6x65�, 2x55�x90�, 2x42�, 0� (��� 1��) (Linux)

  begin
    ForAll[EID_ChangeOperatorName]:= True; // ����� ��������� (��� ���������)
    ForAll[EID_AutomaticSearchRes]:= True; // ��������� ������� �������, ���������� ��� �������������� ������
    ForAll[EID_TestRecordFile]:= True; // ���� ������ ������������ ������
    ForAll[EID_OperatorRemindLabel]:= True; // ������� ����, ������� ���������� � ������ ��� ����������� ��������� (��������� ������ � ������)
    ForAll[EID_QualityCalibrationRec]:= True;	// �������� ��������� ������� ��������
    ForAll[EID_Temperature]:= True; // �����������
    ForAll[EID_ACData]:= True; // ������ ������������� ��������
    ForAll[EID_RailHeadScaner]:= True; // ������ ����������� ������� ������� ������
    ForAll[EID_SensAllowedRanges]:= True; // ������� ����������� ���������� ��
    ForAll[EID_SensFail]:= True;   // ���������� �������� ���������������� �� ������������ ��������
    ForAll[EID_AScanFrame]:= True; // ���� �-���������
    ForAll[EID_Media]:= True; // �����������
    ForAll[EID_SmallDate]:= True; // ����� ������������� �������
    ForAll[EID_MediumDate]:= True; // ������� ������������� �������
    ForAll[EID_BigDate]:= True; // ������� ������������� �������
    ForAll[EID_SysCrd_NS]:= True; // ��������� ���������� �����, ��������
    ForAll[EID_SysCrd_NF]:= True; // ��������� ���������� �����, ������
  end;
////////////////////////////////////////


  if CompareMem(@DeviceID, @Avicon14Scheme1, 8) or // ������-14 ����������
     CompareMem(@DeviceID, @Avicon14Wheel  , 8) or // ������-14 ��� ����� 1:
     CompareMem(@DeviceID, @Avicon14Wheel2 , 8) or // ������-14 ��� ����� 2: 6x65� , 2x42�, 2x0� (��� 2��)
     CompareMem(@DeviceID, @Avicon16Scheme1, 8) or // ������-16, ��� ����� 1: 4�58��34�, 2�70�, 2�42�, 0� (Windows)
     CompareMem(@DeviceID, @Avicon16Scheme2, 8) or // ������-16, ��� ����� 2: 2x58��34�, 2x58��34� ���, 2�70�, 2�42, 0� (Windows)
     CompareMem(@DeviceID, @Avicon16Scheme3, 8) or // ������-16, ��� ����� 3: 4�58��34�, 70�, 2x58��34� ���, 2�42�, 0� (Windows)
     CompareMem(@DeviceID, @Avicon16Scheme4, 8) or  // ������-16, ��� ����� 4: 4�58��34�, 2�70�, 2�42�, 0� ���� ��� � � ������-31 (Linux)
     CompareMem(@DeviceID, @Avicon16Wheel  , 8) or // ������-16, ��� ����� 1: 4x58��34�, 2x65�, 2x42�, 2x0� (��� 2��) (Windows)
     CompareMem(@DeviceID, @Avicon16Wheel2 , 8) or // ������-16, ��� ����� 2: 6x65� , 2x42�, 2x0� (��� 2��) (Windows)
     CompareMem(@DeviceID, @Avicon16Wheel3 , 8) or   // ������-16, ��� ����� 3: 6x65� , 2x42�, 2x55�,0� (��� 1��) ���� ��� � � Filus X17-DW, ������ 2 ���� (Linux)
     CompareMem(@DeviceID, @EGO_USWScheme1 , 8) or // ������-26, ��� ����� 1: 4x58��34�, 2x65�, 2x42�, 2x0� (��� 2��) - ������� (Windows)
     CompareMem(@DeviceID, @EGO_USWScheme2 , 8) or // ������-26, ��� ����� 2: 6x65�, 2x42�, 2x0� (��� 2��) - ������� (Windows)
     CompareMem(@DeviceID, @EGO_USWScheme3 , 8) or // ������-26, ��� ����� 3: - �� ������������
     CompareMem(@DeviceID, @VMT_US_BigWP   , 8) or // ������-26, ��� ����� 4: 6x65�, 2x45�x90�, 2x42�, 0� (��� 1��) - ���� (Windows)
     CompareMem(@DeviceID, @VMT_US_BigWP_N2, 8) or // ������-26, ��� ����� 5: 6x65�, 2x55�x90�, 2x42�, 0� (��� 1��) - ������ (Windows)
     CompareMem(@DeviceID, @VMT_USW        , 8) or // ������-26, ��� ����� 5, ������� �2: 6x65�, 2x55�x90�, 2x42�, 0� (��� 1��) (Linux)
     CompareMem(@DeviceID, @FilusX27MWScheme1,8) or // ������-26, ��� ����� 5, ������� �2: 6x65�, 2x55�x90�, 2x42�, 0� (��� 1��) (Linux)
     CompareMem(@DeviceID, @FilusX27MW     , 8) then       // ������-26, ��� ����� 5, ������� �2: 6x65�, 2x55�x90�, 2x42�, 0� (��� 1��) (Linux)
  begin
    ForAll[EID_Sensor1]:= True; // ������ ������� �� � �������
    ForAll[EID_AirBrush]:= True; // ��������������
    ForAll[EID_PaintMarkRes]:= True; // ��������� � ���������� ������� �� ������-�������
    ForAll[EID_AirBrushTempOff]:= True; // ��������� ���������� ���������������
    ForAll[EID_AirBrush2]:= True; // �������������� � ���������� �����������
    ForAll[EID_AlarmTempOff]:= True; // ��������� ���������� ��� �� ���� �������
    ForAll[EID_UMUPaintJob]:= True; // ������� ��� �� �������������
    ForAll[EID_SysCrd_UMUA_Left_NF]:= True; // ������ ��������� ���������� ��� ������ ��� A, ����� �������
    ForAll[EID_SysCrd_UMUB_Left_NF]:= True; // ������ ��������� ���������� ��� ������ ��� �, ����� �������
    ForAll[EID_SysCrd_UMUA_Right_NF]:= True; // ������ ��������� ���������� ��� ������ ��� A, ������ �������
    ForAll[EID_SysCrd_UMUB_Right_NF]:= True; // ������ ��������� ���������� ��� ������ ��� �, ������ �������
    ForAll[EID_PaintSystemParams]:= True; // ��������� ������ ��������� ������������
    ForAll[EID_SysCrd_SS]:= True; // ��������� ���������� ��������
    ForAll[EID_SysCrd_SF]:= True; // ������ ��������� ���������� � �������� �������
    ForAll[EID_SysCrd_FS]:= True; // ��������� ���������� ��������
    ForAll[EID_SysCrd_FF]:= True; // ������ ��������� ���������� � ������ �������
    ForAll[EID_SysCrd_NS]:= True; // ��������� ���������� �����, ��������
    ForAll[EID_SysCrd_NF]:= True; // ��������� ���������� �����, ������
  end;

{  if CompareMem(@DeviceID, @Avicon16Wheel3 , 8) or   // ������-16, ��� ����� 3: 6x65� , 2x42�, 2x55�,0� (��� 1��) ���� ��� � � Filus X17-DW, ������ 2 ���� (Linux)
     CompareMem(@DeviceID, @VMT_USW, 8)         or   // ������-26, ��� ����� 5, ������� �2: 6x65�, 2x55�x90�, 2x42�, 0� (��� 1��) (Linux)
     CompareMem(@DeviceID, @Avicon16Scheme4, 8) then // ������-16, ��� ����� 4: 4�58��34�, 2�70�, 2�42�, 0� ���� ��� � � ������-31 (Linux)
  begin
    ForAll[EID_Sensor1]:= True; // ������ ������� �� � �������
    ForAll[EID_AirBrush]:= True; // ��������������
    ForAll[EID_PaintMarkRes]:= True; // ��������� � ���������� ������� �� ������-�������
    ForAll[EID_AirBrushTempOff]:= True; // ��������� ���������� ���������������
    ForAll[EID_AirBrush2]:= True; // �������������� � ���������� �����������
    ForAll[EID_AlarmTempOff]:= True; // ��������� ���������� ��� �� ���� �������
    ForAll[EID_UMUPaintJob]:= True; // ������� ��� �� �������������
    ForAll[EID_PaintSystemParams]:= True; // ��������� ������ ��������� ������������
  end;
}
  if CompareMem(@DeviceID, @USK003R, 8) or // ������-12 ��� ������� - ������ ������ (�����)
     CompareMem(@DeviceID, @USK004R, 8) or //  // ������-12 ��� �������
     CompareMem(@DeviceID, @Avicon15Scheme1, 8) then //  // ������-15 ����� 1

    FillChar(ForAll[128], 127 * SizeOf(Boolean), 1);

  FIsGoodID:= ForAll;
end;

{
function TAviconDataSource.GetGPSCoord(var Coord: mGPSCoord; DisCoord: Integer): Boolean;
var
  ID: Byte;
  I, J: Integer;
  New: TGPSPoint;
  pData: PEventData;
  Flg1, Flg2: Boolean;

  LeftIdx, RightIdx: Integer;
  SameCoord: TIntegerDynArray;
  LeftEvent: TFileEventData;
  RightEvent: TFileEventData;

begin
  Result:= False;
  GetNearestEventIdx(DisCoord, LeftIdx, RightIdx, SameCoord);

  Flg1:= False;
  Flg2:= False;
  for I:= LeftIdx downto 0 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_GPSCoord then
    begin
      LeftEvent:= Event[I];
      Flg1:= True;
      Break;
    end;
  end;

  for I:= LeftIdx + 1 to GetEventCount - 1 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_GPSCoord then
    begin
      RightEvent:= Event[I];
      Flg2:= True;
      Break;
    end;
  end;

  if Flg1 or Flg2 then
  begin
    if (Abs(DisToSysCoord(DisCoord) - LeftEvent.Event.SysCoord) <
        Abs(DisToSysCoord(DisCoord) - RightEvent.Event.SysCoord)) then
    begin
      Coord.Lat:= InvertGPSSingle(pGPSCoord(@LeftEvent.Data)^.Lat);
      Coord.Lon:= InvertGPSSingle(pGPSCoord(@LeftEvent.Data)^.Lon);
      Result:= True;
    end
    else
    begin
      Coord.Lat:= InvertGPSSingle(pGPSCoord(@RightEvent.Data)^.Lat);
      Coord.Lon:= InvertGPSSingle(pGPSCoord(@RightEvent.Data)^.Lon);
      Result:= True;
    end;
  end;
end;
}

type

  TFileHeader_test = packed record // ��������� �����
    FileID: TAviconID; // RSPBRAFD
    DeviceID: TAviconID; // ������������� �������
    DeviceVer: Byte; // ������ �������
    HeaderVer: Byte; // ������ ���������
    MoveDir: Integer; // ����������� ��������: + 1 � ������� ���������� ��������� ����������; - 1 � ������� ���������� ��������� ����������
    ScanStep: Word; // ��� ������� ���� (�� * 100)
    PathCoordSystem: Byte; // ������� ������� ��������� ����������
    UsedItems: TUsedItemsList; // ����� ������������ ������������ ������
    CheckSumm: Cardinal; // ����������� ����� (unsigned 32-bit)
    UnitsCount: Byte; // ���������� ������ �������
    UnitsInfo: TUnitsInfoList; // ���������� � ������ �������
    Organization: THeaderStr; // �������� ����������� �������������� ��������
    RailRoadName: THeaderStr; // �������� �������� ������
    DirectionCode: DWord; // ��� �����������
    PathSectionName: THeaderStr; // �������� ������� ������
    OperatorName: THeaderStr; // ��� ���������
    RailPathNumber: Integer; // ����� �/� ����
    RailSection: Integer; // ����� ������
    Year: Word; // ���� ����� ��������
    Month: Word;
    Day: Word;
    Hour: Word;
    Minute: Word;
    StartKM: Integer; // ��������� ���������� - �����������
    StartPk: Integer;
    StartMetre: Integer;
    WorkRailTypeA: Byte;  // ������� ���� (��� ������������ ��������): 0 � �����, 1 � ������
    StartChainage: TCaCrd;  // ��������� ���������� � ����������� �� PathCoordSystem
    WorkRailTypeB: TCardinalPoints;  // ������� ���� (��� ������������ ��������): NR, SR, WR, ER
    TrackDirection: TCardinalPoints; // ��� �����������:                          NB, SB, EB, WB, CT, PT, RA, TT
    TrackID: TCardinalPoints;        // Track ID:                                 NR, SR, ER, WR
    CorrespondenceSides: TCorrespondenceSides;
	  HCThreshold: Double;
  end;


initialization

  ConfigList:= TDataFileConfigList.Create('');
//  Show-Message(IntToStr(SizeOf(TFileHeader_test)));
//  Show-Message(IntToStr(SizeOf(TUnitInfo)));

  {$IFDEF GENERATE_GLOBAL_LOG}
  FGLog:= TStringList.Create;
  {$ENDIF}


finalization

  {$IFDEF GENERATE_GLOBAL_LOG}
  FGLog.SaveToFile('c:\events.log');
  FGLog.Free;
  {$ENDIF}


end.

(*
    FFConst: array [0..13] of Byte;     // ��������� - 14 x 0xFFH
    DataSize: Integer;                  // ������ ���������
    DataVer: Integer;                   // ������ ������ = 7 19, 20, 21, 22
    EndMRFCrd: TMRFCrd;                 // ������ 12 ����    23 - 34
    EndCaCrd: TCaCrd;                   // ������ 8 ����     35 - 42
    CoordReserv: array [0..55] of Byte; // ������ 0          43
    EndSysCoord: Integer;               // �������� ��������� ����������
    EndDisCoord: Integer;               // �������� ���������� ����������
    Reserv1: array [0..63] of Byte;     // ������ 1

    EventDataVer: Integer;  // ������ ������� ������ ������� �������� ������� ����� (������: 5)
    EventItemSize: Integer; // ������ 1 �������� ������� �������� ������� �����
    EventOffset: Integer;   // ������ �� ������� �������� ������� �����
    EventCount: Integer;    // ���������� ��������� ������� �������� ������� �����

    NotebookDataVer: Integer;  // ������ ������� ������ ������� ���������� �������� � ������� �������� (������: 5)
    NotebookItemSize: Integer; // ������ 1 �������� ������� ���������� �������� � ������� ��������
    NotebookOffset: Integer;   // ������ �� ������� ���������� �������� � ������� ��������
    NotebookCount: Integer;    // ���������� ��������� ������� ���������� �������� � ������� ��������

    Reserv2: array [0..127] of Byte; // ������ 2:

  FillChar(FExHeader.FFConst[0], Length(FExHeader.FFConst), $FF);
  FillChar(FExHeader.Reserv1[0], Length(FExHeader.Reserv1), $FF);
  if not isA31 then FillChar(FExHeader.Reserv2[0], Length(FExHeader.Reserv2), $FF)
               else FillChar(FExHeader.Reserv2[2], Length(FExHeader.Reserv2) - 2, $FF);

1	14
15	4	size
19	4	ver
23	12	crd1
35	8	crd2
43	56	res
99	4	sys
103	4	dis
107	64	res2
171	4
175	4
179	4
183	4
187	4
191	4
195	4
199	4
203	128	res3
*)

