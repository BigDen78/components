{ $ DEFINE LOG}
{ $ DEFINE USEFILESTREAM}

// [�����] [�������]  [�����] [�������]
//    |_________|        |_________|

unit AviconDataContainer;

interface

uses
  Types, Classes, Controls, Windows, Messages, Dialogs, SysUtils, Math, AviconTypes, DataFileConfig, SyncObjs;

const
  MAX_EVENT_ARRAY = 3000;//1000//20000;

type

  // ------------------------< TMyStream >-----------------------------------------

  TMyStream = class
  private
    FPtr: Pointer;
    FSize: Longint;
    FCap: Longint;
    CS: TCriticalSection;

    FPosition: array [0..3] of Longint;
    FDataFile: file;
    FLastSave: Integer;

    FNoFile: Boolean;

  protected
    function GetPosition(Idx: Integer): Longint;
    procedure SetPosition(Idx: Integer; NewPos: Longint);
  public

    constructor Create(FileName: string);
    destructor Destroy; override;
    procedure WriteBuffer(Idx: Integer; const Buffer; Count: Longint);
    procedure ReadBuffer(Idx: Integer; var Buffer; Count: Longint);
    procedure SaveToFile(MinSaveSize: Integer = 0); // ������ ������� �����
    procedure SaveToStream(Dest: TStream); // ������ � �����
    property Position[Idx: Integer]: Longint read GetPosition write SetPosition;
    property Size: Longint read FSize;
//    property Memory: Pointer read FPtr;
  end;

  TMyFileStream = class
  private
//    FLog: TextFile;
    FPtr: Pointer;
    FSize: Longint;
    FCap: Longint;
    CS: TCriticalSection;

    FPosition: array [0..3] of Longint;
    FDataFile: file;
    FLastSave: Integer;

    FNoFile: Boolean;

    FStream: TFileStream;

  protected
    function GetPosition(Idx: Integer): Longint;
    procedure SetPosition(Idx: Integer; NewPos: Longint);
  public

    constructor Create(FileName: string);
    destructor Destroy; override;
    procedure WriteBuffer(Idx: Integer; const Buffer; Count: Longint);
    procedure ReadBuffer(Idx: Integer; var Buffer; Count: Longint);
    procedure SaveToFile(MinSaveSize: Integer = 0); // ������ ������� �����
    procedure SaveToStream(Dest: TStream); // ������ � �����
    property Position[Idx: Integer]: Longint read GetPosition write SetPosition;
    property Size: Longint read FSize;
//    property Memory: Pointer read FPtr;
  end;

  // ------------------------< TMyArray >-----------------------------------------
{
  TContainerParams = packed record
    Sensor1: array [TRail] of Boolean;
    EvalChPar: TEvalChannelsParams;
  end;
}
  TContainerEvent = packed record
    Event: TFileEvent;
    EvalChPar: TEvalChannelsParams;
    Sensor1: TSensor1Data;
    ZerroProbMode: TZerroProbMode;
//    Params: TContainerParams;
  end;

  TMyArray = class
  private
    FPtr: array of Pointer;
    FItemSize: Longint;
    FAddIdx: Longint;
    FCount: Integer;
    FRealCount: Integer;
    FCreateVirtItem: Boolean;
    FVirtItem: TContainerEvent;

  protected
{
    function GetEvent(Index: Integer): TFileEvent;
    function GetParam(Index: Integer): TEvalChannelsParams;
}
    function GetItem_(Index: Integer): TContainerEvent;

  public
    InstanceCount_: Integer;

    constructor Create(CreateVirtualItem: Boolean);
    destructor Destroy; override;
    function Add(const Buffer): Integer;
 {   property Item[Index: Integer]: TFileEvent read GetEvent; default;
    property Data[Index: Integer]: PEventData read GetDataPtr;
    property Param[Index: Integer]: TEvalChannelsParams read GetParam; }
    function GetDataPtr(Index: Integer): PEventData;
    property Item_[Index: Integer]: TContainerEvent read GetItem_; default;
    property Count: Longint read FCount;
    property RealCount: Longint read FRealCount;
    // property VirtualItem: TContEvent read FVirtItem write FVirtItem;
  end;

  TEventArray = class
  private
    FPtr: Pointer;
    FItemSize: Longint;
    FCount: Integer;
    FRealCount: Integer;
    FCreateVirtItem: Boolean;
    FVirtItem: TContainerEvent;
    FBegIdx: Integer;
    FEndIdx: Integer;
  protected
    function GetItem_(Index: Integer): TContainerEvent;
  public
    InstanceCount_: Integer;
    constructor Create(CreateVirtualItem: Boolean);
    destructor Destroy; override;
    function Add(const Buffer): Integer;
    function GetDataPtr(Index: Integer): PEventData;
    property Item_[Index: Integer]: TContainerEvent read GetItem_; default;
    property Count: Longint read FCount;
    property RealCount: Longint read FRealCount;
  end;
  // ------------------------------------------------------------------------------
  // ---------------------[ TAvconDataContainer ]----------------------------------
  // ------------------------------------------------------------------------------

const

  SaveStrIdx = 0;
  LoadStrIdx = 3;

type

  TDataNotifyEvent = function(StartDisCoord: Integer): Boolean of object;

  TKMKontentItem = record
    Pk: Integer;
    Len: Integer;
    Idx: Integer;
  end;

  TCoordListItem = record
    KM: Integer;
    Content: array of TKMKontentItem;
  end;

  TCoordList = array of TCoordListItem;

  TEchoListItem = record
    Count: Integer;
    Signals: TEchoBlock;
  end;

  TEchoList = array [TRail, 0..255] of TEchoListItem;

  TSpeedState = (ssNormalSpeed, // ���������� �������� ��������
                 ssOverSpeed);  // ���������� ���������� �������� ��������

  TCDType = record
    ID: Byte;
    DisCoord: Integer;
  end;

  TAviconDataContainer = class
  private
    FLog: TextFile;
    FCS: TCriticalSection;

    FFileName: string;
    FConfig: TDataFileConfig;
    FHead: TFileHeader;
    FExHeader: TExHeader;
                          // ������: 0,3 - ������; 1, 2 - ������
    {$IFDEF USEFILESTREAM}
    FData: TMyFileStream;
    {$ENDIF}
    {$IFNDEF USEFILESTREAM}
    FData: TMyStream;
    {$ENDIF}
//    FEvents: TMyArray;
    FEvents: TEventArray;
//    FCDList: array of Integer;  // ������ ����� ��������� ����������� ��������
    FCDList: array of TCDType;  // ������ ����� ��������� ����������� ��������

    FID: Byte;
//    FWID: Word;
    FDWID: DWord;
    FScanChNumExists: array [0..255] of Boolean;
    FEvalChNumExists: array [0..255] of Boolean;

    FLastOffset: Integer; // �������� �� ��������� ����������� �������
    FFirstCoord: Boolean;
    FLastSysCrdOffset: Integer; // �������� �� ��������� ����������� ������� ����������

    FCurSaveDisCrd: Integer;
    FCurSaveSysCrd: Integer;
    FLastSaveDisCrd: Integer;  // ���������� ���������� ����������
    FLastSaveSysCrd: Integer;  // ���������� ��������� ����������
    FLastAddSysCrd: Integer;   // ���������� ��������� ����������
    FFirstAddCoord: Boolean;   // ���� - ������ ���������� ����

//    FLastKm: Integer;
//    FLastPk: Integer;
//    FStolbCount: Integer;
//    FCoordList: TCoordList;
//    FLastStolb: edCoordPost;
    FLastPostSysCrd: Integer;
    FLastPostDisCrd: Integer;
    FLastMRFPost: TMRFCrd;
    FLastCaPost: TCaCrd;

//    FLastCoordPost: edCoordPost; // ��������� ����������� �����
//    FLastCoordPostSysCoord: Integer; // ��������� ���������� �� ������� ��� ����������� ��������� �����
//    FDistanceToLastCoordPost: Real48; // ���������� �� ���������� ������ (� �������� �����: ��/�����)
//    FCurHour: Integer; // ������� �����
//    FCurMinute: Integer; // ������� �����

                  // -------------------- ������ ����� --------------------

    FMaxReadOffset: Integer;   // FEfventsCount: Integer;
    FLastCheckSumOffset: Integer;
    FBackMotionFlag: Boolean;  // ������� �������� �����
    FSearchBMEnd: Boolean;     // ���� ������ ��������� ���� �������� �����
    FStartBMSysCoord: Integer; // ��������� ���������� ������ ���� �������� �����
    FCurSaveParam: TEvalChannelsParams; // ������� ��������� �������
    FCurSaveSensor1: TSensor1Data;
    FCurSaveZPMode: TZerroProbMode;

//    FCurrEchoList: TEchoList;  // ������� ������� - ���� ���������� ����������
//    FPrevEchoList: TEchoList;  // ������� ���������� ����������
//    FSaveEchoList: TEchoList;  // ���������������� ������� - ��� ����������
//    FEventFlag: Boolean;       // ���� ������� ������� �� ����������

    FMaxSysCoord: Integer;
    FMinSysCoord: Integer;
    FMaxDisCoord: Integer;

    FSaveSysCoord: Longint;
    FSaveDisCoord: Int64;

    FCurLoadEcho: array [0..3] of TCurEcho;      // ����������� �������
    FCurLoadDisCoord: array [0..3] of Int64;     // ���������� ���������� ����������� ������
    FCurLoadSysCoord: array [0..3] of Integer;   // ��������� ���������� ����������� ������
    FCurLoadOffset: array [0..3] of Integer;     // �������� ����������� ������
    FCurLoadBackMotion: array [0..3] of Boolean; // ���� �������� ����� � ����������� ������
    FCurLoadParams: array [0..3] of TEvalChannelsParams;
    FCurLoadSensor1: array [0..3] of TSensor1Data;
    FCurLoadZPMode: array [0..3] of TZerroProbMode; // ����� ������ ������� 0 ����


    FCurLoadSysCoord_UMUA_Left: Integer;
    FCurLoadSysCoord_UMUB_Left: Integer;
    FCurLoadSysCoord_UMUA_Right: Integer;
    FCurLoadSysCoord_UMUB_Right: Integer;

    FGetNearestEventIdx_StartIndex: Integer;


    FGetParamIdx: array [0..3] of Integer;
    FGetBMStateIdx: array [0..3] of Integer;    // ����� ��� �������� �����
    FGetBMStateState: array [0..3] of Boolean;  // ����� ��� �������� �����

    FHSSrcData: TAviconDataContainer; // ������ ������ ������� ��������
    FHSItem: array of THSItem; // ������ ��� �������� ������ ������� ��������

//    FOnlyBScanData: Boolean;
    FNewSysCoordSystem: Boolean;
    FMakeFile: Boolean;
    FFullHeader: Boolean;
    FExtendedChannels: Boolean;
    FPathCoordSystem: TCoordSys;

    FAirBrushTempOffState: Boolean;
    FAirBrushTempOffDisCrd: Integer;

    function DataTransfer(StartDisCoord: Integer): Boolean; // ������� �������� ������ �� DataContainer � ������

  protected
//    procedure SaveSysCoord(SysCoord: Integer);
//    procedure SaveSignals(EchoList: TEchoList); // ������ ����� ���-��������
//    function AnalyzeSignals: Boolean;
//    procedure ClearEchoList(EchoList: TEchoList);

    procedure AddEvent(ID: Byte; SysCoord: Integer = -MaxInt; DisCoord: Integer = -MaxInt);

    function GetEventCount: Integer; // ?
    function GetEvent(Index: Integer): TFileEvent; // ?

    function NormalizeDisCoord(Src: Integer): Integer;
    function GetMaxDisCoord: Integer;
    function GetMaxSysCoord: Integer;

    function SkipEvent(StreamIdx: Integer; ID: Byte; FuncIdx: Integer = - 1): Boolean; // ������� ���� ������� (������ �� ������ �2)
    function GetEventBodyLen(StreamIdx: Integer; ID: Byte): Integer; // ����������� ����� ���� ������� // ��������� �������� ������ �������������� ��������������� �� ������ ����� ���� ����� ���� ������� - ������� ��� ����� ���������� ����

    function GetCurLoadOffset(Index: Integer): Integer;   // FCurOffset
    function GetCurLoadSysCoord(Index: Integer): Longint; // FCurSysCoord
    function GetCurLoadDisCoord(Index: Integer): Int64;   // FCurDisCoord
    function GetCurLoadEcho(Index: Integer): TCurEcho;   // FCurEcho
    function GetCurLoadBackMotion(Index: Integer): Boolean;
    function GetCurLoadParams(Index: Integer): TEvalChannelsParams;
    function GetCurSaveParams(Rail: TRail; EvalCh: Integer): TEvalChannelParams;
    function GetLoadSensor1(Index: Integer): TSensor1Data;
    function GetLoadZPMode(Index: Integer): TZerroProbMode;
//    function GetStartCoord(): TRealCoord;

  public
    InstanceCount_: Integer;
    GetNearestEventIdxLeftRes: Boolean;
    GetNearestEventIdxRightRes: Boolean;
    SkipDataClose: Boolean;

    constructor Create(Cfg: TDataFileConfig; MakeFile, FullHeader, NewSysCoordSystem, ExtendedChannels: Boolean; FileName: string);
    destructor Destroy; override;
    procedure SaveToStream(Dest: TStream); // ������ � �����

    // ���������� ��������� �����:

    procedure AddDeviceUnitInfo(UnitType: TDeviceUnitType; WorksNumber, FirmwareVer: string); // �������� ���������� � ����� �������
    procedure SetRailRoadName(Name: string); // �������� �������� ������
    procedure SetOrganizationName(Org: string); // �������� ����������� �������������� ��������
    procedure SetDirectionCode(Code: DWord); // ��� �����������
    procedure SetPathSectionName(Name: string); // �������� ������� ������
    procedure SetRailPathNumber(Number: Integer); // ����� �/� ����
    procedure SetRailSection(Number: Integer); // ����� ������
    procedure SetDateTime(DateTime: TDateTime); // ����/����� ��������
    procedure SetOperatorName(Name: string); // ��� ���������
    procedure AddHeader(MovDir: Integer; ScanStep: Word{; CoordSys: TCoordSys});
    procedure SetStartMRFCrd(StartKM, StartPk, StartMetre: Integer);
    procedure SetStartCaCrd(CoordSys: TCoordSys; Chainage: TCaCrd);
    procedure SetWorkRailTypeA(Val: Byte); // ������� ���� (��� ������������ ��������): 0 � �����, 1 � ������
    procedure SetWorkRailTypeB(Val: TCardinalPoints); // ������� ���� (��� ������������ ��������): NR, SR, WR, ER
    procedure SetTrackDirection(Val: TCardinalPoints); // ��� �����������: NB, SB, EB, WB, CT, PT, RA, TT
    procedure SetTrackID(Val: TCardinalPoints); // Track ID: NR, SR, ER, WR
    procedure SetCorrespondenceSides(Val: TCorrespondenceSides); // ������������ ������ ������� ����� ����
    procedure SetUseGPSTrack; // ������� GPS ����
    procedure SetTemperature; // �������������� ������ ����������� ���������� �����
    procedure SetSpeed; // �������������� ������ �������� ��������
    procedure SetControlDirection(cDir: Byte); // ����������� ��������: 0 - A-forward, 1 - B-forward
    procedure SetGaugeSideLeftSide(); // ������������ ������� ������� ������� / �� ������� ����� ������� ������
    procedure SetGaugeSideRightSide(); // ������������ ������� ������� ������� / �� ������� ����� ������� ������

    // ������� � ����������:

    procedure AddSysCoord(SysCoord: Integer);
    procedure AddTestFullSysCoord_UMUA_Left(SysCoord: Integer);
    procedure AddTestFullSysCoord_UMUB_Left(SysCoord: Integer);
    procedure AddTestFullSysCoord_UMUA_Right(SysCoord: Integer);
    procedure AddTestFullSysCoord_UMUB_Right(SysCoord: Integer);
{
    procedure AddTestDeltaSysCoord_UMUA_Left(Delta: ShortInt);
    procedure AddTestDeltaSysCoord_UMUB_Left(Delta: ShortInt);
    procedure AddTestDeltaSysCoord_UMUA_Right(Delta: ShortInt);
    procedure AddTestDeltaSysCoord_UMUB_Right(Delta: ShortInt);
}
    function AddEcho(Rail: TRail; ScanChNum, Count, D0, A0, D1, A1, D2, A2, D3, A3, D4, A4, D5, A5, D6, A6, D7, A7: Byte): Boolean; overload;
    function AddEcho(Rail: TRail; ScanChNum, Count: Byte; EchoBlock: TEchoBlock): Boolean; overload;
    procedure AddSensor1State(Rail: TRail; State: Boolean);
    procedure SetZerroProbMode(Mode: TZerroProbMode);
    procedure AddLongLabel(Rail: TRail; EvalCh, ScanCh: Byte; Type_: Byte; StartDisCoord, EndDisCoord, Data1, Data2, Data3: Integer); // ����� ������ �������� 0 ����

    // ��������� �������:

    procedure AddKu(Rail: TRail; EvalCh: Integer; NewValue: ShortInt); // ��������� �������� ����������������
    procedure AddAtt(Rail: TRail; EvalCh: Integer; NewValue: Byte); // ��������� ��������� ���� ����������� (0 �� �������� ����������������)
    procedure AddTVG(Rail: TRail; EvalCh: Integer; NewValue: Byte); // ��������� ���
    procedure AddStStr(Rail: TRail; EvalCh: Integer; NewValue: Byte); // ��������� ��������� ������ ������
    procedure AddEndStr(Rail: TRail; EvalCh: Integer; NewValue: Byte); // ��������� ��������� ����� ������
    procedure AddPrismDelay(Rail: TRail; EvalCh: Integer; NewValue: Word); // ��������� 2�� ��� * 10 (word)

    // ������:

    procedure AddMode(ModeIdx, InPreviousModeTime: Word; AddChInfo: Boolean; Rail: TRail; EvalCh: Byte);
    procedure AddSetRailType;
    procedure AddHeadPh(Rail: TRail; EvalCh: Byte; Enabled: Boolean);

    // �������:

    procedure AddTextLabel(Text: string);
    procedure AddDefLabel(Rail: TRail; Text: string);
    procedure AddStrelka(Text: string);
    procedure AddStBoltStyk;
    procedure AddEdBoltStyk;
    procedure AddTime(Time: TTime);

    procedure AddMRFPost(Km0, Km1, Pk0, Pk1: Integer);
    procedure AddCaPost(Chainage: TCaCrd);
    procedure AddAirBrushMark(Rail: TRail; DisCoord: Integer; Items: TABItemsList);
    procedure AddAirBrushMark2(Rail: TRail; DisCoord: Integer; Items: TABItemsList2);
    procedure AddAirBrushJob(Rail: TRail; DisCoord: Integer; ErrCod: Byte; Delta: Word);
    procedure AddHandScanFromDataContainer(SrcData: TAviconDataContainer; Rail: TRail; Surf, HandChNum, Att, Ku, TVG: ShortInt; StartCrd, EndCrd: Integer);
    procedure AddAirBrushTempOff(isOff: Boolean {; DisCoord: Integer});
    procedure AddAlarmTempOff(isOff: Boolean {; DisCoord: Integer});
    procedure AddSpeed(Speed:	Single; SpeedState: TSpeedState); // �������� � ���������� �������� ��������
    procedure AddGPSCoord(Lat, Lon, Speed:	Single); // �������������� ����������: Lat - ������; Lon - �������
    procedure AddGPSState(State: Byte; UseSatCount: Byte; AntennaConnected: Boolean; Reserv: GPSStateReserv); // ��������� ��������� GPS

    procedure AddNORDCODate(DisCoord: Integer; NewLine: TNordcoCSVRecordLine); overload;
    procedure AddNORDCODate(StDisCoord, EdDisCoord: Integer;
                            Lat, Lon: Single;
                            CodeDefect: string;
                            SourceType: TSourceType;
                            Status: TNStatus;
                            IsFailure: Boolean;
                            Size, Size2, Size3: Single; UseSize, UseSize2, UseSize3: Boolean;
                            CommentText: string); overload;
    procedure AddPaintSystemParams(PaintSystemParams: TPaintSystemParams);
    procedure AddDebugData(Data: TDebugDataArr);
//    procedure AddUMUPaintJob(DisCoord: Integer);

    // ��������:

    procedure LoadData(StreamIdx: Integer; StartDisCoord, EndDisCoord, LoadShift: Integer; BlockOk: TDataNotifyEvent);
    function DisToFileOffset(StreamIdx: Integer; NeedDisCoord: Integer; var DisCoord: Int64; var SysCoord: Integer; var OffSet: Integer): Boolean;
    function GetParamFirst(StreamIdx: Integer; DisCoord: Integer; var Params: TEvalChannelsParams; var Sensor1: TSensor1Data; var ZPMode: TZerroProbMode): Boolean;
    function GetParamNext(StreamIdx: Integer; DisCoord: Integer; var Params: TEvalChannelsParams; var Sensor1: TSensor1Data; var ZPMode: TZerroProbMode): Boolean;
    function GetBMStateFirst(StreamIdx: Integer; DisCoord: Integer): Boolean;
    function GetBMStateNext(StreamIdx: Integer; var DisCoord: Integer; var State: Boolean): Boolean;

    function SysToDisCoord(Coord: Integer): Integer;
    function DisToSysCoord(Coord: Integer): Integer;
    procedure DisToDisCoords(Coord: Integer; var Res: TIntegerDynArray);

    function GetEventIdx(StartDisCoord, EndDisCoord: Integer; var StartIdx, EndIdx: Integer; EmptyPar: Integer): Boolean;
    procedure GetNearestEventIdx(DisCoord: Integer; var LeftIdx, RightIdx: Integer; var SameCoord: TIntegerDynArray);
    function GetLeftEventIdx(DisCoord: Integer): Integer;
    function GetRightEventIdx(DisCoord: Integer): Integer;
    procedure GetEventData(Idx: Integer; var ID: Byte; var pData: PEventData);

  // -------------< ������� ��������� - ����������� �� >-----------------------------------

    function DisToMRFCrdPrm(DisCoord: Integer; var CoordParams: TMRFCrdParams): Boolean;
    function DisToMRFCrd(DisCoord: Longint): TMRFCrd;
    function MRFCrdToDisCrd(RFC: TMRFCrd; var DisCoord: Longint): Boolean;

  // -------------< ������� ��������� - ��������� >----------------------------------------

    function DisToCaCrdPrm(DisCoord: Integer; var CoordParams: TCaCrdParams): Boolean;
    function DisToCaCrd(DisCoord: Longint): TCaCrd;
    function CaCrdToDis(CaCrd: TCaCrd; var DisCoord: Longint): Boolean;

  // --------------------------------------------------------------------------------------

    function isTwoWheels: Boolean; // ���� �������� (���) �������
    function isVMTUS_Scheme_1: Boolean; // �������
    function isVMTUS_Scheme_2: Boolean; // ������� 3x65
    function isVMTUS_Scheme_4: Boolean; // ����   (���������� 45)
    function isVMTUS_Scheme_5: Boolean; // ������ (���������� 55)
    function isVMTUSW: Boolean; // Linux (���������� 55)
    function isVMTUS: Boolean; // ������-26 ����� VMT � ������������ �������� � ��� �������
    function isVMTUS_BHead: Boolean;
    function isFilus_X17DW: Boolean;

    function isEGOUS_Scheme1: Boolean; //
    function isEGOUS_Scheme2: Boolean;
    function isEGOUS_Scheme3: Boolean;
    function isEGOUS_WheelScheme1: Boolean;
    function isEGOUS_WheelScheme2: Boolean;
    function isEGOUS: Boolean;

    property MaxDisCoord: Integer read GetMaxDisCoord;
    property MaxSysCoord: Integer read GetMaxSysCoord;
    property EventCount: Integer read GetEventCount;
    property Event[Index: Integer]: TFileEvent read GetEvent;

    property CurSaveSysCoord: Longint read FCurSaveSysCrd;  // ��������� ���������� ������
    property CurSaveDisCoord: Integer read FCurSaveDisCrd;  // ���������� ���������� ������

                                                                               // �������� - LoadData
    property CurLoadOffset[Index: Integer]: Integer read GetCurLoadOffset;     // FCurOffset;
    property CurLoadSysCoord[Index: Integer]: Longint read GetCurLoadSysCoord; // FCurSysCoord;
    property CurLoadDisCoord[Index: Integer]: Int64 read GetCurLoadDisCoord;   // FCurDisCoord;
    property CurLoadEcho[Index: Integer]: TCurEcho read GetCurLoadEcho;        // FCurEcho
    property CurLoadBackMotion[Index: Integer]: Boolean read GetCurLoadBackMotion;
    property CurLoadParams[Index: Integer]: TEvalChannelsParams read GetCurLoadParams;
    property CurLoadSensor1[Index: Integer]: TSensor1Data read GetLoadSensor1;
    property CurLoadZPMode[Index: Integer]: TZerroProbMode read GetLoadZPMode;

    property CurSaveParams[Rail: TRail; EvalCh: Integer]: TEvalChannelParams read GetCurSaveParams;
    property Config: TDataFileConfig read FConfig;
    property Header: TFileHeader read FHead;

//    property StartCoord: TRealCoord read GetStartCoord;
    property CoordSys: TCoordSys read FPathCoordSystem;
    property LastCaPost: TCaCrd read FLastCaPost;

    property AirBrushTempOffState: Boolean read FAirBrushTempOffState;
    property AirBrushTempOffDisCrd: Integer read FAirBrushTempOffDisCrd;

  end;

//var
//  ConfigList: TDataFileConfigList;
//  function GetString(Len: Byte; Text: array of Char): string;


implementation

var
  InstanceCount: Integer = 0;
//  GlobalCount: Integer = 0;


// ------------------------------------------------------------------------------
{
function GetString(Len: Byte; Text: array of Char): string;
var
  I: Integer;

begin
  Result:= '';
  for I := 0 to Len - 1 do Result:= Result + Text[I];
end;
}
// ------------------------< TMyStream >-----------------------------------------

constructor TMyStream.Create(FileName: string);
var
  I: Integer;

begin
  CS:= TCriticalSection.Create;
  FPtr:= nil;
  FSize:= 0;
  FCap:= 0;
  for I:= 0 to High(FPosition) do FPosition[I]:= 0;
  FNoFile:= FileName = '';
  if not FNoFile then
  begin
    AssignFile(FDataFile, FileName);
    ReWrite(FDataFile, 1);
  end;
  FLastSave:= 0;
end;

destructor TMyStream.Destroy;
begin
  FreeMem(FPtr);
  if not FNoFile then System.CloseFile(FDataFile);
  CS.Free;
end;

function TMyStream.GetPosition(Idx: Integer): Longint;
begin
  Result:= FPosition[Idx];
end;

procedure TMyStream.SetPosition(Idx: Integer; NewPos: Longint);
begin
  if NewPos > FSize then raise EMyExcept.Create(Format('TMyStream: Position%d out of bounds', [Idx]));
  FPosition[Idx]:= NewPos;
end;

procedure TMyStream.WriteBuffer(Idx: Integer; const Buffer; Count: Longint);
begin
  CS.Enter;
  if FPosition[Idx] + Count > FCap then
  begin
    Inc(FCap, 1024 * 1024);
    ReallocMem(FPtr, FCap);
  end;
  Move(Buffer, Pointer(Longint(FPtr) + FPosition[Idx])^, Count);
  Inc(FPosition[Idx], Count);
  FSize:= Max(FSize, FPosition[Idx]);
  CS.Leave;
end;

procedure TMyStream.ReadBuffer(Idx: Integer; var Buffer; Count: Longint);
begin
//  CS.TryEnter;
  CS.Enter;
  if FPosition[Idx] + Count > FSize then raise EMyExcept.Create(Format('TMyStream: Position%d out of bounds', [Idx]));
  Move(Pointer(Longint(FPtr) + FPosition[Idx])^, Buffer, Count);
  Inc(FPosition[Idx], Count);
  CS.Leave;
end;

procedure TMyStream.SaveToFile(MinSaveSize: Integer = 0); // ������ ������� �����
var
  P: Pointer;

begin
  if FNoFile then Exit;
  CS.Enter;
  if  (MinSaveSize = 0) or
     ((MinSaveSize > 0) and (Size - FLastSave > MinSaveSize)) then
  begin
    P:= Pointer(Integer(FPtr) + FLastSave);
    BlockWrite(FDataFile, P^, Size - FLastSave);
    FLastSave:= Size;
  end;
  CS.Leave;
end;

procedure TMyStream.SaveToStream(Dest: TStream); // ������ � �����
begin
  CS.Enter;
  Dest.Size:= 0;
  Dest.Position:= 0;
  Dest.WriteBuffer(FPtr^, FSize);
  CS.Leave;
end;

// ------------------------< TMyFileStream >-----------------------------------------

constructor TMyFileStream.Create(FileName: string);
var
  I: Integer;

begin
//  {$IFDEF LOG}
//  AssignFile(FLog, 'MyFileStream.LOG');
//  Rewrite(FLog);
//  {$ENDIF}

  CS:= TCriticalSection.Create;
//  FPtr:= nil;
//  FSize:= 0;
//  FCap:= 0;
  for I:= 0 to High(FPosition) do FPosition[I]:= 0;
//  FNoFile:= FileName = '';
//  if not FNoFile then
//  begin
//    AssignFile(FDataFile, FileName);
//    ReWrite(FDataFile, 1);
//  end;
//  FLastSave:= 0;

  DeleteFile(FileName);
  FStream:= TFileStream.Create(FileName, fmCreate);
end;

destructor TMyFileStream.Destroy;
begin
//  {$IFDEF LOG}
//  CloseFile(FLog);
//  {$ENDIF}

//  FreeMem(FPtr);
//  if not FNoFile then System.CloseFile(FDataFile);
  FStream.Free;
  CS.Free;
end;

function TMyFileStream.GetPosition(Idx: Integer): Longint;
begin
  Result:= FPosition[Idx];
end;

procedure TMyFileStream.SetPosition(Idx: Integer; NewPos: Longint);
begin

  if NewPos > FStream.Size then raise EMyExcept.Create(Format('TMyStream: Position%d out of bounds', [Idx]));
  FPosition[Idx]:= NewPos;
end;

procedure TMyFileStream.WriteBuffer(Idx: Integer; const Buffer; Count: Longint);
begin

//  {$IFDEF LOG}
//  Writeln(FLog, Format('WriteBuffer - Idx: %d; Pos: %d; Cnt: %d', [Idx, FPosition[Idx], Count]));
//  {$ENDIF}

  CS.Enter;
  FStream.Position:= FPosition[Idx];
  FStream.WriteBuffer(Buffer, Count);
  Inc(FPosition[Idx], Count);
  CS.Leave;
end;

procedure TMyFileStream.ReadBuffer(Idx: Integer; var Buffer; Count: Longint);
begin
  CS.Enter;
  if FPosition[Idx] + Count > FStream.Size then raise EMyExcept.Create(Format('TMyStream: Position%d out of bounds', [Idx]));
  FStream.Position:= FPosition[Idx];
  FStream.ReadBuffer(Buffer, Count);
  Inc(FPosition[Idx], Count);
  CS.Leave;
end;

procedure TMyFileStream.SaveToFile(MinSaveSize: Integer = 0); // ������ ������� �����
var
  P: Pointer;

begin
{  if FNoFile then Exit;
  CS.Enter;
  if  (MinSaveSize = 0) or
     ((MinSaveSize > 0) and (Size - FLastSave > MinSaveSize)) then
  begin
    P:= Pointer(Integer(FPtr) + FLastSave);
    BlockWrite(FDataFile, P^, Size - FLastSave);
    FLastSave:= Size;
  end;
  CS.Leave; }
end;

procedure TMyFileStream.SaveToStream(Dest: TStream); // ������ � �����
begin
{  CS.Enter;
  Dest.Size:= 0;
  Dest.Position:= 0;
  Dest.WriteBuffer(FPtr^, FSize);
  CS.Leave; }
end;

// ------------------------< TMyArray >------------------------------------------

constructor TMyArray.Create(CreateVirtualItem: Boolean);
begin
  FItemSize:= SizeOf(TContainerEvent);
  FAddIdx:= 0;
  FCreateVirtItem:= CreateVirtualItem;
  FCount:= Ord(FCreateVirtItem);
  FRealCount:= 0;
end;

destructor TMyArray.Destroy;
var
  I: Integer;

begin
  for I:= 0 to High(FPtr) do FreeMem(FPtr[I]);
  SetLength(FPtr, 0);
end;

function TMyArray.Add(const Buffer): Integer;
begin
  if (FAddIdx > 999) or (Length(FPtr) = 0) then
  begin
    SetLength(FPtr, Length(FPtr) + 1);
    GetMem(FPtr[ High(FPtr)], 1000 * FItemSize);
    FAddIdx:= 0;
  end;
  Move(Buffer, Pointer(Longint(FPtr[ High(FPtr)]) + FAddIdx * FItemSize)^, FItemSize);
  Result:= FRealCount;
  Inc(FAddIdx);
  Inc(FCount);
  Inc(FRealCount);
end;

(*
function TMyArray.GetEvent(Index: Integer): TFileEvent;
var
  I: Integer;

begin
  if FCreateVirtItem and (Index = FCount - 1) then
  begin
    Result:= VirtItem.Event;
    Exit;
  end;
  I:= 0;
  while Index >= 1000 do
  begin
    Index:= Index - 1000;
    Inc(I);
  end;
  if Index >= Count then raise EMyExcept.Create('TMyArray: Position out of bounds');
  try
    Result:= TContainerEvent(Pointer(Longint(FPtr[I]) + Index * FItemSize)^).Event;
  except
    raise EMyExcept.Create(Format('TMyArray: Position out of bounds Index: %d; PtrArrIdx: %d', [Index, I]));
  end;
end; *)

function TMyArray.GetDataPtr(Index: Integer): PEventData;
var
  I: Integer;

begin
  if FCreateVirtItem and (Index = FCount - 1) then
  begin
    Result:= nil;
    Exit;
  end;
  I:= 0;
  while Index >= 1000 do
  begin
    Index:= Index - 1000;
    Inc(I);
  end;
  if Index >= Count then raise EMyExcept.Create('TMyArray: Position out of bounds');
  try
//    Result:= PEventData(Longint(FPtr[I]) + Index * FItemSize + 13);
    Result:= PEventData(@TContainerEvent(Pointer(Longint(FPtr[I]) + Index * FItemSize)^).Event.Data);
  except
    raise EMyExcept.Create(Format('TMyArray: Position out of bounds Index: %d; PtrArrIdx: %d', [Index, I]));
  end;
end;

function TMyArray.GetItem_(Index: Integer): TContainerEvent;
var
  I: Integer;

begin
  if FCreateVirtItem and (Index = FCount - 1) then
  begin
    Result:= FVirtItem;
    Exit;
  end;
  I:= 0;
  while Index >= 1000 do
  begin
    Index:= Index - 1000;
    Inc(I);
  end;
  if Index >= Count then raise EMyExcept.Create('TMyArray: Position out of bounds');
  try
    Result:= TContainerEvent(Pointer(Longint(FPtr[I]) + Index * FItemSize)^);
  except
    raise EMyExcept.Create(Format('TMyArray: Position out of bounds Index: %d; PtrArrIdx: %d', [Index, I]));
  end;
end;

// ------------------------< TEventArray >------------------------------------------

constructor TEventArray.Create(CreateVirtualItem: Boolean);
begin
  FItemSize:= SizeOf(TContainerEvent);
  FCreateVirtItem:= CreateVirtualItem;
  FCount:= Ord(FCreateVirtItem);
  FRealCount:= 0;
  FPtr:=Nil;
  GetMem(FPtr, MAX_EVENT_ARRAY * FItemSize);
  FBegIdx:= 0;
  FEndIdx:= 0;
end;

destructor TEventArray.Destroy;
var
  I: Integer;
begin
  if Assigned(FPtr) then FreeMem(FPtr);
end;

function TEventArray.Add(const Buffer): Integer;
begin
  Move(Buffer, Pointer(Longint(FPtr) + FEndIdx * FItemSize)^, FItemSize);
  Result:= FRealCount;
  inc(FEndIdx);
  if FEndIdx >= MAX_EVENT_ARRAY then FEndIdx:= 0;
  if FBegIdx = FEndIdx then begin
    inc(FBegIdx);
    if FBegIdx >= MAX_EVENT_ARRAY then FEndIdx:= 0;
  end
  else begin
    Inc(FCount);
    Inc(FRealCount);
  end;
end;

function TEventArray.GetDataPtr(Index: Integer): PEventData;
var
  I: Integer;

begin
  if FCreateVirtItem and (Index = FCount - 1) then
  begin
    Result:= nil;
    Exit;
  end;
  if Index >= Count then raise EMyExcept.Create('TMyArray: Position out of bounds');
  Index:= FBegIdx + Index;
  while Index >= MAX_EVENT_ARRAY do Index:= Index - MAX_EVENT_ARRAY;
  try
    Result:= PEventData(@TContainerEvent(Pointer(Longint(FPtr) + Index * FItemSize)^).Event.Data);
  except
    raise EMyExcept.Create(Format('TMyArray: Position out of bounds Index: %d', [Index]));
  end;
end;

function TEventArray.GetItem_(Index: Integer): TContainerEvent;
var
  I: Integer;

begin
  if FCreateVirtItem and (Index = FCount - 1) then
  begin
    Result:= FVirtItem;
    Exit;
  end;
  if Index >= Count then raise EMyExcept.Create('TMyArray: Position out of bounds');
  Index:= FBegIdx + Index;
  while Index >= MAX_EVENT_ARRAY do Index:= Index - MAX_EVENT_ARRAY;
  try
    Result:= TContainerEvent(Pointer(Longint(FPtr) + Index * FItemSize)^);
  except
    raise EMyExcept.Create(Format('TMyArray: Position out of bounds Index: %d', [Index]));
  end;
end;


// ------------------------< TAvk11DataContainer >---------------------------------------

constructor TAviconDataContainer.Create(Cfg: TDataFileConfig; MakeFile, FullHeader, NewSysCoordSystem, ExtendedChannels: Boolean; FileName: string);
//constructor TAviconDataContainer.Create(Cfg: TDataFileConfig; FileName: string; NewSysCoordSystem: Boolean = False);
var
  R: TRail;
  I: Integer;

begin
  FGetNearestEventIdx_StartIndex:= 0;

  SkipDataClose:= True;//False; //�� ������ ������ ������� � ���� �.�. ������ ������ � ������ ������ ����� ������
  Inc(InstanceCount);
  InstanceCount_:= InstanceCount;

  {$IFDEF LOG}
  AssignFile(FLog, Format('AviconDataContainer_%x%x%x%x%x%x%x%x_ic_%d.LOG', [Cfg.DeviceID[0], Cfg.DeviceID[1], Cfg.DeviceID[2], Cfg.DeviceID[3], Cfg.DeviceID[4], Cfg.DeviceID[5], Cfg.DeviceID[6], Cfg.DeviceID[7], InstanceCount]));
  Rewrite(FLog);
  Writeln(FLog, Format('Create - MakeFile: %d; FullHeader: %d; NewSysCoordSystem: %d;', [Ord(MakeFile), Ord(FullHeader), Ord(NewSysCoordSystem)]));
  {$ENDIF}

  FLastPostSysCrd:= - 1;
  FLastPostDisCrd:= - 1;

  FNewSysCoordSystem:= NewSysCoordSystem;
  FMakeFile:= MakeFile;
  FFullHeader:= FullHeader;
  FExtendedChannels:= ExtendedChannels;
//  FOnlyBScanData:= Cfg.DeviceID = MIG;

  FCS:= TCriticalSection.Create;
  FConfig:= Cfg;

  for I:= 0 to FConfig.ScanChannelCount - 1 do
  begin
    FScanChNumExists[I]:= False;
    FEvalChNumExists[I]:= False;
  end;

//  ClearEchoList(FCurrEchoList);
//  ClearEchoList(FPrevEchoList);
{  for R:= Cfg.MinRail to Cfg.MaxRail do
    for I:= 0 to 15 do
    begin
      FCurrEchoList[R, I].Count:= 0;
      //FPackedState[R, I]:= False;
    end;
  FPrevEchoList:= FCurrEchoList; }

  for I:= 0 to Cfg.ScanChannelCount - 1 do
    FScanChNumExists[Cfg.ScanChannelByIdx[I].BS_Ch_File_Number]:= True;

  for I:= 0 to Cfg.EvalChannelCount - 1 do
    FScanChNumExists[Cfg.EvalChannelByIdx[I].Number]:= True;

  FFirstCoord:= True;      // ���� - ������ ����������
  FLastSaveDisCrd:= 0;     // ���������� ���������� ����������
  FLastSaveSysCrd:= 0;     // ���������� ��������� ����������
  FLastSysCrdOffset:= 0;   // �������� �� ��������� ����������� ������� ����������
  FCurSaveDisCrd:= 0;      //
  FCurSaveSysCrd:= 0;      //
  FFirstAddCoord:= True;
  FLastAddSysCrd:= 0;
//  FStolbCount:= 0;
  FMaxSysCoord:= 0;        //
  FMinSysCoord:= 0;
  FBackMotionFlag:= False; //
  FSearchBMEnd:= False;    //
  FLastCheckSumOffset:= 0;
  FillChar(FHead.FileID, SizeOf(TFileHeader), 0);
  FHead.FileID:= AviconFileID;

  FHead.DeviceID:= FConfig.DeviceID; // ��� ������������
//  FHead.DeviceID:= (FHead.DeviceID and $00FFFFFFFFFFFFFF) or Ord(FFullHeader) shl 56;
  (PByte(Pointer(Integer(@FHead.DeviceID) + 7)))^:= Ord(FFullHeader);

  (PByte(Pointer(Integer(@FHead.DeviceID) + 7)))^:= ((PByte(Pointer(Integer(@FHead.DeviceID) + 7)))^) or (2 * Ord(FExtendedChannels));

  FHead.DeviceVer:= FConfig.DeviceVer; // ������ ������������
{
        FHead.ChIdxtoCID[0]:= $01;
        FHead.ChIdxtoGateIdx[0]:= 1;

        FHead.ChIdxtoCID[1]:= $01;
        FHead.ChIdxtoGateIdx[1]:= 0;

        FHead.ChIdxtoCID[2]:= $06;
        FHead.ChIdxtoGateIdx[2]:= 0;
        FHead.ChIdxtoCID[3]:= $0B;
        FHead.ChIdxtoGateIdx[3]:= 0;
        FHead.ChIdxtoCID[4]:= $17;
        FHead.ChIdxtoGateIdx[4]:= 0;
        FHead.ChIdxtoCID[5]:= $18;
        FHead.ChIdxtoGateIdx[5]:= 0;
        FHead.ChIdxtoCID[6]:= $19;
        FHead.ChIdxtoGateIdx[6]:= 0;
        FHead.ChIdxtoCID[7]:= $1A;
        FHead.ChIdxtoGateIdx[7]:= 0;
        FHead.ChIdxtoCID[8]:= $19;
        FHead.ChIdxtoGateIdx[8]:= 1;
        FHead.ChIdxtoCID[9]:= $1A;
        FHead.ChIdxtoGateIdx[9]:= 1;
        FHead.ChIdxtoCID[10]:= $07;
        FHead.ChIdxtoGateIdx[10]:= 0;
        FHead.ChIdxtoCID[11]:= $0C;
        FHead.ChIdxtoGateIdx[11]:= 0;
}

  FHead.HeaderVer:= 4; // ������ ���������

//  FAnalyzeCount:= 0;
//  FEventFlag:= False;
//  FEventsCount:= 0;
//  FHead.CheckSumm:= 0; // ����������� �����
//  FillChar(FHead.UsedItems[1], 64, 0); // ����� ������������ ������������ ������
//  FHead.UnitsCount:= 0;
//  FillChar(FHead.Reserv[1], 2048, 0); // ������
//  FHead.TableLink:= 0; // ������ �� ����������� ���������

  if FMakeFile then
  begin
    FFileName:= FileName;    // ��� �����
    {$IFDEF USEFILESTREAM}
    FData:= TMyFileStream.Create(FileName);
    {$ENDIF}
    {$IFNDEF USEFILESTREAM}
    FData:= TMyStream.Create(FileName);
    {$ENDIF}
  end
  else
    {$IFDEF USEFILESTREAM}
    FData:= TMyFileStream.Create('');
    {$ENDIF}
    {$IFNDEF USEFILESTREAM}
    FData:= TMyStream.Create('');
    {$ENDIF}

  if not FFullHeader then{ FData.WriteBuffer(SaveStrIdx, FHead, SizeOf(TFileHeader))
                     else}
  begin
    FData.WriteBuffer(SaveStrIdx, FHead, 17);
//    FData.Position[SaveStrIdx]:= 0;
  end;

//  FEvents:= TMyArray.Create(True);
  FEvents:= TEventArray.Create(True);
  FEvents.InstanceCount_:= InstanceCount;
  FData.SaveToFile();
  FEvents.FVirtItem.Event.ID:= EID_EndFile;

  for I:= 0 to 3 do FGetParamIdx[I]:= - 1;
end;

destructor TAviconDataContainer.Destroy;
var
  Buff: array [5 .. 13] of Byte;
  I, TMP: Integer;
  TMP2: DWord;
  Evt: TFileEvent;
//  EndRC: TRealCoord;
  F: file;

begin
  if (FEvents.Count <> 0) and (FEvents[FEvents.Count - 1].Event.DisCoord <> FCurSaveDisCrd) then
  begin
    FLastOffset:= FData.Position[SaveStrIdx];
    AddEvent(EID_LinkPt);
  end;

  // ������ ������� ����� �����
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_EndFile;
  FData.WriteBuffer(SaveStrIdx, FID, 1);

  for TMP:= 5 to 13 do Buff[TMP]:= $FF;

  if not FNewSysCoordSystem then
  begin
    TMP:= FData.Size - FLastSysCrdOffset; // ������ ������ ����� ��������� ��������� ����������
    FData.WriteBuffer(SaveStrIdx, TMP, 4);
  end
  else
  begin
    TMP2:= $FFFFFFFF; // ������ ������ ����� ��������� ��������� ����������
    FData.WriteBuffer(SaveStrIdx, TMP2, 4);
  end;
  FData.WriteBuffer(SaveStrIdx, Buff, 9);
  // ������ ������� ����� �����
  AddEvent(EID_EndFile);

  FData.SaveToFile;
  FData.Free;

  if not SkipDataClose then
    if FMakeFile and FFullHeader then
    begin
      AssignFile(F, FFileName); // �������� ����� ��� ������
      Reset(F, 1);



      (*
        {$OVERFLOWCHECKS OFF}
        CheckSumm:= CheckSummStartValue;
        FData.Position:= 0;
        for I:= 1 to 29 do
        begin
        FData.ReadBuffer(Card, 4);
        CheckSumm:= CheckSumm + Card;
        end;
        FData.Position:= FData.Position + 3 * 4;
        while FData.Position + 4 < FileSize(F) do
        begin
        FData.ReadBuffer(Card, 4);
        CheckSumm:= CheckSumm + Card;
        end;
        Header.CheckSumm:= CheckSumm and $00000000FFFFFFFF;
        {$OVERFLOWCHECKS ON}
        *)

        Seek(F, 0);
  //    if  then
  //    begin
        FHead.TableLink:= FileSize(F); // ����������� ��������� ������ ������������ ���������
        BlockWrite(F, FHead, SizeOf(TFileHeader)); // ������ ������ �������� ��������� ������������ ���������

        if FPathCoordSystem = csMetricRF then FExHeader.EndMRFCrd:= DisToMRFCrd(FCurSaveDisCrd)
                                         else FExHeader.EndCaCrd:= DisToCaCrd(FCurSaveDisCrd);

        FillChar(FExHeader.FFConst[0], Length(FExHeader.FFConst), $FF);
        FillChar(FExHeader.CoordReserv[0], Length(FExHeader.CoordReserv), $FF);
        FillChar(FExHeader.Reserv1[0], Length(FExHeader.Reserv1), $FF);
        FillChar(FExHeader.Reserv2[0], Length(FExHeader.Reserv2), $FF);

        FExHeader.DataVer:= 7;
        FExHeader.DataSize:= SizeOf(TExHeader);

        Seek(F, FHead.TableLink); // ������ ������ ������������ ���������
        BlockWrite(F, FExHeader, FExHeader.DataSize);

        FExHeader.EventDataVer:= 5; // ������ ������ ������� �����
        FExHeader.EventItemSize:= 13;
        FExHeader.EventCount:= FEvents.RealCount;
        FExHeader.EventOffset:= 0;
        if FExHeader.EventCount <> 0 then
        begin
          FExHeader.EventOffset:= FilePos(F);
          for I:= 0 to FEvents.RealCount - 1 do
          begin
            Evt:= FEvents[I].Event;
            BlockWrite(F, Evt, FExHeader.EventItemSize);
          end;
        end;

        FExHeader.NotebookDataVer:= 7; // ������ ������ ������� �������������
        FExHeader.NotebookOffset:= 0;
        FExHeader.NotebookItemSize:= SizeOf(TFileNotebook);
        FExHeader.NotebookCount:= 0;

        Seek(F, FHead.TableLink);
        BlockWrite(F, FExHeader, FExHeader.DataSize);
        Close(F);

  {    end;
      else
      begin
        FHead.TableLink:= 0; // ����������� ��������� ������ ������������ ���������
        BlockWrite(F, FHead, 18); // ������ ������ �������� ��������� ������������ ���������
      end; }
    end;

  FEvents.Free;

  // SetLength(FTimeList, 0);
  SetLength(FCDList, 0);
  // FParDat.Free;

  FCS.Free;
end;

procedure TAviconDataContainer.SaveToStream(Dest: TStream); // ������ � �����
begin
  FData.SaveToStream(Dest);
  {$IFDEF LOG}
  Writeln(FLog, 'SaveToStream');
  {$ENDIF}
end;

// ����������� ����� ���� �������
// ��������� �������� ������ �������������� ��������������� �� ������ ����� ���� ����� ���� ������� - ������� ��� ����� ���������� ����
function TAviconDataContainer.GetEventBodyLen(StreamIdx: Integer; ID: Byte): Integer;
var
  FID: Byte;
  FDWID: DWord;
  Ch: Byte;

begin
  if ID and 128 = 0 then
  begin
    Ch:= (ID shr 2) and 15;
    Result:= EchoLen[ID and $03];
    if FExtendedChannels and (Ch > 14) then Result:= Result + 1;
  end
  else
  case ID of
{0x82}      EID_HandScan: begin // ������
                             FData.ReadBuffer(StreamIdx, FDWID, 4);
                             Result:= SizeOf(THSHead) - 4 + FDWID;
                           end;

{0x90}             EID_Ku: Result:= 3;   // ��������� �������� ����������������
{0x91}            EID_Att: Result:= 3;   // ��������� �����������
{0x92}            EID_TVG: Result:= 3;   // ��������� ���
{0x93}          EID_StStr: Result:= 3;   // ��������� ��������� ������ ������
{0x94}         EID_EndStr: Result:= 3;   // ��������� ��������� ����� ������
{0x95}         EID_HeadPh: Result:= 3;   // ������ ���������� ���������
{0x96}           EID_Mode: Result:= 7;   // ��������� ������
{0x9B}    EID_SetRailType: Result:= 0;   // ��������� �� ��� ������
{0x9C}     EID_PrismDelay: Result:= 4;   // ��������� 2�� (word)
{0xA0}          EID_Stolb: Result:= 144; // ������� ����������
{0xA1}         EID_Switch: begin // ����� ����������� ��������
                             FData.ReadBuffer(StreamIdx, FID, 1);
                             Result:= FID * SizeOf(Char);
                           end;

{0xA2}       EID_DefLabel: begin // ����� �������
                             FData.ReadBuffer(StreamIdx, FID, 1);
                             Result:= 1 + FID * SizeOf(Char);
                           end;

{0xA3}      EID_TextLabel: begin // ��������� �������
                             FData.ReadBuffer(StreamIdx, FID, 1);
                             Result:= FID * SizeOf(Char);
                           end;

{0xA4}     EID_StBoltStyk: Result:= 0;   // ������� ������ ��
{0xA5}    EID_EndBoltStyk: Result:= 0;   // ���������� ������ ��
{0xA6}           EID_Time: Result:= 2;   // ������� �������
{0xA7}  EID_StolbChainage: Result:= 136; // ������� ���������� Chainage
{0xA8}  EID_ZerroProbMode: Result:= 1;   // ����� ������ �������� 0 ����
{0xA9}      EID_LongLabel: Result:= 24;  // ����������� �������
{0xAA}            EID_SpeedState: Result:= 5;   // �������� � ���������� �������� ��������
{0xAB}    EID_ChangeOperatorName: Result:= 130; // ����� ��������� (��� ���������)
{0xAC}    EID_AutomaticSearchRes: Result:= 9;  // ��������� ������� �������, ���������� ��� �������������� ������

{0xAD}        EID_TestRecordFile: begin                                  // ���� ������ ������������ ������
                                    FData.ReadBuffer(StreamIdx, FDWID, 4);
                                    Result:= FDWID;
                                  end;
{0xAE}   EID_OperatorRemindLabel: Result:= 261; // ������� ����, ������� ���������� � ������ ��� ����������� ��������� (��������� ������ � ������)



{0xAF} EID_QualityCalibrationRec: Result:= 15;  // �������� ��������� ������� ��������
{0xB0}            EID_Sensor1: Result:= 2;   // ������ ������� �� � �������
{0xB1}           EID_AirBrush: Result:= 182; // ��������������
{0xB2}       EID_PaintMarkRes: Result:= 8;   // ��������� � ���������� ������� �� ������-�������
{0xB3}    EID_AirBrushTempOff: Result:= 5;   // ��������� ���������� ���������������
{0xB4}          EID_AirBrush2: Result:= 246; // �������������� � ���������� �����������
{0xB5}       EID_AlarmTempOff: Result:= 5;   // ��������� ���������� ��� �� ���� �������
{0xB6} EID_StartSwitchShunter: Result:= 0; // ������ ���� ����������� ��������
{0xB7}   EID_EndSwitchShunter: Result:= 0; // ����� ���� ����������� ��������
{0xB8}        EID_Temperature: Result:= 5; // �����������





{0xB9}          EID_DebugData: Result:= 128;  // ���������� ����������
{0xBA}  EID_PaintSystemParams: Result:= 2048; // ��������� ������������
{0xBB}        EID_UMUPaintJob: Result:= 8;    // ������� ��� �� �������������
{0xBC}         EID_ACData: Result:= 1; // ������ ������������� ��������
{0xBD}           // ������ // ����� ������������� �������
{0xBE}           // ������ // ������ ����������� ������� ������� ������
{0xBF}           // ������ // ������� ����������� ���������� ��
{0xC0}       EID_GPSCoord: Result:= 8;   // �������������� ����������
{0xC1}       EID_GPSState: Result:= 9;   // ��������� ��������� GPS
{0xC2}          EID_Media: begin              // �����������
                             FData.ReadBuffer(StreamIdx, FDWID, 1);
                             FData.ReadBuffer(StreamIdx, FDWID, 4);
                             Result:= FDWID;
                           end;
{0xC3}      EID_GPSCoord2: Result:= 12;  // �������������� ���������� �� ���������
{0xC4}     EID_NORDCO_Rec: Result:= SizeOf(edNORDCO_Rec); // ������ NORDCO
{0xC5}       EID_SensFail:  Result:= 6;   // ���������� �������� ���������������� �� ������������ ��������


//    //       EID_CheckSum: Result:= 5;   // ����� � ����������� �����
{0xF1}      EID_SysCrd_SS: Result:= 2;   // ��������� ���������� ��������
{0xF4}      EID_SysCrd_SF: Result:= 5;   // ������ ��������� ���������� � �������� �������
{0xF9}      EID_SysCrd_FS: Result:= 5;   // ��������� ���������� ��������
{0xFC}      EID_SysCrd_FF: Result:= 8;   // ������ ��������� ���������� � ������ �������
{0xF2}      EID_SysCrd_NS: Result:= 1;
{0xF3}      EID_SysCrd_NF: Result:= 4;
{0xF5}   EID_SysCrd_UMUA_Left_NF: Result:= 4; // ������ ��������� ���������� ��� ������ ��� A, ����� �������
{0xF6}   EID_SysCrd_UMUB_Left_NF: Result:= 4; // ������ ��������� ���������� ��� ������ ��� �, ����� �������
{0xF7}  EID_SysCrd_UMUA_Right_NF: Result:= 4; // ������ ��������� ���������� ��� ������ ��� A, ������ �������
{0xF8}  EID_SysCrd_UMUB_Right_NF: Result:= 4; // ������ ��������� ���������� ��� ������ ��� �, ������ �������

        EID_EndFile: Result:= 13;  // ����� �����
    else Result:= - 1; // raise EMyExcept.Create('GetEventBodyLen: Unknown event ID');
  end;
  {$IFDEF LOG}
  Writeln(FLog, Format('GetEventBodyLen - Id: %d; Len: %d', [ID, Result]));
  {$ENDIF}
end;

function TAviconDataContainer.SkipEvent(StreamIdx: Integer; ID: Byte; FuncIdx: Integer = - 1): Boolean; // ������� ���� �������
var
  BodyLen: Integer;

begin
  {$IFDEF LOG}
  Writeln(FLog, Format('SkipEvent StrIdx: %d; ID: %x; Pos: %d', [StreamIdx, ID, FData.Position[StreamIdx]]));
  {$ENDIF}

  BodyLen:= GetEventBodyLen(StreamIdx, ID);
  if BodyLen <> - 1 then
  begin
    {$IFDEF LOG}
    Writeln(FLog, Format('BodyLen: %d', [BodyLen]));
    {$ENDIF}
    FData.Position[StreamIdx]:= FData.Position[StreamIdx] + BodyLen;
    {$IFDEF LOG}
    Writeln(FLog, Format('New Pos: %d', [FData.Position[StreamIdx]]));
    {$ENDIF}
    Result:= True;
  end else {Result:= False;}
  begin
    {$IFDEF LOG}
    Writeln(FLog, 'Unknown event ID');
    {$ENDIF}
    raise EMyExcept.Create(Format('GetEventBodyLen: Unknown event ID, StreamIdx = %d, Pos = %d, ID = %d, FuncIdx = %d ', [StreamIdx, FData.Position[StreamIdx], ID, FuncIdx]));
  end;
end;

procedure TAviconDataContainer.AddHeader(MovDir: Integer; ScanStep: Word {; CoordSys: TCoordSys});
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddHeader');
  {$ENDIF}

  FHead.MoveDir:= MovDir;
  FHead.UsedItems[uiMoveDir]:= 1; // ����������� ��������
  FHead.ScanStep:= ScanStep;

  FData.Position[SaveStrIdx]:= 0;
  if FFullHeader then FData.WriteBuffer(SaveStrIdx, FHead, SizeOf(TFileHeader))
                 else FData.WriteBuffer(SaveStrIdx, FHead, 17);


  FLastOffset:= FData.Position[SaveStrIdx];

{
  SetLength(FCoordList, 1);
  FCoordList[0].KM:= Header.StartKM;
  with FCoordList[High(FCoordList)] do
  begin
    SetLength(Content, Length(Content) + 1);
    Content[High(Content)].Pk:= Header.StartPk;
    Content[High(Content)].Idx:= 0;

    FLastKm:= High(FCoordList);
    FLastPk:= High(Content);
  end;
}
  FLastCheckSumOffset:= SizeOf(FHead);
  AddEvent(EID_FwdDir, 0, 0);
end;

procedure TAviconDataContainer.AddDeviceUnitInfo(UnitType: TDeviceUnitType; WorksNumber, FirmwareVer: string); // �������� ���������� � ����� �������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddDeviceUnitInfo');
  {$ENDIF}
  FHead.UnitsInfo[FHead.UnitsCount].UnitType:= UnitType;
  FHead.UnitsInfo[FHead.UnitsCount].WorksNumber:= StringToHeaderStr(WorksNumber);
  FHead.UnitsInfo[FHead.UnitsCount].FirmwareVer:= StringToHeaderStr(FirmwareVer);
  FHead.UnitsCount:= FHead.UnitsCount + 1;
  FHead.UsedItems[uiUnitsCount]:= 1; // ���������� ������ �������
end;

procedure TAviconDataContainer.SetRailRoadName(Name: string); // �������� �������� ������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetRailRoadName');
  {$ENDIF}
  FHead.RailRoadName:= StringToHeaderStr(Name);
  FHead.UsedItems[uiRailRoadName]:= 1; // �������� �������� ������
end;

procedure TAviconDataContainer.SetOrganizationName(Org: string); // �������� ����������� �������������� ��������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetOrganizationName');
  {$ENDIF}
  FHead.Organization:= StringToHeaderStr(Org);
  FHead.UsedItems[uiOrganization]:= 1; // �������� ����������� �������������� ��������
end;

procedure TAviconDataContainer.SetDirectionCode(Code: DWord); // ��� �����������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetDirectionCode');
  {$ENDIF}
  FHead.DirectionCode:= Code;
  FHead.UsedItems[uiDirectionCode]:= 1; // ��� �����������
end;


procedure TAviconDataContainer.SetControlDirection(cDir: Byte); // ����������� ��������
//0 - A-forward, 1 - B-forward
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetControlDirection');
  {$ENDIF}
  FHead.ControlDirection:= cDir;
  FHead.UsedItems[uiControlDir]:= 1; // ����������� ��������
end;

procedure TAviconDataContainer.SetGaugeSideLeftSide(); // ������������ ������� ������� ������� / �� ������� ����� ������� ������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetGaugeSideLeftSide');
  {$ENDIF}
  FHead.UsedItems[uiGaugeSideLeftSide]:= 1;
end;

procedure TAviconDataContainer.SetGaugeSideRightSide(); // ������������ ������� ������� ������� / �� ������� ����� ������� ������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetGaugeSideRightSide');
  {$ENDIF}
  FHead.UsedItems[uiGaugeSideRightSide]:= 1;
end;

procedure TAviconDataContainer.SetPathSectionName(Name: string); // �������� ������� ������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetPathSectionName');
  {$ENDIF}
  FHead.PathSectionName:= StringToHeaderStr(Name);
  FHead.UsedItems[uiPathSectionName]:= 1; // �������� ������� ������
end;

procedure TAviconDataContainer.SetRailPathNumber(Number: Integer); // ����� �/� ����
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetRailPathNumber');
  {$ENDIF}
  FHead.RailPathNumber:= Number;
  FHead.UsedItems[uiRailPathNumber]:= 1; // ����� �/� ����
end;

procedure TAviconDataContainer.SetRailSection(Number: Integer); // ����� ������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetRailSection');
  {$ENDIF}
  FHead.RailSection:= Number;
  FHead.UsedItems[uiRailSection]:= 1; // ����� ������
end;

procedure TAviconDataContainer.SetDateTime(DateTime: TDateTime); // ����/����� ��������
var
  SystemTime: TSystemTime;

begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetDateTime');
  {$ENDIF}
  DateTimeToSystemTime(DateTime, SystemTime);
  FHead.Year:= SystemTime.wYear;
  FHead.Month:= SystemTime.wMonth;
  FHead.Day:= SystemTime.wDay;
  FHead.Hour:= SystemTime.wHour;
  FHead.Minute:= SystemTime.wMinute;
//  FCurHour:= SystemTime.wHour;
//  FCurMinute:= SystemTime.wMinute;
  FHead.UsedItems[uiDateTime]:= 1; // ���� ����� ��������
end;

procedure TAviconDataContainer.SetOperatorName(Name: string); // ��� ���������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetOperatorName');
  {$ENDIF}
  FHead.OperatorName:= StringToHeaderStr(Name);
  FHead.UsedItems[uiOperatorName]:= 1; // ��� ���������
end;

procedure TAviconDataContainer.SetStartMRFCrd(StartKM, StartPk, StartMetre: Integer);
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetMetricStartCoord');
  {$ENDIF}

  FPathCoordSystem:= csMetricRF;
  FHead.PathCoordSystem:= Ord(csMetricRF);
  FHead.UsedItems[uiPathCoordSystem]:= 1; // ������� ��������� ���������

  FHead.StartKM:= StartKM;
  FHead.StartPk:= StartPk;
  FHead.StartMetre:= StartMetre;
  FHead.UsedItems[uiStartMetric]:= 1; // ��������� ���������� - �����������

  FLastMRFPost.Km:= StartKM;
  FLastMRFPost.Pk:= StartPk;
{
  FExHeader.EndLevel0:= StartKM;
  FExHeader.EndLevel1:= StartPk;
  FExHeader.EndLevel2:= 0;
}
end;

procedure TAviconDataContainer.SetStartCaCrd(CoordSys: TCoordSys; Chainage: TCaCrd);
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetChainageStartCoord');
  {$ENDIF}

  FPathCoordSystem:= CoordSys;
  FHead.PathCoordSystem:= Ord(CoordSys);
  FHead.UsedItems[uiPathCoordSystem]:= 1; // ������� ��������� ���������

  FHead.StartChainage:= Chainage;
  FLastCaPost:= Chainage;
  FHead.UsedItems[uiStartChainage]:= 1; // ��������� ���������� - "������"
//  FExHeader.EndLevel0:= 0; Chainage
//  FExHeader.EndLevel1:= 0; Chainage
//  FExHeader.EndLevel2:= 0;
end;

(*
procedure TAviconDataContainer.SetStartCoord(Start: TRealCoord);
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetStartCoord');
  {$ENDIF}
  if Start.CoordSys = csNormal then
  begin
    FHead.StartKM:= Start.Level0;
    FHead.StartPk:= Start.Level1;
    FHead.StartMetre:= Start.Level2;
    FHead.UsedItems[uiStartMetric]:= 1; // ��������� ���������� - �����������
    FExHeader.EndLevel0:= Start.Level0;
    FExHeader.EndLevel1:= Start.Level1;
    FExHeader.EndLevel2:= 0;
  end
  else
  begin
    FHead.StartChainage:= Start.Level0;
    FHead.UsedItems[uiStartChainage]:= 1; // ��������� ���������� - �����������
    FExHeader.EndLevel0:= Start.Level0;
    FExHeader.EndLevel1:= Start.Level1;
    FExHeader.EndLevel2:= Start.Level2;
  end;
end;
 *)
procedure TAviconDataContainer.SetWorkRailTypeA(Val: Byte); // ������� ���� (��� ������������ ��������): 0 � �����, 1 � ������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetWorkRailTypeA');
  {$ENDIF}
  FHead.WorkRailTypeA:= Val;
  FHead.UsedItems[uiWorkRailTypeA]:= 1; // ��������� ���������� - �����������
end;

procedure TAviconDataContainer.SetWorkRailTypeB(Val: TCardinalPoints); // ������� ���� (��� ������������ ��������): NR, SR, WR, ER
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetWorkRailTypeB');
  {$ENDIF}
  FHead.WorkRailTypeB[0]:= Val[0];
  FHead.WorkRailTypeB[1]:= Val[1];
  FHead.UsedItems[uiWorkRailTypeB]:= 1; // ��������� ���������� - �����������
end;

procedure TAviconDataContainer.SetTrackDirection(Val: TCardinalPoints); // ��� �����������: NB, SB, EB, WB, CT, PT, RA, TT
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetTrackDirection');
  {$ENDIF}
  FHead.TrackDirection[0]:= Val[0];
  FHead.TrackDirection[1]:= Val[1];
  FHead.UsedItems[uiTrackDirection]:= 1; // ��������� ���������� - �����������
end;

procedure TAviconDataContainer.SetTrackID(Val: TCardinalPoints); // Track ID: NR, SR, ER, WR
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetTrackID');
  {$ENDIF}
  FHead.TrackID[0]:= Val[0];
  FHead.TrackID[1]:= Val[1];
  FHead.UsedItems[uiTrackId]:= 1; // ��������� ���������� - �����������
end;

procedure TAviconDataContainer.SetCorrespondenceSides(Val: TCorrespondenceSides); // ������������ ������ ������� ����� ����
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetCorrespondenceSides');
  {$ENDIF}
  FHead.CorrespondenceSides:= Val;
  FHead.UsedItems[uiCorrSides]:= 1;
end;

procedure TAviconDataContainer.SetUseGPSTrack; // ������� GPS ����
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetUseGPSTrack');
  {$ENDIF}
  FHead.UsedItems[uiGPSTrackinDegrees]:= 1;
end;

procedure TAviconDataContainer.SetTemperature; // �������������� ������ ����������� ���������� �����
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetUseTemperature');
  {$ENDIF}
  FHead.UsedItems[uiTemperature]:= 1;
end;

procedure TAviconDataContainer.SetSpeed; // �������������� ������ �������� ��������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetUseSpeed');
  {$ENDIF}
  FHead.UsedItems[uiSpeed]:= 1;
end;


(*
function TAviconDataContainer.AddEcho(Rail: TRail; ScanChNum, Count, D1, A1, D2, A2, D3, A3, D4, A4, D5, A5, D6, A6, D7, A7, D8, A8: Byte): Boolean;
var
//  Cnt: Byte;
  Cnt: array [1..3] of Byte;

begin
  FCS.Enter;

  {$IFDEF LOG}
  Writeln(FLog, Format('AddEcho: Pos: %d R:%d S:%d C:%d', [FData.Position[SaveStrIdx], Ord(Rail), ScanChNum, Count]));
  {$ENDIF}

  if not FScanChNumExists[ScanChNum] then
  begin
    Result:= False;
    FCS.Leave;
    Exit;
  end;

  // ���������� �� ������ �� 3 �������
  case Count of
    0: begin Cnt[1]:= 0; Cnt[2]:= 0; Cnt[3]:= 0; end;
    1: begin Cnt[1]:= 1; Cnt[2]:= 0; Cnt[3]:= 0; end;
    2: begin Cnt[1]:= 2; Cnt[2]:= 0; Cnt[3]:= 0; end;
    3: begin Cnt[1]:= 3; Cnt[2]:= 0; Cnt[3]:= 0; end;
    4: begin Cnt[1]:= 3; Cnt[2]:= 1; Cnt[3]:= 0; end;
    5: begin Cnt[1]:= 3; Cnt[2]:= 2; Cnt[3]:= 0; end;
    6: begin Cnt[1]:= 3; Cnt[2]:= 3; Cnt[3]:= 0; end;
    7: begin Cnt[1]:= 3; Cnt[2]:= 3; Cnt[3]:= 1; end;
    8: begin Cnt[1]:= 3; Cnt[2]:= 3; Cnt[3]:= 2; end;
  end;

  FID:= (Byte(Rail) and 1) shl 6 + (ScanChNum and $F) shl 2 + (Cnt[1] and $3);
  FData.WriteBuffer(SaveStrIdx, FID, 1);

  case Cnt[1] of
    0:  ;
    1:  begin
          FData.WriteBuffer(SaveStrIdx, D1, 1);
          FID:= A1 and $0F shl 4;
          FData.WriteBuffer(SaveStrIdx, FID, 1);
        end;
    2:  begin
          FData.WriteBuffer(SaveStrIdx, D1, 1);
          FData.WriteBuffer(SaveStrIdx, D2, 1);
          FID:= A1 and $0F shl 4 + A2 and $0F;
          FData.WriteBuffer(SaveStrIdx, FID, 1);
        end;
    3:  begin
          FData.WriteBuffer(SaveStrIdx, D1, 1);
          FData.WriteBuffer(SaveStrIdx, D2, 1);
          FData.WriteBuffer(SaveStrIdx, D3, 1);
          FID:= A1 and $0F shl 4 + A2 and $0F;
          FData.WriteBuffer(SaveStrIdx, FID, 1);
          FID:= A3 and $0F shl 4;
          FData.WriteBuffer(SaveStrIdx, FID, 1);
        end;
  end;

  if Cnt[2] <> 0 then
  begin
    FID:= (Byte(Rail) and 1) shl 6 + (ScanChNum and $F) shl 2 + (Cnt[2] and $3);
    FData.WriteBuffer(SaveStrIdx, FID, 1);

    case Cnt[2] of
      1:  begin
            FData.WriteBuffer(SaveStrIdx, D4, 1);
            FID:= A4 and $0F shl 4;
            FData.WriteBuffer(SaveStrIdx, FID, 1);
          end;
      2:  begin
            FData.WriteBuffer(SaveStrIdx, D4, 1);
            FData.WriteBuffer(SaveStrIdx, D5, 1);
            FID:= A4 and $0F shl 4 + A5 and $0F;
            FData.WriteBuffer(SaveStrIdx, FID, 1);
          end;
      3:  begin
            FData.WriteBuffer(SaveStrIdx, D4, 1);
            FData.WriteBuffer(SaveStrIdx, D5, 1);
            FData.WriteBuffer(SaveStrIdx, D6, 1);
            FID:= A4 and $0F shl 4 + A5 and $0F;
            FData.WriteBuffer(SaveStrIdx, FID, 1);
            FID:= A6 and $0F shl 4;
            FData.WriteBuffer(SaveStrIdx, FID, 1);
          end;
    end;
  end;

  if Cnt[3] <> 0 then
  begin
    FID:= (Byte(Rail) and 1) shl 6 + (ScanChNum and $F) shl 2 + (Cnt[3] and $3);
    FData.WriteBuffer(SaveStrIdx, FID, 1);

    case Cnt[3] of
      1:  begin
            FData.WriteBuffer(SaveStrIdx, D7, 1);
            FID:= A7 and $0F shl 4;
            FData.WriteBuffer(SaveStrIdx, FID, 1);
          end;
      2:  begin
            FData.WriteBuffer(SaveStrIdx, D7, 1);
            FData.WriteBuffer(SaveStrIdx, D8, 1);
            FID:= A7 and $0F shl 4 + A8 and $0F;
            FData.WriteBuffer(SaveStrIdx, FID, 1);
          end;
    end;
  end;
  Result:= True;

  FCS.Leave;
end;

function TAviconDataContainer.AddEcho(Rail: TRail; ScanChNum, Count: Byte; EchoBlock: TEchoBlock): Boolean;
begin
  Result:= AddEcho(Rail, ScanChNum, Count, EchoBlock[0].T, EchoBlock[0].A,
                                           EchoBlock[1].T, EchoBlock[1].A,
                                           EchoBlock[2].T, EchoBlock[2].A,
                                           EchoBlock[3].T, EchoBlock[3].A,
                                           EchoBlock[4].T, EchoBlock[4].A,
                                           EchoBlock[5].T, EchoBlock[5].A,
                                           EchoBlock[6].T, EchoBlock[6].A,
                                           EchoBlock[7].T, EchoBlock[7].A);
end;

procedure TAviconDataContainer.AddSensor1State(Rail: TRail; State: Boolean);
begin
  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_Sensor1;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Rail, 1);
  FData.WriteBuffer(SaveStrIdx, State, 1);
  AddEvent(EID_Sensor1);
  FCS.Leave;
end;
*)
{
procedure TAviconDataContainer.ClearEchoList(EchoList: TEchoList);
var
  Rail: TRail;
  ScanChNum: Integer;

begin
  for Rail:= FConfig.MinRail to FConfig.MaxRail do
    for ScanChNum:= FConfig.MinScanChNumber to FConfig.MaxScanChNumber do
      FCurrEchoList[Rail, ScanChNum].Count:= 0;
end;
}

function TAviconDataContainer.AddEcho(Rail: TRail; ScanChNum, Count, D0, A0, D1, A1, D2, A2, D3, A3, D4, A4, D5, A5, D6, A6, D7, A7: Byte): Boolean;
var
  Cnt: array [1..2] of Byte;

begin
  if not FScanChNumExists[ScanChNum] then
  begin
    Result:= False;
    Exit;
  end;

  {$IFDEF LOG}
  Writeln(FLog, Format('AddEcho: Pos: %d R:%d S:%d C:%d', [FData.Position[SaveStrIdx], Ord(Rail), ScanChNum, Count]));
  {$ENDIF}

  FCS.Enter;

  if Count <> 0 then
  begin
    // ���������� �� ������ �� 4 �������
    case Count of
      1: begin Cnt[1]:= 1; Cnt[2]:= 0; end;
      2: begin Cnt[1]:= 2; Cnt[2]:= 0; end;
      3: begin Cnt[1]:= 3; Cnt[2]:= 0; end;
      4: begin Cnt[1]:= 4; Cnt[2]:= 0; end;
      5: begin Cnt[1]:= 4; Cnt[2]:= 1; end;
      6: begin Cnt[1]:= 4; Cnt[2]:= 2; end;
      7: begin Cnt[1]:= 4; Cnt[2]:= 3; end;
      8: begin Cnt[1]:= 4; Cnt[2]:= 4; end;
    end;

    if FExtendedChannels and (ScanChNum > 14) then
    begin
      FID:= (Byte(Rail) and 1) shl 6 + ($F shl 2) + ((Cnt[1] - 1) and $3);
      FData.WriteBuffer(SaveStrIdx, FID, 1);
      FID:= ScanChNum - 15;
      FData.WriteBuffer(SaveStrIdx, FID, 1);
    end
    else
    begin
      FID:= (Byte(Rail) and 1) shl 6 + (ScanChNum and $F) shl 2 + ((Cnt[1] - 1) and $3);
      FData.WriteBuffer(SaveStrIdx, FID, 1);
    end;

    case Cnt[1] of
      1:  begin
            FData.WriteBuffer(SaveStrIdx, D0, 1);
            FID:= A0 and $0F shl 4;
            FData.WriteBuffer(SaveStrIdx, FID, 1);
          end;
      2:  begin
            FData.WriteBuffer(SaveStrIdx, D0, 1);
            FData.WriteBuffer(SaveStrIdx, D1, 1);
            FID:= A0 and $0F shl 4 + A1 and $0F;
            FData.WriteBuffer(SaveStrIdx, FID, 1);
          end;
      3:  begin
            FData.WriteBuffer(SaveStrIdx, D0, 1);
            FData.WriteBuffer(SaveStrIdx, D1, 1);
            FData.WriteBuffer(SaveStrIdx, D2, 1);
            FID:= A0 and $0F shl 4 + A1 and $0F;
            FData.WriteBuffer(SaveStrIdx, FID, 1);
            FID:= A2 and $0F shl 4;
            FData.WriteBuffer(SaveStrIdx, FID, 1);
          end;
      4:  begin
            FData.WriteBuffer(SaveStrIdx, D0, 1);
            FData.WriteBuffer(SaveStrIdx, D1, 1);
            FData.WriteBuffer(SaveStrIdx, D2, 1);
            FData.WriteBuffer(SaveStrIdx, D3, 1);
            FID:= A0 and $0F shl 4 + A1 and $0F;
            FData.WriteBuffer(SaveStrIdx, FID, 1);
            FID:= A2 and $0F shl 4 + A3 and $0F;
            FData.WriteBuffer(SaveStrIdx, FID, 1);
          end;
    end;

    if Cnt[2] <> 0 then
    begin
      if FExtendedChannels and (ScanChNum > 14) then
      begin
        FID:= (Byte(Rail) and 1) shl 6 + ($F shl 2) + ((Cnt[2] - 1) and $3);
        FData.WriteBuffer(SaveStrIdx, FID, 1);
        FID:= ScanChNum - 15;
        FData.WriteBuffer(SaveStrIdx, FID, 1);
      end
      else
      begin
        FID:= (Byte(Rail) and 1) shl 6 + (ScanChNum and $F) shl 2 + ((Cnt[2] - 1) and $3);
        FData.WriteBuffer(SaveStrIdx, FID, 1);
      end;


      case Cnt[2] of
        1:  begin
              FData.WriteBuffer(SaveStrIdx, D4, 1);
              FID:= A4 and $0F shl 4;
              FData.WriteBuffer(SaveStrIdx, FID, 1);
            end;
        2:  begin
              FData.WriteBuffer(SaveStrIdx, D4, 1);
              FData.WriteBuffer(SaveStrIdx, D5, 1);
              FID:= A4 and $0F shl 4 + A5 and $0F;
              FData.WriteBuffer(SaveStrIdx, FID, 1);
            end;
        3:  begin
              FData.WriteBuffer(SaveStrIdx, D4, 1);
              FData.WriteBuffer(SaveStrIdx, D5, 1);
              FData.WriteBuffer(SaveStrIdx, D6, 1);
              FID:= A4 and $0F shl 4 + A5 and $0F;
              FData.WriteBuffer(SaveStrIdx, FID, 1);
              FID:= A6 and $0F shl 4;
              FData.WriteBuffer(SaveStrIdx, FID, 1);
            end;
        4:  begin
              FData.WriteBuffer(SaveStrIdx, D4, 1);
              FData.WriteBuffer(SaveStrIdx, D5, 1);
              FData.WriteBuffer(SaveStrIdx, D6, 1);
              FData.WriteBuffer(SaveStrIdx, D7, 1);
              FID:= A4 and $0F shl 4 + A5 and $0F;
              FData.WriteBuffer(SaveStrIdx, FID, 1);
              FID:= A6 and $0F shl 4 + A7 and $0F;
              FData.WriteBuffer(SaveStrIdx, FID, 1);
            end;
      end;
    end;
  end;
  FCS.Leave;
end;

function TAviconDataContainer.AddEcho(Rail: TRail; ScanChNum, Count: Byte; EchoBlock: TEchoBlock): Boolean;
begin
  Result:= AddEcho(Rail, ScanChNum, Count, EchoBlock[0].T, EchoBlock[0].A,
                                           EchoBlock[1].T, EchoBlock[1].A,
                                           EchoBlock[2].T, EchoBlock[2].A,
                                           EchoBlock[3].T, EchoBlock[3].A,
                                           EchoBlock[4].T, EchoBlock[4].A,
                                           EchoBlock[5].T, EchoBlock[5].A,
                                           EchoBlock[6].T, EchoBlock[6].A,
                                           EchoBlock[7].T, EchoBlock[7].A);
end;

procedure TAviconDataContainer.AddSensor1State(Rail: TRail; State: Boolean);
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddSensor1State');
  {$ENDIF}
  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_Sensor1;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Rail, 1);
  FData.WriteBuffer(SaveStrIdx, State, 1);
  AddEvent(EID_Sensor1);
  FCS.Leave;
end;

procedure TAviconDataContainer.SetZerroProbMode(Mode: TZerroProbMode); // ����� ������ �������� 0 ����
begin
  {$IFDEF LOG}
  Writeln(FLog, 'SetZerroProbMode');
  {$ENDIF}
  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_ZerroProbMode;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Byte(Mode), 1);
  AddEvent(EID_ZerroProbMode);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddLongLabel(Rail: TRail; EvalCh, ScanCh: Byte; Type_: Byte; StartDisCoord, EndDisCoord, Data1, Data2, Data3: Integer); // ����������� �������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddLongLabel');
  {$ENDIF}
  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_LongLabel;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Byte(Rail), 1);
  FData.WriteBuffer(SaveStrIdx, EvalCh, 1);
  FData.WriteBuffer(SaveStrIdx, ScanCh, 1);
  FData.WriteBuffer(SaveStrIdx, Type_, 1);
  FData.WriteBuffer(SaveStrIdx, Data1, 4);
  FData.WriteBuffer(SaveStrIdx, Data2, 4);
  FData.WriteBuffer(SaveStrIdx, Data3, 4);
  FData.WriteBuffer(SaveStrIdx, StartDisCoord, 4);
  FData.WriteBuffer(SaveStrIdx, EndDisCoord, 4);
  AddEvent(EID_LongLabel);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddKu(Rail: TRail; EvalCh: Integer; NewValue: ShortInt); // ��������� �������� ����������������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddKu');
  {$ENDIF}
  FCS.Enter;
  FCurSaveParam[Rail, EvalCh].Ku:= NewValue;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_Ku;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Rail, 1);
  FData.WriteBuffer(SaveStrIdx, EvalCh, 1);
  FData.WriteBuffer(SaveStrIdx, NewValue, 1);
  AddEvent(EID_Ku);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddAtt(Rail: TRail; EvalCh: Integer; NewValue: Byte); // ��������� ��������� ���� ����������� (0 �� �������� ����������������)
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddAtt');
  {$ENDIF}
  FCS.Enter;
  FCurSaveParam[Rail, EvalCh].Att:= NewValue;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_Att;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Rail, 1);
  FData.WriteBuffer(SaveStrIdx, EvalCh, 1);
  FData.WriteBuffer(SaveStrIdx, NewValue, 1);
  AddEvent(EID_Att);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddTVG(Rail: TRail; EvalCh: Integer; NewValue: Byte); // ��������� ���
var
  SecondEvalCh: Integer;

begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddTVG');
  {$ENDIF}
  FCS.Enter;
  FCurSaveParam[Rail, EvalCh].TVG:= NewValue;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_TVG;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Rail, 1);
  FData.WriteBuffer(SaveStrIdx, EvalCh, 1);
  FData.WriteBuffer(SaveStrIdx, NewValue, 1);
  AddEvent(EID_TVG);

  if (FConfig.GetSecondEvalByEval(EvalCh, SecondEvalCh)) then
  begin
    FCurSaveParam[Rail, SecondEvalCh].TVG:= NewValue;
    FLastOffset:= FData.Position[SaveStrIdx];
    FID:= EID_TVG;
    FData.WriteBuffer(SaveStrIdx, FID, 1);
    FData.WriteBuffer(SaveStrIdx, Rail, 1);
    FData.WriteBuffer(SaveStrIdx, SecondEvalCh, 1);
    FData.WriteBuffer(SaveStrIdx, NewValue, 1);
    AddEvent(EID_TVG);
  end;
  FCS.Leave;
end;

procedure TAviconDataContainer.AddStStr(Rail: TRail; EvalCh: Integer; NewValue: Byte); // ��������� ��������� ������ ������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddStStr');
  {$ENDIF}
  FCS.Enter;
  FCurSaveParam[Rail, EvalCh].StStr:= NewValue;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_StStr;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Rail, 1);
  FData.WriteBuffer(SaveStrIdx, EvalCh, 1);
  FData.WriteBuffer(SaveStrIdx, NewValue, 1);
  AddEvent(EID_StStr);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddEndStr(Rail: TRail; EvalCh: Integer; NewValue: Byte); // ��������� ��������� ����� ������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddEndStr');
  {$ENDIF}
  FCS.Enter;
  FCurSaveParam[Rail, EvalCh].EndStr:= NewValue;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_EndStr;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Rail, 1);
  FData.WriteBuffer(SaveStrIdx, EvalCh, 1);
  FData.WriteBuffer(SaveStrIdx, NewValue, 1);
  AddEvent(EID_EndStr);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddPrismDelay(Rail: TRail; EvalCh: Integer; NewValue: Word); // ��������� 2�� (Word)
var
  SecondEvalCh: Integer;

begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddPrismDelay');
  {$ENDIF}
  FCS.Enter;
  FCurSaveParam[Rail, EvalCh].PrismDelay:= NewValue;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_PrismDelay;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Rail, 1);
  FData.WriteBuffer(SaveStrIdx, EvalCh, 1);
  FData.WriteBuffer(SaveStrIdx, NewValue, 2);
  AddEvent(EID_PrismDelay);

  if (FConfig.GetSecondEvalByEval(EvalCh, SecondEvalCh)) then
  begin
    FCurSaveParam[Rail, SecondEvalCh].PrismDelay:= NewValue;
    FLastOffset:= FData.Position[SaveStrIdx];
    FID:= EID_PrismDelay;
    FData.WriteBuffer(SaveStrIdx, FID, 1);
    FData.WriteBuffer(SaveStrIdx, Rail, 1);
    FData.WriteBuffer(SaveStrIdx, SecondEvalCh, 1);
    FData.WriteBuffer(SaveStrIdx, NewValue, 2);
    AddEvent(EID_PrismDelay);
  end;
  FCS.Leave;
end;

procedure TAviconDataContainer.AddHeadPh(Rail: TRail; EvalCh: Byte; Enabled: Boolean);
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddHeadPh');
  {$ENDIF}
  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_HeadPh;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Rail, 1);
  FData.WriteBuffer(SaveStrIdx, EvalCh, 1);
  FData.WriteBuffer(SaveStrIdx, Enabled, 1);
  AddEvent(EID_HeadPh);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddMode(ModeIdx, InPreviousModeTime: Word; AddChInfo: Boolean; Rail: TRail; EvalCh: Byte);
begin
  {$IFDEF LOG}
  Writeln(FLog, 'ModeIdx');
  {$ENDIF}
  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_Mode;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, ModeIdx, 2);
  FData.WriteBuffer(SaveStrIdx, AddChInfo, 1);
  FData.WriteBuffer(SaveStrIdx, Rail, 1);
  FData.WriteBuffer(SaveStrIdx, EvalCh, 1);
  FData.WriteBuffer(SaveStrIdx, Time, 2);
  AddEvent(EID_Mode);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddSetRailType;
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddSetRailType');
  {$ENDIF}
  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_SetRailType;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  AddEvent(EID_SetRailType);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddMRFPost(Km0, Km1, Pk0, Pk1: Integer);
var
  Stolb: edCoordPost;

begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddCoordMetricPost');
  {$ENDIF}
  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];

  FID:= EID_Stolb;
  FData.WriteBuffer(SaveStrIdx, FID, 1);

  Stolb.Km[0]:= Km0;
  Stolb.Km[1]:= Km1;
  Stolb.Pk[0]:= Pk0;
  Stolb.Pk[1]:= Pk1;
  FillChar(Stolb.Reserv[0], 32 * 4, 0);
  FData.WriteBuffer(SaveStrIdx, Stolb, 144);

  FLastPostSysCrd:= FCurSaveSysCrd;
  FLastPostDisCrd:= FCurSaveDisCrd;
  FLastMRFPost.Km:= Km1;
  FLastMRFPost.Pk:= Pk1;
  FLastMRFPost.mm:= 0;
//  FExHeader.EndLevel2:= (FCurSaveSysCrd - FLastStolbSysCrd) * FHead.ScanStep div 100;

  AddEvent(EID_Stolb);
//  Inc(FStolbCount);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddCaPost(Chainage: TCaCrd);
var
  Stolb: edCoordPostChainage;

begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddCoordChainagePost');
  {$ENDIF}
  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];

  FID:= EID_StolbChainage;
  FData.WriteBuffer(SaveStrIdx, FID, 1);

  Stolb.Val:= Chainage;
  FillChar(Stolb.Reserv[0], 32 * 4, 0);
  FData.WriteBuffer(SaveStrIdx, Stolb, 136);

//  FExHeader.EndLevel0:= Val;
//  FExHeader.EndLevel1:= 0;
//  FExHeader.EndLevel2:= (FCurSaveSysCrd - FLastStolbSysCrd) * FHead.ScanStep div 100;

  FLastPostSysCrd:= FCurSaveSysCrd;
  FLastPostDisCrd:= FCurSaveDisCrd;
  FLastCaPost:= Chainage;

  AddEvent(EID_StolbChainage);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddAirBrushTempOff(isOff: Boolean {; DisCoord: Integer});
var
  I: Integer;

begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddAirBrushTempOff');
  {$ENDIF}

  FCS.Enter;


  FLastOffset:= FData.Position[SaveStrIdx];

  FID:= EID_AirBrushTempOff;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, isOff, 1);
  I:= 0;
  FData.WriteBuffer(SaveStrIdx, I, 4);

  AddEvent(EID_AirBrushTempOff);

  FCS.Leave;

end;

procedure TAviconDataContainer.AddAlarmTempOff(isOff: Boolean {; DisCoord: Integer});
var
  I: Integer;

begin
  {$IFDEF LOG}
  Writeln(FLog, 'EID_AlarmTempOff');
  {$ENDIF}

  FCS.Enter;

  I:= 0;

  FLastOffset:= FData.Position[SaveStrIdx];

  FID:= EID_AlarmTempOff;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, isOff, 1);
  FData.WriteBuffer(SaveStrIdx, I {DisCoord}, 4);

  AddEvent(EID_AlarmTempOff);

  FCS.Leave;
end;

procedure TAviconDataContainer.AddSpeed(Speed:	Single; SpeedState: TSpeedState); // �������� � ���������� �������� ��������
var
  Speed_: Integer;

begin
  {$IFDEF LOG}
  Writeln(FLog, 'EID_SpeedState');
  {$ENDIF}

  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_SpeedState;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  Speed_:= Round(Speed * 10);
  FData.WriteBuffer(SaveStrIdx, Speed_, 4);
  if SpeedState = ssNormalSpeed then FID:= 0  // ���������� �������� ��������
                                else FID:= 1; // ���������� ���������� �������� ��������
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  AddEvent(EID_SpeedState);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddGPSCoord(Lat, Lon, Speed:	Single); // �������������� ����������: Lat - ������; Lon - �������
begin
  {$IFDEF LOG}
  Writeln(FLog, 'EID_GPSCoord2');
  {$ENDIF}

  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_GPSCoord2;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Lat, 4);
  FData.WriteBuffer(SaveStrIdx, Lon, 4);
  FData.WriteBuffer(SaveStrIdx, Speed, 4);
  AddEvent(EID_GPSCoord);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddGPSState(State: Byte; UseSatCount: Byte; AntennaConnected: Boolean; Reserv: GPSStateReserv); // ��������� ��������� GPS
begin
  {$IFDEF LOG}
  Writeln(FLog, 'EID_GPSState');
  {$ENDIF}

  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_GPSState;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, State, 1);
  FData.WriteBuffer(SaveStrIdx, UseSatCount, 1);
  FData.WriteBuffer(SaveStrIdx, AntennaConnected, 1);
  FData.WriteBuffer(SaveStrIdx, Reserv[0], 6);

  AddEvent(EID_GPSState);

  FCS.Leave;
end;

procedure TAviconDataContainer.AddNORDCODate(DisCoord: Integer; NewLine: TNordcoCSVRecordLine);
var
  tmp: edNORDCO_Rec;

begin
  {$IFDEF LOG}
  Writeln(FLog, 'EID_GPSState');
  {$ENDIF}

  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_NORDCO_Rec;
  FData.WriteBuffer(SaveStrIdx, FID, 1);

  tmp.DisCoord            :=   DisCoord;
  tmp.Line                :=   StringToHeaderStr(NewLine.Line);
  tmp.Track               :=   StringToHeaderStr(NewLine.Track);
  tmp.LocationFrom        :=   NewLine.LocationFrom                ;
  tmp.LocationTo          :=   NewLine.LocationTo                  ;
  tmp.LatitudeFrom        :=   NewLine.LatitudeFrom                ;
  tmp.LatitudeTo          :=   NewLine.LatitudeTo                  ;
  tmp.LongitudeFrom       :=   NewLine.LongitudeFrom               ;
  tmp.LongitudeTo         :=   NewLine.LongitudeTo                 ;
  tmp.Type_               :=   StringToHeaderStr(NewLine.Type_);
  tmp.SurveyDate          :=   NewLine.SurveyDate                  ;
  tmp.Source              :=   StringToHeaderStr(NewLine.Source);
  tmp.Status              :=   StringToHeaderStr(NewLine.Status);
  tmp.IsFailure           :=   NewLine.IsFailure                   ;
  tmp.AssetType           :=   StringToHeaderStr(NewLine.AssetType);
  tmp.DataEntryDate       :=   NewLine.DataEntryDate               ;
  tmp.Size                :=   NewLine.Size                        ;
  tmp.Size2               :=   NewLine.Size2                       ;
  tmp.Size3               :=   NewLine.Size3                       ;
  tmp.UnitofMeasurement   :=   StringToHeaderStr(NewLine.UnitofMeasurement);
  tmp.UnitofMeasurement2  :=   StringToHeaderStr(NewLine.UnitofMeasurement2);
  tmp.UnitofMeasurement3  :=   StringToHeaderStr(NewLine.UnitofMeasurement3);
  tmp.Comment             :=   StringToHeaderBigStr(NewLine.Comment);
  tmp.Operator_           :=   StringToHeaderStr(NewLine.Operator_);
  tmp.Device              :=   StringToHeaderStr(NewLine.Device);
  FData.WriteBuffer(SaveStrIdx, tmp, SizeOf(edNORDCO_Rec));

  AddEvent(EID_NORDCO_Rec);
  FCS.Leave;
end;

// ��������:
// DC - ��������� �� ���� ������������� ������� ���� �����
// StDisCoord, EnDisCoord - ���������� ������ � ����� ������� (������������� ����������� � ��������� ����������)
// Lat, Lon - ���������� ������� - ������ � ������ (������ ������ �� �����������)
// CodeDefect - ��� �������, ������
// SourceType - ����� ����������� ������� - �� ������: Ultrasonic Trolley, Visual Inspection
// IsFailure - TRUE - ��� ������, ������� �������� ����������� ��������
// Size � UseSize - ������ (��) ������� ��� ����������� �������. ��������, ���� ������ ��� �������, �� �� �����.
// Size2 � UseSize2 - ������ (��) ������� ���  ����������� ������� (���� ����� ��� ����������)
// Size3 � UseSize3 - ������ (��) ������� ���  ����������� ������� (���� ����� ��� ����������)
// CommentText - ����������

procedure TAviconDataContainer.AddNORDCODate(StDisCoord, EdDisCoord: Integer;
                                             Lat, Lon: Single;
                                             CodeDefect: string;
                                             SourceType: TSourceType;
                                             Status: TNStatus;
                                             IsFailure: Boolean;
                                             Size, Size2, Size3: Single; UseSize, UseSize2, UseSize3: Boolean;
                                             CommentText: string);

var
  I: Integer;
  tmp: edNORDCO_Rec;
  str: string;

begin
  {$IFDEF LOG}
  Writeln(FLog, 'EID_GPSState');
  {$ENDIF}

  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_NORDCO_Rec;
  FData.WriteBuffer(SaveStrIdx, FID, 1);

  tmp.DisCoord            :=   StDisCoord;
  tmp.Line                :=   Header.PathSectionName;  // �������� ��� ������������� �������������� �����
  tmp.Track               :=   StringToHeaderStr(IntToStr(Header.RailPathNumber));  // ������������� ����, � ������� ��������� ������
  tmp.LocationFrom        :=   CaCrdToVal(csMetric1kmTurkish, DisToCaCrd(StDisCoord)); // ������ ������������� �������
  tmp.LocationTo          :=   CaCrdToVal(csMetric1kmTurkish, DisToCaCrd(EdDisCoord)); // ����� ������������� �������. � ������ ��������� ������� �� �� �����, ��� ����  "Location From"
  tmp.LatitudeFrom        :=   Lat;  // GPS-������ ��� ���� "Location From"
  tmp.LatitudeTo          :=   Lat;  // GPS-������ ��� ���� "Location To"
  tmp.LongitudeFrom       :=   Lon;  // GPS-������� ��� ���� "Location From"
  tmp.LongitudeTo         :=   Lon;  // GPS-������� ��� ���� "Location To"
  tmp.Type_               :=   StringToHeaderStr(CodeDefect);
  tmp.SurveyDate          :=   Trunc(Now);  // ���� ����������� ������ ��������
  if SourceType = stUltrasonicTrolley // ����� ����������� ������� - �� ������: Ultrasonic Trolley, Visual Inspection
      then tmp.Source:= StringToHeaderStr('Ultrasonic Trolley')
      else tmp.Source:= StringToHeaderStr('Visual Inspection');
  if Status = nsOpen then tmp.Status:= StringToHeaderStr('Open')
                     else tmp.Status:= StringToHeaderStr('Close');
  tmp.IsFailure           :=   IsFailure;
//  if AssetType = rLeft then tmp.AssetType:= StringToHeaderStr('Left Rail')
//                           else tmp.AssetType:= StringToHeaderStr('Right Rail'); // ��� �������� �������� ���� - �� ������: Left Rail, Right Rail

  if Header.WorkRailTypeA = 0 then tmp.AssetType:= StringToHeaderStr('Left Rail')
                              else tmp.AssetType:= StringToHeaderStr('Right Rail'); // ��� �������� �������� ���� - �� ������: Left Rail, Right Rail
  tmp.DataEntryDate       :=   Trunc(Now);  // ���� ����������� ������ ��������
  tmp.Size                :=   Size  ;
  tmp.Size2               :=   Size2 ;
  tmp.Size3               :=   Size3 ;

  if UseSize then
  begin
    tmp.Size:= Size;              // ������ ������� ���  ����������� �������. ��������, ���� ������ ��� �������, �� �� �����.
    tmp.UnitofMeasurement:= StringToHeaderStr('mm'); //��. ��������� ����
  end;
  if UseSize2 then
  begin
    tmp.Size2:= Size2;             // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    tmp.UnitofMeasurement2:= StringToHeaderStr('mm'); // ��. ��������� ����
  end;
  if UseSize3 then
  begin
    tmp.Size3:= Size3;             // ������ ������� ���  ����������� ������� (���� ����� ��� ����������)
    tmp.UnitofMeasurement3:= StringToHeaderStr('mm'); // ��. ��������� ����
  end;
  tmp.Comment             :=   StringToHeaderBigStr(CommentText);
  tmp.Operator_           :=   Header.OperatorName;
  str:= 'USK-004R';
  for I := 0 to Header.UnitsCount - 1 do
    if Header.UnitsInfo[I].UnitType = dutUpUnit then
    begin
      str:= str + ' s/n ' + HeaderStrToString(Header.UnitsInfo[I].WorksNumber);
      Break;
    end;
  tmp.Device              :=   StringToHeaderStr(str);

  FData.WriteBuffer(SaveStrIdx, tmp, SizeOf(edNORDCO_Rec));

  AddEvent(EID_NORDCO_Rec);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddPaintSystemParams(PaintSystemParams: TPaintSystemParams);
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddPaintSystemParams');
  {$ENDIF}
  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_PaintSystemParams;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, &PaintSystemParams.Version, 2048);
  AddEvent(EID_PaintSystemParams);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddDebugData(Data: TDebugDataArr);
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddDebugData');
  {$ENDIF}
  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_DebugData;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, &Data[0], 128);
  AddEvent(EID_DebugData);
  FCS.Leave;
end;
(*
procedure TAviconDataContainer.AddUMUPaintJob(DisCoord: Integer);
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddUMUPaintJob');
  {$ENDIF}
  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];
  FID:= EID_UMUPaintJob;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, &DisCoord, 4);
  AddEvent(EID_UMUPaintJob);
  FCS.Leave;
end;
*)
procedure TAviconDataContainer.AddAirBrushMark(Rail: TRail; DisCoord: Integer; Items: TABItemsList);
var
  I: Integer;
  J: Integer;
  B: Byte;
  Empty: edAirBrushItem;
  BreakFlag: Boolean;

begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddAirBrushMark');
  {$ENDIF}

  FCS.Enter;

  I:= 0;
  BreakFlag:= False;

  repeat
    FLastOffset:= FData.Position[SaveStrIdx];

    FID:= EID_AirBrush;
    FData.WriteBuffer(SaveStrIdx, FID, 1);
    FData.WriteBuffer(SaveStrIdx, Rail, 1);
    FData.WriteBuffer(SaveStrIdx, DisCoord, 4);
    B:= Min(16, Length(Items) - I);
    FData.WriteBuffer(SaveStrIdx, B, 1);

    FillChar(Empty, SizeOf(edAirBrushItem), 0);
    for J := 0 to 15 do
    begin
      if I <= High(Items) then FData.WriteBuffer(SaveStrIdx, Items[I], SizeOf(edAirBrushItem))
                          else FData.WriteBuffer(SaveStrIdx, Empty, SizeOf(edAirBrushItem));
      if not (I <= High(Items)) then BreakFlag:= True;
      Inc(I);
    end;

    AddEvent(EID_AirBrush);
  until BreakFlag;

  FCS.Leave;

{
  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];

  FID:= EID_AirBrush;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Rail, 1);
  FData.WriteBuffer(SaveStrIdx, DisCoord, 4);
  B:= Min(16, Length(Items));
  FData.WriteBuffer(SaveStrIdx, B, 1);

  FillChar(Empty, SizeOf(edAirBrushItem), 0);
  for I := 0 to 15 do
    if I <= High(Items) then FData.WriteBuffer(SaveStrIdx, Items[I], SizeOf(edAirBrushItem))
                        else FData.WriteBuffer(SaveStrIdx, Empty, SizeOf(edAirBrushItem));

  AddEvent(EID_AirBrush);
  FCS.Leave;
}
end;


procedure TAviconDataContainer.AddAirBrushMark2(Rail: TRail; DisCoord: Integer; Items: TABItemsList2);
var
  I: Integer;
  J: Integer;
  B: Byte;
  Empty: edAirBrushItem2;
  BreakFlag: Boolean;

begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddAirBrushMark2');
  {$ENDIF}

  FCS.Enter;

  I:= 0;
  BreakFlag:= False;

  repeat
    FLastOffset:= FData.Position[SaveStrIdx];

    FID:= EID_AirBrush2;
    FData.WriteBuffer(SaveStrIdx, FID, 1);
    FData.WriteBuffer(SaveStrIdx, Rail, 1);
    FData.WriteBuffer(SaveStrIdx, DisCoord, 4);
    B:= Min(16, Length(Items) - I);
    FData.WriteBuffer(SaveStrIdx, B, 1);

    FillChar(Empty, SizeOf(edAirBrushItem), 0);
    for J := 0 to 15 do
    begin
      if I <= High(Items) then FData.WriteBuffer(SaveStrIdx, Items[I], SizeOf(edAirBrushItem2))
                          else FData.WriteBuffer(SaveStrIdx, Empty, SizeOf(edAirBrushItem2));
      if not (I <= High(Items)) then BreakFlag:= True;
      Inc(I);
    end;

    AddEvent(EID_AirBrush2);
  until BreakFlag;

  FCS.Leave;

end;

procedure TAviconDataContainer.AddAirBrushJob(Rail: TRail; DisCoord: Integer; ErrCod: Byte; Delta: Word);
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddAirBrushMarkJob');
  {$ENDIF}

  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];

  FID:= EID_PaintMarkRes;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Rail, 1);
  FData.WriteBuffer(SaveStrIdx, DisCoord, 4);
  FData.WriteBuffer(SaveStrIdx, ErrCod, 1);
  FData.WriteBuffer(SaveStrIdx, Delta, 2);

  AddEvent(EID_PaintMarkRes);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddStrelka(Text: string);
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddStrelka');
  {$ENDIF}

  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];

  FID:= EID_Switch;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FID:= Min(255, Length(Text));
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Text[1], SizeOf(Char) * FID);

  AddEvent(EID_Switch);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddDefLabel(Rail: TRail; Text: string);
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddDefLabel');
  {$ENDIF}

  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];

  FID:= EID_DefLabel;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FID:= Min(255, Length(Text));
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Rail, 1);
  FData.WriteBuffer(SaveStrIdx, Text[1], SizeOf(Char) * FID);

  AddEvent(EID_DefLabel);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddTextLabel(Text: string);
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddTextLabel');
  {$ENDIF}

  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];

  FID:= EID_TextLabel;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FID:= Min(255, Length(Text));
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Text[1], SizeOf(Char) * FID);

  AddEvent(EID_TextLabel);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddStBoltStyk;
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddStBoltStyk');
  {$ENDIF}

  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];

  FID:= EID_StBoltStyk;
  FData.WriteBuffer(SaveStrIdx, FID, 1);

  AddEvent(EID_StBoltStyk);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddEdBoltStyk;
begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddEdBoltStyk');
  {$ENDIF}

  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];

  FID:= EID_EndBoltStyk;
  FData.WriteBuffer(SaveStrIdx, FID, 1);

  AddEvent(EID_EndBoltStyk);
  FCS.Leave;
end;

procedure TAviconDataContainer.AddTime(Time: TTime);
var
  Hour: Byte;
  Minute: Byte;
  SystemTime: TSystemTime;

begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddTime');
  {$ENDIF}

  FCS.Enter;
  FLastOffset:= FData.Position[SaveStrIdx];

  DateTimeToSystemTime(Time, SystemTime);
  Hour:= SystemTime.wHour;
  Minute:= SystemTime.wMinute;

  FID:= EID_Time; // ������� �������
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Hour, 1);
  FData.WriteBuffer(SaveStrIdx, Minute, 1);

  AddEvent(EID_Time);
  FCS.Leave;
end;

function TAviconDataContainer.DataTransfer(StartDisCoord: Integer): Boolean;
var
  R: TRail;
  Ch: Integer;
  I, Cnt, FullCount: Integer;
  Dat: TDestDat;

begin
  {$IFDEF LOG}
  Writeln(FLog, 'DataTransfer');
  {$ENDIF}

  for I:= 1 to FHSSrcData.FCurLoadEcho[1, FConfig.MinRail, 0].Count do
  begin
    Dat:= FHSSrcData.FCurLoadEcho[1, FConfig.MinRail, 0];

    FullCount:= Dat.Count;
    if FullCount = 0 then
    begin
      Result:= True;
      Exit;
    end;
    if FullCount > 4 then Cnt:= 4
                     else Cnt:= FullCount;

    SetLength(FHSItem, Length(FHSItem) + 1);
    FHSItem[High(FHSItem)].Sample:= FHSSrcData.FCurLoadDisCoord[1] - StartDisCoord;
    FHSItem[High(FHSItem)].Count:= Cnt;
    case Cnt of
      1:
        begin
          FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[1];
          FHSItem[High(FHSItem)].Data[1]:= Dat.Ampl[1] and $0F shl 4;
        end;
      2:
        begin
          FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[1];
          FHSItem[High(FHSItem)].Data[1]:= Dat.Delay[2];
          FHSItem[High(FHSItem)].Data[2]:= Dat.Ampl[1] and $0F shl 4 + Dat.Ampl[2] and $0F;
        end;
      3:
        begin
          FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[1];
          FHSItem[High(FHSItem)].Data[1]:= Dat.Delay[2];
          FHSItem[High(FHSItem)].Data[2]:= Dat.Delay[3];
          FHSItem[High(FHSItem)].Data[3]:= Dat.Ampl[1] and $0F shl 4 + Dat.Ampl[2] and $0F;
          FHSItem[High(FHSItem)].Data[4]:= Dat.Ampl[3] and $0F shl 4;
        end;
      4:
        begin
          FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[1];
          FHSItem[High(FHSItem)].Data[1]:= Dat.Delay[2];
          FHSItem[High(FHSItem)].Data[2]:= Dat.Delay[3];
          FHSItem[High(FHSItem)].Data[3]:= Dat.Delay[4];
          FHSItem[High(FHSItem)].Data[4]:= Dat.Ampl[1] and $0F shl 4 + Dat.Ampl[2] and $0F;
          FHSItem[High(FHSItem)].Data[5]:= Dat.Ampl[3] and $0F shl 4 + Dat.Ampl[4] and $0F;
        end;
    end;

    if FullCount > 4 then
    begin
      SetLength(FHSItem, Length(FHSItem) + 1);
      FHSItem[ High(FHSItem)].Sample:= FHSSrcData.FCurLoadDisCoord[1] - StartDisCoord;
      FHSItem[ High(FHSItem)].Count:= Cnt;
      case Cnt of
        1:
          begin
            FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[5];
            FHSItem[High(FHSItem)].Data[1]:= Dat.Ampl[5] and $0F shl 4;
          end;
        2:
          begin
            FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[5];
            FHSItem[High(FHSItem)].Data[1]:= Dat.Delay[6];
            FHSItem[High(FHSItem)].Data[2]:= Dat.Ampl[5] and $0F shl 4 + Dat.Ampl[6] and $0F;
          end;
        3:
          begin
            FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[5];
            FHSItem[High(FHSItem)].Data[1]:= Dat.Delay[6];
            FHSItem[High(FHSItem)].Data[2]:= Dat.Delay[7];
            FHSItem[High(FHSItem)].Data[3]:= Dat.Ampl[5] and $0F shl 4 + Dat.Ampl[6] and $0F;
            FHSItem[High(FHSItem)].Data[4]:= Dat.Ampl[7] and $0F shl 4;
          end;
        4:
          begin
            FHSItem[High(FHSItem)].Data[0]:= Dat.Delay[5];
            FHSItem[High(FHSItem)].Data[1]:= Dat.Delay[6];
            FHSItem[High(FHSItem)].Data[2]:= Dat.Delay[7];
            FHSItem[High(FHSItem)].Data[3]:= Dat.Delay[8];
            FHSItem[High(FHSItem)].Data[4]:= Dat.Ampl[5] and $0F shl 4 + Dat.Ampl[6] and $0F;
            FHSItem[High(FHSItem)].Data[5]:= Dat.Ampl[7] and $0F shl 4 + Dat.Ampl[8] and $0F;
          end;
      end;
    end;
  end;
  Result:= True;
end;

procedure TAviconDataContainer.AddHandScanFromDataContainer(SrcData: TAviconDataContainer; Rail: TRail; Surf, HandChNum, Att, Ku, TVG: ShortInt; StartCrd, EndCrd: Integer);
var
  I: Integer;
  TMP: THSItem;
  Head: THSHead;

begin
  {$IFDEF LOG}
  Writeln(FLog, 'AddHandScanFromDataContainer');
  {$ENDIF}

  FCS.Enter;
  if EndCrd - StartCrd + 1 > $FFFE then raise EMyExcept.Create('HandScan data to big');

  // FDataRail:= SrcRail;
  // FDataCh:= SrcScanCh;

  SetLength(FHSItem, 0);
  FHSSrcData:= SrcData;
  FHSSrcData.LoadData(1, StartCrd, EndCrd, 0, Self.DataTransfer);

  FLastOffset:= FData.Position[SaveStrIdx];

  FID:= EID_HandScan;
  FData.WriteBuffer(SaveStrIdx, FID, 1);

  Head.Rail:= Rail;
//  Head.DelayMultiply:= DelayMultiply;
  Head.ScanSurface:= Surf;
  Head.HandChNum:= HandChNum;
  Head.Att:= Att;
  Head.Ku:= Ku;
  Head.TVG:= TVG;
  Head.DataSize:= 0;
  for I:= 0 to High(FHSItem) do Head.DataSize:= Head.DataSize + 3 + HSEchoLen[FHSItem[I].Count];
  Head.DataSize:= Head.DataSize + 3;

  FData.WriteBuffer(SaveStrIdx, Head, SizeOf(THSHead));

  for I:= 0 to High(FHSItem) do
    FData.WriteBuffer(SaveStrIdx, FHSItem[I], 3 + HSEchoLen[FHSItem[I].Count]);
  TMP.Sample:= $FFFF;
  TMP.Count:= 0;
  FData.WriteBuffer(SaveStrIdx, TMP, 3);

  AddEvent(EID_HandScan);
  FCS.Leave;
end;
(*
procedure TAviconDataContainer.AddSysCoord(NewSysCoord: Integer);
var
  Flg: Boolean;
  Last: Integer;

begin
  SaveSignals(FCurrEchoList);   // ������� ��� �������� - ��� ��� ����
  ClearEchoList(FCurrEchoList); // ������� ������� �������
  SaveSysCoord(NewSysCoord); // ����������


 // ���������� ����� �� ����� - ����� ����� ����� ��� ������� ��� ��� (��� ����������)
 // � ��� ����� ����� ����� ��������� ����������

  Flg:= False;
  if FFirstAddCoord then            //  ������ ���������� �� �����, � ����������
  begin
    FFirstAddCoord:= False;         // ���� - ������ ���������� ����
    FLastAddSysCrd:= NewSysCoord;   // ���������� �������� ���������� (������)
    Flg:= True;
{    FCurSaveSysCrd:= NewSysCoord;
    Inc(FCurSaveDisCrd, Abs(NewSysCoord - FLastSaveSysCrd)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
    FLastSaveSysCrd:= NewSysCoord;
    FLastSaveDisCrd:= FCurSaveDisCrd; }
  end
  else
  begin                             // ���� ���������� �� ������
                                    // ����� ���������� ��������� (FLastAddSysCrd) � ������� ������� �������� � ���
    if FEventFlag then              // ���� ���� ������� �� c������ ����� ��� ��������
    begin
      SaveSysCoord(FLastAddSysCrd); // ����������
      SaveSignals(FCurrEchoList);   // ������� ��� �������� - ��� ��� ����
      ClearEchoList(FSaveEchoList); // ������� �������
      if AnalyzeSignals then SaveSignals(FSaveEchoList);
    end
    else                            // ���� �� ���� ������� �� c������ ����� � ���������
    begin
      FSaveEchoList:= FCurrEchoList;  // ���� ��� ������ ��� ������� ��� ���� ����������
      if AnalyzeSignals then          // �������� (�������� ������������� ��������)
      begin                           // ���� ���� ��� ���������� �� ����� ...
        SaveSysCoord(FLastAddSysCrd); // ���������� ...
        SaveSignals(FSaveEchoList);   // ������� - ����� �������� (�������� �������������)
      end else Flg:= True;
    end;

    FPrevEchoList:= FCurrEchoList;  // ���������� ������� �������
    ClearEchoList(FCurrEchoList);   // ������� ������� �������
    FLastAddSysCrd:= NewSysCoord;   // ���������� ����� �������� ����������
    FEventFlag:= False;             // ���������� ���� ������� ������� �� ����������
  end;

  if Flg then
  begin
    FCurSaveSysCrd:= NewSysCoord;
    Last:= FCurSaveDisCrd;
    Inc(FCurSaveDisCrd, Abs(NewSysCoord - FLastSaveSysCrd)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)

    FMaxSysCoord:= Max(FMaxSysCoord, NewSysCoord);
    FMaxDisCoord:= FCurSaveDisCrd;

    if (not FBackMotionFlag) and (FCurSaveSysCrd < FLastSaveSysCrd) then // ���� ���������� ����������� �������� (������ �� �����)
    begin
      FLastOffset:= FData.Position[SaveStrIdx];
      AddEvent(EID_BwdDir, FLastSaveSysCrd, Last);
      FBackMotionFlag:= True;
      if not FSearchBMEnd then
      begin
        FStartBMSysCoord:= FLastSaveSysCrd;
        FSearchBMEnd:= True;
      end;
    end
    else if FBackMotionFlag and (FCurSaveSysCrd > FLastSaveSysCrd) then // ���� ���������� ����������� �������� (����� �� ������)
    begin
      FLastOffset:= FData.Position[SaveStrIdx];
      AddEvent(EID_FwdDir, FLastSaveSysCrd, Last);
      FBackMotionFlag:= False;
    end;
    if FSearchBMEnd and (FCurSaveSysCrd > FStartBMSysCoord) then
    begin
      FSearchBMEnd:= False;
      FLastOffset:= FData.Position[SaveStrIdx];
      if FCurSaveSysCrd - FLastSaveSysCrd <> 0 then AddEvent(EID_EndBM, FStartBMSysCoord, Round(FLastSaveDisCrd + (FStartBMSysCoord - FLastSaveSysCrd) * (FCurSaveDisCrd - FLastSaveDisCrd) / (FCurSaveSysCrd - FLastSaveSysCrd)))
                                               else AddEvent(EID_EndBM);
    end;

    if FData.Position[SaveStrIdx] - FLastOffset >= 16384 then
    begin
      FLastOffset:= FData.Position[SaveStrIdx];
      AddEvent(EID_LinkPt);
    end;

    FLastSysCrdOffset:= FData.Position[SaveStrIdx];

    FLastSaveSysCrd:= NewSysCoord;
    FLastSaveDisCrd:= FCurSaveDisCrd;

    FData.SaveToFile(1024);
    FMaxReadOffset:= FData.Position[SaveStrIdx];

    FEvents.FVirtItem.Event.OffSet:= FMaxReadOffset;
    FEvents.FVirtItem.Event.SysCoord:= FCurSaveSysCrd;
    FEvents.FVirtItem.Event.DisCoord:= FCurSaveDisCrd;

    FExHeader.EndMM:= (CurSaveSysCoord - FLastStolbSysCrd) * FHead.ScanStep div 100;
    FExHeader.EndSysCoord:= FCurSaveSysCrd;
    FExHeader.EndDisCoord:= FCurSaveDisCrd;
    FCoordList[FLastKm].Content[FLastPk].Len:= Round((DisToSysCoord(MaxDisCoord) - Event[FCoordList[FLastKm].Content[FLastPk].Idx].SysCoord) * Header.ScanStep / 100);
{
    FCurSaveSysCrd:= NewSysCoord;
    Inc(FCurSaveDisCrd, Abs(NewSysCoord - FLastSaveSysCrd)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
    FLastSaveSysCrd:= NewSysCoord;
    FLastSaveDisCrd:= FCurSaveDisCrd;

    FMaxReadOffset:= FData.Position[SaveStrIdx];

    FEvents.FVirtItem.Event.OffSet:= FMaxReadOffset;
    FEvents.FVirtItem.Event.SysCoord:= FCurSaveSysCrd;
    FEvents.FVirtItem.Event.DisCoord:= FCurSaveDisCrd;

    FExHeader.EndMM:= (CurSaveSysCoord - FLastStolbSysCrd) * FHead.ScanStep div 100;
    FExHeader.EndSysCoord:= FCurSaveSysCrd;
    FExHeader.EndDisCoord:= FCurSaveDisCrd;
    FCoordList[FLastKm].Content[FLastPk].Len:= Round((DisToSysCoord(MaxDisCoord) - Event[FCoordList[FLastKm].Content[FLastPk].Idx].SysCoord) * Header.ScanStep / 100);
}
  end;

end;

function TAviconDataContainer.AnalyzeSignals: Boolean; // ������ ��������
var
  Rail: TRail;
  ScanChNum: Integer;
  Same: Boolean;

begin
  Result:= False;                // ���������� - ���� ���� �� ��� ����������
  for Rail:= FConfig.MinRail to FConfig.MaxRail do
    for ScanChNum:= FConfig.MinScanChNumber to FConfig.MaxScanChNumber do
    begin
                                 // ��������� ������������ ������� � ���������� �������� (�� ���������� � ����������)
      Same:= (FCurrEchoList[Rail, ScanChNum].Count = FPrevEchoList[Rail, ScanChNum].Count) and
             // (FCurrEchoList[Rail, ScanChNum].Count = 1) and
              CompareMem(@FCurrEchoList[Rail, ScanChNum].Signals,
                         @FPrevEchoList[Rail, ScanChNum].Signals,
                         FCurrEchoList[Rail, ScanChNum].Count * 2);

      if Same then FSaveEchoList[Rail, ScanChNum].Count:= 0  // ���� ������� (������� � ����������) ��������� �� �� �� ����� - �������� ����������
              else  begin // ���� ������� ��������� �� ...
                      Result:= Result or (FCurrEchoList[Rail, ScanChNum].Count <> 0); // ���� ���� ������� �� ���� �� ��� ���������� - ������ ����
                      if (FCurrEchoList[Rail, ScanChNum].Count = 0) then // ���� �������� ��� - ����� ����������� ������� - ��� ��������
                      begin
                        FSaveEchoList[Rail, ScanChNum].Count:= 1;          // ����������� ������� - ��� ��������
                        FSaveEchoList[Rail, ScanChNum].Signals[0].T:= $FF; // ����������� ������� - ��� ��������
                        Result:= True;                                     // ���� ������ ������ ���� (���� ��� ����������)
                      end;
                    end;
    end;
end;

procedure TAviconDataContainer.SaveSignals(EchoList: TEchoList); // ������ ����� ���-��������
var
  Rail: TRail;
  ScanChNum: Integer;
  Cnt: array [1..2] of Byte;
  Same: Boolean;

begin
  FCS.Enter;
  for Rail:= FConfig.MinRail to FConfig.MaxRail do
    for ScanChNum:= FConfig.MinScanChNumber to FConfig.MaxScanChNumber do
      with EchoList[Rail, ScanChNum] do if Count <> 0 then

      begin

        // ���������� �� ������ �� 4 �������
        case Count of
      //    0: begin Cnt[1]:= 0; Cnt[2]:= 0; Cnt[3]:= 0; end;
          1: begin Cnt[1]:= 1; Cnt[2]:= 0; end;
          2: begin Cnt[1]:= 2; Cnt[2]:= 0; end;
          3: begin Cnt[1]:= 3; Cnt[2]:= 0; end;
          4: begin Cnt[1]:= 4; Cnt[2]:= 0; end;
          5: begin Cnt[1]:= 4; Cnt[2]:= 1; end;
          6: begin Cnt[1]:= 4; Cnt[2]:= 2; end;
          7: begin Cnt[1]:= 4; Cnt[2]:= 3; end;
          8: begin Cnt[1]:= 4; Cnt[2]:= 4; end;
        end;

        FID:= (Byte(Rail) and 1) shl 6 + (ScanChNum and $F) shl 2 + ((Cnt[1] - 1) and $3);
        FData.WriteBuffer(SaveStrIdx, FID, 1);

        case Cnt[1] of
          1:  begin
                FData.WriteBuffer(SaveStrIdx, Signals[0].T, 1);
                FID:= Signals[0].A and $0F shl 4;
                FData.WriteBuffer(SaveStrIdx, FID, 1);
              end;
          2:  begin
                FData.WriteBuffer(SaveStrIdx, Signals[0].T, 1);
                FData.WriteBuffer(SaveStrIdx, Signals[1].T, 1);
                FID:= Signals[0].A and $0F shl 4 + Signals[1].A and $0F;
                FData.WriteBuffer(SaveStrIdx, FID, 1);
              end;
          3:  begin
                FData.WriteBuffer(SaveStrIdx, Signals[0].T, 1);
                FData.WriteBuffer(SaveStrIdx, Signals[1].T, 1);
                FData.WriteBuffer(SaveStrIdx, Signals[2].T, 1);
                FID:= Signals[0].A and $0F shl 4 + Signals[1].A and $0F;
                FData.WriteBuffer(SaveStrIdx, FID, 1);
                FID:= Signals[2].A and $0F shl 4;
                FData.WriteBuffer(SaveStrIdx, FID, 1);
              end;
          4:  begin
                FData.WriteBuffer(SaveStrIdx, Signals[0].T, 1);
                FData.WriteBuffer(SaveStrIdx, Signals[1].T, 1);
                FData.WriteBuffer(SaveStrIdx, Signals[2].T, 1);
                FData.WriteBuffer(SaveStrIdx, Signals[3].T, 1);
                FID:= Signals[0].A and $0F shl 4 + Signals[1].A and $0F;
                FData.WriteBuffer(SaveStrIdx, FID, 1);
                FID:= Signals[2].A and $0F shl 4 + Signals[3].A and $0F;
                FData.WriteBuffer(SaveStrIdx, FID, 1);
              end;
        end;

        if Cnt[2] <> 0 then
        begin
          FID:= (Byte(Rail) and 1) shl 6 + (ScanChNum and $F) shl 2 + ((Cnt[2] - 1) and $3);
          FData.WriteBuffer(SaveStrIdx, FID, 1);

          case Cnt[2] of
            1:  begin
                  FData.WriteBuffer(SaveStrIdx, Signals[4].T, 1);
                  FID:= Signals[4].A and $0F shl 4;
                  FData.WriteBuffer(SaveStrIdx, FID, 1);
                end;
            2:  begin
                  FData.WriteBuffer(SaveStrIdx, Signals[4].T, 1);
                  FData.WriteBuffer(SaveStrIdx, Signals[5].T, 1);
                  FID:= Signals[4].A and $0F shl 4 + Signals[5].A and $0F;
                  FData.WriteBuffer(SaveStrIdx, FID, 1);
                end;
            3:  begin
                  FData.WriteBuffer(SaveStrIdx, Signals[4].T, 1);
                  FData.WriteBuffer(SaveStrIdx, Signals[5].T, 1);
                  FData.WriteBuffer(SaveStrIdx, Signals[6].T, 1);
                  FID:= Signals[4].A and $0F shl 4 + Signals[5].A and $0F;
                  FData.WriteBuffer(SaveStrIdx, FID, 1);
                  FID:= Signals[6].A and $0F shl 4;
                  FData.WriteBuffer(SaveStrIdx, FID, 1);
                end;
            4:  begin
                  FData.WriteBuffer(SaveStrIdx, Signals[4].T, 1);
                  FData.WriteBuffer(SaveStrIdx, Signals[5].T, 1);
                  FData.WriteBuffer(SaveStrIdx, Signals[6].T, 1);
                  FData.WriteBuffer(SaveStrIdx, Signals[7].T, 1);
                  FID:= Signals[4].A and $0F shl 4 + Signals[5].A and $0F;
                  FData.WriteBuffer(SaveStrIdx, FID, 1);
                  FID:= Signals[6].A and $0F shl 4 + Signals[7].A and $0F;
                  FData.WriteBuffer(SaveStrIdx, FID, 1);
                end;
          end;
        end;
      end;
  FCS.Leave;
end;
*)

/////////////////////////////////////////////////////////////////////////////////////

                 // ������ ��������� ���������� ��� ������ ��� A, ����� �������
procedure TAviconDataContainer.AddTestFullSysCoord_UMUA_Left(SysCoord: Integer);
begin
  FCS.Enter;
  FID:= EID_SysCrd_UMUA_Left_NF;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, SysCoord, 4);
  FData.SaveToFile(1024);
  FMaxReadOffset:= FData.Position[SaveStrIdx];
  FCS.Leave;
end;

procedure TAviconDataContainer.AddTestFullSysCoord_UMUB_Left(SysCoord: Integer);
begin
  FCS.Enter;
  FID:= EID_SysCrd_UMUB_Left_NF;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, SysCoord, 4);
  FData.SaveToFile(1024);
  FMaxReadOffset:= FData.Position[SaveStrIdx];
  FCS.Leave;
end;

procedure TAviconDataContainer.AddTestFullSysCoord_UMUA_Right(SysCoord: Integer);
begin
  FCS.Enter;
  FID:= EID_SysCrd_UMUA_Right_NF;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, SysCoord, 4);
  FData.SaveToFile(1024);
  FMaxReadOffset:= FData.Position[SaveStrIdx];
  FCS.Leave;
end;

procedure TAviconDataContainer.AddTestFullSysCoord_UMUB_Right(SysCoord: Integer);
begin
  FCS.Enter;
  FID:= EID_SysCrd_UMUB_Right_NF;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, SysCoord, 4);
  FData.SaveToFile(1024);
  FMaxReadOffset:= FData.Position[SaveStrIdx];
  FCS.Leave;
end;

/////////////////////////////////////////////////////////////////////////////////////
{
const
  xFF: Byte = $FF;

procedure TAviconDataContainer.AddTestDeltaSysCoord_UMUA_Left(Delta: ShortInt);
begin
  FID:= EID_SysCrd_UMUA_Left_NF;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Delta, 1);
  FData.WriteBuffer(SaveStrIdx, xFF, 1);
  FData.WriteBuffer(SaveStrIdx, xFF, 1);
  FData.WriteBuffer(SaveStrIdx, xFF, 1);
  FData.SaveToFile(1024);
  FMaxReadOffset:= FData.Position[SaveStrIdx];
end;

procedure TAviconDataContainer.AddTestDeltaSysCoord_UMUB_Left(Delta: ShortInt);
begin
  FID:= EID_SysCrd_UMUB_Left_NF;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Delta, 1);
  FData.WriteBuffer(SaveStrIdx, xFF, 1);
  FData.WriteBuffer(SaveStrIdx, xFF, 1);
  FData.WriteBuffer(SaveStrIdx, xFF, 1);
  FData.SaveToFile(1024);
  FMaxReadOffset:= FData.Position[SaveStrIdx];
end;

procedure TAviconDataContainer.AddTestDeltaSysCoord_UMUA_Right(Delta: ShortInt);
begin
  FID:= EID_SysCrd_UMUA_Right_NF;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Delta, 1);
  FData.WriteBuffer(SaveStrIdx, xFF, 1);
  FData.WriteBuffer(SaveStrIdx, xFF, 1);
  FData.WriteBuffer(SaveStrIdx, xFF, 1);
  FData.SaveToFile(1024);
  FMaxReadOffset:= FData.Position[SaveStrIdx];
end;

procedure TAviconDataContainer.AddTestDeltaSysCoord_UMUB_Right(Delta: ShortInt);
begin
  FID:= EID_SysCrd_UMUB_Right_NF;
  FData.WriteBuffer(SaveStrIdx, FID, 1);
  FData.WriteBuffer(SaveStrIdx, Delta, 1);
  FData.WriteBuffer(SaveStrIdx, xFF, 1);
  FData.WriteBuffer(SaveStrIdx, xFF, 1);
  FData.WriteBuffer(SaveStrIdx, xFF, 1);
  FData.SaveToFile(1024);
  FMaxReadOffset:= FData.Position[SaveStrIdx];
end;
}
////////////////////////////////////////////////////////////////////////////////////

procedure TAviconDataContainer.AddSysCoord(SysCoord: Integer);
var
  R: TRail;
  I: Integer;
  BackLink: Integer;
  Last: Integer;
  P: Pointer;
  F: file;
  EchoBlock: TEchoBlock;
  CheckSum: Byte;
  CheckSumData: Byte;
  CheckSumLen: Integer;

begin
  FCS.Enter;

  {$IFDEF LOG}
  Writeln(FLog, Format('AddCrd: %d', [SysCoord]));
  {$ENDIF}

  FCurSaveSysCrd:= SysCoord;
  Last:= FCurSaveDisCrd;
  Inc(FCurSaveDisCrd, Abs(SysCoord - FLastSaveSysCrd)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)

  FMaxSysCoord:= Max(FMaxSysCoord, SysCoord);
  FMinSysCoord:= Max(FMinSysCoord, SysCoord);
  FMaxDisCoord:= FCurSaveDisCrd;

  if (not FBackMotionFlag) and (FCurSaveSysCrd < FLastSaveSysCrd) then // ���� ���������� ����������� �������� (������ �� �����)
  begin
    FLastOffset:= FData.Position[SaveStrIdx];
    AddEvent(EID_BwdDir, FLastSaveSysCrd, Last);
    FBackMotionFlag:= True;
    if not FSearchBMEnd then
    begin
      FStartBMSysCoord:= FLastSaveSysCrd;
      FSearchBMEnd:= True;
    end;
  end
  else if FBackMotionFlag and (FCurSaveSysCrd > FLastSaveSysCrd) then // ���� ���������� ����������� �������� (����� �� ������)
  begin
    FLastOffset:= FData.Position[SaveStrIdx];
    AddEvent(EID_FwdDir, FLastSaveSysCrd, Last);
    FBackMotionFlag:= False;
  end;
  if FSearchBMEnd and (FCurSaveSysCrd > FStartBMSysCoord) then
  begin
    FSearchBMEnd:= False;
    FLastOffset:= FData.Position[SaveStrIdx];
    if FCurSaveSysCrd - FLastSaveSysCrd <> 0 then AddEvent(EID_EndBM, FStartBMSysCoord, Round(FLastSaveDisCrd + (FStartBMSysCoord - FLastSaveSysCrd) * (FCurSaveDisCrd - FLastSaveDisCrd) / (FCurSaveSysCrd - FLastSaveSysCrd)))
                                             else AddEvent(EID_EndBM);
  end;

  if FData.Position[SaveStrIdx] - FLastOffset >= 16384 then
  begin
    FLastOffset:= FData.Position[SaveStrIdx];
    AddEvent(EID_LinkPt);
  end;

  if FFirstCoord then
  begin
    BackLink:= 0;
    FFirstCoord:= False;
  end else BackLink:= FData.Position[SaveStrIdx] - FLastSysCrdOffset;

  FLastSysCrdOffset:= FData.Position[SaveStrIdx];

  if not FNewSysCoordSystem then // ������ ������� ������ ��������� � ��������
  begin

    if FLastSaveSysCrd and $FFFFFF00 = SysCoord and $FFFFFF00 then
    begin // �������� ����������
      if BackLink and $FFFFFF00 = 0 then // �������� ������
      begin
        FID:= EID_SysCrd_SS;
        FData.WriteBuffer(SaveStrIdx, FID, 1);
        FData.WriteBuffer(SaveStrIdx, BackLink, 1);
        FData.WriteBuffer(SaveStrIdx, SysCoord, 1);
      end
      else
      begin
        FID:= EID_SysCrd_FS;
        FData.WriteBuffer(SaveStrIdx, FID, 1);
        FData.WriteBuffer(SaveStrIdx, BackLink, 4);
        FData.WriteBuffer(SaveStrIdx, SysCoord, 1);
      end;
    end
    else
    begin // ������ ����������
      if BackLink and $FFFFFF00 = 0 then // �������� ������
      begin
        FID:= EID_SysCrd_SF;
        FData.WriteBuffer(SaveStrIdx, FID, 1);
        FData.WriteBuffer(SaveStrIdx, BackLink, 1);
        FData.WriteBuffer(SaveStrIdx, SysCoord, 4);
      end
      else
      begin
        FID:= EID_SysCrd_FF;
        FData.WriteBuffer(SaveStrIdx, FID, 1);
        FData.WriteBuffer(SaveStrIdx, BackLink, 4);
        FData.WriteBuffer(SaveStrIdx, SysCoord, 4);
      end;
    end;
  end
  else   // ����� ������� ������ ��������� ��� ������
  begin

    if FLastSaveSysCrd and $FFFFFF00 = SysCoord and $FFFFFF00 then
    begin // �������� ����������
      FID:= EID_SysCrd_NS;
      FData.WriteBuffer(SaveStrIdx, FID, 1);
      FData.WriteBuffer(SaveStrIdx, SysCoord, 1);
    end
    else
    begin // ������ ����������
      FID:= EID_SysCrd_NF;
      FData.WriteBuffer(SaveStrIdx, FID, 1);
      FData.WriteBuffer(SaveStrIdx, SysCoord, 4);
    end;

  end;

  FLastSaveSysCrd:= SysCoord;
  FLastSaveDisCrd:= FCurSaveDisCrd;

  FData.SaveToFile(1024);
  FMaxReadOffset:= FData.Position[SaveStrIdx];

  if FFullHeader then
  begin
    FEvents.FVirtItem.Event.OffSet:= FMaxReadOffset;
    FEvents.FVirtItem.Event.SysCoord:= FCurSaveSysCrd;
    FEvents.FVirtItem.Event.DisCoord:= FCurSaveDisCrd;

//    FExHeader.EndMM:= (CurSaveSysCoord - FLastStolbSysCrd) * FHead.ScanStep div 100;
    FExHeader.EndSysCoord:= FCurSaveSysCrd;
    FExHeader.EndDisCoord:= FCurSaveDisCrd;
//    FCoordList[FLastKm].Content[FLastPk].Len:= Round((DisToSysCoord(MaxDisCoord) - Event[FCoordList[FLastKm].Content[FLastPk].Idx].SysCoord) * Header.ScanStep / 100);
  end;

  FCS.Leave;
end;

procedure TAviconDataContainer.AddEvent(ID: Byte; SysCoord: Integer = - MaxInt; DisCoord: Integer = - MaxInt);
var
  pData: PEventData;
  NewIdx: Integer;
  New: TFileEvent;
  New_: TContainerEvent;
  EventBodyLen: Integer;

  // HSItem: THSItem;
  // Echo: array [1 .. 6] of Byte;
  // Flag: Boolean;
  // H, M

begin
  {$IFDEF LOG}
  Writeln(FLog, Format('AddEvent: Pos: %d ID:%d', [FData.Position[SaveStrIdx], ID]));
  {$ENDIF}

  New.ID:= ID;
  New.OffSet:= FLastOffset;
  if SysCoord = - MaxInt then New.SysCoord:= FCurSaveSysCrd
                         else New.SysCoord:= SysCoord;
  if DisCoord = - MaxInt then New.DisCoord:= FCurSaveDisCrd
                         else New.DisCoord:= DisCoord;

  FMaxSysCoord:= Max(FMaxSysCoord, New.SysCoord);
  FMaxDisCoord:= New.DisCoord;

  if (New.ID in [EID_Ku           , EID_Att          , EID_TVG          , EID_StStr        ,
                 EID_EndStr       , EID_HeadPh       , EID_Mode         , EID_PrismDelay   ,
                 EID_SetRailType  , EID_Stolb        , EID_Switch       , EID_Time         ,
                 EID_DefLabel     , EID_TextLabel    , EID_StBoltStyk   , EID_EndBoltStyk  , EID_ACData, EID_SensFail, EID_AutomaticSearchRes, EID_OperatorRemindLabel,
                 EID_Sensor1      , EID_ZerroProbMode, EID_LongLabel    , EID_AirBrush     , EID_AirBrush2    ,
                 EID_StolbChainage, EID_PaintMarkRes , EID_AirBrushTempOff, EID_AlarmTempOff, EID_GPSCoord, EID_GPSCoord2, EID_GPSState, EID_NORDCO_Rec]) then
  begin
    FData.Position[LoadStrIdx]:= New.OffSet  + 1;
    EventBodyLen:= GetEventBodyLen(LoadStrIdx, New.ID);
    FData.Position[LoadStrIdx]:= New.OffSet  + 1;
    FData.ReadBuffer(LoadStrIdx, New.Data[1], EventBodyLen);
  end;


  case ID of
//           EID_Ku: FCurParam.Par[TRail(New.Data[1]), New.Data[2]].Ku:= ShortInt(New.Data[3]);
//          EID_Att: FCurParam.Par[TRail(New.Data[1]), New.Data[2]].Att:= New.Data[3];
//          EID_TVG: FCurParam.Par[TRail(New.Data[1]), New.Data[2]].TVG:= New.Data[3];
              EID_StStr: FCurSaveParam[TRail(New.Data[1]), New.Data[2]].StStr:= New.Data[3];
             EID_EndStr: FCurSaveParam[TRail(New.Data[1]), New.Data[2]].EndStr:= New.Data[3];
//   EID_PrismDelay: FCurParam.Par[TRail(New.Data[1]), New.Data[2]].PrismDelay:= New.Data[3] + New.Data[4] * $100;
//         EID_Mode: Params_.Mode:= New.Data[1] + New.Data[2] * $100;
            EID_Sensor1: FCurSaveSensor1[TRail(New.Data[1])]:= Boolean(New.Data[2]);
      EID_ZerroProbMode:
                         FCurSaveZPMode:= TZerroProbMode(New.Data[1]);
  end;

  New_.Event:= New;
  New_.EvalChPar:= FCurSaveParam;
  New_.Sensor1:= FCurSaveSensor1;
  New_.ZerroProbMode:= FCurSaveZPMode;
  NewIdx:= FEvents.Add(New_);
  // FEventsCount:= FEvents.Count;

  if (New.ID = EID_FwdDir) or (New.ID = EID_BwdDir) then
  begin
    SetLength(FCDList, Length(FCDList) + 1);
//    FCDList[ High(FCDList)]:= NewIdx;
    FCDList[ High(FCDList)].ID:= New.ID;
    FCDList[ High(FCDList)].DisCoord:= DisCoord;
  end;

  (*
    // ���������� ������� �������

    if ID in [    EID_Stolb, EID_StBoltStyk,  EID_EndLineLabel,
    EID_Switch, EID_EndBoltStyk, EID_StSwarStyk,
    EID_DefLabel, EID_Time,        EID_EndSwarStyk,
    EID_TextLabel, EID_StLineLabel ] then
    begin
    pData:= Addr(New.Data);

    H:= pData^[Avk11EventLen[ID] - 1];
    M:= pData^[Avk11EventLen[ID] - 0];

    H:= (H shr 4 and $0F) * 10 + (H and $0F);
    M:= (M shr 4 and $0F) * 10 + (M and $0F);

    if (H <> FOldH) or (M <> FOldM) then
    begin
    SetLength(FTimeList, Length(FTimeList) + 1);
    FTimeList[High(FTimeList)].H:= H;
    FTimeList[High(FTimeList)].M:= M;
    FTimeList[High(FTimeList)].DC:= New.DisCoord;
    FOldH:= H;
    FOldM:= M;
    end;
    end;
    *)
  // ----------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------

{  if ID = EID_Stolb then
  begin
    pData:= Addr(New.Data);
    if pCoord(pData)^.Km[0] <> pCoord(pData)^.Km[1] then
    begin
      SetLength(FCoordList, Length(FCoordList) + 1);
      FCoordList[High(FCoordList)].KM:= pCoord(pData)^.Km[1];
      with FCoordList[High(FCoordList)] do
      begin
        SetLength(Content, Length(Content) + 1);
        if Header.MoveDir = 0 then Content[High(Content)].Pk:= 10
                              else Content[High(Content)].Pk:= 1;
        Content[High(Content)].Idx:= NewIdx;
        FCoordList[FLastKm].Content[FLastPk].Len:= Round((New.SysCoord - Event[FCoordList[FLastKm].Content[FLastPk].Idx].SysCoord) * Header.ScanStep / 100);
        FLastKm:= High(FCoordList);
        FLastPk:= High(Content);
      end;
    end;
    if pCoord(pData)^.Km[0] = pCoord(pData)^.Km[1] then
      with FCoordList[High(FCoordList)] do
      begin
        SetLength(Content, Length(Content) + 1);
        Content[High(Content)].Pk:= pCoord(pData)^.Pk[1];
        Content[High(Content)].Idx:= NewIdx;
        FCoordList[FLastKm].Content[FLastPk].Len:= Round((New.SysCoord - Event[FCoordList[FLastKm].Content[FLastPk].Idx].SysCoord) * Header.ScanStep / 100);
        FLastKm:= High(FCoordList);
        FLastPk:= High(Content);
      end;
  end;
}
//  if FFullHeader then
//    FCoordList[FLastKm].Content[FLastPk].Len:= Round((DisToSysCoord(MaxDisCoord) - Event[FCoordList[FLastKm].Content[FLastPk].Idx].SysCoord) * Header.ScanStep / 100);
//  FEventFlag:= True;
end;

(*
function TAviconDataContainer.GetStartCoord(): TRealCoord;
begin
  if FPathCoordSystem = csNormal then
  begin
    Result.Level0:= FHead.StartKM;
    Result.Level1:= FHead.StartPk;
    Result.Level2:= FHead.StartMetre * 1000;
  end
  else
  begin

//    Result:= ChainageValToRealCoord(FHead.StartChainage, FPathCoordSystem);

//    Result.Level0:= FHead.StartChainage; // #ATT
//    Result.Level1:= 0;
//    Result.Level2:= 0;
  end;
  Result.CoordSys:= FPathCoordSystem;
end;
*)
// -------------< ������ � ������� >------------------------------------

function TAviconDataContainer.NormalizeDisCoord(Src: Integer): Integer;
begin
  if Src < 0 then Result:= 0
             else if Src > FMaxDisCoord then Result:= FMaxDisCoord
                                        else Result:= Src;
end;

// ------------------------< ������������ �������� ������ >---------------------

function TAviconDataContainer.GetMaxDisCoord: Integer;
begin
  Result:= FMaxDisCoord;
end;

function TAviconDataContainer.GetMaxSysCoord: Integer;
begin
  Result:= FMaxSysCoord;
end;

function TAviconDataContainer.GetEventCount: Integer;
begin
  Result:= FEvents.Count;
end;

function TAviconDataContainer.GetCurLoadOffset(Index: Integer): Integer;   // FCurOffset
begin
  Result:= FCurLoadOffset[Index];
end;

function TAviconDataContainer.GetCurLoadSysCoord(Index: Integer): Longint; // FCurSysCoord
begin
  Result:= FCurLoadSysCoord[Index];
end;

function TAviconDataContainer.GetCurLoadDisCoord(Index: Integer): Int64;   // FCurDisCoord
begin
  Result:= FCurLoadDisCoord[Index];
end;

function TAviconDataContainer.GetCurLoadEcho(Index: Integer): TCurEcho;   // FCurEcho
begin
  Result:= FCurLoadEcho[Index];
end;

function TAviconDataContainer.GetCurLoadBackMotion(Index: Integer): Boolean;
begin
  Result:= FCurLoadBackMotion[Index];
end;

function TAviconDataContainer.GetCurLoadParams(Index: Integer): TEvalChannelsParams;
begin
  Result:= FCurLoadParams[Index]
end;

function TAviconDataContainer.GetCurSaveParams(Rail: TRail; EvalCh: Integer): TEvalChannelParams;
begin
  Result:= FCurSaveParam[Rail][EvalCh];
end;

function TAviconDataContainer.GetLoadSensor1(Index: Integer): TSensor1Data;
begin
  Result:= FCurLoadSensor1[Index];
end;

function TAviconDataContainer.GetLoadZPMode(Index: Integer): TZerroProbMode;
begin
  Result:= FCurLoadZPMode[Index];
end;

function TAviconDataContainer.GetEvent(Index: Integer): TFileEvent;
begin
  if (Index >= 0) and (Index < FEvents.Count) then Result:= FEvents[Index].Event;
end;

function TAviconDataContainer.SysToDisCoord(Coord: Integer): Integer;
var
  Flg: Boolean;
  I: Integer;

begin
  {$IFDEF LOG}
  Writeln(FLog, Format('SysToDisCoord', []));
  {$ENDIF}

  Result:= 0;
  for I:= 0 to FEvents.Count - 2 do // ����� �� �������
  begin
    if (Coord >= FEvents[I + 0].Event.SysCoord) and (Coord <= FEvents[I + 1].Event.SysCoord) then
    begin
      Result:= FEvents[I + 0].Event.DisCoord + Abs(FEvents[I + 0].Event.SysCoord - Coord);
      Exit;
    end;
  end;

  for I:= 0 to FEvents.Count - 2 do // ����� �� ������
  begin
    if (Coord <= FEvents[I + 0].Event.SysCoord) and (Coord >= FEvents[I + 1].Event.SysCoord) then
    begin
      Result:= FEvents[I + 0].Event.DisCoord + Abs(FEvents[I + 0].Event.SysCoord - Coord);
      Exit;
    end;
  end;

  if Coord > FMaxSysCoord then Result:= FCurSaveSysCrd;
  if Coord < FMinSysCoord then Result:= 0;

end;

function TAviconDataContainer.DisToSysCoord(Coord: Integer): Integer;
var
  LeftIdx, RightIdx: Integer;
  Del: Integer;

begin
  {$IFDEF LOG}
  Writeln(FLog, Format('DisToSysCoord', []));
  {$ENDIF}
  Coord:= NormalizeDisCoord(Coord);
  LeftIdx:= GetLeftEventIdx(Coord);
  if FEvents[LeftIdx].Event.DisCoord = Coord then Result:= FEvents[LeftIdx].Event.SysCoord
  else
  begin
    RightIdx:= GetRightEventIdx(Coord);
    Del:= (FEvents[RightIdx].Event.DisCoord - FEvents[LeftIdx].Event.DisCoord);
    if Del <> 0 then Result:= Round(FEvents[LeftIdx].Event.SysCoord + ((Coord - FEvents[LeftIdx].Event.DisCoord) / Del) * (FEvents[RightIdx].Event.SysCoord - FEvents[LeftIdx].Event.SysCoord))
                else Result:= FEvents[LeftIdx].Event.SysCoord;
  end;
end;

procedure TAviconDataContainer.DisToDisCoords(Coord: Integer; var Res: TIntegerDynArray);
var
  I: Integer;
  Del: Integer;
  Res_: Integer;

begin
  Coord:= DisToSysCoord(NormalizeDisCoord(Coord));
  // for LeftIdx:= 0 to EventCount - 2 do
  for I:= 0 to EventCount - 2 do
  begin

    if (FEvents[I].Event.SysCoord <= Coord) and (FEvents[I + 1].Event.SysCoord >= Coord) or
      (FEvents[I].Event.SysCoord >= Coord) and (FEvents[I + 1].Event.SysCoord <= Coord) then
    begin
      SetLength(Res, Length(Res) + 1);
      Res[ High(Res)]:= FEvents[I + 0].Event.DisCoord + Abs
        (Coord - FEvents[I + 0].Event.SysCoord);
    end;
    {
      if (FEvents[I].Event.SysCoord >= Coord) and (FEvents[I + 1].Event.SysCoord <= Coord) then
      begin
      SetLength(Res, Length(Res) + 1);
      Res[High(Res)]:= FEvents[I + 0].Event.DisCoord + Abs(Coord - FEvents[I + 1].Event.SysCoord);
      end;
      }
  end;

end;

function TAviconDataContainer.DisToFileOffset(StreamIdx: Integer; NeedDisCoord: Integer; var DisCoord: Int64; var SysCoord: Integer; var OffSet: Integer): Boolean;
var
  R: TRail;
  LastSysCoord: Integer;
  SaveOffset: Integer;
  SkipBytes: Integer;
  CurSysCoord: Integer;
  CurDisCoord: Int64;
  LoadID: Byte;
  LoadByte: array [1 .. 22] of Byte;
  LoadLongInt: array [1 .. 5] of Longint; // absolute LoadByte[1];
  HSHead: THSHead;
  HSItem: THSItem;
  StartIdx, EndIdx: Integer;
  DisCoord_: Integer;
  SysCoord_: Integer;
  Offset_: Integer;
  CurDisCoord_: Integer;
  CurSysCoord_: Integer;
  CurPosition_: Integer;
  SameCoord: TIntegerDynArray;
  // BSHead: mBSAmplHead;
  TMP: Boolean;
  SkipEventCount: Integer;

begin
  {$IFDEF LOG}
  Writeln(FLog, Format('DisToFileOffset', []));
  {$ENDIF}
  NeedDisCoord:= NormalizeDisCoord(NeedDisCoord);
  StartIdx:= GetLeftEventIdx(NeedDisCoord);

  if StartIdx = - 1 then
  begin
    DisCoord:= 0;
    SysCoord:= 0;
    OffSet:= 0;
    Result:= False;
    Exit;
  end;

  SkipEventCount:= 1;

  FData.Position[StreamIdx]:= FEvents[StartIdx].Event.OffSet;
  LastSysCoord:= FEvents[StartIdx].Event.SysCoord;
  CurSysCoord:= FEvents[StartIdx].Event.SysCoord;
  CurDisCoord:= FEvents[StartIdx].Event.DisCoord;
  // FLoadPack:= FEvents[StartIdx].Pack;

//  FillChar(FCurEcho, SizeOf(TCurEcho), 0);

  if CurDisCoord >= NeedDisCoord then
  begin
    DisCoord:= CurDisCoord;
    SysCoord:= CurSysCoord;
    OffSet:= FData.Position[StreamIdx];
    Result:= True;
    Exit;
  end;

  // if FSkipBM then TestBMOffsetFirst;
  (*
    for R:= r_Left to r_Right do
    begin
    FCurEcho[R, 1].Count:= Ord(FLoadPack[R].Ampl <> 255);
    FCurEcho[R, 1].Delay[1]:= FLoadPack[R].Delay;
    FCurEcho[R, 1].Ampl[1]:= FLoadPack[R].Ampl;
    end;
    *)
  // BMSkipFlag:= False;
  repeat
    // if not TestOffsetNext then Exit;
    { if FSkipBM then
      begin
      Tmp:= BMSkipFlag;
      BMSkipFlag:= TestBMOffsetNext;
      if Tmp and (not BMSkipFlag) then
      for R:= r_Left to r_Right do
      FCurEcho[R, 1].Count:= 0;
      end; }

    if FData.Position[StreamIdx] >= FMaxReadOffset then
    begin
      Result:= False;
      Break;
    end;

    SaveOffset:= FData.Position[StreamIdx];

    CurDisCoord_:= CurDisCoord;
    CurSysCoord_:= CurSysCoord;
    CurPosition_:= FData.Position[StreamIdx];

    FData.ReadBuffer(StreamIdx, LoadID, 1);
//    SkipBytes:= - 1;
//    if LoadID and 128 = 0 then
(*    begin
        if LoadID shr 3 and $07 = 1 then    // ���� ������ ����� �� �������������
        begin
        FSrcDat[0]:= LoadID;
        FData.ReadBuffer2(FSrcDat[1], Avk11EchoLen[LoadID and $07]);
        DataToEcho;
        end
        else if LoadID shr 3 and $07 = 0 then SkipBytes:= 1   // ���� ������� ����� �� ���������� 1 ����
        else

      SkipBytes:= EchoLen[LoadID and $03]; // ����� ����������
    end
    else if not SkipEvent(2, LoadID) then
    begin    *)
      { if not BMSkipFlag then
        begin }
      case LoadID of

        EID_SysCrd_SS:  begin // ��������� ���������� �������� ������, �������� ����������
                          FData.ReadBuffer(StreamIdx, LoadByte[1], 2);
                          CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[2]);
                        end;
        EID_SysCrd_SF:  begin // ��������� ���������� �������� ������, ������ ����������
                          FData.ReadBuffer(StreamIdx, LoadByte[1], 1);
                          FData.ReadBuffer(StreamIdx, LoadLongInt, 4);
                          CurSysCoord:= LoadLongInt[1];
                        end;
        EID_SysCrd_FS:  begin // ��������� ���������� ������ ������, �������� ����������
                          FData.ReadBuffer(StreamIdx, LoadByte[1], 5);
                          CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[5]);
                        end;
        EID_SysCrd_FF:  begin // ��������� ���������� ������ ������, ������ ����������
                          FData.ReadBuffer(StreamIdx, LoadByte[1], 4);
                          FData.ReadBuffer(StreamIdx, LoadLongInt, 4);
                          CurSysCoord:= LoadLongInt[1];
                        end;
         EID_SysCrd_NS: begin
                          FData.ReadBuffer(StreamIdx, LoadByte[1], 1);
                          CurSysCoord:= Integer((Integer(CurSysCoord) and $FFFFFF00) or LoadByte[1]);
                        end;
         EID_SysCrd_NF: begin
                          FData.ReadBuffer(StreamIdx, LoadLongInt, 4);
                          CurSysCoord:= LoadLongInt[1];
                        end;

        EID_EndFile:    begin // ����� �����
                          DisCoord:= CurDisCoord;
                          SysCoord:= CurSysCoord;
                          OffSet:= FData.Position[StreamIdx];
                          Exit;
                        end;
        else
        begin
          SkipEvent(StreamIdx, LoadID, SkipEventCount);
          SkipEventCount:= SkipEventCount + 1;
        end;
      end;
      { end
        else
        begin
        SkipBytes:= Avk11EventLen[LoadID];
        if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF] then UnPackEcho;
        end; }
//    end;
//    if SkipBytes <> -1 then
//      FData.Position[2]:= FData.Position[2] + SkipBytes;

    if { (not BMSkipFlag) and } (LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF, EID_SysCrd_NF, EID_SysCrd_NS]) then
    begin
      Inc(CurDisCoord, Abs(CurSysCoord - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
      LastSysCoord:= CurSysCoord;

      { if not SkipUnPack then }
      // UnPackEcho;
      // SkipUnPack:= False;

      if CurDisCoord > NeedDisCoord then
      begin
        DisCoord:= CurDisCoord_;
        SysCoord:= CurSysCoord_;
        OffSet:= CurPosition_;
        FData.Position[StreamIdx]:= OffSet;
        Result:= True;
        Break;
      end
      else if CurDisCoord = NeedDisCoord then
      begin
        DisCoord:= CurDisCoord;
        SysCoord:= CurSysCoord;
        OffSet:= FData.Position[StreamIdx];
        Result:= True;
        Break;
      end
    end;
  until FData.Position[StreamIdx] >= FMaxReadOffset;
end;

procedure TAviconDataContainer.LoadData(StreamIdx: Integer; StartDisCoord, EndDisCoord, LoadShift: Integer; BlockOk: TDataNotifyEvent);
var
  R: TRail;
  LoadID: Byte;
  Dat: Byte;
  I, J, K, T, {SkipBytes, } OffSet: Integer;
  ScanCh: Integer;
  LastSysCoord: Longint;
  LastDisCoord: Longint;
  SaveLastSysCoord: Longint;
  LoadByte: array [1 .. 22] of Byte;
  LoadLongInt: array [1 .. 5] of Longint; // absolute LoadByte[1];
  HSHead: THSHead;
  HSItem: THSItem;
  CurEcho_: TCurEcho;
  NewSysCoord: Longint;
  TMP: Boolean;
  P: Pointer;
  // BSHead: mBSAmplHead;
  FSrcDat: array [0..7] of Byte;
  tmp_: Integer;
  NeedDisCoord: Integer;
  StartIdx: Integer;
  P_: Pointer;

begin

  {$IFDEF LOG}
  Writeln(FLog, Format('LoadData MaxReadOffset: %d', [FMaxReadOffset]));
  Writeln(FLog, Format('LoadData StrIdx: %d; StDisCrd: %d; EdDisCrd: %d', [StreamIdx, StartDisCoord, EndDisCoord]));
  {$ENDIF}

  P_:= @FCurLoadEcho[StreamIdx];
  FillChar(P_^, SizeOf(TCurEcho), 0);
  {
  for R:= FConfig.MinRail to FConfig.MaxRail do // ������� ������� ������� ��������
    for I:= 0 to FConfig.MaxScanChNumber do
    begin
      FCurLoadEcho[StreamIdx, R, I].Count:= 0;
//      FCurLoadEcho[StreamIdx, R, I].ZerroFlag:= False;
//      FCurLoadEcho[StreamIdx, R, I].ZerroFlag2:= False;
    end;
  }
  if not DisToFileOffset(StreamIdx, StartDisCoord - LoadShift, FCurLoadDisCoord[StreamIdx], FCurLoadSysCoord[StreamIdx], OffSet) then Exit;

  {$IFDEF LOG}
  Writeln(FLog, Format('DisToFileOffset OffSet: %d', [OffSet]));
  {$ENDIF}
(*
  NeedDisCoord:= NormalizeDisCoord(StartDisCoord - LoadShift);
  // GetEventIdx(NeedDisCoord, NeedDisCoord, StartIdx, EndIdx);

  // ���� �� ���������� ������ � �����
  // ��� ����� - ������� ��������� ������� � ������ ��� ������� �����������
  // ToLog(Format('NeedDisCoord: %d', [NeedDisCoord]));                              // ----------------------------------------------------------------------------------------
  StartIdx:= GetLeftEventIdx(NeedDisCoord);

  I:= FEvents[StartIdx].Event.DisCoord;
  while (I = FEvents[StartIdx].Event.DisCoord) and (StartIdx <> 0) do Dec(StartIdx);

  FCurLoadDisCoord[StreamIdx]:= FEvents[StartIdx].Event.DisCoord;
  FCurLoadSysCoord[StreamIdx]:= FEvents[StartIdx].Event.SysCoord;
  OffSet:= FEvents[StartIdx].Event.OffSet;
*)

(*  for R:= r_Left to r_Right do
    begin
    FCurEcho[R, 1].Count:= Ord(FLoadPack[R].Ampl <> 255);
    FCurEcho[R, 1].Delay[1]:= FLoadPack[R].Delay;
    FCurEcho[R, 1].Ampl[1]:= FLoadPack[R].Ampl;
    end;   *)

  LastSysCoord:= FCurLoadSysCoord[StreamIdx];
  FData.Position[StreamIdx]:= OffSet;

  GetParamFirst(StreamIdx, StartDisCoord - LoadShift, FCurLoadParams[StreamIdx], FCurLoadSensor1[StreamIdx], FCurLoadZPMode[StreamIdx]);

  repeat
    if FData.Position[StreamIdx] >= FMaxReadOffset then Break;

    FCurLoadOffset[StreamIdx]:= FData.Position[StreamIdx];
//    SkipBytes:= -1;
//    LoadID:= 23;
//    tmp_:= FData.Position[StreamIdx];
//    FData.Position[StreamIdx]:= tmp_;
    FData.ReadBuffer(StreamIdx, LoadID, 1);

    if LoadID and 128 = 0 then // ��� �������
    begin
      ScanCh:= LoadID shr 2 and $0F;
      if FExtendedChannels and (ScanCh > 14) then
      begin
        FData.ReadBuffer(StreamIdx, ScanCh, 1);
        ScanCh:= ScanCh + 15;
      end;
      FData.ReadBuffer(StreamIdx, FSrcDat[1], EchoLen[LoadID and $03]);

      {$IFDEF LOG}
      Writeln(FLog, Format('LoadEcho: Pos: %d; R: %d; St: %d; Cnt: %d', [FData.Position[StreamIdx] - 1, LoadID shr 6 and 1, ScanCh, LoadID and $03]));
      {$ENDIF}

      with FCurLoadEcho[StreamIdx, TRail(LoadID shr 6 and 1), ScanCh] do
      begin
      {  if ZerroFlag then
        begin
          Count:= 0;
          ZerroFlag:= False;
          ZerroFlag2:= False;
        end;
      }
        case LoadID and $03 of
        //  0: ZerroFlag:= True;
        {  0: begin
               if Count < 16 then Inc(Count);
               Delay[Count]:= FSrcDat[1];
               Ampl[Count]:= FSrcDat[2] shr 4 and $0F;
             end; }
          0: //if FSrcDat[1] <> $FF then
             begin
               if Count < 16 then Inc(Count);
               Delay[Count]:= FSrcDat[1];
               Ampl[Count]:= FSrcDat[2] shr 4 and $0F;
             end; // else ZerroFlag2:= True;

          1: begin
               if Count < 16 then Inc(Count);
               Delay[Count]:= FSrcDat[1];
               Ampl[Count]:= FSrcDat[3] shr 4 and $0F;
               if Count < 16 then Inc(Count);
               Delay[Count]:= FSrcDat[2];
               Ampl[Count]:= FSrcDat[3] and $0F;
             end;
          2: begin
               if Count < 16 then Inc(Count);
               Delay[Count]:= FSrcDat[1];
               Ampl[Count]:= FSrcDat[4] shr 4 and $0F;
               if Count < 16 then Inc(Count);
               Delay[Count]:= FSrcDat[2];
               Ampl[Count]:= FSrcDat[4] and $0F;
               if Count < 16 then Inc(Count);
               Delay[Count]:= FSrcDat[3];
               Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
             end;
          3: begin
               if Count < 16 then Inc(Count);
               Delay[Count]:= FSrcDat[1];
               Ampl[Count]:= FSrcDat[5] shr 4 and $0F;
               if Count < 16 then Inc(Count);
               Delay[Count]:= FSrcDat[2];
               Ampl[Count]:= FSrcDat[5] and $0F;
               if Count < 16 then Inc(Count);
               Delay[Count]:= FSrcDat[3];
               Ampl[Count]:= FSrcDat[6] shr 4 and $0F;
               if Count < 16 then Inc(Count);
               Delay[Count]:= FSrcDat[4];
               Ampl[Count]:= FSrcDat[6] and $0F;
             end;
        end;
      end;
    end
    else
    begin
      case LoadID of
  EID_AirBrushTempOff:  begin
                          FData.ReadBuffer(StreamIdx, FAirBrushTempOffState, 1);
                          FData.ReadBuffer(StreamIdx, FAirBrushTempOffDisCrd, 4);
                        end;
        EID_SysCrd_SS:  begin // ��������� ���������� �������� ������, �������� ����������
                          FData.ReadBuffer(StreamIdx, LoadByte[1], 2);
                          NewSysCoord:= Integer((Integer(FCurLoadSysCoord[StreamIdx]) and $FFFFFF00) or LoadByte[2]);

                          {$IFDEF LOG}
                          Writeln(FLog, Format('LoadCrd: %d', [NewSysCoord]));
                          {$ENDIF}
                        end;
        EID_SysCrd_SF:  begin // ��������� ���������� �������� ������, ������ ����������
                          FData.ReadBuffer(StreamIdx, LoadByte[1], 1);
                          FData.ReadBuffer(StreamIdx, LoadLongInt, 4);
                          NewSysCoord:= LoadLongInt[1];

                          {$IFDEF LOG}
                          Writeln(FLog, Format('LoadCrd: %d', [NewSysCoord]));
                          {$ENDIF}
                        end;
        EID_SysCrd_FS:  begin // ��������� ���������� ������ ������, �������� ����������
                          FData.ReadBuffer(StreamIdx, LoadByte[1], 5);
                          NewSysCoord:= Integer((Integer(FCurLoadSysCoord[StreamIdx]) and $FFFFFF00) or LoadByte[5]);

                          {$IFDEF LOG}
                          Writeln(FLog, Format('LoadCrd: %d', [NewSysCoord]));
                          {$ENDIF}
                        end;
        EID_SysCrd_FF:  begin // ��������� ���������� ������ ������, ������ ����������
                          FData.ReadBuffer(StreamIdx, LoadByte[1], 4);
                          FData.ReadBuffer(StreamIdx, LoadLongInt, 4);
                          NewSysCoord:= LoadLongInt[1];

                          {$IFDEF LOG}
                          Writeln(FLog, Format('LoadCrd: %d', [NewSysCoord]));
                          {$ENDIF}
                        end;
         EID_SysCrd_NS: begin
                          FData.ReadBuffer(StreamIdx, LoadByte[1], 1);
                          NewSysCoord:= Integer((Integer(FCurLoadSysCoord[StreamIdx]) and $FFFFFF00) or LoadByte[1]);
                          {$IFDEF LOG}
                          Writeln(FLog, Format('LoadCrd: %d', [NewSysCoord]));
                          {$ENDIF}
                        end;
         EID_SysCrd_NF: begin
                          FData.ReadBuffer(StreamIdx, LoadLongInt, 4);
                          NewSysCoord:= LoadLongInt[1];
                          {$IFDEF LOG}
                          Writeln(FLog, Format('LoadCrd: %d', [NewSysCoord]));
                          {$ENDIF}
                        end;

  EID_SysCrd_UMUA_Left_NF: begin // ������ ��������� ���������� ��� ������ ��� A, ����� �������
                              FData.ReadBuffer(StreamIdx, LoadLongInt, 4);
                              FCurLoadSysCoord_UMUA_Left:= LoadLongInt[1];
                            end;
  EID_SysCrd_UMUB_Left_NF: begin // ������ ��������� ���������� ��� ������ ��� �, ����� �������
                              FData.ReadBuffer(StreamIdx, LoadLongInt, 4);
                              FCurLoadSysCoord_UMUB_Left:= LoadLongInt[1];
                            end;
  EID_SysCrd_UMUA_Right_NF: begin // ������ ��������� ���������� ��� ������ ��� A, ������ �������
                              FData.ReadBuffer(StreamIdx, LoadLongInt, 4);
                              FCurLoadSysCoord_UMUA_Right:= LoadLongInt[1];
                            end;
  EID_SysCrd_UMUB_Right_NF: begin // ������ ��������� ���������� ��� ������ ��� �, ������ �������
                              FData.ReadBuffer(StreamIdx, LoadLongInt, 4);
                              FCurLoadSysCoord_UMUB_Right:= LoadLongInt[1];
                            end;

        EID_EndFile:    begin // ����� �����
                          Exit;
                        end;
        else SkipEvent(StreamIdx, LoadID, 0);
      end;

      if LoadID in [EID_SysCrd_SS, EID_SysCrd_SF, EID_SysCrd_FS, EID_SysCrd_FF, EID_SysCrd_NF, EID_SysCrd_NS] then
      begin
        (*
          UnPackEcho;

          FUnPackFlag[r_Left]:= False;
          FUnPackFlag[r_Right]:= False;

          if (FLoadPack[r_Left].Ampl <> $FF) then
          begin
          FCurEcho[r_Left, 1].Delay[1]:= FLoadPack[r_Left].Delay;
          FCurEcho[r_Left, 1].Ampl[1]:= FLoadPack[r_Left].Ampl;
          FCurEcho[r_Left, 1].Count:= 1;
          FCurEcho[r_Left, 1].ZerroFlag:= False;
          FUnPackFlag[r_Left]:= True;
          end;
          if (FLoadPack[r_Right].Ampl <> $FF) then
          begin
          FCurEcho[r_Right, 1].Delay[1]:= FLoadPack[r_Right].Delay;
          FCurEcho[r_Right, 1].Ampl[1]:= FLoadPack[r_Right].Ampl;
          FCurEcho[r_Right, 1].Count:= 1;
          FCurEcho[r_Right, 1].ZerroFlag:= False;
          FUnPackFlag[r_Right]:= True;
          end;
          *)
        FCurLoadBackMotion[StreamIdx]:= NewSysCoord < LastSysCoord;

        if Assigned(BlockOk) and (FCurLoadDisCoord[StreamIdx] >= StartDisCoord) and (FCurLoadDisCoord[StreamIdx] <= EndDisCoord) then
          if not BlockOk(StartDisCoord) then
            Break; // ��������� ������� ��� "������" ����������
        {
        for R:= FConfig.MinRail to FConfig.MaxRail do // ������� ������� ������� ��������
          for I:= FConfig.MinScanChNumber to FConfig.MaxScanChNumber do
            FCurLoadEcho[StreamIdx, R, I].ZerroFlag2:= False;
         }
        FCurLoadSysCoord[StreamIdx]:= NewSysCoord;

          // ���� ���� ������������� ������� ������� ������ �� ����� ����������


//////////////////////////////////////////////////////////////////////////////
              (*

          if (Abs(FCurLoadSysCoord[StreamIdx] - LastSysCoord) > 1) then // ���������� ���� �� ������ - ������ ���� ��������� �������
//            if ((FPack[rLeft].Ampl <> $FF) or (FPack[rRight].Ampl <> $FF)) then
            begin
              FSaveDisCoord:= FCurLoadDisCoord[StreamIdx];
              FSaveSysCoord:= FCurLoadSysCoord[StreamIdx];
   {           Move(CurEcho, CurEcho_, SizeOf(TCurEcho));
              FillChar(FCurEcho, SizeOf(TCurEcho), 0);

//              if (FPack[rLeft].Ampl <> $FF) then
              begin
                FCurEcho[rLeft, 1].Delay[1]:= 50; // FPack[rLeft].Delay;
                FCurEcho[rLeft, 1].Ampl[1]:= 12; // FPack[rLeft].Ampl;
                FCurEcho[rLeft, 1].Count:= 1;
                FCurEcho[rLeft, 1].ZerroFlag:= False;
                FUnPackFlag[rLeft]:= True;
              end;
//              if (FPack[rRight].Ampl <> $FF) then
              begin
                FCurEcho[rRight, 1].Delay[1]:= 50; // FPack[rRight].Delay;
                FCurEcho[rRight, 1].Ampl[1]:= 12; // FPack[rRight].Ampl;
                FCurEcho[rRight, 1].Count:= 1;
                FCurEcho[rRight, 1].ZerroFlag:= False;
                FUnPackFlag[rRight]:= True;
              end;     }

              if FCurLoadSysCoord[StreamIdx] > LastSysCoord then
              begin

                T:= FCurLoadDisCoord[StreamIdx];

                // if FCurSysCoord - LastSysCoord < 5000 then
                for J:= LastSysCoord + 1 to FCurLoadSysCoord[StreamIdx] - 1 do
                begin
                  Inc(FCurLoadDisCoord[StreamIdx]);
                  FCurLoadSysCoord[StreamIdx]:= J;
                  if Assigned(BlockOk) and
                    (FCurLoadDisCoord[StreamIdx] >= StartDisCoord - LoadShift) and
                    (FCurLoadDisCoord[StreamIdx] <= EndDisCoord) then
                    if not BlockOk(StartDisCoord) then Break;
                  if FCurLoadDisCoord[StreamIdx] >= EndDisCoord then Exit;
                end;

                // ToLog(Format('Unpack St: %d Ed: %d; UnPack: 1', [T + 1, FCurDisCoord]));

              end
              else
              begin

                T:= FCurLoadDisCoord[StreamIdx];

                // if LastSysCoord - FCurSysCoord < 5000 then
                for J:= LastSysCoord - 1 downto FCurLoadSysCoord[StreamIdx] + 1 do
                begin
                  Inc(FCurLoadDisCoord[StreamIdx]);
                  FCurLoadSysCoord[StreamIdx]:= J;
                  if Assigned(BlockOk) and (FCurLoadDisCoord[StreamIdx] >= StartDisCoord) and
                    (FCurLoadDisCoord[StreamIdx] <= EndDisCoord) then
                    if not BlockOk(StartDisCoord) then Break;
                  if FCurLoadDisCoord[StreamIdx] >= EndDisCoord then Exit;
                end;

                // ToLog(Format('Unpack St: %d Ed: %d; UnPack: 1', [T + 1, FCurDisCoord]));

              end;

           //   Move(CurEcho_, FCurEcho, SizeOf(TCurEcho));
              FCurLoadDisCoord[StreamIdx]:= FSaveDisCoord;
              FCurLoadSysCoord[StreamIdx]:= FSaveSysCoord;
            end;
                *)
//////////////////////////////////////////////////////////////////////////////

        Inc(FCurLoadDisCoord[StreamIdx], Abs(FCurLoadSysCoord[StreamIdx] - LastSysCoord)); // ������ ���������� ���������� (���������� ���� ��� �������� �����)
        LastSysCoord:= FCurLoadSysCoord[StreamIdx];
        LastDisCoord:= FCurLoadDisCoord[StreamIdx];

        GetParamNext(StreamIdx, FCurLoadDisCoord[StreamIdx], FCurLoadParams[StreamIdx], FCurLoadSensor1[StreamIdx], FCurLoadZPMode[StreamIdx]);
        if FCurLoadDisCoord[StreamIdx] >= EndDisCoord then Exit;

        P_:= @FCurLoadEcho[StreamIdx];
        FillChar(P_^, SizeOf(TCurEcho), 0);
      (*
        for R:= FConfig.MinRail to FConfig.MaxRail do // ������� ������� ������� ��������
        begin
          for I:= 0 to FConfig.ScanChannelCount - 1 do
          begin
            FCurLoadEcho[StreamIdx, R, I].Count:= 0;
           // FCurLoadEcho[StreamIdx, R, I].ZerroFlag:= True;
            //FCurLoadEcho[StreamIdx, R, I].ZerroFlag2:= False;
          end;
          // CurAmpl[R]:= - 1;
        end;
        *)
      end;

    end;
//    if SkipBytes <> -1 then
//      FData.Position[2]:= FData.Position[2] + SkipBytes;
  until FData.Position[StreamIdx] > FMaxReadOffset;
  {$IFDEF LOG}
  Writeln(FLog, 'LoadData Exit');
  {$ENDIF}
end;

function TAviconDataContainer.GetBMStateFirst(StreamIdx: Integer; DisCoord: Integer): Boolean;
var
  I: Integer;
  EndIdx: Integer;
  StartIdx: Integer;

begin
  Result:= False;
  FGetBMStateIdx[StreamIdx]:= 0;

  for I:= High(FCDList) downto 0 do
  begin
    if FCDList[I].DisCoord <= DisCoord then
    begin
      if FCDList[I].ID = EID_FwdDir then
      begin
        Result:= False;
        FGetBMStateIdx[StreamIdx]:= I + 1;
        Break;
      end;
      if FCDList[I].ID = EID_BwdDir { EID_FwdDirEnd } then
      begin
        Result:= True;
        FGetBMStateIdx[StreamIdx]:= I + 1;
        Break;
      end;
      Break;
    end;
  end;
  FGetBMStateState[StreamIdx]:= Result;
end;

function TAviconDataContainer.GetBMStateNext(StreamIdx: Integer; var DisCoord: Integer; var State: Boolean): Boolean;
var
  I: Integer;
  EndIdx: Integer;
  StartIdx: Integer;

begin
  Result:= False;
  State:= FGetBMStateState[StreamIdx];

  for I:= FGetBMStateIdx[StreamIdx] to High(FCDList) do
  begin
    if FCDList[I].ID = EID_FwdDir then
    begin
      State:= False;
      FGetBMStateIdx[StreamIdx]:= I + 1;
      FGetBMStateState[StreamIdx]:= False;
      DisCoord:= FCDList[I].DisCoord;
      Result:= I <> GetEventCount - 1;
      Break;
    end;
    if FCDList[I].ID = EID_BwdDir { EID_FwdDirEnd } then
    begin
      State:= True;
      FGetBMStateIdx[StreamIdx]:= I + 1;
      FGetBMStateState[StreamIdx]:= True;
      DisCoord:= FCDList[I].DisCoord;
      Result:= I <> GetEventCount - 1;
      Break;
    end;
  end;
end;

function TAviconDataContainer.GetEventIdx(StartDisCoord, EndDisCoord: Integer; var StartIdx, EndIdx: Integer; EmptyPar: Integer): Boolean;
var
  I: Integer;
  SameCoord1: TIntegerDynArray;
  SameCoord2: TIntegerDynArray;
  LeftIdx1: Integer;
  LeftIdx2: Integer;
  RightIdx1: Integer;
  RightIdx2: Integer;

begin
  GetNearestEventIdx(StartDisCoord, LeftIdx1, RightIdx1, SameCoord1);
  GetNearestEventIdx(EndDisCoord, LeftIdx2, RightIdx2, SameCoord2);

  if Length(SameCoord1) <> 0 then StartIdx:= SameCoord1[0]
                             else StartIdx:= LeftIdx1 + 1;

  if Length(SameCoord2) <> 0 then EndIdx:= SameCoord2[ High(SameCoord2)]
                             else EndIdx:= RightIdx2 - 1;
end;

procedure TAviconDataContainer.GetNearestEventIdx(DisCoord: Integer; var LeftIdx, RightIdx: Integer; var SameCoord: TIntegerDynArray);
var
  I: Integer;
  CLeft: Integer;
  CRight: Integer;
  CurrPos: Integer;

begin
  GetNearestEventIdxLeftRes:= False;
  GetNearestEventIdxRightRes:= False;
  if FEvents.Count = 0 then
  begin
    LeftIdx:= -1;
    RightIdx:= -1;
    SetLength(SameCoord, 0);
    Exit;
  end;

  CLeft:= FGetNearestEventIdx_StartIndex; // ������ � ������ � ������
  CRight:= FEvents.Count - 1; // ����� �������������� � �����
  repeat
    CurrPos:= (CLeft + CRight) div 2; // ���������� ��������
    begin
      if FEvents[CurrPos].Event.DisCoord < DisCoord then
      begin
        CLeft:= CurrPos; // ������� ����� �������� ����� ��� ����������� �������
        if (FEvents[FEvents.Count - 1].Event.DisCoord - FEvents[CurrPos].Event.DisCoord > 20000) then
          FGetNearestEventIdx_StartIndex:= CurrPos;
      end else CRight:= CurrPos;

      if (CLeft = CRight) or (Abs(CLeft - CRight) = 1) then
      // �������� �� ����� ������
      begin
        LeftIdx:= 0;
        RightIdx:= 0;
        for I:= CLeft downto 0 do
          if DisCoord = FEvents[I].Event.DisCoord then
          begin
            SetLength(SameCoord, Length(SameCoord) + 1);
            SameCoord[ High(SameCoord)]:= I;
            LeftIdx:= I;
            GetNearestEventIdxLeftRes:= True;
          end
          else
          begin
            if Length(SameCoord) <> 0 then LeftIdx:= SameCoord[0]
                                      else LeftIdx:= CLeft;
            GetNearestEventIdxLeftRes:= True;
            Break;
          end;

        for I:= CLeft + 1 to FEvents.Count - 1 do
          if DisCoord = FEvents[I].Event.DisCoord then
          begin
            SetLength(SameCoord, Length(SameCoord) + 1);
            SameCoord[ High(SameCoord)]:= I;
            RightIdx:= I;
            GetNearestEventIdxRightRes:= True;

          end
          else
          begin
            if Length(SameCoord) <> 0 then RightIdx:= SameCoord[ High(SameCoord)]
                                      else RightIdx:= I;
            GetNearestEventIdxRightRes:= True;
            Break;
          end;
        Exit;
      end;
    end;
  until False;
end;

function TAviconDataContainer.GetLeftEventIdx(DisCoord: Integer): Integer;
var
  LeftIdx, RightIdx: Integer;
  SameCoord: TIntegerDynArray;

begin
  GetNearestEventIdx(DisCoord, LeftIdx, RightIdx, SameCoord);
  if Length(SameCoord) <> 0 then Result:= SameCoord[High(SameCoord)]
                            else Result:= LeftIdx;
end;

function TAviconDataContainer.GetRightEventIdx(DisCoord: Integer): Integer;
var
  LeftIdx, RightIdx: Integer;
  SameCoord: TIntegerDynArray;

begin
  GetNearestEventIdx(DisCoord, LeftIdx, RightIdx, SameCoord);
  if Length(SameCoord) <> 0 then Result:= SameCoord[0]
                            else Result:= RightIdx;
end;

procedure TAviconDataContainer.GetEventData(Idx: Integer; var ID: Byte; var pData: PEventData);
begin
  if (Idx >= 0) and (Idx <= FEvents.Count - 1) then
  begin
    ID:= FEvents[Idx].Event.ID;
    pData:= FEvents.GetDataPtr(Idx);
  end;
end;

function TAviconDataContainer.GetParamFirst(StreamIdx: Integer; DisCoord: Integer; var Params: TEvalChannelsParams; var Sensor1: TSensor1Data; var ZPMode: TZerroProbMode): Boolean;
{
  var
  StartIdx, EndIdx: Integer;
  }
begin
  {$IFDEF LOG}
  Writeln(FLog, Format('GetParamFirst', []));
  {$ENDIF}
  DisCoord:= NormalizeDisCoord(DisCoord);
  if DisCoord <= 0 then  DisCoord:= 1;

  FGetParamIdx[StreamIdx]:= GetLeftEventIdx(DisCoord);
  if FGetParamIdx[StreamIdx] <> -1 then
  begin
    Params:= FEvents[FGetParamIdx[StreamIdx]].EvalChPar;
    Sensor1:= FEvents[FGetParamIdx[StreamIdx]].Sensor1;
    ZPMode:= FEvents[FGetParamIdx[StreamIdx]].ZerroProbMode;
  end;
end;

function TAviconDataContainer.GetParamNext(StreamIdx: Integer; DisCoord: Integer; var Params: TEvalChannelsParams; var Sensor1: TSensor1Data; var ZPMode: TZerroProbMode): Boolean;
var
  I: Integer;
  StartIdx, EndIdx: Integer;

begin
  {$IFDEF LOG}
  Writeln(FLog, Format('GetParamNext', []));
  {$ENDIF}
  if FGetParamIdx[StreamIdx] = - 1 then Exit;
  // DisCoord:= NormalizeDisCoord(DisCoord);
  // GetEventIdx(DisCoord, DisCoord, StartIdx, EndIdx);
  // FGetParamIdx:= EndIdx;

  for I:= FGetParamIdx[StreamIdx] to FEvents.RealCount do
  begin
    if DisCoord = FEvents[I].Event.DisCoord then
    begin
    //  Params:= FEvents.Param[I];
      Params:= FEvents[I].EvalChPar;
      Sensor1:= FEvents[I].Sensor1;
      ZPMode:= FEvents[I].ZerroProbMode;

      FGetParamIdx[StreamIdx]:= I;
      Break;
    end;
    if DisCoord < FEvents[I].Event.DisCoord then
    begin
//      Params:= FEvents.Param[I - 1];
      Params:= FEvents[I - 1].EvalChPar;
      Sensor1:= FEvents[I - 1].Sensor1;
      ZPMode:= FEvents[I - 1].ZerroProbMode;
      FGetParamIdx[StreamIdx]:= I;
      Break;
    end;
  end;
end;

// -------------< ������� ��������� - ����������� �� >-----------------------------------

function TAviconDataContainer.DisToMRFCrdPrm(DisCoord: Integer; var CoordParams: TMRFCrdParams): Boolean;
var
  ID: Byte;
  I, J: Integer;
  pData: PEventData;
  StartIdx: Integer;
  MMLen: Integer;
  LeftFlg: Boolean;
  RightFlg: Boolean;
  Post: TMRFCrd;

begin
  DisCoord:= NormalizeDisCoord(DisCoord);

                                      // ����� ������ �������������� ����� �� DisCoord
  LeftFlg:= False;
  for I:= GetLeftEventIdx(DisCoord) downto 0 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_Stolb then
    begin
      CoordParams.LeftStolb[ssLeft].Km:= pCoordPost(pData)^.Km[0];
      CoordParams.LeftStolb[ssLeft].Pk:= pCoordPost(pData)^.Pk[0];
      CoordParams.LeftStolb[ssRight].Km:= pCoordPost(pData)^.Km[1];
      CoordParams.LeftStolb[ssRight].Pk:= pCoordPost(pData)^.Pk[1];
      CoordParams.ToLeftStolbMM:= (DisToSysCoord(DisCoord) - FEvents[I].Event.SysCoord) * FHead.ScanStep div 100;
      CoordParams.LeftIdx:= I;
      LeftFlg:= True;
      Break;
    end;
  end;
                                              // ����� ������ �������������� ������ �� DisCoord
  RightFlg:= False;
  for I:= GetRightEventIdx(DisCoord) to FEvents.Count - 1 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_Stolb then
    begin
      CoordParams.RightStolb[ssLeft].Km:= pCoordPost(pData)^.Km[0];
      CoordParams.RightStolb[ssLeft].Pk:= pCoordPost(pData)^.Pk[0];
      CoordParams.RightStolb[ssRight].Km:= pCoordPost(pData)^.Km[1];
      CoordParams.RightStolb[ssRight].Pk:= pCoordPost(pData)^.Pk[1];
      CoordParams.ToRightStolbMM:= (FEvents[I].Event.SysCoord - DisToSysCoord(DisCoord)) * FHead.ScanStep div 100;
      CoordParams.RightIdx:= I;
      RightFlg:= True;
      Break;
    end;
  end;

  if not LeftFlg then // ��� ������ ����� - ��������� �� ��������� ��������� � ������
  begin
    Post.Km:= FHead.StartKM;
    Post.Pk:= FHead.StartPk;
    Post:= GetPrevMRFPost(Post, FHead.MoveDir);
    CoordParams.LeftStolb[ssLeft].Km:= Post.Km;
    CoordParams.LeftStolb[ssLeft].Pk:= Post.Pk;
    CoordParams.LeftStolb[ssRight].Km:= FHead.StartKM;
    CoordParams.LeftStolb[ssRight].Pk:= FHead.StartPk;
    if FHead.MoveDir > 0 then CoordParams.ToLeftStolbMM:= FHead.StartMetre * 1000 + Trunc( DisToSysCoord(DisCoord) * (FHead.ScanStep / 100) )
                         else CoordParams.ToLeftStolbMM:= (100 - FHead.StartMetre) * 1000 + Trunc( DisToSysCoord(DisCoord) * (FHead.ScanStep / 100) );
    CoordParams.LeftIdx:= - 1;
  end;
  if not RightFlg then // ��� ������ ������ - ��������� �� ��������� �������� ��������� � ������
  begin
    CoordParams.RightStolb[ssLeft].Km:= FLastMRFPost.Km;
    CoordParams.RightStolb[ssLeft].Pk:= FLastMRFPost.Pk;
    Post.Km:= FLastMRFPost.Km;
    Post.Pk:= FLastMRFPost.Pk;
    Post:= GetNextMRFPost(Post, FHead.MoveDir);
    CoordParams.RightStolb[ssRight].Km:= Post.Km;
    CoordParams.RightStolb[ssRight].Pk:= Post.Pk;
    CoordParams.ToRightStolbMM:= 100000 - CoordParams.ToLeftStolbMM; // ���� ��� ������ ������ ������ ��� ����� ������ ����� 100 ������
    CoordParams.RightIdx:= - 1;
  end;
end;

function TAviconDataContainer.DisToMRFCrd(DisCoord: Longint): TMRFCrd;
var
  CoordParams: TMRFCrdParams;

begin
  DisToMRFCrdPrm(DisCoord, CoordParams);
  Result:= MRFCrdParamsToCrd(CoordParams, Header.MoveDir);
end;

function TAviconDataContainer.MRFCrdToDisCrd(RFC: TMRFCrd; var DisCoord: Longint): Boolean;
var
  I, J: Integer;
  ID: Byte;
  pData: PEventData;
  Len: Integer;

begin
  if (FHead.MoveDir > 0) then
  begin
    if (FHead.StartKM = RFC.Km) and
       (FHead.StartPk = RFC.Pk) then
    begin
      Result:= True;
      DisCoord:= SysToDisCoord(Round((RFC.mm - FHead.StartMetre * 1000) / (FHead.ScanStep / 100)));
      Exit;
    end;

    for I:= 0 to EventCount - 1 do
    begin
      GetEventData(I, ID, pData);
      if (ID = EID_Stolb) and
         (pCoordPost(pData)^.Km[1] = RFC.Km) and
         (pCoordPost(pData)^.Pk[1] = RFC.Pk) then
      begin
        Result:= True;
        DisCoord:= SysToDisCoord(DisToSysCoord(Event[I].DisCoord) + Round(RFC.mm / (FHead.ScanStep / 100)));
        Exit;
      end;
    end;
  end
  else
  begin
    if (FHead.StartKM = RFC.Km) and
       (FHead.StartPk = RFC.Pk) then
    begin

      for I:= 0 to EventCount - 1 do // ���� �� ������ ?
      begin
        GetEventData(I, ID, pData);
        if (ID = EID_Stolb) then             // ����
        begin
          if (pCoordPost(pData)^.Km[0] = RFC.Km) and  //
             (pCoordPost(pData)^.Pk[0] = RFC.Pk) then
          begin
            Len:= 0;
            DisCoord:= SysToDisCoord(DisToSysCoord(Event[I].DisCoord) - Round((RFC.mm + Len) / (FHead.ScanStep / 100)));
            Exit;
          end;
        end;
      end;

      Result:= True;
      DisCoord:= SysToDisCoord(Round((FHead.StartMetre * 1000 - RFC.mm) / (FHead.ScanStep / 100)));
      Exit;
    end;

    for I:= 0 to EventCount - 1 do
    begin
      GetEventData(I, ID, pData);
      if (ID = EID_Stolb) and
         (pCoordPost(pData)^.Km[1] = RFC.Km) and
         (pCoordPost(pData)^.Pk[1] = RFC.Pk) then
      begin

        for J:= I + 1 to EventCount - 1 do // ���� �� ������ ����� ?
        begin
          GetEventData(J, ID, pData);
          if ID = EID_Stolb then           // ���� ����� ����������� �� ����
          begin
            Result:= True;
            DisCoord:= SysToDisCoord(DisToSysCoord(Event[J].DisCoord) - Round(RFC.mm / (FHead.ScanStep / 100)));
            Exit;
          end;
        end;
                                           // ���, ����� ������� ��� ����� ������ 100 �
        Result:= True;
        DisCoord:= SysToDisCoord(DisToSysCoord(Event[I].DisCoord) + Round((100000 - RFC.mm) / (FHead.ScanStep / 100)));
        Exit;
      end;
    end;
  end;
end;

  // -------------< ������� ��������� - ��������� >----------------------------------------

function TAviconDataContainer.DisToCaCrdPrm(DisCoord: Integer; var CoordParams: TCaCrdParams): Boolean;
var
  StartIdx: Integer;
  MMLen: Integer;
  ID: Byte;
  I, J: Integer;
  LeftFlg: Boolean;
  RightFlg: Boolean;
  pData: PEventData;

begin
  CoordParams.Sys:= CoordSys;
                                    // ����� ������ �������������� ����� �� DisCoord
  LeftFlg:= False;
  for I:= GetLeftEventIdx(DisCoord) downto 0 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_StolbChainage then
    begin
      CoordParams.LeftPostVal:= pCoordPostChainage(pData)^.Val;
      CoordParams.ToLeftPostMM:= (DisToSysCoord(DisCoord) - FEvents[I].Event.SysCoord) * FHead.ScanStep div 100;
      CoordParams.LeftIdx:= I;
      LeftFlg:= True;
      Break;
    end;
  end;
                                    // ����� ������ �������������� ������ �� DisCoord
  RightFlg:= False;
  for I:= GetRightEventIdx(DisCoord) to FEvents.Count - 1 do
  begin
    GetEventData(I, ID, pData);
    if ID = EID_StolbChainage then
    begin
      CoordParams.RightPostVal:= pCoordPostChainage(pData)^.Val;
      CoordParams.ToRightPostMM:= (FEvents[I].Event.SysCoord - DisToSysCoord(DisCoord)) * FHead.ScanStep div 100;
      CoordParams.RightIdx:= I;
      RightFlg:= True;
      Break;
    end;
  end;

  if not LeftFlg then // ��� ������ ����� - ��������� �� ��������� �������
  begin
    if FHead.MoveDir > 0 then
    begin
      CoordParams.LeftPostVal:= GetPrevCaPost(CoordSys,
                                              FHead.StartChainage,
                                              FHead.MoveDir); // �������� ����� ������� ��� �����
      CoordParams.ToLeftPostMM:= GetMMLenFromCaPost(CoordSys,
                                                    FHead.StartChainage) +
                                 Trunc(DisToSysCoord(DisCoord) * (FHead.ScanStep / 100));

      //      ���������
      //        �����                  StartChainage            DisCoord
      //          |                         |                       |
      //     ---- X <---------------------> X <-------------------> X ------------
      //          |     MMLenFromCaPost        DisToSys * ScanStep  |
      //          |                                                 |
      //          x <---------------- ToLeftPostMM ---------------> X

    end
    else
    begin
      CoordParams.LeftPostVal:= FHead.StartChainage; // �������� ����� ������� ��� �����
      CoordParams.LeftPostVal.YYY:= 0;
      CoordParams.LeftPostVal.XXX:= CoordParams.LeftPostVal.XXX + 1;

      CoordParams.ToLeftPostMM:= PostMMLen(CoordSys) -
                                 GetMMLenFromCaPost(CoordSys, FHead.StartChainage) +
                                 Trunc( DisToSysCoord(DisCoord) * (FHead.ScanStep / 100) );

      //      LeftPostVal             StartChainage            DisCoord         RightPostVal
      //          |                         |                       |                |
      //     -----|-------------------------|-----------------------|----------------|-------
      //                                    |<- DisToSys*ScanStep ->|
      //                                    |<----------- MMLenFromCaPost ---------->|
      //          |<------------------------------ PostMMLen ----------------------->|
      //          |<----------------- ������� �������� ------------>|

    end;
    CoordParams.LeftIdx:= - 1;
  end;

  if not RightFlg  then // ��� ������ ������
  begin
    if FHead.MoveDir > 0 then
    begin
      CoordParams.RightPostVal:= CoordParams.LeftPostVal;  // �������� ����� ������
      CoordParams.RightPostVal.XXX:= CoordParams.RightPostVal.XXX + 1;
      CoordParams.RightPostVal.YYY:= 0;
      CoordParams.ToRightPostMM:= PostMMLen(CoordSys) - CoordParams.ToLeftPostMM;

      //
      //        �����                    DisCoord               EndCaCrd         RightPostVal
      //          |                         |                       |                  |
      //     ---- x <---------------------> x-----------------------x------------------x----
      //          |     ToLeftPostMM        |                                          |
      //          x <--------------------------- PostMMLen --------------------------> x
      //                                    |                                          |
      //                                    x <------ PostMMLen - ToLeftPostMM ------> x

    end
    else
    begin
      CoordParams.RightPostVal:= CoordParams.LeftPostVal;
      CoordParams.RightPostVal.YYY:= 0;
      if CoordParams.RightPostVal.YYY = 0 then Dec(CoordParams.RightPostVal.XXX)
                                          else CoordParams.RightPostVal.YYY:= 0;
      CoordParams.ToRightPostMM:= PostMMLen(CoordSys) - CoordParams.ToLeftPostMM;
    end;
    CoordParams.RightIdx:= - 1;
  end;
end;

function TAviconDataContainer.DisToCaCrd(DisCoord: Longint): TCaCrd;
var
  CoordParams: TCaCrdParams;

begin
  DisToCaCrdPrm(DisCoord, CoordParams);
  Result:= CaCrdParamsToCrd(CoordParams, Header.MoveDir);
end;

function TAviconDataContainer.isTwoWheels: Boolean;
begin
  Result:= isEGOUS_WheelScheme1 or isEGOUS_WheelScheme2 or isVMTUS_Scheme_1 or isVMTUS_Scheme_2;
end;

function TAviconDataContainer.isVMTUS_Scheme_1: Boolean; // �������
begin
  Result:= CompareMem(@FHead.DeviceID, @EGO_USWScheme1, 8);
end;

function TAviconDataContainer.isVMTUS_Scheme_2: Boolean; // �������
begin
  Result:= CompareMem(@FHead.DeviceID, @EGO_USWScheme2, 8);
end;

function TAviconDataContainer.isVMTUS_Scheme_4: Boolean; // ����
begin
  Result:= CompareMem(@FHead.DeviceID, @VMT_US_BigWP, 8);
end;

function TAviconDataContainer.isVMTUS_Scheme_5: Boolean; // ������
begin
  Result:= CompareMem(@FHead.DeviceID, @VMT_US_BigWP_N2, 8);
end;

function TAviconDataContainer.isVMTUS: Boolean; // ����� VMT ��������� ��� �� windows
begin
  Result:= isVMTUS_Scheme_1 or isVMTUS_Scheme_2 or isVMTUS_Scheme_4 or isVMTUS_Scheme_5 or isVMTUSW;
end;

function TAviconDataContainer.isVMTUSW: Boolean; // Linux
begin
  Result:= CompareMem(@FHead.DeviceID, @VMT_USW, 8);
end;

function TAviconDataContainer.isEGOUS_Scheme1: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon16Scheme1, 8);
end;

function TAviconDataContainer.isEGOUS_Scheme2: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon16Scheme2, 8);
end;

function TAviconDataContainer.isEGOUS_Scheme3: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon16Scheme3, 8);
end;

function TAviconDataContainer.isEGOUS_WheelScheme1: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon16Wheel, 8);
end;

function TAviconDataContainer.isEGOUS_WheelScheme2: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @Avicon16Wheel2, 8);
end;

function TAviconDataContainer.isEGOUS: Boolean;
begin
  Result:= isEGOUS_Scheme1 or isEGOUS_Scheme2 or isEGOUS_Scheme3 or isEGOUS_WheelScheme1 or isEGOUS_WheelScheme2;
end;

function TAviconDataContainer.isFilus_X17DW: Boolean;
begin
  Result:= CompareMem(@FHead.DeviceID, @FilusX17DW, 8);
end;

function TAviconDataContainer.isVMTUS_BHead: Boolean;
begin
  Result:= isVMTUS and (Header.ControlDirection = cd_Head_B_Tail_A);
end;

function TAviconDataContainer.CaCrdToDis(CaCrd: TCaCrd; var DisCoord: Longint): Boolean;
var
  ID: Byte;
  I, J, Sys, Len: Integer;
  pData: PEventData;
  Val1, Val2: TCaCrd;
  DisCrd: Integer;
  t1: int64;
  t2: int64;
  t3: int64;

begin
  if (FHead.MoveDir > 0) then
  begin
    if CaCrd.XXX < FHead.StartChainage.XXX then // ���������� ������ ��������� ���������� �����
    begin
      DisCoord:= SysToDisCoord(Round((CaCrdToMM(CoordSys, CaCrd) - CaCrdToMM(CoordSys, FHead.StartChainage)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

    Val1:= CaCrd;
    Val1.XXX:= 0;

    DisCrd:= 0;                                 // ���������� ����� ��������� ���������� �����
    Val2:= FHead.StartChainage;
    if FHead.StartChainage.XXX = CaCrd.XXX then
    begin
      Val2.XXX:= 0;
      DisCoord:= SysToDisCoord(Round((CaCrdToMM(CoordSys, Val1) - CaCrdToMM(CoordSys, Val2)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

    for I:= 0 to EventCount - 1 do              // ���� ����� �������� "�����" �������� ����������
    begin
      GetEventData(I, ID, pData);
      if ID = EID_StolbChainage then
      begin
        Val2:= pCoordPostChainage(pData)^.Val;
        DisCrd:= Event[I].DisCoord;
        if Val2.XXX = CaCrd.XXX then
        begin
          Val2.XXX:= 0;
          DisCoord:= DisCrd + SysToDisCoord(Round((CaCrdToMM(CoordSys, Val1) - CaCrdToMM(CoordSys, Val2)) / (FHead.ScanStep / 100)));
          Result:= True;
          Exit;
        end;
      end;
    end;

    if CaCrd.XXX > Val2.XXX then // ���������� ������ ���������� ���������� ������
    begin
      Val1:= CaCrd;
      DisCoord:= SysToDisCoord(DisToSysCoord(DisCrd) + Round((CaCrdToMM(CoordSys, Val1) - CaCrdToMM(CoordSys, Val2)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

  end
  else
  begin

    if CaCrd.XXX > FHead.StartChainage.XXX then // ���������� ������ ��������� ���������� �����
    begin
      DisCoord:= SysToDisCoord(Round((CaCrdToMM(CoordSys, FHead.StartChainage) - CaCrdToMM(CoordSys, CaCrd)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

    Val1:= CaCrd;
    Val1.XXX:= 0;

    DisCrd:= 0;                                 // ���������� ����� ��������� ���������� �����
    Val2:= FHead.StartChainage;
    if FHead.StartChainage.XXX = CaCrd.XXX then
    begin
      Val2.XXX:= 0;
      DisCoord:= SysToDisCoord(Round((CaCrdToMM(CoordSys, Val2) - CaCrdToMM(CoordSys, Val1)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

    for I:= 0 to EventCount - 1 do              // ���� ����� �������� "�����" �������� ����������
    begin
      GetEventData(I, ID, pData);
      if ID = EID_StolbChainage then
      begin
        Val2:= pCoordPostChainage(pData)^.Val;
        DisCrd:= Event[I].DisCoord;
        if Val2.XXX = CaCrd.XXX + 1 then
        begin

          for J:= I + 1 to EventCount - 1 do // ���� �� ������ ����� ?
          begin
            GetEventData(J, ID, pData);
            if ID = EID_StolbChainage then           // ���� ����� ����������� �� ����
            begin
              Result:= True;
              Sys:= Round(CaCrdToMM(CoordSys, Val1) / (FHead.ScanStep / 100));
              DisCoord:= SysToDisCoord(DisToSysCoord(Event[J].DisCoord) - Sys);
              Result:= True;
              Exit;
            end;
          end;
                                             // ���, ����� ������� ��� ����� �������� PostMMLen ��
          Val2.XXX:= 0;
          Sys:= Round((PostMMLen(CoordSys) - CaCrdToMM(CoordSys, Val1)) / (FHead.ScanStep / 100));
          DisCoord:= SysToDisCoord(DisToSysCoord(DisCrd) + Sys);
          Result:= True;
          Exit;
        end;
      end;
    end;

    if CaCrd.XXX < Val2.XXX then // ���������� ������ ���������� ���������� ������
    begin
      Val1:= CaCrd;
      DisCoord:= DisCrd + SysToDisCoord(Round((CaCrdToMM(CoordSys, Val2) - CaCrdToMM(CoordSys, Val1)) / (FHead.ScanStep / 100)));
      Result:= True;
      Exit;
    end;

  end;
  Result:= False;
end;

initialization

//  ConfigList:= TDataFileConfigList.Create(''); 1

end.

