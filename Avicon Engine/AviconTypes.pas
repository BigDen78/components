﻿//{$I DEF.INC}!
unit AviconTypes;

interface

uses
  Types, Windows, SysUtils, Classes, CfgTablesUnit_2010;

//{$DEFINE VMT_EGO}

type
  TAviconID = array [0..7] of Byte;

const

// 0 48  1 49  2 50  3 51  4 52  5 53  6 54  7 55  8 56  9 57
// A 65  B 66  C 67  D 68  E 69  F 70  G 71  H 72  I 73  J 74  K 75  L 76
// M 77  N 78  O 79  P 80  Q 81  R 82  S 83  T 84  U 85  W 86  X 87  Y 88 Z 89

    // Заголовок файла
     AviconFileID   : TAviconID = ($52, $53, $50, $42, $52, $41, $46, $44); // Идентификатор файла [RSPBRAFD]

    // Тележки MAV
     USK003R        : TAviconID = ($DE, $FE, $55, $53, $4B, $33, $52, $01); // Авикон-12 Для Венгрии - старая версия (макет)
     USK004R        : TAviconID = ($DE, $FE, $55, $53, $4B, $34, $52, $01); // Авикон-12 Для Венгрии

    // Клюшки Радиоавионика
     Avicon15Scheme1: TAviconID = ($DE, $FE, $41, $31, $35, $53, $31, $01); // Авикон-15 схема 1
   Avicon15N8Scheme1: TAviconID = ($DE, $FE, $41, $31, $35, $4E, $31, $01); // Авикон-15 (Nautiz X8)

    // Тележки Радиоавионика / GEISMAR - Filus X27W
     Avicon14Scheme1: TAviconID = ($DE, $FE, $41, $31, $34, $53, $31, $01); // Авикон-14 скольжение
     Avicon14Wheel  : TAviconID = ($DE, $FE, $41, $31, $34, $57, $31, $01); // Авикон-14 КИС схема 1:
     Avicon14Wheel2 : TAviconID = ($DE, $FE, $41, $31, $34, $57, $32, $01); // Авикон-14 КИС схема 2: 6x65° , 2x42°, 2x0° (КИС 2шт)

    // Самоходные тележки GEISMAR - EGO US(W)
     Avicon16Scheme1: TAviconID = ($DE, $FE, $41, $31, $36, $53, $31, $01); // Авикон-16, БРы схема 1: 4х58°х34°, 2х70°, 2х42°, 0° (Windows)
     Avicon16Scheme2: TAviconID = ($DE, $FE, $41, $31, $36, $53, $32, $01); // Авикон-16, БРы схема 2: 2x58°х34°, 2x58°х34° ЗРК, 2х70°, 2х42, 0° (Windows)
     Avicon16Scheme3: TAviconID = ($DE, $FE, $41, $31, $36, $53, $33, $01); // Авикон-16, БРы схема 3: 4х58°х34°, 70°, 2x58°х34° ЗРК, 2х42°, 0° (Windows)
      Avicon16Scheme4: TAviconID = ($DE, $FE, $41, $31, $36, $53, $34, $01); // Авикон-16, БРы схема 4: 4х58°х34°, 2х70°, 2х42°, 0° Тоже что и в Авикон-31 (Linux)
     Avicon16Wheel  : TAviconID = ($DE, $FE, $41, $31, $36, $57, $31, $01); // Авикон-16, КИС схема 1: 4x58°х34°, 2x65°, 2x42°, 2x0° (КИС 2шт) (Windows)
     Avicon16Wheel2 : TAviconID = ($DE, $FE, $41, $31, $36, $57, $32, $01); // Авикон-16, КИС схема 2: 6x65° , 2x42°, 2x0° (КИС 2шт) (Windows)
      Avicon16Wheel3 : TAviconID = ($DE, $FE, $41, $31, $36, $57, $33, $01); // Авикон-16, КИС схема 3: 6x65° , 2x42°, 2x55°,0° (КИС 1шт) Тоже что и в Filus X17-DW, только 2 нити (Linux)

    // Дрезина GEISMAR - VMT US
     EGO_USWScheme1 : TAviconID = ($DE, $FE, $41, $32, $36, $57, $31, $01); // Авикон-26, КИС схема 1: 4x58°х34°, 2x65°, 2x42°, 2x0° (КИС 2шт) - Бангкок (Windows)
     EGO_USWScheme2 : TAviconID = ($DE, $FE, $41, $32, $36, $57, $32, $01); // Авикон-26, КИС схема 2: 6x65°, 2x42°, 2x0° (КИС 2шт) - Бангкок (Windows)
     EGO_USWScheme3 : TAviconID = ($DE, $FE, $41, $32, $36, $57, $33, $01); // Авикон-26, КИС схема 3: - не используется
     VMT_US_BigWP   : TAviconID = ($DE, $FE, $41, $32, $36, $57, $34, $01); // Авикон-26, КИС схема 4: 6x65°, 2x45°x90°, 2x42°, 0° (КИС 1шт) - Каир (Windows)
     VMT_US_BigWP_N2: TAviconID = ($DE, $FE, $41, $32, $36, $57, $35, $01); // Авикон-26, КИС схема 5: 6x65°, 2x55°x90°, 2x42°, 0° (КИС 1шт) - Нагпур (Windows)
     VMT_USW        : TAviconID = ($DE, $FE, $41, $32, $36, $57, $36, $01); // Авикон-26, КИС схема 5, вариант №2: 6x65°, 2x55°x90°, 2x42°, 0° (КИС 1шт) (Linux) //$36, $4C, $35, $01

    // Тележки Радиоавионика
     Avicon31Scheme1: TAviconID = ($DE, $FE, $41, $33, $31, $53, $31, $01); // Авикон-31 схема 1
     Avicon31Scheme2: TAviconID = ($DE, $FE, $41, $33, $31, $53, $32, $01); // Авикон-31 схема 2
     Avicon31Scheme3: TAviconID = ($DE, $FE, $41, $33, $31, $53, $33, $01); // Авикон-31 схема 3 (22°)
     Avicon31Estonia: TAviconID = ($DE, $FE, $41, $33, $31, $45, $31, $01); // Авикон-31 Эстония  схема 1:  2x70°, 4x58°/34°, 2x42°, 0° (Linux)
     Avicon31Estonia2: TAviconID = ($DE, $FE, $41, $33, $31, $45, $32, $01); // Авикон-31 Эстония схема 2:  2x70°, 4x70°/10°, 2x42°, 0° (Linux)

     MDLWheel: TAviconID = ($DE, $FE, $4D, $44, $4C, $53, $31, $01);              // МДЛ: 2x70°, 4x58°, 2x42°, 2x55°, 0° (КИС 2шт)  (Linux)
     MDLWheel2: TAviconID = ($DE, $FE, $4D, $44, $4C, $53, $32, $01);              // МДЛ: 2x70°, 4x58°, 2x42°, 2x55°, 0° (КИС 2шт)  (Linux)

    // Однониточная тележка GEISMAR
     FilusX17DW     : TAviconID = ($DE, $FE, $58, $31, $31, $31, $57, $01); // Filus X17-DW

    // Двухниточная тележка GEISMAR
     FilusX27MWScheme1: TAviconID = ($DE, $FE, $58, $32, $37, $4D, $31, $01); // Filus X27MW  (Авикон-31 схема 1) БРы схема 1: 4х58°х34°, 2х70°, 2х42°, 0°
     FilusX27MW       : TAviconID = ($DE, $FE, $58, $32, $37, $4D, $57, $01); // Filus X27MW  (Авикон-31 Geismar)

    // Прочее
     AviconHandScan : TAviconID = ($FF, $FF, $00, $00, $00, $00, $00, $00); // Данные ручного канала

     Avicon17       : TAviconID = ($DE, $FE, $41, $31, $37, $30, $30, $01); // Авикон-17
     MIGReg         : TAviconID = ($DE, $FE, $4D, $49, $47, $30, $30, $00); // МИГ
     MIGView        : TAviconID = ($DE, $FE, $4D, $49, $47, $30, $31, $00); // МИГ

// ---- Не используются ------------------------

//  FilusX27Wheel  : TAviconID = ($DE, $FE, $46, $32, $37, $57, $30, $01); // FilusX27 колесная схема
    Avicon11Scheme0: TAviconID = ($DE, $FE, $41, $31, $31, $53, $30, $01); // Авикон-11 схема 0
    RKS_U_Scheme_5: TAviconID = ($DE, $FE, $52, $4B, $53, $55, $31, $01); // РКС-У схема 1 (5 пластин)
    RKS_U_Scheme_8: TAviconID = ($DE, $FE, $52, $4B, $53, $55, $32, $01); // РКС-У схема 2 (8 пластин)

  cd_Tail_B_Head_A = 0; // 0 - B >>> A (по ходу / головой вперед)
  cd_Head_B_Tail_A = 1; // 1 - B <<< A (против хода / головой назад)

const

  // ------------------------< Идентификаторы событий файла >---------------------------------------

  EID_HandScan = $82; // Ручник
  EID_Ku = $90; // Изменение условной чувствительности
  EID_Att = $91; // Изменение аттенюатора
  EID_TVG = $92; // Изменение ВРЧ
  EID_StStr = $93; // Изменение положения начала строба
  EID_EndStr = $94; // Изменение положения конца строба
  EID_HeadPh = $95; // Список включенных наушников
  EID_Mode = $96; // Изменение режима
  EID_SetRailType = $9B; // Настройка на тип рельса
  EID_PrismDelay = $9C; // Изменение 2Тп (word)
  EID_Stolb = $A0; // Отметка координаты
  EID_Switch = $A1; // Номер стрелочного перевода
  EID_DefLabel = $A2; // Отметка дефекта
  EID_TextLabel = $A3; // Текстовая отметка
  EID_StBoltStyk = $A4; // Нажатие кнопки БС
  EID_EndBoltStyk = $A5; // Отпускание кнопки БС
  EID_Time = $A6; // Отметка времени
  EID_StolbChainage = $A7; // Отметка координаты Chainage
  EID_ZerroProbMode = $A8; // Режим работы датчиков 0 град
  EID_LongLabel = $A9; // Протяженная отметка

////////////////////////////////////////
  EID_SpeedState = $AA; // Скорость и Превышение скорости контроля
  EID_ChangeOperatorName = $AB; // Смена оператора (ФИО полностью)
  EID_AutomaticSearchRes = $AC; // «Значимые» участки рельсов, получаемые при автоматическом поиске
  EID_TestRecordFile = $AD; // Файл записи контрольного тупика
  EID_OperatorRemindLabel = $AE; // Отметки пути, заранее записанные в прибор для напоминания оператору (дефектные рельсы и другие)
  EID_QualityCalibrationRec = $AF;	// Качество настройки каналов контроля
////////////////////////////////////////

  EID_Sensor1 = $B0; // Сигнал датчика БС и Стрелки
  EID_AirBrush = $B1; // Краскоотметчик
  EID_PaintMarkRes = $B2; // Сообщение о выполнении задания на краско-отметку
  EID_AirBrushTempOff = $B3; // Временное отключение Краскоотметчика
  EID_AirBrush2 = $B4; // Краскоотметчик с отладочной информацией
  EID_AlarmTempOff = $B5; // Временное отключение АСД по всем каналам

////////////////////////////////////////
  EID_StartSwitchShunter = $B6;  // Начало зоны стрелочного перевода
  EID_EndSwitchShunter = $B7;	 // Конец зоны стрелочного перевода
  EID_Temperature = $B8; // Температура
  EID_DebugData = $B9; // Отладочная информация
  EID_PaintSystemParams = $BA; // Параметры работы алгоритма краскопульта
  EID_UMUPaintJob = $BB; // Задание БУМ на краскоотметку
  EID_ACData = $BC; // Данные акустического контакта
////////////////////////////////////////

  EID_SmallDate = $BD; // Малое универсальное событие
  EID_RailHeadScaner = $BE; // Данные уточняющего сканера головки рельса
  EID_SensAllowedRanges = $BF; // Таблица разрешенных диапазонов Ку

  EID_GPSCoord = $C0; // Географическая координата
  EID_GPSState = $C1; // Состояние приемника GPS
  EID_Media = $C2; // Медиаданные
  EID_GPSCoord2 = $C3; // Географическая координата со скоростью
  EID_NORDCO_Rec = $C4; // Запись NORDCO
  EID_SensFail = $C5;   // Отклонение условной чувствительности от нормативного значения

  EID_AScanFrame = $C6; // Кадр А-развертки
  EID_MediumDate = $C7; // Среднее универсальное событие
  EID_BigDate = $C8; // Большое универсальное событие

  EID_CheckSum = $F0; // Контрольная сумма
  EID_SysCrd_SS = $F1; // Системная координата короткая
  EID_SysCrd_SF = $F4; // Полная системная координата с короткой ссылкой
  EID_SysCrd_FS = $F9; // Системная координата короткая
  EID_SysCrd_FF = $FC; // Полная системная координата с полной ссылкой
  EID_SysCrd_NS = $F2; // Системная координата новая, короткая
  EID_SysCrd_NF = $F3; // Системная координата новая, полная
  EID_SysCrd_UMUA_Left_NF = $F5; // Полная системная координата без ссылки БУМ A, левая сторона
  EID_SysCrd_UMUB_Left_NF = $F6; // Полная системная координата без ссылки БУМ В, левая сторона
  EID_SysCrd_UMUA_Right_NF = $F7; // Полная системная координата без ссылки БУМ A, правая сторона
  EID_SysCrd_UMUB_Right_NF = $F8; // Полная системная координата без ссылки БУМ В, правая сторона

  EID_EndFile = $FF; // Конец файла

  // ------------------------< Идентификаторы дополнительных событий >---------------------------------------

  EID_FwdDir = 1; // Телега катится вперед
  EID_LinkPt = 2; // Точка для связи координат и позиции в файле
  EID_BwdDir = 3; // Телега катится назад
  EID_EndBM  = 4; //

  EID_StartBadZone = 5; //
  EID_EndBadZone = 6; //

//  EID_ACData_ = 5; // Данные акустического контакта

  // ------------------------< Заголовок файла >---------------------------------------

  // Флаги определяющие используемые записи в заголовке файла

  uiUnitsCount = 1;       // Количество блоков прибора
  uiRailRoadName = 2;     // Название железной дороги
  uiOrganization = 3;     // Название организации осуществляющей контроль
  uiDirectionCode = 4;    // Код направления
  uiPathSectionName = 5;  // Название участка дороги
  uiRailPathNumber = 6;   // Номер ж/д пути
  uiRailSection = 7;      // Звено рельса
  uiDateTime = 8;         // Дата время контроля
  uiOperatorName = 9;     // Имя оператора
  uiCheckSumm = 10;       // Контрольная сумма
  uiStartMetric = 11;     // Начальная координата - Метрическая
  uiMoveDir = 12;         // Направление движения
  uiPathCoordSystem = 13; // Система путейской координат
  uiWorkRailTypeA = 14;   // Рабочая нить (для однониточных приборов)
  uiStartChainage = 15;   // Начальная координата в XXX.YYY
  uiWorkRailTypeB = 16;   // Рабочая нить вариант №2
  uiTrackDirection = 17;  // Код направления
  uiTrackID = 18;         // TrackID
  uiCorrSides = 19;       // Соответствие стороны тележки нитям пути
  uiControlDir = 20;      // Направление контроля
  uiGaugeSideLeftSide = 21;  // Соответствие стороны тележки рабочей / не рабочей грани головки рельса
  uiGaugeSideRightSide = 22; // Соответствие стороны тележки рабочей / не рабочей грани головки рельса
  uiGPSTrackinDegrees = 23;  // Запись GPS трека (значения в градусах)
  // v5
  uiSetMaintenanceServicesDate = $18; // Дата технического обслуживания (запись тупика)
  uiSetCalibrationDate = $19; // Дата калибровки
  uiSetCalibrationName = $1A; // Название настройки
  // v6
  uiAcousticContact = $1B;    // Запись акустического контакта
  uiBScanTreshold_minus_6dB = $1C; // Порог В-развертки - 6 Дб
  uiBScanTreshold_minus_12dB = $1D; // Порог В-развертки - 12 Дб

  uiHandScanFile = $1E; // Файл ручного контроля

  uiTemperature = $1F; // Температура окружающей стреды
  uiSpeed = $20; // Скорость
  uiRailPathTextNumber = $21; // Номер ж/д пути в формате текст
  uiFlawDetectorNumber = $22; // Номер дефектоскопа (рамы)

  uiAssetNum = $23; // Идентификатор проверяемого актива ЕК АСУИ (Авикон-31,15)
  uiDiagnosticDeviceCode = $24; // Код средства диагностики (Авикон-31,15)
  // ------------------------< Количество каналов оценки >---------------------------------------

  MinEvalChannel = 0;
  MaxEvalChannel = 16;//!!!AK 64;

  // --------------------------------------------------------------------------------------------


//  TChannelID = Integer;
  MaxChannel_ = 44;

  MIRRORSHADOW_0 = 0;
  ECHO_0 = 17;

  FORWARD_BOTH_ECHO_58 = 1;
  FORWARD_WORK_ECHO_58 = 2;
  FORWARD_UWORK_ECHO_58 = 3;

  BACKWARD_BOTH_ECHO_58 = 4;
  BACKWARD_WORK_ECHO_58 = 5;
  BACKWARD_UWORK_ECHO_58 = 6;

  FORWARD_BOTH_MIRROR_58 = 7;
  FORWARD_WORK_MIRROR_58 = 8;
  FORWARD_UWORK_MIRROR_58 = 9;

  BACKWARD_BOTH_MIRROR_58 = 10;
  BACKWARD_WORK_MIRROR_58 = 11;
  BACKWARD_UWORK_MIRROR_58 = 12;

  FORWARD_ECHO_70 = 13;
  BACKWARD_ECHO_70 = 14;

  FORWARD_ECHO_42_WEB = 15;
  BACKWARD_ECHO_42_WEB = 16;

  FORWARD_ECHO_42_BASE = 18;
  BACKWARD_ECHO_42_BASE = 19;

  FORWARD_ECHO_65 = 20;
  BACKWARD_ECHO_65 = 21;

  SIDE_ECHO_65 = 22;

  WORK_MSM_55 = 23;
  UWORK_MSM_55 = 24;

  FORWARD_WORK_ECHO_65 = 25;
  FORWARD_UWORK_ECHO_65 = 26;

  BACKWARD_WORK_ECHO_65 = 27;
  BACKWARD_UWORK_ECHO_65 = 28;

  FORWARD_WORK_ECHO_50 = 29;
  FORWARD_UWORK_ECHO_50 = 30;

  BACKWARD_WORK_ECHO_50 = 31;
  BACKWARD_UWORK_ECHO_50 = 32;

  FORWARD_ECHO_45_WEB = 33;
  BACKWARD_ECHO_45_WEB = 34;
  FORWARD_ECHO_45_BASE = 35;
  BACKWARD_ECHO_45_BASE = 36;

  WORK_MSM_45 = 37;
  UWORK_MSM_45 = 38;

  FORWARD_ECHO_22 = 39;
  BACKWARD_ECHO_22 = 40;

  FORWARD_WORK_ECHO_70 = 41;
  FORWARD_UWORK_ECHO_70 = 42;

  BACKWARD_WORK_ECHO_70 = 43;
  BACKWARD_UWORK_ECHO_70 = 44;

//  Colors: array [0..MaxChannel_] of TColor;

const
  EchoLen: array [0 .. 7] of Integer = (2, 3, 5, 6, 8, 9, 11, 0);
  HSEchoLen: array [1 .. 4] of Integer = (2, 3, 5, 6);


  // Соответствие номер канала             //  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13
  // и колеса в EGO USW (Бангкок)          //  0 - WPA
                                           //  1 - WPB
  EGOUSW_WP_Data: array [0..13] of Integer = ( 0,  0,  0,  1,  0,  1,  0,  1,  0,  1,  1,  1,  0,  1);

  // Соответствие номер канала                //  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13
  // и колеса в EGO USW (Каир)                //  0 - WPA
                                              //  1 - WPB
  EGOUSW_BigWP_Data: array [0..13] of Integer = ( 0,  0,  0,  1,  0,  1,  0,  1,  0,  1,  0,  1,  0,  1);

const                                     //  0  1  2  3  4  5  6  7  8  9  10 11 12 13 14
  EvalChToScanCh: array [0..13] of Integer = (1, 1, 2, 3, 4, 5, 6, 7, 6, 7, 11,11,12,13);

// 1.	тип стрелочного перевода [SwitchType]

//    Undefined = 10

    st_SingleSwitch = 11;                          // одиночный
    st_CrossSwitch = 12;                           // перекрёстный
    st_SingleSwitchContiniousRollingSurface = 13;  // одиночный с непрерывной поверхностью катания
    st_CrossSwitchContiniousRollingSurface = 14;   // перекрестный с непрерывной поверхностью ката-ния
    st_DropSwitch = 15;                            // сбрасывающий
    st_OtherSwitch = 16;                           // другой

// 2.	направление(исполнение) стрелочного перевода [SwitchDirectionType]

//    Undefined = 20,
    sdt_Left = 21;               // левый
    sdt_Right = 22;              // правый
    sdt_SingleThreadLeft = 23;   // 1-ниточный левый
    sdt_SingleThreadRight = 24;  // 1-ниточный правый
    sdt_DualThreadLeft = 25;     // 2-ниточный левый
    sdt_DualThreadRight = 26;    // 2-ниточный правый

// 3.	установленное направление [SwitchFixedDirection]

//    Undefined = 30
    sfd_Plus = 31;       // + (прямое)
    sfd_Minus = 32;      // - (боковое)
    sfd_LeftPlus = 33;   // левое + (прямое)
    sfd_LeftMinus = 34;  // левое - (боковое)
    sfd_RightPlus = 35;  // правое + (прямое)
    sfd_RightMinus = 36; // правое - (боковое)

// 4.	направление движения дефектоскопа [DeviceDirection]

//    Undefined = 40
    sdd_AgainstWool = 41;       // противошёрстное
    sdd_ByWool = 42;            // пошёрстное
    sdd_LeftPlus = 43;          // левое + (прямое)
    sdd_LeftMinus = 44;         // левое - (боковое)
    sdd_RightPlus = 45;         // правое + (прямое)
    sdd_RightMinus = 46;        // правое - (боковое)
    sdd_AgainstWoolPlus = 47;   // противошёрстное + (прямое)
    sdd_AgainstWoolMinus = 48;  // противошёрстное - (боковое)
    sdd_ByWoolPlus = 49;        // пошёрстное + (прямое)
    sdd_ByWoolMinus = 50;       // пошёрстное - (боковое

type


  TVideoLink = record
    DisCrd: Integer;
    ms: Int64;
  end;

  TVideoLinkArr = array of TVideoLink;
//  PVideoLinkArr = ^TVideoLinkArr;


           // Путейская координата

  TCoordSys = (csMetricRF, csImperial, csMetric0_1km, csMetric1km, csMetric1kmTurkish); // Система отсчета путейской координаты

  TCaCrd = record  // Размер 8 байт
    XXX: Integer;
    YYY: Integer;
  end;

  TMRFCrd = record // Размер 12 байт
    Km: Integer;
    Pk: Integer;
    mm: Integer;
  end;

  TRail = (rLeft, rRight, rNotSet);

  TCorrespondenceSides = (csDirect, csReverse); // Соответствие стороны тележки нитям пути: прямое - левая сторона = левой нити
                                                // Соответствие стороны дрезины нитям пути: прямое - БУМы: ID 0, ID 1 = левая нить пути; Обратное - ID 0, ID 1 = правая нить пути

  TZerroProbMode = (zpmBoth = 0, zpmOnlyA = 1, zpmOnlyB = 2); // Режим работы датчиков 0 град

  TInspectionMethod = (imEcho,           // Эхо метод
                       imMirrorShadow,   // Зеркально теневой метод
                       imMirror,         // Зеркальный метод
                       imShadow,         // Теневой метод
                       imMirrorShadow2); // Зеркально теневой метод - Не требует подсветки сигналов попадающих в строб


  TInspectionZone = (izAll, izHead, izWeb, izBase);

                                         // Типы блоков прибора
  TDeviceUnitType = (dutUpUnit = 1,      // Верхний блок
                     dutDownUnit = 2,    // Нижний блок
                     dutMiddleUnit = 3,  // Коммутационный блок
                     dutFlawDetectorNumber = 4); // Номер дефектоскопа

  TAmplItem = record  // Элемент таблицы перекодировки отсчетов в значение амплитуды
    Code: Integer;
    Value: Integer;
  end;

  TInfoSide = (isLeft, isRight); // Сторона окна вывода В-развертки
  TInfoPos = (ipLeft, ipCentre, ipRight); // Положение в окне вывода строба

  TEvalChannelItem = record      // Канал оценки
    Number: Integer;             // Номер канала
    Method: TInspectionMethod;   // Метод контроля
    Zone: TInspectionZone;       // Зона контроля по высоте
    ShortName: string;           // Краткое название канала
    FullName: string;            // Полное название канала
    DirColor: Integer;
    // ?                         // Расчет параметров - Глубина
    ScanChNum: Integer;          // Канал развертки с которым связан
                                 // Информация о зоне вывода параметров каналов оценки
//    InfoSide: TInfoSide;         // Сторона
//    InfoPos: TInfoPos;           // Позиция
  end;

  TEvalChannelItem2 = record     // Канал оценки
    InfoSide: TInfoSide;         // Сторона
    InfoPos: TInfoPos;           // Позиция
  end;


  TScanChannelDataType = (scdtNormal,  // Обычный
                          scdtPacked); // С упаковкой

  TGran = (_None,
           _Both,
           _Work,
           _UnWork,
           _Work_Left__UnWork_Right,
           _UnWork_Left__Work_Right,

           // для Filus-X17DW
           Left_Gran_by_СOND,
           Right_Gran_by_СOND);


  TScanChannelItem = record         // Канал развертки
    BS_Ch_File_Number: Integer;     // Номер канала в файле регистрации
    EnterAngle: Integer;            // Угол ввода (по направлению движения) + Знак Наезжающий / Отъезжающий
    TurnAngle: Integer;             // Угол разворота (по направлению движения)
    Gran: TGran;                    // Контролируемая грань
    Position: Integer;              // Положение в лыже (мм)
    CrossPosition: Integer;         // Положение в лыже (мм)
    Delta: Integer;                 // Здиг сигналов В-развертки для корректировки
    BScanGateMin: Integer;          // Начало строба В-Развертки (мкс)
    BScanGateMax: Integer;          // Конец строба В-Развертки (мкс)
    BScanShift:   Integer;          // Здвиг В-Развертки (мкс)
    BScanDuration: Integer;         // Длительность строба В-Развертки (мкс)
    DelayMultiply: Integer;         // Множитель задержек (величина на которую домнажается значение задержки перед сохранением в byte)

//    DataType: TScanChannelDataType; // Тип данных (обычный тип / Упаковка данных)
//    Order: Integer;                 //
  end;

  THandChannelItem = record         // Канал ручного контроля
    Number: Integer;                // Номер канала
    BScanGateMin: Integer;          // Начало строба В-Развертки (мкс)
    BScanGateMax: Integer;          // Конец строба В-Развертки (мкс)
    DelayMultiply: Integer;         // Множитель задержек (величина на которую домнажается значение задержки перед сохранением в byte)
    Method: TInspectionMethod;      // Метод контроля
    Angle: Integer;                 // Угол ввода (по направлению движения)
    ShortName: string;              // Краткое название канала
    FullName: string;               // Полное название канала
    // ?                            // Расчет параметров - Глубина
  end;

  TBScanLineItem = record
    ScanChIdx: Integer;             // Индекс канала развертки в массиве FScanChannels
//    Order: Integer;               // Индекс канала: 1 - главный канал 2 - не главный канал
//    ChNum: Integer;               // Номер канала развертки
//    ChType: Integer;                // ?
//    ColorIdx: Integer;
  end;

  TBScanLine_ = record               // Полоса вывода В-развертки
    R: TRail;                       // Нить
    Use: Boolean;                   // Используется или нет
    Items: array of TBScanLineItem; // Каналы выводимые на данной полосе
  end;

  TDataNotifyEvent = function(StartDisCoord: Integer): Boolean of object;

  TEvalChannelParams = packed record //9bytes
    Ku: ShortInt;      // Условная чувствительность [дБ]
    Att: Byte;         // Аттенуатор [дБ]
    TVG: Byte;         // ВРЧ [мкс]
    StStr: Byte;       // Начало строба [мкс]
    EndStr: Byte;      // Конец строба [мкс]
    StStr_mul_DelayMultiply: Word;       // Начало строба умноженное на DelayMultiply
    EndStr_mul_DelayMultiply: Word;      // Конец строба умноженное на DelayMultiply
    PrismDelay: Word;  // Время в призмен [мкс x 100]
    HeadPh: Boolean;   // АСД
  end;

//!!!AK  TEvalChannelsParams = array [rLeft..rRight] of array [MinEvalChannel..255 {MaxEvalChannel}] of TEvalChannelParams;
  TEvalChannelsParams = array [rLeft..rRight] of array [MinEvalChannel..{255} MaxEvalChannel] of TEvalChannelParams;
  TSensor1Data = array [TRail] of Boolean;

  EMyExcept = class(Exception);

  THeaderStr = array [0..64] of Char;

  THeaderBigStr = array [0..255] of Char;

  TUnitInfo = packed record  // Информация о блоке прибора
    UnitType: TDeviceUnitType; // Тип блока
    WorksNumber: THeaderStr;   // Заводской номер блока
    FirmwareVer: THeaderStr;   // Версия программного обеспечения блока
  end;

  TCardinalPoints = array [0..1] of WChar; // Стороны света

  TUsedItemsList = array [1 .. 64] of Byte; // Флаги определяющие используемые записи

  TUnitsInfoList = array [0 .. 9] of TUnitInfo; // Информация о блоках прибора

  TFileHeader_v01 = packed record // Заголовок файла
{x}    FileID: uint64; // RSPBRAFD
{+}    DeviceID: uint64; // Идентификатор прибора
{x}    DeviceVer: Byte; // Версия прибора
{x}    HeaderVer: Byte; // Версия заголовка
{+}    MoveDir: Integer; // Направление движения: + 1 в сторону увеличения путейской координаты; - 1 в сторону уменьшения путейской координаты
{x}    ScanStep: Word; // Шаг датчика пути (мм * 100)
{?}    PathCoordSystem: Byte; // Система отсчета путейской координаты

    UsedItems: TUsedItemsList; // Флаги определяющие используемые записи
    CheckSumm: Cardinal; // Контрольная сумма (unsigned 32-bit)
{+}    UnitsCount: Byte; // Количество блоков прибора
{+}    UnitsInfo: TUnitsInfoList; // Информация о блоках прибора
{+}    Organization: THeaderStr; // Название организации осуществляющей контроль
{+}    RailRoadName: THeaderStr; // Название железной дороги
{+}    DirectionCode: DWord; // Код направления
{+}    PathSectionName: THeaderStr; // Название участка дороги
{+}    OperatorName: THeaderStr; // Имя оператора
{+}    RailPathNumber: Integer; // Номер ж/д пути
{+}    RailSection: Integer; // Звено рельса
{+}    Year: Word; // Дата время контроля
{+}    Month: Word;
{+}    Day: Word;
{+}    Hour: Word;
{+}    Minute: Word;
{+}    StartKM: Integer; // Начальная координата - Метрическая
{+}    StartPk: Integer;
{+}    StartMetre: Integer;
    WorkRail: TRail; // Рабочая нить (для однониточных приборов)
    Reserv: array [1 .. 2047] of Byte; // Резерв
    TableLink: Cardinal; // Ссылка на расширенный заголовок
  end;

  TFileHeader_v02 = packed record // Заголовок файла
    FileID: TAviconID; // RSPBRAFD
    DeviceID: TAviconID; // Идентификатор прибора
    DeviceVer: Byte; // Версия прибора
    HeaderVer: Byte; // Версия заголовка
    MoveDir: Integer; // Направление движения: + 1 в сторону увеличения путейской координаты; - 1 в сторону уменьшения путейской координаты
    ScanStep: Word; // Шаг датчика пути (мм * 100)
    PathCoordSystem: Byte; // Система отсчета путейской координаты
    UsedItems: TUsedItemsList; // Флаги определяющие используемые записи
    CheckSumm: Cardinal; // Контрольная сумма (unsigned 32-bit)
    UnitsCount: Byte; // Количество блоков прибора
    UnitsInfo: TUnitsInfoList; // Информация о блоках прибора
    Organization: THeaderStr; // Название организации осуществляющей контроль
    RailRoadName: THeaderStr; // Название железной дороги
    DirectionCode: DWord; // Код направления
    PathSectionName: THeaderStr; // Название участка дороги
    OperatorName: THeaderStr; // Имя оператора
    RailPathNumber: Integer; // Номер ж/д пути
    RailSection: Integer; // Звено рельса
    Year: Word; // Дата время контроля
    Month: Word;
    Day: Word;
    Hour: Word;
    Minute: Word;
    StartKM: Integer; // Начальная координата - Метрическая
    StartPk: Integer;
    StartMetre: Integer;
    WorkRailTypeA: Byte;  // Рабочая нить (для однониточных приборов): 0 – левая, 1 – правая
    StartChainage: TCaCrd;  // Начальная координата в зависимости от PathCoordSystem
    WorkRailTypeB: TCardinalPoints;  // Рабочая нить (для однониточных приборов): NR, SR, WR, ER
    TrackDirection: TCardinalPoints; // Код направления:                          NB, SB, EB, WB, CT, PT, RA, TT
    TrackID: TCardinalPoints;        // Track ID:                                 NR, SR, ER, WR
    CorrespondenceSides: TCorrespondenceSides;
	  HCThreshold: Double;
    Reserv: array [1 .. 2031 - 5 - 8] of Byte; // Резерв
    TableLink: Cardinal; // Ссылка на расширенный заголовок
  end;

  TLinkData = array [0..15] of Word;

  TFileHeader_v03 = packed record // Заголовок файла
    FileID: TAviconID; // RSPBRAFD
    DeviceID: TAviconID; // Идентификатор прибора
    DeviceVer: Byte; // Версия прибора
    HeaderVer: Byte; // Версия заголовка
    MoveDir: Integer; // Направление движения: + 1 в сторону увеличения путейской координаты; - 1 в сторону уменьшения путейской координаты
    ScanStep: Word; // Шаг датчика пути (мм * 100)
    PathCoordSystem: Byte; // Система отсчета путейской координаты
    UsedItems: TUsedItemsList; // Флаги определяющие используемые записи
    CheckSumm: Cardinal; // Контрольная сумма (unsigned 32-bit)
    UnitsCount: Byte; // Количество блоков прибора
    UnitsInfo: TUnitsInfoList; // Информация о блоках прибора
    Organization: THeaderStr; // Название организации осуществляющей контроль
    RailRoadName: THeaderStr; // Название железной дороги
    DirectionCode: DWord; // Код направления
    PathSectionName: THeaderStr; // Название участка дороги
    OperatorName: THeaderStr; // Имя оператора
    RailPathNumber: Integer; // Номер ж/д пути
    RailSection: Integer; // Звено рельса
    Year: Word; // Дата время контроля
    Month: Word;
    Day: Word;
    Hour: Word;
    Minute: Word;
    StartKM: Integer; // Начальная координата - Метрическая
    StartPk: Integer;
    StartMetre: Integer;
    WorkRailTypeA: Byte;  // Рабочая нить (для однониточных приборов): 0 – левая, 1 – правая
    StartChainage: TCaCrd;  // Начальная координата в зависимости от PathCoordSystem
    WorkRailTypeB: TCardinalPoints;  // Рабочая нить (для однониточных приборов): NR, SR, WR, ER
    TrackDirection: TCardinalPoints; // Код направления:                          NB, SB, EB, WB, CT, PT, RA, TT
    TrackID: TCardinalPoints;        // Track ID:                                 NR, SR, ER, WR
    CorrespondenceSides: TCorrespondenceSides;
	  HCThreshold: Double;
    ChIdxtoCID: TLinkData;     // Массив связи индекса канала в файле с CID
    ChIdxtoGateIdx: TLinkData; // Массив связи индекса канала в файле с номером строба
    Reserv: array [1..1954] of Byte; // Резерв
    TableLink: Cardinal; // Ссылка на расширенный заголовок
  end;

  TFileHeader_v04 = packed record // Заголовок файла
    FileID: TAviconID; // RSPBRAFD
    DeviceID: TAviconID; // Идентификатор прибора
    DeviceVer: Byte; // Версия прибора
    HeaderVer: Byte; // Версия заголовка
    MoveDir: Integer; // Направление движения: + 1 в сторону увеличения путейской координаты; - 1 в сторону уменьшения путейской координаты
    ScanStep: Word; // Шаг датчика пути (мм * 100)
    PathCoordSystem: Byte; // Система отсчета путейской координаты
    UsedItems: TUsedItemsList; // Флаги определяющие используемые записи
    CheckSumm: Cardinal; // Контрольная сумма (unsigned 32-bit)
    UnitsCount: Byte; // Количество блоков прибора
    UnitsInfo: TUnitsInfoList; // Информация о блоках прибора
    Organization: THeaderStr; // Название организации осуществляющей контроль
    RailRoadName: THeaderStr; // Название железной дороги
    DirectionCode: DWord; // Код направления
    PathSectionName: THeaderStr; // Название участка дороги
    OperatorName: THeaderStr; // Имя оператора
    RailPathNumber: Integer; // Номер ж/д пути
    RailSection: Integer; // Звено рельса
    Year: Word; // Дата время контроля
    Month: Word;
    Day: Word;
    Hour: Word;
    Minute: Word;
    StartKM: Integer; // Начальная координата - Метрическая
    StartPk: Integer;
    StartMetre: Integer;
    WorkRailTypeA: Byte;  // Рабочая нить (для однониточных приборов): 0 – левая, 1 – правая
    StartChainage: TCaCrd;  // Начальная координата в зависимости от PathCoordSystem
    WorkRailTypeB: TCardinalPoints;  // Рабочая нить (для однониточных приборов): NR, SR, WR, ER
    TrackDirection: TCardinalPoints; // Код направления:                          NB, SB, EB, WB, CT, PT, RA, TT
    TrackID: TCardinalPoints;        // Track ID:                                 NR, SR, ER, WR
    CorrespondenceSides: TCorrespondenceSides;
	  HCThreshold: Double;
    ChIdxtoCID: TLinkData;     // Массив связи индекса канала в файле с CID
    ChIdxtoGateIdx: TLinkData; // Массив связи индекса канала в файле с номером строба
    ControlDirection: Byte;    // Направление контроля: 0 - B >>> A (по ходу / головой вперед); 1 - B <<< A (против хода / головой назад)
    Reserv: array [1..1954 - 1] of Byte; // Резерв
    TableLink: Cardinal; // Ссылка на расширенный заголовок
  end;

  TFileHeader_v05 = packed record // Заголовок файла
    FileID: TAviconID; // RSPBRAFD
    DeviceID: TAviconID; // Идентификатор прибора
    DeviceVer: Byte; // Версия прибора
    HeaderVer: Byte; // Версия заголовка
    MoveDir: Integer; // Направление движения: + 1 в сторону увеличения путейской координаты; - 1 в сторону уменьшения путейской координаты
    ScanStep: Word; // Шаг датчика пути (мм * 100)
    PathCoordSystem: Byte; // Система отсчета путейской координаты
    UsedItems: TUsedItemsList; // Флаги определяющие используемые записи
    CheckSumm: Cardinal; // Контрольная сумма (unsigned 32-bit)
    UnitsCount: Byte; // Количество блоков прибора
    UnitsInfo: TUnitsInfoList; // Информация о блоках прибора
    Organization: THeaderStr; // Название организации осуществляющей контроль
    RailRoadName: THeaderStr; // Название железной дороги
    DirectionCode: DWord; // Код направления
    PathSectionName: THeaderStr; // Название участка дороги
    OperatorName: THeaderStr; // Имя оператора
    RailPathNumber: Integer; // Номер ж/д пути
    RailSection: Integer; // Звено рельса
    Year: Word; // Дата время контроля
    Month: Word;
    Day: Word;
    Hour: Word;
    Minute: Word;
    StartKM: Integer; // Начальная координата - Метрическая
    StartPk: Integer;
    StartMetre: Integer;
    WorkRailTypeA: Byte;  // Рабочая нить (для однониточных приборов): 0 – левая, 1 – правая
    StartChainage: TCaCrd;  // Начальная координата в зависимости от PathCoordSystem
    WorkRailTypeB: TCardinalPoints;  // Рабочая нить (для однониточных приборов): NR, SR, WR, ER
    TrackDirection: TCardinalPoints; // Код направления:                          NB, SB, EB, WB, CT, PT, RA, TT
    TrackID: TCardinalPoints;        // Track ID:                                 NR, SR, ER, WR
    CorrespondenceSides: TCorrespondenceSides;
	  HCThreshold: Double;
    ChIdxtoCID: TLinkData;     // Массив связи индекса канала в файле с CID
    ChIdxtoGateIdx: TLinkData; // Массив связи индекса канала в файле с номером строба
    ControlDirection: Byte;    // Направление контроля: 0 - B >>> A (по ходу / головой вперед); 1 - B <<< A (против хода / головой назад)
    MaintenanceServicesYear: Word; // Дата технического обслуживания (запись тупика)
    MaintenanceServicesMonth: Word;
    MaintenanceServicesDay: Word;
    CalibrationYear: Word; // Дата калибровки
    CalibrationMonth: Word;
    CalibrationDay: Word;
    CalibrationName: THeaderStr; // Название настройки
    RailPathTextNumber: THeaderStr; // Номер ж/д пути (в формате строка)
    UncontrolledSectionMinLen: Byte; // Минимальная длина НПКУч принимаемая в расчет (принимаемая за значимую зону) [мм]
    Reserv: array [1..1680] of Byte; // Резерв
    TableLink: Cardinal; // Ссылка на расширенный заголовок
  end;

  TFileHeader_v06 = packed record // Заголовок файла
    FileID: TAviconID; // RSPBRAFD
    DeviceID: TAviconID; // Идентификатор прибора
    DeviceVer: Byte; // Версия прибора
    HeaderVer: Byte; // Версия заголовка
    MoveDir: Integer; // Направление движения: + 1 в сторону увеличения путейской координаты; - 1 в сторону уменьшения путейской координаты
    ScanStep: Word; // Шаг датчика пути (мм * 100)
    PathCoordSystem: Byte; // Система отсчета путейской координаты
    UsedItems: TUsedItemsList; // Флаги определяющие используемые записи
    CheckSumm: Cardinal; // Контрольная сумма (unsigned 32-bit)
    UnitsCount: Byte; // Количество блоков прибора
    UnitsInfo: TUnitsInfoList; // Информация о блоках прибора
    Organization: THeaderStr; // Название организации осуществляющей контроль
    RailRoadName: THeaderStr; // Название железной дороги
    DirectionCode: DWord; // Код направления
    PathSectionName: THeaderStr; // Название участка дороги
    OperatorName: THeaderStr; // Имя оператора
    RailPathNumber: Integer; // Номер ж/д пути
    RailSection: Integer; // Звено рельса
    Year: Word; // Дата время контроля
    Month: Word;
    Day: Word;
    Hour: Word;
    Minute: Word;
    StartKM: Integer; // Начальная координата - Метрическая
    StartPk: Integer;
    StartMetre: Integer;
    WorkRailTypeA: Byte;  // Рабочая нить (для однониточных приборов): 0 – левая, 1 – правая
    StartChainage: TCaCrd;  // Начальная координата в зависимости от PathCoordSystem
    WorkRailTypeB: TCardinalPoints;  // Рабочая нить (для однониточных приборов): NR, SR, WR, ER
    TrackDirection: TCardinalPoints; // Код направления:                          NB, SB, EB, WB, CT, PT, RA, TT
    TrackID: TCardinalPoints;        // Track ID:                                 NR, SR, ER, WR
    CorrespondenceSides: TCorrespondenceSides;
	  HCThreshold: Double;
    ChIdxtoCID: TLinkData;     // Массив связи индекса канала в файле с CID
    ChIdxtoGateIdx: TLinkData; // Массив связи индекса канала в файле с номером строба
    ControlDirection: Byte;    // Направление контроля: 0 - B >>> A (по ходу / головой вперед); 1 - B <<< A (против хода / головой назад)
    MaintenanceServicesYear: Word; // Дата технического обслуживания (запись тупика)
    MaintenanceServicesMonth: Word;
    MaintenanceServicesDay: Word;
    CalibrationYear: Word; // Дата калибровки
    CalibrationMonth: Word;
    CalibrationDay: Word;
    CalibrationName: THeaderStr; // Название настройки
    RailPathTextNumber: THeaderStr; // Номер ж/д пути (в формате строка)
    UncontrolledSectionMinLen: Byte; // Минимальная длина НПКУч принимаемая в расчет (принимаемая за значимую зону) [мм]
    AssetNum: THeaderStr; // Идентификатор актива (Рабочего задания, ЕК АСУИ)
    DiagnosticDeviceCode: THeaderStr; // Код средства диагностики
    Reserv: array [1..1420] of Byte; // Резерв
    TableLink: Cardinal; // Ссылка на расширенный заголовок
  end;


  TFileHeader = TFileHeader_v06;

  // ----[ параметры краскопульта ]---------------------------------------------------------------

  TPaintSystemParams = packed record           //  | Номер строки в конфиг файле
    Version: Integer;                       //  2 Шаг загрузки данных в обработку [мм]
    LoadStep: Integer;                      //  2 Шаг загрузки данных в обработку [мм]
    Slide_SensorPos_: Integer;              //  3 Положение датчика металла относительно центра ИС для системы скольжения [мм]
    Slide_AirBrushPos_: Integer;            //  4 Положение краскоотметчика относительно центра ИС для системы скольжения [мм]
    SensorWorkZone: Integer;                //  5 Протяженность зоны хранения данных дачика металла [мм]
    MaxLenBetweenABMember: Integer;         //  6 Максимальное расстояние между центрами пачек принимаемых за одну отметку [мм]
    MSMPBMinLen: Integer;                   //  7 Минимальная протяженнось проподания ДС принимаемого за пачку [мм]
{?} ECHOPBMaxGapLen: Integer;               //  8 Максимально допустимый разрыв в пачке эхо сигналов [ШДП]
    TwoEchoMinDelay: Integer;               //  9 Минимальная разница задержек пачек [мкс] (определения признака 2 Эхо)
    TwoEchoMaxDelay: Integer;               // 10 Максимальная разница задержек пачек [мкс] (определения признака 2 Эхо)
{?} DecisionMakingBoundary: Integer;        // 11 ? Граница принятия решения - от центра ИС
    TwoEchoZoneLen: Integer;                // 12 Протяженность зоны КЭ, в ней происходит принятие решения по признаку 2 Эхо
    MinCrdDelayRatio: Single;               // 13 Минимальный допустимый наклон пачки [мкс / мм]
    MaxCrdDelayRatio: Single;               // 14 Максимальный допустимый наклон пачки [мкс / мм]
    MaxAmplToLen: array [0..15] of Integer; // 15...30 Минимальная протяженность группы сигналов (мм) принимаемых за пачку в зависимости от максимальной амплитуды среди сигналов группы
    BSStateFilterLen: Integer;              // 31 Размерность фильтра колебаний состояния ДС [ШДП]
    BSStateParam: Integer;                  // 32 not use
    HeadZone1: Integer;                     // 33 ? Зона головки №1
    HeadZone2: Integer;                     // 34 ? Зона головки №2
    HeadZoneSize: Integer;                  // 35 ? Размер зоны головки
    Wheel_SensorPos_: Integer;              // 36 Положение датчика металла относительно центра ИС для КИС [мм]
    Wheel_AirBrushPos_: Integer;            // 37 Положение краскоотметчика относительно центра ИС для КИС [мм]
    EGOUSW_SensorPos_HeadA: Integer;        // 38 Положение датчика металла относительно центра ИС [мм] EGO USW работа в сторону A
    EGOUSW_AirBrushPos_HeadA: Integer;      // 39 Положение краскоотметчика относительно центра ИС [мм] EGO USW работа в сторону A
    EGOUSW_SensorPos_HeadB: Integer;        // 40 Положение датчика металла относительно центра ИС [мм] EGO USW работа в сторону B
    EGOUSW_AirBrushPos_HeadB: Integer;      // 41 Положение краскоотметчика относительно центра ИС [мм] EGO USW работа в сторону B
    AirBrush_Shift: Integer;                // 42 Корректировка момента срабатывания краскопульта [мм]
    UseChannel_0d: Boolean;                 // 43 Поиск пачек в канале 0°
    UseChannel_42d: Boolean;                // 44 Поиск пачек в канале 42°
    UseChannel_42dTwoEcho: Boolean;         // 45 Поиск пачек в канале 42°, режим 2 Эхо
    UseChannel_58d: Boolean;                // 46 Поиск пачек в канале 58°
    UseChannel_65d: Boolean;                // 47 Поиск пачек в канале 65°
    Channel_0d_PB_MinLen: Integer;          // 48 Минимальная протяженность группы сигналов принимаемых за пачку в канале 0° эхо [мм]
    Channel_42d_PB_MinLen: Integer;         // 49 Минимальная протяженность группы сигналов принимаемых за пачку в канале 42° [мм]
    Channel_58d_PB_MinLen: Integer;         // 50 Минимальная протяженность группы сигналов принимаемых за пачку в канале 58° [мм]
    Channel_65d_PB_MinLen: Integer;         // 51 Минимальная протяженность группы сигналов принимаемых за пачку в канале 65° [мм]
    BH_45d_MinDelay: Integer;               // 52 Начало зоны поиска дефекта 53 [мкс]
    BH_45d_MaxDelay: Integer;               // 53 Конец зоны поиска дефекта 53 [мкс]
    Channel_0d_StGate: Integer;             // 54 Строб поиска пачек в канале 0° [мкс/3]
    Channel_0d_EdGate: Integer;             // 55 Строб поиска пачек в канале 0° [мкс/3]
    Channel_42d_StGate: Integer;            // 56 Строб поиска пачек в канале 42° [мкс]
    Channel_42d_EdGate: Integer;            // 57 Строб поиска пачек в канале 42° [мкс]
    Channel_58d_StGate: Integer;            // 58 Строб поиска пачек в канале 58° [мкс]
    Channel_58d_EdGate: Integer;            // 59 Строб поиска пачек в канале 58° [мкс]
    Channel_65d_StGate: Integer;            // 60 Строб поиска пачек в канале 65° [мкс]
    Channel_65d_EdGate: Integer;            // 61 Строб поиска пачек в канале 65° [мкс]
    BSNoiseAnalyzeZoneLen: Integer;         // 62 Размер зоны поиска неустойчивого ДС [шдп] {100 }
    BSNoiseAnalyzeTreshold: Integer;        // 63 Порог поиска неустойчивого ДС (отсутствий (шт) на зону [п.62]) [шдп] {60 }
    BSNoiseAnalyzeZoneExLen: Integer;       // 64 Расширение найденной зоны неустойчивого ДС [шдп]200
    BSSearchTreshold: Single;               // 65 Порог поиска проподания ДС [% отсутствия ДС] {0.45}
    BSSearchGlueZone: Integer;              // 66 Размер зоны склеивания пачек канала 0° {12 шдп} then // !!! эта величина есть ниже
    AlarmEventLifeTime: Integer;            // 67 Время жизни данных АСД [мкс]
    TickNoDataSleepValue: Integer;          // 68 Параметр функции Sleep в методе Tick [мкс]
    TwoEchoZoneLength: Integer;             // 69 Протяженность зоны 2 эхо принимаемой за дефект [шдп]
    Reserve: array [0..1786] of Byte;
  end;

  // ----[ NORDCO ]---------------------------------------------------------------

  TNStatus = (nsOpen, nsClosed); // Это поле указывает, закрыт ли дефект (т.е. удален из пути)

  TSourceType = (stUltrasonicTrolley, stVisualInspection); // Метод обнаружения дефекта


  TNordcoCSVRecordLine = record

    Line              : string;                                                     // Название или идентификатор контролируемой линии
    Track             : string;                                                     // Идентификатор пути, в котором обнаружен дефект
    LocationFrom      : Single;                                                     // Начало обнаруженного дефекта
    LocationTo        : Single;                                                     // Конец обнаруженного дефекта. В случае точечного дефекта то же самое, как поле  "Location From"
    LatitudeFrom      : Double;                                                     // GPS-широта для поля "Location From"
    LatitudeTo        : Double;                                                     // GPS-широта для поля "Location To"
    LongitudeFrom     : Double;                                                     // GPS-долгота для поля "Location From"
    LongitudeTo       : Double;                                                     // GPS-долгота для поля "Location To"
    Type_             : string;                                                     // Код дефекта по UIC 712
    SurveyDate        : TDateTime;                                                  // Дата обнаружения дефекта
    Source            : string;                                                     // Метод обнаружения дефекта - из списка: Ultrasonic Trolley, Visual Inspection
    Status            : string;                                                     // Это поле указывает, закрыт ли дефект (т.е. удален из пути) - из списка: Open, Closed
    IsFailure         : Boolean;                                                    // Если конкретный дефект вызвало отказ, то это поле должно иметь значение TRUE. Отказом можно считать любой дефект, который вызывает ограничение скорости
    AssetType         : string;                                                     // Тип верхнего строения пути - из списка: Left Rail, Right Rail
    DataEntryDate     : TDateTime;                                                  // Дата составления списка дефектов
    Size              : Single;                                                     // Размер дефекта для  неточечного дефекта. Например, если дефект это трещина, то ее длина.
    Size2             : Single;                                                     // Размер дефекта для  неточечного дефекта (если более чем одномерный)
    Size3             : Single;                                                     // Размер дефекта для  неточечного дефекта (если более чем одномерный)
    UnitofMeasurement : string;                                                     // Ед. измерения поля "Size"
    UnitofMeasurement2: string;                                                     // Ед. измерения поля "Size2"
    UnitofMeasurement3: string;                                                     // Ед. измерения поля "Size3"
    Comment           : string;                                                     // Примечание
    Operator_         : string;                                                     // Оператор
    Device            : string;                                                     // Прибор
  end;

  // ----[ Данные событий файла ]-------------------------------------------------

  // Событие: EID_HandScan

  THSHead_OLD = packed record // Заголовок данных В-развертки ручного канала
    DataSize: DWord;      // Размер буфера данных
    DelayMultiply: Byte;  //
    Rail: TRail;          // Нить
    ScanSurface: Byte;    // Номер поверхности сканирования рельса, описание канала см в config
    HandChNum: Byte;      // Номер канала
    Att: ShortInt;        // Аттенюатор
    Ku: Byte;             // Значение условной чувствительности
  end;

  THSHead = packed record // Заголовок данных В-развертки ручного канала
    DataSize: DWord;      // Размер буфера данных
    DelayMultiply: Byte;  //
    Rail: TRail;          // Нить
    ScanSurface: Byte;    // Номер поверхности сканирования рельса, описание канала см в config
    HandChNum: Byte;      // Номер канала
    Att: ShortInt;        // Аттенюатор
    Ku: Byte;             // Значение условной чувствительности
    TVG: Byte;            // Значение ВРЧ
  end;

//  pedHSHead = ^THSHead;

  THSItem = packed record // Сигналы В-разертки ручного канала (один блок данных БУМ - максимально 4 сигнала)
    Sample: Word;                 // Номер цикла зондирования
    Count: Byte;                  // Количество эхо-сигналов
    Data: array [0 .. 51] of Byte; // Данные сигналов
  end;

//  TEventData = packed array [1 .. 516] of Byte;
  TEventData = packed array [1 .. 2048] of Byte;
  PEventData = ^TEventData;

  // Событие: EID_Sens   - ShortInt
  // EID_Att    - Byte
  // EID_TVG    - Byte
  // EID_StStr  - Byte
  // EID_EdStr  - Byte
  // EID_HeadPh - Boolean

  edEvalChByteParam = packed record // 3 байта
    Rail: TRail;
    EvalCh: Byte;
    Value: Byte;
  end;

  pedEvalChByteParam = ^edEvalChByteParam;

  // Событие: EID_PrismDelay

  edEvalChWordParam = packed record // 4 байта
    Rail: TRail;
    EvalCh: Byte;
    Value: Word;
  end;

  pedEvalChWordParam = ^edEvalChWordParam;

  // Событие: EID_Mode

  edMode = packed record // 7 байт
    ModeIdx: Word;    // Режим
    Channel: Boolean; // Флаг говорит о том что были указаны нить и номер канала
    Rail: TRail;      // Нить
    EvalChNum: Byte;  // Номер канала оценки
    Time: Word;       // Время нахождения в предыдущем режиме (сек)
  end;

  pedMode = ^edMode;

  // Событие: EID_SetRailType
  // нет данных

  // Событие: EID_Stolb

  edCoordPost = packed record     // Размер: 144 Байта
    Km: array [0..1] of Integer;
    Pk: array [0..1] of Integer;
    Reserv: array [0..31] of Integer;
  end;
  pedCoordPost = ^edCoordPost;

  pCoordPost = pedCoordPost;

  // Событие: EID_StolbChainage

  edCoordPostChainage = packed record  // Размер: 136 Байта
    Val: TCaCrd;                       // Координата в фармате XXX.YYY
    Reserv: array [0..31] of Integer;
  end;
  pedCoordPostChainage = ^edCoordPostChainage;

  pCoordPostChainage = pedCoordPostChainage;

  // Событие: EID_ZerroProbMode - Режим работы датчиков 0 град

  edZerroProbMode = packed record // 7 байт - TZerroProbMode
    Mode: Byte; // Режим: 0 – Оба колеса; 1 – Наезжающее КП / КПA; 2 – Отъезжающее КП / КПB
  end;

  pedZerroProbMode = ^edZerroProbMode;
  pZerroProbMode = pedZerroProbMode;

  // Событие: EID_LongLabel Протяженная отметка

  edLongLabel = packed record // 24 байт
    Rail: TRail;      // Нить
    EvalCh: Byte;
    ScanCh: Byte; // Канал развертки
    Type_: Byte;  // Тип: 1 - неустойчевый ДС; 2 - зона КЭ
    Data1: Integer;
    Data2: Integer;
    Data3: Integer;
    StartDisCoord: Integer; // Дисплейная координата начала отметки
    EndDisCoord: Integer; // Дисплейная координата конца отметки
  end;

  pedLongLabel = ^edLongLabel;
  pLongLabel = pedLongLabel;

  // Событие: EID_SpeedState - Скорость и Превышение скорости контроля

//  TSpeedStates = (ssNormalSpeed = 0,      // нормальная скорость
//                  ssStartSpeedExcess = 1, // начало зоны превышения
//                  ssEndSpeedExcess = 2);  // конец зоны превышения


  edSpeedState = packed record
    Speed: Integer; // Км/ч * 10
    State: Byte; // 0 - Нормальная скорость
                 // 1 - Превышение скорости
  end;
  pedSpeedState = ^edSpeedState;

  // Событие: EID_AutomaticSearchRes 0xAC - «Значимые» участки рельсов, получаемые при автоматическом поиске

  edAutomaticSearchRes = packed record // 9 байт ?
    Rail: TRail;  // Нить
    ScanCh: Byte; // Канал развертки
    CentrCoord: Integer; // Дисплейная координата центра отметки
    CoordWidth: Byte; // Ширина зоны
    StDelay: Byte; // Начальная задержка, мкс
    EdDelay: Byte; // Конечная задержка, мкс
  end;

  pedAutomaticSearchRes = ^edAutomaticSearchRes;
(*
  tAutomaticSearchRes = record

    PaintDisCoord: Integer;       // Дисплейная координата для краскоотметки
    SendCmdDisCoord: Integer;     // Дисплейная координата передачи команды на краскоотметку
    Skip: Boolean;
    Data: edAirBrush2;
  end;
*)

  // Событие: EID_OperatorRemindLabel 0xAE - Отметки пути, заранее записанные в прибор для напоминания оператору (дефектные рельсы и другие)

  edOperatorRemindLabel = packed record // 261 байт ?
    Len: Byte;   // Текст, Код дефекта или Группа
    Rail: TRail;  // Нить
    LabelType: Byte; // Тип отметки: 0 – не заданно; 1 – дефект; 2 – ОДР; 3 - подозрение на дефект
    Length: Integer; // Длина, мм
    Depth: Integer; // Глубина, мм
    Text: array [0 .. 255] of Char;
  end;

  pedOperatorRemindLabel = ^edOperatorRemindLabel;

  // Событие: EID_ACData 0xBC - Данные акустического контакта

  edACData = packed record // 1 байт ?

    Data: Byte; // Данные:
                // 7 6 5 4 3 2 1 0
                // │ │ │ │ │ │ │ │
                // │ │ └─┼─┴─┘ ├─┘
                // │ │   │     └─── не используется
                // │ │   └───────── номер канала: 0…15 ультразвук
                // │ └───────────── нить: 1 - правая / 0 - левая
                // └─────────────── состояние АК: 0 – нет; 1 - есть
  end;

  pedACData = ^edACData;

  // Событие: EID_SmallDate 0xBD; // Малое универсальное событие

  edSmallDate = packed record
    DataSize: Byte;
    DataType: Byte;
    Data: array of Byte;
  end;
  pedSmallDate = ^edSmallDate;

  // Событие: EID_RailHeadScaner 0xBE; // Данные уточняющего сканера головки рельса
{
  edRailHeadScaner_Old = packed record
    Size: Integer;
    Rail: TRail;    // Нить пути выполнения контроля: левая / правая
    WorkSide: Byte; // Рабочая грань: сторона сканера 1 / сторона сканера 2
    ThresholdUnit: Byte; // Единицы измерения порога формирования результирующего изобр
    ThresholdValue: Byte; // Порог формирования результирующего изображения: дБ или % (
    OperatingTime: Integer; // Время работы со сканером: сек.
    CodeDefect: THeaderBigStr; // Заключение: код дефекта
    Sizes: THeaderBigStr; // Заключение: размеры
    Comments: THeaderBigStr; // Заключение: комментарии
    Cross_Cross_Section_Image_Size: Integer; // Поперечное сечение - размер изображения в формате PNG
    Vertically_Cross_Section_Image_Size: Integer; // Вертикальное сечение - размер изображения в формате PNG
    Horizontal_Cross_Section_Image_Size: Integer; // Горизонтальное сечение - размер изображения в формате PNG
    Cross_Cross_Section_Image: array of Byte; // Изображение в формате PNG
    Vertically_Cross_Section_Image: array of Byte; // Изображение в формате PNG
    Horizontal_Cross_Section_Image: array of Byte; // Изображение в формате PNG
  end;
  pedRailHeadScaner_Old = ^edRailHeadScaner_Old;


  edRailHeadScaner_ = packed record
    Size: Integer;
    Rail: TRail;    // Нить пути выполнения контроля: левая / правая
    WorkSide: Byte; // Рабочая грань: сторона сканера 1 / сторона сканера 2
    ThresholdUnit: Byte; // Единицы измерения порога формирования результирующего изобр
    ThresholdValue: Byte; // Порог формирования результирующего изображения: дБ или % (
    OperatingTime: Integer; // Время работы со сканером: сек.
    CodeDefect: THeaderBigStr; // Заключение: код дефекта
    Sizes: THeaderBigStr; // Заключение: размеры
    Comments: THeaderBigStr; // Заключение: комментарии
    Cross_Cross_Section_Image_Size: Integer; // Поперечное сечение - размер изображения в формате PNG
    Vertically_Cross_Section_Image_Size: Integer; // Вертикальное сечение - размер изображения в формате PNG
    Horizontal_Cross_Section_Image_Size: Integer; // Горизонтальное сечение - размер изображения в формате PNG
    Zero_Probe_Section_Image_Size: Integer; // Донный сигнал - размер изображения в формате PNG
    Cross_Cross_Section_Image: array of Byte; // Изображение в формате PNG
    Vertically_Cross_Section_Image: array of Byte; // Вертикальное сечение - изображение в формате PNG
    Horizontal_Cross_Section_Image: array of Byte; // Горизонтальное сечение - изображение в формате PNG
    Zero_Probe_Section_Image: array of Byte; // Донный сигнал - Изображение в формате PNG
  end;
  pedRailHeadScaner_ = ^edRailHeadScaner_;


  }
  // Событие: EID_SensAllowedRanges 0xBF; // Таблица разрешенных диапазонов Ку

  tSensAllowedRangesItem = packed record
    Channel: Byte;
    MinKu: ShortInt;
    MaxKu: ShortInt;
  end;

  edSensAllowedRanges = packed record
    ChCount: Byte;
    Ranges: array of tSensAllowedRangesItem;
  end;
  pedSensAllowedRanges = ^edSensAllowedRanges;

  // Событие: EID_AScanFrame 0xC6; // Кадр А-развертки

  tAScanFrame_Type1 = packed record
    Rail: TRail; // Сторона
    Channel: Byte; // Канал оценки
    ScanSurface: Byte; // Номер поверхности сканирования рельса
    TimeDelay: Word; // Время в призме, мкс / 10
    Att: ShortInt; // Аттенюатор, дБ
    Ku: ShortInt; // Значение условной чувствительности, дБ
    TVG: ShortInt; // Значение ВРЧ, мкс
    StGate: Byte; // Начало строба АСД, мкс
    EdGate: Byte; // Конец строба АСД, мкс
    ImageData: array of Byte; // Изображение в формате PNG (Size - 10)
  end;

  edAScanFrame = packed record
    DataSize: Integer;
    DataType: Byte;
    Data: tAScanFrame_Type1;
  end;
  pedAScanFrame = ^edAScanFrame;

  // Событие: EID_MediumDate 0xC7; // Среднее универсальное событие

  edMediumDate = packed record
    DataSize: Word;
    DataType: Byte;
//    Data: array of Byte;
    Data: array [0..65535] of Byte;
  end;
  pedMediumDate = ^edMediumDate;

  // Событие: EID_BigDate 0xC8; // Большое универсальное событие

  edBigDate = packed record
    DataSize: Integer;
    DataType: Byte;
    Data: array of Byte;
  end;
  pedBigDate = ^edBigDate;

  // Событие: EID_SensFail 0xC5;   // Отклонение условной чувствительности от нормативного значения

  edSensFail = packed record // 6 байт ?
    Rail: TRail;  // Нить
    EvalCh: Byte; // Канал контроля с неверным/верным значением Ку
    DeltaKu: Integer; // Величина отклонения в дБ
  end;

  pedSensFail = ^edSensFail;

  // Событие: EID_TextLabel
  //          EID_ChangeOperatorName

  edTextLabel = packed record // max 513 байт
    Len: Byte;
    Text: array [0 .. 255] of Char;
  end;

  pedTextLabel = ^edTextLabel;

  // Событие: EID_Switch

  edSwitch = edTextLabel;
  pedSwitch = ^edSwitch;

  // Событие: EID_DefLabel

  edDefLabel = packed record // max 514 байт
    Len: Byte;
    Rail: TRail;
    Text: array [0 .. 255] of Char;
  end;

  pedDefLabel = ^edDefLabel;

  // Событие: EID_StBoltStyk
  //          EID_EndBoltStyk
  // нет данных

  // Событие: EID_Time

  edTime = packed record // 2 байта
    Hour: Byte;
    Minute: Byte;
  end;

  pedTime = ^edTime;


  // Событие: EID_QualityCalibrationRec // Качество настройки каналов контроля

  edQualityCalibrationRec = packed record // 2 байта
    Rail: TRail;
    RecType: Byte;
    Len: Byte;
    Data: array [0..11] of Byte;
  end;
  pedQualityCalibrationRec = ^edQualityCalibrationRec;

  // Событие: EID_Sensor1

  edSensor1 = packed record // 2 байта
    Rail: TRail;
    State: Boolean;
  end;
  pedSensor1 = ^edSensor1;

  // EID_CheckSum

  edChecksum = packed record // 4 байта
    Value: Word;
    NotUse: Word;
  end;

  pedChecksum = ^edChecksum;

  // Событие: EID_AirBrush

  edAirBrushItem = packed record // 11 байт (Зона на В-развертке)
    ScanCh: Byte; // Канал развертки
    StartDisCoord: Integer; // Дисплейная координата начала зоны
    EndDisCoord: Integer; // Дисплейная координата конца зоны
    StartDelay: Byte; // Задержка начала зоны
    EndDelay: Byte; // Задержка конца зоны
  end;

  TABItemsList = array of edAirBrushItem;

  edAirBrush = packed record // 182 байта
    Rail: TRail;             // Нить
    DisCoord: Integer;       // Дисплейная координата отметки
    Count: Byte;             // Количество элементов
    Items: array [0..15] of edAirBrushItem; // Зоны на В-развертке по которым было принято решение
  end;
  pedAirBrush = ^edAirBrush;

  // Событие: EID_AirBrush2

  edAirBrushItem2 = packed record // 15 байт (Зона на В-развертке)
    ScanCh: Byte; // Канал развертки
    StartDisCoord: Integer; // Дисплейная координата начала зоны
    EndDisCoord: Integer; // Дисплейная координата конца зоны
    StartDelay: Byte; // Задержка начала зоны
    EndDelay: Byte; // Задержка конца зоны
    Debug: Integer; // Для отладки
  end;

  TABItemsList2 = array of edAirBrushItem2;

  edAirBrush2 = packed record // 246 байт
    Rail: TRail;             // Нить
    DisCoord: Integer;       // Дисплейная координата отметки
    Count: Byte;             // Количество элементов
    Items: array [0..15] of edAirBrushItem2; // Зоны на В-развертке по которым было принято решение
  end;
  pedAirBrush2 = ^edAirBrush2;

  tAirBrushItem = record

    PaintDisCoord: Integer;       // Дисплейная координата для краскоотметки
    SendCmdDisCoord: Integer;     // Дисплейная координата передачи команды на краскоотметку
    Skip: Boolean;
    Data: edAirBrush2;
  end;


  // Событие: EID_PaintMarkRes

  edAirBrushJob = packed record // 8 байт
    Rail: TRail;             // Нить
    DisCoord: Integer;       // Дисплейная координата отметки
    ErrCod: Byte;            // Код ошибки
    Delta: Word;             // Ошибка по координате
  end;
  pedAirBrushJob = ^edAirBrushJob;

  // Событие: EID_AirBrushTempOff - Временное отключение Краскоотметчика

  edAirBrushTempOff = packed record // 5 байт
    State: Byte;             // Состояние
    DisCoord: Integer;       // Дисплейная координата отметки
  end;
  pedAirBrushTempOff = ^edAirBrushTempOff;

  // Событие: EID_AlarmTempOff - Временное отключение АСД по всем каналам

  edAlarmTempOff = packed record // 5 байт
    State: Byte;             // Состояние: 0 - ВЫКЛ, 1 - ВКЛ
    DisCoord: Integer;       // Не используется (Дисплейная координата отметки)
  end;
  pedAlarmTempOff = ^edAlarmTempOff;

  // Событие: EID_GPSCoord - Географическая координата

  edGPSCoord = packed record // 8 байт
    Lat: Single; //	Широта
    Lon: Single;  // Долгота
  end;
  pedGPSCoord = ^edGPSCoord;

  edGPSCoord_ = packed record // 8 байт
    Lat: Integer; //	Широта
    Lon: Integer;  // Долгота
  end;
  pedGPSCoord_ = ^edGPSCoord_;


  // Событие: EID_GPSCoord2 - Географическая координата

  edGPSCoord2 = packed record // 8 байт
    Lat:	Single;  //	Широта
    Lon: Single;   // Долгота
    Speed: Single; // Скорость
  end;
  pedGPSCoord2 = ^edGPSCoord2;


  // Событие: EID_NORDCO_Rec - Запись NORDCO

  edNORDCO_Rec = packed record // ? байт

    DisCoord          : Integer;       // Дисплейная координата отметки
    Line              : THeaderStr;    // Название или идентификатор контролируемой линии
    Track             : THeaderStr;    // Идентификатор пути, в котором обнаружен дефект
    LocationFrom      : Single;        // Начало обнаруженного дефекта
    LocationTo        : Single;        // Конец обнаруженного дефекта. В случае точечного дефекта то же самое, как поле  "Location From"
    LatitudeFrom      : Double;        // GPS-широта для поля "Location From"
    LatitudeTo        : Double;        // GPS-широта для поля "Location To"
    LongitudeFrom     : Double;        // GPS-долгота для поля "Location From"
    LongitudeTo       : Double;        // GPS-долгота для поля "Location To"
    Type_             : THeaderStr;    // Код дефекта по UIC 712
    SurveyDate        : TDateTime;     // Дата обнаружения дефекта
    Source            : THeaderStr;    // Метод обнаружения дефекта - из списка: Ultrasonic Trolley, Visual Inspection
    Status            : THeaderStr;    // Это поле указывает, закрыт ли дефект (т.е. удален из пути) - из списка: Open, Closed
    IsFailure         : Boolean;       // Если конкретный дефект вызвало отказ, то это поле должно иметь значение TRUE. Отказом можно считать любой дефект, который вызывает ограничение скорости
    AssetType         : THeaderStr;    // Тип верхнего строения пути - из списка: Left Rail, Right Rail
    DataEntryDate     : TDateTime;     // Дата составления списка дефектов
    Size              : Single;        // Размер дефекта для  неточечного дефекта. Например, если дефект это трещина, то ее длина.
    Size2             : Single;        // Размер дефекта для  неточечного дефекта (если более чем одномерный)
    Size3             : Single;        // Размер дефекта для  неточечного дефекта (если более чем одномерный)
    UnitofMeasurement : THeaderStr;    // Ед. измерения поля "Size"
    UnitofMeasurement2: THeaderStr;    // Ед. измерения поля "Size2"
    UnitofMeasurement3: THeaderStr;    // Ед. измерения поля "Size3"
    Comment           : THeaderBigStr; // Примечание
    Operator_         : THeaderStr;    // Оператор
    Device            : THeaderStr;    // Прибор
  end;
  pedNORDCO_Rec = ^edNORDCO_Rec;

  // Событие: EID_GPSState - Состояние приемника GPS

  GPSStateReserv = array [0..5] of Byte;

  edGPSState = packed record // 9 байт
    State:	Byte;           // Состояние: 0 - данные не объективные; 1 - данные объективные
    UseSatCount: Byte;      // Количество используемых спутников
    AntennaConnected: Byte; // Антенна: 0 – не подключена; 1 - подключена
    Reserv: GPSStateReserv; // Резерв
  end;
  pedGPSState = ^edGPSState;

  // Событие: EID_Media - Медиаданные

  TMediaType = ( mtAudio = 1,
                 mtPhoto = 2,
                 mtVideo = 3);

  edMediaData = packed record
    DataType:	Byte; // Тип данных
    Size: DWord;
  end;
  pedMediaData = ^edMediaData;

  // Событие: EID_Temperature - Температура

{$IFDEF Temperature_is_Byte}
  edTemperature = packed record
    Value: Byte;
  end;
{$ENDIF}
{$IFNDEF Temperature_is_Byte}
  edTemperature = packed record
    Value: Single;
  end;
{$ENDIF}
  pedTemperature = ^edTemperature;

  // Событие: EID_DebugData - Отладочная информация

  TDebugDataArr = packed array [0..31] of Integer;

  edDebugData = packed record
    Data: TDebugDataArr;
  end;
  pedDebugData = ^edDebugData;


  edAlarmData = packed record
    Rail: TRail;
    EvalCh: Integer;
    DisCoord: Integer;
    StDisCrd: Integer;
    EdDisCrd: Integer;
  end;
  pedAlarmData = ^edAlarmData;

  // Событие: EID_PaintSystemParams - Параметры работы алгоритма краскопульта

  edPaintSystemParams = packed record
    Data: TPaintSystemParams;
  end;
  pedPaintSystemParams = ^edPaintSystemParams;

  // Событие: EID_UMUPaintJob - Задание БУМ на краскоотметку

  edUMUPaintJob = packed record
    UMUIndex: Byte;
    SysCoord: Integer;
    DisCoord: Integer;
  end;
  pedUMUPaintJob = ^edUMUPaintJob;

  // Событие: EID_MediumDate - Среднее универсальное событие
(*
  edMediumDateCommon = packed record
    Size: Word;
    Type_: Byte;
    Data: array [0..65535] of Byte;
  end;
  pedMediumDateCommon = ^edMediumDateCommon;
  *)
  // Тип данных = 0x02 - Расширенная отметка начало СП (по ТЗ 2018)

  edStartSwitchEx = packed record
    Size: Word;
    Type_: Byte;
    SwitchType: Byte; // Тип СП
    SwitchDirectionType: Byte; //	Направленее СП
    SwitchFixedDirection: Byte; // Установленное направление
    DeviceDirection: Byte; //Направление контроля
    DeviceSide: Byte; //Контролируемая нить пути
    Len: Byte;        // Текст
    Text: array [0 .. 512] of Char;
  end;
  pedStartSwitchEx = ^edStartSwitchEx;

  // Тип данных = 0x03 - Расширенная отметка конец СП (по ТЗ 2018)

  edEndSwitchEx = packed record
    Size: Word;
    Type_: Byte;
    Len: Byte;       // Текст
    Text: array [0 .. 255] of Char;
  end;
  pedEndSwitchEx = ^edEndSwitchEx;

  // ------------------------< Данные дописываемые к концу файла >---------------------------------------

  TFileEvent = packed record
    ID: Byte;
    OffSet: Integer;
    SysCoord: Integer;
    DisCoord: Integer;
    Data: TEventData;
  end;

  // ?
  TFileNotebook_ver_5 = packed record
    ID: Byte; // Тип: 1 - Ведомость контроля / 2 - Запись блокнота.
    Rail: TRail; // Нить: 0 - левая / 1 - правая
    DisCoord: Integer; // Дисплейная координата
    StDisCoord: Integer; // Дисплейная координата
    EdDisCoord: Integer; // Дисплейная координата

    Defect: string[64]; // Дефект - Код / Смотреть как
//    Defect: THeaderStr;
    BlokNotText: string[250]; // Текст блокнота

    ManName: string[64]; // Расшифровщик ФИО
    Privazka: string[100]; // Привязка
    Prim: string[100]; // Дефект - Примечание
    DT: TDateTime;

    LeftKm1: Integer; // Километр
    LeftKm2: Integer; // Километр
    LeftPk1: Integer; // Пикет
    LeftPk2: Integer; // Пикет

    RightKm1: Integer; // Километр
    RightKm2: Integer; // Километр
    RightPk1: Integer; // Пикет
    RightPk2: Integer; // Пикет

    Zveno1: Integer; // Звено1
    SysCoord: Integer; // Звено2
    LeftMM: Integer; // Миллиметр (от пикета)
    RightMM: Integer; // Миллиметр (от пикета)
    Chema: Integer; // Схема рельса: 1 - чистый; 2 - болтовой; 3 - сварка.
    Zoom: Integer; // Масштаб
    Reduction: Byte; // Сведение: 0 - Выключено / 1 - Тип №1 / 1 - Тип №2

    ShowChGate_: Boolean;
    ShowChSettings_: Boolean;
    ShowChInfo_: Boolean;

    ViewMode: Byte; // Режим отображения: 1 - В развертка / 2 - В виде Рельса
    DrawRail: Byte; { TDrawData } // Отображаемая нить 0 - Левая 1 - Правая 2 - Обе
    AmplTh: Byte; // Порог отображения
    AmplDon: Byte; // Амплитуда донного сигнала 0 - Выкл
    ViewChannel: Byte; // Каналы: 0 - Все / 1 - Наезжающие / 2 - Отъезжающие
    ViewLine: Byte;

    PathNum: Integer; // Путь
    Peregon: string[20];
    FindAt: Integer; // Обнаружен при ..   0 - замена; 1 - срочный; 2 - в течении суток; 3 - 3-5 суток ; 4 - плановый
    ViewType: Integer; // Осмотр ..          0 - при расшифровке; 1 - оператором в пути

    REZERW: array [0..31] of Byte;
  end;

  TFileNotebook_ver_6 = packed record
    ID: Byte; // Тип: 1 - Ведомость контроля / 2 - Запись блокнота.
    Rail: TRail; // Нить: 0 - левая / 1 - правая
    DisCoord: Integer; // Дисплейная координата
    Defect: THeaderStr;
    BlokNotText: THeaderBigStr; // Текст блокнота
    DT: TDateTime;
    SysCoord: Integer; // Звено2
    Zoom: Integer; // Масштаб
    Reduction: Byte; // Сведение: 0 - Выключено / 1 - Тип №1 / 1 - Тип №2
    ShowChGate_: Boolean;
    ShowChSettings_: Boolean;
    ShowChInfo_: Boolean;
    ViewMode: Byte; // Режим отображения: 1 - В развертка / 2 - В виде Рельса
    DrawRail: Byte; { TDrawData } // Отображаемая нить 0 - Левая 1 - Правая 2 - Обе
    AmplTh: Byte; // Порог отображения
    AmplDon: Byte; // Амплитуда донного сигнала 0 - Выкл
    ViewChannel: Byte; // Каналы: 0 - Все / 1 - Наезжающие / 2 - Отъезжающие
    ViewLine: Byte;
  end;

  TFileNotebook_ver_7 = packed record
    ID: Byte; // Тип: 1 - Ведомость контроля / 2 - Запись блокнота / 3 - NORDCO
    Rail: TRail; // Нить: 0 - левая / 1 - правая
    DisCoord: Integer; // Дисплейная координата
    Defect: THeaderStr;
                      // NORDCO::Type - Код дефекта по UIC 712
    BlokNotText: THeaderBigStr; // Текст блокнота
                      // NORDCO::Comment - Примечание
    DT: TDateTime;
                 // NORDCO::SurveyDate - Дата обнаружения дефекта
                 // NORDCO::DataEntryDate - Дата составления списка дефектов
    SysCoord: Integer; // Звено2
    Zoom: Integer; // Масштаб
    Reduction: Byte; // Сведение: 0 - Выключено / 1 - Тип №1 / 1 - Тип №2
    ShowChGate_: Boolean;
    ShowChSettings_: Boolean;
    ShowChInfo_: Boolean;
    ViewMode: Byte; // TBScanViewMode
    DrawRail: Byte; { TDrawData } // Отображаемая нить 0 - Левая 1 - Правая 2 - Обе
    AmplTh: Byte; // Порог отображения
    AmplDon: Byte; // Амплитуда донного сигнала 0 - Выкл
    ViewChannel: Byte; // Каналы: 0 - Все / 1 - Наезжающие / 2 - Отъезжающие
    ViewLine: Byte;

    // Режим "В виде Рельса" (ВКЛ/ВЫКЛ) задается таким образом:
    // 1. Флаг что режим сохранен - Line[0] = 0x55
    // 2. Значение                - Line[1] = 0 ВЫКЛ / 1 ВКЛ

                   // NORDCO ---------------------
    Line              : THeaderStr; // Название или идентификатор контролируемой линии
    Track             : THeaderStr; // Идентификатор пути, в котором обнаружен дефект
    LocationFrom      : Single; // Начало обнаруженного дефекта
    LocationTo        : Single; // Конец обнаруженного дефекта. В случае точечного дефекта то же самое, как поле  "Location From"
    LatitudeFrom      : Double; // GPS-широта для поля "Location From"
    LatitudeTo        : Double; // GPS-широта для поля "Location To"
    LongitudeFrom     : Double; // GPS-долгота для поля "Location From"
    LongitudeTo       : Double; // GPS-долгота для поля "Location To"
    AssetType         : THeaderStr; // Тип верхнего строения пути - из списка: Left Rail, Right Rail
    Source            : THeaderStr; // Метод обнаружения дефекта - из списка: Ultrasonic Trolley, Visual Inspection
    Status            : THeaderStr; // Это поле указывает, закрыт ли дефект (т.е. удален из пути) - из списка: Open, Closed
    IsFailure         : Boolean; // Если конкретный дефект вызвало отказ, то это поле должно иметь значение TRUE. Отказом можно считать любой дефект, который вызывает ограничение скорости
    Size              : Single;                                                     // Размер дефекта для  неточечного дефекта. Например, если дефект это трещина, то ее длина.
    Size2             : Single;                                                     // Размер дефекта для  неточечного дефекта (если более чем одномерный)
    Size3             : Single;                                                     // Размер дефекта для  неточечного дефекта (если более чем одномерный)
    UnitofMeasurement : THeaderStr;                                                     // Ед. измерения поля "Size"
    UnitofMeasurement2: THeaderStr;                                                     // Ед. измерения поля "Size2"
    UnitofMeasurement3: THeaderStr;                                                     // Ед. измерения поля "Size3"
    Operator_         : THeaderStr;                                                     // Оператор
    Device            : THeaderStr;                                                     // Прибор
  end;

  TFileNotebook = TFileNotebook_ver_7;

  pFileNotebook = ^TFileNotebook;

  TExHeaderHead = packed record
    FFConst: array [0 .. 13] of Byte; // Константы - 14 x 0xFFH
    DataSize: Integer;                // Размер заголовка
    DataVer: Integer;                 // Формат данных = 5
  end;

  TExHeader_Ver_7 = packed record
    FFConst: array [0..13] of Byte;     // Константы - 14 x 0xFFH
    DataSize: Integer;                  // Размер заголовка
    DataVer: Integer;                   // Формат данных = 7
    EndKM: Integer;                     // Конечный километр или Значение Chainage
    EndPK: Integer;                     // Конечный пикет
    EndMM: Integer;                     // Миллиметр от последнего столба
    CoordReserv: array [0..63] of Byte; // Резерв 0
    EndSysCoord: Integer;               // Конечная системная координата
    EndDisCoord: Integer;               // Конечная дисплейная координата
    Reserv1: array [0..63] of Byte;     // Резерв 1

    EventDataVer: Integer;  // Версия Формата данных таблицы значимых событий файла (Версия: 5)
    EventItemSize: Integer; // Размер 1 элемента таблицы значимых событий файла
    EventOffset: Integer;   // Ссылка на таблицу значимых событий файла
    EventCount: Integer;    // Количество элементов таблицы значимых событий файла

    NotebookDataVer: Integer;  // Версия Формата данных таблицы ведомостей контроля и записей блокнота (Версия: 5)
    NotebookItemSize: Integer; // Размер 1 элемента таблицы ведомостей контроля и записей блокнота
    NotebookOffset: Integer;   // Ссылка на таблицу ведомостей контроля и записей блокнота
    NotebookCount: Integer;    // Количество элементов таблицы ведомостей контроля и записей блокнота

    Reserv2: array [0..127] of Byte; // Резерв 2
  end;

  TExHeader_Ver_8 = packed record
    FFConst: array [0..13] of Byte;     // Константы - 14 x 0xFFH
    DataSize: Integer;                  // Размер заголовка
    DataVer: Integer;                   // Формат данных = 7
    EndMRFCrd: TMRFCrd;                 // Размер 12 байт
    EndCaCrd: TCaCrd;                   // Размер 8 байт
    CoordReserv: array [0..55] of Byte; // Резерв 0
    EndSysCoord: Integer;               // Конечная системная координата
    EndDisCoord: Integer;               // Конечная дисплейная координата
    Reserv1: array [0..63] of Byte;     // Резерв 1

    EventDataVer: Integer;  // Версия Формата данных таблицы значимых событий файла (Версия: 5)
    EventItemSize: Integer; // Размер 1 элемента таблицы значимых событий файла
    EventOffset: Integer;   // Ссылка на таблицу значимых событий файла
    EventCount: Integer;    // Количество элементов таблицы значимых событий файла

    NotebookDataVer: Integer;  // Версия Формата данных таблицы ведомостей контроля и записей блокнота (Версия: 5)
    NotebookItemSize: Integer; // Размер 1 элемента таблицы ведомостей контроля и записей блокнота
    NotebookOffset: Integer;   // Ссылка на таблицу ведомостей контроля и записей блокнота
    NotebookCount: Integer;    // Количество элементов таблицы ведомостей контроля и записей блокнота

    Reserv2: array [0..127] of Byte; // Резерв 2:
                                     // для Авикон-31: 0 байт = 0xFF - Требуется выполнить анализ файла;
                                     //                       = 0x01 - Анализ файла выполнен алгоритм №1
                                     //                       = 0x02 - Анализ файла выполнен алгоритм №2
                                     //                       = 0x03 - Анализ файла выполнен алгоритм №3
                                     // для Авикон-31: 1 байт = 0xFF - Файл без скачков координаты;
                                     //                       = 0x01 - Файл c cкачком/скачками координаты
  end;

  TExHeader = TExHeader_Ver_8;


//------------------------< GPS >-----------------------------------------------------------

  TGPSPoint = record
    State: Boolean;
    Lat: Single;
    Lon: Single;
    SatCount: Integer;
    Speed: Single;
    DisCoord: Integer;

    SysCoord: Integer;
    {
    KM: Integer;
    Metre: Single;
    LabelExists: Boolean;
    LabelText: string;
    StolbExists: Boolean;
    LeftKm: Smallint;
    LeftPk: Byte;
    RightKm: Smallint;
    RightPk: Byte;
    }
  end;

TExtGPSPoint = record
    State: Boolean;
    Lat: single;//Double;
    Lon: single;//Double;
    SatCount: Integer;
    Speed: Single;
    DisCoord: Integer;
  end;

// ---------< Кооординатный столб >--------------------------------------------------------------------

  TStolbSide = (ssLeft, ssRight);

  TStolbSideData = record
    Km: Integer;
    Pk: Integer;
  end;

  TStolb = array [TStolbSide] of TStolbSideData;

  TMRFCrdParams = record // Информация о двух столбах расположенных слево и справо от точки для Метрической РФ системы координат
    LeftStolb: TStolb;
    ToLeftStolbMM: Int64;
    LeftIdx: Integer;
    RightStolb: TStolb;
    ToRightStolbMM: Int64;
    RightIdx: Integer;
  end;

  TCaCrdParams = record // Информация о двух столбах расположенных слево и справо от точки для Метрической РФ системы координат
    LeftPostVal: TCaCrd;
    ToLeftPostMM: Extended;
    LeftIdx: Integer;
    RightPostVal: TCaCrd;
    ToRightPostMM: Extended;
    RightIdx: Integer;
    Sys: TCoordSys;
  end;

  TCrdParams = record // Информация о двух столбах расположенных слево и справо от точки
    CaCrdParams: TCaCrdParams;
    MRFCrdParams: TMRFCrdParams;
    Sys: TCoordSys;
  end;

  TRealCoord = record
    MRFCrd: TMRFCrd;
    CaCrd: TCaCrd;
    Sys: TCoordSys;
    ToLeftPostMM: Extended;
  end;

  TEchoBlock = array[0..7] of packed record
                                T: Byte;
                                A: Byte;
                              end;

  TDestDat = packed record
//    DelayOffset: array [1..16] of Integer;
    Delay: array [1..16] of Byte;
    Ampl: array [1..16] of Byte;
    Count: Integer;
//    ZerroFlag: Boolean;
//    ZerroFlag2: Boolean;
  end;

  TCurEcho = array [TRail, 0..15] of TDestDat;

  TSortOf2ndAScan = (leftORRightHand,ForwardORBackward);

  // -------------< Система координат >-----------------------------------

  procedure SetCrdToStrCaptions(KM, PK, Metr, SantiMetr, Milimetr, Imperial, Metric0_1km, Metric1km: string);
  function PostMMLen(CoordSys: TCoordSys): Integer;

  // -------------< Система координат, Метрическая РФ >-----------------------------------

  function GetPrevMRFPost(Post: TMRFCrd; MoveDir: Integer): TMRFCrd; // Получить дредыдущий столб
  function GetNextMRFPost(Post: TMRFCrd; MoveDir: Integer): TMRFCrd;
  function MRFCrdParamsToCrd(CoordParams: TMRFCrdParams; MoveDir: Integer): TMRFCrd;
  function MRFCrdToStr(Data: TMRFCrd; Index: Integer = 2) : string;

  // -------------< Система координат, Имперская >----------------------------------------------------

  function GetPrevCaPost(Sys: TCoordSys; Val: TCaCrd; MoveDir: Integer): TCaCrd; // Получить дредыдущий столб
  function GetNextCaPost(Sys: TCoordSys; Val: TCaCrd; MoveDir: Integer): TCaCrd;
  function CaCrdParamsToCrd(CoordParams: TCaCrdParams; MoveDir: Integer): TCaCrd;
  function CaCrdToStr(Sys: TCoordSys; Val: TCaCrd; ToLeftPostMM: Extended = 0; ShowAll: Boolean = false; AddDatainCoord: Boolean = false): string;
  function CaCrdToVal(Sys: TCoordSys; Val: TCaCrd): Single;
//  function CaCrdToStr(Val: TCaCrd): string; overload;
  function CaCrdToStr0(Sys: TCoordSys; Val: TCaCrd): string;
  function GetMMLenFromCaPost(Sys: TCoordSys; Post: TCaCrd): Extended; // Растояние от столба
  function CaCrdToMM(Sys: TCoordSys; Val: TCaCrd): Extended;
  function GetMMLenFromTwoCaCrd(Sys: TCoordSys; FromCaCrd, ToCaCrd: TCaCrd): Extended;
  function StrToCaCrd(CoordSys: TCoordSys; Text: string): TCaCrd;

  // -------------------------------------------------------------------------------------------------

  function CrdParamsToRealCrd(CrdParams: TCrdParams; MoveDir: Integer): TRealCoord;
  function RealCrdToStr(Crd: TRealCoord; Index: Integer = 2) : string;
  function RealCoordToReal_(Crd: TRealCoord): Integer;

  // -----------< Работа с текстом >-------------------------------------------------------

  function HeaderStrToString(Text: THeaderStr): string;
  function StringToHeaderStr(Text: string): THeaderStr;
  function HeaderBigStrToString(Text: THeaderBigStr): string;
  function HeaderBigStrToStringForReport(Text: THeaderBigStr): string;
  function LenHeaderBigStr(Text: THeaderBigStr):Integer;
  function HeaderBigStrToString_(Text: THeaderBigStr): string;
  function StringToHeaderBigStr(Text: string): THeaderBigStr;
  function GetDeviceNameTextId(DeviceID: TAviconID): string;
  function GetInspMethodTextId(Metod: TInspectionMethod; FullName: Boolean): string;
  function GetInspZoneTextId(Zone: TInspectionZone; FullName: Boolean): string;

  // ---------- Tools ----------------------------------------------------------------------

  function GetRealCrd_inMM(ScanCh: TScanChannelItem; Delay: Byte; isEGOUSW_BHead: Boolean): Single;  // Получение координаты отражателя от центра ИС [мм]
  function GetRealCrd_inSysCrd(ScanCh: TScanChannelItem; ScanStep: Integer; Delay: Integer; isEGOUSW_BHead: Boolean): Single; // Получение координаты отражателя от центра ИС [сист. коорд.]
  function GetMoveCrd_inSysCrd(ScanCh: TScanChannelItem; ScanStep: Integer; Delay: Integer): Single;
  function DelayToMMLen(ScanCh: TScanChannelItem; Delay: Byte): Single; // По лучу
  function UpgradeStr(Src: string; Ch: Char; Len: Integer): string;
  function UpgradeInt(Int: Integer; Ch: Char; Len: Integer): string;

  // ---------------------------------------------------------------------------------------

  function GetSurf(ChID: Integer): Integer;
  function SwitchIDsToText(A31: Boolean; IDs: edStartSwitchEx): string;

  function InvertGPSSingle(Src: Single): Single;
  function GPSSingleToText( A: Double; Lat: Boolean ): string; //  Широта = Latitude

  procedure GetSystemPos(DeviceID: TAviconID; PaintSystemParams: TPaintSystemParams; ControlDirection: Byte; SensorPos: PInteger; AirBrushPos: PInteger);
//  function GetSurfaceText(Rail: TRail; Gran: TGran; LangTable: TLanguageTable): string;
{$IFNDEF VMT_EGO}
  function GetSurfaceText(Rail: TRail; Gran: TGran; FileHeader: TFileHeader): string;
{$ENDIF}
  function ConditionIsWork(MoveDir: Integer; WorkRailTypeA: Byte { 0 – левая }): Boolean;

const
  DeviceUnitTypeToTextId: array [TDeviceUnitType] of string = ('UpUnit', 'DownUnit', 'MiddleUnit', 'FlawDetectorNumber');

  function GetString_from1(Len: Byte; Text: array of Char): string;

implementation

uses
  MMSystem, Math, LanguageUnit; // , ConfigUnit;

var
 FKM_Name: string;
 FPK_Name: string;
 FMetr_Name: string;
 FSantiMetr_Name: string;
 FMilimetr_Name: string;
 FImperial_Name: string;
 FMetric0_1km_Name: string;
 FMetric1km_Name: string;

procedure SetCrdToStrCaptions(KM, PK, Metr, SantiMetr, Milimetr, Imperial, Metric0_1km, Metric1km: string);
begin
 FKM_Name:= KM;
 FPK_Name:= PK;
 FMetr_Name:= Metr;
 FSantiMetr_Name:= SantiMetr;
 FMilimetr_Name:= Milimetr;
 FImperial_Name:= Imperial;
 FMetric0_1km_Name:= Metric0_1km;
 FMetric1km_Name:= Metric1km;
end;

function PostMMLen(CoordSys: TCoordSys): Integer;
begin
  case CoordSys of
     csMetricRF: Result:= 100000;
     csImperial: Result:= 30480;
  csMetric0_1km: Result:= 100000;
    csMetric1km: Result:= 100000;
 csMetric1kmTurkish: Result:= 100000;
          else Result:= 100000;
  end;
end;

  // -------------< Система координат, Метрическая РФ >-----------------------------------

function GetPrevMRFPost(Post: TMRFCrd; MoveDir: Integer): TMRFCrd; // Получить дредыдущий столб
begin
  if MoveDir > 0 then
  begin
    Dec(Post.Pk);
    if Post.Pk = 0 then
    begin
      Post.Pk:= 10;
      Dec(Post.Km);
    end;
  end
  else
  begin
    Inc(Post.Pk);
    if Post.Pk = 11 then
    begin
      Post.Pk:= 1;
      Inc(Post.Km);
    end;
  end;
  Result:= Post;
end;

function GetNextMRFPost(Post: TMRFCrd; MoveDir: Integer): TMRFCrd;
begin
  if MoveDir > 0 then
  begin
    Inc(Post.Pk);
    if Post.Pk = 11 then
    begin
      Post.Pk:= 1;
      Inc(Post.Km);
    end;
  end
  else
  begin
    Dec(Post.Pk);
    if Post.Pk = 0 then
    begin
      Post.Pk:= 10;
      Dec(Post.Km);
    end;
  end;
  Result:= Post;
end;

function MRFCrdParamsToCrd(CoordParams: TMRFCrdParams; MoveDir: Integer): TMRFCrd;
begin
  Result.Km:= CoordParams.LeftStolb[ssRight].Km;
  Result.Pk:= CoordParams.LeftStolb[ssRight].Pk;
  if MoveDir > 0 then begin if CoordParams.ToLeftStolbMM < MaxInt then Result.mm:= CoordParams.ToLeftStolbMM; end
                 else begin if CoordParams.ToLeftStolbMM < MaxInt then Result.mm:= CoordParams.ToRightStolbMM; end;
end;

function MRFCrdToStr(Data: TMRFCrd; Index: Integer = 2) : string;
var
  M, MM, Crd, Gekto_1, Gekto_2: Integer;

begin
//  M:= Round(1000 * Frac(Data.mm div 1000 / 1000));
  M:= Data.mm div 1000;
  MM:= Round(1000 * Frac(Data.mm / 1000));

  case Index of
    0: Result:= Format('%D ' + FKM_Name + ' %D ' + FPK_Name + ' %D ' + FMetr_Name + ' %D ' + FMilimetr_Name, [Data.Km, Data.Pk, M, MM]);
    1: Result:= Format('%D ' + FKM_Name + ' %D ' + FPK_Name + ' %D ' + FMetr_Name + ' %D ' + FSantiMetr_Name, [Data.Km, Data.Pk, M, MM div 10]);
    2: Result:= Format('%D ' + FKM_Name + ' %D ' + FPK_Name + ' %D ' + FMetr_Name + '', [Data.Km, Data.Pk, M]);
    3: Result:= Format('%D / %D / %D', [Data.Km, Data.Pk, M]);


    4: begin // Гектометр
         Crd:= Data.Km * 1000 + (Data.Pk - 1) * 100 + M;

         Gekto_1:= Trunc(Crd / 100);
         Gekto_2:= Round(100 * Frac(Crd / 100));

         Result:= Format('%d+%d', [Gekto_1, Gekto_2]);
       end;

    5: begin // Километр
         Result:= Format('%.3f', [(Data.Km * 1000 + (Data.Pk - 1) * 100 + M) / 1000]);
       end;

    6: begin // Метр
         Result:= Format('%d', [Data.Km * 1000 + (Data.Pk - 1) * 100 + M]); // Гектометр
       end;

  end;
end;

  // -------------< Система координат, Имперская >----------------------------------------------------

function GetPrevCaPost(Sys: TCoordSys; Val: TCaCrd; MoveDir: Integer): TCaCrd; // Получить столб слева от точки
begin
  if MoveDir > 0 then
  begin                       // OK
    Result.XXX:= Val.XXX;
    Result.YYY:= 0;
  end
  else
  begin                       // OK
    Result.XXX:= Val.XXX;
    Result.YYY:= 0;
  end;
end;

function GetNextCaPost(Sys: TCoordSys; Val: TCaCrd; MoveDir: Integer): TCaCrd;
var
  S: string;
  I: Integer;

begin
  if MoveDir > 0 then
  begin
    Result.XXX:= Val.XXX + 1; // OK
    Result.YYY:= 0;
  end
  else
  begin
    Result:= Val;
    if Result.YYY = 0 then Result.XXX:= Val.XXX - 1
                      else Result.YYY:= 0;
  end;
end;

function CaCrdToMM(Sys: TCoordSys; Val: TCaCrd): Extended;
var
  t1: Extended;
  t2: Extended;

begin
  case Sys of
     csImperial:
                Result:= {Trunc(}Int64(Val.XXX) *   30480 + Val.YYY * 30.480{)};
     csMetric0_1km, csMetric1km, csMetric1kmTurkish:
                Result:= {Trunc(}Int64(Val.XXX) *  100000 + Val.YYY *    100{)};
     else
                Result:= 0;
  end;
end;

function GetMMLenFromTwoCaCrd(Sys: TCoordSys; FromCaCrd, ToCaCrd: TCaCrd): Extended;
begin
  Result:=  CaCrdToMM(Sys, ToCaCrd) - CaCrdToMM(Sys, FromCaCrd);
end;


function MMToCaCrd(CoordSys: TCoordSys; MMVal: Extended): TCaCrd;
begin
  case CoordSys of
     csImperial: begin
                   Result.XXX:= Trunc(MMVal / 30480);
                   Result.YYY:= Trunc(1000 * Frac(MMVal / 30480));
                   if Result.YYY > 999 then Result.YYY:= 999;
                 end;
  csMetric0_1km,csMetric1kmTurkish,csMetric1km:
                 begin
                   Result.XXX:= Trunc(MMVal / 100000);
                   Result.YYY:= Trunc(1000 * Frac(MMVal / 100000));
                   if Result.YYY > 999 then Result.YYY:= 999;
                 end;
            else begin
                   Result.XXX:= 0;
                   Result.YYY:= 0;
                 end;
  end;
end;

function AddMMToCaCrd(CoordSys: TCoordSys; Chainage: TCaCrd; MMVal: Extended): TCaCrd;
begin
  try
    Result:= MMToCaCrd(CoordSys, CaCrdToMM(CoordSys, Chainage) + MMVal);
  except
//    Result:= 0;
  end;
end;

function CaCrdParamsToCrd(CoordParams: TCaCrdParams; MoveDir: Integer): TCaCrd;
begin
  if MoveDir > 0 then Result:= AddMMToCaCrd(CoordParams.Sys, CoordParams.LeftPostVal, CoordParams.ToLeftPostMM)
                 else Result:= AddMMToCaCrd(CoordParams.Sys, CoordParams.RightPostVal, CoordParams.ToRightPostMM);
end;

function CaCrdToStr(Sys: TCoordSys; Val: TCaCrd; ToLeftPostMM: Extended = 0; ShowAll: Boolean = false; AddDatainCoord: Boolean = false): string;
var
  XXX: string;
  YYY: string;
  ZZZ: string;
  X: string;
  Y: string;
  Sign: Integer;

begin
  if { (Val.XXX < 0) or } (Val.YYY < 0) then Sign:= - 1 else Sign:= + 1;
  if Sys in [csMetric1km, csMetric1kmTurkish] then begin
    XXX:= UpgradeInt(Abs(Val.XXX), '0', 4);
    if Val.XXX < 0 then XXX:='-' + XXX;
  end
  else
    XXX:= IntToStr(Val.XXX);

//  if Round(1000 * Frac(ToLeftPostMM / 1000)) > 991 then
//  begin
//    Val.YYY:= Val.YYY + 1;
//  end;

  YYY:= UpgradeInt(Abs(Val.YYY), '0', 3);

  case Sys of
     csImperial: Result:= XXX + '.' + YYY;
  csMetric0_1km: Result:= XXX + '.' + YYY;
    csMetric1km,csMetric1kmTurkish:
                 begin
                   X:= Copy(XXX, 1, Length(XXX) - 1);
                   if X='' then X:='0';
                   if X='-' then X:='-0';

                   if {Config.DisplayConfig.}AddDatainCoord
                     then ZZZ:= InTToStr(Round(1000 * Frac(ToLeftPostMM / 1000)));
//                     then ZZZ:= UpgradeInt(Round(1000 * Frac(ToLeftPostMM / 1000)), '0', 3);


                   Y:= Copy(XXX, Length(XXX), 1) + Copy(YYY, 1, 2);
                   if {Config.DisplayConfig.}AddDatainCoord then
                   begin
                     if Sys = csMetric1km then Result:= X + '.' + Y { + '.' + ZZZ}
                                          else Result:= X + '+' + Y { + '.' + ZZZ};
                   end
                   else
                   begin
                     if Sys = csMetric1km then Result:= X + '.' + Y
                                          else Result:= X + '+' + Y;
                   end;
                 end;
  end;
  if (Sign < 0) and (Val.XXX >= 0) then Result:= '-' + Result;
end;

function CaCrdToStr0(Sys: TCoordSys; Val: TCaCrd): string;
var
  XXX: string;
  YYY: string;
  X: string;
  Y: string;
  Sign: Integer;

begin
  if { (Val.XXX < 0) or } (Val.YYY < 0) then Sign:= - 1 else Sign:= + 1;
  if Sys in [csMetric1km, csMetric1kmTurkish] then
    XXX:= UpgradeInt(Abs(Val.XXX), '0', 4)
  else
    XXX:= UpgradeInt(Abs(Val.XXX), '0', 5);
  YYY:= UpgradeInt(Abs(Val.YYY), '0', 3);

  case Sys of
     csImperial: Result:= XXX + '.' + YYY;
  csMetric0_1km: Result:= XXX + '.' + YYY;
    csMetric1km,csMetric1kmTurkish:
                 begin
                   X:= Copy(XXX, 1, Length(XXX) - 1);
//                   if X='' then X:='0';
//                   if X='-' then X:='-0';
                   Y:= Copy(XXX, Length(XXX), 1) + Copy(YYY, 1, 2);
                   if Sys = csMetric1km then
                     Result:= X + '.' + Y
                   else
                     Result:= X + '+' + Y;
                 end;
  end;
  if (Sign < 0) and (Val.XXX >= 0) then Result:= '-' + Result;
end;

function CaCrdToVal(Sys: TCoordSys; Val: TCaCrd): Single;
var
  XXX: string;
  YYY: string;
  X: string;
  Y: string;
  Result_: string;
  Sign: Integer;
  SaveDecimalSeparator: Char;

begin
  if { (Val.XXX < 0) or } (Val.YYY < 0) then Sign:= - 1 else Sign:= + 1;
  if Sys in [csMetric1km, csMetric1kmTurkish] then
    XXX:= UpgradeInt(Abs(Val.XXX), '0', 4)
  else
    XXX:= IntToStr(Val.XXX);
  YYY:= UpgradeInt(Abs(Val.YYY), '0', 3);

  case Sys of
     csImperial: Result_:= XXX + '.' + YYY;
  csMetric0_1km: Result_:= XXX + '.' + YYY;
    csMetric1km,csMetric1kmTurkish:
                 begin
                   X:= Copy(XXX, 1, Length(XXX) - 1);
                   Y:= Copy(XXX, Length(XXX), 1) + Copy(YYY, 1, 2);
                   if Sys = csMetric1km then
                     Result_:= X + '.' + Y
                   else
                     Result_:= X + '.' + Y;
                 end;
  end;
  if (Sign < 0) and (Val.XXX >= 0) then Result_:= '-' + Result_;

  {$IFDEF VER210}
  SaveDecimalSeparator:= DecimalSeparator;
  DecimalSeparator:= '.';
  Result:= StrToFloat(Result_);
  DecimalSeparator:= SaveDecimalSeparator;
  {$ENDIF}

  {$IFDEF VER260}
  SaveDecimalSeparator:= FormatSettings.DecimalSeparator;
  FormatSettings.DecimalSeparator:= '.';
  Result:= StrToFloat(Result_);
  FormatSettings.DecimalSeparator:= SaveDecimalSeparator;
  {$ENDIF}

  (*
{$IFDEF VER220} - Delphi XE
{$IFDEF VER230} - Delphi XE2
{$IFDEF VER240} - Delphi XE3
{$IFDEF VER250} - Delphi XE4
{$IFDEF VER260} - Delphi XE5
{$IFDEF VER265} - Appmethod 1.0
{$IFDEF VER270} - Delphi XE6
{$IFDEF VER280} - Delphi XE7
{$IFDEF VER290} - Delphi XE8
{$IFDEF VER300} - Delphi 10 Seattle
  *)
end;

function GetMMLenFromCaPost(Sys: TCoordSys; Post: TCaCrd): Extended;
begin
  case Sys of
     csImperial: Result:= 30480 * Post.YYY / 1000;
  csMetric0_1km, csMetric1km, csMetric1kmTurkish : Result:= 100000 * Post.YYY / 1000;
    else Result:= 0;
  end;
end;

function StrToCaCrd(CoordSys: TCoordSys; Text: string): TCaCrd;
var
  I: Integer;

begin
  Result.XXX:= 0;
  Result.YYY:= 0;

  if CoordSys = csMetric1kmTurkish then
    I:= Pos('+', Text)
  else
    I:= Pos('.', Text);
  if I = 0 then Exit;
  Delete(Text, I, 1);
 try
  case CoordSys of
     csImperial,
  csMetric0_1km:
                begin
                   Result.XXX:= StrToInt(Copy(Text, 1, I - 1));
                   Result.YYY:= StrToInt(Copy(Text, I, Length(Text)));
                end;

    csMetric1km,csMetric1kmTurkish :
                 begin
                   Result.XXX:= StrToInt(Copy(Text, 1, I - 1) + Copy(Text, I, 1));
                   Result.YYY:= StrToInt(Copy(Text, I + 1, Length(Text)) + '0');
                 end;
            else begin
                   Result.XXX:= 0;
                   Result.YYY:= 0;
                 end;

{    csMetric1km: begin
                   Result.XXX:= StrToInt(Copy(Text, 1, I));
                   Result.YYY:= StrToInt(Copy(Text, I + 1, Length(Text)));
                 end; }
  end;
 except
                   Result.XXX:= 0;
                   Result.YYY:= 0;
 end;
end;

// -------------------------------------------------------------------------------------------------

function CrdParamsToRealCrd(CrdParams: TCrdParams; MoveDir: Integer): TRealCoord;
begin

  Result.ToLeftPostMM:= CrdParams.CaCrdParams.ToLeftPostMM;
  Result.Sys:= CrdParams.Sys;
  if CrdParams.Sys = csMetricRF then Result.MRFCrd:= MRFCrdParamsToCrd(CrdParams.MRFCrdParams, MoveDir)
                                else Result.CaCrd:= CaCrdParamsToCrd(CrdParams.CaCrdParams, MoveDir);
end;

function RealCrdToStr(Crd: TRealCoord; Index: Integer = 2) : string;
begin
  if Crd.Sys = csMetricRF then Result:= MRFCrdToStr(Crd.MRFCrd, Index)
                          else Result:= CaCrdToStr(Crd.Sys, Crd.CaCrd, Crd.ToLeftPostMM);
end;

function RealCoordToReal_(Crd: TRealCoord): Integer;
begin
  case Crd.Sys of
     csMetricRF: Result:= Crd.MRFCrd.Km * 1000 + (Crd.MRFCrd.Pk - 1) * 100 + Crd.MRFCrd.mm div 1000;
     csImperial,
  csMetric0_1km,
    csMetric1km,
csMetric1kmTurkish: Result:= Round(CaCrdToMM(Crd.Sys, Crd.CaCrd) / 1000);
  end;
end;


// -----------< Работа с текстом >--------------------------------------------------------------------



function GetDeviceNameTextId(DeviceID: TAviconID): string;
begin
  if CompareMem(@DeviceID, @Avicon16Scheme1, 8) then Result:= 'Avicon16Scheme1';  // Авикон-16 схема 1
  if CompareMem(@DeviceID, @Avicon16Scheme2, 8) then Result:= 'Avicon16Scheme2';  // Авикон-16 схема 2
  if CompareMem(@DeviceID, @Avicon16Scheme3, 8) then Result:= 'Avicon16Scheme3';  // Авикон-16 схема 3
  if CompareMem(@DeviceID, @Avicon16Wheel, 8) then Result:= 'Avicon16Wheel';      // Авикон-16 колесная схема
  if CompareMem(@DeviceID, @Avicon16Wheel2, 8) then Result:= 'Avicon16Wheel2';      // Авикон-16 3x65 колесная схема
  if CompareMem(@DeviceID, @Avicon16Wheel3, 8) then Result:= 'Avicon16Wheel3';      // Авикон-16 3x65 колесная схема
  if CompareMem(@DeviceID, @Avicon11Scheme0, 8) then Result:= 'Avicon11Scheme0';  // Авикон-11
  if CompareMem(@DeviceID, @Avicon15Scheme1, 8) then Result:= 'Avicon15Scheme1';  // Авикон-15
  if CompareMem(@DeviceID, @Avicon14Scheme1, 8) then Result:= 'Avicon14Scheme1';  // Авикон-14 скольжение
  if CompareMem(@DeviceID, @Avicon14Wheel, 8) then Result:= 'Avicon14Wheel';      // Авикон-14 колесная схема
  if CompareMem(@DeviceID, @Avicon14Wheel2, 8) then Result:= 'Avicon14Wheel2';    // Авикон-14 колесная схема 2 (3x65- самоходная тележка GEISMAR Бангкок)
  if CompareMem(@DeviceID, @USK003R, 8) then Result:= 'USK004R';                  // USK004R (Венгрия) - старая версия (макет)
  if CompareMem(@DeviceID, @USK004R, 8) then Result:= 'USK004R';                  // USK004R (Венгрия)
  if CompareMem(@DeviceID, @EGO_USWScheme1, 8) then Result:= 'EGO_USWScheme1';  // EGO_USW схема 1
  if CompareMem(@DeviceID, @EGO_USWScheme2, 8) then Result:= 'EGO_USWScheme2';  // EGO_USW схема 2 (3x65- самоходная тележка GEISMAR Бангкок)
  if CompareMem(@DeviceID, @EGO_USWScheme3, 8) then Result:= 'EGO_USWScheme3';  // EGO_USW схема 3
  if CompareMem(@DeviceID, @Avicon31Scheme1, 8) then Result:= 'Avicon31Scheme1'; //
  if CompareMem(@DeviceID, @Avicon31Scheme2, 8) then Result:= 'Avicon31Scheme2'; //
  if CompareMem(@DeviceID, @Avicon31Estonia, 8) then Result:= 'Avicon31Estonia'; //
  if CompareMem(@DeviceID, @Avicon31Estonia2, 8) then Result:= 'Avicon31Estonia2'; //
  if CompareMem(@DeviceID, @Avicon15N8Scheme1, 8) then Result:= 'Avicon15Scheme1'; // Авикон-15
  if CompareMem(@DeviceID, @FilusX17DW, 8) then Result:= 'FilusX111W'; // FilusX111W
  if CompareMem(@DeviceID, @VMT_US_BigWP, 8) then Result:= 'VMT_US_BigWP'; // VMT US Одно КИС (Каир) - по одному большому колесу на рельс, дрезина VMT GEISMAR Каир
  if CompareMem(@DeviceID, @VMT_US_BigWP_N2, 8) then Result:= 'VMT_US_BigWP_N2'; // VMT US Одно КИС (Каир) - по одному большому колесу на рельс, дрезина VMT GEISMAR Каир
  if CompareMem(@DeviceID, @VMT_USW, 8) then Result:= 'VMT_USW'; // VMT US Одно КИС (Каир) - по одному большому колесу на рельс, дрезина VMT GEISMAR Каир
  if CompareMem(@DeviceID, @FilusX27MWScheme1, 8) then Result:= 'FilusX27MWScheme1'; // VMT US Одно КИС (Каир) - по одному большому колесу на рельс, дрезина VMT GEISMAR Каир
  if CompareMem(@DeviceID, @FilusX27MW, 8) then Result:= 'FilusX27MW'; // VMT US Одно КИС (Каир) - по одному большому колесу на рельс, дрезина VMT GEISMAR Каир
  if CompareMem(@DeviceID, @MDLWheel, 8) then Result:= 'MDLWheel'; //  МДЛ
  if CompareMem(@DeviceID, @MDLWheel2, 8) then Result:= 'MDLWheel2'; //  МДЛ

//  if CompareMem(@DeviceID, @FilusX27Wheel, 8) then Result:= 'FilusX27Wheel';      // FilusX27 колесная схема
end;

function GetInspMethodTextId(Metod: TInspectionMethod; FullName: Boolean): string;
begin
  if FullName then
    case Metod of
            imEcho: Result:= 'InspMethod_FULL:Echo';
    imMirrorShadow: Result:= 'InspMethod_FULL:MirrorShadow';
   imMirrorShadow2: Result:= 'InspMethod_FULL:MirrorShadow';
          imMirror: Result:= 'InspMethod_FULL:Mirror';
          imShadow: Result:= 'InspMethod_FULL:Shadow';
    end
  else
    case Metod of
            imEcho: Result:= 'InspMethod_SHORT:Echo';
    imMirrorShadow: Result:= 'InspMethod_SHORT:MirrorShadow';
   imMirrorShadow2: Result:= 'InspMethod_SHORT:MirrorShadow';
          imMirror: Result:= 'InspMethod_SHORT:Mirror';
          imShadow: Result:= 'InspMethod_SHORT:Shadow';
    end;
end;

function GetInspZoneTextId(Zone: TInspectionZone; FullName: Boolean): string;
begin
  if FullName then
    case Zone of
       izAll: Result:= 'InspZone_SHORT:All';
      izHead: Result:= 'InspZone_SHORT:Head';
       izWeb: Result:= 'InspZone_SHORT:Web';
      izBase: Result:= 'InspZone_SHORT:Base';
    end
  else
    case Zone of
       izAll: Result:= 'InspZone_SHORT:All';
      izHead: Result:= 'InspZone_SHORT:Head';
       izWeb: Result:= 'InspZone_SHORT:Web';
      izBase: Result:= 'InspZone_SHORT:Base';
    end
end;

function HeaderStrToString(Text: THeaderStr): string;
var
  I: Integer;

begin
  Result:= '';
  for I:= 1 to Min(64, Word(Text[0])) do
    if (Text[I] <> '_') and (Text[I] <> Chr($A)) then Result:= Result + Text[I];
end;

function StringToHeaderStr(Text: string): THeaderStr;
var
  I: Integer;

begin
  Result[0]:= Char(Length(Text));
  for I:= 1 to Length(Text) do Result[I]:= Text[I];
end;

function HeaderBigStrToString(Text: THeaderBigStr): string;
var
  I: Integer;

begin
  Result:= '';
  for I:= 1 to Min(255, Word(Text[0])) do Result:= Result + Text[I];
end;

function HeaderBigStrToStringForReport(Text: THeaderBigStr): string;
var
  I: Integer;

begin
  Result:= '';
  for I:= 1 to Min(255, Word(Text[0])) do
    if (Text[I] <> #$A) and (Text[I] <> #$D) then Result:= Result + Text[I]
    else if (Text[I] = #$A) then Result:= Result + ' ';
end;

function LenHeaderBigStr(Text: THeaderBigStr):Integer;
begin
  Result:= Min(255, Word(Text[0]));
end;

function StringToHeaderBigStr(Text: string): THeaderBigStr;
var
  I: Integer;

begin
  Result[0]:= Char(Length(Text));
  for I:= 1 to Length(Text) do Result[I]:= Text[I];
end;

function HeaderBigStrToString_(Text: THeaderBigStr): string;
var
  I: Integer;

begin
  Result:= '';
  for I:= 0 to 255 do if Text[I] <> Chr(0) then Result:= Result + Text[I] else Break;
end;

// ---------- Tools ------------------------------------------------------------

function GetRealCrd_inMM(ScanCh: TScanChannelItem; Delay: Byte; isEGOUSW_BHead: Boolean): Single;

var
  Test1: single;
  Test2: single;
  Test3: single;

begin
  with ScanCh do
  begin
    Test1:= sin(Abs(EnterAngle) * pi / 180);
    Test2:= cos(TurnAngle * pi / 180);
    Test3:= DelayToMMLen(ScanCh, Delay);
    Result:= Test3 * Test1 * Test2;

    if not isEGOUSW_BHead then
    begin
      if EnterAngle > 0 then Result:= Position + Result
                        else Result:= Position - Result;
    end
    else
    begin
      if EnterAngle > 0 then Result:= Position - Result
                        else Result:= Position + Result;
    end;
  end;
end;

function GetRealCrd_inSysCrd(ScanCh: TScanChannelItem; ScanStep: Integer; Delay: Integer; isEGOUSW_BHead: Boolean): Single;
begin
  if Delay < 1000 then Result:= GetRealCrd_inMM(ScanCh, Delay, isEGOUSW_BHead) / (ScanStep / 100)
                  else Result:= 0;
end;

function GetMoveCrd_inSysCrd(ScanCh: TScanChannelItem; ScanStep: Integer; Delay: Integer): Single;
begin
  with ScanCh do
  begin
//    Result:= DelayToMMLen(ScanCh, Delay) * sin(Abs(EnterAngle) * pi / 180) * cos(TurnAngle * pi / 180);
    if EnterAngle > 0 then Result:= - Position //+ Result
                      else Result:= - Position; //- Result;
  end;
  Result:= Result / (ScanStep / 100);
end;

function DelayToMMLen(ScanCh: TScanChannelItem; Delay: Byte): Single; // По лучу
begin
  if Abs(ScanCh.EnterAngle) < 20 then Result:= 5.9 * Delay / 2
                                 else Result:= 3.26 * Delay / 2;
end;

function UpgradeStr(Src: string; Ch: Char; Len: Integer): string;
begin
  Result:= Src;
  while Length(Result) < Len do Result:= Ch + Result;
end;

function UpgradeInt(Int: Integer; Ch: Char; Len: Integer): string;
begin
  Result:= IntToStr(Int);
  while Length(Result) < Len do Result:= Ch + Result;
end;

// -------------------------------------------------------------------------------------------

              {
function GetDir(ChID: Integer): Integer;
begin
  case ChID of
    MIRRORSHADOW_0           : Result:= ;
    ECHO_0                   : Result:= ;
    FORWARD_BOTH_ECHO_58     : Result:= ;
    FORWARD_WORK_ECHO_58     : Result:= ;
    FORWARD_UWORK_ECHO_58    : Result:= ;
    BACKWARD_BOTH_ECHO_58    : Result:= ;
    BACKWARD_WORK_ECHO_58    : Result:= ;
    BACKWARD_UWORK_ECHO_58   : Result:= ;
    FORWARD_BOTH_MIRROR_58   : Result:= ;
    FORWARD_WORK_MIRROR_58   : Result:= ;
    FORWARD_UWORK_MIRROR_58  : Result:= ;
    BACKWARD_BOTH_MIRROR_58  : Result:= ;
    BACKWARD_WORK_MIRROR_58  : Result:= ;
    BACKWARD_UWORK_MIRROR_58 : Result:= ;
    FORWARD_ECHO_70          : Result:= ;
    BACKWARD_ECHO_70         : Result:= ;
    FORWARD_ECHO_42_WEB      : Result:= ;
    BACKWARD_ECHO_42_WEB     : Result:= ;
    FORWARD_ECHO_42_BASE     : Result:= ;
    BACKWARD_ECHO_42_BASE    : Result:= ;
  end;
end;   }

function GetSurf(ChID: Integer): Integer;
begin
  case ChID of
    MIRRORSHADOW_0           : Result:= 1;
    ECHO_0                   : Result:= 1;
    FORWARD_BOTH_ECHO_58     : Result:= 1;
    FORWARD_WORK_ECHO_58     : Result:= 1;
    FORWARD_UWORK_ECHO_58    : Result:= 2;
    BACKWARD_BOTH_ECHO_58    : Result:= 1;
    BACKWARD_WORK_ECHO_58    : Result:= 1;
    BACKWARD_UWORK_ECHO_58   : Result:= 2;
    FORWARD_BOTH_MIRROR_58   : Result:= 1;
    FORWARD_WORK_MIRROR_58   : Result:= 1;
    FORWARD_UWORK_MIRROR_58  : Result:= 2;
    BACKWARD_BOTH_MIRROR_58  : Result:= 1;
    BACKWARD_WORK_MIRROR_58  : Result:= 1;
    BACKWARD_UWORK_MIRROR_58 : Result:= 2;
    FORWARD_ECHO_70          : Result:= 1;
    BACKWARD_ECHO_70         : Result:= 1;
    FORWARD_ECHO_42_WEB      : Result:= 1;
    BACKWARD_ECHO_42_WEB     : Result:= 1;
    FORWARD_ECHO_42_BASE     : Result:= 1;
    BACKWARD_ECHO_42_BASE    : Result:= 1;
    FORWARD_ECHO_65          : Result:= 1;
    BACKWARD_ECHO_65         : Result:= 1;
    SIDE_ECHO_65             : Result:= 1;
    WORK_MSM_55              : Result:= 1;
    UWORK_MSM_55             : Result:= 2;
    FORWARD_WORK_ECHO_65     : Result:= 1;
    FORWARD_UWORK_ECHO_65    : Result:= 2;
    BACKWARD_WORK_ECHO_65    : Result:= 1;
    BACKWARD_UWORK_ECHO_65   : Result:= 2;
    FORWARD_WORK_ECHO_50     : Result:= 1;
    FORWARD_UWORK_ECHO_50    : Result:= 2;
    BACKWARD_WORK_ECHO_50    : Result:= 1;
    BACKWARD_UWORK_ECHO_50   : Result:= 2;
    FORWARD_ECHO_45_WEB      : Result:= 1;
    BACKWARD_ECHO_45_WEB     : Result:= 1;
    FORWARD_ECHO_45_BASE     : Result:= 1;
    BACKWARD_ECHO_45_BASE    : Result:= 1;
    WORK_MSM_45              : Result:= 1;
    UWORK_MSM_45             : Result:= 2;
    FORWARD_ECHO_22          : Result:= 1;
    BACKWARD_ECHO_22         : Result:= 1;
    FORWARD_WORK_ECHO_70     : Result:= 1;
    FORWARD_UWORK_ECHO_70    : Result:= 2;
    BACKWARD_WORK_ECHO_70    : Result:= 1;
    BACKWARD_UWORK_ECHO_70   : Result:= 2;
  end;


end;


function InvertGPSSingle(Src: Single): Single;
var
  Tmp1: array [1..4] of Byte;
  Tmp2: array [1..4] of Byte;

begin
  Move(Src, Tmp1, 4);
  Tmp2[1]:= Tmp1[4];
  Tmp2[2]:= Tmp1[3];
  Tmp2[3]:= Tmp1[2];
  Tmp2[4]:= Tmp1[1];
  Move(Tmp2, Result, 4);

//  Result:= Src;
end;

function GPSSingleToText( A: Double; Lat: Boolean ): string; //  Широта = Latitude
var
  D, M, S: LongInt;
  SW: string;

begin
  try

    if Lat then //  Широта = Latitude
    begin

      if (A >= -90) and (A <= 90) then
      begin
        D:= Trunc( A );
        M:= Trunc( ( A - D ) * 60 );
        S:= Trunc( ((( A - D ) * 3600 ) - M * 60) * 100 );
        if A >= 0 then SW:= 'N' else SW:= 'S';
        Result:= Format( '%s:%d°%d.%d', [SW, D, M, S ] );
      end else Result:= '';
    end
    else
    begin
      if (A >= -180) and (A <= 180) then
      begin
        D:= Trunc( A );
        M:= Trunc( ( A - D ) * 60 );
        S:= Trunc( ((( A - D ) * 3600 ) - M * 60) * 100 );
        if A >= 0 then SW:= 'E' else SW:= 'W';
        Result:= Format( '%s:%d°%d.%d', [SW, D, M, S ] );
      end else Result:= '';
    end;

  except

    Result:= '';

  end;

end;

procedure GetSystemPos(DeviceID: TAviconID; PaintSystemParams: TPaintSystemParams; ControlDirection: Byte; SensorPos: PInteger; AirBrushPos: PInteger);
begin

//  Temp:= DataCont.Config.DeviceID;
  if (PaintSystemParams.Version >= 4) and
      (CompareMem(@DeviceID, @Avicon16Wheel, 8) or
       CompareMem(@DeviceID, @Avicon16Wheel2, 8) { or
       CompareMem(@DeviceID, @Avicon14Wheel, 8)} ) then
  begin                                   // Колеса
    SensorPos^:= PaintSystemParams.Wheel_SensorPos_;
    AirBrushPos^:= PaintSystemParams.Wheel_AirBrushPos_;
  end
  else
  if (PaintSystemParams.Version >= 4) and
      (CompareMem(@DeviceID, @Avicon16Scheme1, 8) or
       CompareMem(@DeviceID, @Avicon16Scheme2, 8) or
       CompareMem(@DeviceID, @Avicon16Scheme3, 8)) then
  begin                                   // Скольжение
    SensorPos^:= PaintSystemParams.Slide_SensorPos_;
    AirBrushPos^:= PaintSystemParams.Slide_AirBrushPos_;
  end
  else
  if (PaintSystemParams.Version >= 5) and
      (CompareMem(@DeviceID, @EGO_USWScheme1, 8) or
       CompareMem(@DeviceID, @EGO_USWScheme2, 8) or
       CompareMem(@DeviceID, @EGO_USWScheme3, 8)) then
  begin                                   // EGO USW
    if ControlDirection = cd_Tail_B_Head_A then
    begin
      SensorPos^:= PaintSystemParams.EGOUSW_SensorPos_HeadA;
      AirBrushPos^:= PaintSystemParams.EGOUSW_AirBrushPos_HeadA;
    end
    else
    begin
      SensorPos^:= PaintSystemParams.EGOUSW_SensorPos_HeadB;
      AirBrushPos^:= PaintSystemParams.EGOUSW_AirBrushPos_HeadB;
    end;
  end;
end;

{
function GPSSingleToText( A: Single; Lat: Boolean ): string; //  Широта = Latitude
var
  D, M, S: LongInt;
  SW: string;

begin
  try

    if Lat then //  ?????? = Latitude
    begin

      if (A >= -90) and (A <= 90) then
      begin
        D:= Trunc( A );
        M:= Trunc( ( A - D ) * 60 );
        S:= Trunc( ((( A - D ) * 3600 ) - M * 60) * 100 );
        if A >= 0 then SW:= 'N' else SW:= 'S';
        Result:= Format( '%s:%d°%d.%d', [SW, D, M, S ] );
      end else Result:= '';
    end
    else
    begin
      if (A >= -180) and (A <= 180) then
      begin
        D:= Trunc( A );
        M:= Trunc( ( A - D ) * 60 );
        S:= Trunc( ((( A - D ) * 3600 ) - M * 60) * 100 );
        if A >= 0 then SW:= 'E' else SW:= 'W';
        Result:= Format( '%s:%d°%d.%d', [SW, D, M, S ] );
      end else Result:= '';
    end;

  except

    Result:= '';

  end;


end;
}

// -------------------------------------------------------------------------------------------

function GetString_from1(Len: Byte; Text: array of Char): string;
var
  I: Integer;

begin
  Result:= '';
  for I := 1 to Len do
    if (Text[I] <> Chr($A)) and (Text[I] <> '_') then Result:= Result + Text[I];
end;

// -------------------------------------------------------------------------------------------

function SwitchIDsToText(A31: Boolean; IDs: edStartSwitchEx): string;
begin
  if A31 then
  begin
                        // Одиночный + Одиночный с непрерывной поверхностью катания - ОК
    if IDs.SwitchType in [st_SingleSwitch, st_SingleSwitchContiniousRollingSurface] then
    begin
      Result:= 'Нач.';
      case IDs.SwitchType of
                                st_SingleSwitch: Result:= Result + 'СП '; // одиночный
        st_SingleSwitchContiniousRollingSurface: Result:= Result + 'СПНПК '; // одиночный с непрерывной поверхностью катания
      end;

      Result:= Result + '№' + GetString_from1(IDs.Len, IDs.Text);

      case IDs.SwitchDirectionType of
        sdt_Left: Result:= Result + '_Л';  // левый
        sdt_Right: Result:= Result + '_П'; // правый
      end;

      case IDs.SwitchFixedDirection of
         sfd_Plus: Result:= Result + '+';  // +
        sfd_Minus: Result:= Result + '-';  // -
      end;

      Result:= Result + '/УЗК:';

      case IDs.DeviceDirection of
  //        sdd_AgainstWool: Result:= Result + 'Пр-ш';  // противошёрстное
  //             sdd_ByWool: Result:= Result + 'Пошёр';  // пошёрстное
      sdd_AgainstWoolPlus: Result:= Result + 'Пр-ш+';  // противошёрстное + (прямое)
     sdd_AgainstWoolMinus: Result:= Result + 'Пр-ш-';  // противошёрстное - (боковое)
           sdd_ByWoolPlus: Result:= Result + 'Пошёр+'; // пошёрстное + (прямое)
          sdd_ByWoolMinus: Result:= Result + 'Пошёр-'; // пошёрстное - (боковое)
      end;
    end

    else

                     // Перекрёстный + Перекрестный с непрерывной поверхностью ката-ния -
    if IDs.SwitchType in [st_CrossSwitch, st_CrossSwitchContiniousRollingSurface] then
    begin
      Result:= 'Нач.';
      case IDs.SwitchType of
                                 st_CrossSwitch: Result:= Result + 'СП '; // перекрёстный
         st_CrossSwitchContiniousRollingSurface: Result:= Result + 'СПНПК '; // перекрестный с непрерывной поверхностью ката-ния
      end;

      Result:= Result + '№' + GetString_from1(IDs.Len, IDs.Text);

      case IDs.SwitchFixedDirection of
      sfd_LeftPlus: Result:= Result + '_Л+'; // левое + (прямое)
     sfd_LeftMinus: Result:= Result + '_Л-';  // левое - (боковое)
     sfd_RightPlus: Result:= Result + '_П+';  // правое + (прямое)
    sfd_RightMinus: Result:= Result + '_П-'; // правое - (боковое)
      end;

      Result:= Result + '/УЗК:';

      case IDs.DeviceDirection of
      sdd_LeftPlus: Result:= Result + 'Л+'; // левое + (прямое)
     sdd_LeftMinus: Result:= Result + 'Л-';  // левое - (боковое)
     sdd_RightPlus: Result:= Result + 'П+';  // правое + (прямое)
    sdd_RightMinus: Result:= Result + 'П-'; // правое - (боковое)
      end;
    end

    else

    if IDs.SwitchType = st_DropSwitch then  // Cбрасывающий - ОК
    begin
      Result:= 'Нач.СП №' + GetString_from1(IDs.Len, IDs.Text) + '_';

      case IDs.SwitchDirectionType of
         sdt_SingleThreadLeft: Result:= Result + '1н-Л';  // 1-ниточный левый
        sdt_SingleThreadRight: Result:= Result + '1н-П';  // 1-ниточный правый
           sdt_DualThreadLeft: Result:= Result + '2н-Л';  // 2-ниточный левый
          sdt_DualThreadRight: Result:= Result + '2н-П';  // 2-ниточный правый
      end;

      case IDs.SwitchFixedDirection of
         sfd_Plus: Result:= Result + '+';  // +
        sfd_Minus: Result:= Result + '-';  // -
      end;

      Result:= Result + '/УЗК:';

      case IDs.DeviceDirection of
          sdd_AgainstWool: Result:= Result + 'Пр-ш';  // противошёрстное
               sdd_ByWool: Result:= Result + 'Пошёр';  // пошёрстное
      end;
    end

    else

    if IDs.SwitchType = st_OtherSwitch then  // Другой - ОК
    begin
      Result:= 'СП №' + GetString_from1(IDs.Len, IDs.Text);
    end;

  end
  else
  begin

    if IDs.SwitchType in [st_SingleSwitch, st_SingleSwitchContiniousRollingSurface] then  // одиночный + одиночный с непрерывной поверхностью катания
    begin
      Result:= 'Нач.';
      case IDs.SwitchType of
                                st_SingleSwitch: Result:= Result + 'СП '; // одиночный
        st_SingleSwitchContiniousRollingSurface: Result:= Result + 'СПНПК '; // одиночный с непрерывной поверхностью катания
      end;

      Result:= Result + '№' + GetString_from1(IDs.Len, IDs.Text);

      case IDs.SwitchDirectionType of
        sdt_Left: Result:= Result + '_Л';  // левый
        sdt_Right: Result:= Result + '_П'; // правый
      end;

      case IDs.SwitchFixedDirection of
         sfd_Plus: Result:= Result + '+';  // +
        sfd_Minus: Result:= Result + '-';  // -
      end;

      Result:= Result + '/УЗК:';

      case IDs.DeviceDirection of
  //        sdd_AgainstWool: Result:= Result + 'Пр-ш';  // противошёрстное
  //             sdd_ByWool: Result:= Result + 'Пошёр'; // пошёрстное
      sdd_AgainstWoolPlus: Result:= Result + 'Пр-ш+';  // противошёрстное + (прямое)
     sdd_AgainstWoolMinus: Result:= Result + 'Пр-ш-';  // противошёрстное - (боковое)
           sdd_ByWoolPlus: Result:= Result + 'Пошёр+'; // пошёрстное + (прямое)
          sdd_ByWoolMinus: Result:= Result + 'Пошёр-'; // пошёрстное - (боковое)
      end;
    end

    else

    if IDs.SwitchType in [st_CrossSwitch, st_CrossSwitchContiniousRollingSurface] then  // перекрёстный + перекрестный с непрерывной поверхностью ката-ния
    begin


      Result:= 'Нач.';
      case IDs.SwitchType of
                                 st_CrossSwitch: Result:= Result + 'СП '; // перекрёстный
         st_CrossSwitchContiniousRollingSurface: Result:= Result + 'СПНПК '; // перекрестный с непрерывной поверхностью ката-ния
      end;

      Result:= Result + '№' + GetString_from1(IDs.Len, IDs.Text);

      case IDs.SwitchFixedDirection of
      sfd_LeftPlus: Result:= Result + '_Л+'; // левое + (прямое)
     sfd_LeftMinus: Result:= Result + '_Л-';  // левое - (боковое)
     sfd_RightPlus: Result:= Result + '_П+';  // правое + (прямое)
    sfd_RightMinus: Result:= Result + '_П-'; // правое - (боковое)
      end;

      Result:= Result + '/УЗК:';

      case IDs.DeviceDirection of
      sdd_LeftPlus: Result:= Result + 'Л+'; // левое + (прямое)
     sdd_LeftMinus: Result:= Result + 'Л-';  // левое - (боковое)
     sdd_RightPlus: Result:= Result + 'П+';  // правое + (прямое)
    sdd_RightMinus: Result:= Result + 'П-'; // правое - (боковое)
      end;

(*
      case IDs.SwitchDirectionType of
         sdt_Left: Result:= Result + 'Л';  // левый
        sdt_Right: Result:= Result + 'П'; // правый
      end;

      Result:= Result;

      case IDs.SwitchFixedDirection of
         sfd_Plus: Result:= Result + '+';  // +
        sfd_Minus: Result:= Result + '-';  // -
      end; *)
    end

    else

    if IDs.SwitchType = st_DropSwitch then  // Cбрасывающий - ОК
    begin
      Result:= 'Нач.СП №' + GetString_from1(IDs.Len, IDs.Text) + '_';

      case IDs.SwitchDirectionType of
         sdt_SingleThreadLeft: Result:= Result + '1н-Л';  // 1-ниточный левый
        sdt_SingleThreadRight: Result:= Result + '1н-П';  // 1-ниточный правый
           sdt_DualThreadLeft: Result:= Result + '2н-Л';  // 2-ниточный левый
          sdt_DualThreadRight: Result:= Result + '2н-П';  // 2-ниточный правый
      end;

      case IDs.SwitchFixedDirection of
         sfd_Plus: Result:= Result + '+';  // +
        sfd_Minus: Result:= Result + '-';  // -
      end;

      Result:= Result + '/УЗК:';

      case IDs.DeviceDirection of
          sdd_AgainstWool: Result:= Result + 'Пр-ш';  // противошёрстное
               sdd_ByWool: Result:= Result + 'Пошёр';  // пошёрстное
      end;
    end
(*

    if IDs.SwitchType = st_DropSwitch then  // сбрасывающий
    begin
      Result:= 'Нач.СП №' + GetString_from1(IDs.Len, IDs.Text) + '_';

      case IDs.SwitchDirectionType of
         sdt_SingleThreadLeft: Result:= Result + '1н';  // 1-ниточный левый
        sdt_SingleThreadRight: Result:= Result + '1н';  // 1-ниточный правый
           sdt_DualThreadLeft: Result:= Result + '2н';  // 2-ниточный левый
          sdt_DualThreadRight: Result:= Result + '2н';  // 2-ниточный правый
      end;

      case IDs.SwitchDirectionType of
         sdt_Left: Result:= Result + '-Л';  // левый
        sdt_Right: Result:= Result + '-П'; // правый
      end;

      Result:= Result + '/УЗК:';

      case IDs.DeviceDirection of
  //        sdd_AgainstWool: Result:= Result + 'Пр-ш';  // противошёрстное
  //             sdd_ByWool: Result:= Result + 'Пошёр';  // пошёрстное
      sdd_AgainstWoolPlus: Result:= Result + 'Пр-ш+';  // противошёрстное + (прямое)
     sdd_AgainstWoolMinus: Result:= Result + 'Пр-ш-';  // противошёрстное - (боковое)
           sdd_ByWoolPlus: Result:= Result + 'Пошёр+'; // пошёрстное + (прямое)
          sdd_ByWoolMinus: Result:= Result + 'Пошёр-'; // пошёрстное - (боковое)
      end;
    end  *)

    else

    if IDs.SwitchType = st_OtherSwitch then  // другой
    begin
      Result:= 'СП №' + GetString_from1(IDs.Len, IDs.Text);
    end;
  end;
end;
(*
function GetSurfaceText(Rail: TRail; Gran: TGran; LangTable: TLanguageTable): string;
begin
  case Gran of
    _None: Result := '';
    _Work: Result := '' + LangTable.Caption['Common:WorkSurf_SHORT'];
    _UnWork: Result := '' + LangTable.Caption['Common:UnWorkSurf_SHORT'];
    _Work_Left__UnWork_Right: if Rail = rLeft then Result := '' + LangTable.Caption['Common:WorkSurf_SHORT']
                                              else Result := '' + LangTable.Caption['Common:UnWorkSurf_SHORT'];
    _UnWork_Left__Work_Right: if Rail = rLeft then Result := '' + LangTable.Caption['Common:UnWorkSurf_SHORT']
                                              else Result := '' + LangTable.Caption['Common:WorkSurf_SHORT'];
               _Gran_by_СOND:
  end;
  // Result:= Result + IntToStr(Ord(Rail)) + IntToStr(Ord(Gran));
end;
*)

{$IFNDEF VMT_EGO}

function GetSurfaceText(Rail: TRail; Gran: TGran; FileHeader: TFileHeader): string;
begin
  case Gran of
                      _None: Result:= '';
                      _Work: Result:= '' + LangTable.Caption['Common:WorkSurf_SHORT'];
                    _UnWork: Result:= '' + LangTable.Caption['Common:UnWorkSurf_SHORT'];
   _Work_Left__UnWork_Right: if Rail = rLeft then Result:= '' + LangTable.Caption['Common:WorkSurf_SHORT']
                                             else Result:= '' + LangTable.Caption['Common:UnWorkSurf_SHORT'];
   _UnWork_Left__Work_Right: if Rail = rLeft then Result:= '' + LangTable.Caption['Common:UnWorkSurf_SHORT']
                                             else Result:= '' + LangTable.Caption['Common:WorkSurf_SHORT'];
          Left_Gran_by_СOND: if ConditionIsWork(FileHeader.MoveDir, FileHeader.WorkRailTypeA) then Result:= '' + LangTable.Caption['Common:WorkSurf_SHORT']
                                                                                              else Result:= '' + LangTable.Caption['Common:UnWorkSurf_SHORT'];
          Right_Gran_by_СOND: if ConditionIsWork(FileHeader.MoveDir, FileHeader.WorkRailTypeA) then Result:= '' + LangTable.Caption['Common:UnWorkSurf_SHORT']
                                                                                              else Result:= '' + LangTable.Caption['Common:WorkSurf_SHORT'];

  end;
//  Result:= Result + IntToStr(Ord(Rail)) + IntToStr(Ord(Gran));
end;

{$ENDIF}

function ConditionIsWork(MoveDir: Integer; WorkRailTypeA: Byte { 0 – левая }): Boolean;
begin
  Result:= ((MoveDir > 0) and (WorkRailTypeA <> 0)) or ((MoveDir < 0) and (WorkRailTypeA = 0));
            // (В стор. увел. && Правая) || (В стор. уменьш. && Левая)


end;

initialization

  FKM_Name:= 'км';
  FPK_Name:= 'пк';
  FMetr_Name:= 'м';
  FSantiMetr_Name:= 'см';
  FMilimetr_Name:= 'мм';
  FImperial_Name:= 'Imperial';
  FMetric0_1km_Name:= 'Metric 100m';
  FMetric1km_Name:= 'Metric 1 km';

end.
