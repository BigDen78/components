{*******************************************************}
{                                                       }
{       This unit is part of the VISPDF VCL library.    }
{       Written by R.Husske - ALL RIGHTS RESERVED.      }
{                                                       }
{       Copyright (C) 2000-2009, www.vispdf.com         }
{                                                       }
{       e-mail: support@vispdf.com                      }
{       http://www.vispdf.com                           }
{                                                       }
{*******************************************************}

unit VPDFFontRecords;

interface

uses
  Windows;

{$I VisPDFLib.inc }

type
  VISPDFUSHORT = Word;
  VISPDFULONG = Longint;
  VISPDFFWORD = WORD;
  VISPDFUFWORD = WORD;
  VISPDFLFIXED = Longint;
  VISPDFFIXED = packed record
    Hi, Lo: VISPDFUSHORT;
  end;

  TGlyfCharsKit = record
    Code: word;
    NCount: word;
  end;

  PVISPDFULONG = ^VISPDFULONG;

  TLinkArray = array of Integer;

  TFontHeader = packed record
    SFNTVersion: VISPDFLFIXED;
    NumTables: VISPDFUSHORT;
    SearchRange: VISPDFUSHORT;
    EntrySelector: VISPDFUSHORT;
    RangeShift: VISPDFUSHORT;
  end;

  CmapTableHeader = packed record
    Version: VISPDFUSHORT;
    NumTables: VISPDFUSHORT;
  end;

  CmapTableType = packed record
    PlatformID: VISPDFUSHORT;
    EncodingID: VISPDFUSHORT;
    TableOffset: VISPDFULONG;
  end;

  DeltaValues = record
    StartCode: VISPDFFWORD;
    EndCode: VISPDFFWORD;
    IDRange: VISPDFFWORD;
  end;

  CmapFormat4 = packed record
    Format: VISPDFUSHORT;
    Length: VISPDFUSHORT;
    Version: VISPDFUSHORT;
    SegCountX2: VISPDFUSHORT;
    SearchRange: VISPDFUSHORT;
    EntrySelector: VISPDFUSHORT;
    RangeShift: VISPDFUSHORT;
    EndCount: array of VISPDFUSHORT;
    ReservedPad: VISPDFUSHORT;
    StartCount: array of VISPDFUSHORT;
    IDDelta: array of SHORT;
    IDRangeOffset: array of VISPDFUSHORT;
    GlyphIdArray: array of VISPDFUSHORT;
  end;

  OutCmapHead = packed record
    Language: VISPDFUSHORT;
    SegCountX2: VISPDFUSHORT;
    SearchRange: VISPDFUSHORT;
    EntrySelector: VISPDFUSHORT;
    RangeShift: VISPDFUSHORT;
  end;

  LinearTable = record
    IsPresent: boolean;
    Item: array of byte;
    TableLength: Integer;
  end;

  WordLinearTable = record
    IsPresent: boolean;
    Item: array of word;
    TableLength: Integer;
  end;

  HeadTableType = packed record
    TableVersionNumber: VISPDFFIXED;
    FontRevision: VISPDFFIXED;
    CheckSum: VISPDFULONG;
    MagicNumber: VISPDFULONG;
    Flags: VISPDFUSHORT;
    UnitsPerEm: VISPDFUSHORT;
    Date: array[1..16] of byte;
    XMin: SHORT;
    YMin: SHORT;
    XMax: SHORT;
    YMax: SHORT;
    MacStyle: VISPDFUSHORT;
    LowestRec: VISPDFUSHORT;
    FontDirection: SHORT;
    IndexToLocFormat: SHORT;
    GlyphDataFormat: SHORT
  end;

  HheaTableType = packed record
    TablVer: LongInt;
    Ascender: VISPDFFWORD;
    Descender: VISPDFFWORD;
    LineGap: VISPDFFWORD;
    AdvanceWidthMax: VISPDFUFWORD;
    MinLeftSideBearing: VISPDFFWORD;
    MinRightSideBearing: VISPDFFWORD;
    XMaxExtent: VISPDFFWORD;
    CaretSlopeRise: SHORT;
    CaretSlopeRun: SHORT;
    CaretOffset: SHORT;
    Reserved1: SHORT;
    Reserved2: SHORT;
    Reserved3: SHORT;
    Reserved4: SHORT;
    MetricDataFormat: SHORT;
    NumberOfHMetrics: VISPDFUSHORT;
  end;

  MaxpTableType = packed record
    Version: VISPDFFIXED;
    NumGlyphs: VISPDFUSHORT;
    MaxPoints: VISPDFUSHORT;
    MaxContours: VISPDFUSHORT;
    MaxCompositePoints: VISPDFUSHORT;
    MaxCompositeContours: VISPDFUSHORT;
    MaxZones: VISPDFUSHORT;
    MaxTwilightPoints: VISPDFUSHORT;
    MaxStorage: VISPDFUSHORT;
    MaxFunctionDefs: VISPDFUSHORT;
    MaxInstructionDefs: VISPDFUSHORT;
    MaxStackElements: VISPDFUSHORT;
    MaxSizeOfInstructions: VISPDFUSHORT;
    MaxComponentElements: VISPDFUSHORT;
    MaxComponentDepth: VISPDFUSHORT;
  end;

  PGlyfRecord = ^GlyfRecord;

  GlyfRecord = packed record
    NumberOfContours: VISPDFULONG;
    RecordSize: VISPDFULONG;
    Curves: PAnsiChar;
    NextGlyph: PGlyfRecord;
    Hidden: boolean;
    IsTagged: boolean;
    Code: word;
  end;

  TCodeMapping = packed record
    Version: VISPDFULONG;
    NumTables: VISPDFULONG;
    PlatformID: VISPDFULONG;
    EncodingID: VISPDFULONG;
    TableOffset: VISPDFULONG;
    Format: VISPDFULONG;
    Length: VISPDFULONG;
    CodeVersion: VISPDFULONG;
    CodeSegment: VISPDFULONG;
    SearchRange: VISPDFULONG;
    EntrySelector: VISPDFULONG;
    RangeShift: VISPDFULONG;
    TabNumbers: VISPDFULONG;
    IDPlatform: VISPDFULONG;
    IDEncoding: VISPDFULONG;
  end;

  TVPDFFontTable = packed record
    Name: LongWord;
    CheckSum: LongInt;
    Offset: Integer;
    Size: Integer;
    Content: array of Byte;
  end;

  TPostTableType = packed record
    TwoVersion: VISPDFLFIXED;
    ItalicAngle: VISPDFLFIXED;
    UnderlinePosition: VISPDFFWORD;
    UnderlineThickness: VISPDFFWORD;
    IsFixedPitch: VISPDFULONG;
    MinMemType42: VISPDFULONG;
    MaxMemType42: VISPDFULONG;
    MinMemType1: VISPDFULONG;
    MaxMemType1: VISPDFULONG;
    NumberOfGlyphs1: VISPDFUSHORT;
    NumberOfGlyphs2: VISPDFUSHORT;
    GlyphNameIndex1: VISPDFULONG;
    GlyphNameIndex2: VISPDFULONG;
    GlyphNameIndex3: VISPDFULONG;
    GlyphNameIndex4: VISPDFULONG;
  end;

  GlyphCodes = record
    CODE: Word;
    UNICODE: Word;
    GlyphID: Word;
    SerialNumber: Word;
    Hidden: boolean;
  end;

const
  CLCT = 'Cannot load font CMAP table';
  CLRT = 'Cannot load required table';
  CLNT = 'Cannot load NAME table';
  CLPT = 'Cannot load POST table';
  CLGT = 'Cannot load GLYPH table';
  CmapTable4Offset = 283;

  FontStreamHeader: TFontHeader =
  (
    SFNTVersion: $00000100;
    NumTables: $0B00;
    SearchRange: $7000;
    EntrySelector: $0300;
    RangeShift: $1000;
    );

  MapPostTable: TPostTableType =
  (TwoVersion: $00000200; ItalicAngle: $00000000;
    UnderlinePosition: $27FF; UnderlineThickness: $9600;
    IsFixedPitch: $00000000; MinMemType42: $00000000;
    MaxMemType42: $00000000; MinMemType1: $00000000;
    MaxMemType1: $00000000;
    NumberOfGlyphs1: $0400; NumberOfGlyphs2: $0000;
    GlyphNameIndex1: $02010000; GlyphNameIndex2: $61094400;
    GlyphNameIndex3: $31696966; GlyphNameIndex4: $35363030;
    );

  CodeMapping: TCodeMapping =
  (Version: $02000000; NumTables: $00000100;
    PlatformID: $14000000; EncodingID: $00000300;
    TableOffset: $1A010000; Format: $06010000;
    Length: $00000000; CodeVersion: $00000000;
    CodeSegment: $00000000; SearchRange: $00000000;
    EntrySelector: $00000000; RangeShift: $00000000;
    TabNumbers: $00000000; IDPlatform: $00000000;
    IDEncoding: $00020000; );

  MarkGlyf: array[1..42] of byte = ($00, $02, $01, $00, $00, $00, $05, $00, $05, $00,
    $00, $03, $00, $07, $00, $00, $21, $11, $21, $11,
    $25, $21, $11, $21, $01, $00, $04, $00, $FC, $20,
    $03, $C0, $FC, $40, $05, $00, $FB, $00, $20, $04,
    $C0, $00);
implementation

end.
