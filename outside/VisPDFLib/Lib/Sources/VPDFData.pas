{*******************************************************}
{                                                       }
{       This unit is part of the VISPDF VCL library.    }
{       Written by R.Husske - ALL RIGHTS RESERVED.      }
{                                                       }
{       Copyright (C) 2000-2009, www.vispdf.com         }
{                                                       }
{       e-mail: support@vispdf.com                      }
{       http://www.vispdf.com                           }
{                                                       }
{*******************************************************}

unit VPDFData;

interface

uses
  Windows;

{$I VisPDFLib.inc }

type
  TVPDFDestinationType = (dtXYZ, dtFit, dtFitH, dtFitV, dtFitR, dtFitB, dtFitBH, dtFitBV);
  TVPDFHorizJust = (vhjLeft, vhjCenter, vhjRight);
  TVPDFVerticalJust = (vvjUp, vvjCenter, vvjDown);
  TXObjectID = integer;

  TVPDF_SIZES = record
    width: integer;
    heigh: integer;
  end;

  TVPDF_STR_TBL = record
    KEY: AnsiString;
    VAL: AnsiString;
  end;

  TVPDF_INT_TBL = record
    KEY: AnsiString;
    VAL: integer;
  end;

  TVPDFFontFeatures = record
    IsMonospaced: boolean;
    Ascent: Integer;
    FontLen: Integer;
    OutTextM: OUTLINETEXTMETRIC;
    ABCArray: array[0..255] of ABC;
  end;

const
  VPDFFree = 'f';
  VPDFInUse = 'n';
  PDFDefaultPageWidth = 596;
  PDFDefaultPageHeight = 842;
  VPDFDefaultFont = 'Arial';
  VPDFDefaultFontSize = 10;
  VPDFAuthor: AnsiString = 'TVPDF by R.Husske';
  MainStream: array[1..20] of byte = ($66, $46, $25, $74, $A8, $7B, $40, $74, $55, $A4, $51, $4C, $71, $13, $2A, $42, $6B, $91, $BE, $01);

  VPDFPageLayouts: array[0..3] of AnsiString = ('SinglePage', 'OneColumn', 'TwoColumnLeft', 'TwoColumnRight');
  VPDFPageModes: array[0..3] of AnsiString = ('UseNone', 'UseOutlines', 'UseThumbs', 'FullScreen');
  VPDFAnnotationTypes: array[0..12] of AnsiString = ('Text', 'Link', 'Popup', 'FreeText', 'Stamp', 'Square', 'Circle', 'StrikeOut', 'Underline', 'Ink', 'Highlight', 'Sound', 'FileAttachment');
  VPDFDestTypes: array[0..7] of AnsiString = ('XYZ', 'Fit', 'FitH', 'FitV', 'FitR', 'FitB', 'FitBH', 'FitBV');

implementation

end.
