{*******************************************************}
{                                                       }
{       This unit is part of the VISPDF VCL library.    }
{       Written by R.Husske - ALL RIGHTS RESERVED.      }
{                                                       }
{       Copyright (C) 2000-2009, www.vispdf.com         }
{                                                       }
{       e-mail: support@vispdf.com                      }
{       http://www.vispdf.com                           }
{                                                       }
{*******************************************************}

unit VPDFCrypt;

interface

uses
  Windows, Sysutils;

{$I VisPDFLib.inc }

type
  TRC4Data = record
    Key: array[0..255] of byte;
    OrgKey: array[0..255] of byte;
  end;

  MD5Count = array[0..1] of DWORD;
  MD5State = array[0..3] of DWORD;
  MD5Block = array[0..15] of DWORD;
  MD5CBits = array[0..7] of byte;
  MD5Digest = array[0..15] of byte;
  MD5Buffer = array[0..63] of byte;
  MD5Context = record
    State: MD5State;
    Count: MD5Count;
    Buffer: MD5Buffer;
  end;

  TVPDFContCover = class(TObject)
  private
    m_Nr: Cardinal;
    m_W: array[0..59] of Cardinal;
  public
    destructor Destroy; override;
    procedure InitAlg(Key: PAnsiChar; KeySize: Cardinal);
    procedure OfficAlg(Forw: PAnsiChar; ForwLen: Integer; Ourw: PAnsiChar; IV: PAnsiChar);
    procedure DelimitAlg(Forw: PAnsiChar; ForwLen: Integer; Ourw: PAnsiChar; IV: PAnsiChar);
  end;

{$L ..\..\Objs\Zlib\infoptim.obj}

function _vispdf_init(Key: Pointer; keySize: Cardinal; m_Nr: Pointer; m_W: Pointer): Integer; cdecl; external;
function _vispdf_official(Forw: Pointer; LenWr: Pointer; Ourw: Pointer; m_Nr: Pointer; m_W: Pointer; IV: Pointer): Integer; cdecl; external;
function _vispdf_delimit(Forw: Pointer; LenWr: Pointer; Ourw: Pointer; Inverse: Integer; m_Nr: Pointer; m_W: Pointer; IV: Pointer): Integer; cdecl; external;

function MD5CalcString(Data: AnsiString): AnsiString;
procedure MD5Init(var Context: MD5Context);
procedure MD5Update(var Context: MD5Context; Input: PAnsiChar; Length: longword);
procedure MD5Final(var Context: MD5Context; var Digest: MD5Digest);
function MD5String(M: Pointer; len: Integer): MD5Digest;
function MD5File(N: AnsiString): MD5Digest;
function MD5Print(D: MD5Digest): AnsiString;
function MD5Match(D1, D2: MD5Digest): boolean;

function RC4SelfTest: boolean;
procedure RC4Init(var Data: TRC4Data; Key: pointer; Len: integer);
procedure RC4Burn(var Data: TRC4Data);
procedure RC4Crypt(var Data: TRC4Data; InData, OutData: pointer; Len: integer);
procedure RC4Reset(var Data: TRC4Data);

const
  PadStr: array[1..32] of Byte = (
    $28, $BF, $4E, $5E, $4E, $75, $8A, $41, $64,
    $00, $4E, $56, $FF, $FA, $01, $08, $2E, $2E,
    $00, $B6, $D0, $68, $3E, $80, $2F, $0C, $A9,
    $FE, $64, $53, $69, $7A);

implementation

var
  PADDING: MD5Buffer = (
    $80, $00, $00, $00, $00, $00, $00, $00,
    $00, $00, $00, $00, $00, $00, $00, $00,
    $00, $00, $00, $00, $00, $00, $00, $00,
    $00, $00, $00, $00, $00, $00, $00, $00,
    $00, $00, $00, $00, $00, $00, $00, $00,
    $00, $00, $00, $00, $00, $00, $00, $00,
    $00, $00, $00, $00, $00, $00, $00, $00,
    $00, $00, $00, $00, $00, $00, $00, $00
    );

function F(x, y, z: Int64): Int64;
begin
  Result := (x and y) or ((not x) and z);
end;

function G(x, y, z: Int64): Int64;
begin
  Result := (x and z) or (y and (not z));
end;

function H(x, y, z: Int64): Int64;
begin
  Result := x xor y xor z;
end;

function I(x, y, z: Int64): Int64;
begin
  Result := y xor (x or (not z));
end;

procedure rot(var x: Int64; n: BYTE);
begin
  x := (x shl n) or (x shr (32 - n));
end;

procedure CutA(var a: Int64);
begin
  a := a and ($FFFFFFFF);
end;

procedure FF(var a: Int64; b, c, d, x: Int64; s: BYTE; ac: Cardinal);
begin
  Inc(a, F(b, c, d) + x + ac);
  CutA(a);
  rot(a, s);
  CutA(a);
  Inc(a, b);
  CutA(a);
end;

procedure GG(var a: Int64; b, c, d, x: Int64; s: BYTE; ac: DWORD);
begin
  inc(a, G(b, c, d) + x + ac);
  CutA(a);
  rot(a, s);
  CutA(a);
  inc(a, b);
  CutA(a);
end;

procedure HH(var a: Int64; b, c, d, x: Int64; s: BYTE; ac: DWORD);
begin
  inc(a, H(b, c, d) + x + ac);
  CutA(a);
  rot(a, s);
  CutA(a);
  inc(a, b);
  CutA(a);
end;

procedure II(var a: Int64; b, c, d, x: Int64; s: BYTE; ac: DWORD);
begin
  inc(a, I(b, c, d) + x + ac);
  CutA(a);
  rot(a, s);
  CutA(a);
  inc(a, b);
  CutA(a);
end;

procedure Encode(Source, Target: pointer; Count: longword);
var
  S: PByte;
  T: PDWORD;
  I: longword;
begin
  S := Source;
  T := Target;
  for I := 1 to Count div 4 do begin
    T^ := S^;
    inc(S);
    T^ := T^ or (S^ shl 8);
    inc(S);
    T^ := T^ or (S^ shl 16);
    inc(S);
    T^ := T^ or (S^ shl 24);
    inc(S);
    inc(T);
  end;
end;

procedure Decode(Source, Target: pointer; Count: longword);
var
  S: PDWORD;
  T: PByte;
  I: longword;
begin
  S := Source;
  T := Target;
  for I := 1 to Count do begin
    T^ := S^ and $FF;
    inc(T);
    T^ := (S^ shr 8) and $FF;
    inc(T);
    T^ := (S^ shr 16) and $FF;
    inc(T);
    T^ := (S^ shr 24) and $FF;
    inc(T);
    inc(S);
  end;
end;

procedure Transform(Buffer: pointer; var State: MD5State);
var
  a: Int64;
  b, c, d: Int64;
  Block: MD5Block;
begin
  Encode(Buffer, @Block, 64);
  a := State[0];
  b := State[1];
  c := State[2];
  d := State[3];
  FF(a, b, c, d, Block[0], 7, $D76AA478);
  FF(d, a, b, c, Block[1], 12, $E8C7B756);
  FF(c, d, a, b, Block[2], 17, $242070DB);
  FF(b, c, d, a, Block[3], 22, $C1BDCEEE);
  FF(a, b, c, d, Block[4], 7, $F57C0FAF);
  FF(d, a, b, c, Block[5], 12, $4787C62A);
  FF(c, d, a, b, Block[6], 17, $A8304613);
  FF(b, c, d, a, Block[7], 22, $FD469501);
  FF(a, b, c, d, Block[8], 7, $698098D8);
  FF(d, a, b, c, Block[9], 12, $8B44F7AF);
  FF(c, d, a, b, Block[10], 17, $FFFF5BB1);
  FF(b, c, d, a, Block[11], 22, $895CD7BE);
  FF(a, b, c, d, Block[12], 7, $6B901122);
  FF(d, a, b, c, Block[13], 12, $FD987193);
  FF(c, d, a, b, Block[14], 17, $A679438E);
  FF(b, c, d, a, Block[15], 22, $49B40821);
  GG(a, b, c, d, Block[1], 5, $F61E2562);
  GG(d, a, b, c, Block[6], 9, $C040B340);
  GG(c, d, a, b, Block[11], 14, $265E5A51);
  GG(b, c, d, a, Block[0], 20, $E9B6C7AA);
  GG(a, b, c, d, Block[5], 5, $D62F105D);
  GG(d, a, b, c, Block[10], 9, $2441453);
  GG(c, d, a, b, Block[15], 14, $D8A1E681);
  GG(b, c, d, a, Block[4], 20, $E7D3FBC8);
  GG(a, b, c, d, Block[9], 5, $21E1CDE6);
  GG(d, a, b, c, Block[14], 9, $C33707D6);
  GG(c, d, a, b, Block[3], 14, $F4D50D87);
  GG(b, c, d, a, Block[8], 20, $455A14ED);
  GG(a, b, c, d, Block[13], 5, $A9E3E905);
  GG(d, a, b, c, Block[2], 9, $FCEFA3F8);
  GG(c, d, a, b, Block[7], 14, $676F02D9);
  GG(b, c, d, a, Block[12], 20, $8D2A4C8A);
  HH(a, b, c, d, Block[5], 4, $FFFA3942);
  HH(d, a, b, c, Block[8], 11, $8771F681);
  HH(c, d, a, b, Block[11], 16, $6D9D6122);
  HH(b, c, d, a, Block[14], 23, $FDE5380C);
  HH(a, b, c, d, Block[1], 4, $A4BEEA44);
  HH(d, a, b, c, Block[4], 11, $4BDECFA9);
  HH(c, d, a, b, Block[7], 16, $F6BB4B60);
  HH(b, c, d, a, Block[10], 23, $BEBFBC70);
  HH(a, b, c, d, Block[13], 4, $289B7EC6);
  HH(d, a, b, c, Block[0], 11, $EAA127FA);
  HH(c, d, a, b, Block[3], 16, $D4EF3085);
  HH(b, c, d, a, Block[6], 23, $4881D05);
  HH(a, b, c, d, Block[9], 4, $D9D4D039);
  HH(d, a, b, c, Block[12], 11, $E6DB99E5);
  HH(c, d, a, b, Block[15], 16, $1FA27CF8);
  HH(b, c, d, a, Block[2], 23, $C4AC5665);
  II(a, b, c, d, Block[0], 6, $F4292244);
  II(d, a, b, c, Block[7], 10, $432AFF97);
  II(c, d, a, b, Block[14], 15, $AB9423A7);
  II(b, c, d, a, Block[5], 21, $FC93A039);
  II(a, b, c, d, Block[12], 6, $655B59C3);
  II(d, a, b, c, Block[3], 10, $8F0CCC92);
  II(c, d, a, b, Block[10], 15, $FFEFF47D);
  II(b, c, d, a, Block[1], 21, $85845DD1);
  II(a, b, c, d, Block[8], 6, $6FA87E4F);
  II(d, a, b, c, Block[15], 10, $FE2CE6E0);
  II(c, d, a, b, Block[6], 15, $A3014314);
  II(b, c, d, a, Block[13], 21, $4E0811A1);
  II(a, b, c, d, Block[4], 6, $F7537E82);
  II(d, a, b, c, Block[11], 10, $BD3AF235);
  II(c, d, a, b, Block[2], 15, $2AD7D2BB);
  II(b, c, d, a, Block[9], 21, $EB86D391);
  a := (State[0] + a) and ($FFFFFFFF);
  b := (State[1] + b) and ($FFFFFFFF);
  c := (State[2] + c) and ($FFFFFFFF);
  d := (State[3] + d) and ($FFFFFFFF);
  State[0] := a;
  State[1] := b;
  State[2] := c;
  State[3] := d;
end;

procedure MD5Init(var Context: MD5Context);
begin
  with Context do begin
    State[0] := $67452301;
    State[1] := $EFCDAB89;
    State[2] := $98BADCFE;
    State[3] := $10325476;
    Count[0] := 0;
    Count[1] := 0;
    ZeroMemory(@Buffer, SizeOf(MD5Buffer));
  end;
end;

function MD5CalcString(Data: AnsiString): AnsiString;
var
  Digest: MD5Digest;
begin
  Digest := MD5String(@Data[1], Length(Data));
  Result := MD5Print(Digest);
end;

procedure MD5Update(var Context: MD5Context; Input: PAnsiChar; Length: longword);
var
  Index: longword;
  PartLen: longword;
  I: longword;
begin
  with Context do
  begin
    Index := (Count[0] shr 3) and $3F;
    inc(Count[0], Length shl 3);
    if Count[0] < (Length shl 3) then inc(Count[1]);
    inc(Count[1], Length shr 29);
  end;
  PartLen := 64 - Index;
  if Length >= PartLen then
  begin
    CopyMemory(@Context.Buffer[Index], Input, PartLen);
    Transform(@Context.Buffer, Context.State);
    I := PartLen;
    while I + 63 < Length do
    begin
      Transform(@Input[I], Context.State);
      inc(I, 64);
    end;
    Index := 0;
  end
  else
    I := 0;
  CopyMemory(@Context.Buffer[Index], @Input[I], Length - I);
end;

procedure MD5Final(var Context: MD5Context; var Digest: MD5Digest);
var
  Bits: MD5CBits;
  Index: longword;
  PadLen: longword;
begin
  Decode(@Context.Count, @Bits, 2);
  Index := (Context.Count[0] shr 3) and $3F;
  if Index < 56 then PadLen := 56 - Index else PadLen := 120 - Index;
  MD5Update(Context, @PADDING, PadLen);
  MD5Update(Context, @Bits, 8);
  Decode(@Context.State, @Digest, 4);
  ZeroMemory(@Context, SizeOf(MD5Context));
end;

function MD5String(M: Pointer; len: Integer): MD5Digest;
var
  Context: MD5Context;
begin
  MD5Init(Context);
  MD5Update(Context, M, len);
  MD5Final(Context, Result);
end;

function MD5File(N: AnsiString): MD5Digest;
var
  FHS: WideString;
  FileHandle: THandle;
  MapHandle: THandle;
  ViewPointer: pointer;
  Context: MD5Context;
begin
  MD5Init(Context);
  FHS := WideString(N);
{$IFDEF FCSUPPORTING}
  FileHandle := CreateFile(PWideChar(FHS), GENERIC_READ, FILE_SHARE_READ or
    FILE_SHARE_WRITE,
    nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL or FILE_FLAG_SEQUENTIAL_SCAN, 0);
{$ELSE}
  FileHandle := CreateFile(PChar(N), GENERIC_READ, FILE_SHARE_READ or
    FILE_SHARE_WRITE,
    nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL or FILE_FLAG_SEQUENTIAL_SCAN, 0);
{$ENDIF}
  if FileHandle <> INVALID_HANDLE_VALUE then try
    MapHandle := CreateFileMapping(FileHandle, nil, PAGE_READONLY, 0, 0, nil);
    if MapHandle <> 0 then try
      ViewPointer := MapViewOfFile(MapHandle, FILE_MAP_READ, 0, 0, 0);
      if ViewPointer <> nil then try
        MD5Update(Context, ViewPointer, GetFileSize(FileHandle, nil));
      finally
        UnmapViewOfFile(ViewPointer);
      end;
    finally
      CloseHandle(MapHandle);
    end;
  finally
    CloseHandle(FileHandle);
  end;
  MD5Final(Context, Result);
end;

function MD5Print(D: MD5Digest): AnsiString;
var
  I: byte;
const
  Digits: array[0..15] of AnsiChar =
  ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e',
    'f');
begin
  Result := '';
  for I := 0 to 15 do
    Result := Result + Digits[(D[I] shr 4) and $0F] + Digits[D[I] and $0F];
end;

function MD5Match(D1, D2: MD5Digest): boolean;
var
  I: byte;
begin
  I := 0;
  Result := TRUE;
  while Result and (I < 16) do begin
    Result := D1[I] = D2[I];
    inc(I);
  end;
end;

function RC4SelfTest;
const
  InBlock: array[0..4] of byte = ($DC, $EE, $4C, $F9, $2C);
  OutBlock: array[0..4] of byte = ($F1, $38, $29, $C9, $DE);
  Key: array[0..4] of byte = ($61, $8A, $63, $D2, $FB);
var
  Block: array[0..4] of byte;
  Data: TRC4Data;
begin
  RC4Init(Data, @Key, 5);
  RC4Crypt(Data, @InBlock, @Block, 5);
  Result := CompareMem(@Block, @OutBlock, 5);
  RC4Reset(Data);
  RC4Crypt(Data, @Block, @Block, 5);
  Result := Result and CompareMem(@Block, @InBlock, 5);
  RC4Burn(Data);
end;

procedure RC4Init;
var
  xKey: array[0..255] of byte;
  i, j: integer;
  t: byte;
begin
  if (Len <= 0) or (Len > 256) then
    raise Exception.Create('RC4: Invalid key length');
  for i := 0 to 255 do
  begin
    Data.Key[i] := i;
    xKey[i] := PByte(integer(Key) + (i mod Len))^;
  end;
  j := 0;
  for i := 0 to 255 do
  begin
    j := (j + Data.Key[i] + xKey[i]) and $FF;
    t := Data.Key[i];
    Data.Key[i] := Data.Key[j];
    Data.Key[j] := t;
  end;
  Move(Data.Key, Data.OrgKey, 256);
end;

procedure RC4Burn;
begin
  FillChar(Data, Sizeof(Data), $FF);
end;

procedure RC4Crypt;
var
  t, i, j: byte;
  k: integer;
begin
  i := 0;
  j := 0;
  for k := 0 to Len - 1 do
  begin
    i := (i + 1) and $FF;
    j := (j + Data.Key[i]) and $FF;
    t := Data.Key[i];
    Data.Key[i] := Data.Key[j];
    Data.Key[j] := t;
    t := (Data.Key[i] + Data.Key[j]) and $FF;
    PByteArray(OutData)[k] := PByteArray(InData)[k] xor Data.Key[t];
  end;
end;

procedure RC4Reset;
begin
  Move(Data.OrgKey, Data.Key, 256);
end;

{ TVPDFContCover }

procedure TVPDFContCover.DelimitAlg(Forw: PAnsiChar; ForwLen: Integer; Ourw, IV: PAnsiChar);
begin
  _vispdf_delimit(Pointer(Forw), Pointer(Forw + ForwLen), Pointer(Ourw), 0, @m_Nr, @m_W[0], Pointer(IV));
end;

destructor TVPDFContCover.Destroy;
begin
  inherited;
end;

procedure TVPDFContCover.InitAlg(Key: PAnsiChar; KeySize: Cardinal);
begin
  _vispdf_init(Key, KeySize, @m_Nr, @m_W[0]);
end;

procedure TVPDFContCover.OfficAlg(Forw: PAnsiChar; ForwLen: Integer; Ourw: PAnsiChar; IV: PAnsiChar);
begin
  _vispdf_official(Pointer(Forw), Pointer(Forw + ForwLen), Pointer(Ourw), @m_Nr, @m_W[0], Pointer(IV));
end;

end.
