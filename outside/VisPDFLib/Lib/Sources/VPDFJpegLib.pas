{*******************************************************}
{                                                       }
{       This unit is part of the VISPDF VCL library.    }
{       Written by R.Husske - ALL RIGHTS RESERVED.      }
{                                                       }
{       Copyright (C) 2000-2009, www.vispdf.com         }
{                                                       }
{       e-mail: support@vispdf.com                      }
{       http://www.vispdf.com                           }
{                                                       }
{*******************************************************}

unit VPDFJpegLib;

interface

uses
  Windows, SysUtils;

{$I VisPDFLib.inc }

const
  JPEG_LIB_VERSION = 62;
  JMSG_STR_PARM_MAX = 80;
  JMSG_LENGTH_MAX = 200;
  NUM_QUANT_TBLS = 4;
  NUM_HUFF_TBLS = 4;
  NUM_ARITH_TBLS = 16;
  MAX_COMPS_IN_SCAN = 4;
  C_MAX_BLOCKS_IN_MCU = 10;
  D_MAX_BLOCKS_IN_MCU = 10;
  DCTSIZE2 = 64;
  JCS_UNKNOWN = 0;
  JCS_GRAYSCALE = 1;
  JCS_RGB = 2;
  JCS_YCbCr = 3;
  JCS_CMYK = 4;
  JCS_YCCK = 5;

type
{$IFDEF VOLDVERSION}
  PPChar = ^PChar;
  PPointer = ^Pointer;
{$ENDIF}
  PRJpegErrorMgr = ^RJpegErrorMgr;
  PRJpegMemoryMgr = Pointer;
  PRJpegProgressMgr = Pointer;
  PRJpegDestinationMgr = ^RJpegDestinationMgr;
  PRJpegSourceMgr = ^RJpegSourceMgr;
  PRJpegComponentInfo = ^RJpegComponentInfo;
  PRJpegQuantTbl = Pointer;
  PRJpegHuffTbl = Pointer;
  PRJpegScanInfo = Pointer;
  PRJpegCompMaster = Pointer;
  PRJpegCMainController = Pointer;
  PRJpegCPrepController = Pointer;
  PRJpegCCoefController = Pointer;
  PRJpegMarkerWriter = Pointer;
  PRJpegColorConverter = Pointer;
  PRJpegDownsampler = Pointer;
  PRJpegForwardDct = Pointer;
  PRJpegEntropyEncoder = Pointer;
  PRJpegSavedMarker = Pointer;
  PRJpegDecompMaster = Pointer;
  PRJpegDMainController = Pointer;
  PRJpegDCoefController = Pointer;
  PRJpegDPosController = Pointer;
  PRJpegInputController = Pointer;
  PRJpegMarkerReader = Pointer;
  PRJpegEntropyDecoder = Pointer;
  PRJpegInverseDct = Pointer;
  PRJpegUpsampler = Pointer;
  PRJpegColorDeconverter = Pointer;
  PRJpegColorQuantizer = Pointer;
  PRJpegCommonStruct = ^RJpegCommonStruct;
  PRJpegCompressStruct = ^RJpegCompressStruct;
  PRJpegDecompressStruct = ^RJpegDecompressStruct;
  TJpegErrorExit = procedure(cinfo: PRJpegCommonStruct); cdecl;
  TJpegEmitMessage = procedure(cinfo: PRJpegCommonStruct; MsgLevel: Integer); cdecl;
  TJpegOutputMessage = procedure(cinfo: PRJpegCommonStruct); cdecl;
  TJpegFormatMessage = procedure(cinfo: PRJpegCommonStruct; Buffer: Pointer); cdecl;
  TJpegResetErrorMgr = procedure(cinfo: PRJpegCommonStruct); cdecl;
  RJpegErrorMgrMsgParm = record
    case Boolean of
      False: (MsgParmI: array[0..7] of Integer);
      True: (MsgParmS: array[0..JMSG_STR_PARM_MAX - 1] of AnsiChar);
  end;
  RJpegErrorMgr = record
    ErrorExit: TJpegErrorExit;
    EmitMessage: TJpegEmitMessage;
    OutputMessage: TJpegOutputMessage;
    FormatMessage: TJpegFormatMessage;
    ResetErrorMgr: TJpegResetErrorMgr;
    MsgCode: Integer;
    MsgParm: RJpegErrorMgrMsgParm;
    TraceLevel: Integer;
    NumWarnings: Integer;
    JpegMessageTable: PPChar;
    LastJpegMessage: Integer;
    AddonMessageTable: PPChar;
    FirstAddonMessage: Integer;
    LastAddonMessage: Integer;
  end;
  TJpegInitDestination = procedure(cinfo: PRJpegCompressStruct); cdecl;
  TJpegEmptyOutputBuffer = function(cinfo: PRJpegCompressStruct): Boolean; cdecl;
  TJpegTermDestination = procedure(cinfo: PRJpegCompressStruct); cdecl;
  RJpegDestinationMgr = record
    NextOutputByte: Pointer;
    FreeInBuffer: Cardinal;
    InitDestination: TJpegInitDestination;
    EmptyOutputBuffer: TJpegEmptyOutputBuffer;
    TermDestination: TJpegTermDestination;
  end;
  TJpegInitSource = procedure(cinfo: PRJpegDecompressStruct); cdecl;
  TJpegFillInputBuffer = function(cinfo: PRJpegDecompressStruct): Boolean; cdecl;
  TJpegSkipInputData = procedure(cinfo: PRJpegDecompressStruct; NumBytes: Integer); cdecl;
  TJpegResyncToRestart = function(cinfo: PRJpegDecompressStruct; Desired: Integer): Boolean; cdecl;
  TJpegTermSource = procedure(cinfo: PRJpegDecompressStruct); cdecl;
  RJpegSourceMgr = record
    NextInputByte: Pointer;
    BytesInBuffer: Cardinal;
    InitSource: TJpegInitSource;
    FillInputBuffer: TJpegFillInputBuffer;
    SkipInputData: TJpegSkipInputData;
    ResyncToRestart: TJpegResyncToRestart;
    TermSource: TJpegTermSource;
  end;
  RJpegComponentInfo = record
    ComponentId: Integer;
    ComponentIndex: Integer;
    HSampFactor: Integer;
    VSampFactor: Integer;
    QuantTblNo: Integer;
    DcTblNo: Integer;
    AsTblNo: Integer;
    WidthInBlocks: Cardinal;
    HeightInBlocks: Cardinal;
    DctScaledSize: Integer;
    DownsampledWidth: Cardinal;
    DownsampledHeight: Cardinal;
    ComponentNeeded: Boolean;
    McuWidth: Integer;
    McuHeight: Integer;
    McuBlocks: Integer;
    McuSampleWidth: Integer;
    LastColWidth: Integer;
    LastRowHeight: Integer;
    QuantTable: PRJpegQuantTbl;
    DctTable: Pointer;
  end;
  RJpegCommonStruct = record
    Err: PRJpegErrorMgr;
    Mem: PRJpegMemoryMgr;
    Progress: PRJpegProgressMgr;
    ClientData: Pointer;
    IsDecompressor: Boolean;
    GlobalState: Integer;
  end;
  RJpegCompressStruct = record
    Err: PRJpegErrorMgr;
    Mem: PRJpegMemoryMgr;
    Progress: PRJpegProgressMgr;
    ClientData: Pointer;
    IsDecompressor: Boolean;
    GlobalState: Integer;
    Dest: PRJpegDestinationMgr;
    ImageWidth: Cardinal;
    ImageHeight: Cardinal;
    InputComponents: Integer;
    InColorSpace: Integer;
    InputGamme: Double;
    DataPrecision: Integer;
    NumComponents: Integer;
    JpegColorSpace: Integer;
    CompInfo: PRJpegComponentInfo;
    QuantTblPtrs: array[0..NUM_QUANT_TBLS - 1] of PRJpegQuantTbl;
    DcHuffTblPtrs: array[0..NUM_HUFF_TBLS - 1] of PRJpegHuffTbl;
    AcHuffTblPtrs: array[0..NUM_HUFF_TBLS - 1] of PRJpegHuffTbl;
    ArithDcL: array[0..NUM_ARITH_TBLS - 1] of Byte;
    ArithDcU: array[0..NUM_ARITH_TBLS - 1] of Byte;
    ArithAcK: array[0..NUM_ARITH_TBLS - 1] of Byte;
    NumScans: Integer;
    ScanInfo: PRJpegScanInfo;
    RawDataIn: Boolean;
    ArithCode: Boolean;
    OptimizeCoding: Boolean;
    CCIR601Sampling: Boolean;
    SmoothingFactor: Integer;
    DctMethod: Integer;
    RestartInterval: Cardinal;
    RestartInRows: Integer;
    WriteJfifHeader: Boolean;
    JfifMajorVersion: Byte;
    JFifMinorVersion: Byte;
    DensityUnit: Byte;
    XDensity: Word;
    YDensity: WOrd;
    WriteAdobeMarker: Boolean;
    NextScanline: Cardinal;
    ProgressiveMode: Boolean;
    MaxHSampFactor: Integer;
    MaxVSampFactor: Integer;
    TotalIMCURows: Cardinal;
    CompsInScan: Integer;
    CurCompInfo: array[0..MAX_COMPS_IN_SCAN - 1] of PRJpegComponentInfo;
    MCUsPerRow: Cardinal;
    MCUsRowsInScan: Cardinal;
    BlocksInMcu: Integer;
    MCUMembership: array[0..C_MAX_BLOCKS_IN_MCU - 1] of Integer;
    Ss, Se, Ah, Al: Integer;
    Master: PRJpegCompMaster;
    Main: PRJpegCMainController;
    Prep: PRJpegCPrepController;
    Coef: PRJpegCCoefController;
    Marker: PRJpegMarkerWriter;
    CConvert: PRJpegColorConverter;
    Downsample: PRJpegDownsampler;
    FDct: PRJpegForwardDct;
    Entropy: PRJpegEntropyEncoder;
    ScriptSpace: PRJpegScanInfo;
    ScriptSpaceSize: Integer;
  end;
  RJpegDecompressStruct = record
    Err: PRJpegErrorMgr;
    Mem: PRJpegMemoryMgr;
    Progress: PRJpegProgressMgr;
    ClientData: Pointer;
    IsDecompressor: Boolean;
    GlobalState: Integer;
    Src: PRJpegSourceMgr;
    ImageWidth: Cardinal;
    ImageHeight: Cardinal;
    NumComponents: Integer;
    JpegColorSpace: Integer;
    OutColorSpace: Integer;
    ScaleNum, ScaleDenom: Cardinal;
    OutputGamme: Double;
    BufferedImage: Boolean;
    RawDataOut: Boolean;
    DctMethod: Integer;
    DoFancyUpsampling: Boolean;
    DoBlockSmoothing: Boolean;
    QuantizeColors: Boolean;
    DitherMode: Integer;
    TwoPassQuantize: Boolean;
    DesiredNumberOfColors: Integer;
    Enable1PassQuant: Boolean;
    EnableExternalQuant: Boolean;
    Enable2PassQuant: Boolean;
    OutputWidth: Cardinal;
    OutputHeight: Cardinal;
    OutColorComponents: Integer;
    OutputComponents: Integer;
    RecOutbufHeight: Integer;
    ActualNumberOfColors: Integer;
    Colormap: Pointer;
    OutputScanline: Cardinal;
    InputScanNumber: Integer;
    InputIMcuRow: Cardinal;
    OutputScanNumber: Integer;
    OutputIMcuRow: Cardinal;
    CoefBits: Pointer;
    QuantTblPtrs: array[0..NUM_QUANT_TBLS - 1] of Pointer;
    DcHuffTblPtrs: array[0..NUM_HUFF_TBLS - 1] of Pointer;
    AcHuffTblPtrs: array[0..NUM_HUFF_TBLS - 1] of Pointer;
    DataPrecision: Integer;
    CompInfo: PRJpegComponentInfo;
    ProgressiveMode: Boolean;
    ArithCode: Boolean;
    ArithDcL: array[0..NUM_ARITH_TBLS - 1] of Byte;
    ArithDcY: array[0..NUM_ARITH_TBLS - 1] of Byte;
    ArithAcK: array[0..NUM_ARITH_TBLS - 1] of Byte;
    RestartInterval: Cardinal;
    SawJfifMarker: Boolean;
    JfifMajorVersion: Byte;
    JfifMinorVersion: Byte;
    XDensity: Word;
    YDensity: Word;
    SawAdobeMarker: Boolean;
    AdobeTransform: Byte;
    Ccir601Sampling: Boolean;
    MarkerList: PRJpegSavedMarker;
    MaxHSampFactor: Integer;
    MaxVSampFactor: Integer;
    MinDctScaledSize: Integer;
    TotalIMcuRows: Cardinal;
    SampleRangeLimit: Pointer;
    CompsInScan: Integer;
    CurCompInfo: array[0..MAX_COMPS_IN_SCAN - 1] of PRJpegComponentInfo;
    McusPerRow: Cardinal;
    McuRowsInScan: Cardinal;
    BlocksInMcu: Integer;
    McuMembership: array[0..D_MAX_BLOCKS_IN_MCU - 1] of Integer;
    Ss, Se, Ah, Al: Integer;
    UnreadMarker: Integer;
    Master: PRJpegDecompMaster;
    Main: PRJpegDMainController;
    Coef: PRJpegDCoefController;
    Post: PRJpegDPosController;
    InputCtl: PRJpegInputController;
    Marker: PRJpegMarkerReader;
    Entropy: PRJpegEntropyDecoder;
    IDct: PRJpegInverseDct;
    Upsample: PRJpegUpsampler;
    CConvert: PRJpegColorDeconverter;
    CQuantize: PRJpegColorQuantizer;
  end;
procedure jpeg_create_compress(cinfo: PRJpegCompressStruct); cdecl;
procedure jpeg_CreateCompress(cinfo: PRJpegCompressStruct; version: Integer; structsize: Cardinal); cdecl; external;
procedure jpeg_create_decompress(cinfo: PRJpegDecompressStruct); cdecl;
procedure jpeg_CreateDecompress(cinfo: PRJpegDecompressStruct; version: Integer; structsize: Cardinal); cdecl; external;
procedure jpeg_abort(cinfo: PRJpegCommonStruct); cdecl; external;
procedure jpeg_set_defaults(cinfo: PRJpegCompressStruct); cdecl; external;
procedure jpeg_set_colorspace(cinfo: PRJpegCompressStruct; colorspace: Integer); cdecl; external;
procedure jpeg_set_quality(cinfo: PRJpegCompressStruct; quality: Integer; force_baseline: Byte); cdecl; external;
procedure jpeg_suppress_tables(cinfo: PRJpegCompressStruct; suppress: Byte); cdecl; external;
procedure jpeg_start_compress(cinfo: PRJpegCompressStruct; write_all_tables: Byte); cdecl; external;
function jpeg_write_scanlines(cinfo: PRJpegCompressStruct; scanlines: PPointer; num_lines: Cardinal): Cardinal; cdecl; external;
function jpeg_write_raw_data(cinfo: PRJpegCompressStruct; data: Pointer; num_lines: Cardinal): Cardinal; cdecl; external;
procedure jpeg_finish_compress(cinfo: PRJpegCompressStruct); cdecl; external;
procedure jpeg_write_tables(cinfo: PRJpegCompressStruct); cdecl; external;
function jpeg_read_header(cinfo: PRJpegDecompressStruct; require_image: Boolean): Integer; cdecl; external;
function jpeg_start_decompress(cinfo: PRJpegDecompressStruct): Byte; cdecl; external;
function jpeg_read_scanlines(cinfo: PRJpegDecompressStruct; scanlines: Pointer; max_lines: Cardinal): Cardinal; cdecl; external;
function jpeg_read_raw_data(cinfo: PRJpegDecompressStruct; data: Pointer; max_lines: Cardinal): Cardinal; cdecl; external;
function jpeg_finish_decompress(cinfo: PRJpegDecompressStruct): Byte; cdecl; external;
procedure jpeg_destroy(cinfo: PRJpegCommonStruct); cdecl; external;
function jpeg_std_error(err: PRJpegErrorMgr): Pointer; cdecl; external;
function jpeg_resync_to_restart(cinfo: PRJpegDecompressStruct; desired: Integer): Byte; cdecl; external;

implementation

uses
  VPDFCLibs;

procedure jpeg_error_exit_raise; cdecl;
begin
  raise Exception.Create('LibJpeg error_exit');
end;

procedure jpeg_create_compress(cinfo: PRJpegCompressStruct); cdecl;
begin
  jpeg_CreateCompress(cinfo, JPEG_LIB_VERSION, SizeOf(RJpegCompressStruct));
end;

procedure jpeg_create_decompress(cinfo: PRJpegDecompressStruct); cdecl;
begin
  jpeg_CreateDecompress(cinfo, JPEG_LIB_VERSION, SizeOf(RJpegDecompressStruct));
end;

function jpeg_get_small(cinfo: PRJpegCommonStruct; sizeofobject: Cardinal): Pointer; cdecl; external;

function jpeg_get_large(cinfo: PRJpegCommonStruct; sizeofobject: Cardinal): Pointer; cdecl; external;

function jpeg_mem_available(cinfo: PRJpegCommonStruct; min_bytes_needed: Integer; max_bytes_needed: Integer; already_allocated: Integer): Integer; cdecl; external;

procedure jpeg_open_backing_store(cinfo: PRJpegCommonStruct; info: Pointer; total_bytes_needed: Integer); cdecl; external;

procedure jpeg_free_large(cinfo: PRJpegCommonStruct; objectt: Pointer; sizeofobject: Cardinal); cdecl; external;

procedure jpeg_free_small(cinfo: PRJpegCommonStruct; objectt: Pointer; sizeofobject: Cardinal); cdecl; external;

procedure jpeg_mem_term(cinfo: PRJpegCommonStruct); cdecl; external;

function jpeg_mem_init(cinfo: PRJpegCommonStruct): Integer; cdecl; external;
{$L ..\..\Objs\Jpeg\jmemnobs.obj}

procedure jinit_memory_mgr(cinfo: PRJpegCommonStruct); cdecl; external;
{$L ..\..\Objs\Jpeg\jmemmgr.obj}

function jpeg_alloc_huff_table(cinfo: PRJpegCommonStruct): Pointer; cdecl; external;

function jpeg_alloc_quant_table(cinfo: PRJpegCommonStruct): Pointer; cdecl; external;
{$L ..\..\Objs\Jpeg\jcomapi.obj}
{$L ..\..\Objs\Jpeg\jerror.obj}
{$L ..\..\Objs\Jpeg\jcapimin.obj}
{$L ..\..\Objs\Jpeg\jcmarker.obj}

function jdiv_round_up(a: Integer; b: Integer): Integer; cdecl; external;

procedure jcopy_sample_rows(input_array: Pointer; source_row: Integer; output_array: Pointer; dest_row: Integer; num_rows: Integer;
  num_cols: Cardinal); cdecl; external;

function jround_up(a: Integer; b: Integer): Integer; cdecl; external;

procedure jcopy_block_row(input_row: Pointer; output_row: Pointer; num_blocks: Cardinal); cdecl; external;
{$L ..\..\Objs\Jpeg\jutils.obj}
{$L ..\..\Objs\Jpeg\jdapimin.obj}
{$L ..\..\Objs\Jpeg\jdmarker.obj}
{$L ..\..\Objs\Jpeg\jdinput.obj}
{$L ..\..\Objs\Jpeg\jcparam.obj}
{$L ..\..\Objs\Jpeg\jcapistd.obj}
{$L ..\..\Objs\Jpeg\jcinit.obj}
{$L ..\..\Objs\Jpeg\jcmaster.obj}
{$L ..\..\Objs\Jpeg\jccolor.obj}
{$L ..\..\Objs\Jpeg\jcsample.obj}
{$L ..\..\Objs\Jpeg\jcprepct.obj}
{$L ..\..\Objs\Jpeg\jcdctmgr.obj}
{$L ..\..\Objs\Jpeg\jcphuff.obj}
{$L ..\..\Objs\Jpeg\jchuff.obj}
{$L ..\..\Objs\Jpeg\jccoefct.obj}
{$L ..\..\Objs\Jpeg\jcmainct.obj}
{$L ..\..\Objs\Jpeg\jfdctint.obj}
{$L ..\..\Objs\Jpeg\jfdctfst.obj}
{$L ..\..\Objs\Jpeg\jfdctflt.obj}
{$L ..\..\Objs\Jpeg\jdapistd.obj}
{$L ..\..\Objs\Jpeg\jdmaster.obj}
{$L ..\..\Objs\Jpeg\jquant1.obj}
{$L ..\..\Objs\Jpeg\jquant2.obj}
{$L ..\..\Objs\Jpeg\jdmerge.obj}
{$L ..\..\Objs\Jpeg\jdcolor.obj}
{$L ..\..\Objs\Jpeg\jdsample.obj}
{$L ..\..\Objs\Jpeg\jdpostct.obj}
{$L ..\..\Objs\Jpeg\jddctmgr.obj}
{$L ..\..\Objs\Jpeg\jdphuff.obj}
{$L ..\..\Objs\Jpeg\jdhuff.obj}
{$L ..\..\Objs\Jpeg\jdcoefct.obj}
{$L ..\..\Objs\Jpeg\jdmainct.obj}
{$L ..\..\Objs\Jpeg\jidctred.obj}
{$L ..\..\Objs\Jpeg\jidctint.obj}
{$L ..\..\Objs\Jpeg\jidctfst.obj}
{$L ..\..\Objs\Jpeg\jidctflt.obj}

end.
