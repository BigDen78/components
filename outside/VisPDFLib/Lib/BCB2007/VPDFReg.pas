{*******************************************************}
{                                                       }
{       This unit is part of the VISPDF VCL library.    }
{       Written by R.Husske - ALL RIGHTS RESERVED.      }
{                                                       }
{       Copyright (C) 2000-2007, www.vispdf.com         }
{                                                       }
{       e-mail: support@vispdf.com                      }
{       http://www.vispdf.com                           }
{                                                       }
{*******************************************************}

unit VPDFReg;

interface

uses
  Windows, Messages, SysUtils, Classes, VPDFDoc, VPDFTypes, VPDFData;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('VisPDF', [TVPDF]);
end;

end.

 