// CodeGear C++Builder
// Copyright (c) 1995, 2007 by CodeGear
// All rights reserved

// (DO NOT EDIT: machine generated header) 'Vpdfcrypt.pas' rev: 11.00

#ifndef VpdfcryptHPP
#define VpdfcryptHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member functions
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <Sysinit.hpp>	// Pascal unit
#include <Windows.hpp>	// Pascal unit
#include <Sysutils.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Vpdfcrypt
{
//-- type declarations -------------------------------------------------------
#pragma pack(push,1)
struct TRC4Data
{
	
public:
	Byte Key[256];
	Byte OrgKey[256];
} ;
#pragma pack(pop)

typedef unsigned MD5Count[2];

typedef unsigned MD5State[4];

typedef unsigned MD5Block[16];

typedef Byte MD5CBits[8];

typedef Byte MD5Digest[16];

typedef Byte MD5Buffer[64];

#pragma pack(push,4)
struct MD5Context
{
	
public:
	unsigned State[4];
	unsigned Count[2];
	Byte Buffer[64];
} ;
#pragma pack(pop)

class DELPHICLASS TVPDFContCover;
class PASCALIMPLEMENTATION TVPDFContCover : public System::TObject 
{
	typedef System::TObject inherited;
	
private:
	unsigned m_Nr;
	unsigned m_W[60];
	
public:
	__fastcall virtual ~TVPDFContCover(void);
	void __fastcall InitAlg(char * Key, unsigned KeySize);
	void __fastcall OfficAlg(char * Forw, int ForwLen, char * Ourw, char * IV);
	void __fastcall DelimitAlg(char * Forw, int ForwLen, char * Ourw, char * IV);
public:
	#pragma option push -w-inl
	/* TObject.Create */ inline __fastcall TVPDFContCover(void) : System::TObject() { }
	#pragma option pop
	
};


//-- var, const, procedure ---------------------------------------------------
extern PACKAGE Byte PadStr[32];
extern PACKAGE void __fastcall MD5Init(MD5Context &Context);
extern PACKAGE AnsiString __fastcall MD5CalcString(AnsiString Data);
extern PACKAGE void __fastcall MD5Update(MD5Context &Context, char * Input, unsigned Length);
extern PACKAGE void __fastcall MD5Final(MD5Context &Context, Byte * Digest);
extern PACKAGE Byte __fastcall MD5String(void * M, int len);
extern PACKAGE Byte __fastcall MD5File(AnsiString N);
extern PACKAGE AnsiString __fastcall MD5Print(Byte * D);
extern PACKAGE bool __fastcall MD5Match(Byte * D1, Byte * D2);
extern PACKAGE bool __fastcall RC4SelfTest(void);
extern PACKAGE void __fastcall RC4Init(TRC4Data &Data, void * Key, int Len);
extern PACKAGE void __fastcall RC4Burn(TRC4Data &Data);
extern PACKAGE void __fastcall RC4Crypt(TRC4Data &Data, void * InData, void * OutData, int Len);
extern PACKAGE void __fastcall RC4Reset(TRC4Data &Data);
extern PACKAGE int __cdecl _vispdf_init(void * Key, unsigned keySize, void * m_Nr, void * m_W);
extern PACKAGE int __cdecl _vispdf_official(void * Forw, void * LenWr, void * Ourw, void * m_Nr, void * m_W, void * IV);
extern PACKAGE int __cdecl _vispdf_delimit(void * Forw, void * LenWr, void * Ourw, int Inverse, void * m_Nr, void * m_W, void * IV);

}	/* namespace Vpdfcrypt */
using namespace Vpdfcrypt;
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// Vpdfcrypt
