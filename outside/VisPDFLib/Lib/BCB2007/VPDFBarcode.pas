{*******************************************************}
{                                                       }
{       This unit is part of the VISPDF VCL library.    }
{       Written by R.Husske - ALL RIGHTS RESERVED.      }
{                                                       }
{       Copyright (C) 2000-2009, www.vispdf.com         }
{                                                       }
{       e-mail: support@vispdf.com                      }
{       http://www.vispdf.com                           }
{                                                       }
{*******************************************************}

unit VPDFBarcode;

interface

{$I VisPDFLib.inc }

uses
  Classes, Graphics;

type
  TVPDFBarLineType = (white, black, black_half);

  TVPDFBarcodeOption = (bcoNone, bcoCode, bcoTyp, bcoBoth);

  TVPDFShowTextPosition =
    (
    stpTopLeft,
    stpTopRight,
    stpTopCenter,
    stpBottomLeft,
    stpBottomRight,
    stpBottomCenter
    );

  TVPDFCheckSumMethod =
    (
    csmNone,
    csmModulo10
    );

  TVPDFBarcode = class(TComponent)
  private
    FHeight: integer;
    FText: AnsiString;
    FTop: integer;
    FLeft: integer;
    FModul: integer;
    FRatio: double;
    FTyp: Integer;
    FCheckSum: boolean;
    FShowText: TVPDFBarcodeOption;
    FAngle: double;
    FColor: TColor;
    FColorBar: TColor;
    FCheckSumMethod: TVPDFCheckSumMethod;
    FOnChange: TNotifyEvent;
    Modules: array[0..3] of shortint;
    FShowTextFont: TFont;
    FShowTextPosition: TVPDFShowTextPosition;
    procedure OneBarProps(code: AnsiChar; var Width: integer; var lt: TVPDFBarLineType);
    procedure DoLines(data: AnsiString; Canvas: TCanvas);
    function SetLen(pI: byte): AnsiString;
    function Code_2_5_interleaved: AnsiString;
    function Code_2_5_industrial: AnsiString;
    function Code_2_5_matrix: AnsiString;
    function Code_39: AnsiString;
    function Code_39Extended: AnsiString;
    function Code_128: AnsiString;
    function Code_93: AnsiString;
    function Code_93Extended: AnsiString;
    function Code_MSI: AnsiString;
    function Code_PostNet: AnsiString;
    function Code_Codabar: AnsiString;
    function Code_EAN8: AnsiString;
    function Code_EAN13: AnsiString;
    function Code_UPC_A: AnsiString;
    function Code_UPC_E0: AnsiString;
    function Code_UPC_E1: AnsiString;
    function Code_Supp5: AnsiString;
    function Code_Supp2: AnsiString;
    function GetTypText: AnsiString;
    procedure MakeModules;
    procedure SetModul(v: integer);
    function GetWidth: integer;
    procedure SetWidth(Value: integer);
    function DoCheckSumming(const data: AnsiString): AnsiString;
    procedure SetRatio(const Value: Double);
    procedure SetTyp(const Value: Integer);
    procedure SetAngle(const Value: Double);
    procedure SetText(const Value: AnsiString);
    procedure SetShowText(const Value: TVPDFBarcodeOption);
    procedure SetTop(const Value: Integer);
    procedure SetLeft(const Value: Integer);
    procedure SetCheckSum(const Value: Boolean);
    procedure SetHeight(const Value: integer);
    function GetCanvasHeight: Integer;
    function GetCanvasWidth: Integer;
    procedure SetShowTextFont(const Value: TFont);
    procedure SetShowTextPosition(const Value: TVPDFShowTextPosition);
    function CheckSumModulo10(const Data: AnsiString): AnsiString;
  protected
    function MakeData: AnsiString;
    procedure DoChange; virtual;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure DrawBarcode(Canvas: TCanvas);
    procedure DrawText(Canvas: TCanvas);
    property CanvasHeight: Integer read GetCanvasHeight;
    property CanvasWidth: Integer read GetCanvasWidth;
  published
    property Height: integer read FHeight write SetHeight;
    property Text: AnsiString read FText write SetText;
    property Top: Integer read FTop write SetTop;
    property Left: Integer read FLeft write SetLeft;
    property Modul: integer read FModul write SetModul;
    property Ratio: Double read FRatio write SetRatio;
    property Typ: Integer read FTyp write SetTyp default
      0;
    property Checksum: boolean read FCheckSum write SetCheckSum default FALSE;
    property CheckSumMethod: TVPDFCheckSumMethod read FCheckSumMethod write
      FCheckSumMethod default csmModulo10;
    property Angle: double read FAngle write SetAngle;
    property ShowText: TVPDFBarcodeOption read FShowText write SetShowText default
      bcoNone;
    property ShowTextFont: TFont read FShowTextFont write SetShowTextFont;
    property ShowTextPosition: TVPDFShowTextPosition read FShowTextPosition write
      SetShowTextPosition default stpTopLeft;
    property Width: integer read GetWidth write SetWidth stored False;
    property Color: TColor read FColor write FColor default clWhite;
    property ColorBar: TColor read FColorBar write FColorBar default clBlack;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;

implementation

uses WinProcs, WinTypes, SysUtils, Math;

type
  TBCdata = record
    Name: AnsiString;
    num: Boolean;
  end;

const
  BCdata: array[0..22] of TBCdata =
  (
    (Name: '2_5_interleaved'; num: True),
    (Name: '2_5_industrial'; num: True),
    (Name: '2_5_matrix'; num: True),
    (Name: 'Code39'; num: False),
    (Name: 'Code39 Extended'; num: False),
    (Name: 'Code128A'; num: False),
    (Name: 'Code128B'; num: False),
    (Name: 'Code128C'; num: True),
    (Name: 'Code93'; num: False),
    (Name: 'Code93 Extended'; num: False),
    (Name: 'MSI'; num: True),
    (Name: 'PostNet'; num: True),
    (Name: 'Codebar'; num: False),
    (Name: 'EAN8'; num: True),
    (Name: 'EAN13'; num: True),
    (Name: 'UPC_A'; num: True),
    (Name: 'UPC_E0'; num: True),
    (Name: 'UPC_E1'; num: True),
    (Name: 'UPC Supp2'; num: True),
    (Name: 'UPC Supp5'; num: True),
    (Name: 'EAN128A'; num: False),
    (Name: 'EAN128B'; num: False),
    (Name: 'EAN128C'; num: True)
    );

function Trim(const S: AnsiString): AnsiString; export;
var
  I, L: Integer;
begin
  L := Length(S);
  I := 1;
  while (I <= L) and (S[I] <= ' ') do
    Inc(I);
  if I > L then
    Result := ''
  else
  begin
    while S[L] <= ' ' do
      Dec(L);
    Result := Copy(S, I, L - I + 1);
  end;
end;

function Convert(const s: AnsiString): AnsiString;
var
  I, v: integer;
begin
  Result := s;
  for I := 1 to Length(s) do
  begin
    v := ord(s[I]) - 1;
    if odd(I) then
      Inc(v, 5);
    Result[I] := AnsiChar(Chr(v));
  end;
end;

function QuerSumme(x: integer): integer;
var
  Sum: integer;
begin
  Sum := 0;
  while x > 0 do
  begin
    Sum := Sum + (x mod 10);
    x := x div 10;
  end;
  Result := Sum;
end;

function Rotate2D(p: TPoint; Alpha: double): TPoint;
var
  sinus, cosinus: Extended;
begin
  SinCos(Alpha, sinus, cosinus);
  Result.x := Round(p.x * cosinus + p.y * sinus);
  Result.y := Round(-p.x * sinus + p.y * cosinus);
end;

function Translate2D(a, b: TPoint): TPoint;
begin
  Result.x := a.x + b.x;
  Result.y := a.y + b.y;
end;

function TranslateQuad2D(const Alpha: double; const Orgin, point: TPoint):
  TPoint;
var
  alphacos: Extended;
  alphasin: Extended;
  moveby: TPoint;
begin
  SinCos(Alpha, alphasin, alphacos);
  if alphasin >= 0 then
  begin
    if alphacos >= 0 then
    begin
      moveby.x := 0;
      moveby.y := Round(alphasin * point.x);
    end
    else
    begin
      moveby.x := -Round(alphacos * point.x);
      moveby.y := Round(alphasin * point.x - alphacos * point.y);
    end;
  end
  else
  begin
    if alphacos >= 0 then
    begin
      moveby.x := -Round(alphasin * point.y);
      moveby.y := 0;
    end
    else
    begin
      moveby.x := -Round(alphacos * point.x) - Round(alphasin * point.y);
      moveby.y := -Round(alphacos * point.y);
    end;
  end;
  Result := Translate2D(Orgin, moveby);
end;

constructor TVPDFBarcode.Create(Owner: TComponent);
begin
  inherited Create(owner);
  FAngle := 0.0;
  FRatio := 2.0;
  FModul := 1;
  FTyp := 14;
  FCheckSum := FALSE;
  FCheckSumMethod := csmModulo10;
  FShowText := bcoCode;
  FColor := clWhite;
  FColorBar := clBlack;
  FShowTextFont := TFont.Create;
  FShowTextPosition := stpBottomCenter;
end;

destructor TVPDFBarcode.Destroy;
begin
  FShowTextFont.Free;
  inherited;
end;

procedure TVPDFBarcode.Assign(Source: TPersistent);
var
  BSource: TVPDFBarcode;
begin
  if Source is TVPDFBarcode then
  begin
    BSource := TVPDFBarcode(Source);
    FHeight := BSource.FHeight;
    FText := BSource.FText;
    FTop := BSource.FTop;
    FLeft := BSource.FLeft;
    FModul := BSource.FModul;
    FRatio := BSource.FRatio;
    FTyp := BSource.FTyp;
    FCheckSum := BSource.FCheckSum;
    FShowText := BSource.FShowText;
    FShowTextPosition := BSource.FShowTextPosition;
    FAngle := BSource.FAngle;
    FColor := BSource.FColor;
    FColorBar := BSource.FColorBar;
    FCheckSumMethod := BSource.FCheckSumMethod;
    FOnChange := BSource.FOnChange;
  end
  else
    inherited;
end;

function TVPDFBarcode.GetTypText: AnsiString;
begin
  Result := BCdata[FTyp].Name;
end;

procedure TVPDFBarcode.SetModul(v: integer);
begin
  if (v >= 1) and (v < 50) then
  begin
    FModul := v;
    DoChange;
  end;
end;

procedure TVPDFBarcode.OneBarProps(code: AnsiChar; var Width: integer; var lt:
  TVPDFBarLineType);
begin
  case code of
    '0':
      begin
        Width := Modules[0];
        lt := white;
      end;
    '1':
      begin
        Width := Modules[1];
        lt := white;
      end;
    '2':
      begin
        Width := Modules[2];
        lt := white;
      end;
    '3':
      begin
        Width := Modules[3];
        lt := white;
      end;
    '5':
      begin
        Width := Modules[0];
        lt := black;
      end;
    '6':
      begin
        Width := Modules[1];
        lt := black;
      end;
    '7':
      begin
        Width := Modules[2];
        lt := black;
      end;
    '8':
      begin
        Width := Modules[3];
        lt := black;
      end;
    'A':
      begin
        Width := Modules[0];
        lt := black_half;
      end;
    'B':
      begin
        Width := Modules[1];
        lt := black_half;
      end;
    'C':
      begin
        Width := Modules[2];
        lt := black_half;
      end;
    'D':
      begin
        Width := Modules[3];
        lt := black_half;
      end;
  else
    begin
      raise Exception.CreateFmt('%s: internal Error', [self.ClassName]);
    end;
  end;
end;

function TVPDFBarcode.MakeData: AnsiString;
var
  I: integer;
begin
  MakeModules;
  if BCdata[Typ].num then
  begin
    FText := Trim(FText);
    for I := 1 to Length(Ftext) do
      if (FText[I] > '9') or (FText[I] < '0') then
        raise Exception.Create('Barcode must be numeric');
  end;
  case Typ of
    0: Result := Code_2_5_interleaved;
    1: Result := Code_2_5_industrial;
    2: Result := Code_2_5_matrix;
    3: Result := Code_39;
    4: Result := Code_39Extended;
    5, 6, 7, 20, 21,
      22: Result := Code_128;
    8: Result := Code_93;
    9: Result := Code_93Extended;
    10: Result := Code_MSI;
    11: Result := Code_PostNet;
    12: Result := Code_Codabar;
    13: Result := Code_EAN8;
    14: Result := Code_EAN13;
    15: Result := Code_UPC_A;
    16: Result := Code_UPC_E0;
    17: Result := Code_UPC_E1;
    18: Result := Code_Supp2;
    19: Result := Code_Supp5;
  else
    raise Exception.CreateFmt('%s: wrong BarcodeType', [self.ClassName]);
  end;
end;

function TVPDFBarcode.GetWidth: integer;
var
  data: AnsiString;
  I: integer;
  w: integer;
  lt: TVPDFBarLineType;
begin
  Result := 0;
  data := MakeData;
  for I := 1 to Length(data) do
  begin
    OneBarProps(data[I], w, lt);
    Inc(Result, w);
  end;
end;

procedure TVPDFBarcode.SetWidth(Value: integer);
var
  data: AnsiString;
  I: integer;
  w, wtotal: integer;
  lt: TVPDFBarLineType;
begin
  wtotal := 0;
  data := MakeData;
  for I := 1 to Length(data) do
  begin
    OneBarProps(data[I], w, lt);
    Inc(wtotal, w);
  end;
  if wtotal > 0 then
    SetModul((FModul * Value) div wtotal);
end;

function TVPDFBarcode.DoCheckSumming(const data: AnsiString): AnsiString;
begin
  case FCheckSumMethod of
    csmNone:
      Result := data;
    csmModulo10:
      Result := CheckSumModulo10(data);
  end;
end;

const
  tabelle_EAN_A: array['0'..'9'] of AnsiString =
  (
    ('2605'),
    ('1615'),
    ('1516'),
    ('0805'),
    ('0526'),
    ('0625'),
    ('0508'),
    ('0706'),
    ('0607'),
    ('2506')
    );

const
  tabelle_EAN_C: array['0'..'9'] of AnsiString =
  (
    ('7150'),
    ('6160'),
    ('6061'),
    ('5350'),
    ('5071'),
    ('5170'),
    ('5053'),
    ('5251'),
    ('5152'),
    ('7051')
    );

function TVPDFBarcode.Code_EAN8: AnsiString;
var
  I: integer;
  tmp: AnsiString;
begin
  if FCheckSum then
  begin
    tmp := SetLen(7);
    tmp := DoCheckSumming(copy(tmp, length(tmp) - 6, 7));
  end
  else
    tmp := SetLen(8);
  Assert(Length(tmp) = 8, 'Invalid Text len (EAN8)');
  Result := '505';
  for I := 1 to 4 do
    Result := Result + tabelle_EAN_A[tmp[I]];
  Result := Result + '05050';
  for I := 5 to 8 do
    Result := Result + tabelle_EAN_C[tmp[I]];
  Result := Result + '505';
end;

const
  tabelle_EAN_B: array['0'..'9'] of AnsiString =
  (
    ('0517'),
    ('0616'),
    ('1606'),
    ('0535'),
    ('1705'),
    ('0715'),
    ('3505'),
    ('1525'),
    ('2515'),
    ('1507')
    );

const
  tabelle_ParityEAN13: array[0..9, 1..6] of AnsiChar =
  (
    ('A', 'A', 'A', 'A', 'A', 'A'),
    ('A', 'A', 'B', 'A', 'B', 'B'),
    ('A', 'A', 'B', 'B', 'A', 'B'),
    ('A', 'A', 'B', 'B', 'B', 'A'),
    ('A', 'B', 'A', 'A', 'B', 'B'),
    ('A', 'B', 'B', 'A', 'A', 'B'),
    ('A', 'B', 'B', 'B', 'A', 'A'),
    ('A', 'B', 'A', 'B', 'A', 'B'),
    ('A', 'B', 'A', 'B', 'B', 'A'),
    ('A', 'B', 'B', 'A', 'B', 'A')
    );

function TVPDFBarcode.Code_EAN13: AnsiString;
var
  I, LK: integer;
  tmp: AnsiString;
begin
  if FCheckSum then
  begin
    tmp := SetLen(12);
    tmp := DoCheckSumming(tmp);
  end
  else
    tmp := SetLen(13);
  Assert(Length(tmp) = 13, 'Invalid Text len (EAN13)');
  LK := StrToInt(String(AnsiChar(tmp[1])));
  tmp := copy(tmp, 2, 12);
  Result := '505';
  for I := 1 to 6 do
  begin
    case tabelle_ParityEAN13[LK, I] of
      'A': Result := Result + tabelle_EAN_A[tmp[I]];
      'B': Result := Result + tabelle_EAN_B[tmp[I]];
      'C': Result := Result + tabelle_EAN_C[tmp[I]];
    end;
  end;
  Result := Result + '05050';
  for I := 7 to 12 do
    Result := Result + tabelle_EAN_C[tmp[I]];
  Result := Result + '505';
end;

const
  tabelle_2_5: array['0'..'9', 1..5] of AnsiChar =
  (
    ('0', '0', '1', '1', '0'),
    ('1', '0', '0', '0', '1'),
    ('0', '1', '0', '0', '1'),
    ('1', '1', '0', '0', '0'),
    ('0', '0', '1', '0', '1'),
    ('1', '0', '1', '0', '0'),
    ('0', '1', '1', '0', '0'),
    ('0', '0', '0', '1', '1'),
    ('1', '0', '0', '1', '0'),
    ('0', '1', '0', '1', '0')
    );

function TVPDFBarcode.Code_2_5_interleaved: AnsiString;
var
  I, j: integer;
  c: AnsiChar;

begin
  Result := '5050';
  for I := 1 to Length(FText) div 2 do
  begin
    for j := 1 to 5 do
    begin
      if tabelle_2_5[FText[I * 2 - 1], j] = '1' then
        c := '6'
      else
        c := '5';
      Result := Result + c;
      if tabelle_2_5[FText[I * 2], j] = '1' then
        c := '1'
      else
        c := '0';
      Result := Result + c;
    end;
  end;

  Result := Result + '605';
end;

function TVPDFBarcode.Code_2_5_industrial: AnsiString;
var
  I, j: integer;
begin
  Result := '606050';
  for I := 1 to Length(FText) do
  begin
    for j := 1 to 5 do
    begin
      if tabelle_2_5[FText[I], j] = '1' then
        Result := Result + '60'
      else
        Result := Result + '50';
    end;
  end;
  Result := Result + '605060';
end;

function TVPDFBarcode.Code_2_5_matrix: AnsiString;
var
  I, j: integer;
  c: AnsiChar;
begin
  Result := '705050';
  for I := 1 to Length(FText) do
  begin
    for j := 1 to 5 do
    begin
      if tabelle_2_5[FText[I], j] = '1' then
        c := '1'
      else
        c := '0';
      if odd(j) then
        c := AnsiChar(chr(ord(c) + 5));
      Result := Result + c;
    end;
    Result := Result + '0';
  end;
  Result := Result + '70505';
end;

function TVPDFBarcode.Code_39: AnsiString;
type
  TCode39 =
    record
    c: AnsiChar;
    data: array[0..9] of AnsiChar;
    chk: shortint;
  end;

const
  tabelle_39: array[0..43] of TCode39 = (
    (c: '0'; data: '505160605'; chk: 0),
    (c: '1'; data: '605150506'; chk: 1),
    (c: '2'; data: '506150506'; chk: 2),
    (c: '3'; data: '606150505'; chk: 3),
    (c: '4'; data: '505160506'; chk: 4),
    (c: '5'; data: '605160505'; chk: 5),
    (c: '6'; data: '506160505'; chk: 6),
    (c: '7'; data: '505150606'; chk: 7),
    (c: '8'; data: '605150605'; chk: 8),
    (c: '9'; data: '506150605'; chk: 9),
    (c: 'A'; data: '605051506'; chk: 10),
    (c: 'B'; data: '506051506'; chk: 11),
    (c: 'C'; data: '606051505'; chk: 12),
    (c: 'D'; data: '505061506'; chk: 13),
    (c: 'E'; data: '605061505'; chk: 14),
    (c: 'F'; data: '506061505'; chk: 15),
    (c: 'G'; data: '505051606'; chk: 16),
    (c: 'H'; data: '605051605'; chk: 17),
    (c: 'I'; data: '506051605'; chk: 18),
    (c: 'J'; data: '505061605'; chk: 19),
    (c: 'K'; data: '605050516'; chk: 20),
    (c: 'L'; data: '506050516'; chk: 21),
    (c: 'M'; data: '606050515'; chk: 22),
    (c: 'N'; data: '505060516'; chk: 23),
    (c: 'O'; data: '605060515'; chk: 24),
    (c: 'P'; data: '506060515'; chk: 25),
    (c: 'Q'; data: '505050616'; chk: 26),
    (c: 'R'; data: '605050615'; chk: 27),
    (c: 'S'; data: '506050615'; chk: 28),
    (c: 'T'; data: '505060615'; chk: 29),
    (c: 'U'; data: '615050506'; chk: 30),
    (c: 'V'; data: '516050506'; chk: 31),
    (c: 'W'; data: '616050505'; chk: 32),
    (c: 'X'; data: '515060506'; chk: 33),
    (c: 'Y'; data: '615060505'; chk: 34),
    (c: 'Z'; data: '516060505'; chk: 35),
    (c: '-'; data: '515050606'; chk: 36),
    (c: '.'; data: '615050605'; chk: 37),
    (c: ' '; data: '516050605'; chk: 38),
    (c: '*'; data: '515060605'; chk: 0),
    (c: '$'; data: '515151505'; chk: 39),
    (c: '/'; data: '515150515'; chk: 40),
    (c: '+'; data: '515051515'; chk: 41),
    (c: '%'; data: '505151515'; chk: 42)
    );

  function FindIdx(z: AnsiChar): integer;
  var
    I: integer;
  begin
    for I := 0 to High(tabelle_39) do
    begin
      if z = tabelle_39[I].c then
      begin
        Result := I;
        exit;
      end;
    end;
    Result := -1;
  end;

var
  I, idx: integer;
  checksum: integer;

begin
  checksum := 0;
  Result := tabelle_39[FindIdx('*')].data + '0';
  for I := 1 to Length(FText) do
  begin
    idx := FindIdx(AnsiChar(FText[I]));
    if idx < 0 then
      continue;
    Result := Result + tabelle_39[idx].data + '0';
    Inc(checksum, tabelle_39[idx].chk);
  end;
  if FCheckSum then
  begin
    checksum := checksum mod 43;
    for I := 0 to High(tabelle_39) do
      if checksum = tabelle_39[I].chk then
      begin
        Result := Result + tabelle_39[I].data + '0';
        break;
      end;
  end;
  Result := Result + tabelle_39[FindIdx('*')].data;
end;

function TVPDFBarcode.Code_39Extended: AnsiString;

const
  code39x: array[0..127] of AnsiString =
  (
    ('%U'), ('$A'), ('$B'), ('$C'), ('$D'), ('$E'), ('$F'), ('$G'),
    ('$H'), ('$I'), ('$J'), ('$K'), ('$L'), ('$M'), ('$N'), ('$O'),
    ('$P'), ('$Q'), ('$R'), ('$S'), ('$T'), ('$U'), ('$V'), ('$W'),
    ('$X'), ('$Y'), ('$Z'), ('%A'), ('%B'), ('%C'), ('%D'), ('%E'),
    (' '), ('/A'), ('/B'), ('/C'), ('/D'), ('/E'), ('/F'), ('/G'),
    ('/H'), ('/I'), ('/J'), ('/K'), ('/L'), ('/M'), ('/N'), ('/O'),
    ('0'), ('1'), ('2'), ('3'), ('4'), ('5'), ('6'), ('7'),
    ('8'), ('9'), ('/Z'), ('%F'), ('%G'), ('%H'), ('%I'), ('%J'),
    ('%V'), ('A'), ('B'), ('C'), ('D'), ('E'), ('F'), ('G'),
    ('H'), ('I'), ('J'), ('K'), ('L'), ('M'), ('N'), ('O'),
    ('P'), ('Q'), ('R'), ('S'), ('T'), ('U'), ('V'), ('W'),
    ('X'), ('Y'), ('Z'), ('%K'), ('%L'), ('%M'), ('%N'), ('%O'),
    ('%W'), ('+A'), ('+B'), ('+C'), ('+D'), ('+E'), ('+F'), ('+G'),
    ('+H'), ('+I'), ('+J'), ('+K'), ('+L'), ('+M'), ('+N'), ('+O'),
    ('+P'), ('+Q'), ('+R'), ('+S'), ('+T'), ('+U'), ('+V'), ('+W'),
    ('+X'), ('+Y'), ('+Z'), ('%P'), ('%Q'), ('%R'), ('%S'), ('%T')
    );

var
  save: AnsiString;
  I: integer;
begin
  save := FText;
  FText := '';
  for I := 1 to Length(save) do
  begin
    if ord(save[I]) <= 127 then
      FText := FText + code39x[ord(save[I])];
  end;
  Result := Code_39;
  FText := save;
end;

function TVPDFBarcode.Code_128: AnsiString;
type
  TCode128 =
    record
    a, b: AnsiChar;
    c: AnsiString;
    data: AnsiString;
  end;

const
  tabelle_128: array[0..102] of TCode128 = (
    (a: ' '; b: ' '; c: '00'; data: '212222'),
    (a: '!'; b: '!'; c: '01'; data: '222122'),
    (a: '"'; b: '"'; c: '02'; data: '222221'),
    (a: '#'; b: '#'; c: '03'; data: '121223'),
    (a: '$'; b: '$'; c: '04'; data: '121322'),
    (a: '%'; b: '%'; c: '05'; data: '131222'),
    (a: '&'; b: '&'; c: '06'; data: '122213'),
    (a: ''''; b: ''''; c: '07'; data: '122312'),
    (a: '('; b: '('; c: '08'; data: '132212'),
    (a: ')'; b: ')'; c: '09'; data: '221213'),
    (a: '*'; b: '*'; c: '10'; data: '221312'),
    (a: '+'; b: '+'; c: '11'; data: '231212'),
    (a: ','; b: ','; c: '12'; data: '112232'),
    (a: '-'; b: '-'; c: '13'; data: '122132'),
    (a: '.'; b: '.'; c: '14'; data: '122231'),
    (a: '/'; b: '/'; c: '15'; data: '113222'),
    (a: '0'; b: '0'; c: '16'; data: '123122'),
    (a: '1'; b: '1'; c: '17'; data: '123221'),
    (a: '2'; b: '2'; c: '18'; data: '223211'),
    (a: '3'; b: '3'; c: '19'; data: '221132'),
    (a: '4'; b: '4'; c: '20'; data: '221231'),
    (a: '5'; b: '5'; c: '21'; data: '213212'),
    (a: '6'; b: '6'; c: '22'; data: '223112'),
    (a: '7'; b: '7'; c: '23'; data: '312131'),
    (a: '8'; b: '8'; c: '24'; data: '311222'),
    (a: '9'; b: '9'; c: '25'; data: '321122'),
    (a: ':'; b: ':'; c: '26'; data: '321221'),
    (a: ';'; b: ';'; c: '27'; data: '312212'),
    (a: '<'; b: '<'; c: '28'; data: '322112'),
    (a: '='; b: '='; c: '29'; data: '322211'),
    (a: '>'; b: '>'; c: '30'; data: '212123'),
    (a: '?'; b: '?'; c: '31'; data: '212321'),
    (a: '@'; b: '@'; c: '32'; data: '232121'),
    (a: 'A'; b: 'A'; c: '33'; data: '111323'),
    (a: 'B'; b: 'B'; c: '34'; data: '131123'),
    (a: 'C'; b: 'C'; c: '35'; data: '131321'),
    (a: 'D'; b: 'D'; c: '36'; data: '112313'),
    (a: 'E'; b: 'E'; c: '37'; data: '132113'),
    (a: 'F'; b: 'F'; c: '38'; data: '132311'),
    (a: 'G'; b: 'G'; c: '39'; data: '211313'),
    (a: 'H'; b: 'H'; c: '40'; data: '231113'),
    (a: 'I'; b: 'I'; c: '41'; data: '231311'),
    (a: 'J'; b: 'J'; c: '42'; data: '112133'),
    (a: 'K'; b: 'K'; c: '43'; data: '112331'),
    (a: 'L'; b: 'L'; c: '44'; data: '132131'),
    (a: 'M'; b: 'M'; c: '45'; data: '113123'),
    (a: 'N'; b: 'N'; c: '46'; data: '113321'),
    (a: 'O'; b: 'O'; c: '47'; data: '133121'),
    (a: 'P'; b: 'P'; c: '48'; data: '313121'),
    (a: 'Q'; b: 'Q'; c: '49'; data: '211331'),
    (a: 'R'; b: 'R'; c: '50'; data: '231131'),
    (a: 'S'; b: 'S'; c: '51'; data: '213113'),
    (a: 'T'; b: 'T'; c: '52'; data: '213311'),
    (a: 'U'; b: 'U'; c: '53'; data: '213131'),
    (a: 'V'; b: 'V'; c: '54'; data: '311123'),
    (a: 'W'; b: 'W'; c: '55'; data: '311321'),
    (a: 'X'; b: 'X'; c: '56'; data: '331121'),
    (a: 'Y'; b: 'Y'; c: '57'; data: '312113'),
    (a: 'Z'; b: 'Z'; c: '58'; data: '312311'),
    (a: '['; b: '['; c: '59'; data: '332111'),
    (a: '\'; b: '\'; c: '60'; data: '314111'),
    (a: ']'; b: ']'; c: '61'; data: '221411'),
    (a: '^'; b: '^'; c: '62'; data: '431111'),
    (a: '_'; b: '_'; c: '63'; data: '111224'),
    (a: ' '; b: '`'; c: '64'; data: '111422'),
    (a: ' '; b: 'a'; c: '65'; data: '121124'),
    (a: ' '; b: 'b'; c: '66'; data: '121421'),
    (a: ' '; b: 'c'; c: '67'; data: '141122'),
    (a: ' '; b: 'd'; c: '68'; data: '141221'),
    (a: ' '; b: 'e'; c: '69'; data: '112214'),
    (a: ' '; b: 'f'; c: '70'; data: '112412'),
    (a: ' '; b: 'g'; c: '71'; data: '122114'),
    (a: ' '; b: 'h'; c: '72'; data: '122411'),
    (a: ' '; b: 'I'; c: '73'; data: '142112'),
    (a: ' '; b: 'j'; c: '74'; data: '142211'),
    (a: ' '; b: 'k'; c: '75'; data: '241211'),
    (a: ' '; b: 'l'; c: '76'; data: '221114'),
    (a: ' '; b: 'm'; c: '77'; data: '413111'),
    (a: ' '; b: 'n'; c: '78'; data: '241112'),
    (a: ' '; b: 'o'; c: '79'; data: '134111'),
    (a: ' '; b: 'p'; c: '80'; data: '111242'),
    (a: ' '; b: 'q'; c: '81'; data: '121142'),
    (a: ' '; b: 'r'; c: '82'; data: '121241'),
    (a: ' '; b: 's'; c: '83'; data: '114212'),
    (a: ' '; b: 't'; c: '84'; data: '124112'),
    (a: ' '; b: 'u'; c: '85'; data: '124211'),
    (a: ' '; b: 'v'; c: '86'; data: '411212'),
    (a: ' '; b: 'w'; c: '87'; data: '421112'),
    (a: ' '; b: 'x'; c: '88'; data: '421211'),
    (a: ' '; b: 'y'; c: '89'; data: '212141'),
    (a: ' '; b: 'z'; c: '90'; data: '214121'),
    (a: ' '; b: '{'; c: '91'; data: '412121'),
    (a: ' '; b: '|'; c: '92'; data: '111143'),
    (a: ' '; b: '}'; c: '93'; data: '111341'),
    (a: ' '; b: '~'; c: '94'; data: '131141'),
    (a: ' '; b: ' '; c: '95'; data: '114113'),
    (a: ' '; b: ' '; c: '96'; data: '114311'),
    (a: ' '; b: ' '; c: '97'; data: '411113'),
    (a: ' '; b: ' '; c: '98'; data: '411311'),
    (a: ' '; b: ' '; c: '99'; data: '113141'),
    (a: ' '; b: ' '; c: '  '; data: '114131'),
    (a: ' '; b: ' '; c: '  '; data: '311141'),
    (a: ' '; b: ' '; c: '  '; data: '411131')
    );
  StartA = '211412';
  StartB = '211214';
  StartC = '211232';
  Stop = '2331112';

  function Find_Code128AB(c: AnsiChar): integer;
  var
    I: integer;
    v: AnsiChar;
  begin
    for I := 0 to High(tabelle_128) do
    begin
      if FTyp = 5 then
        v := tabelle_128[I].a
      else
        v := tabelle_128[I].b;
      if c = v then
      begin
        Result := I;
        exit;
      end;
    end;
    Result := -1;
  end;

  function Find_Code128C(c: AnsiString): integer;
  var
    I: integer;
  begin
    for I := 0 to High(tabelle_128) do
    begin
      if tabelle_128[I].c = c then
      begin
        Result := I;
        exit;
      end;
    end;
    Result := -1;
  end;

var
  I, j, idx: integer;
  startcode: AnsiString;
  checksum: integer;
  codeword_pos: integer;

begin
  case FTyp of
    5, 20:
      begin
        checksum := 103;
        startcode := StartA;
      end;
    6, 21:
      begin
        checksum := 104;
        startcode := StartB;
      end;
    7, 22:
      begin
        checksum := 105;
        startcode := StartC;
      end;
  else
    raise Exception.CreateFmt('%s: wrong BarcodeType in Code_128',
      [self.ClassName]);
  end;
  Result := startcode;
  codeword_pos := 1;
  case FTyp of
    20, 21, 22:
      begin
        Result := Result + tabelle_128[102].data;
        Inc(checksum, 102 * codeword_pos);
        Inc(codeword_pos);
        if FCheckSum then
          FText := DoCheckSumming(FTEXT);
      end;
  end;
  if (FTyp = 7) or (FTyp = 22) then
  begin
    if (Length(FText) mod 2 <> 0) then
      FText := '0' + FText;
    for I := 1 to (Length(FText) div 2) do
    begin
      j := (I - 1) * 2 + 1;
      idx := Find_Code128C(copy(Ftext, j, 2));
      if idx < 0 then
        idx := Find_Code128C('00');
      Result := Result + tabelle_128[idx].data;
      Inc(checksum, idx * codeword_pos);
      Inc(codeword_pos);
    end;
  end
  else
    for I := 1 to Length(FText) do
    begin
      idx := Find_Code128AB(FText[I]);
      if idx < 0 then
        idx := Find_Code128AB(' ');
      Result := Result + tabelle_128[idx].data;
      Inc(checksum, idx * codeword_pos);
      Inc(codeword_pos);
    end;
  checksum := checksum mod 103;
  Result := Result + tabelle_128[checksum].data;
  Result := Result + Stop;
  Result := Convert(Result);
end;

function TVPDFBarcode.Code_93: AnsiString;
type
  TCode93 =
    record
    c: AnsiChar;
    data: array[0..5] of AnsiChar;
  end;

const
  tabelle_93: array[0..46] of TCode93 = (
    (c: '0'; data: '131112'),
    (c: '1'; data: '111213'),
    (c: '2'; data: '111312'),
    (c: '3'; data: '111411'),
    (c: '4'; data: '121113'),
    (c: '5'; data: '121212'),
    (c: '6'; data: '121311'),
    (c: '7'; data: '111114'),
    (c: '8'; data: '131211'),
    (c: '9'; data: '141111'),
    (c: 'A'; data: '211113'),
    (c: 'B'; data: '211212'),
    (c: 'C'; data: '211311'),
    (c: 'D'; data: '221112'),
    (c: 'E'; data: '221211'),
    (c: 'F'; data: '231111'),
    (c: 'G'; data: '112113'),
    (c: 'H'; data: '112212'),
    (c: 'I'; data: '112311'),
    (c: 'J'; data: '122112'),
    (c: 'K'; data: '132111'),
    (c: 'L'; data: '111123'),
    (c: 'M'; data: '111222'),
    (c: 'N'; data: '111321'),
    (c: 'O'; data: '121122'),
    (c: 'P'; data: '131121'),
    (c: 'Q'; data: '212112'),
    (c: 'R'; data: '212211'),
    (c: 'S'; data: '211122'),
    (c: 'T'; data: '211221'),
    (c: 'U'; data: '221121'),
    (c: 'V'; data: '222111'),
    (c: 'W'; data: '112122'),
    (c: 'X'; data: '112221'),
    (c: 'Y'; data: '122121'),
    (c: 'Z'; data: '123111'),
    (c: '-'; data: '121131'),
    (c: '.'; data: '311112'),
    (c: ' '; data: '311211'),
    (c: '$'; data: '321111'),
    (c: '/'; data: '112131'),
    (c: '+'; data: '113121'),
    (c: '%'; data: '211131'),
    (c: '['; data: '121221'),
    (c: ']'; data: '312111'),
    (c: '{'; data: '311121'),
    (c: '}'; data: '122211')
    );

  function Find_Code93(c: AnsiChar): integer;
  var
    I: integer;
  begin
    for I := 0 to High(tabelle_93) do
    begin
      if c = tabelle_93[I].c then
      begin
        Result := I;
        exit;
      end;
    end;
    Result := -1;
  end;
var
  I, idx: integer;
  checkC, checkK, weightC, weightK: integer;
begin
  Result := '111141';
  for I := 1 to Length(FText) do
  begin
    idx := Find_Code93(FText[I]);
    if idx < 0 then
      raise Exception.CreateFmt('%s:Code93 bad Data <%s>', [self.ClassName,
        FText]);
    Result := Result + tabelle_93[idx].data;
  end;
  checkC := 0;
  checkK := 0;
  weightC := 1;
  weightK := 2;
  for I := Length(FText) downto 1 do
  begin
    idx := Find_Code93(FText[I]);
    Inc(checkC, idx * weightC);
    Inc(checkK, idx * weightK);
    Inc(weightC);
    if weightC > 20 then
      weightC := 1;
    Inc(weightK);
    if weightK > 15 then
      weightC := 1;
  end;
  Inc(checkK, checkC);
  checkC := checkC mod 47;
  checkK := checkK mod 47;
  Result := Result + tabelle_93[checkC].data +
    tabelle_93[checkK].data;
  Result := Result + '1111411';
  Result := Convert(Result);
end;

function TVPDFBarcode.Code_93Extended: AnsiString;
const
  code93x: array[0..127] of AnsiString =
  (
    (']U'), ('[A'), ('[B'), ('[C'), ('[D'), ('[E'), ('[F'), ('[G'),
    ('[H'), ('[I'), ('[J'), ('[K'), ('[L'), ('[M'), ('[N'), ('[O'),
    ('[P'), ('[Q'), ('[R'), ('[S'), ('[T'), ('[U'), ('[V'), ('[W'),
    ('[X'), ('[Y'), ('[Z'), (']A'), (']B'), (']C'), (']D'), (']E'),
    (' '), ('{A'), ('{B'), ('{C'), ('{D'), ('{E'), ('{F'), ('{G'),
    ('{H'), ('{I'), ('{J'), ('{K'), ('{L'), ('{M'), ('{N'), ('{O'),
    ('0'), ('1'), ('2'), ('3'), ('4'), ('5'), ('6'), ('7'),
    ('8'), ('9'), ('{Z'), (']F'), (']G'), (']H'), (']I'), (']J'),
    (']V'), ('A'), ('B'), ('C'), ('D'), ('E'), ('F'), ('G'),
    ('H'), ('I'), ('J'), ('K'), ('L'), ('M'), ('N'), ('O'),
    ('P'), ('Q'), ('R'), ('S'), ('T'), ('U'), ('V'), ('W'),
    ('X'), ('Y'), ('Z'), (']K'), (']L'), (']M'), (']N'), (']O'),
    (']W'), ('}A'), ('}B'), ('}C'), ('}D'), ('}E'), ('}F'), ('}G'),
    ('}H'), ('}I'), ('}J'), ('}K'), ('}L'), ('}M'), ('}N'), ('}O'),
    ('}P'), ('}Q'), ('}R'), ('}S'), ('}T'), ('}U'), ('}V'), ('}W'),
    ('}X'), ('}Y'), ('}Z'), (']P'), (']Q'), (']R'), (']S'), (']T')
    );

var
  save: AnsiString;
  I: integer;
begin
  save := FText;
  FText := '';
  for I := 1 to Length(save) do
  begin
    if ord(save[I]) <= 127 then
      FText := FText + code93x[ord(save[I])];
  end;
  Result := Code_93;
  FText := save;
end;

function TVPDFBarcode.Code_MSI: AnsiString;
const
  tabelle_MSI: array['0'..'9'] of AnsiString =
  (
    ('51515151'),
    ('51515160'),
    ('51516051'),
    ('51516060'),
    ('51605151'),
    ('51605160'),
    ('51606051'),
    ('51606060'),
    ('60515151'),
    ('60515160')
    );
var
  I: integer;
  check_even, check_odd, checksum: integer;
begin
  Result := '60';
  check_even := 0;
  check_odd := 0;
  for I := 1 to Length(FText) do
  begin
    if odd(I - 1) then
      check_odd := check_odd * 10 + ord(FText[I])
    else
      check_even := check_even + ord(FText[I]);

    Result := Result + tabelle_MSI[FText[I]];
  end;
  checksum := quersumme(check_odd * 2) + check_even;
  checksum := checksum mod 10;
  if checksum > 0 then
    checksum := 10 - checksum;
  Result := Result + tabelle_MSI[chr(ord('0') + checksum)];
  Result := Result + '515';
end;

function TVPDFBarcode.Code_PostNet: AnsiString;
const
  tabelle_PostNet: array['0'..'9'] of AnsiString =
  (
    ('5151A1A1A1'),
    ('A1A1A15151'),
    ('A1A151A151'),
    ('A1A15151A1'),
    ('A151A1A151'),
    ('A151A151A1'),
    ('A15151A1A1'),
    ('51A1A1A151'),
    ('51A1A151A1'),
    ('51A151A1A1')
    );
var
  I: integer;
begin
  Result := '51';
  for I := 1 to Length(FText) do
  begin
    Result := Result + tabelle_PostNet[FText[I]];
  end;
  Result := Result + '5';
end;

function TVPDFBarcode.Code_Codabar: AnsiString;
type
  TCodabar =
    record
    c: AnsiChar;
    data: array[0..6] of AnsiChar;
  end;

const
  tabelle_cb: array[0..19] of TCodabar = (
    (c: '1'; data: '5050615'),
    (c: '2'; data: '5051506'),
    (c: '3'; data: '6150505'),
    (c: '4'; data: '5060515'),
    (c: '5'; data: '6050515'),
    (c: '6'; data: '5150506'),
    (c: '7'; data: '5150605'),
    (c: '8'; data: '5160505'),
    (c: '9'; data: '6051505'),
    (c: '0'; data: '5050516'),
    (c: '-'; data: '5051605'),
    (c: '$'; data: '5061505'),
    (c: ':'; data: '6050606'),
    (c: '/'; data: '6060506'),
    (c: '.'; data: '6060605'),
    (c: '+'; data: '5060606'),
    (c: 'A'; data: '5061515'),
    (c: 'B'; data: '5151506'),
    (c: 'C'; data: '5051516'),
    (c: 'D'; data: '5051615')
    );

  function Find_Codabar(c: AnsiChar): integer;
  var
    I: integer;
  begin
    for I := 0 to High(tabelle_cb) do
    begin
      if c = tabelle_cb[I].c then
      begin
        Result := I;
        exit;
      end;
    end;
    Result := -1;
  end;

var
  I, idx: integer;
begin
  Result := tabelle_cb[Find_Codabar('A')].data + '0';
  for I := 1 to Length(FText) do
  begin
    idx := Find_Codabar(FText[I]);
    Result := Result + tabelle_cb[idx].data + '0';
  end;
  Result := Result + tabelle_cb[Find_Codabar('B')].data;
end;

function TVPDFBarcode.SetLen(pI: byte): AnsiString;
begin
  Result := StringOfChar(AnsiChar('0'), pI - Length(FText)) + FText;
end;

function TVPDFBarcode.Code_UPC_A: AnsiString;
var
  I: integer;
  tmp: AnsiString;
begin
  FText := SetLen(12);
  if FCheckSum then
    tmp := DoCheckSumming(copy(FText, 1, 11));
  if FCheckSum then
    FText := tmp
  else
    tmp := FText;
  Result := '505';
  for I := 1 to 6 do
    Result := Result + tabelle_EAN_A[tmp[I]];
  Result := Result + '05050';
  for I := 7 to 12 do
    Result := Result + tabelle_EAN_C[tmp[I]];
  Result := Result + '505';
end;

const
  tabelle_UPC_E0: array['0'..'9', 1..6] of AnsiChar =
  (
    ('E', 'E', 'E', 'o', 'o', 'o'),
    ('E', 'E', 'o', 'E', 'o', 'o'),
    ('E', 'E', 'o', 'o', 'E', 'o'),
    ('E', 'E', 'o', 'o', 'o', 'E'),
    ('E', 'o', 'E', 'E', 'o', 'o'),
    ('E', 'o', 'o', 'E', 'E', 'o'),
    ('E', 'o', 'o', 'o', 'E', 'E'),
    ('E', 'o', 'E', 'o', 'E', 'o'),
    ('E', 'o', 'E', 'o', 'o', 'E'),
    ('E', 'o', 'o', 'E', 'o', 'E')
    );

function TVPDFBarcode.Code_UPC_E0: AnsiString;
var
  I, j: integer;
  tmp: AnsiString;
  c: AnsiChar;
begin
  FText := SetLen(7);
  tmp := DoCheckSumming(copy(FText, 1, 6));
  c := tmp[7];
  if FCheckSum then
    FText := tmp
  else
    tmp := FText;
  Result := '505';
  for I := 1 to 6 do
  begin
    if tabelle_UPC_E0[c, I] = 'E' then
    begin
      for j := 1 to 4 do
        Result := Result + tabelle_EAN_C[tmp[I], 5 - j];
    end
    else
    begin
      Result := Result + tabelle_EAN_A[tmp[I]];
    end;
  end;
  Result := Result + '050505';
end;

function TVPDFBarcode.Code_UPC_E1: AnsiString;
var
  I, j: integer;
  tmp: AnsiString;
  c: AnsiChar;
begin
  FText := SetLen(7);
  tmp := DoCheckSumming(copy(FText, 1, 6));
  c := tmp[7];
  if FCheckSum then
    FText := tmp
  else
    tmp := FText;
  Result := '505';
  for I := 1 to 6 do
  begin
    if tabelle_UPC_E0[c, I] = 'E' then
    begin
      Result := Result + tabelle_EAN_A[tmp[I]];
    end
    else
    begin
      for j := 1 to 4 do
        Result := Result + tabelle_EAN_C[tmp[I], 5 - j];
    end;
  end;
  Result := Result + '050505';
end;

function getSupp(Nr: AnsiString): AnsiString;
var
  I, Fak, Sum: Integer;
  tmp: AnsiString;
begin
  Sum := 0;
  tmp := copy(nr, 1, Length(Nr) - 1);
  Fak := Length(tmp);
  for I := 1 to length(tmp) do
  begin
    if (Fak mod 2) = 0 then
      Sum := Sum + (StrToInt(String(tmp[I])) * 9)
    else
      Sum := Sum + (StrToInt(String(tmp[I])) * 3);
    dec(Fak);
  end;
  Sum := ((Sum mod 10) mod 10) mod 10;
  Result := tmp + AnsiString(IntToStr(Sum));
end;

function TVPDFBarcode.Code_Supp5: AnsiString;
var
  I, j: integer;
  tmp: AnsiString;
  c: AnsiChar;
begin
  FText := SetLen(5);
  tmp := getSupp(copy(FText, 1, 5) + '0');
  c := tmp[6];
  if FCheckSum then
    FText := tmp
  else
    tmp := FText;
  Result := '506';
  for I := 1 to 5 do
  begin
    if tabelle_UPC_E0[c, (6 - 5) + I] = 'E' then
    begin
      for j := 1 to 4 do
        Result := Result + tabelle_EAN_C[tmp[I], 5 - j];
    end
    else
    begin
      Result := Result + tabelle_EAN_A[tmp[I]];
    end;
    if I < 5 then
      Result := Result + '05';
  end;
end;

function TVPDFBarcode.Code_Supp2: AnsiString;
var
  I, j: integer;
  tmp, mS: AnsiString;
begin
  FText := SetLen(2);
  I := StrToInt(String(Ftext));
  case I mod 4 of
    3: mS := 'EE';
    2: mS := 'Eo';
    1: mS := 'oE';
    0: mS := 'oo';
  end;
  tmp := getSupp(copy(FText, 1, 5) + '0');
  if FCheckSum then
    FText := tmp
  else
    tmp := FText;
  Result := '506';
  for I := 1 to 2 do
  begin
    if mS[I] = 'E' then
    begin
      for j := 1 to 4 do
        Result := Result + tabelle_EAN_C[tmp[I], 5 - j];
    end
    else
    begin
      Result := Result + tabelle_EAN_A[tmp[I]];
    end;
    if I < 2 then
      Result := Result + '05';
  end;
end;

procedure TVPDFBarcode.MakeModules;
begin
  case Typ of
    0, 1, 3, 13, 14, 4, 12, 15, 16, 17, 18, 19:
      begin
        if Ratio < 2.0 then
          Ratio := 2.0;
        if Ratio > 3.0 then
          Ratio := 3.0;
      end;
    2:
      begin
        if Ratio < 2.25 then
          Ratio := 2.25;
        if Ratio > 3.0 then
          Ratio := 3.0;
      end;
    5, 6, 7, 8, 9, 10, 11: ;
  end;
  Modules[0] := FModul;
  Modules[1] := Round(FModul * FRatio);
  Modules[2] := Modules[1] * 3 div 2;
  Modules[3] := Modules[1] * 2;
end;

procedure TVPDFBarcode.DoLines(data: AnsiString; Canvas: TCanvas);
var
  I: integer;
  lt: TVPDFBarLineType;
  Xadd: integer;
  Width, height: integer;
  a, b, c, d, Orgin: TPoint;
  Alpha: double;
begin
  Xadd := 0;
  Orgin.x := FLeft;
  Orgin.y := FTop;
  Alpha := FAngle / 180.0 * pi;
  Orgin := TranslateQuad2D(Alpha, Orgin, Point(Self.Width, Self.Height));
  with Canvas do
  begin
    Pen.Width := 1;
    for I := 1 to Length(data) do
    begin
      OneBarProps(data[I], Width, lt);
      if (lt = black) or (lt = black_half) then
      begin
        Pen.Color := FColorBar;
      end
      else
      begin
        Pen.Color := FColor;
      end;
      Brush.Color := Pen.Color;
      if lt = black_half then
        height := FHeight * 2 div 5
      else
        height := FHeight;
      a.x := Xadd;
      a.y := 0;
      b.x := Xadd;
      b.y := height;
      c.x := Xadd + Width - 1;
      c.y := Height;
      d.x := Xadd + Width - 1;
      d.y := 0;
      a := Translate2D(Rotate2D(a, Alpha), Orgin);
      b := Translate2D(Rotate2D(b, Alpha), Orgin);
      c := Translate2D(Rotate2D(c, Alpha), Orgin);
      d := Translate2D(Rotate2D(d, Alpha), Orgin);
      Polygon([a, b, c, d]);
      Xadd := Xadd + Width;
    end;
  end;
end;

procedure TVPDFBarcode.DrawBarcode(Canvas: TCanvas);
var
  data: AnsiString;
  SaveFont: TFont;
  SavePen: TPen;
  SaveBrush: TBrush;
begin
  Savefont := TFont.Create;
  SavePen := TPen.Create;
  SaveBrush := TBrush.Create;
  data := MakeData;
  try
    Savefont.Assign(Canvas.Font);
    SavePen.Assign(Canvas.Pen);
    SaveBrush.Assign(Canvas.Brush);
    DoLines(data, Canvas);
    if FShowText <> bcoNone then
      DrawText(Canvas);
    Canvas.Font.Assign(savefont);
    Canvas.Pen.Assign(SavePen);
    Canvas.Brush.Assign(SaveBrush);
  finally
    Savefont.Free;
    SavePen.Free;
    SaveBrush.Free;
  end;
end;

procedure TVPDFBarcode.DrawText(Canvas: TCanvas);
var
  PosX, PosY: Integer;
  SaveFont: TFont;
  SColor: TColor;
begin
  with Canvas do
  begin
    SaveFont := TFont.Create;
    try
      Font.Color := FColorBar;
      Font.Assign(ShowTextFont);
      try
        Pen.Color := Font.Color;
        Brush.Color := clWhite;
        PosX := FLeft;
        PosY := FTop;
        if ShowTextPosition in [stpTopLeft, stpBottomLeft] then
          PosX := FLeft
        else
          if ShowTextPosition in [stpTopRight, stpBottomRight] then
            PosX := FLeft + Width - TextWidth(String(Text))
          else
            if ShowTextPosition in [stpTopCenter, stpBottomCenter] then
              PosX := FLeft + Trunc((Width - TextWidth(String(Text))) / 2);
        if ShowTextPosition in [stpTopLeft, stpTopCenter, stpTopRight] then
          PosY := FTop
        else
          if ShowTextPosition in [stpBottomLeft, stpBottomCenter, stpBottomRight] then
            PosY := FTop + Height - TextHeight(String(Text));
        if FShowText in [bcoCode, bcoBoth] then
        begin
          Pen.Color := FColor;
          SColor := Brush.Color;
          Brush.Color := FColor;
          Rectangle(PosX, PosY, PosX + TextWidth(String(Text)) + 2, PosY + TextHeight(String(Text)) { + 3});
          Brush.Color := SColor;
          Rectangle(FLeft - 1, FTop, FLeft + Width, FTop - 2);
          Rectangle(FLeft - 1, FTop + Height, FLeft + Width, FTop + Height + 2);
          Pen.Color := FColorBar;
          Brush.Color := FColorBar;
          Font.Color := FColorBar;
          TextOut(PosX, PosY, String(FText));
        end;
        if FShowText in [bcoTyp, bcoBoth] then
        begin
          TextOut(FLeft, FTop + Round(Font.Height * 2.5), String(GetTypText));
        end;
      finally
        Font.Assign(SaveFont);
      end;
    finally
      SaveFont.Free;
    end;
  end;
end;

procedure TVPDFBarcode.DoChange;
begin
  if Assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TVPDFBarcode.SetRatio(const Value: Double);
begin
  if Value <> FRatio then
  begin
    FRatio := Value;
    DoChange;
  end;
end;

procedure TVPDFBarcode.SetTyp(const Value: Integer);
begin
  if Value <> FTyp then
  begin
    FTyp := Value;
    DoChange;
  end;
end;

procedure TVPDFBarcode.SetAngle(const Value: Double);
begin
  if Value <> FAngle then
  begin
    FAngle := Value;
    DoChange;
  end;
end;

procedure TVPDFBarcode.SetText(const Value: AnsiString);
begin
  if Value <> FText then
  begin
    FText := Value;
    DoChange;
  end;
end;

procedure TVPDFBarcode.SetShowText(const Value: TVPDFBarcodeOption);
begin
  if Value <> FShowText then
  begin
    FShowText := Value;
    DoChange;
  end;
end;

procedure TVPDFBarcode.SetTop(const Value: Integer);
begin
  if Value <> FTop then
  begin
    FTop := Value;
    DoChange;
  end;
end;

procedure TVPDFBarcode.SetLeft(const Value: Integer);
begin
  if Value <> FLeft then
  begin
    FLeft := Value;
    DoChange;
  end;
end;

procedure TVPDFBarcode.SetCheckSum(const Value: Boolean);
begin
  if Value <> FCheckSum then
  begin
    FCheckSum := Value;
    DoChange;
  end;
end;

procedure TVPDFBarcode.SetHeight(const Value: integer);
begin
  if Value <> FHeight then
  begin
    FHeight := Value;
    DoChange;
  end;
end;

function TVPDFBarcode.GetCanvasHeight: Integer;
var
  Alpha: Extended;
begin
  Alpha := FAngle / 180.0 * pi;
  Result := Round(Abs(Sin(Alpha)) * Self.Width + Abs(Cos(Alpha)) * Self.Height +
    0.5);
end;

function TVPDFBarcode.GetCanvasWidth: Integer;
var
  Alpha: Extended;
begin
  Alpha := FAngle / 180.0 * pi;
  Result := Round(Abs(Cos(Alpha)) * Self.Width + Abs(Sin(Alpha)) * Self.Height +
    0.5);
end;

procedure TVPDFBarcode.SetShowTextFont(const Value: TFont);
begin
  FShowTextFont.Assign(Value);
  DoChange;
end;

procedure TVPDFBarcode.SetShowTextPosition(const Value: TVPDFShowTextPosition);
begin
  if Value <> FShowTextPosition then
  begin
    FShowTextPosition := Value;
    DoChange;
  end;
end;

function TVPDFBarcode.CheckSumModulo10(const Data: AnsiString): AnsiString;
var
  I, Fak, Sum: Integer;
begin
  Sum := 0;
  Fak := Length(Data);
  for I := 1 to Length(Data) do
  begin
    if (Fak mod 2) = 0 then
      Sum := Sum + (StrToInt(String(Data[i])) * 1)
    else
      Sum := Sum + (StrToInt(String(Data[i])) * 3);
    Dec(Fak);
  end;
  if (Sum mod 10) = 0 then
    Result := Data + '0'
  else
    Result := Data + AnsiString(IntToStr(10 - (Sum mod 10)));
end;

end.
