{*******************************************************}
{                                                       }
{       This unit is part of the VISPDF VCL library.    }
{       Written by R.Husske - ALL RIGHTS RESERVED.      }
{                                                       }
{       Copyright (C) 2000-2009, www.vispdf.com         }
{                                                       }
{       e-mail: support@vispdf.com                      }
{       http://www.vispdf.com                           }
{                                                       }
{*******************************************************}

unit VPDFFonts;

interface

uses
  Windows, SysUtils, Classes, Graphics, Math, VPDFFontRecords;

{$I VisPDFLib.inc }

type
  CDescript = record
    Index: Word;
    Width: LongWord;
  end;

  TCDescript = array of CDescript;

  PCDescript = ^TCDescript;

  TTrueTypeTables = class
  private
    MaxGlyf: Integer;
    Links: TLinkArray;
    FCharSet: Integer;
    CapsuleCount: VISPDFULONG;
    BreaksPresent: Boolean;
    GlyfRoot: PGlyfRecord;
    IsMonoSpaced: Boolean;
    FFontName: TFontname;
    FFontStyle: TFontStyles;
    CmapHeader: CMAPTableHeader;
    CmapTable: CmapTableType;
    CMAPTable4: CmapFormat4;
    CVTTable: LinearTable;
    FPGMTable: LinearTable;
    LocaTable: LinearTable;
    PrepTable: LinearTable;
    HmtxTable: WordLinearTable;
    HeadTable: HeadTableType;
    HheaTable: HheaTableType;
    MaxpTable: MaxpTableType;
    LongCmap: array of byte;
    HandleCharsKit: array of TGlyfCharsKit;
    GlyfStack: array[0..255] of Pointer;
    SymbolSelection: array of GlyphCodes;
  protected
    procedure CompressGlyfTable(FullEscort: boolean);
    procedure GetSourceTables(Extension: boolean);
    procedure LoadCMAPTable(HDescript: HDC; Extension: boolean);
    procedure LoadPostTable(HDescript: HDC);
    procedure LoadGlyphTable(HDescript: HDC);
    procedure CreateFontTable(Name: LongWord; var Table: TVPDFFontTable);
    procedure MoveRange(var SegmentMapping: array of DeltaValues; ItemIndex:
      Integer);
    procedure LoadStructTable(HDescript: HDC; TableName: Cardinal; EmbTable:
      Pointer);
    procedure LoadWordTable(HDescript: HDC; TableName: Cardinal; var
      EmbTable:
      WordLinearTable);
    procedure LoadLinearTable(HDescript: HDC; TableName: Cardinal; var
      EmbTable:
      LinearTable);
    procedure ExtractFontCodes(Buffer: PWord; BufferLength: Word);
    function MarkUsedGlyf(GlyfCode: VISPDFUSHORT): word;
    procedure GetGLYF(TempStream: TStream);
    procedure GetHMTX(TempStream: TStream);
    procedure GetLOCA(TempStream: TStream);
    procedure GetFullCMAP(TempStream: TStream);
    procedure MergeCompositLinks(GlyfLink: PGlyfRecord);
    procedure EditMonopictedLinks(GlyfLink: PGlyfRecord; SLinks:
      TLinkArray);
    function DoubleSwap(L: LongWord): LongWord;
    function GetGlyfCode(GyfNumber: Word): Word;
    function Checksum(TableBuffer: PVISPDFULONG; Length: VISPDFULONG): VISPDFULONG;
    function ConvertToUnicode(ID: Word): Word;
    function GetCodepage(Charset: Byte): Cardinal;
    procedure SaveWideCharFont(Stream: TStream);
    procedure FreeTempTables;
    procedure QSort(var Arr: array of GlyphCodes; BegIndex, EndIndex:
      Integer;
      FromFirstItem: boolean);
  public
    function ConvertFromUnicode(ID: Word): Word;
    procedure CharacterDescription(CDArray: PCDescript; FontName: TFontname;
      FontStyle: TFontStyles);
    procedure GetFontEnsemble(FontName: TFontname; FontStyle: TFontStyles;
      OutStream: Tstream; var CharsetBuffer: array of word; CharBufferLen:
      Word);
    procedure GetFontLongEnsemble(FontName: TFontname; FontStyle:
      TFontStyles;
      OutStream: Tstream; var CharsetBuffer: array of word; CharBufferLen:
      Word);
    property CharSet: Integer read FCharSet write FCharSet;
  end;

implementation

procedure TTrueTypeTables.LoadCMAPTable(HDescript: HDC; Extension: boolean);
var
  I: Integer;
  PCMap: Pointer;
  DoubleArrLen, ArrayLen, SegCount: Integer;
  CmapName: Cardinal;
  TableSize: Cardinal;
  TabOffset: Integer;
  sTablesCount: Integer;
  CmapStream: TMemoryStream;
  sPlatform, sEncoding: VISPDFUSHORT;
begin
  CmapName := DoubleSwap(1668112752);
  CmapStream := TMemoryStream.Create;
  try
    TabOffset := 0;
    TableSize := GetFontData(HDescript, CmapName, 0, nil, 0);
    CmapStream.SetSize(TableSize);
    if GetFontData(HDescript, CmapName, 0, CmapStream.Memory, TableSize) =
      GDI_ERROR then raise Exception.Create(CLCT);
    if Extension then
    begin
      SetLength(LongCmap, TableSize);
      PCMap := @LongCmap[0];
      CmapStream.Read(PCMap^, TableSize);
      CmapStream.Position := 0;
    end;
    CmapStream.Read(CmapHeader, SizeOf(CmapHeader));
    sTablesCount := Swap(CmapHeader.NumTables);
    for I := 1 to sTablesCount do
    begin
      CmapStream.Read(CmapTable, sizeof(CmapTable));
      sPlatform := Swap(CmapTable.PlatformID);
      sEncoding := Swap(CmapTable.EncodingID);
      if (sPlatform = 3) and (sEncoding = 0) then
        TabOffset := DoubleSwap(CmapTable.TableOffset);
      if (sPlatform = 3) and (sEncoding = 1) then
      begin
        TabOffset := DoubleSwap(CmapTable.TableOffset);
        Break;
      end;
    end;
    if CmapStream.Seek(TabOffset, soFromBeginning) <> (TabOffset) then
      raise exception.Create(CLCT);
    CmapStream.Read(CMAPTable4, 14);
    SegCount := Swap(CMAPTable4.segCountX2) shr 1;
    ArrayLen := SegCount;
    SetLength(CMAPTable4.EndCount, ArrayLen);
    SetLength(CMAPTable4.StartCount, ArrayLen);
    SetLength(CMAPTable4.IDDelta, ArrayLen);
    SetLength(CMAPTable4.IDRangeOffset, ArrayLen);
    DoubleArrLen := ArrayLen * 2;
    CmapStream.Read(CMAPTable4.EndCount[0], DoubleArrLen);
    for I := 0 to ArrayLen - 1 do
      CMAPTable4.EndCount[i] := Swap(CMAPTable4.EndCount[i]);
    CmapStream.Read(CMAPTable4.ReservedPad, 2);
    CmapStream.Read(CMAPTable4.StartCount[0], DoubleArrLen);
    for I := 0 to ArrayLen - 1 do
      CMAPTable4.StartCount[i] := Swap(CMAPTable4.StartCount[i]);
    CmapStream.Read(CMAPTable4.IDDelta[0], DoubleArrLen);
    for I := 0 to ArrayLen - 1 do
      CMAPTable4.IDDelta[i] := Swap(CMAPTable4.IDDelta[i]);
    CmapStream.Read(CMAPTable4.IDRangeOffset[0], DoubleArrLen);
    for I := 0 to ArrayLen - 1 do
      CMAPTable4.IDRangeOffset[i] := Swap(CMAPTable4.IDRangeOffset[i]);
    ArrayLen := ((Swap(CMAPTable4.length) shr 1) - 8 - 4 * ArrayLen);
    SetLength(CMAPTable4.GlyphIdArray, ArrayLen + 1);
    CmapStream.Read(CMAPTable4.GlyphIdArray[0], (ArrayLen + 1) * 2);
    for I := 0 to ArrayLen do
      CMAPTable4.GlyphIdArray[i] := Swap(CMAPTable4.GlyphIdArray[i]);
  finally
    CmapStream.Free;
  end;
end;

procedure TTrueTypeTables.LoadPostTable(HDescript: HDC);
var
  ConvName: Cardinal;
  TableSize: Cardinal;
  FixPitch: LongInt;
  TblStream: TMemoryStream;
begin
  ConvName := DoubleSwap(1886352244);
  TblStream := TMemoryStream.Create;
  try
    TableSize := GetFontData(HDescript, ConvName, 0, nil, 0);
    TblStream.SetSize(TableSize);
    if GetFontData(HDescript, ConvName, 0, TblStream.Memory, TableSize) =
      GDI_ERROR then raise Exception.Create(CLPT);
    TblStream.Position := TblStream.Position + 12;
    TblStream.Read(FixPitch, 4);
    IsMonoSpaced := (FixPitch <> 0);
  finally
    TblStream.Free;
  end;
end;

procedure TTrueTypeTables.LoadLinearTable(HDescript: HDC; TableName: Cardinal;
  var EmbTable: LinearTable);
var
  ConvName: Cardinal;
  TableSize: Cardinal;
  TblStream: TMemoryStream;
begin
  ConvName := DoubleSwap(TableName);
  TblStream := TMemoryStream.Create;
  try
    TableSize := GetFontData(HDescript, ConvName, 0, nil, 0);
    TblStream.SetSize(TableSize);
    if GetFontData(HDescript, ConvName, 0, TblStream.Memory, TableSize) =
      GDI_ERROR then EmbTable.IsPresent := false
    else
    begin
      EmbTable.IsPresent := true;
      EmbTable.TableLength := TableSize;
      SetLength(EmbTable.Item, TableSize);
      TblStream.Read(EmbTable.Item[0], TableSize);
    end;
  finally
    TblStream.Free;
  end;
end;

procedure TTrueTypeTables.LoadStructTable(HDescript: HDC; TableName: Cardinal;
  EmbTable: Pointer);
var
  ConvName: Cardinal;
  TableSize: Cardinal;
  TblStream: TMemoryStream;
begin
  ConvName := DoubleSwap(TableName);
  TblStream := TMemoryStream.Create;
  try
    TableSize := GetFontData(HDescript, ConvName, 0, nil, 0);
    TblStream.SetSize(TableSize);
    if GetFontData(HDescript, ConvName, 0, TblStream.Memory, TableSize) =
      GDI_ERROR then raise Exception.Create(CLRT);
    TblStream.Read(EmbTable^, TableSize);
  finally
    TblStream.Free;
  end;
end;

procedure TTrueTypeTables.LoadWordTable(HDescript: HDC; TableName: Cardinal; var
  EmbTable: WordLinearTable);
var
  ConvName: Cardinal;
  TableSize: Cardinal;
  TblStream: TMemoryStream;
begin
  ConvName := DoubleSwap(TableName);
  TblStream := TMemoryStream.Create;
  try
    TableSize := GetFontData(HDescript, ConvName, 0, nil, 0);
    TblStream.SetSize(TableSize);
    if GetFontData(HDescript, ConvName, 0, TblStream.Memory, TableSize) =
      GDI_ERROR then EmbTable.IsPresent := false
    else
    begin
      EmbTable.IsPresent := true;
      EmbTable.TableLength := TableSize;
      SetLength(EmbTable.Item, Trunc(TableSize / 2 + 0.5));
      TblStream.Read(EmbTable.Item[0], TableSize);
    end;
  finally
    TblStream.Free;
  end;
end;

procedure TTrueTypeTables.LoadGlyphTable(HDescript: HDC);
var
  I: Integer;
  LocaIndex: SmallInt;
  GlyphCount: Integer;
  ContoursSize: longint;
  XLongVector: VISPDFULONG;
  XShortVector: VISPDFUSHORT;
  ConvName: Cardinal;
  TableSize: Cardinal;
  TblStream: TMemoryStream;
  GlyfPointer, GlyfCapsule: PGlyfRecord;
begin
  ConvName := DoubleSwap(1735162214);
  TblStream := TMemoryStream.Create;
  try
    TableSize := GetFontData(HDescript, ConvName, 0, nil, 0);
    TblStream.SetSize(TableSize);
    if GetFontData(HDescript, ConvName, 0, TblStream.Memory, TableSize) =
      GDI_ERROR then
      raise Exception.Create(CLGT);
    New(GlyfRoot);
    TblStream.Position := 0;
    GlyfPointer := GlyfRoot;
    GlyphCount := MaxpTable.numGlyphs;
    for I := 1 to GlyphCount do
    begin
      with LocaTable do
      begin
        LocaIndex := Swap(HeadTable.IndexToLocFormat);
        if LocaIndex = 1 then
        begin
          XLongVector := Item[I * 4] shl 24 + Item[I * 4 + 1] shl 16 +
            Item[I * 4
            + 2] shl 8 + Item[I * 4 + 3];
          ContoursSize := XLongVector - Item[(I - 1) * 4] shl 24 -
            Item[(I - 1)
            * 4 + 1] shl 16 - Item[(I - 1) * 4 + 2] shl 8 - Item[(I
            - 1) * 4 +
            3];
        end
        else
          ContoursSize := 2 * (Item[I * 2] shl 8 + Item[I * 2 + 1] -
            Item[(I - 1)
            * 2] shl 8 - Item[(I - 1) * 2 + 1]);
        if ContoursSize <> 0 then
          GetMem(GlyfPointer.Curves, ContoursSize)
        else GlyfPointer.Curves := nil;
        GlyfPointer.NumberOfContours := I;
        GlyfPointer.RecordSize := ContoursSize;
        GlyfPointer.IsTagged := false;
        if LocaIndex = 1 then
        begin
          XLongVector := Item[(I - 1) * 4] shl 24 + Item[(I - 1) * 4 +
            1] shl 16
            + Item[(I - 1) * 4 + 2] shl 8 + Item[(I - 1) * 4 + 3];
          TblStream.Seek(XLongVector, soFromBeginning);
        end
        else
        begin
          XShortVector := Item[(I - 1) * 2] shl 8 + Item[(I - 1) * 2 +
            1];
          TblStream.Seek(2 * XShortVector, soFromBeginning);
        end;
        TblStream.Read(GlyfPointer.Curves^, ContoursSize);
        if I <> GlyphCount then
        begin
          New(GlyfCapsule);
          GlyfCapsule.NextGlyph := nil;
          GlyfCapsule.Curves := nil;
          GlyfPointer.Hidden := true;
          GlyfPointer.NextGlyph := GlyfCapsule;
          GlyfPointer := GlyfCapsule;
        end
        else GlyfPointer := nil;
      end;
    end;
  finally
    TblStream.Free;
  end;
end;

procedure TTrueTypeTables.GetSourceTables(Extension: boolean);
var
  HDescript: HDC;
  LFont: TLogFont;
  HandleObj: THandle;
  TxtMetrics: TTextMetric;
begin
  HDescript := CreateCompatibleDC(0);
  try
    FillChar(LFont, SizeOf(LFont), 0);
    LFont.lfWidth := 0;
    LFont.lfHeight := -10;
    LFont.lfEscapement := 0;
    LFont.lfOrientation := 0;
    if fsBold in FFontStyle then LFont.lfWeight := FW_BOLD
    else LFont.lfWeight := FW_NORMAL;
    LFont.lfCharSet := DEFAULT_CHARSET;
    LFont.lfItalic := Byte(fsItalic in FFontStyle);
    LFont.lfUnderline := Byte(fsUnderline in FFontStyle);
    LFont.lfStrikeOut := Byte(fsStrikeOut in FFontStyle);
    StrPCopy(LFont.lfFaceName, FFontName);
    LFont.lfQuality := DEFAULT_QUALITY;
    LFont.lfOutPrecision := OUT_DEFAULT_PRECIS;
    LFont.lfClipPrecision := CLIP_DEFAULT_PRECIS;
    LFont.lfPitchAndFamily := DEFAULT_PITCH;
    HandleObj := CreateFontIndirect(LFont);
    try
      SelectObject(HDescript, HandleObj);
      GetTextMetrics(HDescript, TxtMetrics);
      LoadCMAPTable(HDescript, Extension);
      LoadPostTable(HDescript);
      LoadWordTable(HDescript, 1752003704, HmtxTable);
      LoadLinearTable(HDescript, 1668707360, CVTTable);
      LoadLinearTable(HDescript, 1718642541, FPGMTable);
      LoadLinearTable(HDescript, 1819239265, LocaTable);
      LoadStructTable(HDescript, 1751474532, @HeadTable);
      LoadStructTable(HDescript, 1751672161, @HheaTable);
      LoadStructTable(HDescript, 1835104368, @MaxpTable);
      MaxpTable.NumGlyphs := Swap(MaxpTable.NumGlyphs);
      LoadLinearTable(HDescript, 1886545264, PrepTable);
      LoadGlyphTable(HDescript);
    finally
      DeleteObject(HandleObj);
    end;
  finally
    DeleteDC(HDescript);
  end;
end;

procedure TTrueTypeTables.ExtractFontCodes(Buffer: PWord; BufferLength: Word);
var
  I, H: Integer;
  CountOfSegments: VISPDFUSHORT;
  CharsCount: Integer;
begin
  CharsCount := 0;
  CountOfSegments := Trunc(Swap(CMAPTable4.SegCountX2) / 2);
  for I := 1 to BufferLength do
  begin
    for H := 0 to CountOfSegments - 1 do
      if (Buffer^ <= CMAPTable4.EndCount[H]) and (Buffer^ >=
        CMAPTable4.startCount[H]) then
      begin
        if (Buffer^ <> 152) and (Buffer^ <> 160) then
        begin
          Inc(CharsCount);
          SetLength(HandleCharsKit, CharsCount);
          HandleCharsKit[CharsCount - 1].Code := Buffer^;
          Break;
        end;
      end;
    Inc(Buffer);
  end;
  for I := 0 to (CharsCount - 1) do
    if (HandleCharsKit[I].Code > 32) then
      HandleCharsKit[I].NCount := MarkUsedGlyf(HandleCharsKit[I].Code);
end;

function TTrueTypeTables.MarkUsedGlyf(GlyfCode: VISPDFUSHORT): word;
var
  I: Integer;
  GlyfIdOffset: LongInt;
  CountOfSegments: VISPDFUSHORT;
  GlyfCountour: VISPDFUSHORT;
  CurrentGlyf: PGlyfRecord;
begin
  GlyfCountour := 0;
  if (GlyfCode = 160) then BreaksPresent := true;
  with CMAPTable4 do
  begin
    CountOfSegments := Swap(SegCountX2) shr 1;
    for I := 0 to CountOfSegments - 1 do
      if (not (GlyfCode < StartCount[I])) and (not (GlyfCode >
        EndCount[I]))
        then
      begin
        if IDRangeOffset[I] = 0 then
        begin
          GlyfCountour := GlyfCode + IDDelta[i];
          Break;
        end
        else
        begin
          GlyfIdOffset := I + IDRangeOffset[I] shr 1 - CountOfSegments
            + GlyfCode
            - StartCount[I];
          GlyfCountour := GlyphIdArray[GlyfIdOffset] + IDDelta[I];
          Break;
        end;
      end
      else GlyfCountour := 0;
  end;
  CurrentGlyf := GlyfRoot;
  Result := GlyfCountour;
  for I := 1 to MaxpTable.numGlyphs do
  begin
    if (I = GlyfCountour + 1) then
    begin
      GlyfStack[ConvertFromUnicode(GlyfCode)] := CurrentGlyf;
      if GlyfCode > MaxGlyf then MaxGlyf := GlyfCode;
      CurrentGlyf.IsTagged := true;
      CurrentGlyf.Hidden := false;
      break;
    end;
    CurrentGlyf := CurrentGlyf.NextGlyph;
  end;
end;

procedure TTrueTypeTables.MergeCompositLinks(GlyfLink: PGlyfRecord);
var
  I: integer;
  CurveKeyline: Integer;
  CurveIndex: Integer;
  StoreAllGlyfs: Boolean;
  GlyfCover: PGlyfRecord;
begin
  CurveIndex := 11;
  StoreAllGlyfs := False;
  GlyfCover := GlyfRoot;
  CurveKeyline := (byte(GlyfLink.Curves[CurveIndex + 1]) shl 8) +
    byte(GlyfLink.Curves[CurveIndex + 2]);
  for I := 1 to CurveKeyline do
    GlyfCover := GlyfCover.NextGlyph;
  GlyfCover.IsTagged := True;
  if (byte(GlyfCover.Curves[0]) = 255) and (byte(GlyfCover.Curves[1]) = 255)
    then
    MergeCompositLinks(GlyfCover);
  repeat
    if (byte(GlyfLink.Curves[CurveIndex]) and 33) = 33 then
      CurveIndex := CurveIndex + 8
    else if (byte(GlyfLink.Curves[CurveIndex]) and 32) = 32 then
      CurveIndex := CurveIndex + 6
    else StoreAllGlyfs := True;
    CurveKeyline := (byte(GlyfLink.Curves[CurveIndex + 1]) shl 8) +
      byte(GlyfLink.Curves[CurveIndex + 2]);
    GlyfCover := GlyfRoot;
    for I := 1 to CurveKeyline do
      GlyfCover := GlyfCover.NextGlyph;
    GlyfCover.IsTagged := True;
    if (byte(GlyfCover.Curves[0]) = 255) and (byte(GlyfCover.Curves[1]) =
      255)
      then
      MergeCompositLinks(GlyfCover);
  until StoreAllGlyfs = True;
end;

procedure TTrueTypeTables.EditMonopictedLinks(GlyfLink: PGlyfRecord; SLinks:
  TLinkArray);
var
  I: integer;
  OffsetIndex: Word;
  GlyfCoverOff: PWord;
  CurveIndex: Integer;
  GlyfCover: PGlyfRecord;
  CurveKeyline: Integer;
begin
  CurveIndex := 11;
  GlyfCover := GlyfRoot;
  CurveKeyline := byte(GlyfLink.Curves[CurveIndex + 1]) shl 8 +
    byte(GlyfLink.Curves[CurveIndex + 2]);
  GlyfCoverOff := PWord(GlyfLink.Curves + CurveIndex + 1);
  OffsetIndex := SLinks[CurveKeyline] + 1;
  GlyfCoverOff^ := Swap(OffsetIndex);
  for I := 1 to CurveKeyline do
    GlyfCover := GlyfCover.NextGlyph;
  if (byte(GlyfCover.Curves[0]) = 255) and (byte(GlyfCover.Curves[1]) = 255)
    then
    EditMonopictedLinks(GlyfCover, SLinks);
  repeat
    if (byte(GlyfLink.Curves[CurveIndex])) and (33) = 33 then
      CurveIndex := CurveIndex + 8
    else if (byte(GlyfLink.Curves[CurveIndex])) and (32) = 32 then
      CurveIndex := CurveIndex + 6
    else break;
    CurveKeyline := byte(GlyfLink.Curves[CurveIndex + 1]) shl 8 +
      byte(GlyfLink.Curves[CurveIndex + 2]);
    GlyfCoverOff := PWord(GlyfLink.Curves + CurveIndex + 1);
    OffsetIndex := SLinks[CurveKeyline] + 1;
    GlyfCoverOff^ := Swap(OffsetIndex);
    GlyfCover := GlyfRoot;
    for I := 1 to CurveKeyline do
      GlyfCover := GlyfCover.NextGlyph;
    if (byte(GlyfCover.Curves[0]) = 255) and (byte(GlyfCover.Curves[1]) =
      255)
      then EditMonopictedLinks(GlyfCover, SLinks);
  until false;
end;

procedure TTrueTypeTables.CompressGlyfTable(FullEscort: boolean);
var
  I: Integer;
  CharsCount: word;
  SNCounter: Integer;
  GlyfExtCode, GlyfCmapCode: Word;
  TempGlyf: PGlyfRecord;
begin
  CapsuleCount := 1;
  TempGlyf := GlyfRoot;
  for I := 1 to MaxpTable.NumGlyphs do
  begin
    if (TempGlyf.IsTagged) and (TempGlyf.RecordSize <> 0) then
      if (byte(TempGlyf.Curves[0]) = 255) and (byte(TempGlyf.Curves[1]) =
        255)
        then
        MergeCompositLinks(TempGlyf);
    TempGlyf := TempGlyf.NextGlyph;
  end;
  TempGlyf := GlyfRoot;
  if FullEscort then
  begin
    SetLength(Links, MaxpTable.NumGlyphs);
    CharsCount := Length(HandleCharsKit);
    for I := 0 to (CharsCount - 1) do
      if (HandleCharsKit[I].Code > 32) then
      begin
        Links[HandleCharsKit[I].NCount] := I;
        Inc(CapsuleCount);
      end;
    TempGlyf := GlyfRoot;
    for I := 1 to MaxpTable.numGlyphs do
    begin
      if TempGlyf.IsTagged = True then
      begin
        if not (TempGlyf.Hidden) then
        begin
          TempGlyf := TempGlyf.NextGlyph;
          continue;
        end;
        Links[(i - 1)] := CapsuleCount;
        Inc(CapsuleCount);
      end
      else
        TempGlyf.RecordSize := 0;
      TempGlyf := TempGlyf.NextGlyph;
    end;
  end
  else
  begin
    SetLength(Links, MaxpTable.NumGlyphs + 1);
    for I := 1 to MaxpTable.numGlyphs do
    begin
      Links[i] := CapsuleCount - 1;
      Inc(CapsuleCount);
      TempGlyf := TempGlyf.NextGlyph;
    end;
  end;
  TempGlyf := GlyfRoot;
  for i := 1 to MaxpTable.numGlyphs do
  begin
    if (TempGlyf.IsTagged = true) and (TempGlyf.RecordSize <> 0) then
    begin
      if (byte(TempGlyf.Curves[0]) = 255) and (byte(TempGlyf.Curves[1]) =
        255)
        then
        EditMonopictedLinks(TempGlyf, Links);
    end;
    TempGlyf := TempGlyf.NextGlyph;
  end;
  CapsuleCount := 0;
  SNCounter := 0;
  TempGlyf := GlyfRoot;
  SetLength(SymbolSelection, MaxpTable.numGlyphs);
  if FullEscort then
  begin
    for I := 1 to MaxpTable.numGlyphs do
    begin
      if (TempGlyf.IsTagged { RecordSize <> 0}) then
      begin
        Inc(CapsuleCount);
        SymbolSelection[CapsuleCount - 1].GlyphID := i;
        GlyfCmapCode := HandleCharsKit[Links[i - 1]].Code;
        SymbolSelection[CapsuleCount - 1].UNICODE := GlyfCmapCode;
        GlyfExtCode := ConvertFromUnicode(GlyfCmapCode);
        SymbolSelection[CapsuleCount - 1].Hidden := TempGlyf.Hidden;
        SymbolSelection[CapsuleCount - 1].CODE := GlyfExtCode;
        if not (TempGlyf.Hidden) then
        begin
          Inc(SNCounter);
          SymbolSelection[CapsuleCount - 1].SerialNumber := SNCounter + 3;
        end;
      end;
      TempGlyf := TempGlyf.NextGlyph;
    end;
  end
  else
  begin
    for I := 1 to MaxpTable.numGlyphs do
    begin
      if (TempGlyf.IsTagged { RecordSize <> 0}) then
      begin
        Inc(CapsuleCount);
        SymbolSelection[CapsuleCount - 1].GlyphID := i;
        GlyfCmapCode := GetGlyfCode(i - 1);
        SymbolSelection[CapsuleCount - 1].UNICODE := GlyfCmapCode;
        GlyfExtCode := ConvertFromUnicode(GlyfCmapCode);
        SymbolSelection[CapsuleCount - 1].Hidden := TempGlyf.Hidden;
        SymbolSelection[CapsuleCount - 1].CODE := GlyfExtCode;
        if not (TempGlyf.Hidden) then
        begin
          Inc(SNCounter);
          SymbolSelection[CapsuleCount - 1].SerialNumber := SNCounter + 3;
        end;
      end;
      TempGlyf := TempGlyf.NextGlyph;
    end;
  end;
  QSort(SymbolSelection, 0, CapsuleCount - 1, false);
end;

function TTrueTypeTables.Checksum(TableBuffer: PVISPDFULONG; Length: VISPDFULONG): VISPDFULONG;
var
  Sum: VISPDFULONG;
  Endptr: PVISPDFULONG;
  RealSize: Integer;
begin
  Sum := 0;
  if TableBuffer <> nil then
  begin
    RealSize := Trunc(((Length + 3) and (not 3)) / sizeof(VISPDFULONG)) - 1;
    Endptr := TableBuffer;
    Inc(Endptr, RealSize);
    while (TableBuffer <> EndPtr) do
    begin
      Sum := Integer(Sum + TableBuffer^);
      Inc(TableBuffer);
    end;
  end;
  result := Sum;
end;

function TTrueTypeTables.GetGlyfCode(GyfNumber: Word): Word;
var
  I, H: Integer;
  Segments: Integer;
  CurrentOffset: VISPDFULONG;
  GlyfLine: VISPDFUSHORT;
begin
  Result := 0;
  Segments := Swap(CMAPTable4.segCountX2) shr 1;
  for I := 0 to Segments - 1 do
  begin
    for H := CMAPTable4.StartCount[I] to CMAPTable4.EndCount[I] do
    begin
      if CMAPTable4.IdRangeOffset[I] = 0 then
        GlyfLine := H + CMAPTable4.IdDelta[I]
      else
      begin
        CurrentOffset := I + (CMAPTable4.IdRangeOffset[I] shr 1) -
          Segments + H
          - CMAPTable4.StartCount[I];
        GlyfLine := CMAPTable4.GlyphIdArray[CurrentOffset] +
          CMAPTable4.IdDelta[I];
      end;
      if GyfNumber = GlyfLine then
      begin
        Result := H;
        Exit;
      end;
    end;
  end;
end;

procedure TTrueTypeTables.GetGLYF(TempStream: TStream);
var
  I: Integer;
  TmpGlyf: PGlyfRecord;
begin
  CapsuleCount := 0;
  TempStream.Write(MarkGlyf[1], 42);
  for I := 1 to 255 do
  begin
    if GlyfStack[I] <> nil then
    begin
      if PGlyfRecord(GlyfStack[I]).RecordSize <> 0 then
      begin
        if PGlyfRecord(GlyfStack[I]).Hidden then continue;
        TempStream.Write(PGlyfRecord(GlyfStack[I]).Curves^, PGlyfRecord(GlyfStack[I]).RecordSize);
        Inc(CapsuleCount);
      end;
    end;
  end;
  TmpGlyf := GlyfRoot;
  for I := 1 to MaxpTable.numGlyphs do
  begin
    if TmpGlyf.RecordSize <> 0 then
    begin
      if not (TmpGlyf.Hidden) then
      begin
        TmpGlyf := TmpGlyf.NextGlyph;
        continue;
      end;
      TempStream.Write(TmpGlyf.Curves^, TmpGlyf.RecordSize);
      Inc(CapsuleCount);
    end;
    TmpGlyf := TmpGlyf.NextGlyph;
  end;
  TempStream.Position := 0;
end;

procedure TTrueTypeTables.GetHMTX(TempStream: TStream);
var
  I, SetVal: Integer;
  Count: Integer;
  TmpGlyf: PGlyfRecord;
const
  Mark = $06000100;
  Space = $02390000;
begin
  SetVal := 0;
  SetVal := DoubleSwap(Mark);
  TempStream.Write(SetVal, 4);
  SetVal := DoubleSwap(Space);
  TempStream.Write(SetVal, 4);
  TmpGlyf := GlyfRoot;
  HheaTable.numberOfHMetrics := swap(HheaTable.numberOfHMetrics);
  Count := (HmtxTable.TableLength div HheaTable.numberOfHMetrics);
  for I := 1 to MaxpTable.numGlyphs do
  begin
    if TmpGlyf.RecordSize > 0 then
    begin
      if TmpGlyf.Hidden then
      begin
        TmpGlyf := TmpGlyf.NextGlyph;
        continue;
      end;
      if ((IsMonoSpaced) and (Count <> 4)) then
      begin
        TempStream.Write(HmtxTable.Item[0], 2);
        TempStream.Write(HmtxTable.Item[i + 2], 2);
      end
      else
      begin
        TempStream.Write(HmtxTable.Item[(i * 2) - 2], 2);
        TempStream.Write(HmtxTable.Item[(i * 2) - 1], 2);
      end;
    end;
    TmpGlyf := TmpGlyf.NextGlyph;
  end;
  TmpGlyf := GlyfRoot;
  for I := 1 to MaxpTable.numGlyphs do
  begin
    if TmpGlyf.RecordSize > 0 then
    begin
      if not (TmpGlyf.Hidden) then
      begin
        TmpGlyf := TmpGlyf.NextGlyph;
        continue;
      end;
      if ((IsMonoSpaced) and (Count <> 4)) then
      begin
        TempStream.Write(HmtxTable.Item[0], 2);
        TempStream.Write(HmtxTable.Item[i + 2], 2);
      end
      else
      begin
        TempStream.Write(HmtxTable.Item[(i * 2) - 2], 2);
        TempStream.Write(HmtxTable.Item[(i * 2) - 1], 2);
      end;
    end;
    TmpGlyf := TmpGlyf.NextGlyph;
  end;
end;

procedure TTrueTypeTables.GetLOCA(TempStream: TStream);
var
  I, SetVal: Integer;
  CharsCount: word;
  Count: LongWord;
  TmpGlyf: PGlyfRecord;

begin
  SetVal := 0;
  TempStream.Write(SetVal, 4);
  Count := 42;
  SetVal := (DoubleSwap(Count));
  TempStream.Write(SetVal, 4);
  TempStream.Write(SetVal, 4);
  CharsCount := Length(HandleCharsKit);
  for I := 0 to (CharsCount - 1) do
  begin
    if (HandleCharsKit[I].Code > 32) then
    begin
      Inc(Count, PGlyfRecord(GlyfStack[ConvertFromUnicode(HandleCharsKit[I].Code)]).RecordSize);
      SetVal := (DoubleSwap(Count));
      TempStream.Write(SetVal, 4);
    end;
  end;
  TmpGlyf := GlyfRoot;
  for I := 1 to MaxpTable.numGlyphs do
  begin
    if TmpGlyf.RecordSize > 0 then
    begin
      if not (TmpGlyf.Hidden) then
      begin
        TmpGlyf := TmpGlyf.NextGlyph;
        continue;
      end;
      Inc(Count, TmpGlyf.RecordSize);
      SetVal := (DoubleSwap(Count));
      TempStream.Write(SetVal, 4);
    end;
    TmpGlyf := TmpGlyf.NextGlyph;
  end;
end;

function TTrueTypeTables.ConvertToUnicode(ID: Word): Word;
var
  MBStr: AnsiString;
  CCodepage: Cardinal;
  CharArray: array[0..0] of word;
begin
  MBStr := AnsiString(chr(ID));
  CCodepage := GetCodepage(FCharSet);
  MultiByteToWideChar(CCodepage, 0, PAnsiChar(MBStr), 1, PWideChar(@CharArray[0]), 1);
  Result := CharArray[0];
end;

function TTrueTypeTables.ConvertFromUnicode(ID: Word): Word;
var
  MBStr: word;
  CCodepage: Cardinal;
  CharArray: array[0..0] of word;
begin
  MBStr := 0;
  CharArray[0] := ID;
  CCodepage := GetCodepage(FCharSet);
  WideCharToMultiByte(CCodepage, 0, PWideChar(@CharArray[0]), 1, PAnsiChar(@MBStr), 1, nil, nil);
  Result := MBStr;
end;

procedure TTrueTypeTables.CreateFontTable(Name: LongWord; var Table:
  TVPDFFontTable);
var
  I: Integer;
  TableStream: TMemoryStream;
  TmpValue: SHORT;
  PostVersion: VISPDFLFIXED;
begin
  TableStream := TMemoryStream.Create;
  try
    if Name = 1668707360 then
    begin
      SetLength(Table.Content, CVTTable.TableLength);
      Move(CVTTable.Item[0], Table.Content[0], CVTTable.TableLength);
      Table.Size := DoubleSwap(CVTTable.TableLength);
    end
    else if Name = 1718642541 then
    begin
      SetLength(Table.Content, FPGMTable.TableLength);
      Move(FPGMTable.Item[0], Table.Content[0], FPGMTable.TableLength);
      Table.Size := DoubleSwap(FPGMTable.TableLength);
    end
    else if Name = 1735162214 then
    begin
      GetGLYF(TableStream);
      TableStream.Position := 0;
      Table.Size := DoubleSwap(TableStream.Size);
      SetLength(Table.Content, TableStream.Size);
      TableStream.Read(Table.Content[0], TableStream.Size);
    end
    else if Name = 1751474532 then
    begin
      SetLength(Table.Content, Sizeof(HeadTableType));
      TmpValue := HeadTable.IndexToLocFormat;
      HeadTable.IndexToLocFormat := Swap(1);
      Move(HeadTable, Table.Content[0], Sizeof(HeadTableType));
      HeadTable.IndexToLocFormat := TmpValue;
      Table.Size := DoubleSwap(Sizeof(HeadTableType));
    end
    else if Name = 1751672161 then
    begin
      SetLength(Table.Content, Sizeof(HheaTableType));
      TmpValue := HheaTable.numberOfHMetrics;
      HheaTable.numberOfHMetrics := swap(CapsuleCount + 2);
      Move(HheaTable, Table.Content[0], Sizeof(HheaTable));
      HheaTable.numberOfHMetrics := TmpValue;
      Table.Size := DoubleSwap(Sizeof(HheaTable));
    end
    else if Name = 1752003704 then
    begin
      GetHMTX(TableStream);
      TableStream.Position := 0;
      Table.Size := DoubleSwap(TableStream.Size);
      SetLength(Table.Content, TableStream.Size);
      TableStream.Read(Table.Content[0], TableStream.Size);
    end
    else if Name = 1819239265 then
    begin
      GetLOCA(TableStream);
      TableStream.Position := 0;
      Table.Size := DoubleSwap(TableStream.Size);
      SetLength(Table.Content, TableStream.Size);
      TableStream.Read(Table.Content[0], TableStream.Size);
    end
    else if Name = 1835104368 then
    begin
      SetLength(Table.Content, SizeOf(MaxpTableType));
      TmpValue := MaxpTable.numGlyphs;
      MaxpTable.numGlyphs := swap(CapsuleCount + 2);
      Move(MaxpTable, Table.Content[0], SizeOf(MaxpTableType));
      MaxpTable.numGlyphs := TmpValue;
      Table.Size := DoubleSwap(SizeOf(MaxpTableType));
    end
    else if Name = 1886352244 then
    begin
      SetLength(Table.Content, SizeOf(TPostTableType));
      TableStream.Position := 0;
      PostVersion := DoubleSwap(196608);
      TableStream.Write(PostVersion, 4);
      PostVersion := 0;
      for I := 0 to 6 do
        TableStream.Write(PostVersion, 4);
      TableStream.Position := 0;
      TableStream.Read(Table.Content[0], 32);
      Table.Size := DoubleSwap(32);
    end
    else if Name = 1886545264 then
    begin
      SetLength(Table.Content, PrepTable.TableLength);
      Move(PrepTable.Item[0], Table.Content[0], PrepTable.TableLength);
      Table.Size := DoubleSwap(PrepTable.TableLength);
    end;
  finally
    TableStream.Free;
  end;
  Table.Name := DoubleSwap(Name);
  Table.CheckSum := Checksum(PVISPDFULONG(@Table.Content[0]),
    DoubleSwap(Table.Size));
end;

procedure TTrueTypeTables.MoveRange(var SegmentMapping: array of DeltaValues;
  ItemIndex: Integer);
var
  SMItem: DeltaValues;
begin
  while SegmentMapping[ItemIndex].StartCode < SegmentMapping[ItemIndex -
    1].StartCode do
  begin
    SMItem := SegmentMapping[ItemIndex - 1];
    SegmentMapping[ItemIndex - 1] := SegmentMapping[ItemIndex];
    SegmentMapping[ItemIndex] := SMItem;
    Dec(ItemIndex);
  end;
end;

procedure TTrueTypeTables.GetFullCMAP(TempStream: TStream);
var
  I, H: Integer;
  TableSign: Word;
  SMLength: Integer;
  SubSegment: TMemoryStream;
  ExtendedTableSign: SmallInt;
  SegmentEntering: OutCmapHead;
  SymbolCode, PrevSymbolCode: Word;
  SymbolNumber, VectorLength: Integer;
  SegmentMapping: array of DeltaValues;
  FirstCode: Integer;
begin
  SymbolNumber := 1;
  PrevSymbolCode := 32;
  FirstCode := DoubleSwap(1);
  TempStream.Write(CodeMapping, 55);
  TempStream.Write(FirstCode, 4);
  VectorLength := Length(SymbolSelection);
  for I := 1 to VectorLength do
  begin
    if SymbolSelection[I - 1].CODE <> 0 then
    begin
      if SymbolSelection[I - 1].Hidden then continue;
      for H := 1 to (SymbolSelection[I - 1].CODE - PrevSymbolCode - 1) do
      begin
        TempStream.Write(CodeMapping, 1);
      end;
      PrevSymbolCode := SymbolSelection[I - 1].CODE;
      Inc(SymbolNumber);
      TempStream.Write(SymbolNumber, 1);
    end;
  end;
  PrevSymbolCode := 32;
  QSort(SymbolSelection, 0, length(SymbolSelection) - 1, true);
  SMLength := 2;
  SetLength(SegmentMapping, SMLength + 1);
  SegmentMapping[0].StartCode := 61440;
  SegmentMapping[0].EndCode := 61440;
  SegmentMapping[0].IDRange := 4096;
  SegmentMapping[1].StartCode := 61472;
  SegmentMapping[1].idRange := 4065;
  QSort(SymbolSelection, 0, length(SymbolSelection) - 1, false);
  for I := 1 to VectorLength do
  begin
    if SymbolSelection[i - 1].CODE <> 0 then
    begin
      if SymbolSelection[I - 1].Hidden then continue;
      SymbolCode := SymbolSelection[i - 1].CODE;
      if ((abs(SymbolCode - PrevSymbolCode) >= 2)) then
      begin
        SegmentMapping[SMLength - 1].EndCode := PrevSymbolCode + 61440;
        MoveRange(SegmentMapping, SMLength - 1);
        Inc(SMLength);
        SetLength(SegmentMapping, SMLength);
        SegmentMapping[SMLength - 1].StartCode := SymbolCode + 61440;
        SegmentMapping[SMLength - 1].idRange := (4096 - SymbolCode) +
          (Links[SymbolSelection[i - 1].GlyphID - 1] + 1);
      end;
      PrevSymbolCode := SymbolCode;
    end;
  end;
  SegmentMapping[Length(SegmentMapping) - 1].EndCode := PrevSymbolCode +
    61440;
  MoveRange(SegmentMapping, SMLength - 1);
  Inc(SMLength);
  SetLength(SegmentMapping, SMLength);
  SegmentMapping[SMLength - 1].EndCode := $FFFF;
  SegmentMapping[SMLength - 1].StartCode := $FFFF;
  SegmentMapping[SMLength - 1].IDRange := $0001;
  TableSign := TempStream.Size;
  QSort(SymbolSelection, 0, length(SymbolSelection) - 1, true);
  TempStream.Seek(0, soFromEnd);
  while TableSign < CmapTable4Offset do
  begin
    TempStream.Write(CodeMapping, 1);
    Inc(TableSign);
  end;
  TableSign := 4;
  TempStream.Write(TableSign, 1);
  SubSegment := TMemoryStream.Create;
  try
    SegmentEntering.Language := 0;
    SegmentEntering.SegCountX2 := Swap(SMLength * 2);
    SegmentEntering.SearchRange := (2 * trunc(power(2, log2(SMLength))));
    SegmentEntering.RangeShift := (2 * SMLength) -
      SegmentEntering.SearchRange;
    SegmentEntering.EntrySelector := Round(Log2(SegmentEntering.SearchRange) / 2);
    SegmentEntering.SearchRange := Swap(SegmentEntering.SearchRange);
    SegmentEntering.EntrySelector := Swap(SegmentEntering.EntrySelector);
    SegmentEntering.RangeShift := Swap(SegmentEntering.RangeShift);
    SubSegment.Write(SegmentEntering, sizeof(OutCmapHead));
    for i := 0 to SMLength - 1 do
    begin
      TableSign := Swap(SegmentMapping[i].EndCode);
      SubSegment.Write(TableSign, 2);
    end;
    TableSign := 0;
    SubSegment.Write(TableSign, 2);
    for i := 0 to SMLength - 1 do
    begin
      TableSign := Swap(SegmentMapping[i].StartCode);
      SubSegment.Write(TableSign, 2);
    end;
    for i := 0 to SMLength - 1 do
    begin
      ExtendedTableSign := Swap(SegmentMapping[i].idRange);
      SubSegment.Write(ExtendedTableSign, 2);
    end;
    TableSign := 0;
    for i := 0 to SMLength - 1 do
      SubSegment.Write(TableSign, 2);
    TableSign := 0;
    TableSign := Swap(TableSign);
    SubSegment.Write(TableSign, 2);
    SymbolNumber := 2;
    SegmentMapping[0].StartCode := 32;
    SubSegment.Position := 0;
    TableSign := SubSegment.Size;
    TableSign := Swap(TableSign + 2);
    TempStream.Write(TableSign, 2);
    TempStream.CopyFrom(SubSegment, Swap(TableSign) - 2);
  finally
    SubSegment.Free;
  end;
end;

procedure TTrueTypeTables.SaveWideCharFont(Stream: TStream);
var
  CMAPTbl: TVPDFFontTable;
  CVTTbl: TVPDFFontTable;
  FPGMTbl: TVPDFFontTable;
  GLYFTbl: TVPDFFontTable;
  HEADTbl: TVPDFFontTable;
  HHEATbl: TVPDFFontTable;
  HMTXTbl: TVPDFFontTable;
  LOCATbl: TVPDFFontTable;
  MAXPTbl: TVPDFFontTable;
  POSTTbl: TVPDFFontTable;
  PREPTbl: TVPDFFontTable;
  PrevOff, PrevSize: Integer;
  TableStream: TMemoryStream;
begin
  PrevSize := 0;
  TableStream := TMemoryStream.Create;
  try
    GetFullCMAP(TableStream);
    TableStream.Position := 0;
    CMAPTbl.Size := DoubleSwap(TableStream.Size);
    SetLength(CMAPTbl.Content, TableStream.Size);
    TableStream.Read(CMAPTbl.Content[0], TableStream.Size);
    CMAPTbl.Name := DoubleSwap(1668112752);
    CMAPTbl.CheckSum := Checksum(PVISPDFULONG(@CMAPTbl.Content[0]),
      DoubleSwap(CMAPTbl.Size));
  finally
    TableStream.Free;
  end;
  if FPGMTable.IsPresent then
    PrevOff := 12 + (16 * Swap(FontStreamHeader.NumTables))
  else
    PrevOff := 12 + (16 * (Swap(FontStreamHeader.NumTables) - 1));
  Stream.Write(FontStreamHeader, 12);
  if CVTTable.IsPresent then
  begin
    CreateFontTable(1668707360, CVTTbl);
    CVTTbl.Offset := DoubleSwap(PrevOff);
    PrevSize := DoubleSwap(CVTTbl.Size);
    Stream.Write(CVTTbl, 16);
  end;
  PrevOff := PrevOff + PrevSize;
  CmapTbl.Offset := DoubleSwap(PrevOff);
  Stream.Write(CmapTbl, 16);
  PrevSize := DoubleSwap(CmapTbl.Size);
  CreateFontTable(1751474532, HEADTbl);
  PrevOff := PrevOff + PrevSize;
  HEADTbl.Offset := DoubleSwap(PrevOff);
  Stream.Write(HEADTbl, 16);
  PrevSize := DoubleSwap(HEADTbl.Size);
  CreateFontTable(1735162214, GLYFTbl);
  PrevOff := PrevOff + PrevSize;
  GlyfTbl.Offset := DoubleSwap(PrevOff);
  Stream.Write(GLYFTbl, 16);
  PrevSize := DoubleSwap(GLYFTbl.Size);
  if FPGMTable.IsPresent then
  begin
    CreateFontTable(1718642541, FPGMTbl);
    PrevOff := PrevOff + PrevSize;
    FPGMTbl.Offset := DoubleSwap(PrevOff);
    Stream.Write(FPGMTbl, 16);
    PrevSize := DoubleSwap(FPGMTbl.Size);
  end;
  CreateFontTable(1751672161, HHEATbl);
  PrevOff := PrevOff + PrevSize;
  HHEATbl.Offset := DoubleSwap(PrevOff);
  Stream.Write(HHEATbl, 16);
  PrevSize := DoubleSwap(HHEATbl.Size);
  CreateFontTable(1752003704, HMTXTbl);
  PrevOff := PrevOff + PrevSize;
  HMTXTbl.Offset := DoubleSwap(PrevOff);
  Stream.Write(HMTXTbl, 16);
  PrevSize := DoubleSwap(HMTXTbl.Size);
  CreateFontTable(1886352244, POSTTbl);
  PrevOff := PrevOff + PrevSize;
  POSTTbl.Offset := DoubleSwap(PrevOff);
  Stream.Write(POSTTbl, 16);
  PrevSize := DoubleSwap(POSTTbl.Size);
  CreateFontTable(1835104368, MAXPTbl);
  PrevOff := PrevOff + PrevSize;
  MAXPTbl.Offset := DoubleSwap(PrevOff);
  Stream.Write(MAXPTbl, 16);
  PrevSize := DoubleSwap(MAXPTbl.Size);
  CreateFontTable(1819239265, LOCATbl);
  PrevOff := PrevOff + PrevSize;
  LOCATbl.Offset := DoubleSwap(PrevOff);
  Stream.Write(LOCATbl, 16);
  PrevSize := DoubleSwap(LOCATbl.Size);
  CreateFontTable(1886545264, PREPTbl);
  PrevOff := PrevOff + PrevSize;
  PREPTbl.Offset := DoubleSwap(PrevOff);
  Stream.Write(PREPTbl, 16);
  if CVTTable.IsPresent then
    Stream.Write(CVTTbl.Content[0], DoubleSwap(CVTTbl.Size));
  Stream.Write(CMAPTbl.Content[0], DoubleSwap(CMAPTbl.Size));
  Stream.Write(HEADTbl.Content[0], DoubleSwap(HEADTbl.Size));
  Stream.Write(GLYFTbl.Content[0], DoubleSwap(GLYFTbl.Size));
  if FPGMTable.IsPresent then
    Stream.Write(FPGMTbl.Content[0], DoubleSwap(FPGMTbl.Size));
  Stream.Write(HHEATbl.Content[0], DoubleSwap(HHEATbl.Size));
  Stream.Write(HMTXTbl.Content[0], DoubleSwap(HMTXTbl.Size));
  Stream.Write(POSTTbl.Content[0], DoubleSwap(POSTTbl.Size));
  Stream.Write(MAXPTbl.Content[0], DoubleSwap(MAXPTbl.Size));
  Stream.Write(LOCATbl.Content[0], DoubleSwap(LOCATbl.Size));
  Stream.Write(PREPTbl.Content[0], DoubleSwap(PREPTbl.Size));
end;

procedure TTrueTypeTables.QSort(var Arr: array of GlyphCodes; BegIndex,
  EndIndex: Integer; FromFirstItem: boolean);
var
  EndItem, BegItem, Mid: Integer;
  Item: GlyphCodes;
begin
  if Length(Arr) > 0 then
  begin
    EndItem := EndIndex;
    BegItem := BegIndex;
    if FromFirstItem then Mid := Arr[(EndItem + BegItem) div 2].UNICODE
    else Mid := Arr[(EndItem + BegItem) div 2].CODE;
    repeat
      if FromFirstItem then while Arr[BegItem].UNICODE < Mid do
          Inc(BegItem)
      else while Arr[BegItem].CODE < Mid do Inc(BegItem);
      if FromFirstItem then while Arr[EndItem].UNICODE > Mid do
          Dec(EndItem)
      else while Arr[EndItem].CODE > Mid do Dec(EndItem);
      if BegItem <= EndItem then
      begin
        Item := Arr[BegItem];
        Arr[BegItem] := Arr[EndItem];
        Arr[EndItem] := Item;
        Inc(BegItem);
        Dec(EndItem);
      end;
    until BegItem > EndItem;
    if EndItem > BegIndex then QSort(Arr, BegIndex, EndItem, FromFirstItem);
    if BegItem < EndIndex then QSort(Arr, BegItem, EndIndex, FromFirstItem);
  end;
end;

function TTrueTypeTables.DoubleSwap(L: Longword): Longword; assembler;
asm
    mov EAX,L
    bswap EAX
    mov @result,EAX
end;

procedure TTrueTypeTables.CharacterDescription(CDArray: PCDescript; FontName:
  TFontname; FontStyle: TFontStyles);
var
  I, H: Word;
  CMap4CLen: Word;
  UPM: Word;
  SlimWidth: Real;
  UniOffset: longint;

begin
  FFontName := FontName;
  FFontStyle := FontStyle;
  GetSourceTables(false);
  SetLength(CDArray^, 65535);
  UPM := Swap(HeadTable.UnitsPerEm);
  CMap4CLen := Round(Swap(CMAPTable4.segCountX2) / 2);
  for I := 0 to CMap4CLen - 2 do
    for H := CMAPTable4.StartCount[I] to CMAPTable4.EndCount[I] do
    begin
      if CMAPTable4.IDRangeOffset[I] = 0 then
        CDArray^[H].Index := Word(H + CMAPTable4.IDDelta[I])
      else
      begin
        UniOffset := I + Trunc(CMAPTable4.idRangeOffset[I] shr 1) -
          Trunc(swap(CMAPTable4.segCountX2) shr 1) + H -
          CMAPTable4.StartCount[I];
        CDArray^[H].Index := Word(CMAPTable4.GlyphIdArray[UniOffset]) +
          CMAPTable4.IDDelta[I];
      end;
      if IsMonoSpaced then
        SlimWidth := Swap(HmtxTable.Item[0])
      else
        SlimWidth := Swap(HmtxTable.Item[CDArray^[H].Index * 2]);
      SlimWidth := SlimWidth / (UPM / 1000);
      CDArray^[H].Width := Trunc(SlimWidth);
    end;
  FreeTempTables;
end;

procedure TTrueTypeTables.GetFontLongEnsemble(FontName: TFontname; FontStyle:
  TFontStyles; OutStream: Tstream; var CharsetBuffer: array of word;
  CharBufferLen: Word);
var
  i: Integer;
  Swaped: Integer;
  LocaByte: PAnsiChar;
  NumOfGlyfs: word;
  TbName: LongWord;
  TbCheckSum: LongInt;
  sLocaOffset: VISPDFUSHORT;
  TmpGlyf: PGlyfRecord;
  LenExpansion: Integer;
  GCurves: TMemoryStream;
  TbOffset, TbSize: Integer;
  PrevOff, PrevSize: Integer;
  LongFontHeader: TFontHeader;
  LocaOffset, xLocaOffset: longint;

  procedure FillStream;
  begin
    TbOffset := (TbOffset + 3) and not 3;
    OutStream.Write((@TbName)^, 4);
    PrevOff := DoubleSwap(TbOffset);
    PrevSize := DoubleSwap(TbSize);
    OutStream.Write((@TbCheckSum)^, 4);
    OutStream.Write((@PrevOff)^, 4);
    OutStream.Write((@PrevSize)^, 4);
  end;

begin
  FFontName := FontName;
  FFontStyle := FontStyle;
  GetSourceTables(true);
  for I := 0 to CharBufferLen - 1 do
    if CharsetBuffer[I] <> 160 then
      MarkUsedGlyf(CharsetBuffer[I]);
  CompressGlyfTable(false);
  LenExpansion := 0;
  if CVTTable.IsPresent then
    Inc(LenExpansion);
  if FPGMTable.IsPresent then
    Inc(LenExpansion);
  if HmtxTable.IsPresent then
    Inc(LenExpansion);
  LongFontHeader.SFNTVersion := $00000100;
  LongFontHeader.NumTables := $0900;
  LongFontHeader.SearchRange := $8000;
  LongFontHeader.EntrySelector := $0300;
  LongFontHeader.RangeShift := $1000;
  LocaByte := PAnsiChar(@LongFontHeader);
  OutStream.Write(LocaByte^, 12);
  TbOffset := 12 + (16 * (6 + LenExpansion));
  if CVTTable.IsPresent then
  begin
    TbName := $20747663;
    TbSize := CVTTable.TableLength;
    TbCheckSum := CheckSum(@CVTTable.Item[0], CVTTable.TableLength);
    FillStream;
  end;
  if FPGMTable.IsPresent then
  begin
    TbName := $6D677066;
    TbOffset := TbOffset + TbSize;
    TbSize := FPGMTable.TableLength;
    TbCheckSum := CheckSum(@FPGMTable.Item[0], FPGMTable.TableLength);
    FillStream;
  end;
  GCurves := TMemoryStream.Create;
  try
    TmpGlyf := GlyfRoot;
    for I := 1 to MaxpTable.numGlyphs do
    begin
      if TmpGlyf.IsTagged then
        GCurves.Write(TmpGlyf.Curves^, TmpGlyf.RecordSize);
      TmpGlyf := TmpGlyf.NextGlyph;
    end;
    TbName := $66796C67;
    TbOffset := TbOffset + TbSize;
    TbSize := GCurves.Size;
    TbCheckSum := CheckSum(GCurves.Memory, GCurves.Size);
    FillStream;
    TbName := $64616568;
    TbOffset := TbOffset + TbSize;
    TbSize := Sizeof(HeadTable);
    TbCheckSum := CheckSum(PVISPDFULONG(@HeadTable), Sizeof(HeadTable));
    FillStream;
    TbName := $61656868;
    TbOffset := TbOffset + TbSize;
    TbSize := Sizeof(HheaTable);
    TbCheckSum := CheckSum(PVISPDFULONG(@HheaTable), Sizeof(HheaTable));
    FillStream;
    TbName := $78746D68;
    TbOffset := TbOffset + TbSize;
    TbSize := HmtxTable.TableLength;
    TbCheckSum := CheckSum(@HmtxTable.Item[0], HmtxTable.TableLength);
    FillStream;
    LocaOffset := 0;
    TmpGlyf := GlyfRoot;
    NumOfGlyfs := MaxpTable.numGlyphs;
    for i := 1 to NumOfGlyfs do
    begin
      if TmpGlyf.IsTagged = false then TmpGlyf.RecordSize := 0;
      if swap(HeadTable.IndexToLocFormat) = 1 then
      begin
        xLocaOffset := DoubleSwap(LocaOffset);
        LocaByte := PAnsiChar(@xLocaOffset);
        LocaTable.Item[(i - 1) * 4] := byte(LocaByte[0]);
        LocaTable.Item[(i - 1) * 4 + 1] := byte(LocaByte[1]);
        LocaTable.Item[(i - 1) * 4 + 2] := byte(LocaByte[2]);
        LocaTable.Item[(i - 1) * 4 + 3] := byte(LocaByte[3]);
        LocaOffset := LocaOffset + TmpGlyf.RecordSize;
      end
      else
      begin
        sLocaOffset := swap(trunc(LocaOffset / 2));
        LocaByte := PAnsiChar(@sLocaOffset);
        LocaTable.Item[(i - 1) * 2] := byte(LocaByte[0]);
        LocaTable.Item[(i - 1) * 2 + 1] := byte(LocaByte[1]);
        LocaOffset := LocaOffset + TmpGlyf.RecordSize;
      end;
      TmpGlyf := TmpGlyf.NextGlyph;
    end;
    if swap(HeadTable.IndexToLocFormat) = 1 then
    begin
      xLocaOffset := DoubleSwap(LocaOffset);
      LocaByte := PAnsiChar(@xLocaOffset);
      LocaTable.Item[NumOfGlyfs * 4] := byte(LocaByte[0]);
      LocaTable.Item[NumOfGlyfs * 4 + 1] := byte(LocaByte[1]);
      LocaTable.Item[NumOfGlyfs * 4 + 2] := byte(LocaByte[2]);
      LocaTable.Item[NumOfGlyfs * 4 + 3] := byte(LocaByte[3]);
    end
    else
    begin
      sLocaOffset := swap(trunc(LocaOffset / 2));
      LocaByte := PAnsiChar(@sLocaOffset);
      LocaTable.Item[NumOfGlyfs * 2] := byte(LocaByte[0]);
      LocaTable.Item[NumOfGlyfs * 2 + 1] := byte(LocaByte[1]);
    end;
    TbName := $61636F6C;
    TbOffset := TbOffset + TbSize;
    if swap(HeadTable.IndexToLocFormat) = 1 then
      TbSize := ((4 + 4 * NumOfGlyfs) + 3) and not 3
    else
      TbSize := ((2 + 2 * NumOfGlyfs) + 3) and not 3;
    TbCheckSum := CheckSum(@LocaTable.Item[0], TbSize);
    FillStream;
    TbName := $7078616D;
    TbOffset := TbOffset + TbSize;
    TbSize := Sizeof(MaxpTable);
    TbCheckSum := CheckSum(PVISPDFULONG(@MaxpTable), TbSize);
    FillStream;
    TbName := $70657270;
    TbOffset := TbOffset + TbSize;
    TbSize := PrepTable.TableLength;
    TbCheckSum := CheckSum(@PrepTable.Item[0], TbSize);
    FillStream;
    if CVTTable.IsPresent then
      OutStream.Write((@CVTTable.Item[0])^, (CVTTable.TableLength + 3) and
        not
        3);
    if FPGMTable.IsPresent then
      OutStream.Write((@FPGMTable.Item[0])^, (FPGMTable.TableLength + 3)
        and not
        3);
    GCurves.Position := 0;
    OutStream.Write(GCurves.Memory^, ((GCurves.Size + 3) and not 3));
  finally
    GCurves.Free;
  end;
  OutStream.Write((@HeadTable)^, (Sizeof(HeadTable) + 3) and not 3);
  OutStream.Write((@HheaTable)^, (Sizeof(HheaTable) + 3) and not 3);
  if HmtxTable.IsPresent then
    OutStream.Write((@HmtxTable.Item[0])^, (HmtxTable.TableLength + 3) and
      not
      3);
  if swap(HeadTable.IndexToLocFormat) = 1 then
    OutStream.Write((@LocaTable.Item[0])^, ((4 + 4 * NumOfGlyfs) + 3) and not
      3)
  else
    OutStream.Write((@LocaTable.Item[0])^, ((2 + 2 * NumOfGlyfs) + 3) and
      not
      3);
  Swaped := MaxpTable.NumGlyphs;
  MaxpTable.NumGlyphs := Swap(MaxpTable.NumGlyphs);
  OutStream.Write((@MaxpTable)^, (Sizeof(MaxpTable) + 3) and not 3);
  MaxpTable.NumGlyphs := Swaped;
  OutStream.Write((@PrepTable.Item[0])^, (PrepTable.TableLength + 3) and not
    3);
  LongCmap := nil;
  FreeTempTables;
  OutStream.Position := 0;
end;


procedure TTrueTypeTables.GetFontEnsemble(FontName: TFontname; FontStyle:
  TFontStyles; OutStream: Tstream; var CharsetBuffer: array of word;
  CharBufferLen: Word);
var
  I: Integer;
  SourceStr: AnsiString;
  CurrCodePage: Cardinal;
  CharArray: array of word;
begin
  if CharBufferLen = 0 then Exit;
  MaxGlyf := 32;
  FFontName := FontName;
  FFontStyle := FontStyle;
  GetSourceTables(false);
  SourceStr := '';
  for I := 1 to CharBufferLen do
    if (CharsetBuffer[I - 1] <> 160) and (CharsetBuffer[I - 1] <> 152) then
      SourceStr := SourceStr + AnsiChar(chr(CharsetBuffer[I - 1]));
  CharBufferLen := Length(SourceStr);
  SetLength(CharArray, CharBufferLen);
  CurrCodePage := GetCodepage(FCharSet);
  MultiByteToWideChar(CurrCodePage, CP_ACP, PAnsiChar(SourceStr), CharBufferLen, @CharArray[0], CharBufferLen);
  ExtractFontCodes(@CharArray[0], CharBufferLen);
  CompressGlyfTable(true);
  SaveWideCharFont(OutStream);
  FreeTempTables;
end;

function TTrueTypeTables.GetCodepage(Charset: Byte): Cardinal;
begin
  if Charset = 1 then
    Charset := GetDefFontCharSet;
  case Charset of
    0: Result := 1252;
    77: Result := 10000;
    128: Result := 932;
    129: Result := 949;
    130: Result := 1361;
    134: Result := 936;
    136: Result := 950;
    161: Result := 1253;
    162: Result := 1254;
    163: Result := 1258;
    177: Result := 1255;
    178: Result := 1256;
    186: Result := 1257;
    204: Result := 1251;
    222: Result := 874;
    238: Result := 1250;
  else
    Result := 1252;
  end;
end;

procedure TTrueTypeTables.FreeTempTables;
var
  i: Integer;
  Temp1, Temp2: PGlyfRecord;
begin
  Temp1 := GlyfRoot;
  for i := 1 to MaxpTable.NumGlyphs do
  begin
    Temp2 := Temp1.NextGlyph;
    if Temp1.Curves <> nil then Freemem(Temp1.Curves);
    Dispose(Temp1);
    Temp1 := Temp2;
  end;
end;

end.
