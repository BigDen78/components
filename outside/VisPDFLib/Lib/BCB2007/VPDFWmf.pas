{*******************************************************}
{                                                       }
{       This unit is part of the VISPDF VCL library.    }
{       Written by R.Husske - ALL RIGHTS RESERVED.      }
{                                                       }
{       Copyright (C) 2000-2009, www.vispdf.com         }
{                                                       }
{       e-mail: support@vispdf.com                      }
{       http://www.vispdf.com                           }
{                                                       }
{*******************************************************}

unit VPDFWmf;

interface
uses Windows, Classes, Sysutils, Graphics, VPDFDoc, VPDFFonts, VPDFData,
  VPDFTypes, Math;

{$I VisPDFLib.inc }

type
  TSmallPolyLine = array[0..$FFFFF] of TSmallPoint;
  PSmallPolyLine = ^TSmallPolyLine;
  TPolyLine = array[0..$FFFFF] of TPoint;
  PPolyLine = ^TPolyLine;

  TVPDFWmf = class
  private
    DContext: HDC;
    MetaHandle: THandle;
    FPage: TVPDFPage;
    Meta: TMetafile;
    MetaCanvas: TMetafileCanvas;
    IsCounterClockwise: Boolean;
    ProjectMode: Integer;
    FontScale: Single;
    PosiX: Single;
    PosiY: Single;
    MadeClip: Boolean;
    CurrentVal: TPoint;
    IsNullBrush: Boolean;
    InterSectClipRect: Boolean;
    FTextContinue: Boolean;
    PolyFIllMode: Boolean;
    BLMode: Integer;
    BKMode: Boolean;
    BKColor: Cardinal;
    TextColor: Cardinal;
    VertMode: TVPDFVerticalJust;
    HorMode: TVPDFHorizJust;
    UpdatePos: Boolean;
    IsCliped: Boolean;
    CurrentPen: TVPDFPen;
    CurrentBrush: TLogBrush;
    CurrentFont: TLogFont;
    CurrentFill: Cardinal;
    ClipRect: TVPDFRectangle;
    lpXForm: TXForm;
    WTransform: Boolean;
    DCBuffer: array of TXForm;
    DCBufferLen: Integer;
    XOffset, YOffset: Single;
    ZScaleX, ZScaleY: Single;
    WTypeFont: Boolean;
    GDIObjects: array of HGDIOBJ;
    GDIObjectsCount: DWORD;
    FPathContinue: Boolean;
    WinOrgEx, WinExtEx, WinOrgEy, WinExtEy: Integer;
    ViewportOrgEx, ViewportExtEx, ViewportOrgEy, ViewportExtEy: Integer;
    procedure ExecuteRecord(Data: PEnhMetaRecord);
    procedure VEMRMOVETOEX(Data: PEMRLineTo);
    procedure VEMRLINETO(Data: PEMRLineTo);
    procedure VEMRENDPATH;
    procedure VEMRFILLPATH;
    procedure VEMRBEGINPATH;
    procedure VEMRCLOSEFIGURE;
    procedure VEMRSTROKEANDFILLPATH;
    procedure VEMRSTROKEPATH;
    procedure VEMRSELECTCLIPPATH;
    procedure VEMRABORTPATH;
    procedure VEMRFILLRGN(Data: PEMRFillRgn);
    procedure VEMRSETTEXTCOLOR(Data: PEMRSetTextColor);
    procedure VEMRSETTEXTALIGN(Data: PEMRSelectClipPath);
    procedure VEMRPOLYBEZIER(Data: PEMRPolyline);
    procedure VEMRPOLYBEZIERTO16(Data: PEMRPolyline16);
    procedure VEMRPOLYBEZIER16(Data: PEMRPolyline16);
    procedure VEMRPOLYBEZIERTO(Data: PEMRPolyline);
    procedure VEMRPOLYLINE(Data: PEMRPolyline);
    procedure VEMRPOLYLINETO(Data: PEMRPolyline);
    procedure VEMRPOLYLINE16(Data: PEMRPolyline16);
    procedure VEMRPOLYLINETO16(Data: PEMRPolyline16);
    procedure VEMRPOLYPOLYLINE(Data: PEMRPolyPolyline);
    procedure VEMRPOLYPOLYLINE16(Data: PEMRPolyPolyline16);
    procedure VEMRPOLYGON(Data: PEMRPolyline);
    procedure VEMRPOLYGON16(Data: PEMRPolyline16);
    procedure VEMRPOLYPOLYGON(Data: PEMRPolyPolyline);
    procedure VEMRPOLYPOLYGON16(Data: PEMRPolyPolyline16);
    procedure VEMRPOLYDRAW(Data: PEMRPolyDraw);
    procedure VEMRPOLYDRAW16(Data: PEMRPolyDraw16);
    procedure VEMRANGLEARC(Data: PEMRAngleArc);
    procedure VEMRELLIPSE(Data: PEMREllipse);
    procedure VEMRRECTANGLE(Data: PEMREllipse);
    procedure VEMRROUNDRECT(Data: PEMRRoundRect);
    procedure VEMRARC(Data: PEMRArc);
    procedure VEMRARCTO(Data: PEMRArc);
    procedure VEMRCHORD(Data: PEMRChord);
    procedure VEMRPIE(Data: PEMRPie);
    procedure VEMREXTTEXTOUT(Data: PEMRExtTextOut);
    procedure VEMRSMALLTEXTOUT(Data: PEMRSMALLTEXTOUTA);
    procedure VEMRALPHABLEND(Data: PEMRAlphaBlend);
    procedure VEMRSETBKMODE(Data: PEMRSelectclippath);
    procedure VEMRSETBKCOLOR(Data: PEMRSetTextColor);
    procedure VEMRSETPOLYFILLMODE(Data: PEMRSelectclippath);
    procedure VEMREXTSELECTCLIPRGN(Data: PEMRExtSelectClipRgn);
    procedure VEMRINTERSECTCLIPRECT(Data: PEMRIntersectClipRect);
    procedure VEMRSETSTRETCHBLTMODE(Data: PEMRSetStretchBltMode);
    procedure VEMRCREATEPEN(Data: PEMRCreatePen);
    procedure VEMREXTCREATEPEN(Data: PEMRExtCreatePen);
    procedure VEMRCREATEBRUSHINDIRECT(Data: PEMRCreateBrushIndirect);
    procedure VEMREXTCREATEFONTINDIRECTW(Data: PEMRExtCreateFontIndirect);
    procedure VEMRSETPIXELV(Data: PEMRSetPixelV);
    procedure VEMRSETMAPMODE(Data: PEMRSetMapMode);
    procedure VEMRSETWINDOWEXTEX(Data: PEMRSetViewportExtEx);
    procedure VEMRSETWINDOWORGEX(Data: PEMRSetViewportOrgEx);
    procedure VEMRSETVIEWPORTEXTEX(Data: PEMRSetViewportExtEx);
    procedure VEMRSETVIEWPORTORGEX(Data: PEMRSetViewportOrgEx);
    procedure VEMRSAVEDC;
    procedure VEMRRESTOREDC(Data: PEMRRestoreDC);
    procedure VEMRSETWORLDTRANSFORM(Data: PEMRSetWorldTransform);
    procedure VEMRMODIFYWORLDTRANSFORM(Data: PEMRModifyWorldTransform);
    procedure VEMRSELECTOBJECT(Data: PEMRSelectObject);
    procedure VEMRDELETEOBJECT(Data: PEMRDeleteObject);
    procedure VEMRSETARCDIRECTION(Data: PEMRSetArcDirection);
    procedure VEMRSETDIBITSTODEVICE(Data: PEMRSetDIBitsToDevice);
    procedure VEMRSTRETCHDIBITS(Data: PEMRStretchDIBits);
    procedure VEMRBITBLT(Data: PEMRBitBlt);
    procedure VEMRSETSTRETCHBLT(Data: PEMRStretchBlt);
    function Displace(Buffer: Pointer; Offset: Integer): Pointer;
  private
    function ZoomX: Single;
    function ZoomY: Single;
    procedure StrokeOrPath;
    procedure FSOrPath;
    procedure SetPenColor;
    procedure SetFontColor;
    procedure MFSetBKColor;
    procedure ActivateCurrentFont;
    procedure SetBrushColor(IsPath: Boolean = True);
    function ProjectX(Value: Single): Single;
    function ProjectY(Value: Single): Single;
    procedure SetTextContinue(const Value: Boolean);
    procedure SetPathContinue(const Value: Boolean);
    function ScaleX(Value: Single; Proj: Boolean = True): Single;
    function ScaleY(Value: Single; Proj: Boolean = True): Single;
    property PathContinue: Boolean read FPathContinue write SetPathContinue;
    property TextContinue: Boolean read FTextContinue write SetTextContinue;
  public
    constructor Create(ParentPage: TVPDFPage);
    procedure Analyse(MF: TMetafile);
    destructor Destroy; override;
  end;

implementation

{ TVPDFWmf }

procedure TVPDFWmf.VEMRMOVETOEX(Data: PEMRLineTo);
begin
  CurrentVal.x := Data^.ptl.x;
  CurrentVal.y := Data^.ptl.y;
  if PathContinue then
  begin
    if TextContinue then TextContinue := False;
    FPage.MoveTo(ScaleX(CurrentVal.x), ScaleY(CurrentVal.y));
  end;
end;

procedure TVPDFWmf.VEMRLINETO(Data: PEMRLineTo);
begin
  if not PathContinue then
    FPage.MoveTo(ScaleX(CurrentVal.x), ScaleY(CurrentVal.y));
  FPage.LineTo(ScaleX(Data^.ptl.x), ScaleY(Data^.ptl.y));
  CurrentVal := Data^.ptl;
  if not PathContinue then StrokeOrPath;
end;

procedure TVPDFWmf.VEMRBEGINPATH;
begin
  PathContinue := True;
  FPage.NewPath;
  FPage.MoveTo(ScaleX(CurrentVal.x), ScaleY(CurrentVal.y));
end;

procedure TVPDFWmf.VEMRENDPATH;
begin
  PathContinue := False;
end;

constructor TVPDFWmf.Create(ParentPage: TVPDFPage);
begin
  FPage := ParentPage;
  Meta := TMetafile.Create;
  MetaCanvas := TMetafileCanvas.Create(Meta, FPage.FParent.FCHandle);
  PosiX := FPage.FParent.Resolution / GetDeviceCaps(FPage.FParent.FCHandle,
    LOGPIXELSX);
  PosiY := FPage.FParent.Resolution / GetDeviceCaps(FPage.FParent.FCHandle,
    LOGPIXELSY);
end;

destructor TVPDFWmf.Destroy;
begin
  MetaCanvas.Free;
  Meta.Free;
  inherited;
end;

procedure TVPDFWmf.VEMRFILLPATH;
begin
  PathContinue := False;
  if not IsNullBrush then FPage.Fill;
  FPage.NewPath;
end;

procedure TVPDFWmf.VEMRCLOSEFIGURE;
begin
  FPage.ClosePath;
end;

procedure TVPDFWmf.VEMRABORTPATH;
begin
  FPage.NewPath;
  PathContinue := False;
end;

procedure TVPDFWmf.VEMRSTROKEPATH;
begin
  PathContinue := False;
  StrokeOrPath;
  FPage.NewPath;
end;

procedure TVPDFWmf.VEMRSTROKEANDFILLPATH;
begin
  PathContinue := False;
  FSOrPath;
  PathContinue := False;
  FPage.NewPath;
end;

procedure TVPDFWmf.VEMREXTSELECTCLIPRGN(Data: PEMRExtSelectClipRgn);
var
  I: Integer;
  RGNRect: TRect;
  RegData: PRgnData;
  DataBuff: Pointer;
begin
  if IsCliped then
  begin
    IsCliped := False;
    FPage.GStateRestore;
    FPage.SetLineWidth(PosiX * CurrentPen.lopnWidth);
    SetPenColor;
    SetBrushColor(False);
    MadeClip := True;
  end;
  if Data^.cbRgnData <> 0 then
  begin
    FPage.GStateSave;
    GetMem(DataBuff, Data^.cbRgnData);
    try
      FPage.NewPath;
      IsCliped := True;
      InterSectClipRect := False;
      RegData := DataBuff;
      Move(Data^.RgnData, DataBuff^, data^.cbRgnData);
      for I := 0 to RegData^.rdh.nCount - 1 do
      begin
        Move(RegData^.Buffer[I * SizeOf(TRect)], RGNRect, SizeOf(RGNRect));
        FPage.MFRectangle(ScaleX(RGNRect.Left, False) + 1, ScaleY(RGNRect.Top,
          False) + 1, ScaleX(RGNRect.Right, False), ScaleY(RGNRect.Bottom, False));
      end;
      FPage.Clip;
      FPage.NewPath;
    finally
      FreeMem(DataBuff);
    end;
  end;
end;

procedure TVPDFWmf.VEMRFILLRGN(Data: PEMRFillRgn);
var
  I: Integer;
  RegData: PRgnData;
  DataBuff: Pointer;
  RGNRect: TRect;
begin
  if Data^.cbRgnData <> 0 then
  begin
    GetMem(DataBuff, Data^.cbRgnData);
    try
      FPage.NewPath;
      RegData := DataBuff;
      Move(Data^.RgnData, DataBuff^, data^.cbRgnData);
      for I := 0 to RegData^.rdh.nCount - 1 do
      begin
        Move(RegData^.Buffer[I * SizeOf(TRect)], RGNRect, SizeOf(RGNRect));
        FPage.MFRectangle(ScaleX(RGNRect.Left, False), ScaleY(RGNRect.Top,
          False), ScaleX(RGNRect.Right, False), ScaleY(RGNRect.Bottom, False));
      end;
      if not IsNullBrush then FPage.Fill;
      FPage.NewPath;
    finally
      FreeMem(DataBuff);
    end;
  end;
end;

procedure TVPDFWmf.VEMRSMALLTEXTOUT(Data: PEMRSMALLTEXTOUTA);
var
  X, Y: Extended;
  TextStr: AnsiString;
  RestoreClip: Boolean;
  RegData: PEMR_SMALLTEXTOUTA;
begin
  RestoreClip := False;
  if Data^.nChars = 0 then Exit;
  if (Data^.fuOptions and $100 = 0) then
  begin
    if IsCliped then
    begin
      RestoreClip := True;
      FPage.GStateRestore;
    end;
    RegData := PEMR_SMALLTEXTOUTA(Data);
    FPage.GStateSave;
    FPage.NewPath;
    FPage.MFRectangle(ScaleX(RegData^.rclClip.Left), ScaleY(RegData^.rclClip.Top),
      ScaleX(RegData^.rclClip.Right), ScaleY(RegData^.rclClip.Bottom));
    FPage.Clip;
    FPage.NewPath;
    X := ScaleX(Data^.ptlReference.x);
    Y := ScaleY(Data^.ptlReference.y);
    SetFontColor;
    ActivateCurrentFont;
    if (Data^.fuOptions and $200 <> 0) then
    begin
      SetLength(TextStr, RegData^.nChars);
      Move(RegData^.cString, TextStr[1], RegData^.nChars);
{$IFNDEF BCB}
      FPage.TextOut(X, Y, CurrentFont.lfEscapement / 10, TextStr);
{$ELSE}
      FPage.PrintText(X, Y, CurrentFont.lfEscapement / 10, TextStr);
{$ENDIF}
    end
    else
    begin
      FPage.UnicodeTextOut(X, Y, CurrentFont.lfEscapement / 10, @(RegData^.cString), RegData^.nChars);
    end;
    FPage.GStateRestore;
    MadeClip := True;
    if RestoreClip then
      if InterSectClipRect then
      begin
        FPage.GStateSave;
        FPage.MFRectangle(ClipRect.Left, ClipRect.Top, ClipRect.Right, ClipRect.Bottom);
        FPage.Clip;
        FPage.NewPath;
      end
      else
        IsCliped := False;
  end
  else
  begin
    X := ScaleX(Data^.ptlReference.x);
    Y := ScaleY(Data^.ptlReference.y);
    SetFontColor;
    ActivateCurrentFont;
    if (Data^.fuOptions and $200 <> 0) then
    begin
      SetLength(TextStr, data^.nChars);
      Move(Data^.cString, TextStr[1], Data^.nChars);
{$IFNDEF BCB}
      FPage.TextOut(X, Y, CurrentFont.lfEscapement / 10, TextStr);
{$ELSE}
      FPage.PrintText(X, Y, CurrentFont.lfEscapement / 10, TextStr);
{$ENDIF}
    end
    else
    begin
      FPage.UnicodeTextOut(X, Y, CurrentFont.lfEscapement / 10, @(Data^.cString), Data^.nChars);
    end;
  end;
end;

procedure TVPDFWmf.VEMRSETTEXTCOLOR(Data: PEMRSetTextColor);
begin
  TextColor := Data^.crColor;
  SetTextColor(DContext, Data^.crColor);
  if TextContinue then SetFontColor;
end;

procedure TVPDFWmf.VEMRSETTEXTALIGN(Data: PEMRSelectClipPath);
begin
  SetTextAlign(DContext, Data^.iMode);
  case Data^.iMode and (TA_LEFT or ta_Right or ta_center) of
    TA_LEFT: HorMode := vhjLeft;
    TA_RIGHT: HorMode := vhjRight;
    TA_CENTER: HorMode := vhjCenter;
  end;
  case Data^.iMode and (TA_Top or ta_BaseLine or ta_Bottom) of
    TA_TOP: VertMode := vvjUp;
    TA_BOTTOM: VertMode := vvjDown;
    TA_BASELINE: VertMode := vvjCenter;
  end;
  UpdatePos := (Data^.iMode and TA_UPDATECP = TA_UPDATECP);
end;

procedure TVPDFWmf.VEMRPOLYBEZIER(Data: PEMRPolyline);
var
  I: Integer;
begin
  if Data^.cptl >= 4 then
  begin
    FPage.MoveTo(ScaleX(Data^.aptl[0].x), ScaleY(Data^.aptl[0].y));
    for I := 1 to (Data^.cptl - 1) div 3 do
      FPage.CurvetoC(ScaleX(Data^.aptl[1 + (I - 1) * 3].x),
        ScaleY(Data^.aptl[1 + (I - 1) * 3].y),
          ScaleX(Data^.aptl[1 + (I - 1) * 3 + 1].x),
        ScaleY(Data^.aptl[1 + (I - 1) * 3 + 1].y),
          ScaleX(Data^.aptl[1 + (I - 1) * 3 + 2].x),
        ScaleY(Data^.aptl[1 + (I - 1) * 3 + 2].y));
    if not PathContinue then StrokeOrPath;
  end;
end;

procedure TVPDFWmf.VEMRPOLYBEZIER16(Data: PEMRPolyline16);
var
  I: Integer;
begin
    if Data^.cpts >= 4 then
    begin
      FPage.MoveTo(ScaleX(Data^.apts[0].x), ScaleY(Data^.apts[0].y));
      for I := 1 to (Data^.cpts - 1) div 3 do
      FPage.CurvetoC(ScaleX(Data^.apts[1 + (I - 1) * 3].x),
          ScaleY(Data^.apts[1 + (I - 1) * 3].y),
          ScaleX(Data^.apts[1 + (I - 1) * 3 + 1].x),
          ScaleY(Data^.apts[1 + (I - 1) * 3 + 1].y),
          ScaleX(Data^.apts[1 + (I - 1) * 3 + 2].x),
          ScaleY(Data^.apts[1 + (I - 1) * 3 + 2].y));
      if not PathContinue then StrokeOrPath;
    end;
  end;

procedure TVPDFWmf.VEMRPOLYBEZIERTO(Data: PEMRPolyline);
var
  I: Integer;
begin
    if Data^.cptl >= 3 then
    begin
      if not PathContinue then
        FPage.MoveTo(ScaleX(CurrentVal.x), ScaleY(CurrentVal.y));
      for I := 1 to (Data^.cptl) div 3 do
      begin
        FPage.CurveToC(ScaleX(Data^.aptl[(I - 1) * 3].x), ScaleY(Data^.aptl[(I
            - 1) * 3].y), ScaleX(Data^.aptl[(I - 1) * 3 + 1].x), ScaleY(Data^.aptl[(I
            - 1) * 3 + 1].y), ScaleX(Data^.aptl[(I - 1) * 3 + 2].x),
            ScaleY(Data^.aptl[(I - 1) * 3 + 2].y));
        CurrentVal := Point(Data^.aptl[(I - 1) * 3 + 2].x, Data^.aptl[(I - 1) * 3 + 2].y);
      end;
      if not PathContinue then StrokeOrPath;
    end;
end;

procedure TVPDFWmf.VEMRPOLYBEZIERTO16(Data: PEMRPolyline16);
var
  I: Integer;
begin
    if Data^.cpts >= 3 then
    begin
      if not PathContinue then FPage.MoveTo(ScaleX(CurrentVal.x), ScaleY(CurrentVal.y));
      for I := 1 to Data^.cpts div 3 do
      begin
        FPage.CurvetoC(ScaleX(Data^.apts[(i - 1) * 3].x), ScaleY(Data^.apts[(i - 1) * 3].y),
          ScaleX(Data^.apts[(i - 1) * 3 + 1].x), ScaleY(Data^.apts[(i - 1) * 3 + 1].y),
            ScaleX(Data^.apts[(i - 1) * 3 + 2].x), ScaleY(Data^.apts[(i - 1) * 3 + 2].y));
        CurrentVal := Point(Data^.apts[(i - 1) * 3 + 2].x, Data^.apts[(i - 1) * 3 + 2].y);
      end;
      if not PathContinue then StrokeOrPath;
    end;
end;

procedure TVPDFWmf.VEMRPOLYDRAW(Data: PEMRPolyDraw);
var
  PLView: PByteArray;
  WCurrPnt: TPoint;
  NCrank: Cardinal;
begin
    if not PathContinue then FPage.NewPath;
    FPage.MoveTo(ScaleX(CurrentVal.x), ScaleY(CurrentVal.y));
    WCurrPnt := CurrentVal;
    PLView := @(Data^.aptl[Data^.cptl]);
    NCrank := 0;
    while NCrank < Data^.cptl do
    begin
      if PLView[NCrank] = PT_MOVETO then
      begin
        WCurrPnt.x := Data^.aPTL[NCrank].x;
        WCurrPnt.y := Data^.aPTL[NCrank].y;
        FPage.MoveTo(ScaleX(WCurrPnt.x), ScaleY(WCurrPnt.y));
        Inc(NCrank);
        CurrentVal := WCurrPnt;
      end
      else if (PLView[NCrank] and PT_LINETO) <> 0 then
      begin
        FPage.LineTo(ScaleX(Data^.aPTL[NCrank].x), ScaleY(Data^.aPTL[NCrank].y));
        Inc(NCrank);
        CurrentVal := Point(Data^.aPTL[NCrank].x, Data^.aPTL[NCrank].y);
        if (PLView[NCrank] and PT_ClOSEFIGURE) <> 0 then
        begin
          FPage.LineTo(ScaleX(WCurrPnt.x), ScaleY(WCurrPnt.y));
          CurrentVal := WCurrPnt;
        end;
      end
      else if (PLView[NCrank] and PT_BEZIERTO) <> 0 then
      begin
        FPage.CurvetoC(ScaleX(Data^.aPTL[NCrank].x), ScaleY(Data^.aPTL[NCrank].y),
          ScaleX(Data^.aPTL[NCrank + 1].x), ScaleY(Data^.aPTL[NCrank + 1].y),
          ScaleX(Data^.aPTL[NCrank + 2].x), ScaleY(Data^.aPTL[NCrank + 2].y));
        CurrentVal := Point(Data^.aPTL[NCrank + 2].x, Data^.aPTL[NCrank + 2].y);
        Inc(NCrank, 3);
        if (PLView[NCrank] and PT_ClOSEFIGURE) <> 0 then
        begin
          FPage.LineTo(ScaleX(WCurrPnt.x), ScaleY(WCurrPnt.y));
          CurrentVal := WCurrPnt;
        end;
      end
    end;
    if not PathContinue then StrokeOrPath;
end;

procedure TVPDFWmf.VEMRPOLYDRAW16(Data: PEMRPolyDraw16);
var
  I: Integer;
  PLView: PByteArray;
  WCurrPnt: TPoint;
  NCrank: Cardinal;
begin
    if not PathContinue then FPage.NewPath;
    FPage.MoveTo(ScaleX(CurrentVal.x), ScaleY(CurrentVal.y));
    WCurrPnt := CurrentVal;
    PLView := @(Data^.apts[Data^.cpts]);
    NCrank := 0;
    I := 0;
    while NCrank < Data^.cpts do
    begin
      if PLView[I] = PT_MOVETO then
      begin
        WCurrPnt.x := Data^.aPTs[NCrank].x;
        WCurrPnt.y := Data^.aPTs[NCrank].y;
        FPage.MoveTo(ScaleX(WCurrPnt.x), ScaleY(WCurrPnt.y));
        Inc(NCrank);
        CurrentVal := WCurrPnt;
      end
      else if (PLView[I] and PT_LINETO) <> 0 then
      begin
        FPage.LineTo(ScaleX(Data^.aPTs[NCrank].x), ScaleY(Data^.aPTs[NCrank].y));
        Inc(NCrank);
        CurrentVal := Point(Data^.aPTS[NCrank].x, Data^.aPTs[NCrank].y);
        if (PLView[I] and PT_ClOSEFIGURE) <> 0 then
        begin
          FPage.LineTo(ScaleX(WCurrPnt.x), ScaleY(WCurrPnt.y));
          CurrentVal := WCurrPnt;
        end;
      end
      else if (PLView[I] and PT_BEZIERTO) <> 0 then
      begin
        FPage.CurvetoC(ScaleX(Data^.aPTs[NCrank].x), ScaleY(Data^.aPTs[NCrank].y),
          ScaleX(Data^.aPTs[NCrank + 1].x), ScaleY(Data^.aPTs[NCrank + 1].y),
          ScaleX(Data^.aPTs[NCrank + 2].x), ScaleY(Data^.aPTs[NCrank + 2].y));
        CurrentVal := Point(Data^.aPTs[NCrank + 2].x, Data^.aPTs[NCrank + 2].y);
        Inc(NCrank, 3);
        if (PLView[I] and PT_ClOSEFIGURE) <> 0 then
        begin
          FPage.LineTo(ScaleX(WCurrPnt.x), ScaleY(WCurrPnt.y));
          CurrentVal := WCurrPnt;
        end;
      end
      else Inc(NCrank);
    end;
    if not PathContinue then StrokeOrPath;
end;

procedure TVPDFWmf.VEMRPOLYGON(Data: PEMRPolyline);
var
  I: Integer;
begin
    if Data^.cptl > 0 then
    begin
      FPage.NewPath;
      FPage.MoveTo(ScaleX(Data^.aptl[0].x), ScaleY(Data^.aptl[0].y));
      for I := 1 to Data^.cptl - 1 do
      begin
        FPage.LineTo(ScaleX(Data^.aptl[i].x), ScaleY(Data^.aptl[I].y));
      end;
      if ( not PathContinue ) then
      begin
        FPage.ClosePath;
        FSOrPath;
      end;
    end;
end;

procedure TVPDFWmf.VEMRPOLYGON16(Data: PEMRPolyline16);
var
  I: Integer;
begin
  if Data^.cpts > 0 then
  begin
    FPage.NewPath;
    FPage.MoveTo(ScaleX(Data^.apts[0].x), ScaleY(Data^.apts[0].y));
    for I := 1 to Data^.cpts - 1 do
    begin
      FPage.LineTo(ScaleX(Data^.apts[I].x), ScaleY(Data^.apts[I].y));
    end;
    if not PathContinue then
    begin
      FPage.ClosePath;
      FSOrPath;
    end;
  end;
end;

procedure TVPDFWmf.VEMRPOLYLINE(Data: PEMRPolyline);
var
  I: Integer;
begin
  if Data^.cptl > 0 then
  begin
    FPage.NewPath;
    FPage.MoveTo(ScaleX(Data^.aptl[0].x), ScaleY(Data^.aptl[0].y));
    for I := 1 to Data^.cptl - 1 do
    begin
      FPage.LineTo(ScaleX(Data^.aptl[I].x), ScaleY(Data^.aptl[I].y));
    end;
    if not PathContinue then StrokeOrPath;
  end;
end;

procedure TVPDFWmf.VEMRPOLYLINE16(Data: PEMRPolyline16);
var
  I: Integer;
begin
  if Data^.cpts > 0 then
  begin
    FPage.NewPath;
    FPage.MoveTo(ScaleX(Data^.apts[0].x), ScaleY(Data^.apts[0].y));
    for I := 1 to Data^.cpts - 1 do
    begin
      FPage.LineTo(ScaleX(Data^.apts[I].x), ScaleY(Data^.apts[I].y));
    end;
    if not PathContinue then StrokeOrPath;
  end;
end;

procedure TVPDFWmf.VEMRPOLYLINETO(Data: PEMRPolyline);
var
  I: Integer;
begin
  if Data^.cptl > 0 then
  begin
    if not PathContinue then
    begin
      FPage.NewPath;
      FPage.MoveTo(ScaleX(CurrentVal.x), ScaleY(CurrentVal.y));
    end;
    for I := 0 to Data^.cptl - 1 do
    begin
      FPage.LineTo(ScaleX(Data^.aptl[I].x), ScaleY(Data^.aptl[I].y));
    end;
    if not PathContinue then StrokeOrPath;
    CurrentVal := Point(Data^.aptl[Data^.cptl - 1].x, Data^.aptl[Data^.cptl - 1].y);
  end;
end;

procedure TVPDFWmf.VEMRPOLYLINETO16(Data: PEMRPolyline16);
var
  I: Integer;
begin
  if Data^.cpts > 0 then
  begin
    if not PathContinue then
    begin
      FPage.NewPath;
      FPage.MoveTo(ScaleX(CurrentVal.x), ScaleY(CurrentVal.y));
    end;
    for I := 0 to Data^.cpts - 1 do
    begin
      FPage.LineTo(ScaleX(Data^.apts[I].x), ScaleY(Data^.apts[I].y));
    end;
    if not PathContinue then StrokeOrPath;
    CurrentVal := Point(Data^.apts[Data^.cpts - 1].x,
      Data^.apts[Data^.cpts - 1].y);
  end;
end;

procedure TVPDFWmf.VEMRPOLYPOLYGON(Data: PEMRPolyPolyline);
var
  I, J: Integer;
  SCrank: Integer;
  CrankLen: Integer;
  PlLine: PPolyLine;
begin
    FPage.NewPath;
    SCrank := SizeOf(TEMRPolyPolyline) - SizeOf(TPoint) + SizeOf(dword) * (Data^.nPolys - 1);
    PlLine := Displace(Data, SCrank);
    SCrank := 0;
    for J := 0 to Data^.nPolys - 1 do
    begin
      FPage.MoveTo(ScaleX(PlLine[SCrank].X), ScaleY(PlLine[SCrank].Y));
      CrankLen := SCrank;
      Inc(SCrank);
      for I := 1 to Data^.aPolyCounts[J] - 1 do
      begin
        FPage.LineTo(ScaleX(PlLine[SCrank].X), ScaleY(PlLine[SCrank].Y));
        Inc(SCrank);
      end;
      FPage.LineTo(ScaleX(PlLine[CrankLen].X), ScaleY(PlLine[CrankLen].Y));
    end;
    FSOrPath;
end;

procedure TVPDFWmf.VEMRPOLYPOLYGON16(Data: PEMRPolyPolyline16);
var
  I, J: Integer;
  SCrank: Integer;
  CrankLen: Integer;
  PlLine: PSmallPolyLine;
begin
    FPage.NewPath;
    SCrank := SizeOf(TEMRPolyPolyline16) - SizeOf(TSmallPoint) + SizeOf(dword) * (Data^.nPolys - 1);
    PlLine := Displace(Data, SCrank);
    SCrank := 0;
    for J := 0 to Data^.nPolys - 1 do
    begin
      FPage.MoveTo(ScaleX(PlLine[SCrank].x), ScaleY(PlLine[SCrank].y));
      CrankLen := SCrank;
      Inc(SCrank);
      for I := 1 to Data^.aPolyCounts[J] - 1 do
      begin
        FPage.LineTo(ScaleX(PlLine[SCrank].x), ScaleY(PlLine[SCrank].y));
        Inc(SCrank);
      end;
      FPage.LineTo(ScaleX(PlLine[CrankLen].x), ScaleY(PlLine[CrankLen].y));
    end;
    FSOrPath;
end;

procedure TVPDFWmf.VEMRPOLYPOLYLINE(Data: PEMRPolyPolyline);
var
  I, J: Integer;
  SCrank: Integer;
  PlLine: PPolyLine;
begin
    FPage.NewPath;
    SCrank := SizeOf(TEMRPolyPolyline) - SizeOf(TPoint) + SizeOf(dword) *
      (Data^.nPolys - 1);
    PlLine := Displace(Data, SCrank);
    SCrank := 0;
    for J := 0 to Data^.nPolys - 1 do
    begin
      FPage.MoveTo(ScaleX(PlLine[SCrank].x), ScaleY(PlLine[SCrank].y));
      Inc(SCrank);
      for I := 1 to Data^.aPolyCounts[J] - 1 do
      begin
        FPage.LineTo(ScaleX(PlLine[SCrank].x), ScaleY(PlLine[SCrank].y));
        Inc(SCrank);
      end;
    end;
    if not PathContinue then StrokeOrPath;
end;

procedure TVPDFWmf.VEMRPOLYPOLYLINE16(Data: PEMRPolyPolyline16);
var
  I, J: Integer;
  SCrank: Integer;
  PlLine: PSmallPolyLine;
begin
  FPage.NewPath;
  SCrank := SizeOf(TEMRPolyPolyline16) - SizeOf(TSmallPoint) + SizeOf(dword) * (Data^.nPolys - 1);
  PlLine := Displace(Data, SCrank);
  SCrank := 0;
  for J := 0 to Data^.nPolys - 1 do
  begin
    FPage.MoveTo(ScaleX(PlLine[SCrank].x), ScaleY(PlLine[SCrank].y));
    Inc(SCrank);
    for I := 1 to Data^.aPolyCounts[J] - 1 do
    begin
      FPage.LineTo(ScaleX(PlLine[SCrank].x), ScaleY(PlLine[SCrank].y));
      Inc(SCrank);
    end;
  end;
  if not PathContinue then StrokeOrPath;
end;

procedure TVPDFWmf.VEMRANGLEARC(Data: PEMRAngleArc);
begin
  FPage.MoveTo(ScaleX(CurrentVal.x), ScaleY(CurrentVal.y));
  FPage.LineTo(ScaleX(Data^.ptlCenter.x + cos(Data^.eStartAngle * Pi / 180) *
    Data^.nRadius), ScaleY(Data^.ptlCenter.y -
    sin(Data^.eStartAngle * Pi / 180) * Data^.nRadius));
  FPage.Ellipse(ScaleX(data^.ptlCenter.x - Integer(Data^.nRadius)),
    ScaleY(data^.ptlCenter.y - Integer(Data^.nRadius)),
    ScaleX(Integer(Data^.nRadius * 2)), ScaleY(Integer(Data^.nRadius * 2)));
  CurrentVal := Point(Round(Data^.ptlCenter.x + cos((Data^.eStartAngle +
    Data^.eSweepAngle) * Pi / 180) * Data^.nRadius),
    Round(Data^.ptlCenter.y - sin((Data^.eStartAngle + Data^.eSweepAngle) *
    Pi / 180) * Data^.nRadius));
  if not PathContinue then
    StrokeOrPath;
end;

procedure TVPDFWmf.VEMRARC(Data: PEMRArc);
begin
  if (not IsCounterClockwise) then
    FPage.DrawArc(ScaleX(Data^.rclBox.Left), ScaleY(Data^.rclBox.Top),
      ScaleX(Data^.rclBox.Right), ScaleY(Data^.rclBox.Bottom),
      ScaleX(Data^.ptlStart.x), ScaleY(Data^.ptlStart.y),
      ScaleX(Data^.ptlEnd.x),
      ScaleY(Data^.ptlEnd.y))
  else
    FPage.DrawArc(ScaleX(Data^.rclBox.Left), ScaleY(Data^.rclBox.Top),
      ScaleX(Data^.rclBox.Right), ScaleY(Data^.rclBox.Bottom),
      ScaleX(Data^.ptlEnd.x), ScaleY(Data^.ptlEnd.y),
      ScaleX(Data^.ptlStart.x),
      ScaleY(Data^.ptlStart.y));
  if not PathContinue then StrokeOrPath;
end;

procedure TVPDFWmf.VEMRARCTO(Data: PEMRArc);
var
  CenterX, CenterY: Extended;
  RadiusX, RadiusY: Extended;
  StartAngle, EndAngle: Extended;

  procedure Exchange(var Left, Right: Integer);
  var
    Middle: Integer;
  begin
    Middle := Left;
    Left := Right;
    Right := Middle;
  end;

begin
  FPage.MoveTo(ScaleX(CurrentVal.x), ScaleY(CurrentVal.y));
  if not IsCounterClockwise then
  begin
    Exchange(Data^.ptlStart.x, Data^.ptlEnd.x);
    Exchange(Data^.ptlStart.y, Data^.ptlEnd.y);
  end;
  CenterX := (Data^.rclBox.Left + Data^.rclBox.Right) / 2;
  CenterY := (Data^.rclBox.Top + Data^.rclBox.Bottom) / 2;
  RadiusX := abs(Data^.rclBox.Left - Data^.rclBox.Right) / 2;
  RadiusY := abs(Data^.rclBox.Top - Data^.rclBox.Bottom) / 2;
  if RadiusX < 0 then RadiusX := 0;
  if RadiusY < 0 then RadiusY := 0;
  StartAngle := ArcTan2(-(Data^.ptlStart.y - CenterY) * RadiusX,
    (Data^.ptlStart.x - CenterX) * RadiusY);
  EndAngle := ArcTan2(-(Data^.ptlEnd.y - CenterY) * RadiusX,
    (Data^.ptlEnd.x - CenterX) * RadiusY);
  FPage.LineTo(ScaleX(CenterX + RadiusX * cos(StartAngle)), ScaleY(CenterY -
    RadiusY *
    sin(StartAngle)));
  FPage.DrawArc(ScaleX(Data^.rclBox.Left), ScaleY(Data^.rclBox.Top),
    ScaleX(Data^.rclBox.Right), ScaleY(Data^.rclBox.Bottom),
    ScaleX(Data^.ptlStart.x), ScaleY(Data^.ptlStart.y),
    ScaleX(Data^.ptlEnd.x),
    ScaleY(Data^.ptlEnd.y));
  CurrentVal := Point(round(CenterX + RadiusX * cos(EndAngle)), Round(CenterY
    - RadiusY * sin(StartAngle)));
  if not PathContinue then StrokeOrPath;
end;


procedure TVPDFWmf.VEMRPIE(Data: PEMRPie);
begin
  FPage.DrawPie(ScaleX(Data^.rclBox.Left), ScaleY(Data^.rclBox.Top),
    ScaleX(Data^.rclBox.Right), ScaleY(Data^.rclBox.Bottom),
    ScaleX(Data^.ptlStart.x), ScaleY(Data^.ptlStart.y),
    ScaleX(Data^.ptlEnd.x),
    ScaleY(Data^.ptlEnd.y));
  if not PathContinue then FSOrPath;
end;

procedure TVPDFWmf.VEMRELLIPSE(Data: PEMREllipse);
begin
  FPage.FMEllipse(ScaleX(data^.rclBox.Left), ScaleY(Data^.rclBox.Top),
    ScaleX(Data^.rclBox.Right), ScaleY(Data^.rclBox.Bottom));
  if not PathContinue then
    if (CurrentPen.lopnWidth <> 0) and (CurrentPen.lopnStyle <> ps_null) then
      if not IsNullBrush then FPage.FillAndStroke
      else FPage.Stroke
    else if not IsNullBrush then FPage.Fill
    else FPage.NewPath;
end;

procedure TVPDFWmf.VEMRRECTANGLE(Data: PEMREllipse);
begin
  if (data^.rclBox.Left = data^.rclBox.Right) or (data^.rclBox.Top = data^.rclBox.Bottom) then
  begin
    FPage.NewPath;
    Exit;
  end;
  FPage.MFRectangle(ScaleX(data^.rclBox.Left), ScaleY(data^.rclBox.Top),
    ScaleX(Data^.rclBox.Right), ScaleY(Data^.rclBox.Bottom));
  if (not PathContinue ) then
    if (CurrentPen.lopnWidth <> 0) and (CurrentPen.lopnStyle <> ps_null) then
      if not IsNullBrush then FPage.FillAndStroke
      else FPage.Stroke
    else if not IsNullBrush then FPage.Fill
    else FPage.NewPath;
end;

procedure TVPDFWmf.VEMRCHORD(Data: PEMRChord);
var
  StartPoint: TVPDFCurrPoint;
begin
  if IsCounterClockwise then
    StartPoint := FPage.DrawArc(ScaleX(Data^.rclBox.Left),
      ScaleY(Data^.rclBox.Top),
      ScaleX(Data^.rclBox.Right), ScaleY(Data^.rclBox.Bottom),
      ScaleX(Data^.ptlStart.x), ScaleY(Data^.ptlStart.y),
      ScaleX(Data^.ptlEnd.x),
      ScaleY(Data^.ptlEnd.y))
  else
    StartPoint := FPage.DrawArc(ScaleX(Data^.rclBox.Left),
      ScaleY(Data^.rclBox.Top),
      ScaleX(Data^.rclBox.Right), ScaleY(Data^.rclBox.Bottom),
      ScaleX(Data^.ptlEnd.x), ScaleY(Data^.ptlEnd.y),
      ScaleX(Data^.ptlStart.x),
      ScaleY(Data^.ptlStart.y));
  FPage.LineTo(ScaleX(StartPoint.x), ScaleY(StartPoint.y));
  if not PathContinue then
    FSOrPath;
end;

procedure TVPDFWmf.VEMREXTTEXTOUT(Data: PEMRExtTextOut);
var
  I: Integer;
  X, Y: Extended;
  WXScal, WYScal: Extended;
  TextStr: AnsiString;
  TxtPLace: String;
  TextData: Pointer;
  TextSize: Extended;
  LOffsetX: PINT;
  SOffsetX: PINT;
  RotAngle: Extended;
  TXLen: Extended;
  RestoreClip: Boolean;
  IsCLOP: Boolean;
  TextLen: Integer;
  KGlyphs: PWordArray;
  GlyphArray: array of Word;
  Combined: boolean;
  IsGlyphs: Boolean;
  TextGCP: tagGCP_RESULTS;
  CodePage: Integer;
  FntCharset: TFontCharset;
begin
  RestoreClip := False;
  FPage.TopTextPosition := true;
  IsGlyphs := Data^.emrtext.fOptions and ETO_GLYPH_INDEX <> 0;
  if not PathContinue then
  begin
    if (Data^.emrtext.fOptions and ETO_CLIPPED <> 0) or
      (Data^.emrtext.fOptions and ETO_OPAQUE <> 0) or BKMode then
    begin
      IsCLOP := True;
      if (Data^.emrtext.fOptions and ETO_Clipped <> 0) and
        (Data^.emrtext.nChars <> 0) then
      begin
        if IsCliped then
        begin
          RestoreClip := True;
          FPage.GStateRestore;
        end;
        FPage.GStateSave;
        FPage.NewPath;
        if ((Data^.emrtext.fOptions and ETO_OPAQUE <> 0) or BKMode) and
          (Data^.emrtext.rcl.Right - Data^.emrtext.rcl.Left > 0) and
          (Data^.emrtext.rcl.Bottom - Data^.emrtext.rcl.Top > 0) then
          IsCLOP := False;
        if not IsCLOP then MFSetBKColor;
        FPage.MFRectangle(ScaleX(Data^.emrtext.rcl.Left),
          ScaleY(Data^.emrtext.rcl.Top),
          ScaleX(Data^.emrtext.rcl.Right),
          ScaleY(Data^.emrtext.rcl.Bottom));
        FPage.Clip;
        if not IsCLOP then FPage.Fill
        else FPage.NewPath;
      end;
      if IsCLOP and ((Data^.emrtext.fOptions and ETO_OPAQUE <> 0) or BKMode)
        and (Data^.emrtext.rcl.Right - Data^.emrtext.rcl.Left > 0) and
        (Data^.emrtext.rcl.Bottom - Data^.emrtext.rcl.Top > 0) then
      begin
        MFSetBKColor;
        FPage.NewPath;
        FPage.MFRectangle(ScaleX(Data^.emrtext.rcl.Left),
          ScaleY(Data^.emrtext.rcl.Top),
          ScaleX(Data^.emrtext.rcl.Right),
          ScaleY(Data^.emrtext.rcl.Bottom));
        FPage.Fill;
      end;
    end;
  end;
  TextLen := Data^.emrtext.nChars;
  KGlyphs := Displace(Data, Data^.emrtext.offString);
  if TextLen <> 0 then
  begin
    SetFontColor;
    ActivateCurrentFont;
    if Data^.emrtext.offDx <> 0 then
      LOffsetX := Displace(Data, Data^.emrtext.offDx)
    else LOffsetX := nil;
    if Data^.emr.iType = EMR_EXTTEXTOUTW then
    begin
      Combined := False;
      if not IsGlyphs then
      begin
        for i := 0 to Data^.emrtext.nChars - 1 do
          if (KGlyphs[i] >= $0300) and (KGlyphs[i] <= $036F) then
          begin
            Combined := true;
            break;
          end;
      end;
      if Combined then
      begin
        if WTransform then
        begin
          FillChar(TextGCP, SizeOf(TextGCP), 0);
          TextGCP.lStructSize := SizeOf(TextGCP);
          TextGCP.nGlyphs := TextLen;
          TextGCP.nMaxFit := TextLen;
          SetLength(GlyphArray, TextLen);
          TextGCP.lpGlyphs := @GlyphArray[0];
{$IFDEF VXVERSION}
          if GetCharacterPlacementW(DContext, @KGlyphs[0], LongBool(TextLen), LongBool(0),
{$ELSE}
          if GetCharacterPlacementW(DContext, @KGlyphs[0], TextLen, 0,
{$ENDIF}
            TextGCP, GCP_DIACRITIC or GCP_GLYPHSHAPE or GCP_REORDER) <> 0 then
          begin
            if TextLen <> Integer(TextGCP.nGlyphs) then
            begin
              TextLen := TextGCP.nGlyphs;
              LOffsetX := nil;
              KGlyphs := PWORDArray(TextGCP.lpGlyphs);
            end;
          end;
        end
        else
        begin
          if CurrentFont.lfCharSet = DEFAULT_CHARSET then
          begin
            FntCharset := GetDefFontCharSet;
          end
          else
          begin
            FntCharset := CurrentFont.lfCharSet;
          end;
          case FntCharset of
            EASTEUROPE_CHARSET: CodePage := 1250;
            RUSSIAN_CHARSET: CodePage := 1251;
            GREEK_CHARSET: CodePage := 1253;
            TURKISH_CHARSET: CodePage := 1254;
            BALTIC_CHARSET: CodePage := 1257;
            VIETNAMESE_CHARSET: CodePage := 1258;
            SHIFTJIS_CHARSET: CodePage := 932;
            129: CodePage := 949;
            CHINESEBIG5_CHARSET: CodePage := 950;
            GB2312_CHARSET: CodePage := 936;
          else
            CodePage := 1252;
          end;
          I := WideCharToMultiByte(CodePage, 0, @KGlyphs[0], TextLen, nil, 0,
            nil, nil);
          if I <> 0 then
          begin
            SetLength(TextStr, I);
            I := WideCharToMultiByte(CodePage, 0, @KGlyphs[0], TextLen, @TextStr[1], I, nil, nil);
            if I <> 0 then
            begin
              FillChar(TextGCP, SizeOf(TextGCP), 0);
              TextGCP.lStructSize := SizeOf(TextGCP);
              TextGCP.nGlyphs := I;
              TextGCP.nMaxFit := I;
              SetLength(GlyphArray, I);
              TextGCP.lpGlyphs := @GlyphArray[0];
              TxtPLace := String(TextStr);
{$IFDEF VXVERSION}
              if GetCharacterPlacement(DContext, PChar(TxtPLace), LongBool(I), LongBool(0),
{$ELSE}
              if GetCharacterPlacement(DContext, PChar(TxtPLace), I, 0,
{$ENDIF}
                TextGCP, GCP_DIACRITIC or GCP_GLYPHSHAPE or
                GCP_REORDER) <> 0 then
              begin
                if I <> Integer(TextGCP.nGlyphs) then
                begin
                  TextLen := TextGCP.nGlyphs;
                  LOffsetX := nil;
                  KGlyphs := PWORDArray(TextGCP.lpGlyphs);
                end;
              end;
            end;
          end;
        end;
      end;
    end
    else
    begin
      SetLength(TextStr, Data^.emrtext.nChars);
      TextData := Displace(Data, Data^.emrtext.offString);
      Move(TextData^, TextStr[1], Data^.emrtext.nChars);
    end;
    if LOffsetX <> nil then
    begin
      TextSize := 0;
      for i := 0 to TextLen - 1 do
      begin
        TextSize := TextSize + LOffsetX^;
        Inc(LOffsetX);
      end;
      TextSize := TextSize * ZoomX;
    end
    else TextSize := Data^.emrtext.rcl.Right - Data^.emrtext.rcl.Left;
    if UpdatePos then
    begin
      X := CurrentVal.X;
      Y := CurrentVal.Y;
      if CurrentFont.lfEscapement <> 0 then
      begin
        CurrentVal.X := CurrentVal.X + round(TextSize *
          cos(CurrentFont.lfEscapement * Pi /
          1800));
        CurrentVal.Y := CurrentVal.Y - round(TextSize *
          sin(CurrentFont.lfEscapement * Pi /
          1800));
      end
      else CurrentVal.X := CurrentVal.X + round(TextSize);
    end
    else
    begin
      X := Data^.emrtext.ptlReference.X;
      Y := Data^.emrtext.ptlReference.Y;
    end;
    if CurrentFont.lfEscapement = 0 then
    begin
      case VertMode of
        vvjCenter:
          begin
            Y := ScaleY(Y) - FPage.TextHeight('Xg');
            FPage.TopTextPosition := false;
          end;
        vvjDown: Y := ScaleY(Y) - FPage.TextHeight('Xg');
      else Y := ScaleY(Y);
      end;
        case HorMode of
          vhjRight: x := ScaleX(X - TextSize);
          vhjCenter: x := ScaleX(X - TextSize / 2);
        else X := ScaleX(X);
      end;
    end
    else
    begin
      if (VertMode = vvjUp) and (HorMode = vhjLeft) then
      begin
        Y := ScaleY(Y, true);
        X := ScaleX(X, true);
      end
      else
      begin
        case VertMode of
          vvjCenter: WYScal := MetaCanvas.TextHeight('X');
          vvjDown: WYScal := MetaCanvas.TextHeight('Xg');
        else WYScal := 0;
        end;
        case HorMode of
          vhjRight: WXScal := TextSize;
          vhjCenter: WXScal := TextSize / 2;
        else WXScal := 0;
        end;
        RotAngle := CurrentFont.lfEscapement * Pi / 1800;
        if WYScal = 0 then
        begin
          X := X - WXScal * cos(RotAngle);
          Y := Y + WXScal * sin(RotAngle);
        end
        else
        begin
          TXLen := sqrt(sqr(WXScal) + sqr(WYScal));
          X := X - TXLen * cos(RotAngle - ArcSin(WYScal / TXLen));
          Y := Y + TXLen * sin(RotAngle - ArcSin(WYScal / TXLen));
        end;
        Y := ScaleY(Y, true);
        X := ScaleX(X, true);
      end;
    end;
    if Data^.emr.iType = EMR_EXTTEXTOUTW then
    begin
      FPage.UnicodeTextOut(X, Y, CurrentFont.lfEscapement / 10, @KGlyphs[0], TextLen);
    end
    else
    begin
{$IFNDEF BCB}
      FPage.TextOut(X, Y, CurrentFont.lfEscapement / 10, TextStr);
{$ELSE}
      FPage.PrintText(X, Y, CurrentFont.lfEscapement / 10, TextStr);
{$ENDIF}
    end;
  end;
  if not PathContinue then
  begin
    if (Data^.emrtext.fOptions and ETO_Clipped <> 0) then
    begin
      FPage.GStateRestore;
      MadeClip := True;
      if RestoreClip then
        if InterSectClipRect then
        begin
          FPage.GStateSave;
          FPage.MFRectangle(ClipRect.Left, ClipRect.Top,
            ClipRect.Right, ClipRect.Bottom);
          FPage.Clip;
          FPage.NewPath;
        end
        else IsCliped := False;
    end;
  end;
end;

procedure TVPDFWmf.VEMRALPHABLEND(Data: PEMRAlphaBlend);
var
  I: Integer;
  BMSrc: TBitmap;
  BMTemp: TBitmap;
  BMBits: Pointer;
  HImgLib: THandle;
  BMInfo: PBitmapInfo;
  AlphaComplete: Boolean;
  BlendStruc: _BLENDFUNCTION;

  BlendFnc: function(hdcDest: HDC; nXOriginDest, nYOriginDest, nWidthDest, nHeightDest: Integer;
                 hdcSrc: HDC; nXOriginSrc, nYOriginSrc, nWidthSrc, nHeightSrc: Integer;
                 blendFunction: TBlendFunction): BOOL; stdcall;
begin
  BMInfo := Displace(Data, Data^.offBmiSrc);
  BMBits := Displace(Data, Data^.offBitsSrc);
  I := 0;
  if (Data^.cySrc > 0) and (Data^.cxSrc > 0) then
  begin
    BMTemp := TBitmap.Create;
    try
      BMTemp.Width := Data^.cxSrc;
      BMTemp.Height := Data^.cySrc;
      StretchDIBits(BMTemp.Canvas.Handle, 0, 0, BMTemp.Width, BMTemp.Height, Data^.xSrc,
        Data^.ySrc, BMTemp.Width, BMTemp.Height, BMBits, BMInfo^, Data^.iUsageSrc, SRCCOPY);
      AlphaComplete := False;
      HImgLib := LoadLibrary('msimg32.dll');
      if HImgLib <> 0 then
      begin
        @BlendFnc := GetProcAddress(HImgLib, 'AlphaBlend');
        if Assigned(@BlendFnc) then
        begin
          AlphaComplete := True;
          BMSrc := TBitmap.Create;
          try
            BMSrc.Width := BMTemp.Width;
            BMSrc.Height := BMTemp.Height;
            Move(Data^.dwRop, BlendStruc, sizeof(BlendStruc));
            BlendFnc(BMSrc.Canvas.Handle, 0, 0, BMTemp.Width, BMTemp.Height,
              BMTemp.Canvas.Handle, 0, 0, BMTemp.Width, BMTemp.Height, BlendStruc);
            if BMSrc.PixelFormat = pf1bit then
              I := FPage.FParent.AddImage(BMSrc, icCCITT42)
            else
              I := FPage.FParent.AddImage(BMSrc, icFlate);
          finally
            BMSrc.Free;
          end;
        end;
      end;
      if (not (AlphaComplete)) then
      begin
        if BMTemp.PixelFormat = pf1bit then I := FPage.FParent.AddImage(BMTemp, icCCITT42)
        else I := FPage.FParent.AddImage(BMTemp, icFlate);
      end;
      FPage.ShowImage(I, ScaleX(Data^.rclBounds.Left, False),
        ScaleY(Data^.rclBounds.Top, False),
        ScaleX(Data^.rclBounds.Right - Data^.rclBounds.Left, False),
        ScaleY(Data^.rclBounds.Bottom - Data^.rclBounds.Top, False), 0);
    finally
      BMTemp.Free;
    end;
  end
  else
  begin
    if (data^.cxDest = 0) or (data^.cyDest = 0) then
      FPage.NewPath
    else
    begin
      FPage.MFRectangle(ScaleX(data^.xDest), ScaleY(Data^.yDest),
        ScaleX(data^.xDest + Data^.cxDest), ScaleY(Data^.yDest + Data^.cyDest));
      FPage.Fill;
    end;
  end;
end;

procedure TVPDFWmf.VEMRBITBLT(Data: PEMRBitBlt);
var
  I: Integer;
  TextPres: Boolean;
  SourBM: TBitmap;
  BMBits: Pointer;
  BMInfo: PBitmapInfo;
  BMRow: Cardinal;
begin
  BMRow := Data^.dwRop;
  if not ((BMRow = SRCCOPY) or (BMRow = BLACKNESS) or (BMRow = DSTINVERT) or
     (BMRow = MERGECOPY) or (BMRow = MERGEPAINT) or (BMRow = NOTSRCCOPY) or
     (BMRow = NOTSRCERASE) or (BMRow = PATCOPY) or (BMRow = PATINVERT) or
     (BMRow = PATPAINT) or (BMRow = SRCAND) or (BMRow = SRCERASE) or
     (BMRow = SRCINVERT) or (BMRow = SRCPAINT) or (BMRow = WHITENESS)) then Exit;
  if TextContinue then
  begin
    TextContinue := False;
    TextPres := True;
  end
  else TextPres := False;
  if (Data^.cbBmiSrc = 0) or (Data^.cbBitsSrc = 0) then
  begin
    if (data^.cxDest = 0) or (data^.cyDest = 0) then FPage.NewPath
    else
    begin
      SetBrushColor(not MadeClip);
      FPage.MFRectangle(ScaleX(data^.xDest), ScaleY(Data^.yDest),
        ScaleX(data^.xDest + Data^.cxDest), ScaleY(Data^.yDest + Data^.cyDest));
      FPage.Fill;
    end;
  end
  else
  begin
    BMInfo := Displace(Data, Data^.offBmiSrc);
    BMBits := Displace(Data, Data^.offBitsSrc);
    SourBM := TBitmap.Create;
    try
      SourBM.Width := Data^.cxDest;
      SourBM.Height := Data^.cyDest;
      StretchDIBits(SourBM.Canvas.Handle, 0, 0, SourBM.Width, SourBM.Height, Data^.xSrc,
        Data^.ySrc, SourBM.Width, SourBM.Height, BMBits, BMInfo^, Data^.iUsageSrc, Data^.dwRop);
      if SourBM.PixelFormat = pf1bit then I := FPage.FParent.AddImage(SourBM, icCCITT42)
      else I := FPage.FParent.AddImage(SourBM, icFlate);
      FPage.ShowImage(I, ScaleX(Data^.rclBounds.Left, False),
        ScaleY(Data^.rclBounds.Top, False),
        ScaleX(Data^.rclBounds.Right - Data^.rclBounds.Left, False),
        ScaleY(Data^.rclBounds.Bottom - Data^.rclBounds.Top, False), 0);
    finally
      SourBM.Free;
    end;
  end;
  if TextPres then TextContinue := True;
end;

procedure TVPDFWmf.VEMRCREATEBRUSHINDIRECT(Data: PEMRCreateBrushIndirect);
begin
  if Data^.ihBrush >= GDIObjectsCount then Exit;
  if GDIObjects[Data^.ihBrush] <> $FFFFFFFF then
    DeleteObject(GDIObjects[Data^.ihBrush]);
  if Data^.lb.lbStyle = BS_SOLID then
    GDIObjects[Data^.ihBrush] := CreateSolidBrush(Data^.lb.lbColor)
  else GDIObjects[Data^.ihBrush] := CreateBrushIndirect(Data^.lb);
end;

procedure TVPDFWmf.VEMREXTCREATEFONTINDIRECTW(Data: PEMRExtCreateFontIndirect);
var
  LogFont: TLogFontA;
begin
  if Data^.ihFont >= GDIObjectsCount then Exit;
  if GDIObjects[Data^.ihFont] <> $FFFFFFFF then DeleteObject(GDIObjects[Data^.ihFont]);
  GDIObjects[Data^.ihFont] := CreateFontIndirectW(Data^.elfw.elfLogFont);
  if GDIObjects[Data^.ihFont] = 0 then
  begin
    Move(data^.elfw.elfLogFont, LogFont, SizeOf(LogFont));
    WideCharToMultiByte(CP_ACP, 0, Data^.elfw.elfLogFont.lfFaceName,
      LF_FACESIZE, LogFont.lfFaceName, LF_FACESIZE, nil, nil);
    GDIObjects[Data^.ihFont] := CreateFontIndirectA(LogFont);
  end;
end;

procedure TVPDFWmf.VEMRCREATEPEN(Data: PEMRCreatePen);
begin
  if Data^.ihPen >= GDIObjectsCount then Exit;
  if GDIObjects[Data^.ihPen] <> $FFFFFFFF then
    DeleteObject(GDIObjects[Data^.ihPen]);
  GDIObjects[Data^.ihPen] := CreatePen(Data^.lopn.lopnStyle,
    Data^.lopn.lopnWidth.x, Data^.lopn.lopnColor);
end;

procedure TVPDFWmf.VEMRDELETEOBJECT(Data: PEMRDeleteObject);
begin
  if Data^.ihObject >= GDIObjectsCount then Exit;
  DeleteObject(GDIObjects[data^.ihObject]);
  GDIObjects[data^.ihObject] := $FFFFFFFF;
end;

procedure TVPDFWmf.VEMREXTCREATEPEN(Data: PEMRExtCreatePen);
begin
  if Data^.ihPen >= GDIObjectsCount then Exit;
  if GDIObjects[Data^.ihPen] <> $FFFFFFFF then
    DeleteObject(GDIObjects[Data^.ihPen]);
  GDIObjects[Data^.ihPen] := CreatePen(Data^.elp.elpPenStyle and
    PS_STYLE_MASK, Data^.elp.elpWidth, Data^.elp.elpColor);
end;

procedure TVPDFWmf.VEMRINTERSECTCLIPRECT(Data: PEMRIntersectClipRect);
begin
  if IsCliped then
  begin
    FPage.GStateRestore;
    MadeClip := True;
    SetPenColor;
    SetBrushColor(False);
    FPage.SetLineWidth(PosiX * CurrentPen.lopnWidth);
  end;
  FPage.GStateSave;
  IsCliped := True;
  FPage.NewPath;
  FPage.MFRectangle(ScaleX(Data^.rclClip.Left), ScaleY(Data^.rclClip.Top),
    ScaleX(Data^.rclClip.Right), ScaleY(Data^.rclClip.Bottom));
  InterSectClipRect := True;
  ClipRect.Left := ScaleX(Data^.rclClip.Left);
  ClipRect.Top := ScaleY(Data^.rclClip.Top);
  ClipRect.Right := ScaleX(Data^.rclClip.Right);
  ClipRect.Bottom := ScaleY(Data^.rclClip.Bottom);
  FPage.Clip;
  FPage.NewPath;
end;

procedure TVPDFWmf.VEMRRESTOREDC(Data: PEMRRestoreDC);
var
  I: DWORD;
  DCPen: TLogPen;
  TmpObj: HGDIOBJ;
  TmpForm: TXForm;
  BlockSize: TSize;
  BlockBuff: TPoint;
  DCBrush: TLogBrush;
  DCFont: TLogFont;
begin
  RestoreDC(DContext, Data^.iRelative);
  if WTransform then
  begin
    GetWorldTransform(DContext, TmpForm);
    XOffset := TmpForm.eDx * PosiX;
    YOffset := TmpForm.eDy * PosiY;
    ZScaleX := TmpForm.eM11;
    ZScaleY := TmpForm.eM22;
  end
  else
  begin
    if Data^.iRelative < 0 then
      if DCBufferLen + Data^.iRelative >= 0 then
      begin
        lpXForm := DCBuffer[DCBufferLen + Data^.iRelative];
        ZScaleX := lpXForm.eM11;
        ZScaleY := lpXForm.eM22;
        XOffset := lpXForm.eDx * PosiX;
        YOffset := lpXForm.eDy * PosiY;
        DCBufferLen := DCBufferLen + Data^.iRelative;
        SetLength(DCBuffer, DCBufferLen);
      end
      else DCBuffer := nil
    else if DCBufferLen > Data^.iRelative then
    begin
      lpXForm := DCBuffer[Data^.iRelative - 1];
      ZScaleX := lpXForm.eM11;
      ZScaleY := lpXForm.eM22;
      XOffset := lpXForm.eDx * PosiX;
      YOffset := lpXForm.eDy * PosiY;
      DCBufferLen := Data^.iRelative - 1;
      SetLength(DCBuffer, DCBufferLen);
    end;
  end;
  ProjectMode := GetMapMode(DContext);
  BKMode := not (GetBkMode(DContext) = TRANSPARENT);
  TextColor := GetTextColor(DContext);
  if TextContinue then SetFontColor;
  BKColor := GetBkColor(DContext);
  PolyFIllMode := (GetPolyFillMode(DContext) = ALTERNATE);
  GetViewportExtEx(DContext, BlockSize);
  ViewportExtEx := BlockSize.cx;
  ViewportExtEy := BlockSize.cy;
  GetWindowExtEx(DContext, BlockSize);
  WinExtEx := BlockSize.cx;
  WinExtEy := BlockSize.cy;
  GetViewportOrgEx(DContext, BlockBuff);
  ViewportOrgEx := BlockBuff.x;
  ViewportOrgEy := BlockBuff.y;
  GetWindowOrgEx(DContext, BlockBuff);
  WinOrgEx := BlockBuff.x;
  WinOrgEy := BlockBuff.y;
  if IsCliped then
  begin
    IsCliped := False;
    MadeClip := True;
    if TextContinue then
    begin
      FPage.GStateRestore;
    end
    else FPage.GStateRestore;
  end;
  TmpObj := GetCurrentObject(DContext, OBJ_PEN);
  GetObject(TmpObj, SizeOf(DCPen), @DCPen);
  if DCPen.lopnColor <> CurrentPen.lopnColor then
  begin
    CurrentPen.lopnColor := DCPen.lopnColor;
    SetPenColor;
  end;
  if DCPen.lopnStyle <> CurrentPen.lopnStyle then
  begin
    CurrentPen.lopnStyle := DCPen.lopnStyle;
    case CurrentPen.lopnStyle of
      ps_Solid, ps_InsideFrame: FPage.NoDash;
      ps_Dash: FPage.SetDash([8, 8], 0);
      ps_Dot: FPage.SetDash([2, 2], 0);
      ps_DashDot: FPage.SetDash([8, 2, 2, 2], 0);
      ps_DashDotDot: FPage.SetDash([8, 2, 2, 2, 2, 2], 0);
    end;
  end;
  if DCPen.lopnWidth.x * ZScaleX * ZoomX <> CurrentPen.lopnWidth then
  begin
    if DCPen.lopnWidth.x = 0 then CurrentPen.lopnWidth := ZoomX * ZScaleX
    else CurrentPen.lopnWidth := DCPen.lopnWidth.x * ZScaleX * ZoomX;
    FPage.SetLineWidth(PosiX * CurrentPen.lopnWidth);
  end;
  TmpObj := GetCurrentObject(DContext, OBJ_BRUSH);
  GetObject(TmpObj, SizeOf(DCBrush), @DCBrush);
  if DCBrush.lbColor <> CurrentBrush.lbColor then
  begin
    CurrentBrush.lbColor := DCBrush.lbColor;
    if not TextContinue then SetBrushColor;
  end;
  if DCBrush.lbStyle = 1 then IsNullBrush := True;
  TmpObj := GetCurrentObject(DContext, OBJ_FONT);
  GetObject(TmpObj, SizeOf(DCFont), @DCFont);
  if (CurrentFont.lfFaceName <> DCFont.lfFaceName) or
     (CurrentFont.lfWeight <> DCFont.lfWeight) or
     (CurrentFont.lfItalic <> DCFont.lfItalic) or
    (CurrentFont.lfUnderline <> DCFont.lfUnderline) or
    (CurrentFont.lfStrikeOut <> DCFont.lfStrikeOut) or
    (CurrentFont.lfCharSet <> DCFont.lfCharSet) or
    (CurrentFont.lfHeight <> DCFont.lfHeight) then
  begin
    Move(DCFont, CurrentFont, SizeOf(CurrentFont));
    MadeClip := True;
  end;
  I := GetTextAlign(DContext);
  case I and (TA_LEFT or ta_Right or ta_center) of
    ta_left: HorMode := vhjLeft;
    ta_right: HorMode := vhjRight;
    ta_center: HorMode := vhjCenter;
  end;
  case I and (TA_Top or ta_BaseLine or ta_Bottom) of
    TA_Top: VertMode := vvjUp;
    ta_Bottom: VertMode := vvjDown;
    TA_BASELINE: VertMode := vvjCenter;
  end;
  UpdatePos := (I and TA_UPDATECP = TA_UPDATECP);
end;

procedure TVPDFWmf.VEMRROUNDRECT(Data: PEMRRoundRect);
begin
  FPage.RoundRect(Round(ScaleX(Data^.rclBox.Left)),
    Round(ScaleY(Data^.rclBox.Top)),
    Round(ScaleX(Data^.rclBox.Right)), Round(ScaleY(Data^.rclBox.Bottom)),
    Round(ScaleX(Data^.szlCorner.cx)), Round(ScaleY(Data^.szlCorner.cy)));
  if not PathContinue then
    if (CurrentPen.lopnWidth <> 0) and (CurrentPen.lopnStyle <> ps_null) then
      if not IsNullBrush then FPage.FillAndStroke
      else FPage.Stroke
    else if not IsNullBrush then FPage.Fill
    else FPage.NewPath;
end;

procedure TVPDFWmf.VEMRSAVEDC;
begin
  SaveDC(DContext);
  if not WTransform then
  begin
    Inc(DCBufferLen);
    if (DCBufferLen >= 1) then
    begin
      SetLength(DCBuffer, DCBufferLen);
      DCBuffer[DCBufferLen - 1] := lpXForm;
    end;
  end;
end;

procedure TVPDFWmf.VEMRSELECTCLIPPATH;
begin
  if IsCliped then
  begin
    FPage.GStateRestore;
    FPage.SetLineWidth(PosiX * CurrentPen.lopnWidth);
    SetPenColor;
    SetBrushColor(False);
    MadeClip := True;
  end;
  FPage.GStateSave;
  IsCliped := True;
  FPage.Clip;
  InterSectClipRect := False;
  FPage.NewPath;
  PathContinue := False;
end;

procedure TVPDFWmf.VEMRSELECTOBJECT(Data: PEMRSelectObject);
var
  ObjPen: TLogPen;
  ObjBrush: TLogBrush;
  ObjFont: TLogFont;
  ObjType: DWORD;
begin
  if (Data^.ihObject and $80000000) = 0 then
  begin
    if Data^.ihObject >= GDIObjectsCount then Exit;
    SelectObject(DContext, GDIObjects[Data^.ihObject]);
    ObjType := GetObjectType(GDIObjects[Data^.ihObject]);
    case ObjType of
      OBJ_PEN:
        begin
          GetObject(GDIObjects[Data^.ihObject], SizeOf(ObjPen), @ObjPen);
          if ObjPen.lopnColor <> CurrentPen.lopnColor then
          begin
            CurrentPen.lopnColor := ObjPen.lopnColor;
            SetPenColor;
          end;
          if ObjPen.lopnStyle <> CurrentPen.lopnStyle then
          begin
            CurrentPen.lopnStyle := ObjPen.lopnStyle;
            case CurrentPen.lopnStyle of
              ps_Solid, ps_InsideFrame: FPage.NoDash;
              ps_Dash: FPage.SetDash([8, 8], 0);
              ps_Dot: FPage.SetDash([2, 2], 0);
              ps_DashDot: FPage.SetDash([8, 2, 2, 2], 0);
              ps_DashDotDot: FPage.SetDash([8, 2, 2, 2, 2, 2], 0);
            end;
          end;
          if ObjPen.lopnWidth.x * ZScaleX * ZoomX <> CurrentPen.lopnWidth then
          begin
            if ObjPen.lopnWidth.x = 0 then CurrentPen.lopnWidth := ZScaleX * ZoomX
            else CurrentPen.lopnWidth := ObjPen.lopnWidth.x * ZScaleX * ZoomX;
            FPage.SetLineWidth(PosiX * CurrentPen.lopnWidth);
          end;
        end;
      OBJ_BRUSH:
        begin
          IsNullBrush := False;
          GetObject(GDIObjects[Data^.ihObject], SizeOf(ObjBrush),
            @ObjBrush);
          if ObjBrush.lbColor <> CurrentBrush.lbColor then
          begin
            CurrentBrush.lbColor := ObjBrush.lbColor;
            if not TextContinue then SetBrushColor;
          end;
          if ObjBrush.lbStyle = 1 then IsNullBrush := True;
        end;
      OBJ_FONT:
        begin
          GetObject(GDIObjects[Data^.ihObject], SizeOf(ObjFont),
            @ObjFont);
          if (CurrentFont.lfFaceName <> ObjFont.lfFaceName) or
            (CurrentFont.lfWeight <> ObjFont.lfWeight) or
            (CurrentFont.lfItalic <> ObjFont.lfItalic) or
            (CurrentFont.lfUnderline <> ObjFont.lfUnderline) or
            (CurrentFont.lfStrikeOut <> ObjFont.lfStrikeOut) or
            (CurrentFont.lfCharSet <> ObjFont.lfCharSet) or
            (CurrentFont.lfHeight <> ObjFont.lfHeight) then
          begin
            Move(ObjFont, CurrentFont, SizeOf(CurrentFont));
            MadeClip := True;
          end
          else if (CurrentFont.lfEscapement <> ObjFont.lfEscapement) or
            (CurrentFont.lfOrientation <> ObjFont.lfOrientation) then
            Move(ObjFont, CurrentFont, SizeOf(CurrentFont));
        end;
    end;
  end
  else
  begin
    ObjType := Data^.ihObject and $7FFFFFFF;
    SelectObject(DContext, GetStockObject(ObjType));
    case ObjType of
      WHITE_BRUSH:
        begin
          IsNullBrush := False;
          CurrentBrush.lbColor := clWhite;
          if not TextContinue then SetBrushColor;
        end;
      LTGRAY_BRUSH:
        begin
          IsNullBrush := False;
          CurrentBrush.lbColor := $AAAAAA;
          if not TextContinue then SetBrushColor;
        end;
      GRAY_BRUSH:
        begin
          IsNullBrush := False;
          CurrentBrush.lbColor := $808080;
          if not TextContinue then SetBrushColor;
        end;
      DKGRAY_BRUSH:
        begin
          IsNullBrush := False;
          CurrentBrush.lbColor := $666666;
          if not TextContinue then SetBrushColor;
        end;
      BLACK_BRUSH:
        begin
          IsNullBrush := False;
          CurrentBrush.lbColor := 0;
          if not TextContinue then SetBrushColor;
        end;
      Null_BRUSH:
        begin
          CurrentBrush.lbColor := clWhite;
          IsNullBrush := True;
          if not TextContinue then SetBrushColor;
        end;
      WHITE_PEN:
        begin
          CurrentPen.lopnColor := clWhite;
          FPage.SetRGBStrokeColor(clWhite);
          CurrentPen.lopnWidth := ZScaleX * ZoomX;
          FPage.SetLineWidth(PosiX * CurrentPen.lopnWidth);
        end;
      BLACK_PEN:
        begin
          CurrentPen.lopnColor := clBlack;
          FPage.SetRGBStrokeColor(clBlack);
          CurrentPen.lopnWidth := ZScaleX * ZoomX;
          FPage.SetLineWidth(PosiX * CurrentPen.lopnWidth);
        end;
      Null_PEN:
        begin
          CurrentPen.lopnStyle := PS_NULL;
        end;
      OEM_FIXED_FONT, ANSI_FIXED_FONT, ANSI_VAR_FONT, SYSTEM_FONT:
        begin
          CurrentFont.lfFaceName := 'Arial';
          MadeClip := True;
        end;
    end;
  end;
end;

procedure TVPDFWmf.VEMRSETARCDIRECTION(Data: PEMRSetArcDirection);
begin
  IsCounterClockwise := Data^.iArcDirection = AD_COUNTERCLOCKWISE;
end;

procedure TVPDFWmf.VEMRSETBKCOLOR(Data: PEMRSetTextColor);
begin
  BKColor := Data^.crColor;
  SetBkColor(DContext, Data^.crColor);
end;

procedure TVPDFWmf.VEMRSETBKMODE(Data: PEMRSelectclippath);
begin
  BKMode := not (Data^.iMode = TRANSPARENT);
  SetBkMode(DContext, Data^.iMode);
end;

procedure TVPDFWmf.VEMRSETDIBITSTODEVICE(Data: PEMRSetDIBitsToDevice);
var
  I: Integer;
  BMBits: Pointer;
  BMTemp: TBitmap;
  BMInfo: PBitmapInfo;
begin
  BMInfo := Displace(Data, Data^.offBmiSrc);
  BMBits := Displace(Data, Data^.offBitsSrc);
  BMTemp := TBitmap.Create;
  try
    BMTemp.Width := Data^.cxSrc;
    BMTemp.Height := Data^.cySrc;
    SetDIBitsToDevice(BMTemp.Canvas.Handle, 0, 0, BMTemp.Width, BMTemp.Height, Data^.xSrc,
      Data^.ySrc, Data^.iStartScan, Data^.cScans, BMBits, BMInfo^, Data^.iUsageSrc);
    if BMTemp.PixelFormat = pf1bit then I := FPage.FParent.AddImage(BMTemp, icCCITT42)
    else I := FPage.FParent.AddImage(BMTemp, icFlate);
    FPage.ShowImage(I, ScaleX(Data^.rclBounds.Left, false), ScaleY(Data^.rclBounds.Top, False),
      PosiX * ZoomX * ZScaleX * data^.cxSrc, PosiY * ZoomY * ZScaleY * Data^.cySrc, 0);
  finally
    BMTemp.Free;
  end;
end;

procedure TVPDFWmf.VEMRSETPIXELV(Data: PEMRSetPixelV);
begin
  FPage.NewPath;
  if Data^.crColor <> CurrentPen.lopnColor then
    FPage.SetRGBStrokeColor(Data^.crColor);
  if CurrentPen.lopnWidth <> 1 then
    FPage.SetLineWidth(1);
  FPage.MoveTo(ScaleX(Data^.ptlPixel.x), ScaleY(Data^.ptlPixel.y));
  FPage.LineTo(ScaleX(Data^.ptlPixel.x) + 0.01, ScaleY(Data^.ptlPixel.y) + 0.01);
  StrokeOrPath;
  if CurrentPen.lopnWidth <> 1 then FPage.SetLineWidth(PosiX * CurrentPen.lopnWidth);
  if Data^.crColor <> CurrentPen.lopnColor then FPage.SetRGBStrokeColor(CurrentPen.lopnColor);
end;

procedure TVPDFWmf.VEMRSETPOLYFILLMODE(Data: PEMRSelectclippath);
begin
  PolyFIllMode := (Data^.iMode = ALTERNATE);
  SetPolyFillMode(DContext, Data^.iMode);
end;

procedure TVPDFWmf.VEMRSETSTRETCHBLTMODE(Data: PEMRSetStretchBltMode);
begin
  BLMode := Data^.iMode;
  SetStretchBltMode(DContext, data^.iMode);
end;

procedure TVPDFWmf.VEMRSETVIEWPORTEXTEX(Data: PEMRSetViewportExtEx);
begin
  ViewportExtEx := Data^.szlExtent.cx;
  ViewportExtEy := Data^.szlExtent.cy;
  SetViewportExtEx(DContext, Data^.szlExtent.cx, data^.szlExtent.cy, nil);
end;

procedure TVPDFWmf.VEMRSETVIEWPORTORGEX(Data: PEMRSetViewportOrgEx);
begin
  ViewportOrgEx := Data^.ptlOrigin.X;
  ViewportOrgEy := Data^.ptlOrigin.Y;
  SetViewportOrgEx(DContext, data^.ptlOrigin.X, data^.ptlOrigin.Y, nil);
end;

procedure TVPDFWmf.VEMRSETWINDOWEXTEX(Data: PEMRSetViewportExtEx);
begin
  WinExtEx := data^.szlExtent.cx;
  WinExtEy := Data^.szlExtent.cy;
  SetWindowExtEx(DContext, data^.szlExtent.cx, Data^.szlExtent.cy, nil);
end;

procedure TVPDFWmf.VEMRSETWINDOWORGEX(Data: PEMRSetViewportOrgEx);
begin
  WinOrgEx := Data^.ptlOrigin.X;
  WinOrgEy := Data^.ptlOrigin.Y;
  SetWindowOrgEx(DContext, Data^.ptlOrigin.X, Data^.ptlOrigin.Y, nil);
end;

procedure TVPDFWmf.VEMRSETWORLDTRANSFORM(Data: PEMRSetWorldTransform);
begin
  ZScaleX := Data^.xform.eM11;
  ZScaleY := Data^.xform.eM22;
  XOffset := Data^.xform.eDx * PosiX;
  YOffset := Data^.xform.eDy * PosiY;
  if WTransform then SetWorldTransform(DContext, Data^.xform)
  else Move(Data^.xform, lpXForm, SizeOf(lpXForm));
end;

procedure TVPDFWmf.VEMRSETSTRETCHBLT(Data: PEMRStretchBlt);
var
  I: Integer;
  BMTemp: TBitmap;
  BMInfo: PBitmapInfo;
  BMCanvas: TBitmap;
  BMBits: Pointer;
begin
  if (Data^.offBmiSrc > 0) and (Data^.offBitsSrc > 0) then
  begin
    BMInfo := Displace(Data, Data^.offBmiSrc);
    BMBits := Displace(Data, Data^.offBitsSrc);
    BMTemp := TBitmap.Create;
    try
      BMTemp.Width := BMInfo^.bmiHeader.biWidth;
      BMTemp.Height := BMInfo^.bmiHeader.biHeight;
      StretchDIBits(BMTemp.Canvas.Handle, 0, 0, BMTemp.Width, BMTemp.Height, 0, 0,
        BMTemp.Width, BMTemp.Height, BMBits, BMInfo^, Data^.iUsageSrc, SRCCOPY);
      BMCanvas := TBitmap.Create;
      try
        BMCanvas.Width := abs(Data^.cxDest);
        BMCanvas.Height := abs(Data^.cyDest);
        StretchBlt(BMCanvas.Canvas.Handle, 0, 0, BMCanvas.Width, BMCanvas.Height,
          BMTemp.Canvas.Handle, 0, 0, BMTemp.Width, BMTemp.Height, Data^.dwRop);
        if BMCanvas.PixelFormat = pf1bit then I := FPage.FParent.AddImage(BMCanvas, icCCITT42)
        else I := FPage.FParent.AddImage(BMCanvas, icFlate);
        FPage.ShowImage(I, ScaleX(Data^.rclBounds.Left, False),
          ScaleY(Data^.rclBounds.Top, False),
          ScaleX(Data^.rclBounds.Right - Data^.rclBounds.Left, False),
          ScaleY(Data^.rclBounds.Bottom - Data^.rclBounds.Top, False),
          0);
      finally
        BMCanvas.Free;
      end;
    finally
      BMTemp.Free;
    end;
  end
  else
  begin
    if (data^.cxDest = 0) or (data^.cyDest = 0) then FPage.NewPath
    else
    begin
      FPage.MFRectangle(ScaleX(data^.xDest), ScaleY(Data^.yDest),
        ScaleX(data^.xDest + Data^.cxDest), ScaleY(Data^.yDest + Data^.cyDest));
      FPage.Fill;
    end;
  end;
end;

procedure TVPDFWmf.VEMRSTRETCHDIBITS(Data: PEMRStretchDiBits);
var
  I: Integer;
  BMWidth: Integer;
  BMHeight: Integer;
  BMObj: HGDIOBJ;
  PrevBMObj: HGDIOBJ;
  BMTemp: TBitmap;
  BMBits: Pointer;
  BMInfo: PBitmapInfo;
  DContext, ScreenDC: HDC;
begin
  BMInfo := Displace(Data, Data^.offBmiSrc);
  BMBits := Displace(Data, Data^.offBitsSrc);
  if BMInfo^.bmiHeader.biBitCount = 1 then
  begin
    BMObj := CreateBitmap(BMInfo^.bmiHeader.biWidth, BMInfo^.bmiHeader.biHeight, 1, 1, nil);
    ScreenDC := GetDC(0);
    DContext := CreateCompatibleDC(ScreenDC);
    PrevBMObj := SelectObject(DContext, BMObj);
    try
      StretchDIBits(DContext, 0, 0, BMInfo^.bmiHeader.biWidth,
        BMInfo^.bmiHeader.biHeight,
        Data^.xSrc, Data^.ySrc, Data^.cxDest, Data^.cyDest, BMBits, BMInfo^,
        Data^.iUsageSrc, Data^.dwRop);
      BMTemp := TBitmap.Create;
      try
        BMTemp.Handle := BMObj;
        I := FPage.FParent.AddImage(BMTemp, icCCITT42);
        if (Data^.rclBounds.Right - Data^.rclBounds.Left > 0) and
          (Data^.rclBounds.Bottom - Data^.rclBounds.Top > 0) then
          FPage.ShowImage(I, ScaleX(Data^.rclBounds.Left, false),
            ScaleY(Data^.rclBounds.Top, false),
            ScaleX(Data^.rclBounds.Right - Data^.rclBounds.Left,
            False), ScaleY(Data^.rclBounds.Bottom - Data^.rclBounds.Top,
            False), 0)
        else
          FPage.ShowImage(I, ScaleX(Data^.xDest), ScaleY(Data^.yDest),
            ScaleX(data^.cxDest, False), ScaleY(Data^.cyDest,
            False), 0);
        BMTemp.ReleaseHandle;
      finally
        BMTemp.Free;
      end;
    finally
      SelectObject(DContext, PrevBMObj);
      DeleteDC(DContext);
      ReleaseDC(0, ScreenDC);
    end;
  end
  else
  begin
    BMTemp := TBitmap.Create;
    try
      BMTemp.Width := Data^.cxSrc;
      BMTemp.Height := Data^.cySrc;
      StretchDIBits(BMTemp.Canvas.Handle, 0, 0, BMTemp.Width, BMTemp.Height, Data^.xSrc,
        Data^.ySrc,
        BMTemp.Width, BMTemp.Height, BMBits, BMInfo^, Data^.iUsageSrc, Data^.dwRop);
      if BMInfo^.bmiHeader.biBitCount = 1 then
      begin
        BMTemp.PixelFormat := pf1bit;
        I := FPage.FParent.AddImage(BMTemp, icCCITT42)
      end
      else
        I := FPage.FParent.AddImage(BMTemp, icFlate);
      BMWidth := Data^.rclBounds.Right - Data^.rclBounds.Left;
      BMHeight := Data^.rclBounds.Bottom - Data^.rclBounds.Top;
      if (BMWidth > 0) and (BMHeight > 0) then
        FPage.ShowImage(I, ScaleX(Data^.rclBounds.Left, false),
          ScaleY(Data^.rclBounds.Top, false),
          ScaleX(BMWidth, False), ScaleY(BMHeight, False), 0)
      else
        FPage.ShowImage(I, ScaleX(Data^.xDest), ScaleY(Data^.yDest),
          PosiX * ZoomX *
          ZScaleX * data^.cxDest,
          PosiY * ZoomY * ZScaleY * Data^.cyDest, 0);
    finally
      BMTemp.Free;
    end;
  end;
end;

procedure TVPDFWmf.Analyse(MF: TMetafile);
var
  I: Integer;
  DCont: HDC;
  CrFontObj: HGDIOBJ;
  Header: EnhMetaHeader;

  function EnumEMFRecordsProc(DC: HDC; HandleTable: PHandleTable; EMFRecord: PEnhMetaRecord;
                              ObjectCount: Integer; OptionData: TVPDFWmf): Bool; stdcall;
  begin
    OptionData.ExecuteRecord(EMFRecord);
    Result := True;
  end;

begin
  MetaHandle := MF.Handle;
  GetEnhMetaFileHeader(MetaHandle, SizeOf(Header), @Header);
  GDIObjectsCount := Header.nHandles;
  SetLength(GDIObjects, GDIObjectsCount);
  for I := 0 to GDIObjectsCount - 1 do
  begin
    GDIObjects[i] := $FFFFFFFF;
  end;
  Meta.Clear;
  DContext := MetaCanvas.Handle;
  SetGraphicsMode(DContext, GM_ADVANCED);
  lpXForm.eM11 := 1;
  lpXForm.eM12 := 0;
  lpXForm.eM21 := 0;
  lpXForm.eM22 := 1;
  lpXForm.eDx := 0;
  lpXForm.eDy := 0;
  WTransform := SetWorldTransform(DContext, lpXForm);
  CrFontObj := GetCurrentObject(DContext, OBJ_FONT);
  GetObject(CrFontObj, sizeof(CurrentFont), @CurrentFont);
  ZScaleX := 1;
  ZScaleY := 1;
  XOffset := 0;
  YOffset := 0;
  DCBufferLen := 0;
  DCBuffer := nil;
  FPage.SetHorizontalScaling(100);
  FPage.SetCharacterSpacing(0);
  FPage.SetWordSpacing(0);
  BKMode := True;
  PolyFIllMode := True;
  VertMode := vvjUp;
  HorMode := vhjLeft;
  XOffset := 0;
  YOffset := 0;
  ZScaleX := 1;
  ZScaleY := 1;
  IsCliped := False;
  UpdatePos := False;
  WinExtEx := 1;
  WinExtEy := 1;
  FontScale := 1;
  MadeClip := True;
  ViewportExtEx := 1;
  ViewportExtEy := 1;
  IsNullBrush := False;
  PathContinue := False;
  IsCounterClockwise := True;
  ProjectMode := 1;
  CurrentFill := 0;
  CurrentVal.X := 0;
  CurrentVal.Y := 0;
  DCont := GetDC(0);
  BKColor := GetBKColor(DCont);
  ReleaseDC(0, DCont);
  EnumEnhMetafile(0, MetaHandle, @EnumEMFRecordsProc, self, Rect(0, 0, 0, 0));
  for I := 1 to GDIObjectsCount - 1 do
  begin
    DeleteObject(GDIObjects[I]);
  end;
  GDIObjects := nil;
  DCBuffer := nil;
  FPage.TopTextPosition := true;
  FPage.SetHorizontalScaling(100);
  FPage.SetCharacterSpacing(0);
  FPage.SetWordSpacing(0);
end;

function TVPDFWmf.ScaleX(Value: Single; Proj: Boolean = True): Single;
begin
  if Proj then Result := XOffset + ZScaleX * ProjectX(Value) * PosiX
  else Result := Value * PosiX;
end;

function TVPDFWmf.ScaleY(Value: Single; Proj: Boolean = True): Single;
begin
  if Proj then Result := YOffset + ZScaleY * ProjectY(Value) * PosiY
  else Result := Value * PosiY;
end;

procedure TVPDFWmf.ActivateCurrentFont;
var
  IsTT: Boolean;
  BMTemp: TBitmap;
  FntName: AnsiString;
  LogFont: TLogFont;
  FntSize: Extended;
  SecFntName: AnsiString;
  FontHandle: THandle;
  TxtMetric: TEXTMETRIC;
  FntStyle: TFontStyles;
  PrevWidth: Extended;
  LastWidth: Extended;
begin
  if not MadeClip then
    Exit;
  MadeClip := False;
  FntStyle := [];
  if CurrentFont.lfWeight >= 600 then FntStyle := FntStyle + [fsBold];
  if CurrentFont.lfItalic <> 0 then FntStyle := FntStyle + [fsItalic];
  if CurrentFont.lfStrikeOut <> 0 then FntStyle := FntStyle + [fsStrikeOut];
  if CurrentFont.lfUnderline <> 0 then FntStyle := FntStyle + [fsUnderline];
  IsTT := False;
  FntName := AnsiString(UpperCase(CurrentFont.lfFaceName));
  if FntName = 'ZAPFDINGBATS' then IsTT := False;
  if (FntName = 'HELVETICA') and (CurrentFont.lfCharSet <> 0) then IsTT := True;
  if FntName = 'SYMBOL' then IsTT := False;
  if FntName = 'WINGDINGS' then
  begin
    WTypeFont := True;
    CurrentFont.lfCharset := 2;
  end
  else WTypeFont := False;
  if CurrentFont.lfHeight < 0 then
    FntSize := MulDiv(abs(CurrentFont.lfHeight), 72,
      GetDeviceCaps(FPage.FParent.FCHandle, LOGPIXELSY)) * abs(ZScaleY)
  else
  begin
    GetTextMetrics(DContext, TxtMetric);
    FntSize := MulDiv(TxtMetric.tmHeight - TxtMetric.tmInternalLeading, 72,
      GetDeviceCaps(FPage.FParent.FCHandle, LOGPIXELSY)) * abs(ZScaleY)
  end;
  if not IsTT then FPage.SetFont(AnsiString(CurrentFont.lfFaceName), FntStyle, FntSize * ZoomY, 0)
  else
  begin
    if FntName = 'COURIER' then
      FPage.SetFont('Courier New', FntStyle, FntSize * ZoomY, CurrentFont.lfCharSet)
    else if FntName = 'TIMES' then
      FPage.SetFont('Times New Roman', FntStyle, FntSize * ZoomY,
        CurrentFont.lfCharSet)
    else if SecFntName <> '' then
      FPage.SetFont(SecFntName, FntStyle, FntSize * ZoomY, CurrentFont.lfCharSet)
    else
      FPage.SetFont('Arial', FntStyle, FntSize * ZoomY, 0);
  end;
  if CurrentFont.lfWidth = 0 then FPage.SetHorizontalScaling(100)
  else
  begin
    BMTemp := TBitmap.Create;
    try
      Move(CurrentFont, LogFont, SizeOf(LogFont));
      FontHandle := CreateFontIndirect(LogFont);
      SelectObject(BMTemp.Canvas.Handle, FontHandle);
      PrevWidth := BMTemp.Canvas.TextWidth('X');
      DeleteObject(FontHandle);
      LogFont.lfWidth := 0;
      FontHandle := CreateFontIndirect(LogFont);
      SelectObject(BMTemp.Canvas.Handle, FontHandle);
      LastWidth := BMTemp.Canvas.TextWidth('X');
      DeleteObject(FontHandle);
      FPage.SetHorizontalScaling(PrevWidth / LastWidth * 100);
    finally
      BMTemp.Free;
    end;
  end;
end;

procedure TVPDFWmf.SetPathContinue(const Value: Boolean);
begin
  FPathContinue := Value;
  if PathContinue then FPage.MoveTo(ScaleX(CurrentVal.X), ScaleY(CurrentVal.Y));
end;

procedure TVPDFWmf.FSOrPath;
begin
  if not IsNullBrush then
  begin
    if CurrentPen.lopnStyle <> ps_null then
      if PolyFIllMode then FPage.EoFillAndStroke
      else FPage.FillAndStroke
    else if PolyFIllMode then FPage.EoFill
    else FPage.Fill
  end
  else StrokeOrPath;
end;

procedure TVPDFWmf.StrokeOrPath;
begin
  if CurrentPen.lopnStyle <> ps_null then FPage.Stroke
  else FPage.NewPath;
end;

procedure TVPDFWmf.SetTextContinue(const Value: Boolean);
begin
  if FTextContinue = Value then Exit;
  FTextContinue := Value;
  if not Value then
  begin
    SetBrushColor;
  end;
end;

procedure TVPDFWmf.SetBrushColor(IsPath: Boolean = True);
var
  TmpObj: HGDIOBJ;
  NumOfEnt: Integer;
  BrColor: Byte;
  PalEntries: array[0..19] of TPALETTEENTRY;
begin
  if (CurrentBrush.lbColor > $FFFFFF) and (CurrentBrush.lbColor - $FFFFFF < 21) then
  begin
    TmpObj := GetStockObject(DEFAULT_PALETTE);
    NumOfEnt := GetPaletteEntries(TmpObj, 0, 20, PalEntries);
    BrColor := CurrentBrush.lbColor and $FF;
    if NumOfEnt <> 0 then
      CurrentBrush.lbColor := RGB(PalEntries[BrColor].peRed,
        PalEntries[BrColor].peGreen, PalEntries[BrColor].peBlue)
  end;
  if (CurrentFill <> CurrentBrush.lbColor) or (not IsPath) then
  begin
    CurrentFill := CurrentBrush.lbColor;
    FPage.SetRGBFillColor(CurrentBrush.lbColor);
  end;
end;

procedure TVPDFWmf.SetFontColor;
begin
  if (CurrentFill <> TextColor) or (MadeClip) then
  begin
    CurrentFill := TextColor;
    FPage.SetRGBFillColor(TextColor);
  end;
end;

procedure TVPDFWmf.SetPenColor;
var
  TmpObj: HGDIOBJ;
  BrColor: Byte;
  NumOfEnt: Integer;
  PalEntries: array[0..19] of TPALETTEENTRY;
begin
  if (CurrentPen.lopnColor > $FFFFFF) and (CurrentPen.lopnColor - $FFFFFF < 21) then
  begin
    TmpObj := GetStockObject(DEFAULT_PALETTE);
    NumOfEnt := GetPaletteEntries(TmpObj, 0, 20, PalEntries);
    BrColor := CurrentPen.lopnColor and $FF;
    if NumOfEnt <> 0 then
      CurrentPen.lopnColor := RGB(PalEntries[BrColor].peRed, PalEntries[BrColor].peGreen, PalEntries[BrColor].peBlue)
  end;
  FPage.SetRGBStrokeColor(CurrentPen.lopnColor);
end;

procedure TVPDFWmf.MFSetBKColor;
begin
  if CurrentFill <> BKColor then
  begin
    CurrentFill := BKColor;
    FPage.SetRGBFillColor(BKColor);
  end;
end;

procedure TVPDFWmf.VEMRMODIFYWORLDTRANSFORM(Data: PEMRModifyWorldTransform);
var
  CurrXFrm: TXForm;

  function MergeForms(XFrm1, XFrm2: TXForm): TXForm;
  begin
    Result.eM11 := XFrm1.eM11 * XFrm2.eM11 + XFrm1.eM12 * XFrm2.eM21;
    Result.eM12 := XFrm1.eM11 * XFrm2.eM12 + XFrm1.eM12 * XFrm2.eM22;
    Result.eM21 := XFrm1.eM21 * XFrm2.eM11 + XFrm1.eM22 * XFrm2.eM21;
    Result.eM22 := XFrm1.eM21 * XFrm2.eM12 + XFrm1.eM22 * XFrm2.eM22;
    Result.eDx := XFrm1.eDx * XFrm2.eM11 + XFrm1.eDy * XFrm2.eM21 + XFrm2.eDx;
    Result.eDy := XFrm1.eDy * XFrm2.eM12 + XFrm1.eDy * XFrm2.eM22 + XFrm2.eDy;
  end;

begin
  if WTransform then
  begin
    ModifyWorldTransform(DContext, Data^.xform, Data^.iMode);
    GetWorldTransform(DContext, CurrXFrm);
    XOffset := CurrXFrm.eDx * PosiX;
    YOffset := CurrXFrm.eDy * PosiY;
    ZScaleX := CurrXFrm.eM11;
    ZScaleY := CurrXFrm.eM22;
  end
  else
    case Data^.iMode of
      2:
        begin
          lpXForm := MergeForms(Data^.xform, lpXForm);
          ZScaleX := lpXForm.eM11;
          ZScaleY := lpXForm.eM22;
          XOffset := lpXForm.eDx * PosiX;
          YOffset := lpXForm.eDy * PosiY;
        end;
      3:
        begin
          lpXForm := MergeForms(lpXForm, Data^.xform);
          ZScaleX := lpXForm.eM11;
          ZScaleY := lpXForm.eM22;
          XOffset := lpXForm.eDx * PosiX;
          YOffset := lpXForm.eDy * PosiY;
        end;
      4:
        begin
          ZScaleX := Data^.xform.eM11;
          ZScaleY := Data^.xform.eM22;
          XOffset := Data^.xform.eDx * PosiX;
          YOffset := Data^.xform.eDy * PosiY;
          Move(Data^.xform, lpXForm, SizeOf(lpXForm));
        end;
    end;
end;

procedure TVPDFWmf.VEMRSETMAPMODE(Data: PEMRSetMapMode);
begin
  ProjectMode := Data^.iMode;
  SetMapMode(DContext, Data^.iMode);
  MadeClip := True;
end;

procedure TVPDFWmf.ExecuteRecord(Data: PEnhMetaRecord);
begin
  if PathContinue and (Data^.iType in [EMR_EXTTEXTOUTA, EMR_EXTTEXTOUTW, EMR_SMALLTEXTOUT]) then Exit;
  if TextContinue then
    if not (Data^.iType in [EMR_EXTTEXTOUTA, EMR_EXTTEXTOUTW, EMR_SELECTOBJECT, EMR_BITBLT,
      EMR_CREATEBRUSHINDIRECT, EMR_CREATEPEN, EMR_SAVEDC, EMR_RESTOREDC, EMR_SETTEXTALIGN,
      EMR_SETBKMODE, EMR_EXTCREATEFONTINDIRECTW, EMR_SMALLTEXTOUT, EMR_DELETEOBJECT,
      EMR_SETTEXTCOLOR, EMR_MOVETOEX, EMR_SETBKCOLOR]) then TextContinue := False;
  if (Data^.iType in [EMR_EXTTEXTOUTA, EMR_EXTTEXTOUTW, EMR_SMALLTEXTOUT]) then
    if (not (TextContinue) )then TextContinue := True;
  case Data^.iType of
    EMR_SETWINDOWEXTEX: VEMRSETWINDOWEXTEX(PEMRSetViewportExtEx(Data));
    EMR_SETWINDOWORGEX: VEMRSETWINDOWORGEX(PEMRSetViewportOrgEx(Data));
    EMR_SETVIEWPORTEXTEX: VEMRSETVIEWPORTEXTEX(PEMRSetViewportExtEx(Data));
    EMR_SETVIEWPORTORGEX: VEMRSETVIEWPORTORGEX(PEMRSetViewportOrgEx(Data));
    EMR_SETMAPMODE: VEMRSETMAPMODE(PEMRSetMapMode(Data));
    EMR_POLYBEZIER: VEMRPOLYBEZIER(PEMRPolyline(Data));
    EMR_POLYGON: VEMRPOLYGON(PEMRPolyline(Data));
    EMR_POLYLINE: VEMRPOLYLINE(PEMRPolyline(Data));
    EMR_POLYBEZIERTO: VEMRPOLYBEZIERTO(PEMRPolyline(Data));
    EMR_POLYLINETO: VEMRPOLYLINETO(PEMRPolyline(Data));
    EMR_POLYPOLYLINE: VEMRPOLYPOLYLINE(PEMRPolyPolyline(Data));
    EMR_POLYPOLYGON: VEMRPOLYPOLYGON(PEMRPolyPolyline(Data));
    EMR_SETPIXELV: VEMRSETPIXELV(PEMRSetPixelV(Data));
    EMR_SETBKMODE: VEMRSETBKMODE(PEMRSelectclippath(Data));
    EMR_SETPOLYFILLMODE: VEMRSETPOLYFILLMODE(PEMRSelectclippath(Data));
    EMR_SETTEXTALIGN: VEMRSETTEXTALIGN(PEMRSelectclippath(Data));
    EMR_SETTEXTCOLOR: VEMRSETTEXTCOLOR(PEMRSetTextColor(Data));
    EMR_SETBKCOLOR: VEMRSETBKCOLOR(PEMRSetTextColor(Data));
    EMR_MOVETOEX: VEMRMOVETOEX(PEMRLineTo(Data));
    EMR_INTERSECTCLIPRECT: VEMRINTERSECTCLIPRECT(PEMRIntersectClipRect(Data));
    EMR_EXTSELECTCLIPRGN: VEMREXTSELECTCLIPRGN(PEMRExtSelectClipRgn(Data));
    EMR_SAVEDC: VEMRSAVEDC;
    EMR_RESTOREDC: VEMRRESTOREDC(PEMRRestoreDC(Data));
    EMR_SETWORLDTRANSFORM: VEMRSETWORLDTRANSFORM(PEMRSetWorldTransform(Data));
    EMR_MODIFYWORLDTRANSFORM: VEMRMODIFYWORLDTRANSFORM(PEMRModifyWorldTransform(Data));
    EMR_SELECTOBJECT: VEMRSELECTOBJECT(PEMRSelectObject(Data));
    EMR_CREATEPEN: VEMRCREATEPEN(PEMRCreatePen(Data));
    EMR_CREATEBRUSHINDIRECT: VEMRCREATEBRUSHINDIRECT(PEMRCreateBrushIndirect(Data));
    EMR_DELETEOBJECT: VEMRDELETEOBJECT(PEMRDeleteObject(Data));
    EMR_ANGLEARC: VEMRANGLEARC(PEMRAngleArc(Data));
    EMR_ELLIPSE: VEMRELLIPSE(PEMREllipse(Data));
    EMR_RECTANGLE: VEMRRECTANGLE(PEMREllipse(Data));
    EMR_ROUNDRECT: VEMRROUNDRECT(PEMRRoundRect(Data));
    EMR_FILLRGN: VEMRFILLRGN(PEMRFillRgn(Data));
    EMR_ARC: VEMRARC(PEMRArc(Data));
    EMR_CHORD: VEMRCHORD(PEMRChord(Data));
    EMR_PIE: VEMRPIE(PEMRPie(Data));
    EMR_LINETO: VEMRLINETO(PEMRLineTo(Data));
    EMR_ARCTO: VEMRARCTO(PEMRArc(Data));
    EMR_POLYDRAW: VEMRPOLYDRAW(PEMRPolyDraw(Data));
    EMR_SETARCDIRECTION: VEMRSETARCDIRECTION(PEMRSetArcDirection(Data));
    EMR_BEGINPATH: VEMRBEGINPATH;
    EMR_ENDPATH: VEMRENDPATH;
    EMR_CLOSEFIGURE: VEMRCLOSEFIGURE;
    EMR_FILLPATH: VEMRFILLPATH;
    EMR_STROKEANDFILLPATH: VEMRSTROKEANDFILLPATH;
    EMR_STROKEPATH: VEMRSTROKEPATH;
    EMR_SELECTCLIPPATH: VEMRSELECTCLIPPATH;
    EMR_ABORTPATH: VEMRABORTPATH;
    EMR_SETDIBITSTODEVICE: VEMRSETDIBITSTODEVICE(PEMRSetDIBitsToDevice(Data));
    EMR_STRETCHDIBITS: VEMRSTRETCHDIBITS(PEMRStretchDiBits(Data));
    EMR_EXTCREATEFONTINDIRECTW: VEMREXTCREATEFONTINDIRECTW(PEMRExtCreateFontIndirect(Data));
    EMR_EXTTEXTOUTA, EMR_EXTTEXTOUTW: VEMREXTTEXTOUT(PEMRExtTextOut(Data));
    EMR_POLYBEZIER16: VEMRPOLYBEZIER16(PEMRPolyline16(Data));
    EMR_POLYGON16: VEMRPOLYGON16(PEMRPolyline16(Data));
    EMR_POLYLINE16: VEMRPOLYLINE16(PEMRPolyline16(Data));
    EMR_POLYBEZIERTO16: VEMRPOLYBEZIERTO16(PEMRPolyline16(Data));
    EMR_POLYLINETO16: VEMRPOLYLINETO16(PEMRPolyline16(Data));
    EMR_POLYPOLYLINE16: VEMRPOLYPOLYLINE16(PEMRPolyPolyline16(Data));
    EMR_POLYPOLYGON16: VEMRPOLYPOLYGON16(PEMRPolyPolyline16(Data));
    EMR_POLYDRAW16: VEMRPOLYDRAW16(PEMRPolyDraw16(Data));
    EMR_EXTCREATEPEN: VEMREXTCREATEPEN(PEMRExtCreatePen(Data));
    EMR_BITBLT: VEMRBITBLT(PEMRBitBlt(Data));
    EMR_SETSTRETCHBLTMODE: VEMRSETSTRETCHBLTMODE(PEMRSetStretchBltMode(Data));
    EMR_STRETCHBLT: VEMRSETSTRETCHBLT(PEMRStretchBlt(Data));
    EMR_SMALLTEXTOUT: VEMRSMALLTEXTOUT(PEMRSMALLTEXTOUTA(Data));
    EMR_ALPHABLEND: VEMRALPHABLEND(PEMRAlphaBlend(Data));
  end;
end;

function TVPDFWmf.ProjectX(Value: Single): Single;
begin
  case ProjectMode of
    MM_ANISOTROPIC: Result := ((Value - WinOrgEx) * ViewportExtEx / WinExtEx) + ViewportOrgEx;
    MM_ISOTROPIC: Result := ((Value - WinOrgEx) * ViewportExtEx / WinExtEx) + ViewportOrgEx;
  else
    Result := Value + ViewportOrgEx;
  end;
end;

function TVPDFWmf.ProjectY(Value: Single): Single;
begin
  case ProjectMode of
    MM_ANISOTROPIC: Result := ((Value - WinOrgEy) * ViewportExtEy / WinExtEy) + ViewportOrgEy;
    MM_ISOTROPIC: Result := ((Value - WinOrgEy) * ViewportExtEy / WinExtEy) + ViewportOrgEy;
  else
    Result := Value + ViewportOrgEy;
  end;
end;

function TVPDFWmf.ZoomX: Single;
begin
  if ProjectMode = 1 then Result := 1
  else Result := ABS(ViewportExtEx / WinExtEx);
end;

function TVPDFWmf.ZoomY: Single;
begin
  if ProjectMode = 1 then Result := 1
  else Result := ABS(ViewportExtEy / WinExtEy);
end;

function TVPDFWmf.Displace(Buffer: Pointer; Offset: Integer): Pointer;
var
  ABuffer: PByte;
begin
  ABuffer := Buffer;
  Inc(ABuffer, Offset);
  Result := Pointer(ABuffer);
end;

end.

