{*******************************************************}
{                                                       }
{       This unit is part of the VISPDF VCL library.    }
{       Written by R.Husske - ALL RIGHTS RESERVED.      }
{                                                       }
{       Copyright (C) 2000-2009, www.vispdf.com         }
{                                                       }
{       e-mail: support@vispdf.com                      }
{       http://www.vispdf.com                           }
{                                                       }
{*******************************************************}

unit VPDFTiff;

interface

uses
  Windows, SysUtils, Classes;

{$I VisPDFLib.inc }

const
  TIFF_NOTYPE = 0;
  TIFF_BYTE = 1;
  TIFF_ASCII = 2;
  TIFF_SHORT = 3;
  TIFF_LONG = 4;
  TIFF_RATIONAL = 5;
  TIFF_SBYTE = 6;
  TIFF_UNDEFINED = 7;
  TIFF_SSHORT = 8;
  TIFF_SLONG = 9;
  TIFF_SRATIONAL = 10;
  TIFF_FLOAT = 11;
  TIFF_DOUBLE = 12;
  TIFF_IFD = 13;
  TIFF_UNICODE = 14;
  TIFF_COMPLEX = 15;
  TIFF_LONG8 = 16;
  TIFF_SLONG8 = 17;
  TIFF_IFD8 = 18;
  TIFFTAG_SUBFILETYPE = 254;
  FILETYPE_REDUCEDIMAGE = $1;
  FILETYPE_PAGE = $2;
  FILETYPE_MASK = $4;
  TIFFTAG_OSUBFILETYPE = 255;
  OFILETYPE_IMAGE = 1;
  OFILETYPE_REDUCEDIMAGE = 2;
  OFILETYPE_PAGE = 3;
  TIFFTAG_IMAGEWIDTH = 256;
  TIFFTAG_IMAGELENGTH = 257;
  TIFFTAG_BITSPERSAMPLE = 258;
  TIFFTAG_COMPRESSION = 259;
  COMPRESSION_NONE = 1;
  COMPRESSION_CCITTRLE = 2;
  COMPRESSION_CCITTFAX3 = 3;
  COMPRESSION_CCITT_T4 = 3;
  COMPRESSION_CCITTFAX4 = 4;
  COMPRESSION_CCITT_T6 = 4;
  COMPRESSION_LZW = 5;
  COMPRESSION_OJPEG = 6;
  COMPRESSION_JPEG = 7;
  COMPRESSION_NEXT = 32766;
  COMPRESSION_CCITTRLEW = 32771;
  COMPRESSION_PACKBITS = 32773;
  COMPRESSION_THUNDERSCAN = 32809;
  COMPRESSION_IT8CTPAD = 32895;
  COMPRESSION_IT8LW = 32896;
  COMPRESSION_IT8MP = 32897;
  COMPRESSION_IT8BL = 32898;
  COMPRESSION_PIXARFILM = 32908;
  COMPRESSION_PIXARLOG = 32909;
  COMPRESSION_DEFLATE = 32946;
  COMPRESSION_ADOBE_DEFLATE = 8;
  COMPRESSION_DCS = 32947;
  COMPRESSION_JBIG = 34661;
  COMPRESSION_SGILOG = 34676;
  COMPRESSION_SGILOG24 = 34677;
  COMPRESSION_JP2000 = 34712;
  TIFFTAG_PHOTOMETRIC = 262;
  PHOTOMETRIC_MINISWHITE = 0;
  PHOTOMETRIC_MINISBLACK = 1;
  PHOTOMETRIC_RGB = 2;
  PHOTOMETRIC_PALETTE = 3;
  PHOTOMETRIC_MASK = 4;
  PHOTOMETRIC_SEPARATED = 5;
  PHOTOMETRIC_YCBCR = 6;
  PHOTOMETRIC_CIELAB = 8;
  PHOTOMETRIC_ICCLAB = 9;
  PHOTOMETRIC_ITULAB = 10;
  PHOTOMETRIC_LOGL = 32844;
  PHOTOMETRIC_LOGLUV = 32845;
  TIFFTAG_THRESHHOLDING = 263;
  THRESHHOLD_BILEVEL = 1;
  THRESHHOLD_HALFTONE = 2;
  THRESHHOLD_ERRORDIFFUSE = 3;
  TIFFTAG_CELLWIDTH = 264;
  TIFFTAG_CELLLENGTH = 265;
  TIFFTAG_FILLORDER = 266;
  FILLORDER_MSB2LSB = 1;
  FILLORDER_LSB2MSB = 2;
  TIFFTAG_DOCUMENTNAME = 269;
  TIFFTAG_IMAGEDESCRIPTION = 270;
  TIFFTAG_MAKE = 271;
  TIFFTAG_MODEL = 272;
  TIFFTAG_STRIPOFFSETS = 273;
  TIFFTAG_ORIENTATION = 274;
  ORIENTATION_TOPLEFT = 1;
  ORIENTATION_TOPRIGHT = 2;
  ORIENTATION_BOTRIGHT = 3;
  ORIENTATION_BOTLEFT = 4;
  ORIENTATION_LEFTTOP = 5;
  ORIENTATION_RIGHTTOP = 6;
  ORIENTATION_RIGHTBOT = 7;
  ORIENTATION_LEFTBOT = 8;
  TIFFTAG_SAMPLESPERPIXEL = 277;
  TIFFTAG_ROWSPERSTRIP = 278;
  TIFFTAG_STRIPBYTECOUNTS = 279;
  TIFFTAG_MINSAMPLEVALUE = 280;
  TIFFTAG_MAXSAMPLEVALUE = 281;
  TIFFTAG_XRESOLUTION = 282;
  TIFFTAG_YRESOLUTION = 283;
  TIFFTAG_PLANARCONFIG = 284;
  PLANARCONFIG_CONTIG = 1;
  PLANARCONFIG_SEPARATE = 2;
  TIFFTAG_PAGENAME = 285;
  TIFFTAG_XPOSITION = 286;
  TIFFTAG_YPOSITION = 287;
  TIFFTAG_FREEOFFSETS = 288;
  TIFFTAG_FREEBYTECOUNTS = 289;
  TIFFTAG_GRAYRESPONSEUNIT = 290;
  GRAYRESPONSEUNIT_10S = 1;
  GRAYRESPONSEUNIT_100S = 2;
  GRAYRESPONSEUNIT_1000S = 3;
  GRAYRESPONSEUNIT_10000S = 4;
  GRAYRESPONSEUNIT_100000S = 5;
  TIFFTAG_GRAYRESPONSECURVE = 291;
  TIFFTAG_GROUP3OPTIONS = 292;
  TIFFTAG_T4OPTIONS = 292;
  GROUP3OPT_2DENCODING = $1;
  GROUP3OPT_UNCOMPRESSED = $2;
  GROUP3OPT_FILLBITS = $4;
  TIFFTAG_GROUP4OPTIONS = 293;
  TIFFTAG_T6OPTIONS = 293;
  GROUP4OPT_UNCOMPRESSED = $2;
  TIFFTAG_RESOLUTIONUNIT = 296;
  RESUNIT_NONE = 1;
  RESUNIT_INCH = 2;
  RESUNIT_CENTIMETER = 3;
  TIFFTAG_PAGENUMBER = 297;
  TIFFTAG_COLORRESPONSEUNIT = 300;
  COLORRESPONSEUNIT_10S = 1;
  COLORRESPONSEUNIT_100S = 2;
  COLORRESPONSEUNIT_1000S = 3;
  COLORRESPONSEUNIT_10000S = 4;
  COLORRESPONSEUNIT_100000S = 5;
  TIFFTAG_TRANSFERFUNCTION = 301;
  TIFFTAG_SOFTWARE = 305;
  TIFFTAG_DATETIME = 306;
  TIFFTAG_ARTIST = 315;
  TIFFTAG_HOSTCOMPUTER = 316;
  TIFFTAG_PREDICTOR = 317;
  TIFFTAG_WHITEPOINT = 318;
  TIFFTAG_PRIMARYCHROMATICITIES = 319;
  TIFFTAG_COLORMAP = 320;
  TIFFTAG_HALFTONEHINTS = 321;
  TIFFTAG_TILEWIDTH = 322;
  TIFFTAG_TILELENGTH = 323;
  TIFFTAG_TILEOFFSETS = 324;
  TIFFTAG_TILEBYTECOUNTS = 325;
  TIFFTAG_BADFAXLINES = 326;
  TIFFTAG_CLEANFAXDATA = 327;
  CLEANFAXDATA_CLEAN = 0;
  CLEANFAXDATA_REGENERATED = 1;
  CLEANFAXDATA_UNCLEAN = 2;
  TIFFTAG_CONSECUTIVEBADFAXLINES = 328;
  TIFFTAG_SUBIFD = 330;
  TIFFTAG_INKSET = 332;
  INKSET_CMYK = 1;
  INKSET_MULTIINK = 2;
  TIFFTAG_INKNAMES = 333;
  TIFFTAG_NUMBEROFINKS = 334;
  TIFFTAG_DOTRANGE = 336;
  TIFFTAG_TARGETPRINTER = 337;
  TIFFTAG_EXTRASAMPLES = 338;
  EXTRASAMPLE_UNSPECIFIED = 0;
  EXTRASAMPLE_ASSOCALPHA = 1;
  EXTRASAMPLE_UNASSALPHA = 2;
  TIFFTAG_SAMPLEFORMAT = 339;
  SAMPLEFORMAT_UINT = 1;
  SAMPLEFORMAT_INT = 2;
  SAMPLEFORMAT_IEEEFP = 3;
  SAMPLEFORMAT_VOID = 4;
  SAMPLEFORMAT_COMPLEXINT = 5;
  SAMPLEFORMAT_COMPLEXIEEEFP = 6;
  TIFFTAG_SMINSAMPLEVALUE = 340;
  TIFFTAG_SMAXSAMPLEVALUE = 341;
  TIFFTAG_CLIPPATH = 343;
  TIFFTAG_XCLIPPATHUNITS = 344;
  TIFFTAG_YCLIPPATHUNITS = 345;
  TIFFTAG_INDEXED = 346;
  TIFFTAG_JPEGTABLES = 347;
  TIFFTAG_OPIPROXY = 351;
  TIFFTAG_JPEGPROC = 512;
  JPEGPROC_BASELINE = 1;
  JPEGPROC_LOSSLESS = 14;
  TIFFTAG_JPEGIFOFFSET = 513;
  TIFFTAG_JPEGIFBYTECOUNT = 514;
  TIFFTAG_JPEGRESTARTINTERVAL = 515;
  TIFFTAG_JPEGLOSSLESSPREDICTORS = 517;
  TIFFTAG_JPEGPOINTTRANSFORM = 518;
  TIFFTAG_JPEGQTABLES = 519;
  TIFFTAG_JPEGDCTABLES = 520;
  TIFFTAG_JPEGACTABLES = 521;
  TIFFTAG_YCBCRCOEFFICIENTS = 529;
  TIFFTAG_YCBCRSUBSAMPLING = 530;
  TIFFTAG_YCBCRPOSITIONING = 531;
  YCBCRPOSITION_CENTERED = 1;
  YCBCRPOSITION_COSITED = 2;
  TIFFTAG_REFERENCEBLACKWHITE = 532;
  TIFFTAG_XMLPACKET = 700;
  TIFFTAG_OPIIMAGEID = 32781;
  TIFFTAG_REFPTS = 32953;
  TIFFTAG_REGIONTACKPOINT = 32954;
  TIFFTAG_REGIONWARPCORNERS = 32955;
  TIFFTAG_REGIONAFFINE = 32956;
  TIFFTAG_MATTEING = 32995;
  TIFFTAG_DATATYPE = 32996;
  TIFFTAG_IMAGEDEPTH = 32997;
  TIFFTAG_TILEDEPTH = 32998;
  TIFFTAG_PIXAR_IMAGEFULLWIDTH = 33300;
  TIFFTAG_PIXAR_IMAGEFULLLENGTH = 33301;
  TIFFTAG_PIXAR_TEXTUREFORMAT = 33302;
  TIFFTAG_PIXAR_WRAPMODES = 33303;
  TIFFTAG_PIXAR_FOVCOT = 33304;
  TIFFTAG_PIXAR_MATRIX_WORLDTOSCREEN = 33305;
  TIFFTAG_PIXAR_MATRIX_WORLDTOCAMERA = 33306;
  TIFFTAG_WRITERSERIALNUMBER = 33405;
  TIFFTAG_COPYRIGHT = 33432;
  TIFFTAG_RICHTIFFIPTC = 33723;
  TIFFTAG_IT8SITE = 34016;
  TIFFTAG_IT8COLORSEQUENCE = 34017;
  TIFFTAG_IT8HEADER = 34018;
  TIFFTAG_IT8RASTERPADDING = 34019;
  TIFFTAG_IT8BITSPERRUNLENGTH = 34020;
  TIFFTAG_IT8BITSPEREXTENDEDRUNLENGTH = 34021;
  TIFFTAG_IT8COLORTABLE = 34022;
  TIFFTAG_IT8IMAGECOLORINDICATOR = 34023;
  TIFFTAG_IT8BKGCOLORINDICATOR = 34024;
  TIFFTAG_IT8IMAGECOLORVALUE = 34025;
  TIFFTAG_IT8BKGCOLORVALUE = 34026;
  TIFFTAG_IT8PIXELINTENSITYRANGE = 34027;
  TIFFTAG_IT8TRANSPARENCYINDICATOR = 34028;
  TIFFTAG_IT8COLORCHARACTERIZATION = 34029;
  TIFFTAG_IT8HCUSAGE = 34030;
  TIFFTAG_IT8TRAPINDICATOR = 34031;
  TIFFTAG_IT8CMYKEQUIVALENT = 34032;
  TIFFTAG_FRAMECOUNT = 34232;
  TIFFTAG_ICCPROFILE = 34675;
  TIFFTAG_PHOTOSHOP = 34377;
  TIFFTAG_JBIGOPTIONS = 34750;
  TIFFTAG_FAXRECVPARAMS = 34908;
  TIFFTAG_FAXSUBADDRESS = 34909;
  TIFFTAG_FAXRECVTIME = 34910;
  TIFFTAG_STONITS = 37439;
  TIFFTAG_FEDEX_EDR = 34929;
  TIFFTAG_DCSHUESHIFTVALUES = 65535;
  TIFFTAG_FAXMODE = 65536;
  FAXMODE_CLASSIC = $0;
  FAXMODE_NORTC = $1;
  FAXMODE_NOEOL = $2;
  FAXMODE_BYTEALIGN = $4;
  FAXMODE_WORDALIGN = $8;
  FAXMODE_CLASSF = FAXMODE_NORTC;
  TIFFTAG_JPEGQUALITY = 65537;
  TIFFTAG_JPEGCOLORMODE = 65538;
  JPEGCOLORMODE_RAW = $0;
  JPEGCOLORMODE_RGB = $1;
  TIFFTAG_JPEGTABLESMODE = 65539;
  JPEGTABLESMODE_QUANT = $1;
  JPEGTABLESMODE_HUFF = $2;
  TIFFTAG_FAXFILLFUNC = 65540;
  TIFFTAG_PIXARLOGDATAFMT = 65549;
  PIXARLOGDATAFMT_8BIT = 0;
  PIXARLOGDATAFMT_8BITABGR = 1;
  PIXARLOGDATAFMT_11BITLOG = 2;
  PIXARLOGDATAFMT_12BITPICIO = 3;
  PIXARLOGDATAFMT_16BIT = 4;
  PIXARLOGDATAFMT_FLOAT = 5;
  TIFFTAG_DCSIMAGERTYPE = 65550;
  DCSIMAGERMODEL_M3 = 0;
  DCSIMAGERMODEL_M5 = 1;
  DCSIMAGERMODEL_M6 = 2;
  DCSIMAGERFILTER_IR = 0;
  DCSIMAGERFILTER_MONO = 1;
  DCSIMAGERFILTER_CFA = 2;
  DCSIMAGERFILTER_OTHER = 3;
  TIFFTAG_DCSINTERPMODE = 65551;
  DCSINTERPMODE_NORMAL = 0;
  DCSINTERPMODE_PREVIEW = 1;
  TIFFTAG_DCSBALANCEARRAY = 65552;
  TIFFTAG_DCSCORRECTMATRIX = 65553;
  TIFFTAG_DCSGAMMA = 65554;
  TIFFTAG_DCSTOESHOULDERPTS = 65555;
  TIFFTAG_DCSCALIBRATIONFD = 65556;
  TIFFTAG_ZIPQUALITY = 65557;
  TIFFTAG_PIXARLOGQUALITY = 65558;
  TIFFTAG_DCSCLIPRECTANGLE = 65559;
  TIFFTAG_SGILOGDATAFMT = 65560;
  SGILOGDATAFMT_FLOAT = 0;
  SGILOGDATAFMT_16BIT = 1;
  SGILOGDATAFMT_RAW = 2;
  SGILOGDATAFMT_8BIT = 3;
  TIFFTAG_SGILOGENCODE = 65561;
  SGILOGENCODE_NODITHER = 0;
  SGILOGENCODE_RANDITHER = 1;
  TIFFPRINT_NONE = $0;
  TIFFPRINT_STRIPS = $1;
  TIFFPRINT_CURVES = $2;
  TIFFPRINT_COLORMAP = $4;
  TIFFPRINT_JPEGQTABLES = $100;
  TIFFPRINT_JPEGACTABLES = $200;
  TIFFPRINT_JPEGDCTABLES = $200;
  TIFF_ANY = TIFF_NOTYPE;
  TIFF_VARIABLE = -1;
  TIFF_SPP = -2;
  TIFF_VARIABLE2 = -3;
  FIELD_CUSTOM = 65;

type
{$IFDEF VOLDVERSION}
  PPointer = ^Pointer;
  PCardinal = ^Cardinal;
{$ENDIF}
  PTIFF = Pointer;
  PTIFFRGBAImage = Pointer;
  TIFFErrorHandler = procedure(a: Pointer; b: Pointer; c: Pointer); cdecl;
  LibTiffDelphiErrorHandler = procedure(const a, b: AnsiString);
  TIFFReadWriteProc = function(Fd: Cardinal; Buffer: Pointer; Size: Integer): Integer; cdecl;
  TIFFSeekProc = function(Fd: Cardinal; Off: Cardinal; Whence: Integer): Cardinal; cdecl;
  TIFFCloseProc = function(Fd: Cardinal): Integer; cdecl;
  TIFFSizeProc = function(Fd: Cardinal): Cardinal; cdecl;
  TIFFMapFileProc = function(Fd: Cardinal; PBase: PPointer; PSize: PCardinal): Integer; cdecl;
  TIFFUnmapFileProc = procedure(Fd: Cardinal; Base: Pointer; Size: Cardinal); cdecl;
  TIFFExtendProc = procedure(Handle: PTIFF); cdecl;
  TIFFInitMethod = function(Handle: PTIFF; Scheme: Integer): Integer; cdecl;
  PTIFFCodec = ^TIFFCodec;
  TIFFCodec = record
    Name: PAnsiChar;
    Scheme: Word;
    Init: TIFFInitMethod;
  end;
  PTIFFFieldInfo = ^TIFFFieldInfo;
  TIFFFieldInfo = record
    FieldTag: Cardinal;
    FieldReadCount: Smallint;
    FieldWriteCount: Smallint;
    FieldType: Integer;
    FieldBit: Word;
    FieldOkToChange: Byte;
    FieldPassCount: Byte;
    FieldName: PAnsiChar;
  end;
  PTIFFTagValue = ^TIFFTagValue;
  TIFFTagValue = record
    Info: PTIFFFieldInfo;
    Count: Integer;
    Value: Pointer;
  end;
function LibTiffDelphiVersion: AnsiString;
procedure TIFFReadRGBAImageSwapRB(Width, Height: Cardinal; Memory: Pointer); forward;
function TIFFGetVersion: PAnsiChar; cdecl; external;
function TIFFFindCODEC(Scheme: Word): PTIFFCodec; cdecl; external;
function TIFFRegisterCODEC(Scheme: Word; Name: PAnsiChar; InitMethod: TIFFInitMethod): PTIFFCodec; cdecl; external;
procedure TIFFUnRegisterCODEC(c: PTIFFCodec); cdecl; external;
function TIFFIsCODECConfigured(Scheme: Word): Integer; cdecl; external;
function TIFFGetConfiguredCODECs: PTIFFCodec; cdecl; external;
function TIFFOpen(const Name: AnsiString; const Mode: AnsiString): PTIFF;
function TIFFOpenStream(const Stream: TStream; const Mode: AnsiString): PTIFF;
function TIFFClientOpen(Name: PAnsiChar; Mode: PAnsiChar; ClientData: Cardinal;
  ReadProc: TIFFReadWriteProc;
  WriteProc: TIFFReadWriteProc;
  SeekProc: TIFFSeekProc;
  CloseProc: TIFFCloseProc;
  SizeProc: TIFFSizeProc;
  MapProc: TIFFMapFileProc;
  UnmapProc: TIFFUnmapFileProc): PTIFF; cdecl; external;
procedure TIFFCleanup(Handle: PTIFF); cdecl; external;
procedure TIFFClose(Handle: PTIFF); cdecl; external;
function TIFFFileno(Handle: PTIFF): Integer; cdecl; external;
function TIFFSetFileno(Handle: PTIFF; Newvalue: Integer): Integer; cdecl; external;
function TIFFClientdata(Handle: PTIFF): Cardinal; cdecl; external;
function TIFFSetClientdata(Handle: PTIFF; Newvalue: Cardinal): Cardinal; cdecl; external;
function TIFFGetMode(Handle: PTIFF): Integer; cdecl; external;
function TIFFSetMode(Handle: PTIFF; Mode: Integer): Integer; cdecl; external;
function TIFFFileName(Handle: PTIFF): Pointer; cdecl; external;
function TIFFSetFileName(Handle: PTIFF; Name: PAnsiChar): PAnsiChar; cdecl; external;
function TIFFGetReadProc(Handle: PTIFF): TIFFReadWriteProc; cdecl; external;
function TIFFGetWriteProc(Handle: PTIFF): TIFFReadWriteProc; cdecl; external;
function TIFFGetSeekProc(Handle: PTIFF): TIFFSeekProc; cdecl; external;
function TIFFGetCloseProc(Handle: PTIFF): TIFFCloseProc; cdecl; external;
function TIFFGetSizeProc(Handle: PTIFF): TIFFSizeProc; cdecl; external;
{$IFNDEF VOLDVERSION}
procedure TIFFError(Module: Pointer; Fmt: Pointer); cdecl; external; varargs;
procedure TIFFWarning(Module: Pointer; Fmt: Pointer); cdecl; external; varargs;
{$ELSE}
procedure TIFFError(Module: Pointer; Fmt: Pointer); cdecl; external;
procedure TIFFWarning(Module: Pointer; Fmt: Pointer); cdecl; external;
{$ENDIF}
function TIFFSetErrorHandler(Handler: TIFFErrorHandler): TIFFErrorHandler; cdecl; external;
function LibTiffDelphiGetErrorHandler: LibTiffDelphiErrorHandler;
function LibTiffDelphiSetErrorHandler(Handler: LibTiffDelphiErrorHandler): LibTiffDelphiErrorHandler;
function TIFFSetWarningHandler(Handler: TIFFErrorHandler): TIFFErrorHandler; cdecl; external;
function LibTiffDelphiGetWarningHandler: LibTiffDelphiErrorHandler;
function LibTiffDelphiSetWarningHandler(Handler: LibTiffDelphiErrorHandler): LibTiffDelphiErrorHandler;
function TIFFSetTagExtender(Extender: TIFFExtendProc): TIFFExtendProc; cdecl; external;
function TIFFFlush(Handle: PTIFF): Integer; cdecl; external;
function TIFFFlushData(Handle: PTIFF): Integer; cdecl; external;
function TIFFReadDirectory(Handle: PTIFF): Integer; cdecl; external;
function TIFFCurrentDirectory(Handle: PTIFF): Word; cdecl; external;
function TIFFCurrentDirOffset(Handle: PTIFF): Cardinal; cdecl; external;
function TIFFLastDirectory(Handle: PTIFF): Integer; cdecl; external;
function TIFFNumberOfDirectories(Handle: PTIFF): Word; cdecl; external;
function TIFFSetDirectory(Handle: PTIFF; Dirn: Word): Integer; cdecl; external;
function TIFFSetSubDirectory(Handle: PTIFF; Diroff: Cardinal): Integer; cdecl; external;
function TIFFCreateDirectory(Handle: PTIFF): Integer; cdecl; external;
function TIFFWriteDirectory(Handle: PTIFF): Integer; cdecl; external;
function TIFFUnlinkDirectory(handle: PTIFF; Dirn: Word): Integer; cdecl; external;
procedure TIFFPrintDirectory(Handle: PTIFF; Fd: Pointer; Flags: Integer); cdecl; external;
{$IFNDEF VOLDVERSION}
function TIFFGetField(Handle: PTIFF; Tag: Cardinal): Integer; cdecl; external; varargs;
function TIFFGetFieldDefaulted(Handle: PTIFF; Tag: Cardinal): Integer; cdecl; external; varargs;
function TIFFSetField(Handle: PTIFF; Tag: Cardinal): Integer; cdecl; external; varargs;
{$ELSE}
function TIFFGetField(Handle: PTIFF; Tag: Cardinal; var Value: Cardinal): Integer; cdecl; external;
function TIFFGetFieldDefaulted(Handle: PTIFF; Tag: Cardinal; var Value: Cardinal): Integer; cdecl; external;
function TIFFSetField(Handle: PTIFF; Tag: Cardinal; var Value: Cardinal): Integer; cdecl; external;
{$ENDIF}
function TIFFVGetField(Handle: PTIFF; Tag: Cardinal; Ap: Pointer): Integer; cdecl; external;
function TIFFVSetField(Handle: PTIFF; Tag: Cardinal; Ap: Pointer): Integer; cdecl; external;
function TIFFIsBigEndian(Handle: PTIFF): Integer; cdecl; external;
function TIFFIsTiled(Handle: PTIFF): Integer; cdecl; external;
function TIFFIsByteSwapped(Handle: PTIFF): Integer; cdecl; external;
function TIFFIsUpSampled(Handle: PTIFF): Integer; cdecl; external;
function TIFFIsMSB2LSB(Handle: PTIFF): Integer; cdecl; external;
function TIFFGetTagListCount(Handle: PTIFF): Integer; cdecl; external;
function TIFFGetTagListEntry(Handle: PTIFF; TagIndex: Integer): Cardinal; cdecl; external;
procedure TIFFMergeFieldInfo(Handle: PTIFF; Info: PTIFFFieldInfo; N: Integer); cdecl; external;
function TIFFFindFieldInfo(Handle: PTIFF; Tag: Cardinal; Dt: Integer): PTIFFFieldInfo; cdecl; external;
function TIFFFindFieldInfoByName(Handle: PTIFF; FIeldName: PAnsiChar; Dt: Integer): PTIFFFieldInfo; cdecl; external;
function TIFFFieldWithTag(Handle: PTIFF; Tag: Cardinal): PTIFFFieldInfo; cdecl; external;
function TIFFFieldWithName(Handle: PTIFF; FieldName: PAnsiChar): PTIFFFieldInfo; cdecl; external;
function TIFFDataWidth(DataType: Integer): Integer; cdecl; external;
function TIFFReadRGBAImage(Handle: PTIFF; RWidth, RHeight: Cardinal; Raster: Pointer; Stop: Integer): Integer; cdecl; external;
function TIFFReadRGBAImageOriented(Handle: PTIFF; RWidth, RHeight: Cardinal; Raster: Pointer; Orientation: Integer; Stop: Integer): Integer; cdecl; external;
function TIFFReadRGBAStrip(Handle: PTIFF; Row: Cardinal; Raster: Pointer): Integer; cdecl; external;
function TIFFReadRGBATile(Handle: PTIFF; Col, Row: Cardinal; Raster: Pointer): Integer; cdecl; external;
function TIFFRGBAImageOk(Handle: PTIFF; Emsg: PAnsiChar): Integer; cdecl; external;
function TIFFRGBAImageBegin(Img: PTIFFRGBAImage; Handle: PTIFF; Stop: Integer; Emsg: PAnsiChar): Integer; cdecl; external;
function TIFFRGBAImageGet(Img: PTIFFRGBAImage; Raster: Pointer; W, H: Cardinal): Integer; cdecl; external;
procedure TIFFRGBAImageEnd(Img: PTIFFRGBAImage); cdecl; external;
function TIFFCurrentRow(Handle: PTIFF): Cardinal; cdecl; external;
function TIFFStripSize(Handle: PTIFF): Integer; cdecl; external;
function TIFFRawStripSize(Handle: PTIFF; Strip: Cardinal): Integer; cdecl; external;
function TIFFVStripSize(Handle: PTIFF; NRows: Cardinal): Integer; cdecl; external;
function TIFFDefaultStripSize(Handle: PTIFF; Request: Cardinal): Cardinal; cdecl; external;
function TIFFNumberOfStrips(Handle: PTIFF): Cardinal; cdecl; external;
function TIFFComputeStrip(Handle: PTIFF; Row: Cardinal; Sample: Word): Cardinal; cdecl; external;
function TIFFReadRawStrip(Handle: PTIFF; Strip: Cardinal; Buf: Pointer; Size: Integer): Integer; cdecl; external;
function TIFFReadEncodedStrip(Handle: PTIFF; Strip: Cardinal; Buf: Pointer; Size: Integer): Integer; cdecl; external;
function TIFFWriteRawStrip(Handle: PTIFF; Strip: Cardinal; Data: Pointer; Cc: Integer): Integer; cdecl; external;
function TIFFWriteEncodedStrip(Handle: PTIFF; Strip: Cardinal; Data: Pointer; Cc: Integer): Integer; cdecl; external;
function TIFFCurrentStrip(Handle: PTIFF): Cardinal; cdecl; external;
function TIFFTileSize(Handle: PTIFF): Integer; cdecl; external;
function TIFFTileRowSize(Handle: PTIFF): Integer; cdecl; external;
function TIFFVTileSize(Handle: PTIFF; NRows: Cardinal): Integer; cdecl; external;
procedure TIFFDefaultTileSize(Handle: PTIFF; Tw: PCardinal; Th: PCardinal); cdecl; external;
function TIFFNumberOfTiles(Handle: PTIFF): Cardinal; cdecl; external;
function TIFFComputeTile(Handle: PTIFF; X, Y, Z: Cardinal; S: Word): Cardinal; cdecl; external;
function TIFFReadRawTile(Handle: PTIFF; Tile: Cardinal; Buf: Pointer; Size: Integer): Integer; cdecl; external;
function TIFFReadEncodedTile(Handle: PTIFF; Tile: Cardinal; Buf: Pointer; Size: Integer): Integer; cdecl; external;
function TIFFWriteRawTile(Handle: PTIFF; Tile: Cardinal; Data: Pointer; Cc: Integer): Integer; cdecl; external;
function TIFFWriteEncodedTile(Handle: PTIFF; Tile: Cardinal; Data: Pointer; Cc: Integer): Integer; cdecl; external;
function TIFFCurrentTile(Handle: PTIFF): Cardinal; cdecl; external;
function TIFFScanlineSize(Handle: PTIFF): Integer; cdecl; external;
function TIFFRasterScanlineSize(Handle: PTIFF): Integer; cdecl; external;
function TIFFReadScanline(Handle: PTIFF; Buf: Pointer; Row: Cardinal; Sample: Word): Integer; cdecl; external;
function TIFFWriteScanline(Handle: PTIFF; Buf: Pointer; Row: Cardinal; Sample: Word): Integer; cdecl; external;
procedure TIFFSetWriteOffset(Handle: PTIFF; Off: Cardinal); cdecl; external;
procedure TIFFSwabShort(Wp: PWord); cdecl; external;
procedure TIFFSwabLong(Lp: PCardinal); cdecl; external;
procedure TIFFSwabDouble(Dp: PDouble); cdecl; external;
procedure TIFFSwabArrayOfShort(Wp: PWord; N: Cardinal); cdecl; external;
procedure TIFFSwabArrayOfLong(Lp: PCardinal; N: Cardinal); cdecl; external;
procedure TIFFSwabArrayOfDouble(Dp: PDouble; N: Cardinal); cdecl; external;
procedure TIFFReverseBits(Cp: Pointer; N: Cardinal); cdecl; external;
function TIFFGetBitRevTable(Reversed: Integer): Pointer; cdecl; external;
function _TIFFmalloc(s: Longint): Pointer; cdecl;
function _TIFFrealloc(p: Pointer; s: Longint): Pointer; cdecl;
procedure _TIFFfree(p: Pointer); cdecl;

implementation

uses
  Math, VPDFCLibs, VPDFJpegLib, VPDFZLib;

type

  TQsortCompare = function(a, b: Pointer): Integer; cdecl;
  TBsearchFcmp = function(a: Pointer; b: Pointer): Integer; cdecl;

function floor(x: Double): Double; cdecl; forward;

function pow(x: Double; y: Double): Double; cdecl; forward;

function sqrt(x: Double): Double; cdecl; forward;

function atan2(y: Double; x: Double): Double; cdecl; forward;

function exp(x: Double): Double; cdecl; forward;

function log(x: Double): Double; cdecl; forward;

function fabs(x: Double): Double; cdecl; forward;

function rand: Integer; cdecl; forward;

function strlen(s: Pointer): Cardinal; cdecl; forward;
{$IFNDEF VOLDVERSION}
function strcmp(a: Pointer; b: Pointer): Integer; cdecl; forward;
{$ENDIF}

function strncmp(a: Pointer; b: Pointer; c: Longint): Integer; cdecl; forward;

procedure qsort(base: Pointer; num: Cardinal; width: Cardinal; compare: TQSortCompare); cdecl; forward;

function bsearch(key: Pointer; base: Pointer; nelem: Cardinal; width: Cardinal; fcmp: TBsearchFcmp): Pointer; cdecl; forward;

function memmove(dest: Pointer; src: Pointer; n: Cardinal): Pointer; cdecl; forward;

function strchr(s: Pointer; c: Integer): Pointer; cdecl; forward;

procedure _TIFFmemcpy(d: Pointer; s: Pointer; c: Longint); cdecl; forward;

procedure _TIFFmemset(p: Pointer; v: Integer; c: Longint); cdecl; forward;

function _TIFFmemcmp(buf1: Pointer; buf2: Pointer; count: Cardinal): Integer; cdecl; forward;

var

  _TIFFwarningHandler: TIFFErrorHandler;
  _TIFFerrorHandler: TIFFErrorHandler;
  FLibTiffDelphiWarningHandler: LibTiffDelphiErrorHandler;
  FLibTiffDelphiErrorHandler: LibTiffDelphiErrorHandler;

function fabs(x: Double): Double;
begin
  if x < 0 then
    Result := -x
  else
    Result := x;
end;

function atan2(y: Double; x: Double): Double;
begin
  Result := ArcTan2(y, x);
end;

function rand: Integer; cdecl;
begin
  Result := Trunc(Random * ($7FFF + 1));
end;

function sqrt(x: Double): Double; cdecl;
begin
  Result := System.Sqrt(x);
end;

function log(x: Double): Double; cdecl;
begin
  Result := Ln(x);
end;

function exp(x: Double): Double; cdecl;
begin
  Result := System.Exp(x);
end;

function strchr(s: Pointer; c: Integer): Pointer; cdecl;
begin
  Result := s;
  while True do
  begin
    if PByte(Result)^ = c then exit;
    if PByte(Result)^ = 0 then
    begin
      Result := nil;
      exit;
    end;
    Inc(PByte(Result));
  end;
end;

function memmove(dest: Pointer; src: Pointer; n: Cardinal): Pointer; cdecl;
begin
  MoveMemory(dest, src, n);
  Result := dest;
end;

function _TIFFmemcmp(buf1: Pointer; buf2: Pointer; count: Cardinal): Integer; cdecl;
var
  ma, mb: PByte;
  n: Integer;
begin
  ma := buf1;
  mb := buf2;
  n := 0;
  while Cardinal(n) < Count do
  begin
    if ma^ <> mb^ then
    begin
      if ma^ < mb^ then
        Result := -1
      else
        Result := 1;
      exit;
    end;
    Inc(ma);
    Inc(mb);
    Inc(n);
  end;
  Result := 0;
end;

procedure _TIFFmemset(p: Pointer; v: Integer; c: Longint); cdecl;
begin
  FillMemory(p, c, v);
end;

function bsearch(key: Pointer; base: Pointer; nelem: Cardinal; width: Cardinal; fcmp: TBsearchFcmp): Pointer; cdecl;
begin
  raise Exception.Create('Bsearch - should presumably not occur');
end;

procedure qsort(base: Pointer; num: Cardinal; width: Cardinal; compare: TQSortCompare); cdecl;
var
  m: Pointer;
  n: Integer;
  o: Pointer;
  oa, ob, oc: Integer;
  p: Integer;
begin
  if num < 2 then exit;
  GetMem(m, num * width);
  if compare(base, Pointer(Cardinal(base) + width)) <= 0 then
    CopyMemory(m, base, (width shl 1))
  else
  begin
    CopyMemory(m, Pointer(Cardinal(base) + width), width);
    CopyMemory(Pointer(Cardinal(m) + width), base, width);
  end;
  n := 2;
  while Cardinal(n) < num do
  begin
    o := Pointer(Cardinal(base) + Cardinal(n) * width);
    if compare(m, o) >= 0 then
      ob := 0
    else
    begin
      oa := 0;
      ob := n;
      while oa + 1 < ob do
      begin
        oc := ((oa + ob) shr 1);
        p := compare(Pointer(Cardinal(m) + Cardinal(oc) * width), o);
        if p < 0 then
          oa := oc
        else if p = 0 then
        begin
          ob := oc;
          break;
        end
        else
          ob := oc;
      end;
    end;
    if ob = 0 then
    begin
      MoveMemory(Pointer(Cardinal(m) + width), m, Cardinal(n) * width);
      CopyMemory(m, o, width);
    end
    else if ob = n then
      CopyMemory(Pointer(Cardinal(m) + Cardinal(n) * width), o, width)
    else
    begin
      MoveMemory(Pointer(Cardinal(m) + Cardinal(ob + 1) * width), Pointer(Cardinal(m) + Cardinal(ob) * width),
        Cardinal(n - ob) * width);
      CopyMemory(Pointer(Cardinal(m) + Cardinal(ob) * width), o, width);
    end;
    Inc(n);
  end;
  CopyMemory(base, m, num * width);
  FreeMem(m, num * width);
end;

function _TIFFrealloc(p: Pointer; s: Longint): Pointer; cdecl;
var
  m: TMemoryManager;
begin
  GetMemoryManager(m);
  if p = nil then
    Result := m.GetMem(s)
  else
    Result := m.ReallocMem(p, s);
end;

function strncmp(a: Pointer; b: Pointer; c: Longint): Integer; cdecl;
var
  ma, mb: PByte;
  n: Integer;
begin
  ma := a;
  mb := b;
  n := 0;
  while n < c do
  begin
    if ma^ <> mb^ then
    begin
      if ma^ < mb^ then
        Result := -1
      else
        Result := 1;
      exit;
    end;
    if ma^ = 0 then
    begin
      Result := 0;
      exit;
    end;
    Inc(ma);
    Inc(mb);
    Inc(n);
  end;
  Result := 0;
end;

{$IFNDEF VOLDVERSION}
function strcmp(a: Pointer; b: Pointer): Integer; cdecl;
var
  ma, mb: PByte;
begin
  ma := a;
  mb := b;
  while True do
  begin
    if ma^ <> mb^ then
    begin
      if ma^ < mb^ then
        Result := -1
      else
        Result := 1;
      exit;
    end;
    if ma^ = 0 then
    begin
      Result := 0;
      exit;
    end;
    Inc(ma);
    Inc(mb);
  end;
  Result := 0;
end;
{$ENDIF}

function strlen(s: Pointer): Cardinal; cdecl;
var
  m: PByte;
begin
  Result := 0;
  m := s;
  while m^ <> 0 do
  begin
    Inc(Result);
    Inc(m);
  end;
end;

procedure _TIFFfree(p: Pointer); cdecl;
var
  m: TMemoryManager;
begin
  GetMemoryManager(m);
  m.FreeMem(p);
end;

procedure _TIFFmemcpy(d: Pointer; s: Pointer; c: Longint); cdecl;
begin
  CopyMemory(d, s, c);
end;

function pow(x: Double; y: Double): Double; cdecl;
begin
  Result := Power(x, y);
end;

function floor(x: Double): Double; cdecl;
begin
  Result := Trunc(x);
end;

function _TIFFmalloc(s: Longint): Pointer; cdecl;
var
  m: TMemoryManager;
begin
  GetMemoryManager(m);
  Result := m.GetMem(s);
end;

procedure TIFFReadRGBAImageSwapRB(Width, Height: Cardinal; Memory: Pointer);
type
  PCarPoints = ^Cardinal;
var
  m: PCarPoints;
  n: Cardinal;
  o: Cardinal;
begin
  m := Memory;
  for n := 0 to Width * Height - 1 do
  begin
    o := m^;
    m^ := (o and $FF00FF00) or
      ((o and $00FF0000) shr 16) or
      ((o and $000000FF) shl 16);
    Inc(m);
  end;
end;

function LibTiffDelphiVersion: AnsiString;
var
  m: AnsiString;
  na, nb: Integer;
begin
  Result := '';
  m := TIFFGetVersion;
  na := 1;
  while True do
  begin
    nb := na;
    while nb <= Length(m) do
    begin
      if m[nb] = #10 then break;
      Inc(nb);
    end;
    Result := Result + Copy(m, na, nb - na);
    if nb > Length(m) then break;
    Result := Result + #13#10;
    na := nb + 1;
  end;
  Result := Result;
end;

procedure LibTiffDelphiWarningThrp(a: Pointer; b: Pointer; c: Pointer); cdecl;
var
  m: Integer;
  n: AnsiString;
begin
  if @FLibTiffDelphiWarningHandler <> nil then
  begin
    m := sprintfsec(nil, b, @c);
    SetLength(n, m);
    sprintfsec(Pointer(n), b, @c);
    FLibTiffDelphiWarningHandler(PAnsiChar(a), n);
  end;
end;

procedure LibTiffDelphiErrorThrp(a: Pointer; b: Pointer; c: Pointer); cdecl;
var
  m: Integer;
  n: AnsiString;
begin
  if @FLibTiffDelphiErrorHandler <> nil then
  begin
    m := sprintfsec(nil, b, @c);
    SetLength(n, m);
    sprintfsec(Pointer(n), b, @c);
    FLibTiffDelphiErrorHandler(PAnsiChar(a), n);
  end;
end;

function LibTiffDelphiGetWarningHandler: LibTiffDelphiErrorHandler;
begin
  Result := FLibTiffDelphiWarningHandler;
end;

function LibTiffDelphiSetWarningHandler(Handler: LibTiffDelphiErrorHandler): LibTiffDelphiErrorHandler;
begin
  Result := FLibTiffDelphiWarningHandler;
  FLibTiffDelphiWarningHandler := Handler;
end;

function LibTiffDelphiGetErrorHandler: LibTiffDelphiErrorHandler;
begin
  Result := FLibTiffDelphiErrorHandler;
end;

function LibTiffDelphiSetErrorHandler(Handler: LibTiffDelphiErrorHandler): LibTiffDelphiErrorHandler;
begin
  Result := FLibTiffDelphiErrorHandler;
  FLibTiffDelphiErrorHandler := Handler;
end;

procedure _TIFFSwab16BitData(tif: Pointer; buf: Pointer; cc: Integer); cdecl; external;

procedure _TIFFSwab32BitData(tif: Pointer; buf: Pointer; cc: Integer); cdecl; external;

procedure _TIFFSwab64BitData(tif: Pointer; buf: Pointer; cc: Integer); cdecl; external;

procedure _TIFFNoPostDecode(tif: Pointer; buf: Pointer; cc: Integer); cdecl; external;

function TIFFReadTile(tif: Pointer; buf: Pointer; x: Cardinal; y: Cardinal; z: Cardinal; s: Word): Integer; cdecl; external;

{$L ..\..\Objs\Tiff\tif_read.obj}

function _TIFFSampleToTagType(tif: Pointer): Integer; cdecl; external;

procedure _TIFFSetupFieldInfo(tif: Pointer); cdecl; external;

function _TIFFCreateAnonFieldInfo(tif: Pointer; tag: Cardinal; field_type: Integer): Pointer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_dirinfo.obj}
{$L ..\..\Objs\Tiff\tif_dirwrite.obj}
{$L ..\..\Objs\Tiff\tif_flush.obj}

function TIFFFlushData1(tif: Pointer): Integer; cdecl; external;

function TIFFSetupStrips(tif: Pointer): Integer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_write.obj}

function TIFFInitDumpMode(tif: Pointer; scheme: Integer): Integer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_dumpmode.obj}

function TIFFSetCompressionScheme(tif: Pointer; scheme: Integer): Integer; cdecl; external;

procedure _TIFFSetDefaultCompressionState(tif: Pointer); cdecl; external;
{$L ..\..\Objs\Tiff\tif_compress.obj}
{$L ..\..\Objs\Tiff\tif_dirread.obj}

procedure TIFFFreeDirectory(tif: Pointer); cdecl; external;

function TIFFDefaultDirectory(tif: Pointer): Integer; cdecl; external;

function TIFFReassignTagToIgnore(task: Integer; TIFFtagID: Integer): Integer; cdecl; external;

procedure _TIFFsetString(cpp: Pointer; cp: Pointer); cdecl; external;

procedure _TIFFsetByteArray(vpp: Pointer; vp: Pointer; n: Integer); cdecl; external;
{$L ..\..\Objs\Tiff\tif_dir.obj}

function TIFFVGetFieldDefaulted(tif: Pointer; tag: Cardinal; ap: Pointer): Integer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_aux.obj}

procedure TIFFCIELabToXYZ(cielab: Pointer; l: Cardinal; a: Integer; b: Integer; X: Pointer; Y: Pointer; Z: Pointer); cdecl; external;

procedure TIFFXYZToRGB(cielab: Pointer; X: Single; Y: Single; Z: Single; r: Pointer; g: Pointer; b: Pointer); cdecl; external;

procedure TIFFYCbCrtoRGB(ycbcr: Pointer; Y: Cardinal; Cb: Integer; Cr: Integer; r: Pointer; g: Pointer; b: Pointer); cdecl; external;

function TIFFYCbCrToRGBInit(ycbcr: Pointer; luma: Pointer; refBlackWhite: Pointer): Integer; cdecl; external;

function TIFFCIELabToRGBInit(cielab: Pointer; display: Pointer; refWhite: Pointer): Integer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_color.obj}
{$L ..\..\Objs\Tiff\tif_close.obj}
{$L ..\..\Objs\Tiff\tif_extension.obj}

function _TIFFgetMode(mode: PAnsiChar; module: PAnsiChar): Integer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_open.obj}
{$L ..\..\Objs\Tiff\tif_getimage.obj}

function TIFFPredictorInit(tif: PTIFF): Integer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_predict.obj}
{$L ..\..\Objs\Tiff\tif_print.obj}
{$L ..\..\Objs\Tiff\tif_error.obj}

function _TIFFDefaultStripSize(tif: Pointer; s: Cardinal): Cardinal; cdecl; external;
{$L ..\..\Objs\Tiff\tif_strip.obj}
{$L ..\..\Objs\Tiff\tif_swab.obj}

function TIFFCheckTile(tif: Pointer; x: Cardinal; y: Cardinal; z: Cardinal; s: Word): Integer; cdecl; external;

procedure _TIFFDefaultTileSize(tif: Pointer; tw: Pointer; th: Pointer); cdecl; external;
{$L ..\..\Objs\Tiff\tif_tile.obj}
{$L ..\..\Objs\Tiff\tif_warning.obj}

function TIFFInitCCITTRLE(tif: PTIFF; scheme: Integer): Integer; cdecl; external;

function TIFFInitCCITTRLEW(tif: PTIFF; scheme: Integer): Integer; cdecl; external;

function TIFFInitCCITTFax3(tif: PTIFF; scheme: Integer): Integer; cdecl; external;

function TIFFInitCCITTFax4(tif: PTIFF; scheme: Integer): Integer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_fax3.obj}
{$L ..\..\Objs\Tiff\tif_fax3sm.obj}

procedure TIFFjpeg_error_exit_raise; cdecl;
begin
  raise Exception.Create('TIFFjpeg_error_exit');
end;

function TIFFcallvjpeg_jpeg_CreateCompress(cinfo: Pointer; version: Integer; structsize: Cardinal): Integer; cdecl;
begin
  try
    jpeg_CreateCompress(cinfo, version, structsize);
    Result := 1;
  except
    Result := 0;
  end;
end;

function TIFFcallvjpeg_jpeg_CreateDecompress(cinfo: Pointer; version: Integer; structsize: Cardinal): Integer; cdecl;
begin
  try
    jpeg_CreateDecompress(cinfo, version, structsize);
    Result := 1;
  except
    Result := 0;
  end;
end;

function TIFFcallvjpeg_jpeg_set_defaults(cinfo: Pointer): Integer; cdecl;
begin
  try
    jpeg_set_defaults(cinfo);
    Result := 1;
  except
    Result := 0;
  end;
end;

function TIFFcallvjpeg_jpeg_set_colorspace(cinfo: Pointer; colorspace: Integer): Integer; cdecl;
begin
  try
    jpeg_set_colorspace(cinfo, colorspace);
    Result := 1;
  except
    Result := 0;
  end;
end;

function TIFFcallvjpeg_jpeg_set_quality(cinfo: Pointer; quality: Integer; force_baseline: Byte): Integer; cdecl;
begin
  try
    jpeg_set_quality(cinfo, quality, force_baseline);
    Result := 1;
  except
    Result := 0;
  end;
end;

function TIFFcallvjpeg_jpeg_suppress_tables(cinfo: PRJpegCompressStruct; suppress: Byte): Integer; cdecl;
begin
  try
    jpeg_suppress_tables(cinfo, suppress);
    Result := 1;
  except
    Result := 0;
  end;
end;

function TIFFcallvjpeg_jpeg_start_compress(cinfo: PRJpegCompressStruct; write_all_tables: Byte): Integer; cdecl;
begin
  try
    jpeg_start_compress(cinfo, write_all_tables);
    Result := 1;
  except
    Result := 0;
  end;
end;

function TIFFcalljpeg_jpeg_write_scanlines(errreturn: Integer; cinfo: PRJpegCompressStruct; scanlines: Pointer;
  num_lines: Cardinal): Integer; cdecl;
begin
  try
    Result := jpeg_write_scanlines(cinfo, scanlines, num_lines);
  except
    Result := errreturn;
  end;
end;

function TIFFcalljpeg_jpeg_write_raw_data(errreturn: Integer; cinfo: PRJpegCompressStruct; data: Pointer; num_lines:
  Cardinal): Integer; cdecl;
begin
  try
    Result := jpeg_write_raw_data(cinfo, data, num_lines);
  except
    Result := errreturn;
  end;
end;

function TIFFcallvjpeg_jpeg_finish_compress(cinfo: PRJpegCompressStruct): Integer; cdecl;
begin
  try
    jpeg_finish_compress(cinfo);
    Result := 1;
  except
    Result := 0;
  end;
end;

function TIFFcallvjpeg_jpeg_write_tables(cinfo: PRJpegCompressStruct): Integer; cdecl;
begin
  try
    jpeg_write_tables(cinfo);
    Result := 1;
  except
    Result := 0;
  end;
end;

function TIFFcalljpeg_jpeg_read_header(errreturn: Integer; cinfo: PRJpegDecompressStruct; require_image: Byte): Integer;
  cdecl;
begin
  try
    Result := jpeg_read_header(cinfo, Boolean(require_image));
  except
    Result := errreturn;
  end;
end;

function TIFFcallvjpeg_jpeg_start_decompress(cinfo: PRJpegDecompressStruct): Integer; cdecl;
begin
  try
    jpeg_start_decompress(cinfo);
    Result := 1;
  except
    Result := 0;
  end;
end;

function TIFFcalljpeg_jpeg_read_scanlines(errreturn: Integer; cinfo: PRJpegDecompressStruct; scanlines: Pointer;
  max_lines: Cardinal): Integer; cdecl;
begin
  try
    Result := jpeg_read_scanlines(cinfo, scanlines, max_lines);
  except
    Result := errreturn;
  end;
end;

function TIFFcalljpeg_jpeg_read_raw_data(errreturn: Integer; cinfo: PRJpegDecompressStruct; data: Pointer; max_lines:
  Cardinal): Integer; cdecl;
begin
  try
    Result := jpeg_read_raw_data(cinfo, data, max_lines);
  except
    Result := errreturn;
  end;
end;

function TIFFcalljpeg_jpeg_finish_decompress(errreturn: Integer; cinfo: PRJpegDecompressStruct): Integer; cdecl;
begin
  try
    Result := jpeg_finish_decompress(cinfo);
  except
    Result := errreturn;
  end;
end;

function TIFFcallvjpeg_jpeg_abort(cinfo: PRJpegCommonStruct): Integer; cdecl;
begin
  try
    jpeg_abort(cinfo);
    Result := 1;
  except
    Result := 0;
  end;
end;

function TIFFcallvjpeg_jpeg_destroy(cinfo: PRJpegCommonStruct): Integer; cdecl;
begin
  try
    jpeg_destroy(cinfo);
    Result := 1;
  except
    Result := 0;
  end;
end;

type
  jpeg_alloc_sarray = function(cinfo: PRJpegCommonStruct; pool_id: Integer; samplesperrow: Cardinal; numrows:
    Cardinal): Pointer; cdecl;

function TIFFcalljpeg_alloc_sarray(alloc_sarray: jpeg_alloc_sarray; cinfo: PRJpegCommonStruct; pool_id: Integer;
  samplesperrow: Cardinal;
  numrows: Cardinal): Pointer; cdecl;
begin
  try
    Result := alloc_sarray(cinfo, pool_id, samplesperrow, numrows);
  except
    Result := nil;
  end;
end;

function TIFFInitJPEG(tif: PTIFF; scheme: Integer): Integer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_jpeg.obj}

function TIFFInitSGILog(tif: PTIFF; scheme: Integer): Integer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_luv.obj}

function TIFFInitLZW(tif: PTIFF; scheme: Integer): Integer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_lzw.obj}

function TIFFInitNeXT(tif: PTIFF; scheme: Integer): Integer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_next.obj}

function TIFFInitPackBits(tif: PTIFF; scheme: Integer): Integer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_packbits.obj}

function TIFFInitPixarLog(tif: PTIFF; scheme: Integer): Integer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_pixarlog.obj}

function TIFFInitThunderScan(tif: PTIFF; scheme: Integer): Integer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_thunder.obj}
{$L ..\..\Objs\Tiff\tif_version.obj}

function TIFFInitZIP(tif: PTIFF; scheme: Integer): Integer; cdecl; external;
{$L ..\..\Objs\Tiff\tif_zip.obj}

function NotConfigured(tif: PTIFF; scheme: Integer): Integer; cdecl; external;
const
  _TIFFBuiltinCODECS: array[0..17] of TIFFCodec = (
    (name: 'None'; scheme: COMPRESSION_NONE; init: TIFFInitDumpMode),
    (name: 'LZW'; scheme: COMPRESSION_LZW; init: TIFFInitLZW),
    (name: 'PackBits'; scheme: COMPRESSION_PACKBITS; init: TIFFInitPackBits),
    (name: 'ThunderScan'; scheme: COMPRESSION_THUNDERSCAN; init: TIFFInitThunderScan),
    (name: 'NeXT'; scheme: COMPRESSION_NEXT; init: TIFFInitNeXT),
    (name: 'JPEG'; scheme: COMPRESSION_JPEG; init: TIFFInitJPEG),
    (name: 'Old-style JPEG'; scheme: COMPRESSION_OJPEG; init: NotConfigured),
    (name: 'CCITT RLE'; scheme: COMPRESSION_CCITTRLE; init: TIFFInitCCITTRLE),
    (name: 'CCITT RLE/W'; scheme: COMPRESSION_CCITTRLEW; init: TIFFInitCCITTRLEW),
    (name: 'CCITT Group 3'; scheme: COMPRESSION_CCITTFAX3; init: TIFFInitCCITTFax3),
    (name: 'CCITT Group 4'; scheme: COMPRESSION_CCITTFAX4; init: TIFFInitCCITTFax4),
    (name: 'ISO JBIG'; scheme: COMPRESSION_JBIG; init: NotConfigured),
    (name: 'Deflate'; scheme: COMPRESSION_DEFLATE; init: TIFFInitZIP),
    (name: 'AdobeDeflate'; scheme: COMPRESSION_ADOBE_DEFLATE; init: TIFFInitZIP),
    (name: 'PixarLog'; scheme: COMPRESSION_PIXARLOG; init: TIFFInitPixarLog),
    (name: 'SGILog'; scheme: COMPRESSION_SGILOG; init: TIFFInitSGILog),
    (name: 'SGILog24'; scheme: COMPRESSION_SGILOG24; init: TIFFInitSGILog),
    (name: nil; scheme: 0; init: nil));
{$L ..\..\Objs\Tiff\tif_codec.obj}

function TIFFFileReadProc(Fd: Cardinal; Buffer: Pointer; Size: Integer): Integer; cdecl; forward;

function TIFFFileWriteProc(Fd: Cardinal; Buffer: Pointer; Size: Integer): Integer; cdecl; forward;

function TIFFFileSizeProc(Fd: Cardinal): Cardinal; cdecl; forward;

function TIFFFileSeekProc(Fd: Cardinal; Off: Cardinal; Whence: Integer): Cardinal; cdecl; forward;

function TIFFFileCloseProc(Fd: Cardinal): Integer; cdecl; forward;

function TIFFStreamReadProc(Fd: Cardinal; Buffer: Pointer; Size: Integer): Integer; cdecl; forward;

function TIFFStreamWriteProc(Fd: Cardinal; Buffer: Pointer; Size: Integer): Integer; cdecl; forward;

function TIFFStreamSizeProc(Fd: Cardinal): Cardinal; cdecl; forward;

function TIFFStreamSeekProc(Fd: Cardinal; Off: Cardinal; Whence: Integer): Cardinal; cdecl; forward;

function TIFFStreamCloseProc(Fd: Cardinal): Integer; cdecl; forward;

function TIFFNoMapProc(Fd: Cardinal; PBase: PPointer; PSize: PCardinal): Integer; cdecl; forward;

procedure TIFFNoUnmapProc(Fd: Cardinal; Base: Pointer; Size: Cardinal); cdecl; forward;

function TIFFFileCloseProc(Fd: Cardinal): Integer; cdecl;
begin
  if CloseHandle(Fd) = True then
    Result := 0
  else
    Result := -1;
end;

function TIFFFileSizeProc(Fd: Cardinal): Cardinal; cdecl;
begin
  Result := GetFileSize(Fd, nil);
end;

function TIFFFileSeekProc(Fd: Cardinal; Off: Cardinal; Whence: Integer): Cardinal; cdecl;
const
  SEEK_SET = 0;
  SEEK_CUR = 1;
  SEEK_END = 2;
var
  MoveMethod: Cardinal;
begin
  if Off = $FFFFFFFF then
  begin
    Result := $FFFFFFFF;
    exit;
  end;
  case Whence of
    SEEK_SET: MoveMethod := FILE_BEGIN;
    SEEK_CUR: MoveMethod := FILE_CURRENT;
    SEEK_END: MoveMethod := FILE_END;
  else
    MoveMethod := FILE_BEGIN;
  end;
  Result := SetFilePointer(Fd, Off, nil, MoveMethod);
end;

function TIFFFileReadProc(Fd: Cardinal; Buffer: Pointer; Size: Integer): Integer; cdecl;
var
  m: Cardinal;
begin
  if ReadFile(Fd, Buffer^, Cardinal(Size), m, nil) = False then
    Result := 0
  else
    Result := m;
end;

function TIFFFileWriteProc(Fd: Cardinal; Buffer: Pointer; Size: Integer): Integer; cdecl;
var
  m: Cardinal;
begin
  if WriteFile(Fd, Buffer^, Cardinal(Size), m, nil) = False then
    Result := 0
  else
    Result := m;
end;

function TIFFStreamCloseProc(Fd: Cardinal): Integer; cdecl;
begin
  Result := 0;
end;

function TIFFStreamSizeProc(Fd: Cardinal): Cardinal; cdecl;
begin
  try
    Result := TStream(Fd).Size;
  except
    Result := 0;
  end;
end;

function TIFFStreamSeekProc(Fd: Cardinal; Off: Cardinal; Whence: Integer): Cardinal; cdecl;
const
  SEEK_SET = 0;
  SEEK_CUR = 1;
  SEEK_END = 2;
var
  MoveMethod: Word;
begin
  if Off = $FFFFFFFF then
  begin
    Result := $FFFFFFFF;
    exit;
  end;
  case Whence of
    SEEK_SET: MoveMethod := soFromBeginning;
    SEEK_CUR: MoveMethod := soFromCurrent;
    SEEK_END: MoveMethod := soFromEnd;
  else
    MoveMethod := soFromBeginning;
  end;
  try
    Result := TStream(Fd).Seek(Off, MoveMethod);
  except
    Result := 0;
  end;
end;

function TIFFStreamReadProc(Fd: Cardinal; Buffer: Pointer; Size: Integer): Integer; cdecl;
begin
  try
    Result := TStream(Fd).Read(Buffer^, Size);
  except
    Result := 0;
  end;
end;

function TIFFStreamWriteProc(Fd: Cardinal; Buffer: Pointer; Size: Integer): Integer; cdecl;
begin
  try
    Result := TStream(Fd).Write(Buffer^, Size);
  except
    Result := 0;
  end;
end;

function TIFFNoMapProc(Fd: Cardinal; PBase: PPointer; PSize: PCardinal): Integer; cdecl;
begin
  Result := 0;
end;

procedure TIFFNoUnmapProc(Fd: Cardinal; Base: Pointer; Size: Cardinal); cdecl;
begin
end;

function TIFFOpen(const Name: AnsiString; const Mode: AnsiString): PTIFF;
const
  Module: AnsiString = 'TIFFOpen';
  O_RDONLY = 0;
  O_WRONLY = 1;
  O_RDWR = 2;
  O_CREAT = $0100;
  O_TRUNC = $0200;
var
  m: Integer;
  wcName: WideString;
  DesiredAccess: Cardinal;
  CreateDisposition: Cardinal;
  FlagsAndAttributes: Cardinal;
  fd: THandle;
begin
  m := _TIFFgetMode(PAnsiChar(Mode), PAnsiChar(Module));
  if m = o_RDONLY then
    DesiredAccess := GENERIC_READ
  else
    DesiredAccess := (GENERIC_READ or GENERIC_WRITE);
  case m of
    O_RDONLY: CreateDisposition := OPEN_EXISTING;
    O_RDWR: CreateDisposition := OPEN_ALWAYS;
    (O_RDWR or O_CREAT): CreateDisposition := OPEN_ALWAYS;
    (O_RDWR or O_TRUNC): CreateDisposition := CREATE_ALWAYS;
    (O_RDWR or O_CREAT or O_TRUNC): CreateDisposition := CREATE_ALWAYS;
  else
    Result := nil;
    exit;
  end;
  if m = O_RDONLY then
    FlagsAndAttributes := FILE_ATTRIBUTE_READONLY
  else
    FlagsAndAttributes := FILE_ATTRIBUTE_NORMAL;
  wcName := WideString(Name);
{$IFDEF FCSUPPORTING}
  fd := CreateFile(PWideChar(wcName), DesiredAccess, FILE_SHARE_READ, nil, CreateDisposition, FlagsAndAttributes, 0);
{$ELSE}
  fd := CreateFile(PChar(Name), DesiredAccess, FILE_SHARE_READ, nil, CreateDisposition, FlagsAndAttributes, 0);
{$ENDIF}
  if fd = INVALID_HANDLE_VALUE then
  begin
{$IFNDEF VOLDVERSION}
    TiffError(PAnsiChar(Module), PAnsiChar('%s: Cannot open'), PAnsiChar(Name));
{$ELSE}
    TiffError(PAnsiChar(Module), PAnsiChar('%s: Cannot open'));
{$ENDIF}
    Result := nil;
    exit;
  end;
  Result := TIFFClientOpen(PAnsiChar(Name), PAnsiChar(Mode), fd, @TIFFFileReadProc, @TIFFFileWriteProc, @TIFFFileSeekProc,
    @TIFFFileCloseProc,
    @TIFFFileSizeProc, @TIFFNoMapProc, @TIFFNoUnmapProc);
  if Result <> nil then
    TIFFSetFileno(Result, fd)
  else
    CloseHandle(fd);
end;

function TIFFOpenStream(const Stream: TStream; const Mode: AnsiString): PTIFF;
var
  m: AnsiString;
begin
  m := 'Stream';
  Result := TIFFClientOpen(PAnsiChar(m), PAnsiChar(Mode), Cardinal(Stream), @TIFFStreamReadProc, @TIFFStreamWriteProc,
    @TIFFStreamSeekProc, @TIFFStreamCloseProc,
    @TIFFStreamSizeProc, @TIFFNoMapProc, @TIFFNoUnmapProc);
  if Result <> nil then TIFFSetFileno(Result, Cardinal(Stream));
end;

initialization
  _TIFFwarningHandler := LibTiffDelphiWarningThrp;
  _TIFFerrorHandler := LibTiffDelphiErrorThrp;

end.
