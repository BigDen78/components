{*******************************************************}
{                                                       }
{       This unit is part of the VISPDF VCL library.    }
{       Written by R.Husske - ALL RIGHTS RESERVED.      }
{                                                       }
{       Copyright (C) 2000-2009, www.vispdf.com         }
{                                                       }
{       e-mail: support@vispdf.com                      }
{       http://www.vispdf.com                           }
{                                                       }
{*******************************************************}

unit VPDFZLibConst;

interface

{$I VisPDFLib.inc }

resourcestring
  sTargetBufferTooSmall = 'Target buffer may be too small';
  sInvalidStreamOp = 'Invalid stream operation';

implementation

end.
